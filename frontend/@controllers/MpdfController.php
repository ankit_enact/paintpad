<?php

namespace frontend\controllers;

use Yii;
use app\models\TblCommunications;
use app\models\TblCommunicationsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

use app\models\TblContacts;
use app\models\TblAddress;
use app\models\TblQuotes;
use common\models\User;

use app\models\TblUsers;

use app\models\TblUsersDetails;

use yii\helpers\ArrayHelper;


class MpdfController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /* function to generate quote PDF */

    public function actionQuotepdf()
    {


        $modelQuery        = array();
        $data              = array();
        $qdata             = array();
        $cdata             = array();
        $quote_description = '';
        $client_name       = '';
        $client_email      = '';
        $client_phone      = '';
        $sub_email         = '';
        $sub_name          = '';
        $sub_phone         = '';
        $siteAddress       = '';


        /*$json_string   = '{"qid":"11",
                            "scope":"Painting 4 Rooms with Dulux",
                            "includes":"Ceilings, Cornices, Cornice Coves, Ceiling Rose - Ornate, Walls, Feature Wall (enter m2), Decorative Arch - Plaster, Decorative Arch",
                            "spl":[
                                "A Colour Consultant",
                                "Bring the covers"
                            ],
                            "image":"http://enacteservices.com/paintpad/image/logo.png",
                            "subtotal":"38,634.31",
                            "gst":"3,863.43",
                            "total":"42,497.74",
                            "deposit":"1,586.52",
                            "balance":"40,911.22",
                            "start":"04/07/2018",
                            "completion":"14 BUSINESS DAYS"}';*/


        $json_string = '{"qid":"11",
                        "scope":"Painting 4 Rooms with Dulux",
                        "brand_logo":"http://enacteservices.com/paintpad/image/Main_logo.jpg",
                        "image":"http://enacteservices.com/paintpad/image/logo.png",
                        "nop":"2",
                        "qty":"144.5L",
                        "hrs":"223.43",
                        "start":"04/07/2018",
                        "end":"23/07/2018",
                        "spl":[
                            "A Colour Consultant",
                            "Bring the covers"
                        ],
                        "rooms":[
                            {
                                "name":"Bedroom 1",
                                "hrs":"190:43",
                                "prep_level":"Premium",
                                "components":[
                                    {
                                        "name":"Ceilings - Standard (120m2)",
                                        "product":"DLX CEILING WHITE , Flat Acrylic",
                                        "color":"Colorbond Ironstone Single",
                                        "time":"4:48",
                                        "paint":"10"
                                    },
                                    {
                                        "name":"Cornices - Standard - 55mm",
                                        "product":"DLX CEILING WHITE , Flat Acrylic",
                                        "color":"Colorbond Ironstone Single",
                                        "time":"0:15",
                                        "paint":"0.22"
                                    }
                                ]
                            },
                            {
                                "name":"Bedroom 2",
                                "hrs":"5:14",
                                "prep_level":"Premium",
                                "components":[
                                    {
                                        "name":"Ceilings - Standard (120m2)",
                                        "product":"DLX CEILING WHITE , Flat Acrylic",
                                        "color":"Colorbond Ironstone Single",
                                        "time":"4:48",
                                        "paint":"10"
                                    },
                                    {
                                        "name":"Cornices - Standard - 55mm",
                                        "product":"DLX CEILING WHITE , Flat Acrylic",
                                        "color":"Colorbond Ironstone Single",
                                        "time":"0:13",
                                        "paint":"0.22"
                                    }
                                ]
                            },
                            {
                                "name":"Bedroom 3",
                                "hrs":"7:24",
                                "prep_level":"Premium",
                                "components":[
                                    {
                                        "name":"Ceilings - Standard (120m2)",
                                        "product":"DLX CEILING WHITE , Flat Acrylic",
                                        "color":"Colorbond Ironstone Single",
                                        "time":"4:48",
                                        "paint":"10"
                                    },
                                    {
                                        "name":"Cornices - Standard - 55mm",
                                        "product":"DLX CEILING WHITE , Flat Acrylic",
                                        "color":"Colorbond Ironstone Single",
                                        "time":"0:15",
                                        "paint":"0.22"
                                    }
                                ]
                            }
                        ],
                        "materials":[
                            {
                                "name":"Dulux",
                                "stocks":[
                                    {
                                        "litres":"34L",
                                        "product":"DLX CEILING WHITE , 2x15L , 1x4L , Flat Acrylic",
                                        "Colour":"Colorbond Ironstone Single"
                                    },
                                    {
                                        "litres":"4L",
                                        "product":"DLX AQUANAMEL , 1x4L , Semi Gloss Acrylic",
                                        "Colour":"Colorbond Ironstone Single"
                                    }
                                ]
                            }
                        ]
                    }';                    


        $mydata = json_decode($json_string, TRUE);

        $qid        = $mydata['qid'];
        $image      = $mydata['image'];
        $brand_logo = $mydata['brand_logo'];
        
        //echo 'qid'.$mydata['qid']; exit;

        if(!empty($qid)){

            $modelQuery = TblQuotes::find()->joinWith('contact')->joinWith('siteAddress')->where([ 'tbl_quotes.quote_id'=>11])->orderBy('quote_id DESC')->all();
            //echo "<pre>"; print_r($modelQuery); exit;


            if(count($modelQuery)>0){

                $subId = $modelQuery[0]->subscriber_id;

                $userData = TblUsers::find()
                                ->joinWith('tblUsersDetails')                             
                                ->where([ 'tbl_users.user_id' => $subId])
                                ->all();

                //echo '<pre>'; print_r($userData); exit;

                    if(count($userData)>0){

                        $sub_email         = $userData[0]->email;
                        $sub_name          = $userData[0]->tblUsersDetails[0]->name;
                        $sub_phone         = $userData[0]->tblUsersDetails[0]->phone;

                    }

                    $quote_description = $modelQuery[0]->description;
                    $quote_date        = date('m/d/Y',$modelQuery[0]->created_at);
                    $client_name       = $modelQuery[0]->contact->name;
                    $client_email      = $modelQuery[0]->contact->email;
                    $client_phone      = $modelQuery[0]->contact->phone;

                    $siteAddress       = $modelQuery[0]->siteAddress->formatted_address;    

            }
        }



        $data   = $mydata;

        $mpdf   = new \Mpdf\Mpdf(['margin_left'=>8,'margin_right'=>8,'margin_top'=>8]);

        $mpdf->SetHTMLHeader('<div>
                            <table>
                                <tr>
                                    <td style="height: 15px"></td>
                                </tr>
                            </table>
                            <table cellpadding="0" cellspacing="0" style="border-bottom: 0px solid #e1e1e1; width:100%;">
                                <tr style=" font-size: 12px; text-align: right; float: right;">
                                    <td style="text-align: right;">
                                        <h1 style="padding-bottom: 13px;color:#0a80c5; font-size: 19px; font-weight: normal; text-align: right;font-family: Helvetica, sans-serif;">INTERIOR PAINTING QUOTE</h1>
                                    </td>
                                </tr>
                            </table>
                            <table class="heading" style="border-bottom: 2px solid #e1e1e1; width:100%;">                   
                                <tr>          
                                    <td style="padding-right: 295px;">
                                    <div> <span> <img src="'.$brand_logo.'" alt="" style="width:100px;"> </span></div>
                                    </td>
                                    <td style="background:#ececec;">
                                        <table class="" style="border:0px; background:#ececec;">
                                            <tr>
                                                <td style=" border:0px;padding: 0 8px 0 10px;color:#525251;font-weight: normal;text-align: left;">
                                                    <table class="" style="border:0px; background:#ececec;">
                                                    <tr>
                                                    <td><b style="color:#0a86c2; font-size: 12px; text-transform: uppercase;">Quote No.</b></td>
                                                    </tr>
                                                    <tr>
                                                    <td style="font-size: 11px;color:#62615e">'.$qid.'</td>
                                                    </tr>
                                                    </table>
                                                </td>                        
                                                <td style=" border:0px;padding: 0 0 0 2px;color:#525251; font-weight: normal;text-align: left;">
                                                    <table class="" style="border:0px; background:#ececec;">
                                                    <tr>
                                                    <td><b style="color:#0a86c2; font-size: 12px; text-transform: uppercase; ">Quote Date</b></td>
                                                    </tr>
                                                    <tr>
                                                    <td style="font-size: 11px;color:#62615e">'.$quote_date.'</td>
                                                    </tr>
                                                    </table>
                                                </td>
                                                <td style=" border:0px;padding: 0 0 0 5px;color:#525251; text-align: left;">
                                                    <table class="" style="border:0px;">
                                                    <tr>
                                                    <td><img width= "100px" style="padding: 0;" src="'.$image.'" alt=""></td>
                                                    </tr>
                                                    <tr>
                                                    <td></td>
                                                    </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div> 
                        <div>
                            <table>
                                    <tr>
                                        <td style="height: 10px"></td>
                                    </tr>
                            </table>    
                            <table>     
                                <tr>
                                    <td style="width:18%; margin-bottom: 13px;color:#0a80c5; font-size: 13px; font-weight: bold; float: left;width: 100%; text-align: left;">'.$client_name.'</td>
                                    <td style="width:30%; margin-bottom: 13px;color:#0a80c5; font-size: 13px; font-weight: bold; float: left;width: 100%; text-align: left;">Site Details</td>
                                    <td style="width:10%; margin-bottom: 13px;color:#0a80c5; font-size: 13px; font-weight: bold; float: left;width: 100%; text-align: left;">Quoted by</td>
                                </tr>
                                <tr>
                                    <td style="font-size: 11px;color:#62615e">Contact:'.$client_name.'</td>
                                    <td style="font-size: 11px;color:#62615e">Contact:'.$client_name.'</td>
                                    <td style="font-size: 11px;color:#62615e">'.$sub_name.'</td>
                                </tr>
                                <tr>
                                    <td style="font-size: 11px;color:#62615e">Mobile:'.$client_phone.'</td>
                                    <td style="font-size: 11px;color:#62615e">Mobile:'.$client_phone.'</td>
                                    <td style="font-size: 11px;color:#62615e">'.$sub_phone.'</td>
                                </tr>
                                <tr>
                                    <td style="font-size: 11px;color:#62615e"></td>
                                    <td style="font-size: 11px;color:#62615e">'.$siteAddress.'</td>
                                    <td style="font-size: 11px;color:#62615e">'.$sub_email.'</td>
                                </tr>   
                            </table>
                            <table>
                                <tr>
                                    <td style="height: 10px"></td>
                                </tr>
                        </table>
                        </div>');





        $mpdf->SetHTMLFooter('
        <table width="100%">
            <tr>
                <td width="33%"></td>
                <td width="33%" align="center">{PAGENO}/{nbpg}</td>
                <td width="33%" style="text-align: right;"></td>
            </tr>
        </table>');


        $mpdf->AddPage('', // L - landscape, P - portrait 
                '', '', '', '',
                5, // margin_left
                5, // margin right
               60, // margin top
               30, // margin bottom
                0, // margin header
                0); // margin footer
           


        //$mpdf->WriteHTML('Hello World');


        $mpdf->WriteHTML($this->renderPartial('jss',array('header'=>$header, 'data'=>$data)));
        $mpdf->Output();
        exit;
    } //actionQuotepdf



    /* function to generate quote PDF */

    public function actionPdf()
    {

        $modelQuery        = array();
        $data              = array();
        $qdata             = array();
        $cdata             = array();
        $quote_description = '';
        $client_name       = '';
        $client_email      = '';
        $client_phone      = '';
        $client_addr       = '';
        $sub_email         = '';
        $sub_name          = '';
        $sub_phone         = '';
        $siteAddress       = '';


        $json_string   = '{"qid":"11",
                            "scope":"Painting 4 Rooms with Dulux",
                            "includes":"Ceilings, Cornices, Cornice Coves, Ceiling Rose - Ornate, Walls, Feature Wall (enter m2), Decorative Arch - Plaster, Decorative Arch",
                            "rooms_summary":"Bedroom 1, Bedroom 2, Bedroom 3, Entrance",
                            "color_consultant":"0",
                            "spl":[
                                "Bedroom 1- Bring additional brush",
                                "Bedroom 1 - Bring the covers"
                            ],
                            "brand_logo":"http://enacteservices.com/paintpad/image/Main_logo.jpg",
                            "image":"http://enacteservices.com/paintpad/image/logo.png",
                            "rooms":"Bedroom1,Bedroom2,Bedroom3,Entrance",
                            "subtotal":"38,634.31",
                            "gst":"3,863.43",
                            "total":"42,497.74",
                            "deposit":"1,586.52",
                            "balance":"40,911.22",
                            "start":"04/07/2018",
                            "completion":"14 BUSINESS DAYS"}';

        $mydata = json_decode($json_string, TRUE);

        $qid = $mydata['qid'];
        //echo 'qid'.$mydata['qid']; exit;

        if(!empty($qid)){
          $modelQuery = TblQuotes::find()->joinWith('contact')->joinWith('siteAddress')->where([ 'tbl_quotes.quote_id'=>$qid])->orderBy('quote_id DESC')->all();
          //echo "<pre>"; print_r($modelQuery); exit;

          if(count($modelQuery)>0){

                $subId = $modelQuery[0]->subscriber_id;

                $userData = TblUsers::find()
                                ->joinWith('tblUsersDetails')                             
                                ->where([ 'tbl_users.user_id' => $subId])
                                ->all();

                //echo '<pre>'; print_r($userData); exit;

                    if(count($userData)>0){

                        $sub_email         = $userData[0]->email;
                        $sub_name          = $userData[0]->tblUsersDetails[0]->name;
                        $sub_phone         = $userData[0]->tblUsersDetails[0]->phone;

                    }

                    $quote_description = $modelQuery[0]->description;
                    $quote_date        = date('m/d/Y',$modelQuery[0]->created_at);
                    $client_name       = $modelQuery[0]->contact->name;
                    $client_email      = $modelQuery[0]->contact->email;
                    $client_phone      = $modelQuery[0]->contact->phone;
                    $client_addid      = $modelQuery[0]->contact->address_id;


                    if($client_addid != ''){
                        $model = TblAddress::find()
                                   ->select('formatted_address')
                                   ->where(['address_id' => $client_addid])
                                   ->one();

                        $client_addr = $model->formatted_address;
                    }


                    $siteAddress       = $modelQuery[0]->siteAddress->formatted_address;    

                      /* client data */
                      $cdata['cname']  = $client_name;
                      $cdata['cemail'] = $client_email;
                      $cdata['cphone'] = $client_phone;

                      /* quote data */
                      $qdata['number']      = $qid;
                      $qdata['description'] = $quote_description;
                      $qdata['date']        = $quote_date;

          }

        }

        $header = 'INTERIOR PAINTING QUOTE';
        $data   = $mydata;

        //$mpdf   = new \Mpdf\Mpdf(['','','','','margin_left'=>8,'margin_right'=>8,'','','','','']);
        $mpdf   = new \Mpdf\Mpdf(['margin_left'=>8,'margin_right'=>8,'margin_top'=>8]);

        $mpdf->SetHTMLFooter('
        <table width="100%">
            <tr>
                <td width="33%"></td>
                <td width="33%" align="center">{PAGENO}/{nbpg}</td>
                <td width="33%" style="text-align: right;"></td>
            </tr>
        </table>');

        
        $mpdf->WriteHTML($this->renderPartial('pdf',array('header'=>$header, 'cdata'=>$cdata, 'qdata'=>$qdata, 'data'=>$data, 'sub_email'=>$sub_email, 'sub_name'=>$sub_name, 'sub_phone'=>$sub_phone,'siteAddress'=>$siteAddress,'client_addr'=>$client_addr)));
        $mpdf->Output();
        exit;

    } //actionPdf



    /* function to generate quote PDF */

    public function actionQuotepdfnew()
    {

        $modelQuery        = array();
        $data              = array();
        $qdata             = array();
        $cdata             = array();
        $quote_description = '';
        $client_name       = '';
        $client_email      = '';
        $client_phone      = '';
        $client_addr      = '';
        $sub_email         = '';
        $sub_name          = '';
        $sub_phone         = '';
        $siteAddress       = '';


        $json_string = '{"qid":"11",
                        "scope":"PAINTING 4 ROOMS WITH DULUX WASH & WEAR PLUS, ONE COAT OF PAINT, PREP LEVEL PREMIUM",
                        "brand_logo":"http://enacteservices.com/paintpad/image/Main_logo.jpg",
                        "image":"http://enacteservices.com/paintpad/image/logo.png",
                        "rooms_summary":"Bedroom 1, Bedroom 2, Bedroom 3, Entrance",
                        "color_consultant":"1",
                        "nop":"2",
                        "qty":"144.5L",
                        "hrs":"223.43",
                        "start":"04/07/2018",
                        "end":"23/07/2018",
                        "spl":[
                            "Bedroom 1- Bring additional brush",
                            "Bedroom 1 - Bring the covers"
                        ],
                        "rooms":[
                            {
                                "name":"Bedroom 1",
                                "hrs":"190:43",
                                "prep_level":"Premium",
                                "components":[
                                    {
                                        "name":"Ceilings - Standard (120m2)",
                                        "product":"DLX CEILING WHITE , Flat Acrylic",
                                        "color":"Colorbond Ironstone Single",
                                        "time":"4:48",
                                        "paint":"10"
                                    },
                                    {
                                        "name":"Cornices - Standard - 55mm",
                                        "product":"DLX CEILING WHITE , Flat Acrylic",
                                        "color":"Colorbond Ironstone Single",
                                        "time":"0:15",
                                        "paint":"0.22"
                                    },
                                    {
                                        "name":"Cornices - Standard - 55mm",
                                        "product":"DLX CEILING WHITE , Flat Acrylic",
                                        "color":"Colorbond Ironstone Single",
                                        "time":"0:15",
                                        "paint":"0.22"
                                    }
                                ]
                            },
                            {
                                "name":"Bedroom 2",
                                "hrs":"5:14",
                                "prep_level":"Premium",
                                "components":[
                                    {
                                        "name":"Ceilings - Standard (120m2)",
                                        "product":"DLX CEILING WHITE , Flat Acrylic",
                                        "color":"Colorbond Ironstone Single",
                                        "time":"4:48",
                                        "paint":"10"
                                    },
                                    {
                                        "name":"Cornices - Standard - 55mm",
                                        "product":"DLX CEILING WHITE , Flat Acrylic",
                                        "color":"Colorbond Ironstone Single",
                                        "time":"0:13",
                                        "paint":"0.22"
                                    }                       
                                ]
                            },
                            {
                                "name":"Bedroom 3",
                                "hrs":"7:24",
                                "prep_level":"Premium",
                                "components":[
                                    {
                                        "name":"Ceilings - Standard (120m2)",
                                        "product":"DLX CEILING WHITE , Flat Acrylic",
                                        "color":"Colorbond Ironstone Single",
                                        "time":"4:48",
                                        "paint":"10"
                                    },
                                    {
                                        "name":"Cornices - Standard - 55mm",
                                        "product":"DLX CEILING WHITE , Flat Acrylic",
                                        "color":"Colorbond Ironstone Single",
                                        "time":"0:15",
                                        "paint":"0.22"
                                    }
                                ]
                            }
                        ],
                        "materials":[
                            {
                                "name":"Dulux",
                                "stocks":[
                                    {
                                        "litres":"34L",
                                        "product":"DLX CEILING WHITE , 2x15L , 1x4L , Flat Acrylic",
                                        "Colour":"Colorbond Ironstone Single"
                                    },
                                    {
                                        "litres":"4L",
                                        "product":"DLX AQUANAMEL , 1x4L , Semi Gloss Acrylic",
                                        "Colour":"Colorbond Ironstone Single"
                                    }
                                ]
                            },
                            {
                                "name":"Dulux",
                                "stocks":[
                                    {
                                        "litres":"34L",
                                        "product":"DLX CEILING WHITE , 2x15L , 1x4L , Flat Acrylic",
                                        "Colour":"Colorbond Ironstone Single"
                                    },
                                    {
                                        "litres":"4L",
                                        "product":"DLX AQUANAMEL , 1x4L , Semi Gloss Acrylic",
                                        "Colour":"Colorbond Ironstone Single"
                                    }
                                ]
                            },
                            {
                                "name":"Dulux",
                                "stocks":[
                                    {
                                        "litres":"34L",
                                        "product":"DLX CEILING WHITE , 2x15L , 1x4L , Flat Acrylic",
                                        "Colour":"Colorbond Ironstone Single"
                                    },
                                    {
                                        "litres":"4L",
                                        "product":"DLX AQUANAMEL , 1x4L , Semi Gloss Acrylic",
                                        "Colour":"Colorbond Ironstone Single"
                                    }
                                ]
                            }
                        ]
                    }';                    


        $mydata = json_decode($json_string, TRUE);

        $qid        = $mydata['qid'];
        $image      = $mydata['image'];
        $brand_logo = $mydata['brand_logo'];
        $header = 'Job Specification';
        
        //echo 'qid'.$mydata['qid']; exit;

        if(!empty($qid)){

            $modelQuery = TblQuotes::find()->joinWith('contact')->joinWith('siteAddress')->where([ 'tbl_quotes.quote_id'=>$qid])->orderBy('quote_id DESC')->all();
            //echo "<pre>"; print_r($modelQuery); exit;

            if(count($modelQuery)>0){

                $subId = $modelQuery[0]->subscriber_id;

                $userData = TblUsers::find()
                                ->joinWith('tblUsersDetails')                             
                                ->where([ 'tbl_users.user_id' => $subId])
                                ->all();

                //echo '<pre>'; print_r($userData); exit;

                    if(count($userData)>0){
                        $sub_email         = $userData[0]->email;
                        $sub_name          = $userData[0]->tblUsersDetails[0]->name;
                        $sub_phone         = $userData[0]->tblUsersDetails[0]->phone;
                    }

                    $quote_description = $modelQuery[0]->description;
                    $quote_date        = date('m/d/Y',$modelQuery[0]->created_at);

                    $client_name       = $modelQuery[0]->contact->name;
                    $client_email      = $modelQuery[0]->contact->email;
                    $client_phone      = $modelQuery[0]->contact->phone;
                    $siteAddress       = $modelQuery[0]->siteAddress->formatted_address;

                    $client_addid      = $modelQuery[0]->contact->address_id;

                    if($client_addid != ''){
                        $model = TblAddress::find()
                                   ->select('formatted_address')
                                   ->where(['address_id' => $client_addid])
                                   ->one();

                        $client_addr = $model->formatted_address;
                    }
               

            }
        }



        $data   = $mydata;

        $mpdf   = new \Mpdf\Mpdf(['margin_left'=>8,'margin_right'=>8,'margin_top'=>8]);

        $mpdf->SetHTMLHeader('<div>
                            <table>
                                <tr>
                                    <td style="height: 15px"></td>
                                </tr>
                            </table>
                            <table cellpadding="0" cellspacing="0" style="border-bottom: 0px solid #e1e1e1; width:100%;">
                                <tr style=" font-size: 12px; text-align: right; float: right;">
                                    <td style="text-align: right;">
                                        <h1 style="padding-bottom: 13px;color:#0a80c5; font-size: 19px; font-weight: normal; text-align: right;font-family: Helvetica, sans-serif;">INTERIOR PAINTING QUOTE</h1>
                                    </td>
                                </tr>
                            </table>
                            <table class="heading" style="border-bottom: 2px solid #e1e1e1; width:100%;">                   
                                <tr>          
                                    <td style="padding-right: 295px;">
                                    <div> <span> <img src="'.$brand_logo.'" alt="" style="width:100px;"> </span></div>
                                    </td>
                                    <td style="background:#ececec;">
                                        <table class="" style="border:0px; background:#ececec;">
                                            <tr>
                                                <td style=" border:0px;padding: 0 8px 0 10px;color:#525251;font-weight: normal;text-align: left;">
                                                    <table class="" style="border:0px; background:#ececec;">
                                                    <tr>
                                                    <td><b style="color:#0a86c2; font-size: 12px; text-transform: uppercase;">Quote No.</b></td>
                                                    </tr>
                                                    <tr>
                                                    <td style="font-size: 11px;color:#62615e">'.$qid.'</td>
                                                    </tr>
                                                    </table>
                                                </td>                        
                                                <td style=" border:0px;padding: 0 0 0 2px;color:#525251; font-weight: normal;text-align: left;">
                                                    <table class="" style="border:0px; background:#ececec;">
                                                    <tr>
                                                    <td><b style="color:#0a86c2; font-size: 12px; text-transform: uppercase; ">Quote Date</b></td>
                                                    </tr>
                                                    <tr>
                                                    <td style="font-size: 11px;color:#62615e">'.$quote_date.'</td>
                                                    </tr>
                                                    </table>
                                                </td>
                                                <td style=" border:0px;padding: 0 0 0 5px;color:#525251; text-align: left;">
                                                    <table class="" style="border:0px;">
                                                    <tr>
                                                    <td><img width= "100px" style="padding: 0;" src="'.$image.'" alt=""></td>
                                                    </tr>
                                                    <tr>
                                                    <td></td>
                                                    </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div> 
                        <div>
                            <table>
                                    <tr>
                                        <td style="height: 10px"></td>
                                    </tr>
                            </table>    
                            <table>     
                                <tr>
                                    <td style="width:18%; margin-bottom: 13px;color:#0a80c5; font-size: 13px; font-weight: bold; float: left;width: 100%; text-align: left;"><b>'.$client_name.'</b></td>
                                    <td style="width:30%; margin-bottom: 13px;color:#0a80c5; font-size: 13px; font-weight: bold; float: left;width: 100%; text-align: left;"><b>Site Details</b></td>
                                    <td style="width:10%; margin-bottom: 13px;color:#0a80c5; font-size: 13px; font-weight: bold; float: left;width: 100%; text-align: left;"><b>Quoted by</b></td>
                                </tr>
                                <tr>
                                    <td style="font-size: 11px;color:#62615e"><b>Contact:'.$client_name.'</b></td>
                                    <td style="font-size: 11px;color:#62615e"><b>Contact:'.$client_name.'</b></td>
                                    <td style="font-size: 11px;color:#62615e"><b>'.ucfirst($sub_name).'</td>
                                </tr>
                                <tr>
                                    <td style="font-size: 11px;color:#62615e"><b>Mobile:'.$client_phone.'</b></td>
                                    <td style="font-size: 11px;color:#62615e"><b>Mobile:'.$client_phone.'</b></td>
                                    <td style="font-size: 11px;color:#62615e">'.$sub_phone.'</td>
                                </tr>
                                <tr>
                                    <td style="font-size: 11px;color:#62615e">'.$client_addr.'</td>
                                    <td style="font-size: 11px;color:#62615e;vertical-align: text-top;">'.$siteAddress.'</td>
                                    <td style="font-size: 11px;color:#62615e;vertical-align: text-top;">'.$sub_email.'</td>
                                </tr>   
                            </table>
                            <table>
                                <tr>
                                    <td style="height: 10px"></td>
                                </tr>
                        </table>
                        </div>');





        $mpdf->SetHTMLFooter('
        <table width="100%">
            <tr>
                <td width="33%"></td>
                <td width="33%" align="center">{PAGENO}/{nbpg}</td>
                <td width="33%" style="text-align: right;"></td>
            </tr>
        </table>');


        $mpdf->AddPage('', // L - landscape, P - portrait 
                '', '', '', '',
                5, // margin_left
                5, // margin right
               60, // margin top
               30, // margin bottom
                0, // margin header
                0); // margin footer
           


        //$mpdf->WriteHTML('Hello World');


        $mpdf->WriteHTML($this->renderPartial('jssnewcopy',array('header'=>$header, 'data'=>$data, 'quote_description'=>$quote_description, 'qid'=>$qid, 'quote_date'=>$quote_date)));
        $mpdf->Output();
        exit;

    } //actionQuotepdfnew

public function actionPdfcopy()
    {

        $modelQuery        = array();
        $data              = array();
        $qdata             = array();
        $cdata             = array();
        $quote_description = '';
        $client_name       = '';
        $client_email      = '';
        $client_phone      = '';
        $sub_email         = '';
        $sub_name          = '';
        $sub_phone         = '';
        $siteAddress       = '';


        $json_string   = '{"qid":"11",
                            "scope":"Painting 4 Rooms with Dulux",
                            "includes":"Ceilings, Cornices, Cornice Coves, Ceiling Rose - Ornate, Walls, Feature Wall (enter m2), Decorative Arch - Plaster, Decorative Arch",
                            "spl":[
                                "A Colour Consultant - Professional on-site Colour Consultation - 1hr - You will receive an individiual consultation to assist you select the right colours for you. Selections will be added to our Job Specification Sheet",
                                "Bedroom 1 - Bring the sheets to cover furniture while painting!"
                            ],
                            "brand_logo":"http://enacteservices.com/paintpad/image/Main_logo.jpg",
                            "image":"http://enacteservices.com/paintpad/image/logo.png",
                            "rooms":"Bedroom1,Bedroom2,Bedroom3,Entrance",
                            "subtotal":"38,634.31",
                            "gst":"3,863.43",
                            "total":"42,497.74",
                            "deposit":"1,586.52",
                            "balance":"40,911.22",
                            "start":"04/07/2018",
                            "completion":"14 BUSINESS DAYS"}';

        $mydata = json_decode($json_string, TRUE);

        $qid = $mydata['qid'];
        //echo 'qid'.$mydata['qid']; exit;

        if(!empty($qid)){
          $modelQuery = TblQuotes::find()->joinWith('contact')->joinWith('siteAddress')->where([ 'tbl_quotes.quote_id'=>$qid])->orderBy('quote_id DESC')->all();
          //echo "<pre>"; print_r($modelQuery); exit;

          if(count($modelQuery)>0){

                $subId = $modelQuery[0]->subscriber_id;

                $userData = TblUsers::find()
                                ->joinWith('tblUsersDetails')                             
                                ->where([ 'tbl_users.user_id' => $subId])
                                ->all();

                //echo '<pre>'; print_r($userData); exit;

                    if(count($userData)>0){

                        $sub_email         = $userData[0]->email;
                        $sub_name          = $userData[0]->tblUsersDetails[0]->name;
                        $sub_phone         = $userData[0]->tblUsersDetails[0]->phone;

                    }

                    $quote_description = $modelQuery[0]->description;
                    $quote_date        = date('m/d/Y',$modelQuery[0]->created_at);
                    $client_name       = $modelQuery[0]->contact->name;
                    $client_email      = $modelQuery[0]->contact->email;
                    $client_phone      = $modelQuery[0]->contact->phone;

                    $siteAddress       = $modelQuery[0]->siteAddress->formatted_address;    

                      /* client data */
                      $cdata['cname']  = $client_name;
                      $cdata['cemail'] = $client_email;
                      $cdata['cphone'] = $client_phone;

                      /* quote data */
                      $qdata['number']      = $qid;
                      $qdata['description'] = $quote_description;
                      $qdata['date']        = $quote_date;

          }

        }

        $header = 'INTERIOR PAINTING QUOTE';
        $data   = $mydata;

        //$mpdf   = new \Mpdf\Mpdf(['','','','','margin_left'=>8,'margin_right'=>8,'','','','','']);
        $mpdf   = new \Mpdf\Mpdf(['margin_left'=>8,'margin_right'=>8,'margin_top'=>8]);

        $mpdf->SetHTMLFooter('
        <table width="100%">
            <tr>
                <td width="33%"></td>
                <td width="33%" align="center">{PAGENO}/{nbpg}</td>
                <td width="33%" style="text-align: right;"></td>
            </tr>
        </table>');

        
        $mpdf->WriteHTML($this->renderPartial('pdfcopy',array('header'=>$header, 'cdata'=>$cdata, 'qdata'=>$qdata, 'data'=>$data, 'sub_email'=>$sub_email, 'sub_name'=>$sub_name, 'sub_phone'=>$sub_phone,'siteAddress'=>$siteAddress)));
        $mpdf->Output();
        exit;

    } //actionPdfcopy



}
