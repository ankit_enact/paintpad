<?php

namespace frontend\controllers;

use Yii;
use app\models\UserDetails;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\Model;
use yii\filters\auth\QueryParamAuth;
use yii\web\UploadedFile;
use yii\helpers\Url;

use common\models\LoginForm;
use common\models\SignupForm;
use common\models\User;
use app\models\TblUsers;
use app\models\TblUsersDetails;
use app\models\TblContacts;
use app\models\TblAddress;
use app\models\TblQuotes;
use app\models\TblCommunications;
use app\models\TblAppointments;
use app\models\TblAppointmentType;
use app\models\TblMembers;
use app\models\TblInvoices;
use app\models\TblBrands;
use app\models\TblTiers;
use app\models\TblStrengths;
use app\models\TblSheen;
use app\models\TblColorTags;
use app\models\TblColors;
use app\models\TblComponents;
use app\models\TblComponentGroups;
use app\models\TblComponentType;
use app\models\TblSpecialItems;
use app\models\TblRoomTypes;
use app\models\TblNotes;
use app\models\TblBrandPref;
use app\models\TblPaintDefaults;

use app\models\TblRooms;
use app\models\TblRoomPref;
use app\models\TblRoomCompPref;
use app\models\TblRoomDimensions;
use app\models\TblRoomSpecialItems;
use app\models\TblRoomNotes;


use app\models\TblProductStock;


class WebservicecopyController extends Controller
{

    /**
     * @inheritdoc
     */


    public $totals;


    public function behaviors(){

		return [
		    'verbs' => [
		        'class' => VerbFilter::className(),
		        'actions' => [
		            'quote-summary'=> ['POST'],
		        ],
		    ],
		    /*'authenticator' =>[
		    	'class'=> QueryParamAuth::className(),
		    ],*/
		];

    }

    public function actionQuoteSummary()
    {
    	$payments        = array();
    	$balance         = array();
    	$quoteInclusions = array();

		$noOfPainters        = '';
		$daysToComplete      = '';
		$dateEstCommencement = '';
		$dateEstCompletion   = '';
		$authorisedPerson    = '';
		$totalExGst          = '';
		$gst                 = '';
		$totalInGst          = '';
    	
	    	$payments = array(
				    		array("desc"=>"Initial Payment","amount"=>200,"part_payment"=>25,"date"=>153434304,"payment_schedule_id"=>1,"payment_date"=>15455435), 
				    		array("desc"=>"Initial Payment","amount"=>200,"part_payment"=>25,"date"=>153434304,"payment_schedule_id"=>2,"payment_date"=>15455435), 
				    		array("desc"=>"Initial Payment","amount"=>200,"part_payment"=>25,"date"=>153434304,"payment_schedule_id"=>3,"payment_date"=>15455435)
			    		);
	    	$balance = array("amount"=>100,"date"=>153434304);
	    	$quoteInclusions = array(
	    							array("name"=>"Entrance","count"=>1), 
    								array("name"=>"Kitchen","count"=>2)
    							);
	    	
			$noOfPainters        = 1;
			$daysToComplete      = 88;
			$dateEstCommencement = 153727432;
			$dateEstCompletion   = 153727432;
			$authorisedPerson    = "Bravo";
			$totalExGst          = 3267;
			$gst                 = 67;
			$totalInGst          = 4329;

				$response = [
					'success' => 1,
					'message' => "Quote Summary data",
					'data'=>['payments'=>$payments,'balance'=>$balance,'noOfPainters'=>$noOfPainters,'daysToComplete'=>$daysToComplete,'dateEstCommencement'=>$dateEstCommencement,'dateEstCompletion'=>$dateEstCompletion,'authorisedPerson'=>$authorisedPerson,'quoteInclusions'=>$quoteInclusions,'totalExGst'=>$totalExGst,'gst'=>$gst,'totalInGst'=>$totalInGst]
				];
				echo json_encode($response);exit();

    } //quote-summary
}