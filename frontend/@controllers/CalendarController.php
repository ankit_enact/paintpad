<?php

namespace frontend\controllers;
use Yii;
use app\models\event;
use app\models\eventSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\TblAppointments;
use app\models\TblContacts;
use yii\helpers\Url;
/**
 * EventController implements the CRUD actions for event model.
 */
class CalendarController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all event models.
     * @return mixed
     */
    public function actionIndex()
    {
		$events=TblAppointments::find()->all();

		$tasks=[];
		foreach($events as $eve)
		{
			$event = new \yii2fullcalendar\models\event();
			$event->id = $eve->id;
			$event->title = $eve->note;
			$date=date("Y-m-d H:i:s", $eve->date);
			$event->start = $date;
			$tasks[] = $event;
		}
		return $this->render('index', [
			'event'=>$tasks,
			'appointment'=>$events,
		]);

    }

    public function actionEvents(){
        $events=TblAppointments::find()->all();

        $tasks=[];
        foreach($events as $eve)
        {

            $event = new \yii2fullcalendar\models\event();
            $event->id = $eve->id;
            $event->title = $eve->note;
            $date=date("Y-m-d H:i:s", $eve->date);
            $event->start = $date;
            $tasks[] = $event;
        }
        echo json_encode($tasks);exit();
    }
   

    /**
     * Displays a single event model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new event model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TblAppointments();
        $data=1;
        $dataContact = TblContacts::find()->select('name,contact_id,subscriber_id')->all();
        if ($model->load(Yii::$app->request->post())) 
        {
            
                //date_default_timezone_set('UTC');
                $calender=$_POST['calender'];
                $f= $_POST['dateSelect']['From'];
                $from=strtotime($_POST['dateSelect']['From']);
                $combined_date_and_time = $calender.''.$f;
                $ca =strtotime($combined_date_and_time);
                $to=strtotime($_POST['dateSelect']['to']);
                $duration=$to-$from;
                $model->type=$_POST['TblAppointments']['type'];
                $model->note=$_POST['TblAppointments']['note'];
                $model->member_id=$_POST['TblAppointments']['member_id'];
                $model->allDay=$_POST['TblAppointments']['allDay'];
                $model->duration=$duration;
                $model->date=$ca;
                $flag=$model->save(false);
                if($flag)
                {
                    return $data;
                }
        }

        return $this->renderAjax('create', [
            'model' => $model,
            'dataContact'=>$dataContact,
        ]);
    }

    /**
     * Updates an existing event model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
      
        $model = $this->findModel($id);
        $data=1;

         $dataContact = TblContacts::find()->all();
        if ($model->load(Yii::$app->request->post())) {  
         // echo "<pre>";
         //       print_r($_POST);  
              date_default_timezone_set('UTC');
              
              
                $calender=$_POST['calender'];
                $f= $_POST['dateSelect']['From'];
                $from=strtotime($_POST['dateSelect']['From']);
                $combined_date_and_time = $calender.''.$f;
                $ca =strtotime($combined_date_and_time);
                $to=strtotime($_POST['dateSelect']['to']);
                $duration=$to-$from;
                $model->type=$_POST['TblAppointments']['type'];
                $model->note=$_POST['TblAppointments']['note'];
                $model->member_id=$_POST['TblAppointments']['member_id'];
                $model->allDay=$_POST['TblAppointments']['allDay'];
                $model->duration=$duration;
                $model->date=$ca;
                $flag=$model->save(false);
                if($flag)
                {
                    return $data;
                }
        }

        return $this->renderAjax('update', [
            'model' => $model,
            'dataContact'=>$dataContact,
        ]);
    }

    public function actionSearchfield()
    {
        if(isset($_POST))
        {
            $ev=$_POST['event'];
            $event = TblContacts::find()
                ->filterWhere(['LIKE', "name", $_POST['name']])
                ->orderBy(['contact_id' => SORT_ASC])
                ->all();
                
           // print_r($_POST['name']);
           // echo "ravi";
             return $this->renderPartial('notesearch', [
                    'event' => $event,
                    'ev'=>$ev,
             ]);
        }
    }

    /**
     * Deletes an existing event model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the event model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return event the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TblAppointments::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
