<?php

namespace frontend\controllers;

use Yii;
use app\models\TblContacts;
use app\models\TblContactsSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;
use yii\web\UploadedFile;

use app\models\TblBrands;
use app\models\TblBrandsSearch;
use app\models\TblAddress;
use app\models\TblQuotes;

/**
 * ContactController implements the CRUD actions for TblContacts model.
 */
class ContactController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {      
        if ($this->action->id == 'index') {
            Yii::$app->controller->enableCsrfValidation = false;
        }
        return true;
    }



    /**
     * Lists all TblContacts models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TblContactsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $cntModel = new TblContacts();       
        $addModel = new TblAddress();

        $brandModel = new TblBrandsSearch();
        $brandProvider = $brandModel->search(Yii::$app->request->queryParams);

        $sub_id = 1;

        $model = TblContacts::find()                          
                ->joinWith('address')
                ->joinWith('tblQuotes')
                ->joinWith('tblCommunications')
                ->joinWith('tblInvoices')
                ->joinWith('tblAppointments')
                ->where([ 'tbl_contacts.subscriber_id' => $sub_id])
                ->orderBy('tbl_quotes.quote_id DESC')            
                ->offset(0)
                ->limit(50)                    
                ->all();

        if ($cntModel->load(Yii::$app->request->post()) && $addModel->load(Yii::$app->request->post())) {

            $files = UploadedFile::getInstance($cntModel, 'image');

            $image = '';

            if(!empty($files)){
                $varr = time().rand(1,100);
                $files->saveAs('uploads/' . $varr .'_' . $files->name);
                $image = $varr .'_' . $files->name;
            }

            date_default_timezone_set("UTC");
            $milliseconds = strtotime(gmdate("Y-m-d H:i:s"));

            $current_uid = Yii::$app->user->identity->id;
            $request = Yii::$app->request->post();
        
            // to save data when posted
                $street1     = $request['TblAddress']['street1'];
                $suburb      = $request['TblAddress']['suburb'];
                $state_id    = $request['TblAddress']['state_id'];
                $postal_code = $request['TblAddress']['postal_code'];

                    $addModel->street1     = $street1;
                    $addModel->suburb      = $suburb;
                    $addModel->state_id    = $state_id;
                    $addModel->postal_code = $postal_code;
                    $addModel->created_at  = $milliseconds;
                    $addModel->updated_at  = $milliseconds;   

                 if($addModel->save(false)){

                    $add_id = $addModel->address_id;

                        $name  = $request['TblContacts']['name'];
                        $email = $request['TblContacts']['email'];
                        $phone = $request['TblContacts']['phone'];

                            $cntModel->address_id    = $add_id;
                            $cntModel->subscriber_id = $current_uid;
                            $cntModel->name          = $name;
                            $cntModel->email         = $email;
                            $cntModel->phone         = $phone;
                            $cntModel->image         = $image;
                            $cntModel->created_at    = $milliseconds;
                            $cntModel->updated_at    = $milliseconds;

                            $cntModel->save(false);


                            $data = 1;
                            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                            return $data;

                }

        }else{

            return $this->render('index', [
                'searchModel'   => $searchModel,
                'dataProvider'  => $dataProvider,
                'brandModel'    => $brandModel,
                'brandProvider' => $brandProvider,
                'model'         => $model,

                'cntModel' => $cntModel,                
                'addModel' => $addModel,
                
            ]);

        }

    } //actionIndex

    /**
     * Displays a single TblContacts model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),            
        ]);
    }

    /**
     * Creates a new TblContacts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model    = new TblContacts();
        $addModel = new TblAddress();

        //if ($model->load(Yii::$app->request->post()) && $model->save()) {
        if ($model->load(Yii::$app->request->post())) {

            $model->image = UploadedFile::getInstance($model, 'image');

            return $this->redirect(['view', 'id' => $model->contact_id]);
        }

        /*return $this->render('create', [
            'model' => $model,
        ]);*/

        return $this->renderAjax('create', [
            'model' => $model,
        ]);

    }

    /**
     * Updates an existing TblContacts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->contact_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TblContacts model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    public function actionDetail()
    {
        //$model = new TblContacts();

        /*$contactData = TblContacts::find()
            ->all();

        return $this->render('detail', [
            'contactData' => $contactData,
        ]);*/

        /*$contactProvider = new ActiveDataProvider([
            'query' => TblContacts::find(),
        ]);*/


        $sub_id = 1;

        $model = TblContacts::find()                        
                ->joinWith('address')
                ->joinWith(['tblQuotes' => function($q){ $q->joinWith('siteAddress')->orderBy('quote_id DESC')->limit(2)->offset(0);}])                    
                ->where([ 'tbl_contacts.subscriber_id' => $sub_id])
                //->andWhere(['tbl_contacts.contact_id'=>$con_id])
                ->orderBy('quote_id DESC')
                ->all();



        $brandProvider = new ActiveDataProvider([
            'query' => TblBrands::find(),
        ]);

        $searchModel = new TblContactsSearch();
        $contactProvider = $searchModel->search(Yii::$app->request->queryParams);

        $brandModel = new TblBrandsSearch();
        $brandProvider = $brandModel->search(Yii::$app->request->queryParams);

        $randomString = 'Test String';
        $randomKey    = 'Test Key';


        $cntModel = new TblContacts();
        $qteModel = new TblQuotes();
        $addModel = new TblAddress();


        return $this->render('indexORG', [
            'randomString' => $randomString,
            'randomKey' => $randomKey,
            'searchModel' => $searchModel,
            'dataProvider' => $contactProvider,
            'brandModel' => $brandModel,
            'brandProvider' => $brandProvider,
            'model' => $model,

            'cntModel' => $cntModel,
            'qteModel' => $qteModel,
            'addModel' => $addModel,
            
        ]);


    }

    public function actionData(){

        if(isset($_POST) && $_POST['cid'] !=''){

            $sub_id = 1;
            $cid    = $_POST['cid'];

            $model = TblContacts::find()                          
                    ->joinWith('address')
                    ->joinWith('tblQuotes')
                    ->joinWith('tblCommunications')
                    ->joinWith('tblInvoices')
                    ->joinWith('tblAppointments')
                    ->where([ 'tbl_contacts.subscriber_id' => $sub_id])
                    ->andWhere(['tbl_contacts.contact_id'=>$cid])
                    ->orderBy('tbl_quotes.quote_id DESC')
                    ->offset(0)
                    ->limit(50)  
                    ->all();

            if(count($model)>0){

                $profile      = '';
                $quote        = '';
                $quotetab     = '';
                $comm         = '';
                $invoices     = '';
                $appointments = '';
                $finalQuotes  = array();
                $carray       = array();
                $iarray       = array();
                $aarray       = array();

                $host_name = isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : $_SERVER['HTTP_HOST'];          
                $siteURL = 'http://' . $host_name . '/' . Yii::$app->request->baseUrl . '/uploads';   


                $user_name  = $model[0]->name;
                $user_email = $model[0]->email;
                $user_phone = $model[0]->phone;
                $user_addr  = $model[0]->address->street1;
                $user_image = $model[0]->image;

                if(empty($user_image)){
                  $user_image = 'noImage.png';
                }else{
                  $user_image = $model[0]->image;
                }

                // array for quotes
                if(isset($model[0]->tblQuotes) && count($model[0]->tblQuotes)>0){
                  $qarray = array_reverse($model[0]->tblQuotes);                 
                  $finalQuotes = $this->getQuotes($qarray,2);
                }

                // array for communications
                if(isset($model[0]->tblCommunications) && count($model[0]->tblCommunications)>0){
                  $carray = array_reverse($model[0]->tblCommunications);
                }

                // array for invoices
                if(isset($model[0]->tblInvoices) && count($model[0]->tblInvoices)>0){
                  $iarray = array_reverse($model[0]->tblInvoices);
                }

                // array for appointments
                if(isset($model[0]->tblAppointments) && count($model[0]->tblAppointments)>0){
                  $aarray = array_reverse($model[0]->tblAppointments);
                }



                    $profile ='<div class="personal_inner left col text-center">';
                    $profile .='<span class="personal_profile"><img height="55" width="55" src="'. $siteURL.'/'.$user_image.'"></span>';
                    $profile .='<h4 class="contact_name">'.ucfirst($user_name).'</h4>';
                    $profile .='<a href="#" class="contact_email">'.$user_email.'</a>';
                    $profile .='<a href="#" class="contact_phn">'.$user_phone.'</a>';
                    $profile .='</div>';
                    $profile .='<div class="personal_inner mid col">';
                    $profile .='<h5>Address</h5>';
                    $profile .='<p>'.$user_addr.'</p>';
                    $profile .='<div class="map_location">';
                    $profile .='<span class="location_icon"><img src="'.$siteURL.'/location.png"></span>';
                    //$profile .='<div class="location_map"><img src="'.$siteURL.'/contact_map.png"></div>';
                    $profile .='<div class="location_map"><iframe width="216" height="144" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.it/maps?q='.$user_addr.'&output=embed"></iframe></div>';
                    $profile .='</div>';
                    $profile .='</div>';
                    $profile .='<div class="personal_inner right col">';
                    $profile .='<ul class="personal_count">';
                    $profile .='<li>';
                    $profile .='<h4>10<h4>';
                    $profile .='<span><img src="'.$siteURL.'/mail.png"></span>';
                    $profile .='</li>';
                    $profile .='<li>';
                    $profile .='<h4>8<h4>';
                    $profile .='<span><img src="'.$siteURL.'/Invoices_icon.png"></span>';
                    $profile .='</li>';
                    $profile .='<li>';
                    $profile .='<h4>8<h4>';
                    $profile .='<span><img src="'.$siteURL.'/Quote_icon.png"></span>';
                    $profile .='</li>';
                    $profile .='<li>';
                    $profile .='<h4>12<h4>';
                    $profile .='<span><img src="'.$siteURL.'/calender_new.png"></span>';
                    $profile .='</li>';
                    $profile .='</ul>';
                    $profile .='</div>';


                  foreach($finalQuotes as $_finalQuotes){

                    $date = $_finalQuotes['created_at'];
                    $date = gmdate('F d Y', $date);
                    $date = explode(' ',$date);

                            //for type
                            $type = $_finalQuotes['type'];
                            switch ($type) {
                              case "0":
                                  $finaltype = "Interiors";
                                  break;
                              case "1":
                                  $finaltype =  "Exteriors";
                                  break;
                              default:
                                  $finaltype =  "Interiors";
                            }

                            //for status
                            $status = $_finalQuotes['status'];
                            switch ($status) {
                                case "1":
                                    $finalstatus = "Pending";
                                    break;
                                case "2":
                                    $finalstatus =  "In-progress";
                                    break;
                                case "3":
                                    $finalstatus =  "Completed";
                                    break;
                                default:
                                    $finalstatus =  "Pending";
                            }

                        $quote .='<ul class="quotes_list">';
                        $quote .='<li><small>'.$date[0].'</small><h4>'.$date[1].'</h4></li>';
                        $quote .='<li><span>142</span><h3 class="name">'.$user_name.'</h3></li>';
                        $quote .='<li><p>'.$_finalQuotes['description'].'</p></li>';
                        //$quote .='<li><div class="interior"><img src="'.$siteURL.'/quotes_Interior_icon.png"><span>Interiors</span></div></li>';
                        $quote .='<li><div class="interior"><img src="'.$siteURL.'/quotes_Interior_icon.png"><span>'.$finaltype.'</span></div></li>';
                        $quote .='<li><div class="interior"><img src="'.$siteURL.'/quote_price-icon.png"><span>$1645.54</span></div></li>';
                        //$quote .='<li><div class="compt_Action"><img src="'.$siteURL.'/complet.png"><span>Completed</span></div></li>';
                        $quote .='<li><div class="compt_Action"><img src="'.$siteURL.'/complet.png"><span>'.$finalstatus.'</span></div></li>';
                        $quote .='</ul>';

                  }//foreach

                  $quotetab ='<h3>Quotes</h3>';

                  foreach($finalQuotes as $_finalQuotes){

                    $date = $_finalQuotes['created_at'];
                    $date = gmdate('F d Y', $date);
                    $date = explode(' ',$date);

                            //for type
                            $type = $_finalQuotes['type'];
                            switch ($type) {
                                  case "0":
                                      $finaltype = "Interiors";
                                      break;
                                  case "1":
                                      $finaltype =  "Exteriors";
                                      break;
                                  default:
                                      $finaltype =  "Interiors";
                            }

                            $status = $_finalQuotes['status'];
                            switch ($status) {
                                case "1":
                                    $finalstatus = "Pending";
                                    break;
                                case "2":
                                    $finalstatus =  "In-progress";
                                    break;
                                case "3":
                                    $finalstatus =  "Completed";
                                    break;
                                default:
                                    $finalstatus =  "Pending";
                            }

                        $quotetab .='<ul class="quotes_list">';
                        $quotetab .='<li><small>'.$date[0].'</small><h4>'.$date[1].'</h4></li>';
                        $quotetab .='<li><span>142</span><h3 class="name">'.$user_name.'</h3></li>';
                        $quotetab .='<li><p>'.$_finalQuotes['description'].'</p></li>';
                        //$quotetab .='<li><div class="interior"><img src="'.$siteURL.'/quotes_Interior_icon.png"><span>Interiors</span></div></li>';
                        $quotetab .='<li><div class="interior"><img src="'.$siteURL.'/quotes_Interior_icon.png"><span>'.$finaltype.'</span></div></li>';
                        $quotetab .='<li><div class="interior"><img src="'.$siteURL.'/quote_price-icon.png"><span>$1645.54</span></div></li>';
                        //$quotetab .='<li><div class="compt_Action"><img src="'.$siteURL.'/complet.png"><span>Completed</span></div></li>';
                        $quotetab .='<li><div class="compt_Action"><img src="'.$siteURL.'/complet.png"><span>'.$finalstatus.'</span></div></li>';
                        $quotetab .='</ul>';

                  }//foreach


                if(count($carray)>0){

                    foreach($carray as $_modelComm){

                      $comm .='<div class="mail_box">';
                        $comm .='<div class="mail_heading">'.date('d F Y',$_modelComm->created_at).'</div>';
                          $comm .='<div class="inner_mail_box">';
                            $comm .='<h4 class="mail_sub">'.ucwords($_modelComm->commQuote->description).'</h4>';
                              $comm .='<div class="form-group">';
                              $comm .='<label>From</label>';
                              $comm .='<p>'.$_modelComm->commSubscriber->username.'</p>';
                              $comm .='</div>';/* form-group */
                              $comm .='<div class="form-group">';
                              $comm .='<label>Date</label>';
                              $comm .='<p>'.date('d F Y',$_modelComm->created_at).'</p>';
                              $comm .='</div>';/* form-group */
                              $comm .='<div class="form-group">';
                              $comm .='<label>To</label>';
                              $comm .='<p>'.$_modelComm->receiver_email.'</p>';
                              $comm .='</div>';/* form-group */
                              $comm .='<div class="form-group">';
                              $comm .='<label>To</label>';
                              $comm .='<p>'.$_modelComm->receiver_email.'</p>';
                              $comm .='</div>';/* form-group */
                              $comm .='<div class="mail-content-box">'.$_modelComm->body.'<a href="#" class="text-right">View More</a>';
                              $comm .='</div>';/* mail-content-box */
                          $comm .='</div>'; /*inner_mail_box*/
                      $comm .='</div>';
                    }//foreach

                  } //if



                    $invoices .='<table class="invoice_table table table-borderless table-condensed table-hover">';
                    $invoices .='<thead>';
                    $invoices .='<tr>';
                    $invoices .='<th>ID</th>';
                    $invoices .='<th>Quote ID</th>';
                    $invoices .='<th>Quote Description</th>';
                    $invoices .='<th>Description</th>';
                    $invoices .='<th>Amount</th>';
                    $invoices .='<th>Create/Send</th>';
                    $invoices .='<th>Paid</th>';
                    $invoices .='<th>Date</th>';
                    $invoices .='</tr>';
                    $invoices .='</thead>';
                    $invoices .='<tbody>';

                  if(count($iarray)>0){

                    foreach($iarray as $_iarray){

                      $invoices .='<tr>';
                        $invoices .='<td>'.$_iarray->invoice_id.'</td>';
                        $invoices .='<td>'.$_iarray->quote_id.'</td>';
                        $invoices .='<td>'.$_iarray->description.'</td>';
                        $invoices .='<td>Deopsit</td>';
                        $invoices .='<td>$159.32</td>';
                        $invoices .='<td>No</td>';
                        $invoices .='<td>-</td>';
                        $invoices .='<td>03-04-2018</td>';
                      $invoices .='</tr>';

                    }//foreach

                  }//if

                    $invoices .='</tbody>';
                    $invoices .='</table>'; //


                    $appointments .='<div class="container">';
                    $appointments .='<div class="row head">';
                    $appointments .='<div class="col">';
                    $appointments .='<h5>Date/Time</h5>';
                    $appointments .='</div>';
                    $appointments .='<div class="col-6">';
                    $appointments .='<h5>Location</h5>';
                    $appointments .='</div>';
                    $appointments .='<div class="col">';
                    $appointments .='<h5>User Note</h5>';
                    $appointments .='</div>';
                    $appointments .='</div>';

                    if(count($aarray)>0){

                      foreach($aarray as $_aarray){

                        $appointments .='<div class="row  body">';
                          $appointments .='<div class="col">';
                           $appointments .='<h4>'.date('d F Y', $_aarray->created_at).'</h4>';
                            $appointments .='<small>'.date('h:i a', $_aarray->created_at).'</small>';
                          $appointments .='</div>';
                          $appointments .='<div class="col-6">';
                            $appointments .='<p>'.$_aarray->formatted_addr.'</p>';
                          $appointments .='</div>';
                          $appointments .='<div class="col">';
                            $appointments .='<p>'.$_aarray->note.'</p>';
                          $appointments .='</div>';
                        $appointments .='</div>';

                      }//foreach
                    }//if

                $appointments .='</div>';


                    $html_content="<p>hello this is sample text";
                    $json_array=array(
                        'success'=>1,
                        'profiledata'     => $profile,
                        'quotedata'       => $quote,
                        'quotetabdata'    => $quotetab,
                        'commdata'        => $comm,
                        'invoicedata'     => $invoices,
                        'appointmentdata' => $appointments
                    ); 

                    ob_start();
                    echo json_encode($json_array);

            } //if

        }//if

    }// actionData


    public function actionSearch(){

        $host_name = isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : $_SERVER['HTTP_HOST'];          
        $siteURL = 'http://' . $host_name . '/' . Yii::$app->request->baseUrl . '/uploads';   

        $cntct_li = '';

        if(isset($_REQUEST)){

            //echo 'herre'.$_GET['search']; exit;

            $name = $_REQUEST['search'];
            
            $model = TblContacts::find()
                        ->where(['like', 'name', $name])
                        ->all();

            if(isset($model) && count($model)>0){

              /* create li for contact details */
              foreach($model as $_model){

                    if(empty($_model->image)){
                      $image = 'noImage.png';
                    }else{
                      $image = $_model->image;
                    }

                  $cntct_li .= '<li cid="'.$_model->contact_id.'" class="ciid">';
                    $cntct_li .= '<span class="cnt_img"><img height="55" width="55" src="'.$siteURL.'/'.$image.'"></span>';
                      $cntct_li .= '<div class="contact_person_detail">';
                        $cntct_li .= '<p>'.ucfirst($_model->name).'</p>';
                        $cntct_li .= '<small>'.$_model->phone.'</small>';
                      $cntct_li .= '</div>';
                  $cntct_li .= '</li>';
              }//foreach

            } //if

            $json_array=array(
                'success'=>1,
                'searchdata' => $cntct_li
            ); 

            ob_start();
            echo json_encode($json_array);

        }//if

    }//actionSearch


    function getQuotes( $an_array, $elements ) {
      return array_slice( $an_array, 0, $elements );
    } //getQuotes


    /**
     * Finds the TblContacts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TblContacts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TblContacts::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
