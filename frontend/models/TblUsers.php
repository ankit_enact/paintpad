<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_users".
 *
 * @property int $user_id
 * @property string $username
 * @property string $email
 * @property string $pwd
 * @property string $auth_key
 * @property int $role
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblContacts[] $tblContacts
 * @property TblUsersDetails[] $tblUsersDetails
 */
class TblUsers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'email', 'pwd', 'auth_key', 'role', 'status', 'created_at', 'updated_at'], 'required'],
            [['role', 'status', 'created_at', 'updated_at'], 'integer'],
            [['username', 'email'], 'string', 'max' => 255],
            //[['username', 'email'], 'unique'],
            [['pwd'], 'string', 'max' => 60],
            [['auth_key'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'username' => 'Username',
            'email' => 'Email',
            'pwd' => 'Pwd',
            'auth_key' => 'Auth Key',
            'role' => 'Role',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblContacts()
    {
        return $this->hasMany(TblContacts::className(), ['subscriber_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblUsersDetails()
    {
        return $this->hasMany(TblUsersDetails::className(), ['user_id' => 'user_id']);
    }
    public function getTblQuotes()
    {
        return $this->hasMany(TblQuotes::className(), ['user_id' => 'subscriber_id']);
    }    
    public function getTblCommunications()
    {
        return $this->hasMany(TblCommunications::className(), ['subscriber_id' => 'user_id']);
    }
    public function getTblMembers()
    {
        return $this->hasMany(TblMembers::className(), ['subscriber_id' => 'user_id']);
    }
    public function getTblCompany()
    {
        return $this->hasOne(TblCompany::className(), ['company_id' => 'company_id']);
    }
    public function getTblCompanyDocuments()
    {
        return $this->hasMany(TblCompanyDocuments::className(), ['user_id' => 'subscriber_id']);
    }

}
