<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_company_prep_level".
 *
 * @property int $prep_id
 * @property int $company_id
 * @property string $prep_level
 * @property double $uplift_cost
 * @property double $uplift_time
 * @property int $is_default "0 -> No, 1 -> Yes"
 * @property int $created_at
 * @property int $updated_at
 */
class TblCompanyPrepLevel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_company_prep_level';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'prep_level', 'uplift_cost', 'uplift_time'], 'required'],
            [['prep_id', 'company_id', 'is_default', 'created_at', 'updated_at'], 'integer'],
            [['uplift_cost', 'uplift_time'], 'number'],
            [['prep_level'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'prep_id' => 'Prep ID',
            'company_id' => 'Company ID',
            'prep_level' => 'Prep Level',
            'uplift_cost' => 'Uplift Cost',
            'uplift_time' => 'Uplift Time',
            'is_default' => 'Is Default',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'deletable' => 'Delatable'
        ];
    }

    public function getCompany()
    {
        return $this->hasOne(TblCompany::className(), ['company_id' => 'company_id']);
    }
}
