<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_sheen".
 *
 * @property int $sheen_id
 * @property string $name
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblSheensTopCoats[] $tblSheensTopCoats
 * @property TblTierSheens[] $tblTierSheens
 */
class TblSheen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_sheen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sheen_id' => 'Sheen ID',
            'name' => 'Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblSheensTopCoats()
    {
        return $this->hasMany(TblSheensTopCoats::className(), ['sheen_id' => 'sheen_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblTierSheens()
    {
        return $this->hasMany(TblTierSheens::className(), ['sheen_id' => 'sheen_id']);
    }
}
