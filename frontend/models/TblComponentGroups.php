<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_component_groups".
 *
 * @property int $group_id
 * @property string $name
 * @property int $type_id
 * @property int $enabled
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblProductType $type
 * @property TblComponents[] $tblComponents
 */
class TblComponentGroups extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_component_groups';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'type_id', 'created_at', 'updated_at'], 'required'],
            [['type_id', 'enabled', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblProductType::className(), 'targetAttribute' => ['type_id' => 'type_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'group_id' => 'Group ID',
            'name' => 'Name',
            'type_id' => 'Type ID',
            'enabled' => 'Enabled',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(TblProductType::className(), ['type_id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblComponents()
    {
        return $this->hasMany(TblComponents::className(), ['group_id' => 'group_id']);
    }
    public function getTblTierCoats()
    {
        return $this->hasMany(TblTierCoats::className(), ['comp_group_id' => 'group_id']);
    }
    public function getTblPaintDefaultsComp()
    {
        return $this->hasOne(TblPaintDefaults::className(), ['comp_id' => 'group_id']);
    }   
    public function getTblRoomPaintDefaultsComp()
    {
        return $this->hasOne(TblRoomPaintDefaults::className(), ['comp_id' => 'group_id']);
    }    
}
