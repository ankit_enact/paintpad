<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_ext_quote_component".
 *
 * @property int $quote_component_id
 * @property int $quote_id
 * @property int $room_id
 * @property int $comp_id
 *
 * @property TblComponents $comp
 * @property TblQuotes $quote
 */
class TblExtQuoteComponent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_ext_quote_component';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['quote_id', 'comp_id'], 'required'],
            [['quote_id', 'room_id', 'comp_id'], 'integer'],
            //[['comp_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblComponents::className(), 'targetAttribute' => ['comp_id' => 'comp_id']],
            [['quote_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblQuotes::className(), 'targetAttribute' => ['quote_id' => 'quote_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'quote_component_id' => 'Quote Component ID',
            'quote_id' => 'Quote ID',
            'room_id' => 'Room ID',
            'comp_id' => 'Comp ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComp()
    {
        return $this->hasOne(TblComponentGroups::className(), ['group_id' => 'comp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuote()
    {
        return $this->hasOne(TblQuotes::className(), ['quote_id' => 'quote_id']);
    }
}
