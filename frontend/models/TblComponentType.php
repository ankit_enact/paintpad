<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_component_type".
 *
 * @property int $comp_type_id
 * @property string $name
 * @property int $work_rate
 * @property int $spread_ratio
 * @property int $enabled
 * @property int $comp_id
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblComponents $comp
 */
class TblComponentType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_component_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'work_rate', 'spread_ratio', 'comp_id', 'created_at', 'updated_at'], 'required'],
            [['work_rate', 'spread_ratio', 'enabled', 'comp_id', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['comp_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblComponents::className(), 'targetAttribute' => ['comp_id' => 'comp_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'comp_type_id' => 'Comp Type ID',
            'name' => 'Name',
            'work_rate' => 'Work Rate',
            'spread_ratio' => 'Spread Ratio',
            'enabled' => 'Enabled',
            'comp_id' => 'Comp ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComp()
    {
        return $this->hasOne(TblComponents::className(), ['comp_id' => 'comp_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblAreaCompPref()
    {
        return $this->hasMany(TblAreaCompPref::className(), ['option_id' => 'comp_type_id']);
    }
}
