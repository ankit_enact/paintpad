<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_quote_email_settings".
 *
 * @property int $quote_email_settings_id
 * @property int $company_id
 * @property string $quote_email_subject
 * @property string $created_at
 */
class TblQuoteEmailSettings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_quote_email_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'quote_email_subject', 'created_at'], 'required'],
            [['company_id'], 'integer'],
            [['quote_email_subject'], 'string'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'quote_email_settings_id' => 'Quote Email Settings ID',
            'company_id' => 'Company ID',
            'quote_email_subject' => 'Quote Email Subject',
            'created_at' => 'Created At',
        ];
    }
}
