<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_room_special_items".
 *
 * @property int $id
 * @property string $name
 * @property int $room_id
 * @property string $item_name
 * @property double $price
 * @property double $labour_price
 * @property double $labour_hour
 * @property int $qty
 * @property int $include
 * @property int $is_price_include
 * @property string $device_id
 * @property string $unique_id
 * @property int $flag
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblQuotes $quote
 * @property TblRooms $room
 */
class TblRoomSpecialItems extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_room_special_items';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'item_name', 'device_id', 'unique_id'], 'string'],
            [['room_id', 'include', 'flag', 'created_at', 'updated_at'], 'required'],
            [['room_id', 'include', 'is_price_include', 'flag', 'created_at', 'updated_at'], 'integer'],
            [['price','qty','labour_price','labour_hour'], 'number'],
            [['room_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblRooms::className(), 'targetAttribute' => ['room_id' => 'rooms_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'room_id' => 'Room ID',
            'item_name' => 'Item Name',
            'price' => 'Price',
            'qty' => 'Qty',
            'include' => 'Include',
            'is_price_include' => 'Is Price Include',
            'labour_price' => 'Labour Price',
            'labour_hour' => 'Labour Hour',
            'device_id' => 'Device ID',
            'unique_id' => 'Unique ID',
            'flag' => 'Flag',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoom()
    {
        return $this->hasOne(TblRooms::className(), ['rooms_id' => 'room_id']);
    }
}
