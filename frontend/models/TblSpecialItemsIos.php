<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_special_items_ios".
 *
 * @property int $id
 * @property string $name
 * @property int $quote_id
 * @property string $room_id
 * @property string $item_name
 * @property string $price
 * @property string $qty
 * @property string $include
 * @property string $device_id
 * @property string $unique_id
 * @property int $flag
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblQuotes $quote
 */
class TblSpecialItemsIos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_special_items_ios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'quote_id', 'room_id', 'item_name', 'price', 'qty', 'include', 'device_id', 'unique_id', 'flag', 'created_at', 'updated_at'], 'required'],
            [['name', 'room_id', 'item_name', 'price', 'qty', 'include', 'device_id', 'unique_id'], 'string'],
            [['quote_id', 'flag', 'created_at', 'updated_at'], 'integer'],
            [['quote_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblQuotes::className(), 'targetAttribute' => ['quote_id' => 'quote_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'quote_id' => 'Quote ID',
            'room_id' => 'Room ID',
            'item_name' => 'Item Name',
            'price' => 'Price',
            'qty' => 'Qty',
            'include' => 'Include',
            'device_id' => 'Device ID',
            'unique_id' => 'Unique ID',
            'flag' => 'Flag',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuote()
    {
        return $this->hasOne(TblQuotes::className(), ['quote_id' => 'quote_id']);
    }
}
