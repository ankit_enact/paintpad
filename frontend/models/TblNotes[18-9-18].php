<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_notes".
 *
 * @property int $note_id
 * @property int $quote_id
 * @property string $description
 * @property string $image
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblQuotes $quote
 */
class TblNotes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_notes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['quote_id', 'description', 'image', 'created_at', 'updated_at'], 'required'],
            [['quote_id', 'created_at', 'updated_at'], 'integer'],
            [['description'], 'string'],
            [['image'], 'string', 'max' => 255],
            [['quote_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblQuotes::className(), 'targetAttribute' => ['quote_id' => 'quote_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'note_id' => 'Note ID',
            'quote_id' => 'Quote ID',
            'description' => 'Description',
            'image' => 'Image',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuote()
    {
        return $this->hasOne(TblQuotes::className(), ['quote_id' => 'quote_id']);
    }
}
