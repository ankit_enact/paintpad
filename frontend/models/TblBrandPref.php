<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_brand_pref".
 *
 * @property int $id
 * @property string $name
 * @property int $quote_id
 * @property string $brand_id
 * @property string $tier_id
 * @property string $coats
 * @property string $prep_level
 * @property string $apply_undercoat
 * @property string $color_consultant
 * @property string $device_id
 * @property string $unique_id
 * @property int $flag
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblQuotes $quote
 */
class TblBrandPref extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_brand_pref';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['quote_id', 'brand_id', 'tier_id', 'coats', 'prep_level', 'apply_undercoat', 'color_consultant', 'flag', 'created_at', 'updated_at'], 'required'],
            [['name', 'device_id', 'unique_id'], 'string'],
            [['quote_id', 'flag', 'created_at', 'updated_at'], 'integer'],
            [['quote_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblQuotes::className(), 'targetAttribute' => ['quote_id' => 'quote_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'quote_id' => 'Quote ID',
            'brand_id' => 'Brand ID',
            'tier_id' => 'Tier ID',
            'coats' => 'Coats',
            'prep_level' => 'Prep Level',
            'apply_undercoat' => 'Apply Undercoat',
            'color_consultant' => 'Color Consultant',
            'device_id' => 'Device ID',
            'unique_id' => 'Unique ID',
            'flag' => 'Flag',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuote()
    {
        return $this->hasOne(TblQuotes::className(), ['quote_id' => 'quote_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(TblBrands::className(), ['brand_id' => 'brand_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTiers()
    {
        return $this->hasOne(TblTiers::className(), ['tier_id' => 'tier_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPreplevel()
    {
        return $this->hasOne(TblPrepLevel::className(), ['prep_id' => 'prep_level']);
    }
}
