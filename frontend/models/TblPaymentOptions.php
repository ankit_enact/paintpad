<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_payment_options".
 *
 * @property int $id
 * @property string $name
 * @property int $enabled
 * @property int $created_at
 * @property int $updated_at
 */
class TblPaymentOptions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_payment_options';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['enabled', 'created_at', 'updated_at'], 'required'],
            [['enabled', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'enabled' => 'Enabled',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
