<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_component_methods".
 *
 * @property int $method_id
 * @property string $name
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblComponents[] $tblComponents
 */
class TblComponentMethods extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_component_methods';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'method_id' => 'Method ID',
            'name' => 'Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblComponents()
    {
        return $this->hasMany(TblComponents::className(), ['method_id' => 'method_id']);
    }
}
