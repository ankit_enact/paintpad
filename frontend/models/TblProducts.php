<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_products".
 *
 * @property int $product_id
 * @property string $name
 * @property double $spread_rate
 * @property string $image
 * @property int $brand_id
 * @property int $type_id
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblProductType $type
 * @property TblBrands $brand
 * @property TblSheensTopCoats[] $tblSheensTopCoats
 * @property TblTierUnderCoats[] $tblTierUnderCoats
 */
class TblProducts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_products';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'spread_rate', 'image', 'brand_id', 'type_id', 'created_at', 'updated_at'], 'required'],
            [['spread_rate'], 'number'],
            [['brand_id', 'type_id', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['image'], 'string', 'max' => 255],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblProductType::className(), 'targetAttribute' => ['type_id' => 'type_id']],
            [['brand_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblBrands::className(), 'targetAttribute' => ['brand_id' => 'brand_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'name' => 'Name',
            'spread_rate' => 'Spread Rate',
            'image' => 'Image',
            'brand_id' => 'Brand ID',
            'type_id' => 'Type ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(TblProductType::className(), ['type_id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(TblBrands::className(), ['brand_id' => 'brand_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblSheensTopCoats()
    {
        return $this->hasMany(TblSheensTopCoats::className(), ['product_id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblTierUnderCoats()
    {
        return $this->hasMany(TblTierUnderCoats::className(), ['product_id' => 'product_id']);
    }
}
