<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_company_stock".
 *
 * @property int $company_stock_id
 * @property int $company_id
 * @property int $stock_id
 * @property double $price
 */
class TblCompanyStock extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_company_stock';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['stock_id', 'company_id', 'price'], 'required'],
            [['stock_id', 'company_id'], 'integer'],
            [['price'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'company_stock_id' => 'Company Stock ID',
            'stock_id' => 'Stock ID',
            'company_id' => 'Company ID',
            'price' => 'Price',
        ];
    }
}
