<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_paint_order".
 *
 * @property int $distinct_paint_order_id
 * @property string $paint_order_id
 * @property int $quote_id
 * @property int $product_id
 * @property int $stock_id
 * @property double $size
 * @property string $deliver_detail
 * @property int $quantity
 * @property string $brand
 * @property string $product
 * @property string $sheen
 * @property string $colour
 * @property string $notes
 * @property int $created_at
 *
 * @property TblQuotes $quote
 */
class TblPaintOrder extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_paint_order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['paint_order_id', 'quote_id', 'product_id', 'created_at'], 'required'],
            [['quote_id', 'product_id', 'stock_id', 'quantity', 'created_at'], 'integer'],
            [['size'], 'number'],
            [['deliver_detail', 'brand', 'product', 'sheen', 'colour', 'notes'], 'string'],
            [['paint_order_id'], 'string', 'max' => 30],
            [['quote_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblQuotes::className(), 'targetAttribute' => ['quote_id' => 'quote_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'distinct_paint_order_id' => 'Distinct Paint Order ID',
            'paint_order_id' => 'Paint Order ID',
            'quote_id' => 'Quote ID',
            'product_id' => 'Product ID',
            'stock_id' => 'Stock ID',
            'size' => 'Size',
            'deliver_detail' => 'Deliver Detail',
            'quantity' => 'Quantity',
            'brand' => 'Brand',
            'product' => 'Product',
            'sheen' => 'Sheen',
            'colour' => 'Colour',
            'notes' => 'Notes',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuote()
    {
        return $this->hasOne(TblQuotes::className(), ['quote_id' => 'quote_id']);
    }
}
