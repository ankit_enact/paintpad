<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_room_brand_pref".
 *
 * @property int $id
 * @property string $name
 * @property int $quote_id
 * @property int $room_id
 * @property int $brand_id
 * @property int $tier_id
 * @property string $coats
 * @property string $prep_level
 * @property string $apply_undercoat
 * @property string $color_consultant
 * @property string $lock_room
 * @property string $device_id
 * @property string $unique_id
 * @property int $flag
 * @property int $created_at
 * @property int $updated_at
 */
class TblRoomBrandPref extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_room_brand_pref';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['quote_id', 'brand_id', 'tier_id', 'coats', 'prep_level', 'apply_undercoat', 'color_consultant', 'flag', 'created_at', 'updated_at'], 'required'],
            [['name', 'device_id', 'unique_id'], 'string'],
            [['quote_id', 'flag', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'quote_id' => 'Quote ID',
            'room_id' => 'Room ID',
            'brand_id' => 'Brand ID',
            'tier_id' => 'Tier ID',
            'coats' => 'Coats',
            'prep_level' => 'Prep Level',
            'apply_undercoat' => 'Apply Undercoat',
            'color_consultant' => 'Color Consultant',
            'lock_room' => 'Lock Room',
            'device_id' => 'Device ID',
            'unique_id' => 'Unique ID',
            'flag' => 'Flag',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
