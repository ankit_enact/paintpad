<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_states".
 *
 * @property int $state_id
 * @property string $name
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblAddress[] $tblAddresses
 */
class TblStates extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_states';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'state_id' => 'State ID',
            'name' => 'Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblAddresses()
    {
        return $this->hasMany(TblAddress::className(), ['state_id' => 'state_id']);
    }
}
