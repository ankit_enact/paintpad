<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_states".
 *
 * @property int $state_id
 * @property string $name
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblAddress[] $tblAddresses
 */
class TblCustomerSources extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_customer_sources';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['source'], 'required'],
            [['source'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cs_id' => 'Customer Source Id',
            'source' => 'Source',
            'cs_status' => 'Custmer Support Status',
        ];
    }

}
