<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_users_settings".
 *
 * @property int $users_settings_id
 * @property int $company_id
 * @property double $application_cost
 * @property double $profit_markup
 * @property double $gross_margin_percent
 * @property double $default_deposit_percent
 * @property int $terms_condition
 * @property int $created_at
 * @property int $updated_at
 */
class TblUsersSettings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_users_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'application_cost', 'profit_markup', 'gross_margin_percent', 'default_deposit_percent', 'created_at', 'updated_at'], 'required'],
            [['company_id', 'created_at', 'updated_at'], 'integer'],
            [['application_cost', 'profit_markup', 'gross_margin_percent', 'default_deposit_percent'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'users_settings_id' => 'Users Settings ID',
            'company_id' => 'Company ID',
            'application_cost' => 'Application Cost',
            'profit_markup' => 'Profit Markup',
            'gross_margin_percent' => 'Gross Margin Percent',
            'default_deposit_percent' => 'Default Deposit Percent',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getTblCompanyDocuments()
    {
        return $this->hasOne(TblCompanyDocuments::className(), ['doc_id' => 'terms_condition']);
    }
}
