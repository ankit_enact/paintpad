<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_emails".
 *
 * @property int $id
 * @property string $receiver_name
 * @property string $receiver_email
 * @property string $subject
 * @property string $content
 * @property string $attachment
 * @property int $created_at
 * @property int $updated_at
 */
class TblEmails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_emails';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //[['receiver_name', 'receiver_email', 'subject', 'content', 'attachment', 'created_at', 'updated_at'], 'required'],
            [['receiver_email', 'subject', 'content', 'attachment'], 'required'],
            [['content'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['receiver_name'], 'string', 'max' => 50],
            //[['receiver_email'], 'string', 'max' => 200],
            [['subject', 'attachment'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'receiver_name' => 'Receiver Name',
            'receiver_email' => 'Receiver Email',
            'subject' => 'Subject',
            'content' => 'Content',
            'attachment' => 'Attachment',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
