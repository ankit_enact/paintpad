<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TblCommunications;

/**
 * TblCommunicationsSearch represents the model behind the search form of `app\models\TblCommunications`.
 */
class TblCommunicationsSearch extends TblCommunications
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['comm_id', 'contact_id', 'subscriber_id', 'quote_id', 'newsletter_id', 'date', 'created_at', 'updated_at'], 'integer'],
            [['receiver_email', 'comm_from', 'comm_to', 'bcc', 'subject', 'body', 'attachment'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TblCommunications::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'comm_id' => $this->comm_id,
            'contact_id' => $this->contact_id,
            'subscriber_id' => $this->subscriber_id,
            'quote_id' => $this->quote_id,
            'newsletter_id' => $this->newsletter_id,
            'date' => $this->date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'receiver_email', $this->receiver_email])
            ->andFilterWhere(['like', 'comm_from', $this->comm_from])
            ->andFilterWhere(['like', 'comm_to', $this->comm_to])
            ->andFilterWhere(['like', 'bcc', $this->bcc])
            ->andFilterWhere(['like', 'subject', $this->subject])
            ->andFilterWhere(['like', 'body', $this->body])
            ->andFilterWhere(['like', 'attachment', $this->attachment]);
            

        return $dataProvider;
    }
}
