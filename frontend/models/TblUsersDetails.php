<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_users_details".
 *
 * @property int $user_id
 * @property string $name
 * @property string $phone
 * @property int $address_id
 * @property string $logo
 * @property string $website_link
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblAddress $address
 * @property TblUsers $user
 */
class TblUsersDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_users_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'name', 'phone', 'address_id', 'logo', 'website_link', 'created_at', 'updated_at'], 'required'],
            [['user_id', 'address_id', 'created_at', 'updated_at'], 'integer'],
            [['name', 'phone'], 'string', 'max' => 50],
            [['logo', 'website_link'], 'string', 'max' => 100],
            [['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblAddress::className(), 'targetAttribute' => ['address_id' => 'address_id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblUsers::className(), 'targetAttribute' => ['user_id' => 'user_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'name' => 'Name',
            'phone' => 'Phone',
            'address_id' => 'Address ID',
            'logo' => 'Logo',
            'website_link' => 'Website Link',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(TblAddress::className(), ['address_id' => 'address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(TblUsers::className(), ['user_id' => 'user_id']);
    }
}
