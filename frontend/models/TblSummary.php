<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_summary".
 *
 * @property int $summary_id
 * @property int $quote_id
 * @property int $painter
 * @property int $days
 * @property int $est_comm
 * @property int $est_comp
 * @property string $authorised_person
 * @property string $sign
 * @property double $amount
 * @property double $dateBalance
 * @property int $created_at
 * @property int $updated_at
 */
class TblSummary extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_summary';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['quote_id', 'created_at', 'updated_at'], 'required'],
            [['quote_id', 'painter', 'days', 'est_comm', 'est_comp', 'created_at', 'updated_at'], 'integer'],
            [['authorised_person', 'sign','paymentNotes'], 'string'],
            [['amount', 'dateBalance'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'summary_id' => 'Summary ID',
            'quote_id' => 'Quote ID',
            'painter' => 'Painter',
            'days' => 'Days',
            'est_comm' => 'Est Comm',
            'est_comp' => 'Est Comp',
            'authorised_person' => 'Authorised Person',
            'sign' => 'Sign',
            'amount' => 'Amount',
            'dateBalance' => 'Date Balance',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getTblPaymentOptions()
    {
        return $this->hasOne(TblPaymentOptions::className(), ['id' => 'payment_option']);
    }
}
