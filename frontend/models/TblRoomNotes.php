<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_room_notes".
 *
 * @property int $id
 * @property string $name
 * @property int $room_id
 * @property string $image
 * @property string $description
 * @property string $device_id
 * @property string $unique_id
 * @property int $flag
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblRooms $room
 */
class TblRoomNotes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_room_notes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'image', 'description', 'device_id', 'unique_id'], 'string'],
            [['room_id', 'flag', 'created_at', 'updated_at'], 'required'],
            [['room_id', 'flag', 'created_at', 'updated_at'], 'integer'],
            [['room_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblRooms::className(), 'targetAttribute' => ['room_id' => 'rooms_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'room_id' => 'Room ID',
            'image' => 'Image',
            'description' => 'Description',
            'device_id' => 'Device ID',
            'unique_id' => 'Unique ID',
            'flag' => 'Flag',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoom()
    {
        return $this->hasOne(TblRooms::className(), ['rooms_id' => 'room_id']);
    }
}
