<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_company_email_settings".
 *
 * @property int $quote_email_settings_id
 * @property int $company_id
 * @property int $type_id 1: Quote, 2: JSS
 * @property string $quote_email_subject
 * @property string $created_at
 *
 * @property TblEmailSettingsDoc[] $tblEmailSettingsDocs
 */
class TblCompanyEmailSettings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_company_email_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'type_id', 'created_at'], 'required'],
            [['company_id', 'type_id'], 'integer'],
            [['quote_email_subject'], 'string'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'quote_email_settings_id' => 'Quote Email Settings ID',
            'company_id' => 'Company ID',
            'type_id' => 'Type ID',
            'quote_email_subject' => 'Quote Email Subject',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblEmailSettingsDocs()
    {
        return $this->hasMany(TblEmailSettingsDoc::className(), ['email_setting_id' => 'quote_email_settings_id']);
    }
}
