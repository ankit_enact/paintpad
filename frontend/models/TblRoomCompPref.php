<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_room_comp_pref".
 *
 * @property int $id
 * @property string $name
 * @property int $room_id
 * @property int $tbl_room_pref_id
 * @property int $option_id
 * @property string $option_value
 * @property string $device_id
 * @property string $unique_id
 * @property int $flag
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblRoomPref $tblRoomPref
 * @property TblRooms $room
 */
class TblRoomCompPref extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_room_comp_pref';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tbl_room_pref_id', 'option_id','created_at', 'updated_at'], 'required'],
            [['tbl_room_pref_id', 'option_id', 'created_at', 'updated_at'], 'integer'],
            [['tbl_room_pref_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblRoomPref::className(), 'targetAttribute' => ['tbl_room_pref_id' => 'id']],
            [['room_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblRooms::className(), 'targetAttribute' => ['room_id' => 'rooms_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'room_id' => 'Room ID',
            'tbl_room_pref_id' => 'Tbl Room Pref ID',
            'option_id' => 'Option ID',
            'option_value' => 'Option Value',
            'device_id' => 'Device ID',
            'unique_id' => 'Unique ID',
            'flag' => 'Flag',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblRoomPref()
    {
        return $this->hasOne(TblRoomPref::className(), ['id' => 'tbl_room_pref_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoom()
    {
        return $this->hasOne(TblRooms::className(), ['rooms_id' => 'room_id']);
    }
}
