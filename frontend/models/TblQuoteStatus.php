<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_quote_status".
 *
 * @property int $status_id
 * @property string $status_name
 * @property int $enabled
 * @property string $icons
 */
class TblQuoteStatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_quote_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status_name', 'enabled', 'icons'], 'required'],
            [['enabled'], 'integer'],
            [['status_name'], 'string', 'max' => 100],
            [['icons'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'status_id' => 'Status ID',
            'status_name' => 'Status Name',
            'enabled' => 'Enabled',
            'icons' => 'Icons',
        ];
    }
}
