<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_paint_defaults".
 *
 * @property int $id
 * @property string $name
 * @property int $quote_id
 * @property string $tier_id
 * @property string $comp_id
 * @property string $sheen_id
 * @property string $topcoat
 * @property string $strength
 * @property string $color_id
 * @property string $hasUnderCoat
 * @property string $undercoat
 * @property string $custom_name
 * @property string $device_id
 * @property string $unique_id
 * @property int $flag
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblQuotes $quote
 */
class TblPaintDefaults extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_paint_defaults';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['quote_id', 'tier_id', 'comp_id'], 'required'],
            [['quote_id', 'flag'], 'integer'],
            [['quote_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblQuotes::className(), 'targetAttribute' => ['quote_id' => 'quote_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'quote_id' => 'Quote ID',
            'tier_id' => 'Tier ID',
            'comp_id' => 'Comp ID',
            'sheen_id' => 'Sheen ID',
            'topcoat' => 'Topcoat',
            'strength' => 'Strength',
            'color_id' => 'Color ID',
            'hasUnderCoat' => 'Has Under Coat',
            'undercoat' => 'Undercoat',
            'custom_name' => 'Custom Name',
            'device_id' => 'Device ID',
            'unique_id' => 'Unique ID',
            'flag' => 'Flag',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuote()
    {
        return $this->hasOne(TblQuotes::className(), ['quote_id' => 'quote_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getColor()
    {
        return $this->hasOne(TblColors::className(), ['color_id' => 'color_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(TblComponentGroups::className(), ['group_id' => 'comp_id']);
    }
}
