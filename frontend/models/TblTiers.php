<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_tiers".
 *
 * @property int $tier_id
 * @property string $name
 * @property int $brand_id
 * @property int $type_id
 * @property int $enabled 0=NO, 1=YES
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblProductType $type
 * @property TblBrands $brand
 */
class TblTiers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_tiers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'brand_id', 'type_id', 'created_at', 'updated_at'], 'required'],
            [['brand_id', 'type_id', 'enabled', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblProductType::className(), 'targetAttribute' => ['type_id' => 'type_id']],
            [['brand_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblBrands::className(), 'targetAttribute' => ['brand_id' => 'brand_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'tier_id' => 'Tier ID',
            'name' => 'Name',
            'brand_id' => 'Brand ID',
            'type_id' => 'Type ID',
            'enabled' => 'Enabled',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(TblProductType::className(), ['type_id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(TblBrands::className(), ['brand_id' => 'brand_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrandpref()
    {
        return $this->hasOne(TblBrandPref::className(), ['tier_id' => 'tier_id']);
    }
}
