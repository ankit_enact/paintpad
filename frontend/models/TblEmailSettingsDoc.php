<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_email_settings_doc".
 *
 * @property int $email_doc_id
 * @property int $email_setting_id
 * @property int $doc_id
 * @property string $created_at
 *
 * @property TblCompanyEmailSettings $emailSetting
 */
class TblEmailSettingsDoc extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_email_settings_doc';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email_setting_id', 'doc_id', 'created_at'], 'required'],
            [['email_setting_id', 'doc_id'], 'integer'],
            [['created_at'], 'safe'],
            [['email_setting_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblCompanyEmailSettings::className(), 'targetAttribute' => ['email_setting_id' => 'quote_email_settings_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'email_doc_id' => 'Email Doc ID',
            'email_setting_id' => 'Email Setting ID',
            'doc_id' => 'Doc ID',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmailSetting()
    {
        return $this->hasOne(TblCompanyEmailSettings::className(), ['quote_email_settings_id' => 'email_setting_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyDocuments()
    {
        return $this->hasOne(TblCompanyDocuments::className(), ['doc_id' => 'doc_id']);
    }
}
