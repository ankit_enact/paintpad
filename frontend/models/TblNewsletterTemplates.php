<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_newsletter_templates".
 *
 * @property int $temp_id
 * @property string $temp_name
 * @property string $temp_body
 * @property int $created_at
 * @property int $updated_at
 */
class TblNewsletterTemplates extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_newsletter_templates';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['temp_name', 'temp_body', 'created_at', 'updated_at'], 'required'],
            [['temp_body'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['temp_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'temp_id' => 'Temp ID',
            'temp_name' => 'Temp Name',
            'temp_body' => 'Temp Body',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
