<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_app_settings".
 *
 * @property int $app_settings_id
 * @property string $app_version
 * @property int $is_major_update
 * @property int $is_logout_needed
 */
class TblAppSettings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_app_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['app_version', 'is_major_update', 'is_logout_needed'], 'required'],
            [['is_major_update', 'is_logout_needed'], 'integer'],
            [['app_version'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'app_settings_id' => 'App Settings ID',
            'app_version' => 'App Version',
            'is_major_update' => 'Is Major Update',
            'is_logout_needed' => 'Is Logout Needed',
        ];
    }
}
