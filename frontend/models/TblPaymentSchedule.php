<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_payment_schedule".
 *
 * @property int $id
 * @property string $name
 * @property int $quote_id
 * @property string $descp
 * @property string $amount
 * @property string $part_payment
 * @property string $payment_date
 * @property string $device_id
 * @property string $unique_id
 * @property int $flag
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblQuotes $quote
 */
class TblPaymentSchedule extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_payment_schedule';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['quote_id', 'created_at', 'updated_at'], 'required'],
            [['descp', 'device_id', 'unique_id','qb_invoice_id','qb_invoice_status'], 'string'],
            [['quote_id', 'flag', 'created_at', 'updated_at'], 'integer'],
            [['quote_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblQuotes::className(), 'targetAttribute' => ['quote_id' => 'quote_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'quote_id' => 'Quote ID',
            'descp' => 'Descp',
            'amount' => 'Amount',
            'part_payment' => 'Part Payment',
            'payment_date' => 'Payment Date',
            'device_id' => 'Device ID',
            'unique_id' => 'Unique ID',
            'flag' => 'Flag',
            'qb_invoice_id' => 'QB Invoice Id',
            'qb_invoice_status' => 'QB Invoice Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuote()
    {
        return $this->hasOne(TblQuotes::className(), ['quote_id' => 'quote_id']);
    }
}
