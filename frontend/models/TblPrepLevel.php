<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_prep_level".
 *
 * @property int $prep_id
 * @property string $prep_level
 * @property double $uplift_cost
 * @property double $uplift_time
 * @property int $is_default "0 -> No, 1 -> Yes"
 * @property int $created_at
 * @property int $updated_at
 */
class TblPrepLevel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_prep_level';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['prep_level', 'uplift_cost', 'uplift_time', 'created_at', 'updated_at'], 'required'],
            [['uplift_cost', 'uplift_time'], 'number'],
            [['is_default', 'created_at', 'updated_at'], 'integer'],
            [['prep_level'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'prep_id' => 'Prep ID',
            'prep_level' => 'Prep Level',
            'uplift_cost' => 'Uplift Cost',
            'uplift_time' => 'Uplift Time',
            'is_default' => 'Is Default',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    
    public function getCompany()
    {
        return $this->hasOne(TblCompany::className(), ['company_id' => 'company_id']);
    }
}
