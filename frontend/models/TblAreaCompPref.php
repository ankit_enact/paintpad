<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_area_comp_pref".
 *
 * @property int $id
 * @property string $name
 * @property int $room_id
 * @property int $tbl_room_pref_id
 * @property double $length
 * @property double $width
 * @property double $area
 * @property int $comp_id
 * @property int $option_id
 * @property string $option_value
 * @property string $device_id
 * @property string $unique_id
 * @property int $flag
 * @property int $created_at
 * @property int $updated_at
 */
class TblAreaCompPref extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_area_comp_pref';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'device_id', 'unique_id'], 'string'],
            [['room_id', 'tbl_room_pref_id', 'comp_id', 'option_id', 'created_at', 'updated_at'], 'integer'],
            [['tbl_room_pref_id', 'created_at', 'updated_at'], 'required'],
            [['length', 'width', 'area'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'room_id' => 'Room ID',
            'tbl_room_pref_id' => 'Tbl Room Pref ID',
            'length' => 'Length',
            'width' => 'Width',
            'area' => 'Area',
            'comp_id' => 'Comp ID',
            'option_id' => 'Option ID',
            'option_value' => 'Option Value',
            'device_id' => 'Device ID',
            'unique_id' => 'Unique ID',
            'flag' => 'Flag',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblRoomPref()
    {
        return $this->hasOne(TblRoomPref::className(), ['id' => 'tbl_room_pref_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoom()
    {
        return $this->hasOne(TblRooms::className(), ['rooms_id' => 'room_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblComponentType()
    {
        return $this->hasOne(TblComponentType::className(), ['comp_type_id' => 'option_id']);
    }
}
