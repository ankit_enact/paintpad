<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_room_pref".
 *
 * @property int $id
 * @property string $name
 * @property int $quote_id
 * @property int $room_id
 * @property int $comp_id
 * @property int $color_id
 * @property int $isSelected
 * @property int $isUndercoat
 * @property string $device_id
 * @property string $unique_id
 * @property int $flag
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblRoomCompPref[] $tblRoomCompPrefs
 * @property TblQuotes $quote
 * @property TblRooms $room
 * @property TblColors $color
 */
class TblRoomPref extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_room_pref';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string'],
            [['quote_id', 'room_id', 'comp_id', 'isSelected', 'isUndercoat', 'flag', 'created_at', 'updated_at'], 'required'],
            [['quote_id', 'room_id', 'comp_id', 'isSelected', 'isUndercoat', 'flag', 'created_at', 'updated_at'], 'integer'],
            [['quote_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblQuotes::className(), 'targetAttribute' => ['quote_id' => 'quote_id']],
            [['room_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblRooms::className(), 'targetAttribute' => ['room_id' => 'rooms_id']],
            [['color_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblColors::className(), 'targetAttribute' => ['color_id' => 'color_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'quote_id' => 'Quote ID',
            'room_id' => 'Room ID',
            'comp_id' => 'Comp ID',
            'color_id' => 'Color ID',
            'custom_color_name' => 'Custom Color Name',
            'isSelected' => 'Is Selected',
            'isUndercoat' => 'Is Undercoat',
            'device_id' => 'Device ID',
            'unique_id' => 'Unique ID',
            'flag' => 'Flag',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblRoomCompPrefs()
    {
        return $this->hasMany(TblRoomCompPref::className(), ['tbl_room_pref_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblAreaCompPref()
    {
        return $this->hasMany(TblAreaCompPref::className(), ['tbl_room_pref_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuote()
    {
        return $this->hasOne(TblQuotes::className(), ['quote_id' => 'quote_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoom()
    {
        return $this->hasOne(TblRooms::className(), ['rooms_id' => 'room_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getColor()
    {
        return $this->hasOne(TblColors::className(), ['color_id' => 'color_id']);
    }
}
