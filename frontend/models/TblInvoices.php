<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_invoices".
 *
 * @property int $invoice_id
 * @property int $contact_id
 * @property int $subscriber_id
 * @property int $quote_id
 * @property string $quote_note
 * @property string $description
 * @property string $sent
 * @property string $paid
 * @property int $date
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblQuotes $quote
 * @property TblContacts $contact
 * @property TblUsers $subscriber
 */
class TblInvoices extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_invoices';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['contact_id', 'subscriber_id', 'quote_id', 'quote_note', 'description', 'sent', 'paid', 'date', 'created_at', 'updated_at'], 'required'],
            [['contact_id', 'subscriber_id', 'quote_id', 'date', 'created_at', 'updated_at'], 'integer'],
            [['quote_note', 'description'], 'string'],
            [['sent', 'paid'], 'string', 'max' => 20],
            [['quote_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblQuotes::className(), 'targetAttribute' => ['quote_id' => 'quote_id']],
            [['contact_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblContacts::className(), 'targetAttribute' => ['contact_id' => 'contact_id']],
            [['subscriber_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblUsers::className(), 'targetAttribute' => ['subscriber_id' => 'user_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'invoice_id' => 'Invoice ID',
            'contact_id' => 'Contact ID',
            'subscriber_id' => 'Subscriber ID',
            'quote_id' => 'Quote ID',
            'quote_note' => 'Quote Note',
            'description' => 'Description',
            'sent' => 'Sent',
            'paid' => 'Paid',
            'date' => 'Date',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuote()
    {
        return $this->hasOne(TblQuotes::className(), ['quote_id' => 'quote_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(TblContacts::className(), ['contact_id' => 'contact_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriber()
    {
        return $this->hasOne(TblUsers::className(), ['user_id' => 'subscriber_id']);
    }
}
