<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_user_roles".
 *
 * @property int $role_id
 * @property string $role_name
 * @property int $status
 */
class TblUserRoles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_user_roles';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['role_name', 'status'], 'required'],
            [['role_name'], 'string'],
            [['status'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'role_id' => 'Role ID',
            'role_name' => 'Role Name',
            'status' => 'Status',
        ];
    }
}
