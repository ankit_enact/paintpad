<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_quote_special_items".
 *
 * @property int $id
 * @property string $name
 * @property int $quote_id
 * @property string $item_name
 * @property double $price
 * @property double $labour_price
 * @property double $labour_hour
 * @property int $qty
 * @property int $include
 * @property int $is_price_include
 * @property string $device_id
 * @property string $unique_id
 * @property int $flag
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblQuotes $quote
 */
class TblQuoteSpecialItems extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_quote_special_items';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'item_name', 'device_id', 'unique_id'], 'string'],
            [['quote_id', 'include', 'created_at', 'updated_at'], 'required'],
            [['quote_id', 'include', 'is_price_include', 'flag', 'created_at', 'updated_at'], 'integer'],
            [['price', 'qty', 'labour_price', 'labour_hour'], 'number'],
            [['quote_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblQuotes::className(), 'targetAttribute' => ['quote_id' => 'quote_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'quote_id' => 'Quote ID',
            'item_name' => 'Item Name',
            'price' => 'Price',
            'qty' => 'Qty',
            'include' => 'Include',
            'is_price_include' => 'Is Price Include',
            'device_id' => 'Device ID',
            'unique_id' => 'Unique ID',
            'flag' => 'Flag',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuote()
    {
        return $this->hasOne(TblQuotes::className(), ['quote_id' => 'quote_id']);
    }
}
