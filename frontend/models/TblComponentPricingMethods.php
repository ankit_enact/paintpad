<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_component_pricing_methods".
 *
 * @property int $price_method_id
 * @property string $name
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblComponents[] $tblComponents
 */
class TblComponentPricingMethods extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_component_pricing_methods';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'price_method_id' => 'Price Method ID',
            'name' => 'Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblComponents()
    {
        return $this->hasMany(TblComponents::className(), ['price_method_id' => 'price_method_id']);
    }
}
