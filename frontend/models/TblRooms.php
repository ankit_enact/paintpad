<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_rooms".
 *
 * @property int $rooms_id
 * @property int $quote_id
 * @property string $room_name
 * @property int $room_type_id
 * @property int $is_custom
 * @property int $include
 * @property int $is_save_cust_special_items
 * @property int $created_at
 * @property int $updated_at
 *
 * @property TblRoomCompPref[] $tblRoomCompPrefs
 * @property TblRoomDimensions[] $tblRoomDimensions
 * @property TblRoomDimensions[] $tblRoomDimensions0
 * @property TblRoomNotes[] $tblRoomNotes
 * @property TblRoomPref[] $tblRoomPrefs
 * @property TblRoomSpecialItems[] $tblRoomSpecialItems
 */
class TblRooms extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_rooms';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['quote_id', 'created_at', 'updated_at'], 'required'],
            [['quote_id','is_save_cust_special_items', 'include', 'created_at', 'updated_at'], 'integer'],
            [['room_name'], 'string'],
            [['quote_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblQuotes::className(), 'targetAttribute' => ['quote_id' => 'quote_id']],
            [['room_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => TblRoomTypes::className(), 'targetAttribute' => ['room_type_id' => 'room_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'rooms_id' => 'Rooms ID',
            'quote_id' => 'Quote ID',
            'room_name' => 'Room Name',
            'room_type_id' => 'Room Type ID',
            'is_custom' => 'Is Custom',
            'is_save_cust_special_items' => 'Is Save Cust Special Items',
            'include' => 'Include',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblRoomCompPrefs()
    {
        return $this->hasMany(TblRoomCompPref::className(), ['room_id' => 'rooms_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblRoomDimensions()
    {
        return $this->hasMany(TblRoomDimensions::className(), ['room_id' => 'rooms_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblRoomPrefs()
    {
        return $this->hasMany(TblRoomPref::className(), ['room_id' => 'rooms_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblRoomSpecialItems()
    {
        return $this->hasMany(TblRoomSpecialItems::className(), ['room_id' => 'rooms_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblRoomBrandPref()
    {
        return $this->hasMany(TblRoomBrandPref::className(), ['room_id' => 'rooms_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblRoomPaintDefaults()
    {
        return $this->hasMany(TblRoomPaintDefaults::className(), ['room_id' => 'rooms_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblRoomNotes()
    {
        return $this->hasMany(TblRoomNotes::className(), ['room_id' => 'rooms_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuote()
    {
        return $this->hasOne(TblQuotes::className(), ['quote_id' => 'quote_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoomType()
    {
        return $this->hasOne(TblRoomTypes::className(), ['room_id' => 'room_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTblExtQuoteComponent()
    {
        return $this->hasMany(TblExtQuoteComponent::className(), ['room_id' => 'rooms_id']);
    }
}
