<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class ContactAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/jquery-ui.css',
        'frontend/web/css/style.css',
        'frontend/web/css/bootstrap-select.min.css',
        'frontend/web/css/timepicki.css',
        'frontend/web/css/jquery.datepick.css',
        'frontend/web/css/clndr.css',
        //'css/contactCss.css',
    ];
    public $js = [
        'frontend/web/js/moment.js',
        'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js',
        'frontend/web/js/underscore.js',
        'frontend/web/js/tether.min.js', 
        'frontend/web/js/bootstrap.min.js',
        'frontend/web/js/bootstrap-select.min.js',
        'js/jquery-ui.min.js',
        'frontend/web/js/timepicki.js',
        'frontend/web/js/timepicker.js',
        'frontend/web/js/jquery.datepick.js',
        'js/map-function.js',
        'frontend/web/js/clndr.js',
        'frontend/web/js/calendarInit.js',

        //'js/contactJs.js',
        'http://maps.google.com/maps/api/js?key=AIzaSyC2Qg3ywv9Dhg-Ptjv-5fG3I3UEt0rLufQ&libraries=places&callback=initMap',
        
        'js/jquery.mCustomScrollbar.concat.min.js', 
        'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js',
        'frontend/web/js/custom-contact.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
