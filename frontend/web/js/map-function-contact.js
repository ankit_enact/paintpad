var base_url = window.location.origin + '/' + window.location.pathname.split ('/') [1] + '/';

function initMapContact(static_lat,static_lng){
	var mapDivSite = document.getElementById('static-map-canvas');
	// var static_lat = document.getElementById('client_lat').value;
	// var static_lng = document.getElementById('client_lng').value;
	mapStatic = new google.maps.Map(mapDivSite, {
	zoom: 15,
	disableDefaultUI: true
	});
	mapStatic.setCenter(new google.maps.LatLng(static_lat, static_lng))

	editQuotemapDivSite = document.getElementById('static-edit-quote-map-canvas');
	editQuoteMapStatic = new google.maps.Map(editQuotemapDivSite, {
	zoom: 15,
	disableDefaultUI: true
	});
}