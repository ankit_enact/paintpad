
$(document).ready(function(){

    var base_url=siteBaseUrl+'/';
    var quoteStatusArr =
                {
                    1:{'status':'Pending','statusIcon':'quote_pending_active.png'},
                    2:{'status':'In-progress','statusIcon':'quote_progress_active.png'},
                    3:{'status':'Completed','statusIcon':'quote_progress_active.png'},
                    4:{'status':'Accepted','statusIcon':'quote_complt_active.png'},
                    5:{'status':'Declined','statusIcon':'quote_complt_active.png'},
                    6:{'status':'New','statusIcon':'quote_complt_active.png'},
                    7:{'status':'Offered','statusIcon':'quote_complt_active.png'},
                    8:{'status':'Open','statusIcon':'quote_complt_active.png'},
                } ;

    $('#timepicker1').timepicki();
    $('#timepicker2').timepicki();

	$(".fa-bars").click(function() {
		  $(".menu").removeClass('menuClose');
		  $(".menu").addClass('menuOpen');

		  $(".mainClose").addClass('mainOpen');
		  $(".mainOpen").removeClass('mainClose');

		  $(".fa-bars").hide(500);
		  $(".fa-times").show(500);
	});

	$(".fa-times").click(function() {
		  $(".menu").addClass('menuClose');
		  $(".menu").removeClass('menuOpen');

		  $(".mainOpen").addClass('mainClose');
		  $(".mainClose").removeClass('mainOpen');

		  $(".fa-times").hide(500);
		  $(".fa-bars").show(500);

    });
    
    $("#searchContactName").on("keyup", function(){
		$('.noAppointment').remove();
		var searchClient = $(this).val();
		//console.log(searchClient);
		searchClient = searchClient.toLowerCase();
		if(searchClient != ""){
			$('.tosearch').show();
			var flag=0;
			$(".contact_nameSearch").each(function(){
				var thisHtml = $(this).html();
				thisHtml = thisHtml.toLowerCase();
				//console.log(thisHtml);
				if(thisHtml.indexOf(searchClient) < 0 ){
					$(this).parent().parent().hide();
				}
				else
				{
					flag=1;
				}
			});
			if(flag ==0)
			{
				var fieldHtml="<li class='alert alert-danger noAppointment'>NO Contacts</li>";
				$('.contact_list').append(fieldHtml);
			}
		}
		else
		{
			$('.tosearch').show();
		}
    });

    function quotesGet(quotesListing,a){
        var i=0;
        while(item = quotesListing[i++]){

            //console.log(item);
            var date=item.date;
            
            var status  = item.status; 
            var dateObj = new Date(date*1000)
            var month = dateObj.getMonth() + 1; //months from 1-12
            var monthName = moment.months(month - 1); 
            //console.log(monthName);
            var monthName = moment.months(month - 1);  
            var day = dateObj.getDate();
            //console.log(day);
            var types = item.type;
            if(types == 1)
            {
                types = 'Interiors';
                var icons =	'quotes_Interior_icon.png';
            }
            else if(types == 2)
            {
                types = 'Exteriors';
                var icons =	'quotes_exterior_icon.png';
            }

            statusStr 		= quoteStatusArr[status]['status'];
            statusIcon		= quoteStatusArr[status]['statusIcon'];

            fieldHtmlQuote='<ul class="quotes_list quoteTr" data-href="'+base_url+'quote/view/'+item.quote_id+'"><li class="quoteTr" ><small class="detailsQuotesMonth">'+monthName+'</small><h4 class="detailsQuotesDate">'+day+'</h4></li><li><span class="detailsQuotesId">'+item.quote_id+'</span><h3 class="contact_name">'+item.contact.name+'</h3><small>'+item.contact.phone+'</small></li><li><p class="detailsQuotesdescription">'+item.description+'</p></li><li><div class="interior"><img src="<?php echo Url::base();?>/image/'+icons+'"><span class="detailsQuotesType">'+types+'</span></div></li><li><div class="interior"><img src="<?php echo Url::base();?>/image/quote_price-icon.png"><span class="detailsQuotesprice">$'+item.price+'</span></div></li><li><div class="compt_Action"><img src="<?php echo Url::base();?>/image/'+statusIcon+'"><span>'+statusStr+'</span></div></li></ul>'
            // console.log(item);
            //    console.log(item.date);
            if(a==0)
            {
                $("#detailsQuotesShow").append(fieldHtmlQuote);	
            }
            if(a==1)
            {
                $(".contact_quotes").append(fieldHtmlQuote);
            }
           
            
        }

    }
    
    //console.log(quoteStatusArr);
    function getContactData(id,sid){
        $.getJSON(
            base_url+"webservice/contact-details?subscriber_id="+sid+"&contact_id="+id,
            function (data) {
                //console.log(data);	
                $("#detailsQuotesShow").empty();	
                // console.log(data.data.contact.image);
                // console.log(base_url+"uploads/");
                if(base_url+"uploads/" == data.data.contact.image){
                    data.data.contact.image = base_url+"uploads/avtar.png"
                    //console.log(data.data.contact.image)
                    //console.log("ravi");
                }

                var quotesListing = data.data.quotes;
                quotesGet(quotesListing,0);


                var lat=data.data.contact.lat;
                var lng=data.data.contact.lng;
                $("#latlng").val(lat+','+lng)
                $(".contact_name" ).empty();
                $(".contact_email").empty();
                $(".contact_phn").empty();
                $(".personal_profile").empty();	
                $(".contactsIcon").empty();				
                $(".docsIcon").empty();			
                $(".jssIcon").empty();	
                $(".quotesIcon").empty();	
                $(".addressClicked").empty();	
                $(".contact_name" ).append(data.data.contact.name);
                $(".contact_email" ).append(data.data.contact.email);
                $(".contact_phn" ).append(data.data.contact.phone); 
                $(".personal_profile" ).append("<img src='"+data.data.contact.image+"' height='80px' width='80px' style='border-radius:58px'>"); 	
                $(".contactsIcon" ).append(data.data.counts.contacts); 		
                $(".docsIcon" ).append(data.data.counts.docs); 	            
                $(".jssIcon" ).append(data.data.counts.jss); 	       
                $(".quotesIcon" ).append(data.data.counts.quotes); 	   
                $(".addressClicked").append(data.data.contact.formatted_addr);
                $('.updateContactIcon').attr('contactIdUpdate',data.data.contact.contact_id);
                    
        });
    }

    var id = $('#detailsContact').val();
    var sid= $("#subscriber_id").val();
    //var sid=1;
    getContactData(id,sid);



    //console.log(base_url);
    $("ul").on("click",".tosearch", function(e){
        var id = $(this).attr('data-id');
        $('.tosearch').removeClass("activeContact")
        var sid= $("#subscriber_id").val();
        //var sid=1;
        getContactData(id,sid);
        $(this).addClass("activeContact")		
    })

    $(function() {
        $("div").on("click",".bodyViewer", function(e){
            var body_id = $(this).attr('body-item');
            var sid= $("#subscriber_id").val();
            //var sid=1;
            $.ajax({
                url: base_url+'/communications/view?id='+body_id,
                headers: { 'QuoteID': body_id,'SubscriberID':sid},
                success: function (response) {						 
                    $('#myModal').modal({show:true});	
                    $('.modal-bodyCommunications').html(response);		
                }
            });
            e.stopImmediatePropagation();
            return false;
        });			
    });
    

    var contactUrl=base_url+"webservice/create-contact";
    function createContact(url){
        var id =$('#contact_idForm').val();
        var sid =$('#subscriber_idForm').val();
        var form = $('form')[0]; // You need to use standard javascript object here
        var form = $('#contactId')[0];
        var data = new FormData(form);
        $.ajax({
            url:  url,
            type: "POST",
            data: data, // Send the object.

            success: function(response) {
                //console.log(response);
                $('.contact_list').empty();	
                $.ajax({
                        url:base_url+"contact/searchcontact",
                        //type: "POST",
                        //data: data, // Send the object.		    
                        success: function(response) {
                            var parsed = JSON.parse(response);
                            //console.log(parsed);
                            var i=0;	
                            while(item = parsed.data[i++]){
                                if(item.image=="")
                                {
                                    item.image="avtar.png";
                                }
                                var fieldhtmlList ="<li class='tosearch' data-id='"+item.id+"'><span class='cnt_img'><img src='"+base_url+"uploads/"+item.image+"' height='58px'  width='58px' style='border-radius: 50%;''></span><div class='contact_person_detail'><p class='contact_nameSearch'>"+item.name+"</p><small>"+item.phone+"</small></div></li>";
                                $(".contact_list").append(fieldhtmlList);    
                            }
                            $("#addContact").modal('hide');
                            getContactData(id,sid);

                        },
                });	
                                                
            },			           
            contentType: false,   
            processData:false,	
        });	
    }

    $('#saveContactData').click(function(){
		createContact(contactUrl);
	});


	$('.add_contact').click(function(){
		$('#blah').attr('src',base_url+'image/avtar.png');
		contactUrl=base_url+"webservice/create-contact";		
    });
    
    $('.updateContactIcon').click(function(){
		var id=$(this).attr("contactidupdate");
		contactUrl = base_url+"webservice/update-contact";	
		var sid= $("#subscriber_id").val();

		$(function () {
		    $.getJSON(
		       base_url+"webservice/contact-details?subscriber_id="+sid+"&contact_id="+id,
		        function (data) {
		        	//console.log(data);
		        	//console.log(data.data.contact.contact_id);
		        	$('#contactName').val(data.data.contact.name);
		        	$('#contactEmail').val(data.data.contact.email);
		        	$('#contactPhone').val(data.data.contact.phone);
		        	$('#lat').val(data.data.contact.lat);
		        	$('#lng').val(data.data.contact.lng);
		        	$('#address').val(data.data.contact.formatted_addr);
		        	$('#street').val(data.data.contact.street1);
		        	$('#suburb').val(data.data.contact.suburb);
		        	$('#state').val(data.data.contact.state);
		        	$('#zipCode').val(data.data.contact.postal);
		        	$('#blah').attr("src",data.data.contact.image);
		        	$('#contact_idForm').val(data.data.contact.contact_id);
		        }
	        )
		})
	})



	$("div").on("click",".addAppointment", function(e){		
		$('#myModal2').modal({show:true});
		$('#myModal2').removeAttr('tabindex');	
		e.stopImmediatePropagation();
		return false;
    })
    
    $(".clientDetail").click(function(){
		//$(".tab-content").append();
	})
});
