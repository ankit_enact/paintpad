// Call this from the developer console and you can control both instances
var calendars = {};

$(document).ready( function() {
    // console.info(
    //     'Welcome to the CLNDR demo. Click around on the calendars and' +
    //     'the console will log different events that fire.');

    // Assuming you've got the appropriate language files,
    // clndr will respect whatever moment's language is set to.
    // moment.locale('ru');

    // Here's some magic to make sure the dates are happening this month.
    var thisMonth = moment().format('YYYY-MM');
    // Events to load into calendar
    /*var eventArray = [
        {
            title: 'Multi-Day Event',
            endDate: thisMonth + '-14',
            startDate: thisMonth + '-10'
        }, {
            endDate: thisMonth + '-23',
            startDate: thisMonth + '-21',
            title: 'Another Multi-Day Event'
        }, {
            date: thisMonth + '-27',
            title: 'Single Day Event'
        }
    ];*/

    var date = $("#calenderEventsId").val();
    var eventArray = JSON.parse(date);
    
    /*var eventArray = [
        { date: '2018-09-09' }
    ];*/

    // The order of the click handlers is predictable. Direct click action
    // callbacks come first: click, nextMonth, previousMonth, nextYear,
    // previousYear, nextInterval, previousInterval, or today. Then
    // onMonthChange (if the month changed), inIntervalChange if the interval
    // has changed, and finally onYearChange (if the year changed).
    calendars.clndr1 = $('.cal1').clndr({
        daysOfTheWeek: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        events: eventArray,
        clickEvents: {
            click: function (target) {
               // console.log('Cal-1 clicked: ', target);
                $("#w0").fullCalendar( 'gotoDate', target.date._d ) ;
                var dateObj = new Date(target.date._d);
                     var month = dateObj.getMonth() + 1; //months from 1-12
                     var day = dateObj.getDate();
                     var year = dateObj.getFullYear();

                    if (day.toString().length == 1) {
                      day = "0" + day;
                    }
                    if (month.toString().length == 1) {
                      month = "0" + month;
                    }
                    newdate = year + "-" + month + "-" + day;
                    $('.day').removeClass('intro');
                    $(".calendar-day-"+newdate).addClass("intro");
                    console.log(newdate);
                //console.log('Cal-1 clicked: ', target);
            },
            today: function (target) {
                //console.log('Cal-1 today');
            },
            nextMonth: function (target) {
                //console.log('Cal-1 next month',target);
                $("#w0").fullCalendar( 'gotoDate', target._d ) ;
            },
            previousMonth: function (target) {
                //console.log('Cal-1 previous month',target);
                 $("#w0").fullCalendar( 'gotoDate', target._d ) ;
            },
            onMonthChange: function () {
                //console.log('Cal-1 month changed');
            },
            nextYear: function () {
                //console.log('Cal-1 next year');
            },
            previousYear: function () {
                //console.log('Cal-1 previous year');
            },
            onYearChange: function () {
                //console.log('Cal-1 year changed');
            },
            nextInterval: function () {
                //console.log('Cal-1 next interval');
            },
            previousInterval: function () {
                //console.log('Cal-1 previous interval');
            },
            onIntervalChange: function () {
                //console.log('Cal-1 interval changed');
            }
        },
        multiDayEvents: {
            singleDay: 'date',
            endDate: 'endDate',
            startDate: 'startDate'
        },
        showAdjacentMonths: true,
        adjacentDaysChangeMonth: false
    });

    

    // Bind all clndrs to the left and right arrow keys
    $(document).keydown( function(e) {
        // Left arrow
        if (e.keyCode == 37) {
            calendars.clndr1.back();
            calendars.clndr2.back();
            calendars.clndr3.back();
        }

        // Right arrow
        if (e.keyCode == 39) {
            calendars.clndr1.forward();
            calendars.clndr2.forward();
            calendars.clndr3.forward();
        }
    });
});