<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TblCommunications */

$this->title = 'Create Tbl Communications';
$this->params['breadcrumbs'][] = ['label' => 'Tbl Communications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-communications-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model, 'emails' =>$emails, 'head' => $head              
    ]) ?>

</div>
