<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TblCommunications */

//$this->title = $model->comm_id;
/*$this->params['breadcrumbs'][] = ['label' => 'Tbl Communications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/

if(isset($head) && $head == 1){
  echo '<style>.mainClose{ display: none; } .text-center{ display: none; } .footer{ display: none; }.menu{ display: none; }</style>';
}

?>



<style type="text/css">
.mail_box .form-group label{
    text-align: inherit;
    width: 3% !important;
}    
.mail_box .form-group p {
    text-align: inherit;
}  
.groupp label{
    text-align: inherit;
    width: 5% !important;
    font-size: 14px;
}   

body {
    width: 100%;
    background-color: #ffffff;
    margin: 0;
    padding: 0;
    -webkit-font-smoothing: antialiased;
    mso-margin-top-alt: 0px;
    mso-margin-bottom-alt: 0px;
    mso-padding-alt: 0px 0px 0px 0px;
}

p,h1,h2,h3,h4 {
    margin-top: 0;
    margin-bottom: 0;
    padding-top: 0;
    padding-bottom: 0;
}
table{
    border-collapse: unset;
}

/* ----------- responsivity ----------- */

@media only screen and (max-width: 640px) {                     
    td, tr, table {
        width: 99% !important;
        float: left !important;
        margin: 0 !important;
    }
}

</style>

<div class="tbl-communications-view">

<?php   
//echo '<pre>'; print_r($model); exit; 
// if(count($model)>0){

  $host_name = isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : $_SERVER['HTTP_HOST'];          
  $siteURL = 'http://' . $host_name . '/' . Yii::$app->request->baseUrl;  
?>

<table class="bg_color" style="text-align: center;margin: 12px auto;" width="98%" border="0" bgcolor="ffffff">
 <tr>  
 <td align="left" width="25%"  style="vertical-align: top;">
   <table border="0" align="left" cellpadding="0" cellspacing="0" class="container590">
             <tr>
                <td align="left">
                    <table>
                        <tbody><tr>
                            <td style=" font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif;" align="left">                              
                                <p style="margin-bottom:15px;font-weight:bold">
                                    <?php echo date('d F Y',$model->created_at); ?>, BY <?php echo ucwords($model->commSubscriber->username); ?>
                                </p>
                                <p style="margin-bottom:20px;background: #5a5a5a;text-align: center;border-radius: 3px;width: 60px;color: #fff;">
                                    email
                                </p>
                            </td>
                        </tr>
                       </tbody>
                    </table>
                </td>
         </tr>
    </table>
 </td> 
<td align="left" width="75%">
<table border="0" align="left" cellpadding="0" cellspacing="0" class="container590" style="box-shadow: 2px 4px 5px 0px rgba(0,0,0,.5);padding: 10px 20px;">
         <tr>
            <td align="left">
                <table>
                    <tbody><tr>
                        <td style=" font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;" align="left">       
                            <p style=" margin-bottom:5px;">
                                <?php echo ucwords($model->commQuote->description); ?>
                            </p>
                            <p style="margin-bottom:15px;">
                                <b>From : </b> &nbsp; <?php echo ucwords($model->commSubscriber->username); ?>
                                <br>
                                <b>Date : </b> &nbsp; <?php echo date('d F Y',$model->created_at); ?>
                                <br>
                                <b>To : </b> &nbsp; <?php echo $model->receiver_email; ?>                                                              
                            </p>                            
                        </td>
                    </tr>
                   </tbody>
                </table>
            </td>
     </tr>
<tr> 
     <td>
         <table border="0" width="100%" align="left" style="height:1px; background:#ccc">
             <tr></tr>
         </table>
    </td>
</tr>     
<tr> 
     <td>
         <table style="" width="100%" border="0" align="left">
             <tbody><tr>
            <td align="left">
                <table>
                    <tbody>

                    <tr>
                        <td style=" font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;" align="left">                        
                            <?php echo $model->body; ?>                   
                        </td>
                    </tr>
                    <!-- <tr style="margin-bottom:15px;float: left;">
                       <td align="left" style="float: left;">
                            <a href="" style="display: block; border-style: none !important; border: 0 !important;"><img style="display: block; width: 80px;" src="https://mdbootstrap.com/img/logo/mdb-email.png" alt="" width="80" border="0"></a>
                        </td> 
                        <td align="left" style="float: left;margin-left: 20px;">
                            <a href="" style="display: block; border-style: none !important; border: 0 !important;"><img style="display: block; width: 80px;" src="https://mdbootstrap.com/img/logo/mdb-email.png" alt="" width="80" border="0"></a>
                        </td>
                    </tr> -->   

                   </tbody>
                </table>
            </td>
         </tr>
         </tbody>
         </table>
    </td>
</tr>
</table>
</td> 
</tr>
<tr> 
    <td align="left" width="25%" style="vertical-align: top;">
        <table border="0" align="left" cellpadding="0" cellspacing="0" class="container590">
            <tr>
                <td align="left">
                    <table>
                        <tbody></tbody>
                    </table>
                </td>
            </tr>
        </table>
    </td>
    <td align="left" width="75%">
        <table border="0" align="left" cellpadding="0" cellspacing="0" width="100%"  style="padding: 10px 00px;">
            <tr>
                <td align="left">
                    <table width="100%">
                        <tr>
                            <ul style="padding: 0; margin: 0; margin-top: 20px;">

                                <?php

                                    if($model->attachment !='' ){
                                       $comma = ',';
                                       $attachments = $model->attachment;

                                       if( strpos($attachments, $comma) !== false ) {                   
                                           $arr = explode(',', $attachments);                                           
                                           foreach($arr as $_arr){
                                            echo '<li style="border: 1px solid #ccc;list-style: none;padding: 3px 10px;margin: 0;"><a download=""  href="'.$siteURL.'/'.$_arr.'"><img src="'.$siteURL.'/'.$_arr.'" height=30 width=30/></a></li>';
                                           }// foreach                                           
                                       }else{
                                        echo '<div><img src="'.$siteURL.'/'.$model->attachment.'" height=100 width=100/> </div>';
                                       }
                                    }

                                ?>

                            </ul>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>    
</tr>
</table>

    <?php
      //  }//if
    ?>

</div>

<?php
$script = <<< JS
    $(document).ready(function(){

        $('body').addClass('respond');
        $('body').attr('leftmargin',0);
        $('body').attr('topmargin',0);
        $('body').attr('marginwidth',0);
        $('body').attr('marginheight',0);

    });
JS;
$this->registerJs($script);
?>

