var base_url = window.location.origin + '/' + window.location.pathname.split ('/') [1] + '/';
const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

function setQuoteDetail(){
	//alert('chc');
	var quote_id = $("#quote_id").val();
	var subscriber_id = $("#subscriber_id").val();
	url = siteBaseUrl+'/webservice/quotes?subscriber_id='+subscriber_id+'&quote_id='+quote_id;
	$.ajax({
		url:url,
		method:'get',
		dataType:'JSON',
		success:function(response){
			//console.log(response)
			if(response.success == "1"){
				//console.log(response)
				var curQuote =response.data.quotes[0];
				if(curQuote.contact.image != ""){
					console.log(curQuote.contact.image+"::")
					$("#quoteContactImg").attr("src",curQuote.contact.image)
				}
				
				$("#quoteContactName").html(curQuote.contact.name)
				$("#quoteContactEmailPhone").html(curQuote.contact.email+"<br>"+curQuote.contact.phone)

				if(curQuote.description == ""){
					$("#quoteNameDesc").html("Project "+curQuote.quote_id+" - "+curQuote.client_name)
				}
				else{
					$("#quoteNameDesc").html("Project "+curQuote.quote_id+" "+curQuote.description+" - "+curQuote.client_name)
				}
					
				$("#quoteContactSiteAddress").html(curQuote.site.formatted_addr)

				$("#quoteSiteName").html(curQuote.site.name)
				$("#quoteSiteEmail").html(curQuote.site.email)
				$("#quoteSitePhone").html(curQuote.site.phone)

				$("#quote_count_jss").html(curQuote.counts.jss)
				$("#quote_count_contacts").html(curQuote.counts.contacts)
				$("#quote_count_quotes").html(curQuote.counts.quotes)
				$("#quote_count_docs").html(curQuote.counts.docs)

				$("#paintDefaultExists").val(curQuote.paintDefaultExists);
				$("#roomsExists").val(curQuote.roomsExists);

				$("#quote_status").val(curQuote.status);

				initializeEditQuotePopUp(curQuote);

				var li = document.getElementById('contact_'+curQuote.contact.contact_id);

				$(li).click();
				
			}
			else{
				//dummy alert
				alert(response.message)
			}

		},
		error:function(err){
			console.log(err)
		}
	});
}

$(document).ready(function(){


	window.addEventListener('load', function(){
		var allimages= document.getElementsByClassName('lazy_load_image');
		for (var i=0; i<allimages.length; i++) {
			if (allimages[i].getAttribute('data-src')) {
				allimages[i].setAttribute('src', allimages[i].getAttribute('data-src'));
			}
		}
	}, false)


	var subscriber_id = $("#subscriber_id").val();
	var contact_id;
	var quoteTypes;
	var status;
	var from;
	var to;
	var quote_id;
	var if_filter_applied = 0;
	var site_url = $("#site_url").val();
	var undercoatOptions ='';

	function printBrandPrefDefaults(){
		var sbText = $("#selbrand option:selected").text();
		var stText = $("#seltiers option:selected").text();
		var dcText = $("#defaultCoat option:selected").text();
		var dplText = $("#defaultPrepLevel option:selected").text();
		var ucmText = "No Undercoats";

		if($("#underCoatMain:checked").length > 0){
			ucmText = "With Undercoats";
		}

		$('.room_default_setting').html(sbText+', '+stText+', '+dcText+', '+dplText+', '+ucmText)
	}

	if($("#ifUpdate").length > 0){
		printBrandPrefDefaults()
	}

	

	if($("#page_name").val() == "quoteEdit"){	
		setQuoteDetail();		
	}

	$(document).on('click','#editQuoteTrigger',function(){
		$('#InteriorQuote').modal('show');
		$('#menu2').removeClass('active')
		$('#menu2').removeClass('show')
		$('#new_client_link').removeClass('active')
		$('#new_client_link').removeClass('show')
		$('#existing_client_link').addClass('active')
		$('#menu3').addClass('in active show')
		//$('.existing_client_list').animate({ scrollTop: $('.existing_client_list li:nth-child(14)').position().top}, 200);

		var selected_client = $("#selected_client").val();
		setTimeout(function(){
			var listItem = $( "#contact_"+selected_client );
			//alert( "Index: " + listItem.index( ".existing_client_list li" ) );
			var lichild = listItem.index( ".existing_client_list li" );

			$('.existing_client_list').scrollTop($('.existing_client_list li:nth-child('+lichild+')').position().top);

			$('.existing_client_list').css("opacity",1);
		}, 1000);

			
	});

	$(document).on('keyup','#search_existing_client',function(){
		var searchClient = $(this).val();
		searchClient = searchClient.toLowerCase();
		if(searchClient != ""){
		  $('.contact_li').show();
		  $(".client_name_list").each(function(){
		    var thisHtml = $(this).html();
		    thisHtml = thisHtml.toLowerCase();
		    if(thisHtml.indexOf(searchClient) < 0 ){
		      $(this).parent().parent().hide();
		    }
		  });
		}
		else{
			$('.contact_li').show();
		}
	});

	$(".fa-bars").click(function() {
		$(".menu").removeClass('menuClose');
		$(".menu").addClass('menuOpen');

		$(".mainClose").addClass('mainOpen');
		$(".mainOpen").removeClass('mainClose');

		$(".fa-bars").hide(500);
		$(".fa-times").show(500);
	});

	$(".fa-times").click(function() {
		$(".menu").addClass('menuClose');
		$(".menu").removeClass('menuOpen');

		$(".mainOpen").addClass('mainClose');
		$(".mainClose").removeClass('mainOpen');

		$(".fa-times").hide(500);
		$(".fa-bars").show(500);

	});

	$(document).on("click",".quoteTr",function() {
		window.location = $(this).data("href");
	});

	/*
	* Name: Jotpal
	* Date: 20180912
	* Comment: 
	*/
	function getQuoteStatus(status,site_url){
		statusStr = "Pending";
		statusImg = site_url+'/image/quote_pending_active.png';
		switch (status) {
		  case 2:
		      statusStr = "In-progress";
		      statusImg = site_url+'/image/quote_progress_active.png';
		      break;
		  case 3:
		      statusStr = "Completed";
		      break;
		  case 4:
		      statusStr = "Accepted";
		      break;
		  case 5:
		      statusStr = "Declined";
		      break;
		  case 6:
		      statusStr = "New";
		      break;
		  case 7:
		      statusStr = "Offered";
		      break;
		  case 8:
		      statusStr = "Open";
		      break;
		  default:
		      statusStr = "Pending";
		      break;
		}

		return [statusStr,statusImg];
	}

  /*
  * Name: Jotpal
  * Date: 20180912
  * Comment: 
  */
  function getQuotes(event,page){

    if(if_filter_applied == 1){
      var data = {"subscriber_id":subscriber_id,"contact_id":contact_id,"type":quoteTypes,"status":status,"from":from,"to":to,"quote_id":quote_id,"page":page}
    }
    else{
      var data = {"subscriber_id":subscriber_id,"page":page}
    }
    var quote_url = $("#quote_url").val();
    $.ajax({
          url:quote_url,
          method:'get',
          data:data,
          dataType:'JSON',
          success:function(response){
            if(response.success == "1"){
              page = parseInt(page)+1;
              var quote_view_url = $("#quote_view_url").val();
              $("#quote_page").val(page);
              if(response.data != "" && response.data.quotes.length > 0){
                  var str = "";
                  for (var i = 0; i < response.data.quotes.length; i++) {
                      var curData = response.data.quotes[i];

                      statusList = getQuoteStatus(curData['status'],site_url);
                      statusStr = statusList[0];
                      statusImg = statusList[1];
                      var d = new Date(curData['date']*1000);
                      var curDate = d.getDate();
                      if(curDate < 10){
                        curDate = String(curDate);
                        curDate= 0 + curDate;
                      }
                      
                      
                      var curMonth = monthNames[d.getMonth()]

                      typeStr = (curData['type'] == 1)?'Interiors':'Exteriors';


                      var curNote  = ( curData['note'] != null ) ? curData['note'] : '';
                      // console.log(curData['note'])


                      str += '<tr class="quoteTr" data-href="'+quote_view_url+'/'+curData['quote_id']+'">'+
                        '<td><small>'+curMonth+'</small><h4>'+curDate+'</h4></td>'+
                        '<td><span>'+curData['quote_id']+'</span><h3 class="name">'+curData['contact']['name']+'</h3><span class="phnNo">'+curData['contact']['phone']+'</span></td>'+
                        '<td>'+curNote+'</td>'+
                        '<td><div class="interior"><img src="'+site_url+'/image/quotes_Interior_icon.png"><span>'+typeStr+'</span></div></td>'+
                        '<td><div class="interior"><img src="'+site_url+'/image/quote_price-icon.png"><span>$'+curData['price']+'</span></div></td>'+
                        '<td><div class="interior statusReport"><img src="'+statusImg+'"><span>'+statusStr+'</span></div></td>'+
                      '</tr>';
                  }
                  if(event == "endScroll"){
                    $("#mCSB_1_container").append(str);
                  }
                  else{
                    $("#mCSB_1_container").html(str);
                  }
              }
              else{
                if(event != "endScroll"){
                  $("#mCSB_1_container").html("<tr><td style='text-align: center;' colspan='6'>No Results Found.</td></tr>");
                }
              }
            }
            else{

            }
          },
          error:function(error){

          },
        });
  }

  /*
  * Name: Jotpal
  * Date: 20180912
  * Comment: 
  */
  /*
  $(document).on('scroll','#mCSB_1', function () {
    //mCSB_1_container
    console.log($(this).scrollTop())
      if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
        var page = $("#quote_page").val();
        getQuotes('endScroll',page);
      }
  })
  */
  if($('#quotesTbody').length > 0){
    $('#quotesTbody').mCustomScrollbar({
        callbacks:{
            onTotalScroll:function(){
              var page = $("#quote_page").val();
              getQuotes('endScroll',page);
              /*console.log($(this).scrollTop())
              if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                var page = $("#quote_page").val();
                getQuotes('endScroll',page);
              }*/
            }
        }
    });
  }

  $(document).on('click','#filerApply',function(){
      page = 0;

      subscriber_id = $("#subscriber_id").val();
      contact_id = $("#quote_contact_id").val();
      var if_interior_quote = $("#if_interior_quote").val();
      var if_exterior_quote = $("#if_exterior_quote").val();
      var typeArr = [];
      
      if(if_interior_quote == 1){typeArr.push(1)}
      if(if_exterior_quote == 1){typeArr.push(2)}
      


      	quoteTypes = typeArr.join(',');

  		var selectedStatus = [];

	    $.each($('.QuotePageStatus'), function(){

	        if (!$(this).hasClass('exclude-filter')) {
	        	selectedStatus.push( $(this).data('value'))
	        }
	    });

	    var ss = selectedStatus.join(',');

    	$("#quote_status").val( ss );



      status = $("#quote_status").val();
      from = $("#txtFromDate").val();
      to = $("#txtToDate").val();
      quote_id = $("#quote_id").val();

      if(from != ""){
        from = from.split("/").reverse().join("-");
        from = new Date(from).getTime() / 1000;
      }
      if(to != ""){
        to = to.split("/").reverse().join("-");
        to = new Date(to).getTime() / 1000;
      }
      if_filter_applied = 1
      getQuotes('filterapply',page);
  });

  $(document).on('click','#filterReset',function(){

  	$('.QuotePageStatus').removeClass('exclude-filter');
      if_filter_applied = 0
      page = 0;
      getQuotes('filterReset',page);
  });

  $(document).on('click','.quoteTypeSel',function(){
      var curtochange = $(this).data('tochange');
      curVal = $("#"+curtochange).val();
      ifSet = (curVal == 1)?0:1
      if(ifSet == 0){
        var checkAttr = $(this).data('check');
        check = $("#"+checkAttr).val();
        if(check == 1){
          $("#"+curtochange).val(ifSet);
          if($(this).hasClass('quotetypeactive')){
            $(this).removeClass('quotetypeactive')
          }
          else{
            $(this).addClass('quotetypeactive')
          }
        }
      }
      else{
        $("#"+curtochange).val(ifSet);
        if($(this).hasClass('quotetypeactive')){
          $(this).removeClass('quotetypeactive')
        }
        else{
          $(this).addClass('quotetypeactive')
        }
      }
  });

  $(".fa-bars").click(function() {
    $(".menu").removeClass('menuClose');
    $(".menu").addClass('menuOpen');

    $(".mainClose").addClass('mainOpen');
    $(".mainOpen").removeClass('mainClose');

    $(".fa-bars").hide(500);
    $(".fa-times").show(500);
  });

  $(".fa-times").click(function() {
    $(".menu").addClass('menuClose');
    $(".menu").removeClass('menuOpen');

    $(".mainOpen").addClass('mainClose');
    $(".mainClose").removeClass('mainOpen');

    $(".fa-times").hide(500);
    $(".fa-bars").show(500);

  });

  if($("#txtFromDate").length > 0){
    $("#txtFromDate").datepicker({
        dateFormat: 'dd/mm/yy',
        onSelect: function(selected) {
          $("#txtToDate").datepicker("option","minDate", selected)
        }
    });
    $("#txtToDate").datepicker({
        dateFormat: 'dd/mm/yy',
        onSelect: function(selected) {
           $("#txtFromDate").datepicker("option","maxDate", selected)
        }
    });
  }


	$(function() {
		var ifSelectedFromACList = 0;
		if($("#contact_users").length>0){
			var contact_users = $("#contact_users").val();
			contact_users = JSON.parse(contact_users);
			$("#search_existing_client_quote").autocomplete({
				source: contact_users,
				minLength: 1,
				select: function(event, ui) {
					ifSelectedFromACList = 1;
					$("#quote_contact_id").val(ui.item.id)
				},
				html: true,
				focus: function(event, ui) {
					// ifSelectedFromACList = 0;
					// console.log(ifSelectedFromACList)
				},
				open: function(event, ui) {
				  $(".ui-autocomplete").css("z-index", 1000);
				},
				change: function() {
				  if(ifSelectedFromACList == 0){
				    $("#search_existing_client_quote").val('');
				    $("#quote_contact_id").val('');
				  }
				  //var curAcUser = $("#search_existing_client_quote").val();
				}
			}).autocomplete( "instance" )._renderItem = function( ul, item ) {
			return $( "<li class='contact_li'><div class='contact_li_img'><img src='"+item.img+"'></div><div class='existing_client_detail'><h3 class='client_name_list'>"+item.value+"</h3><small class='client_phone_list'>"+item.phone+"</small></div></li>" ).appendTo( ul );
			};

			$(document).on('focus','#search_existing_client_quote',function(){
				ifSelectedFromACList = 0;
				console.log(ifSelectedFromACList)
			})
		}
	});

	/*
		var date = $("#calenderEventsId").val();
		date = JSON.parse(date);
		//var fieldhtml= "<span>.</span>";
		$.each(date, function(key, value){
			console.log('calendar-day-'+value);
			$('.calendar-day-'+value).addClass('isEvent');
			$('.calendar-day-'+value).addClass('event');
		});
	*/
  
	if($('#defaultBrand').length > 0){
	  	/*setTimeout(function(){
			defaultBrand = $('#defaultBrand').val();
			$('.quote_tiers').each(function(){
				var c = $(this).attr('class');
				var mc = c.split(' ');
				if(mc[0]=='tb_'+defaultBrand){
					$(this).parent().show()
				}
				else{
					$(this).parent().hide()
				}
			});
		}, 2000);
		*/

		$(document).on('change','#selbrand',function(){
			defaultBrand = $(this).val();
			/*var found = 0;
			$('.quote_tiers').each(function(){
				var c = $(this).attr('class');
				var mc = c.split(' ');
				if(mc[0]=='tb_'+defaultBrand){
					found = 1
					$("#seltiers").val($(this).val());
					$("#seltiers").selectpicker('refresh');
					return;
				}
			});*/
			
			$('.quote_tiers').each(function(){
				var c = $(this).attr('class');
				var mc = c.split(' ');
				if(mc[0]=='tb_'+defaultBrand){
					$(this).show()
				}
				else{
					$(this).hide()
				}
			});

			$('#seltiers>option:eq(0)').attr('selected', true);
			
			printBrandPrefDefaults();
		});

		$(document).on('change','#seltiers, #defaultCoat, #defaultPrepLevel',function(){
			printBrandPrefDefaults();
		});
	}

	$(document).on('change','.sheendd',function(){
		var curSheen = $(this).val();
		var curGroup = $(this).data('group');

		$("#product_"+curGroup).find('option').hide();
		//$("#product_"+curGroup).find('option').hide();

		$("#product_"+curGroup+" option:selected").removeAttr('selected');

		var sheen_products = $("#product_"+curGroup).find('.sheen_'+curSheen);
		sheen_products.each(function(){
			var isdefault = $(this).data('isdefault');
			if(isdefault == 1){
				console.log($(this).val())
				$(this).attr("selected",true);
			}
		});
		$("#product_"+curGroup).find('.sheen_'+curSheen).show();

	});


	$(document).on('click','.underChecks',function(){
		var groupId =  $(this).data('group');
		if($('#undercheck_'+groupId+':checked').length > 0 ){
			$('#undercoatdd_'+groupId).parent().show()
		}
		else{
			$('#undercoatdd_'+groupId).parent().hide()
		}
	})

	$(document).on('click','#underCoatMain',function(){
		if($('#underCoatMain:checked').length > 0 ){
			$('.underChecks').prop('checked',true)
			$('.undercoat').parent().show()		
		}
		else{
			$('.underChecks').prop('checked',false)
			$('.undercoat').parent().hide()
		}
		printBrandPrefDefaults();
	})

	$(document).on('click','.checkChange',function(){
		if($("#ifPaintDefaultChanged").val() == 1){
			//console.log($("#paintDefaultForm").serialize());
			//console.log('change detected');
			//var data = $("#paintDefaultForm").serialize();

			var type_id = $("#quote_type").val();
			var quote_id = $("#quote_id").val();
			var subscriber_id = $("#subscriber_id").val();
			var brand_pref_id = $("#selbrand").val();
			var company_id = $("#company_id").val();

			var paint_default = []
			$(".job_default_details").each(function(){
				var curCompId = $(this).data('id');
				var curCompPaintDefault = {};
				
				console.log($("#undercheck_"+curCompId+":checked").length)
				curCompPaintDefault.topcoat = $("select[name='component["+curCompId+"][product]']:selected").val();
				console.log(curCompPaintDefault.topcoat)
				curCompPaintDefault.undercoat = null;
				/* if(){
					curCompPaintDefault.undercoat = 
				} */
				
				paint_default.push(curCompPaintDefault)
			});
			
			/* data={"brand_pref_id":brand_pref_id,"company_id":company_id,"quote_id":quote_id,"subscriber_id":subscriber_id};
			url = siteBaseUrl+'/quote/save-paint-defaults';
			$.ajax({
				url: url,
				type: 'post',
				data: data,
				dataType:'JSON',
				success: function (response) {
					//console.log(response);
					$("#ifPaintDefaultChanged").val(0);
				},
					error: function () {
					alert("Something went wrong");
				}
			}); */
			
		}
	})

	$(document).on('change','.changeType',function(){
		console.log('change event firing');
		var changetype = $(this).data('changetype');
		if(changetype == 2){
			console.log('making changes');
			$("#ifPaintDefaultChanged").val(1)
		}
	});

	$(document).on('change','.getPaintDetails',function(){
		var selbrand = $('#selbrand').val();
		var selTier = $('#seltiers').val();
		var quoteType = $('#quote_type').val();
		var quote_id = $("#quote_id").val();
		var defaultCoat = $('#defaultCoat').val();
		var defaultPrepLevel = $('#defaultPrepLevel').val();

		if(selbrand != "" && selTier!=""){
			
			url = siteBaseUrl+'/webservice/tiers-component';
			//url = siteBaseUrl+'/quote/get-paint-components';
			$.ajax({
				url: url,
				type: 'get',
				data: {'tier_id':selTier,"type_id":quoteType,"quote_id":quote_id},
				dataType:'JSON',
				success: function (response) {
					
					if(response.success==1){
						var groupStr='';
						groups = response.data;

						groups_length = Object.keys(groups).length;
						if(groups_length > 0){
							components = groups.tier_types;
							//var selectedSheen = response.selectedSheen;
							//var selectedSheen = response.selectedSheen;
							
							/* undercoat = response.undercoat;
							topcoat = response.topcoat;
							sheens = response.sheens;
							sheenProducts = response.sheenProducts; */
							components_length = Object.keys(components).length;
							if(components_length > 0){

								for (var ci=0; ci<components_length; ci++) {

									var curComp = components[ci];
									var gk = curComp.id;
									var sheens = curComp.sheens;
									var undercoat = curComp.under_coat;

									undercoatOptions ='';
									undercoat_length = Object.keys(undercoat).length;
									if(undercoat_length > 0){
										for (var uck in undercoat) {
											if(undercoat[uck]['default'] == 1){
												undercoatOptions+='<option selected value="'+undercoat[uck]['product_id']+'" >'+undercoat[uck]['name']+'</option>';
											}
											else{
												undercoatOptions+='<option value="'+undercoat[uck]['product_id']+'" >'+undercoat[uck]['name']+'</option>';
											}
										}
									}

									//console.log(undercoatOptions);

									var sheenOptions ='';
									var productOptions ='';
									sheens_length = Object.keys(sheens).length;
									if(sheens_length>0){
										for(var cs=0; cs<sheens.length; cs++) {
											var curSheen = sheens[cs];
											if(curSheen.default == 1){
												sheenOptions+='<option selected value="'+curSheen.sheen_id+'" >'+curSheen.name+'</option>';
											}
											else{
												sheenOptions+='<option value="'+curSheen.sheen_id+'" >'+curSheen.name+'</option>';
											}												

											var topcoat = curSheen.top_coats;
											if(topcoat.length>0){
												for(var ctc=0; ctc<topcoat.length; ctc++) {
													var curTopCoat = topcoat[ctc];
													if(curSheen.default == 1){
														if(curTopCoat.default == 1){
															productOptions+='<option style="display:block;" selected data-isdefault="'+curTopCoat.default+'" class="sheen_'+curSheen.sheen_id+'" value="'+curTopCoat.product_id+'" >'+curTopCoat.name+'</option>';
														}
														else{
															productOptions+='<option style="display:block;" data-isdefault="'+curTopCoat.default+'" class="sheen_'+curSheen.sheen_id+'" value="'+curTopCoat.product_id+'" >'+curTopCoat.name+'</option>';
														}
													}
													else{
														productOptions+='<option style="display:none;" data-isdefault="'+curTopCoat.default+'" class="sheen_'+curSheen.sheen_id+'" value="'+curTopCoat.product_id+'" >'+curTopCoat.name+'</option>';
													}
												}
											}
										}
									}

									groupStr+=
									'<div class="job_default_details" data-id="'+gk+'">'+
										'<h3 class="default_heading"><span><a href="javascript:void(0)" data-toggle="modal" data-target="#colorCeleings" data-dismiss="modal"><img src="'+siteBaseUrl+'/image/coloricon.png" alt=""></a></span>'+curComp.name+'</h3>'+
										'<div class="fill_default_form">'+
											'<div class="form-group small">'+
												'<input name="component['+gk+'][compId]" type="hidden" value="'+gk+'">'+
												'<input name="component['+gk+'][color]" type="text" value="" placeholder="Colour Name" class="form-control colour-name">'+
											'</div>'+
											'<div class="form-group small">'+
												'<select name="component['+gk+'][sheen]" id="sheen_'+gk+'" data-changetype="2" data-group="'+gk+'" class="sheen sheendd changetype" data-style="btn-new" tabindex="-98">'+
												sheenOptions+
												'</select>'+
											'</div>'+
											'<div class="form-group long">'+
												'<select name="component['+gk+'][product]" id="product_'+gk+'" data-changetype="2" data-group="'+gk+'" class="Product changetype" data-style="btn-new" tabindex="-98">'+
												productOptions+
												'</select>'+
											'</div>'+
										'</div>'+
										'<div class="fill_default_form">'+
											'<div class="form-group small">'+
												'<select name="component['+gk+'][strength]" class="strength changetype" data-changetype="2" data-style="btn-new" tabindex="-98">'+
													'<option value="1">One</option>'+
													'<option value="2">Two</option>'+
													'<option value="3">Three</option>'+
													'<option value="4">Four</option>'+
												'</select>'+
											'</div>'+
											'<div class="form-group small">'+
												'<div class="form-group checkbox_div">'+
													'<label class="checkbox_label">Undercoat'+
														'<input name="component['+gk+'][underChecks]" data-group="'+gk+'" data-changetype="2" class="underChecks changetype" id="undercheck_'+gk+'" type="checkbox">'+
														'<span class="checkmark"></span>'+
													'</label>'+
												'</div>'+
											'</div>'+
											'<div class="form-group long" style="display:none;">'+
												'<select name="component['+gk+'][undercoat]" id="undercoatdd_'+gk+'" data-changetype="2" class="undercoat changetype" data-style="btn-new" tabindex="-98">'+
													'<option>Select Undercoat</option>'+
													undercoatOptions+
												'</select>'+
											'</div>'+
										'</div>'+
									'</div>';
								}
							}
							$("#blankDefaultPane").hide();
							//console.log(groupStr);
							$("#mCSB_1_container").html(groupStr);
							$("#paintDefaultMain").show();
						}
					}
					
				},
					error: function () {
					alert("Something went wrong");
				}
			});


		}
	});

	$(document).on('change','#roomType',function(){
		var roomType = $('#roomType').val();
		var quote_type = $('#quote_type').val();
		if(roomType!=""){
			
			url = siteBaseUrl+'/quote/get-room-components';
			$.ajax({
				url: url,
				type: 'get',
				data: {'roomType':roomType,'quote_type':quote_type},
				dataType:'JSON',
				success: function (response) {
					$('#custom_room_name').val("");
					roomCompList = response.roomCompList;
					roomComp = response.roomComp;
					roomCompDetail = response.roomCompDetail;
					var str = "";
					var count = 0;
					if(roomCompList.length > 0){
						
							for (var ck in roomComp) {
								if(count%2 == 0){
									str+='<div class="row addroom_row">';
								}
									str+='<div class="col-6">'+
										'<div class="form-group">'+
											'<h3 class="default_heading"><span><img src="'+siteBaseUrl+'/image/default_search.png" alt=""></span> '+roomComp[ck]['name']+' <small>(Area: 0 m <sup>2</sup> )</small></h3>'+
											'<div style="display:none" id="ucCheck_'+ck+'" data-compid="'+ck+'"><input type="checkbox" class="ucIsSelected" name="uc_'+ck+'">U/C</div>'+
											'<div class="select_ceilings staircase">';
												for (var rcdk in roomCompDetail[ck]) {
													if(roomCompDetail[ck][rcdk]['price_method_id'] == 1 || roomCompDetail[ck][rcdk]['price_method_id'] == 3){
														str+='<label class="ceilint_container">'+
															'<input type="checkbox" class="selectiveCompOption" data-compid="'+ck+'" name="compDetail['+ck+'][selOption][]" value="'+roomCompDetail[ck][rcdk]['comp_type_id']+'">'+
															'<span class="checkbox_text">'+roomCompDetail[ck][rcdk]['component_type_name']+'</span>'+
														'</label>';
													}
													else{
														str+='<input data-compid="'+ck+'" name="compDetail['+ck+'][inputOption][]" type="text" value="" placeholder="'+roomCompDetail[ck][rcdk]['component_type_name']+'" class="form-control number_small  class="inputCompOption"">';
													}
												}
											str+='</div>'+
										'</div>'+
									'</div>';

								if(count%2 != 0){
									str+='</div>';
								}

								count++;
							}
						$("#room_comp_list").html(str);
					}
				},
				error: function (error) {

				}
			});

		}
	});


	$(document).on('click','.selectiveCompOption', function() {
		var $box = $(this);
		if ($box.is(":checked")) {
			var group = "input:checkbox[name='" + $box.attr("name") + "']";
			$(group).prop("checked", false);
			$box.prop("checked", true);
			$("#ucCheck_"+$box.data('compid')).show();
		}else {
			$box.prop("checked", false);
			$("#ucCheck_"+$box.data('compid')).hide();
		}
	});	

	$(document).on('click','.roomTabChange',function(e){
		var href = $(this).data('href');
		if($('.active.roomTabChange').data('href') == "detail" && href != "detail"){

			rl = $("#r_l").val();
			rw = $("#r_w").val();
			rh = $("#r_h").val();
			//console.log(rl,rw,rh);
			if(rl == '' || rw == '' || rh == ''){
				alert('Please fill in the Room Dimesions');
			}
			else{
				$('.roomTabChange').removeClass('active');
				$(this).addClass('active');
				$('.roomTabChangePane').hide();
				$('#'+href).show();	
			}
		}
		else{
			$('.roomTabChange').removeClass('active');
			$(this).addClass('active');
			$('.roomTabChangePane').hide();
			$('#'+href).show();
		}
	});
	
	$(document).on('click','.add_dim_btn',function(){

		var ts = Math.round((new Date()).getTime() / 1000);
		var str = "";
		str+='<div class="form-group dimension_row">'+
			'<input type="text" min="0" value="" placeholder="0" class="form-control number_input" name="l[]">'+
			'<input type="text" min="0" value="" placeholder="0" class="form-control number_input" name="w[]">'+
			'<span class="checkboxmainSpan"><input type="checkbox" class="l_w_check" name="check_'+ts+'" value="1">L</span><span class="checkboxmainSpan"><input type="checkbox" class="l_w_check" name="check_'+ts+'" value="2">W</span>'+
			'<span class="btn btn-transparent pull-right remove_dim_btn" style=""><i class="material-icons">-</i></span>'+
		'</div>';

		$('.room_dimesions').append(str);

		
	});

	$(document).on('click','.remove_dim_btn',function(){
		$(this).parent().remove();
	});

	$(document).on('click','.quoteDetailMainTabs',function(){
		var href = $(this).data('href');

		var panelNoArr = href.split('panel');
		var panelNo = panelNoArr[1];

		if(panelNo < 3){
			$('.quoteDetailMainTabs').removeClass('active');
			$(this).addClass('active');
			$('.quoteDetailMainPane').hide();
			$(href).show();
		}
		else{
			sb = $("#selbrand").val();
			st = $("#seltiers").val();
			dc = $("#defaultCoat").val();
			dpl = $("#defaultPrepLevel").val();
			error = 0;
			if(sb==""){
				msg = "Please choose Brand in the Paint Defaults";
				error = 1;
			}
			else if(st==""){
				msg = "Please choose Quality/Tier in the Paint Defaults";
				error = 1;
			}
			else if(dc==""){
				msg = "Please choose Caots in the Paint Defaults";
				error = 1;
			}
			else if(dpl==""){
				msg = "Please choose Prep. Level in the Paint Defaults";
				error = 1;
			}

			if (error == 1) {
				alert(msg);
				return false;
			}
			else{
				$('.quoteDetailMainTabs').removeClass('active');
				$(this).addClass('active');
				$('.quoteDetailMainPane').hide();
				console.log(href)
				if(href == "#panel4"){
					var cqid = $("#quote_id").val();
					var ctid = $("#quote_type").val();
					var data = '{"quote_id" : '+cqid+',"type_id" : '+ctid+'}';
					$.ajax({
						url: siteBaseUrl+'/webservice/get-room-details',
						type: 'post',
						data: {'json':data},
						success: function (response) {
							console.log(response);
							if(response.success == 1){
								
							}
						},
						error: function () {
							alert("Something went wrong");
						}
					});
				}

				$(href).show();
			}
		}
	});

	$(document).on("change","#room_name",function(){
		$('#custom_room_name').val("");
	});

	$(document).on("change","#custom_room_name",function(){
		$('#room_name').val("");
		$('#roomType>option:eq(0)').attr('selected', false);
		$('#roomType>option:eq(0)').attr('selected', true);
	});

	$("#saveRoom").click(function(){
		rl = $("#r_l").val();
		rw = $("#r_w").val();
		rh = $("#r_h").val();
		//console.log(rl,rw,rh);
		if(rl == '' || rw == '' || rh == ''){
			alert('Please fill in the Room Dimesions');
			return false;
		}

		room_id = 0;
		room_type_id = $("#roomType").val();
		room_name = $("#room_name").val();
		/*is_custom

		if(){

		}*/
		$(".selectiveCompOption").each(function(){

		});
	})

	$(document).on("click","#quote_status_button",function(){
		var quote_status = $("#quote_status").val();
		var quote_id = $("#quote_id").val();
		var url = siteBaseUrl+'/webservice/update-status';

		$.ajax({
			url: url,
			method:'POST',
			dataType:'JSON',
			data: {'quote_id':quote_id,'status':quote_status},
			success: function (response) {
				alert(response.message)
			},
			error: function () {
			  alert("Something went wrong");
		  }
	   });
	   return false;
	});
});



$('body').on('beforeSubmit', '#myid', function () {
     var form = $(this);
     //console.log("sdadas");
     // submit form
     $.ajax({
          url: form.attr('action'),
          type: 'post',
          data: form.serialize(),
          success: function (response) {
               // do something with response
               console.log(response);

               if(response == 1){
                 console.log('here11');

               }
          },
          error: function () {
            alert("Something went wrong");
        }
     });
     return false;
});

/************************ [for quote address] ****************************************/

  $(function() {
      /*
            var addresspickerMap = $( "#tbladdress-street1" ).addresspicker({
              updateCallback: showCallback,
              mapOptions: {
                zoom: 4,
                center: new google.maps.LatLng(-34, 138),
                scrollwheel: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP
              },
              elements: {
                map:      "#map",
                lat:      "#lat",
                lng:      "#lng",
                street_number: '#street_number',
                route: '#route',
                locality: '#tbladdress-suburb',
                sublocality: '#sublocality',
                administrative_area_level_3: '#administrative_area_level_3',
                administrative_area_level_2: '#administrative_area_level_2',
                administrative_area_level_1: '#tbladdress-state_id',
                country:  '#country',
                postal_code: '#tbladdress-postal_code',
                type:    '#type'
              }
            });

            var gmarker = addresspickerMap.addresspicker( "marker");
            gmarker.setVisible(true);
            addresspickerMap.addresspicker( "updatePosition");


            $("#tbladdress-street1").addresspicker("option", "reverseGeocode", true);

            function showCallback(geocodeResult, parsedGeocodeResult){
              $('#callback_result').text(JSON.stringify(parsedGeocodeResult, null, 4));
            }
            // Update zoom field
            var map = $("#tbladdress-street1").addresspicker("map");
            google.maps.event.addListener(map, 'idle', function(){
              $('#zoom').val(map.getZoom());
            });
      */
  });


/************************ [for site address address] ****************************************/

	$(function() {
		/*
		    var addresspickerMap2 = $( "#site-address" ).addresspicker({
		      updateCallback: showCallback2,
		      mapOptions: {
		        zoom: 4,
		        center: new google.maps.LatLng(-34, 138),
		        scrollwheel: false,
		        mapTypeId: google.maps.MapTypeId.ROADMAP
		      },
		      elements: {
		        map:      "#map2",
		        locality: '#site-suburb',
		        administrative_area_level_1: '#site-state',
		        postal_code: '#site-postcode',
		      }
		    });

		    var gmarker2 = addresspickerMap2.addresspicker( "marker");
		    gmarker2.setVisible(true);
		    addresspickerMap2.addresspicker( "updatePosition");

		    $("#tbladdress-street1").addresspicker("option", "reverseGeocode", true);

		    function showCallback2(geocodeResult, parsedGeocodeResult){
		      $('#callback_result').text(JSON.stringify(parsedGeocodeResult, null, 4));
		    }
		    // Update zoom field
		    var map2 = $("#site-address").addresspicker("map");
		    google.maps.event.addListener(map2, 'idle', function(){
		      $('#zoom').val(map2.getZoom());
		    });
		*/
	});


$('body').on('beforeSubmit', '#myidcontact', function (event) {

    event.preventDefault();
    //do something
    $('.subm').prop('disabled', true);


    var formData = new FormData($('#myidcontact')[0]);
    var baseUrl = base_url + 'contact/';
    $.ajax({
        url: baseUrl,  //Server script to process data
        type: 'POST',

        // Form data
        data: formData,

        success: function(response) {
            console.log(response);

            if(response == 1){

                $('#filee').html('');
                $('#file').show();
                $('#file').parent().show();
                $('.subm').prop('disabled', false);

                $('#addContact').modal('hide');
                $('#addContact').find('form').trigger('reset');



                var url = base_url + 'contact/search/';
                $.ajax({
                  type: "POST",
                  url: url,
                  data: {
                      'search' : ''
                  },
                  success: function(resp){
                     //console.log(response);                   
                     var datta = $.parseJSON(resp);

                     if(datta.success == 1){
                        console.log('here12');
                        $('.contact_list').html('');           
                        $('.contact_list').html(datta.searchdata);

                     }
                  }
                });


            }

        },
        error: function(){
            alert('ERROR at PHP side!!');
        },

        //Options to tell jQuery not to process data or worry about content-type.
        cache: false,
        contentType: false,
        processData: false
    });

    return false;
});


/*****************[ search contacts using AJAX ]************************/


var searchRequest = null;

$(function () {
    var minlength = 0

    var baseUrl = base_url + 'contact/search/';

    $(".contact_search").keyup(function () {
        var that = this,
        value = $(this).val();

        //if (value.length >= minlength ) {
            if (searchRequest != null) 
                searchRequest.abort();
                searchRequest = $.ajax({
                    type: "GET",
                    url: baseUrl,
                    data: {
                        'search' : value
                    },
                    success: function(response){
                       //console.log(response);                   
                       var data = $.parseJSON(response);

                       if(data.success == 1){
                          console.log('here12');
                          $('.contact_list').html('');           
                          $('.contact_list').html(data.searchdata);
                       }
                    }
                });
        //}
    });
});


/************* get contact id on click of li ******************/

$(document).on('click', '.ciid', function(){
	var baseUrl = base_url + 'contact/data/';
	var cid = $(this).attr('cid');
	var datastring = {cid:cid};
	$.ajax({
		url: baseUrl,
		type: 'post',
		data: datastring,
		success: function (response) {
			// do something with response
			var data = $.parseJSON(response)     
			if(data.success == 1){
				$('.personal_div1').html('');
				$('.personal_div2').html('');
				$('.contact_quotes').html('');
				$('.comm_sec').html('');
				$('.invoices_list').html('');
				$('.appointment_list').html('');                  

				$('.personal_div1').html(data.profiledata);
				$('.personal_div2').html(data.quotedata);
				$('.contact_quotes').html(data.quotetabdata);
				$('.comm_sec').html(data.commdata);
				$('.invoices_list').html(data.invoicedata);
				$('.appointment_list').html(data.appointmentdata);
			}
		},
		error: function () {
			alert("Something went wrong");
		}
	});
});

/***********[ to reset form when  bootstrap modal is hidden or closed]**************/

$(document).on('hidden.bs.modal', '#addContact', function () {

    $(this).find('form').trigger('reset');
});




$(document).ready(function(){



   /* 
      comment:for modal pop up 
      name :ravikant 
      date :20181209

    */
    $(function(){
      $('#modalEventButton').click(function(){
        $('#modalevent').modal('show')
        .find('#modalEvent')
        .load ($(this).attr('value'));
       });
    });

    /*

      comment:to remove attribute tab index 
      name :ravikant 
      date :20181209

    */
    $("#modalEventButton").click(function(){
        $("#modalevent").removeAttr("tabindex");
    });


    /*

      comment:for searching the event
      name :ravikant 
      date :20181209

    */
    // $("#search").on("keyup", function(){
    //   $('.noAppointment').remove();
    //   var searchClient = $(this).val();
    //   //console.log(searchClient);
    //   searchClient = searchClient.toLowerCase();
    //   if(searchClient != ""){
    //       $('.tosearch').show();
    //       var flag=0;
    //       $(".contactName").each(function(){
    //           var thisHtml = $(this).html();
    //           thisHtml = thisHtml.toLowerCase();
    //           //console.log(thisHtml);
    //           if(thisHtml.indexOf(searchClient) < 0 ){
    //             $(this).parent().hide();
    //           }
    //           else
    //           {
    //             flag=1;
    //           }

    //       });
    //       if(flag ==0)
    //       {
    //         var fieldHtml="<li class='alert alert-danger noAppointment'>NO Appointments</li>";
    //         $('.SelectedEventlist').append(fieldHtml);
    //       }
    //   }
    //   else
    //   {
    //     $('.tosearch').show();
    //   }
    // }); 
})




/*
	comment:form validation and form submission
	name :ravikant 
	date :20181209
*/

function validateData()
{
	var name=document.getElementById('userDataSelect').value;
	var type =document.getElementById('appointmentTypeID').value;
	var member=document.getElementById('memberID').value;
	var from =document.getElementById('timepicker1').value;
	console.log(from);
	var to   =document.getElementById('timepicker2').value;
	var note =document.getElementById('noteId').value;

	var error = 0;
	var text;
	if(name=="")
	{
	error = 1;
	text = "Please Enter Customer Name";
	}
	else if(type=="")
	{
	error = 1;
	text = "Please Enter Appointment Type";
	}
	else if(member=="")
	{
	error = 1;
	text = "Please Select Team Member";
	}
	else if(from=="")
	{
	error = 1;
	text = "Please Select Appointment Start Time";          
	}
	else if(note=="")
	{
	error = 1;
	text = "Please Enter Notes";
	}
	else if(to=="")
	{
	error = 1;
	text = "Please Select Appointment End Time";
	}

	if(error == 1){
	swal({
		title: 'PAINTPAD',
		text: text,
		type: 'error',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'OK!'
	});  
	return false;
	}
	else
	{
		$('body').on('beforeSubmit', '#myCalender', function (e) {
		var pathname = window.location.href;       
		var form = $(this);
		$.ajax({
			url: form.attr('action'),
			type: 'post',
			data: form.serialize(),
			success: function (response) {
			if(response == 1){
				$("#modalevent").modal('hide');
				$("#w0").fullCalendar( 'refetchEvents' );
				//$.pjax.reload('#eventpjax' , {timeout : false});
			}
			}
		});
		e.stopImmediatePropagation();
		return false;
		});
	}
}


/*

	comment:form validation and form updation
	name :ravikant 
	date :20181209

*/


function validateForm()
{
	var name=document.getElementById('userDataSelectUpdate').value;
	var type =document.getElementById('appointmentTypeIDUpdate').value;
	var member=document.getElementById('memberIDUpdate').value;
	var from =document.getElementById('timepicker1Update').value;
	console.log(from);
	var to   =document.getElementById('timepicker2Update').value;
	var note =document.getElementById('noteIdUpdate').value;

	var error = 0;
	var text;
	if(name=="")
	{
	error = 1;
	text = "Please Enter Customer Name";
	}
	else if(type=="")
	{
	error = 1;
	text = "Please Enter Appointment Type";
	}
	else if(member=="")
	{
	error = 1;
	text = "Please Select Team Member";
	}
	else if(from=="")
	{
	error = 1;
	text = "Please Select Appointment Start Time";          
	}
	else if(note=="")
	{
	error = 1;
	text = "Please Enter Notes";
	}
	else if(to=="")
	{
	error = 1;
	text = "Please Select Appointment End Time";
	}

	if(error == 1){
	swal({
		title: 'PAINTPAD',
		text: text,
		type: 'error',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'OK!'
	});  
	return false;
	}
	else
	{
		$('body').on('beforeSubmit', '#myCalender2', function (e) {
		var pathname = window.location.href;       
		var form = $(this);
		$.ajax({
			url: form.attr('action'),
			type: 'post',
			data: form.serialize(),
			success: function (response) {
			if(response == 1){
				$("#modalevent").modal('hide');
				$("#w0").fullCalendar( 'refetchEvents' );
				//$.pjax.reload('#eventpjax' , {timeout : false});
			}
			}
		});
		e.stopImmediatePropagation();
		return false;
		});
	}

	
}  
