<?php
use yii\helpers\Url;

foreach($model as $model_contact)
{
	if($model_contact->image=="")
	{
		$model_contact->image="avtar.png";
	}
	echo "<li class='tosearch' data-id='".$model_contact->contact_id."'>";
	echo '<span class="cnt_img"><img src="'.Url::base().'/uploads/'.$model_contact->image.'" height="58px"	 width="58px" style="border-radius: 50%;"></span>';
	echo '<div class="contact_person_detail">';
	echo '<p class="contact_nameSearch">'.$model_contact->name.'</p>';
	echo '<small>'.$model_contact->phone.'</small>';
	echo '</div>';
	echo "</li>";
}
?>