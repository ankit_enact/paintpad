<?php
use app\models\TblAppointmentType;
use app\models\TblMembers;
use frontend\assets\ContactAsset;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
ContactAsset::register($this);
// use frontend\assets\DashboardAsset;
// DashboardAsset::register($this);
use yii\widgets\ActiveForm;
?>
<?php

$dat = [];
$dataSub = [];
foreach ($dataContact as $data) {
	$dat[$data->contact_id] = $data->name;
	$datSub[$data->contact_id] = $data->subscriber_id;
}

?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>

	#SHOWmap {
		/* height: 100%;*/
		width: 100%;
		height: 243px;
		background-color: grey;
		cursor:default !important;
	}
	html, body {
		height: 100%;
		margin: 0;
		padding: 0;
	}
	#floating-panel {
		position: absolute;
		top: 10px;
		left: 25%;
		z-index: 5;
		background-color: #fff;
		padding: 5px;
		border: 1px solid #999;
		text-align: center;
		font-family: 'Roboto','sans-serif';
		line-height: 30px;
		padding-left: 10px;
	}

	#map-canvas, #map-canvas-site, #static-map-canvas {
		height: 150px;
		margin: 0;
	}
	.centerMarker, .siteCenterMarker{
		position: absolute;
		background: url(<?=Url::base(true) . "/image/mapIcon.png";?>) no-repeat;
		top: 54%;
		left: 50%;
		z-index: 1;
		margin-left: -10px;
		margin-top: -34px;
		height: 34px;
		width: 20px;
		cursor: pointer;
	}
	.pac-container {
		z-index: 1050 !important;
	}

	.existing_client_list{
		max-height: 190px;
		overflow-y: scroll;
	}
	.existing_client_list li{
		cursor: pointer;
	}
	ul.existing_client_list img {
		height: 45px;
		width: 45px;
		border-radius: 50%;
		margin-right: 10px;
	}
	#client_image img {
		height: 60px;
		width: 60px;
		border-radius: 50%;
	}

	#google-map-overlay{
		height : 150px;
		width: 270px;
		background: transparent;
		position: absolute;
		top: 0px;
		left: 0px;
		z-index: 99;
	}

	#map-canvas-site-overlay {
		height : 150px;
		width: 270px;
		background: transparent;
		position: absolute;
		top: 0px;
		left: 0px;
		z-index: 0;
	}
	/*=== Naveen ====*/
	#InteriorQuote .modal-content {
		background: #f6fbff;
	}
	#InteriorQuote .modal-body {
		padding: 0px 0 0;
		background: #fff;
		margin-top: 30px;
	}
	.Detail_prt_div {
		width: 100%;
		margin-top: -18px;
	}
	.Detail_inner .nav.nav-tabs a.nav-link {
		padding: 3px 6px;
		display: inline-block;
	}
	.Detail_inner .nav.nav-tabs {
		float: left;
		width: 100%;
	}

	.quote_right_sec {
		float: right;
		width: 101%;
		padding: 60px 20px 20px 20px;
	}
	.quoteTr{
		cursor:pointer;
	}
	.mainDiv {
		height:750px;
	}
	.text {
		position: relative;
		font-size: 14px;
		color: black;
		width: 250px;
	}

	.text-concat {
		position: relative;
		display: inline-block;
		word-wrap: break-word;
		overflow: hidden;
		max-height: 1.6em;
		line-height: 0.2em;
		text-align:justify;
	}

	.text.ellipsis::after {
		/*content: "...";*/
		position: absolute;
		right: -12px;
		bottom: 4px;
	}
	.cnt_img img{
		border-radius: 50%;
	}

	.crop {
		white-space: nowrap;
		width: 12em;
		overflow: hidden;
		width: 400px;
	}
	.crop::after {
		content: "...";
	}
	#blah
	{
		height: 90px;
		width: 90px;
	}




	.tabContact.card {
		border: 0;
		box-shadow: none;
		width: 69% !important;
		float: right;
		margin-left: 1px;
	}

	.mCustomScrollbar
	{
		/*overflow-y: scroll;*/
		height: 700px;
	}

	/*#myModal a {
    display: none;
    }*/
    .contact_list li:hover, .contact_list li.activeContact {
    	background: #efefef;
    }
    .bodyViewer
    {
    	cursor: pointer;
    }
    .text-centerCustom {
    	text-align: center!important;
    }
    #addContact h3 {
    	float: left;
    	width: 90%;
    }
    #static-edit-quote-map-canvas {
    	height: 150px;
    	margin: 0;
    }
    #google-map-overlay-2{
    	height : 150px;
    	width: 100%;
    	background: transparent;
    	position: absolute;
    	top: 0px;
    	left: 0px;
    	z-index: 99;
    }
    #error_msg{
    	color: red;
    }
</style>
<?php
$this->registerCssFile(Yii::getAlias('@web') . '/frontend/web/css/style.css', ['depends' => [yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::getAlias('@web') . '/frontend/web/css/bootstrap-select.min.css', ['depends' => [yii\web\JqueryAsset::className()]]);
$this->title = "PaintPad||Contact";

$commonLat = 28.517957;
$commonLng = -81.36591199999999;

$commonAddress = "2000 S Mills Ave, Orlando, FL 32806, USA";
$commonStreet = "2000 South Mills Avenue";
$commonSuburb = "Orlando";
$commonState = "Florida";
$commonZip = "32806";

$con = [];
foreach ($model as $c) {
	$con[] = $c->contact_id;
	$first = reset($con);
}
// print_r($con);
//
$sid = Yii::$app->user->id;
$company_id = Yii::$app->user->identity->company_id;
//echo $sid;die;

if(isset($_GET['contact_id']) && !empty($_GET['contact_id'])){
	$first = $_GET['contact_id'];
}else{
	$first = $first;
}
if(isset($_GET['select']) && !empty($_GET['select'])){
	$tabSelector = $_GET['select'];
}else{
	$tabSelector = '';
}
?>
<div class="wrap">
	<input id="latlng" type="hidden" value="40.714224,-73.961452">
	<input id="submit" type="button" value="Reverse Geocode" style="display:none">
	<input type="hidden" id="detailsContact" value="<?php echo $first; ?>">
	<input type="hidden" id="tabSelector" value="<?php echo $tabSelector; ?>">
	<input type="hidden" id="subscriber_id" value="<?php echo $sid; ?>">
	<input type="hidden" id="quote_page" value="1">

	<div class="quote_detail_wrap">
		<div class="container">
			<div class="quote_detail_inner" data-company="">
				<!-- Nav tabs -->

				<div class="quote_left_fix text-centerCustom" >
					<input type="text" id="searchContactName" class="form-control searchfield" name="Search" value="" placeholder="Search">


					<div class="form-group search_div mCustomScrollbar" id="contactListMain" data-page="1">

						<ul class="contact_list" id="contact_list">

							<?php

							foreach ($model as $model_contact) {
								if ($model_contact->image == "") {
									$model_contact->image = "avtar.png";
								}
								echo "<li class='tosearch' data-id='" . $model_contact->contact_id . "'>";
								echo '<span class="cnt_img"><img src="' . Url::base() . '/uploads/' . $model_contact->image . '" height="58px"	 width="58px" style="border-radius: 50%;"></span>';
								echo '<div class="contact_person_detail">';
								echo '<p class="contact_nameSearch">' . $model_contact->name . '</p>';
								echo '<small>' . $model_contact->phone . '</small>';
								echo '</div>';
								echo "</li>";
							}

							?>


						</ul><!-- cotact_list -->


					</div><!-- search_div -->

					<a href="#" data-toggle="modal" class="add_contact text-right"><img src="<?php echo Url::base(); ?>/uploads/contact_blue.png"></a>
				</div><!-- quote_left_fix -->
				<div>
					<ul class="nav nav-tabs nav-justified contact_tabs_list">
						<li class="nav-item">
							<a class="nav-link <?php echo ($tabSelector == '') ? 'active' : '' ; ?>" data-toggle="tab" id="contact_detail" onclick="getContactData();" href="#detail" role="tab">Details</a>
						</li>
						<li class="nav-item">
							<a class="nav-link <?php echo ($tabSelector == 'quotes') ? 'active' : '' ; ?>" data-toggle="tab" id="contact_quotes" onclick="getContactDataQuotes();" href="#quotes" role="tab">Quotes</a>
						</li>
						<li class="nav-item">
							<a class="nav-link <?php echo ($tabSelector == 'communications') ? 'active' : '' ; ?>" data-toggle="tab" id="contact_communication" onclick="getContactCommunications();" href="#communication" role="tab">Communications</a>
						</li>
						<li class="nav-item">
							<a class="nav-link <?php echo ($tabSelector == 'invoices') ? 'active' : '' ; ?>" data-toggle="tab" id="contact_invoices" onclick="getContactInvoices();" href="#invoices" role="tab">Invoices</a>
						</li>
						<li class="nav-item">
							<a class="nav-link <?php echo ($tabSelector == 'appointments') ? 'active' : '' ; ?>" data-toggle="tab" id="contact_appointment" onclick="getContactAppointments();" href="#appointment" role="tab">Appointments</a>
						</li>
					</ul>
				</div>
				<div class="mainDiv">
					<!-- Tab panels -->
					<div class="tab-content card tabContact">
						<!--Panel 1-->
						<div class="tab-pane fade in <?php echo ($tabSelector == '') ? 'show active' : '' ; ?>" id="detail" role="tabpanel">


							<div class="quote_right_sec">
								<div class="contact_detail_heading">
									<h3>Personal Profile</h3>
									<a href="#" class="edit text-right updateContactIcon" data-toggle="modal" ><img src="<?php echo Url::base(); ?>/image/edit_new.png"></a>
								</div><!-- contact_detail_heading -->
								<div class="personal_deatil row">
									<div class="personal_inner left col text-centerCustom">
										<span class="personal_profile"><img src=""></span>
										<h4 class="contact_name"></h4>
										<h5 class="contact_email"></h5>
										<h5 class="contact_phn"></h5>
									</div><!-- personal_left -->
									<div class="personal_inner mid col">
										<h5>Address</h5>
									<!-- <input type="text" id="client_lat">
										<input type="text" id="client_lng"> -->
										<p class="addressClicked"></p>
									<!-- <div id="SHOWmap">
									</div> --><!-- MAP LOCATION -->
									<div class="map_div">
										<div id="SHOWmap"></div>
									</div>

								</div><!-- personal_left -->
								<div class="personal_inner right col">
									<ul class="personal_count">
										<li onclick="$('#contact_communication').trigger('click')">
											<h4 class="contactsIcon"><h4>
											<span><img src="<?php echo Url::base(); ?>/image/mail.png"></span>
										</li>
										<li onclick="$('#contact_invoices').trigger('click')">
											<h4 class="docsIcon"><h4>
											<span><img src="<?php echo Url::base(); ?>/image/Invoices_icon.png"></span>
										</li>
										<li onclick="$('#contact_quotes').trigger('click')">
											<h4 class="quotesIcon"><h4>
											<span><img src="<?php echo Url::base(); ?>/image/Quote_icon.png"></span>
										</li>
										<li onclick="$('#contact_appointment').trigger('click')">
											<h4 class="jssIcon"><h4>
											<span><img src="<?php echo Url::base(); ?>/image/calender_new.png"></span>
										</li>
										</ul><!-- personal_count -->
									</div><!-- personal_left -->
								</div><!-- personal_deatail -->
								<div class="contact_detail_heading">
									<h3>Quotes</h3>
									<a class="edit text-right" data-target="#smallShoes" data-toggle="modal"><img src="<?php echo Url::base(true); ?>/image/add_quote_con.png"></a>
								</div><!-- contact_detail_heading -->

								<div class="personal_deatil" id="details_Quotes_Show">
									<ul class="quotes_list">
										<li ><small class="detailsQuotesMonth"></small><h4 class="detailsQuotesDate"></h4></li>
										<li><span class="detailsQuotesId"></span><h3 class="contact_name"></h3></li>
										<li><p class="detailsQuotesdescription"></p></li>
										<li><div class="interior"><img src="<?php echo Url::base(true); ?>/image/quotes_Interior_icon.png"><span class="detailsQuotesType"></span></div></li>
										<li><div class="interior"><img src="<?php echo Url::base(true); ?>/image/quote_price-icon.png"><span class="detailsQuotesprice"></span></div></li>
										<li><div class="compt_Action"><img src="<?php echo Url::base(true); ?>/image/complet.png"><span></span></div></li>
									</ul><!-- quotes_list -->
								</div><!-- personal_detail -->
							</div><!-- quote_right_sec -->
									</div>
									<!--/.Panel 1-->
									<!--Panel 2-->
										<div class="tab-pane fade  <?php echo ($tabSelector == 'quotes') ? 'show active' : '' ; ?>" id="quotes" role="tabpanel">
										<div class="quote_right_sec contactQuoteSection">
											<h3>Quotes</h3>
											<ul class="list-inline">
												<li class="list-inline-item"><span class="">Date</span></li>
												<li class="list-inline-item"><span class="">Client</span></li>
												<li class="list-inline-item"><span class="">I/E</span></li>
												<li class="list-inline-item"><span class="">Price(Inc. GST)</span></li>
												<li class="list-inline-item"><span class="">Status</span></li>
											</ul>

											<a class="edit text-right" data-target="#smallShoes" data-toggle="modal"><img src="<?php echo Url::base(true); ?>/image/add_quote_con.png"></a>

											<div class="mCustomScrollbar scrollbarqoute">
				
												<div class="contactPageQuote contact_quotes" id="contact_quotes_main">


													
													</div><!-- quote_right_sec -->
												</div>
											</div>
										</div>
										<!--/.Panel 2-->
										<!--Panel 3-->
										<div class="tab-pane fade  <?php echo ($tabSelector == 'communications') ? 'show active' : '' ; ?>" id="communication" role="tabpanel">


											<div class="mail_box">
												<div><a class="pull-right createCommunications add_communication" href="#" data-toggle="modal" data-target="#add_communication"><img src="<?php echo Url::base(); ?>/image/add_room.png" alt=""></a></div>
												<!-- <div><h1 class='pull-right createCommunications'>Create</h1></div> -->

												<div class="quote_right_sec communicationListing">
													
												</div>

												<!-- mail_box -->
											</div><!-- quote_right_sec -->
										</div><!--/.Panel 3-->
										<div class="tab-pane fade  <?php echo ($tabSelector == 'invoices') ? 'show active' : '' ; ?>" id="invoices" role="tabpanel">

											<div class="quote_right_sec">
												<div class="appointment_box">
													<table class="invoice_table table table-borderless table-condensed table-hover">
														<thead class="invoicehead" id="pagevalAllinvoices" data-value="0">
															<tr>
																<th>Invoice Id</th>
																<th>Quotes</th>
																<th>Description</th>
																<th>%</th>
																<th>Amount</th>
																<th>Paid</th>
																<th>Date</th>
															</tr>
														</thead>
														<tbody class="invoiceBody mCustomScrollbar" id="invoiceBody">
															
														</tbody>
													</table>
												</div><!-- appointment_box -->
											</div><!-- quote_right_sec -->
										</div><!--/.Panel 4-->
										<div class="tab-pane fade  <?php echo ($tabSelector == 'appointments') ? 'show active' : '' ; ?>" id="appointment" role="tabpanel">

											<div class="quote_right_sec">
												<div class="appointment_box">
													<div class="container appointmentViewer">
														<a href="javascript:void(0)" class="addAppointment"><img src="<?php echo Url::base(); ?>/image/add_quote.png" ></a>
														
													</div><!-- container -->
												</div><!-- appointment_box -->
											</div><!-- quote_right_sec -->
										</div><!--/.Panel 5-->
									</div>
								</div><!-- tab-content-top -->
							</div><!-- quote_detail_inner -->
						</div><!-- container -->

					</div><!-- quote_detail_wrap -->

				</div>
				<!-- wrap -->
				<!-- Modal -->
				<div class="modal fade" id="addContact" tabindex="-1" role="dialog" aria-labelledby="modalLabelSmall" aria-hidden="true">
					<div class="modal-dialog modal-lg">
						<div class="modal-content">

							<div class="modal-header">

								<h3><img src="<?=Yii::$app->request->baseUrl;?>/image/newinteriorquote.png" alt=""><span class="quoteTypeText">CREATE CONTACT</span></h3>
								<div class="Interior_rht_headerIcons">

									<a type="button" id="saveContactData" data-saveType="create">
										<span aria-hidden="true"><img src="<?=Yii::$app->request->baseUrl;?>/image/tick@3x.png" width="28px" height="28px"></span>
									</a>
									<a type="button"  data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true"><img src="<?=Yii::$app->request->baseUrl;?>/image/cross@3x.png" width="28px" height="28px"></span>
									</a>

								</div>
							</div>

							<div class="modal-body" style="width: 672px;">
								<div class="container">
									<div class="row">
										<div class="Detail_prt_div text-centerCustom">
										</div><!-- Detail_prt_div -->
										<div  class="tab-content">
											<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'id' => 'contactId']);?>
											<div class="profile_div"><img src="<?php echo Url::base(); ?>/image/avtar.png" alt="" style="pointer-events: none" id="blah" class='profilePicUpdater'><a  class="profilePicUpdater profile_edit"><img src="<?php echo Url::base(); ?>/image/edit_profile.png" alt="" style="pointer-events: none" >
												<input type="file" style="display:none;" name="image" class="fileInput" onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])">
											</a>
										</div>

										<div role="tabpanel" id="home" class="tab-pane in active">
											<input type="hidden" id="subscriber_idForm" name="subscriber_id" value ="<?=$sid;?>">
											<input type="hidden" name="contact_id" id="contact_idForm">
											<div class="row">
												<div class="col">
													<div class="form-group">
														<input type="text" name="name" id="contactName" data-tochange="site_contactName" class="form-control name getChange" Placeholder="Contact name">
													</div>
												</div>
												<div class="col">
													<div class="form-group">
														<input type="text" name="email" id="contactEmail" data-tochange="site_contactEmail" class="form-control email getChange" Placeholder="Contact email">
													</div>
												</div>
												<div class="col">
													<div class="form-group">
														<input type="text" name="phone" id="contactPhone" data-tochange="site_contactPhone" class="form-control phone getChange" Placeholder="Contact phone">
													</div>
												</div>
											</div>
											<div class="new_exist_client">
												<ul class="nav " role="tablist">
													<li class="nav-item"><h4>Contact Information</h4></li>
												</ul>
												<div class="tab-content">
													<div role="tabpanel" id="menu2" class="tab-pane in active">
														<div class="">
															<div class="row">
																<div class="col-8">
																	<div class="form-group">
																		<input type="hidden" name="lat" id="lat" value="<?=$commonLat;?>">
																		<input type="hidden" name="long" id="lng" value="<?=$commonLng;?>">
																		<input type="text" name="formatted_addr" id="address" data-tochange="search_address" class="form-control Address getChange" Placeholder="Address" value="<?=$commonAddress;?>">
																	</div>
																	<div class="row">
																		<div class="col">
																			<div class="form-group">
																				<input type="text" name="street1" id="street" data-tochange="site_street" class="form-control Suburb getChange" Placeholder="Street" value="<?=$commonStreet;?>">
																			</div>
																		</div>
																		<div class="col">
																			<div class="form-group">
																				<input type="text" name="suburb" id="suburb" data-tochange="site_suburb" class="form-control Suburb getChange" Placeholder="Suburb" value="<?=$commonSuburb;?>">
																			</div>
																		</div>
																	</div>
																	<div class="row">
																		<div class="col-6">
																			<div class="form-group">
																				<input type="text" name="state" id="state" data-tochange="site_state" class="form-control Suburb getChange" Placeholder="State" value="<?=$commonState;?>">
																			</div>
																		</div>
																		<div class="col-6">
																			<div class="form-group">
																				<input type="text" name="postal" id="zipCode" data-tochange="site_zipCode" class="form-control ZipCode getChange" Placeholder="ZipCode" value="<?=$commonZip;?>">
																			</div>
																		</div>
																	</div>
																</div>
																<div class="col-4">
																	<div class="map_div">
																		<div id="map-canvas"></div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<?php ActiveForm::end();?>
													<input type="hidden" name="csi" id="csi" value="<?=Yii::$app->user->id;?>">
													<input type="hidden" name="contactCreateUrl" id="contactCreateUrl" value="<?=Url::toRoute(['webservice/create-contact'], true);?>">
													<input type="hidden" name="quoteCreateUrl" id="quoteCreateUrl" value="<?=Url::toRoute(['webservice/create-quote'], true);?>">
													<input type="hidden" name="quoteUpdateUrl" id="quoteUpdateUrl" value="<?=Url::toRoute(['webservice/update-quote'], true);?>">
												</div>
												<div class="contactPopup_btmBtn"><button type="button" value="clear" name="clear" class="btn clearContactBtn pull-right">Clear</button></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>

			<!-- modal pop up for communication viewing-->
			<!-- Modal Window content -->
			<div id="myModalCommunicationViewer" class="modal fade" tabindex="-1" role="dialog" style="display:none" area-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button"  data-dismiss="modal" onclick="$('#myModalCommunicationViewer').modal('hide')">×</button>
						</div>
						<div class="modal-bodyCommunications">
						</div>
					</div>
				</div>
			</div>

			<!-- modal pop up for communication creating-->
			<!-- Modal Window content -->

<!-- <div id="myModal3" class="modal fade" tabindex="-1" role="dialog" style="display:none" area-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close communicationViewingClose" data-dismiss="modal">×</button>
			</div>
			<div class="modal-bodyCommunicationsCreate">
			</div>
		</div>
	</div>
</div> -->





<!-- modal pop up for communication creating-->
<!-- Modal Window content -->

<div id="createCommunicationsModal" class="modal fade" role="dialog" tabindex="-1" style="display:none" area-hidden="true">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div id="modal-bodyCommunicationsCreate" class="mCustomScrollbar" style="max-height: 550px; overflow: scroll; min-width: 300px">
				<p></p>
			</div>
			<!-- <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div> -->
		</div>

	</div>
</div>




<!-- modal pop up for appointment creating -->
<!-- Modal Window content -->
<div id="myModal2" class="modal fade" tabindex="-1" role="dialog" style="display:none" area-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close communicationViewingClose" data-dismiss="modal">×</button>
			</div>
			<div class="modal-bodyAppointment">



				<div class="event-form">
					<div class="NewEventForm">
						<div class="newEventLft_Form">
							<div class="form-group">
								<input type="hidden" class="form-control eventform_fields" name="TblAppointments[subscriber_id]" Placeholder="Customer Name" id="subs">
								<?php echo $form->field($modelAppointment, "contact_id")->widget(Select2::classname(), [
									'data' => $dat,
									'options' => ['placeholder' => 'Customer Name', 'tabindex' => false, 'class' => 'form-control eventform_fields', 'id' => 'userDataSelect'],
									'pluginOptions' => [
										'allowClear' => true,
									],
								]);
								?>
							</div><!-- form-control -->
							<div class="form-group">
								<div class="EventSelectBtns AppointSelect">

									<?=$form->field($modelAppointment, 'type')->dropDownList(ArrayHelper::map(TblAppointmentType::find()->all(), 'id', 'name'), ['prompt' => 'Appointment Type', 'class' => 'appointmentType', 'id' => 'appointmentTypeID'])->label(false);?>

								</div><!-- appointselect -->
								<div class="EventSelectBtns teamMember">

									<?=$form->field($modelAppointment, 'member_id')->dropDownList(ArrayHelper::map(TblMembers::find()->all(), 'id',
										function ($model2) {
											return $model2['f_name'] . ' ' . $model2['l_name'];
										}
									), ['prompt' => 'Team Member', 'class' => 'teamMemberSelect', 'id' => 'memberID'])->label(false);?>

								</div><!-- appointselect -->
							</div><!-- form-Group -->
							<div class="form-group">
								<div class="evnetFormLft_Calender">

									<input type="hidden" name="calender" id="getDate" >

									<div id="datepicker"></div>
								</div><!-- evnetFormLft_Calender -->

								<div class="EventSelectBtns timeSelect">
									<div class="form-group">


										<input type="text" name="dateSelect[From]" id="timepicker1" class="TimeSelect" placeholder="Time From">

									</div><!-- form-group -->
									<div class="form-group">


										<input type="text" name="dateSelect[to]" id="timepicker2" class="TimeSelect" placeholder="Time To">
									</div><!-- form-group -->
									<div class="form-group">
										<label>All Day</label>
										<div class="RadioMainDiv">
											<?php $modelAppointment->allDay = '0';?>
											<?=$form->field($modelAppointment, 'allDay')->radioList(array('1' => 'YES', '0' => 'NO'))->label(false);?>
										</div><!-- Radiomaindiv -->
									</div><!-- form-group -->
								</div><!-- EventSelectBtns -->
							</div><!-- form-group -->
							<div class="form-group">


								<?=$form->field($modelAppointment, 'note')->textarea(['rows' => '6', 'class' => 'Notefield', 'placeholder' => 'NOTES', 'id' => 'noteId'])->label(false);?>

							</div><!-- form-group -->
						</div><!-- mewEventlft_form -->
						<div class="newEvent_RhtForm">
							<div class="form-group">

								<?=$form->field($modelAppointment, 'formatted_addr')->textInput(['class' => 'form-control eventform_fields location', 'placeholder' => 'LOCATIONS', 'id' => 'pac-input'])->label(false);?>
							</div><!-- form-control -->
							<div class="map_div">
								<div id="map-canvas"></div>
							</div>
						</div><!-- map_eventDiv -->
					</div><!-- newEvent_RhtForm -->
				</div><!-- neweventform -->
			</div>
		</div>
	</div>
</div>
</div>

<!-- modal pop up for appointment creating closed-->
<!-- Modal Window content closed-->

<div class="modal fade" id="smallShoes" tabindex="-1" role="dialog" aria-labelledby="modalLabelSmall" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

			<div class="modal-body">
				<div class="container">
					<div class="row">
						<a href="#" class="col img-sel interior quoteTypeSelect" data-type="1" data-toggle="modal" data-target="#InteriorQuote" data-dismiss="modal"><img src="<?=Yii::$app->request->baseUrl;?>/image/Interior.png"><span class="cap-l"> Interior </span></a>
						<a href="#" class="col img-sel exterior quoteTypeSelect" data-type="2" data-toggle="modal" data-target="#InteriorQuote" data-dismiss="modal"><img src="<?=Yii::$app->request->baseUrl;?>/image/Exterior.png"><span class="cap-l"> Exterior </span></a>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>




<!-- The Interior modal -->
<div class="modal fade" id="InteriorQuote" tabindex="-1" role="dialog" aria-labelledby="modalLabelSmall" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">

			<div class="modal-header">

				<h3><img src="<?=Yii::$app->request->baseUrl;?>/image/newinteriorquote.png" alt=""><span class="quoteTypeText">New Interior Quote</span></h3>
				<div class="Interior_rht_headerIcons">
					<button type="button" id="saveQuote" data-saveType="create">
						<span aria-hidden="true"><img src="<?=Yii::$app->request->baseUrl;?>/image/tick@3x.png"></span>
					</button>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			</div>

			<div class="modal-body">
				<div class="container">
					<div class="row">
						<div class="Detail_prt_div text-center">
							<div class="Detail_inner">
								<ul class="nav nav-tabs" role="tablist">
									<li class="nav-item"><a class="nav-link active" role="tab" data-toggle="tab" href="#home"><h3><span><img src="<?=Yii::$app->request->baseUrl;?>/image/client_blk.png" class="clientIcon"><img src="<?=Yii::$app->request->baseUrl;?>/image/client_detail.png" class="clientIcon icon1"></span> <span class="headingTxt">Client Details</span></h3></a></li>
									<li class="nav-item"><a class="nav-link" role="tab" data-toggle="tab" href="#menu1"><h3><span><img src="<?=Yii::$app->request->baseUrl;?>/image/siteDetail_blk.png" class="clientIcon"><img src="<?=Yii::$app->request->baseUrl;?>/image/siteDetail.png" class="clientIcon icon1"></span> <span class="headingTxt">Site Details</span></h3></a></li>
								</ul>
							</div><!-- detail_inner -->
						</div><!-- Detail_prt_div -->
						<div  class="tab-content">
							<div role="tabpanel" id="home" class="tab-pane in active">
								<div class="client_detail_form">
									<div class="form-group">
										<label>Quote Description</label>
										<textarea class="description_input" id="description" placeholder="Please give your quote a brief description"></textarea>
									</div><!-- form-group -->
								</div><!-- client_detail_form -->
								<div class="new_exist_client">
									<ul class="nav nav-tabs" role="tablist">
										<li class="nav-item"><a class="nav-link active changeQuoteScreen" data-screen="new" role="tab" data-toggle="tab" href="#menu2"><h4>New Client</h4></a></li>
										<li class="nav-item"><a class="nav-link changeQuoteScreen" data-screen="existing" role="tab" data-toggle="tab" href="#menu3"><h4>Existing Client</h4></a></li>
									</ul>
									<div class="tab-content">
										<div role="tabpanel" id="menu2" class="tab-pane in active">
											<div class="container">
												<div class="row">
													<div class="col">
														<div class="form-group">
															<input type="text" name="clientName" id="clientName" data-tochange="site_clientName" class="form-control name getChange" placeholder="Client name">
														</div>
													</div>
													<div class="col">
														<div class="form-group">
															<input type="text" name="contactName" id="contactName" data-tochange="site_contactName" class="form-control name getChange" Placeholder="Contact name">
														</div>
													</div>
													<div class="col">
														<div class="form-group">
															<input type="text" name="contactEmail" id="contactEmail" data-tochange="site_contactEmail" class="form-control email getChange" Placeholder="Contact email">
														</div>
													</div>
													<div class="col">
														<div class="form-group">
															<input type="text" name="contactPhone" id="contactPhone" data-tochange="site_contactPhone" class="form-control phone getChange" Placeholder="Contact phone">
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-8">
														<div class="form-group">
															<input type="hidden" name="lat" id="lat" value="<?=$commonLat;?>">
															<input type="hidden" name="lng" id="lng" value="<?=$commonLng;?>">
															<input type="hidden" name="csi" id="csi" value="<?=Yii::$app->user->id;?>">
															<input type="hidden" name="contactCreateUrl" id="contactCreateUrl" value="<?=Url::toRoute(['webservice/create-contact'], true);?>">
															<input type="hidden" name="contactUpdateUrl" id="contactUpdateUrl" value="<?=Url::toRoute(['webservice/update-contact'], true);?>">
															<input type="hidden" name="quoteCreateUrl" id="quoteCreateUrl" value="<?=Url::toRoute(['webservice/create-quote'], true);?>">
															<input type="hidden" name="quoteUpdateUrl" id="quoteUpdateUrl" value="<?=Url::toRoute(['webservice/update-quote'], true);?>">
															<input type="text" name="address" id="address" data-tochange="search_address" class="form-control Address getChange" Placeholder="Address" value="<?=$commonAddress;?>">
														</div>
														<div class="row">
															<div class="col">
																<div class="form-group">
																	<input type="text" name="street" id="street" data-tochange="site_street" class="form-control Suburb getChange" Placeholder="Street" value="<?=$commonStreet;?>">
																</div>
															</div>
															<div class="col">
																<div class="form-group">
																	<input type="text" name="suburb" id="suburb" data-tochange="site_suburb" class="form-control Suburb getChange" Placeholder="Suburb" value="<?=$commonSuburb;?>">
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-6">
																<div class="form-group">
																	<input type="text" name="state" id="state" data-tochange="site_state" class="form-control Suburb getChange" Placeholder="State" value="<?=$commonState;?>">
																</div>
															</div>
															<div class="col-6">
																<div class="form-group">
																	<input type="text" name="zipCode" id="zipCode" data-tochange="site_zipCode" class="form-control ZipCode getChange" Placeholder="ZipCode" value="<?=$commonZip;?>">
																</div>
															</div>
														</div>
													</div>
													<div class="col-4">
														<div class="map_div">
															<div id="map-canvas"></div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div role="tabpanel" id="menu3" class="tab-pane">
											<div class="container">
												<div class="row">
													<div class="col">
														<div class="form-group">
															<input type="text" id="search_existing_client" name="Search" class="form-control search" placeholder="Search">
														</div>
														<ul class="existing_client_list">
															<?php
															$default_detail = [];
															if (count($contacts['data'])) {
																if (array_key_exists(0, $contacts['data']['contact'])) {
																	$default_detail['name'] = $contacts['data']['contact'][0]['name'];
																	$default_detail['phone'] = $contacts['data']['contact'][0]['phone'];
																	$default_detail['email'] = $contacts['data']['contact'][0]['email'];
																	$default_detail['image'] = $contacts['data']['contact'][0]['image'];
																	$default_detail['formatted_addr'] = $contacts['data']['contact'][0]['address']['formatted_addr'];
																	$default_detail['lat'] = $contacts['data']['contact'][0]['address']['lat'];
																	$default_detail['lng'] = $contacts['data']['contact'][0]['address']['lng'];
																}
															}

															foreach ($contacts['data']['contact'] as $key => $contact) {

																echo '<li class="contact_li" id="contact_' . $contact['contact_id'] . '" data-id="' . $contact['contact_id'] . '" data-name="' . $contact['name'] . '" data-phone="' . $contact['phone'] . '" data-image="' . $contact['image'] . '" data-email="' . $contact['email'] . '" data-street1="' . $contact['address']['street1'] . '" data-street2="' . $contact['address']['street2'] . '" data-suburb="' . $contact['address']['suburb'] . '" data-state="' . $contact['address']['state'] . '" data-postal="' . $contact['address']['postal'] . '" data-country="' . $contact['address']['country'] . '" data-formatted_addr="' . $contact['address']['formatted_addr'] . '" data-lat="' . $contact['address']['lat'] . '" data-lng="' . $contact['address']['lng'] . '" >';
																echo '<img src="' . $contact['image'] . '" />';
																echo '<div class="existing_client_detail"> <h3 class="client_name_list">' . $contact['name'] . '</h3><small class="client_phone_list">' . $contact['phone'] . '</small></div>';
																echo '</li>';
															}

															?>
														</ul><!-- existing_client_list -->
													</div><!-- col -->
													<div class="col border-leftright">
														<div class="client_Detail text-center">
															<input type="hidden" id="client_lat" value="<?=$default_detail['lat'];?>">
															<input type="hidden" id="client_lng" value="<?=$default_detail['lng'];?>">
															<input type="hidden" id="selected_client" value="">
															<div id="client_image" class="profile_img"><img src="<?=$default_detail['image'];?>" alt=""></div>
															<div id="client_name" class="client_name"><?=$default_detail['name'];?></div>
															<div id="client_email" class="client_email"><?=$default_detail['email'];?><br/><?=$default_detail['phone'];?></div>
														</div><!-- client_detail -->
													</div><!-- col -->
													<div class="col address">
														<h4>Address</h4>
														<p id="client_address"><?=$default_detail['formatted_addr'];?></p>
														<div class="map_div">
															<div id="static-map-canvas"></div>
															<div id="google-map-overlay"></div>
															<span class="map-pin"><img src="<?=Yii::$app->request->baseUrl;?>/image/location.png" alt=""></span>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div role="tabpanel" id="menu1" class="tab-pane fade">
								<div class="container">
									<div class="row existing_menu">
										<div class="col-12">
											<label class="clientDetail_Check">Clients details Same
												<input type="checkbox" id="client_details_same" checked="checked">
												<span class="checkmark"></span>
											</label>
										</div>
										<div class="col-8 ">
											<div class="row">
												<div class="col">
													<div class="form-group">
														<input type="hidden" name="site_lat" id="site_lat" value="<?=$commonLat;?>">
														<input type="hidden" name="site_lng" id="site_lng" value="<?=$commonLng;?>">
														<input type="text" id="search_address" name="search_address" class="form-control search_address" Placeholder="Search Address" value="<?=$commonAddress;?>">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col">
													<div class="form-group">
														<input type="text" id="site_street" name="site_street" class="form-control Street" Placeholder="Street" value="<?=$commonStreet;?>">
													</div>
												</div>
												<div class="col">
													<div class="form-group">
														<input type="text" id="site_suburb" name="site_suburb" class="form-control Suburb" Placeholder="Suburb" value="<?=$commonSuburb;?>">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-6">
													<div class="form-group">
														<input type="text" id="site_state" name="State" class="form-control Suburb" Placeholder="State" value="<?=$commonState;?>">
													</div>
												</div>
												<div class="col-6">
													<div class="form-group">
														<input type="text" id="site_zipCode" name="Postcode" class="form-control Postcode" Placeholder="Postcode" value="<?=$commonZip;?>">
													</div>
												</div>
											</div>
										</div>
										<div class="col-4">
											<div class="map_div">
												<div id="map-canvas-site"></div>
												<div id="map-canvas-site-overlay"></div>
											</div>
										</div>
									</div>
									<div class="row"><div class="col text-right"><button type="submit" class="clear btn-primary">Clear</button></div></div>
									<div class="row existing_client_detail">
										<div class="col">
											<div class="form-group">
												<input type="text" id="site_contactName" name="Name" class="form-control name" Placeholder="Contact name">
											</div>
										</div>
										<div class="col">
											<div class="form-group">
												<input type="text" id="site_contactEmail" name="Email" class="form-control email" Placeholder="Contact email">
											</div>
										</div>
										<div class="col">
											<div class="form-group">
												<input type="text" id="site_contactPhone" name="Name" class="form-control phone" Placeholder="Contact phone">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>










<?php
$this->registerJs('
	var subsArray = ' . json_encode($datSub) . ';
	//console.log("subsArray-------->", subsArray);
	$(document).on("change","#userDataSelect", function() {
		//console.log("userDataSelect------->", $("#userDataSelect").find(":selected").attr("value"));
		var selectedSubId = subsArray[$("#userDataSelect").find(":selected").attr("value")];
		//console.log("selectedSubId------>", selectedSubId);
		$("#subs").val(selectedSubId)
		})
		');

		?>
		<?php

		$this->registerJsFile(Yii::getAlias('@web') . '/frontend/web/js/timepicki.js', ['depends' => [yii\web\JqueryAsset::className()]]);
		$this->registerCssFile(Yii::getAlias('@web') . '/frontend/web/css/timepicki.css', ['depends' => [yii\web\JqueryAsset::className()]]);

		?>
		<script>
			$(document).ready(function(){
				$('#timepicker1').timepicki();
				$('#timepicker2').timepicki();
			});
		</script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
		<?php
//$this->registerJsFile(Yii::getAlias('@web').'/frontend/web/js/jquery2.js',['depends' => [yii\web\JqueryAsset::className()]]);
		$this->registerJsFile(Yii::getAlias('@web') . '/frontend/web/js/bootstrap.min.js', ['depends' => [yii\web\JqueryAsset::className()]]);
		$this->registerJsFile(Yii::getAlias('@web') . '/frontend/web/js/bootstrap-select.min.js', ['depends' => [yii\web\JqueryAsset::className()]]);
// $this->registerJsFile(Yii::getAlias('@web').'/frontend/web/js/jquery.datepick.js',['depends' => [yii\web\JqueryAsset::className()]]);
//$this->registerCssFile(Yii::getAlias('@web').'/frontend/web/css/clndr.css',['depends' => [yii\web\JqueryAsset::className()]]);
//$this->registerCssFile(Yii::getAlias('@web').'/frontend/web/css/jquery.datepick.css',['depends' => [yii\web\JqueryAsset::className()]])
		?>
		<script type="text/javascript">


			$(document).ready(function(){

				$(".fa-bars").click(function() {
					$(".menu").removeClass('menuClose');
					$(".menu").addClass('menuOpen');

					$(".mainClose").addClass('mainOpen');
					$(".mainOpen").removeClass('mainClose');

					$(".fa-bars").hide(500);
					$(".fa-times").show(500);
				});

				$(".fa-times").click(function() {
					$(".menu").addClass('menuClose');
					$(".menu").removeClass('menuOpen');

					$(".mainOpen").addClass('mainClose');
					$(".mainClose").removeClass('mainOpen');

					$(".fa-times").hide(500);
					$(".fa-bars").show(500);

				});
			});


///// side menu where all the contacts are listed
$(document).ready(function(){
	$("#searchContactName").on("keyup", function(){
		$('.noAppointment').remove();
		var searchClient = $(this).val();
		searchClient = searchClient.toLowerCase();
		if(searchClient != ""){
			$('.tosearch').show();
			var flag=0;
			$(".contact_nameSearch").each(function(){
				var thisHtml = $(this).html();
				thisHtml = thisHtml.toLowerCase();
				if(thisHtml.indexOf(searchClient) < 0 ){
					$(this).parent().parent().hide();
				}
				else
				{
					flag=1;
				}
			});
			if(flag ==0)
			{
				var fieldHtml="<li class='alert alert-danger noAppointment'>NO Contacts</li>";
				$('.contact_list').append(fieldHtml);
			}
		}
		else
		{
			$('.tosearch').show();
		}
	});
})

</script>

<script>
	function quotesGet(quotesListing,a,b){
		var i=0;
		var base_url = "<?php echo Url::base(true); ?>";
		for(var i=0; i<b; i++)
		{
			var item = quotesListing[i];
			var date=item.date;
			var status  = item.status;
			var dateObj = new Date(date*1000)
			var month = dateObj.getMonth() + 1; //months from 1-12
			var monthName = moment.months(month - 1);
			var monthName = moment.months(month - 1);
			var day = dateObj.getDate();
			var types = item.type;
			if(types == 1)
			{
				types = 'Interiors';
				var icons =	'quotes_Interior_icon.png';
			}
			else if(types == 2)
			{
				types = 'Exteriors';
				var icons =	'quotes_exterior_icon.png';
			}
			statusStr 		= quoteStatusArr[status]['status'];
			statusIcon		= quoteStatusArr[status]['statusIcon'];

			fieldHtmlQuote='<ul class="quotes_list quoteTr" data-href="'+base_url+'/quote/view/'+item.quote_id+'"><li class="quoteTr" ><small class="detailsQuotesMonth">'+monthName+'</small><h4 class="detailsQuotesDate">'+day+'</h4></li><li><span class="detailsQuotesId">'+item.quote_id+'</span><h3 class="contact_name">'+item.contact.name+'</h3><small>'+item.contact.phone+'</small></li><li><p class="detailsQuotesdescription">'+item.description+'</p></li><li><div class="interior"><img src="'+base_url+'/image/'+icons+'"><span class="detailsQuotesType">'+types+'</span></div></li><li><div class="interior"><img src="'+base_url+'/image/quote_price-icon.png"><h3 class="detailsQuotesprice">$'+item.price+'</h3></div></li><li><div class="compt_Action"><img src="'+base_url+'/image/'+statusIcon+'" height="47px" width="50px"><h3 style="color:#b8cabb;">'+statusStr+'</h3></div></li></ul>'
			$("#contact_quotes_main").append(fieldHtmlQuote);
		}
	}

	function quotesGetContact(quotesListing,a,b,name){
		var i=0;
		//while(item = quotesListing[i++]){
		var base_url = "<?php echo Url::base(true); ?>";
		for(var i=0; i<=1; i++)
		{
			if(quotesListing.length==0)
			{
				fieldHtmlQuotes='<ul><small>No Quotes exist for '+name+'</small></ul>'
				$("#details_Quotes_Show").append(fieldHtmlQuotes);
				return false;
			}
			var item = quotesListing[i];
			// console.log(item);
			if(typeof item !== 'undefined'){
				var date=item.date;


				var status  = item.status;
				var dateObj = new Date(date*1000)
				//console.log(dateObj);
				var month = dateObj.getMonth() + 1; //months from 1-12
				var monthName = moment.months(month - 1);
				//console.log(monthName);
				var monthName = moment.months(month - 1);
				var day = dateObj.getDate();
				//console.log(day);
				var types = item.type;
				if(types == 1 || types=="1")
				{
					types = 'Interiors';
					var icons =	'quotes_Interior_icon.png';
				}
				else if(types == 2 || types=="2")
				{
					types = 'Exteriors';
					var icons =	'quotes_exterior_icon.png';
				}
				if(item.price==0)
				{
					item.price = "0.00";
				}

				statusStr 			= quoteStatusArr[status]['status'];
				statusIcon		= quoteStatusArr[status]['statusIcon'];

				fieldHtmlQuotes='<ul class="quotes_list quoteTr" data-href="'+base_url+'/quote/view/'+item.quote_id+'"><li class="quoteTr" ><small class="detailsQuotesMonth">'+monthName+'</small><h4 class="detailsQuotesDate">'+day+'</h4></li><li><span class="detailsQuotesId">'+item.quote_id+'</span><h3 class="contact_name">'+item.contact.name+'</h3><small>'+item.contact.phone+'</small></li><li><p class="detailsQuotesdescription">'+item.description+'</p></li><li><div class="interior"><img src="'+base_url+'/image/'+icons+'"><h3 class="detailsQuotesType">'+types+'</h3></div></li><li><div class="interior"><img src="'+base_url+'/image/quote_price-icon.png"><h3 class="detailsQuotesprice">$'+item.price+'</h3></div></li><li><div class="compt_Action"><img src="'+base_url+'/image/'+statusIcon+'" height="47px" width="50px"><h3 style="color:#b8cabb;">'+statusStr+'</h3></div></li></ul>'
				$("#details_Quotes_Show").append(fieldHtmlQuotes);
			}
		}
	}
	var base_url=siteBaseUrl+'/';
	var quoteStatusArr =
	{
		1:{'status':'Pending','statusIcon':'quote_pending_active.png'},
		2:{'status':'In-progress','statusIcon':'quote_progress_active.png'},
		3:{'status':'Completed','statusIcon':'completed@3x.png'},
		4:{'status':'Accepted','statusIcon':'accepted@3x.png'},
		5:{'status':'Declined','statusIcon':'declined@3x.png'},
		6:{'status':'New','statusIcon':'new@3x.png'},
		7:{'status':'Submitted','statusIcon':'offered@3x.png'},
		8:{'status':'Open','statusIcon':'open@3x.png'},
	} ;

	function getContactData(){
		$(function () {

			var id = $('.activeContact').data('id');
			var sid='<?php echo $sid; ?>';
			$.getJSON(
				base_url+"webservice/contact-details?subscriber_id="+sid+"&contact_id="+id,
				function (data) {
					// console.log(data.data.contact);

					$("#details_Quotes_Show").empty();
					if(base_url+"uploads/" == data.data.contact.image){
						data.data.contact.image = base_url+"uploads/avtar.png"
					}
					var quotesListing = data.data.quotes;

					quotesGetContact(quotesListing,0,2,data.data.contact.name);

					var lat=data.data.contact.lat;
					var lng=data.data.contact.lng;

					$('#client_lat').val(lat);
					$('#client_lng').val(lng);

					$("#latlng").val(lat+','+lng)
					$(".contact_name" ).empty();
					$(".contact_email").empty();
					$(".contact_phn").empty();
					$(".personal_profile").empty();
					$(".contactsIcon").empty();
					$(".docsIcon").empty();
					$(".jssIcon").empty();
					$(".quotesIcon").empty();
					$(".addressClicked").empty();
					$(".contact_name" ).append(data.data.contact.name);
					$(".contact_email" ).append(data.data.contact.email);
					$(".contact_phn" ).append(data.data.contact.phone);
					$(".personal_profile" ).append("<img src='"+data.data.contact.image+"' height='80px' width='80px' style='border-radius:58px'>");
					$(".contactsIcon" ).append(data.data.counts.contacts);
					$(".docsIcon" ).append(data.data.counts.docs);
					$(".jssIcon" ).append(data.data.counts.jss);
					$(".quotesIcon" ).append(data.data.counts.quotes);
					$(".addressClicked").append(data.data.contact.formatted_addr);
					$('.updateContactIcon').attr('contactIdUpdate',data.data.contact.contact_id);
					//alert(lat);
					//setCoords(lat,lng)
					// initMap(lat,lng)
					// function initMap(lat,lng) {
					// 	var uluru = {lat:lat,lng:lng};
					// 	var map = new google.maps.Map(
					// 	document.getElementById('SHOWmap'), {zoom: 12, center: uluru,draggable: false, disableDefaultUI: true,mapTypeId: google.maps.MapTypeId.TERRAIN  });
					// 	var marker = new google.maps.Marker({position: uluru, map: map});
					// }
					initMapContact(lat,lng);
				});
		});

	}


	// by ankit
	$(document).ready(function(){
		var id = $('#detailsContact').val();
		var thi = $('.tosearch[data-id="'+id+'"]');
		thi.removeAttr('class');
		thi.attr('class','tosearch activeContact')
		var sid='<?php echo $sid; ?>';
		// if come from any other page
		var selector = $('#tabSelector').val();
		switch (selector) {
		  case 'communications':
		    getContactCommunications();
		    break;
		  case 'quotes':
		    getContactDataQuotes();
		    break;
		  case 'invoices':
		    getContactInvoices();
		    break;
		  case 'appointments':
		    getContactAppointments();
		    break;
		  default:
		  getContactData();
		}
		
	});


	//console.log(base_url);
	$("ul").on("click",".tosearch", function(e){
		var id = $(this).attr('data-id');
		$('.tosearch').removeClass("activeContact")
		var sid='<?php echo $sid; ?>';
		$('#contact_detail').trigger('click');
		getContactData();
		$(this).addClass("activeContact");
		$('#contact_detail').trigger('click');
	});

	// $("#contact_appointment").on("click", function(e){
	// 	var id = $('.activeContact').data('id');
	// 	var sid='<?php echo $sid; ?>';
	// 	getContactAppointments(id,sid);
	// });
</script>
<!--
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC2Qg3ywv9Dhg-Ptjv-5fG3I3UEt0rLufQ"></script> -->

	<script>
		var base_url=siteBaseUrl+'/';
		function getContactDataQuotes(id,sid){
			$(function () {
				var id = $('.activeContact').data('id');
				var sid='<?php echo $sid; ?>';
				$.getJSON(
					base_url+"webservice/quotes?subscriber_id="+sid+"&contact_id="+id,
					function (data) {
						$('#contact_quotes_main').empty();
						var quotesListing = data.data.quotes;
						var length = data.data.quotes.length;
						quotesGet(quotesListing,1,length);
					});
			});
		}


		var contact =<?php echo json_encode($con); ?>;
		// $("ul").on("click",".tosearch", function(e){
		// 	var id = $(this).attr('data-id');
		// 	var sid='<?php echo $sid; ?>';
		// 	getContactDataQuotes(id,sid);
		// });

		// by ankit
		// $(document).ready(function(){
		// 	var id = $('#detailsContact').val();
		// 	var sid='<?php echo $sid; ?>';
	 //   	//var sid=1;
	 //   	getContactDataQuotes(id,sid);
	 //   });

	</script>

	<script>

		var base_url=siteBaseUrl+'/';
		function getContactCommunications(){
			$(function () {
				var id = $('.activeContact').data('id');
				var sid='<?php echo $sid; ?>';
				$.getJSON(
					base_url+"webservice/communication?subscriber_id="+sid+"&contact_id="+id,
					function (data) {
				//console.log(data.data.emails.length);
				$(".communicationListing").empty();

				$(".createCommunications").attr('data-id',id)
				//$(".communicationListing").append("<div></div>");
				if(data.data.emails.length>0)
				{
					var i=0;
					while(item = data.data.emails[i++]){
					//console.log(item);
					//console.log(item);
					var date=item.date;
					//console.log(date);
					var dateObj = new Date(date*1000)
					//console.log(dateObj);
					var month = dateObj.getMonth() + 1; //months from 1-12
					var monthName = moment.months(month - 1);
					//console.log(monthName);
					var day = dateObj.getDate();
					var year = dateObj.getFullYear();
					if (day.toString().length == 1) {
						day = "0" + day;
					}
					// <div class="mail_heading">'+day+" "+monthName+" "+year+'</div>
					var bodyItem =item.body;
					var res = bodyItem.substr(0,90);
					//console.log(res);
					var htmlCommunicationField ='<div class="mail_box"><div class="inner_mail_box"><h4 class="mail_sub">'+item.subject+'</h4><div class="form-group"><label>From</label><p>'+item.From+'</p></div><div class="form-group"><label>Date</label><p>Wed Jan 24 2018 11:45</p></div><div class="form-group"><label>To</label><p>info@paintpad.com.au</p></div><div class="form-group"><label>To</label><p>'+item.to+'</p></div><div class="mail-content-box "  body-item="'+item.comm_id+'">'+res+'...</div><span class="bodyViewerCommunication small pull-right viewCommunicationListing" body-item="'+item.comm_id+'" data-toggle="modal" >View more</span></div></div>';
					$(".communicationListing").append(htmlCommunicationField);
				}
			}
			else
			{
				$(".communicationListing").append("<div>No Communications</div>");
			}
		}
		);
			});
		};

		// $("ul").on("click",".tosearch", function(e){
		// 	var id = $(this).attr('data-id');
		// 	var sid='<?php echo $sid; ?>';
		// 	getContactCommunications(id,sid);

		// });

		// by ankit
		// $(document).ready(function(){
		// 	var id = $('#detailsContact').val();
		// 	var sid='<?php echo $sid; ?>';
		// 	getContactCommunications(id,sid);
		// });
	</script>

	<script>
		var base_url=siteBaseUrl+'/';
		function getContactInvoices() {
			// $('#loader').show();

			var id = $('.activeContact').data('id');
			var sid='<?php echo $sid; ?>';
			var page = $('#pagevalAllinvoices').data('val');
			var company_id = <?php echo $company_id; ?>;
		    var data = '{"company_id":"'+company_id+'", "contact_id" : '+id+', "page": "'+page+'", "filter":"" }';
			$.ajax({
				url: siteBaseUrl+'/webservice/get-all-invoices',
				type: 'post',
				data: {'json':data},
				success: function (response) {
					var response = JSON.parse(response);
					if(response.success == 1){
						$('.invoiceBody').empty();
						if(response.data.length != 0){
							$(".invoicehead").show();
							var str = '';
							$.each(response.data.payments, function(d,i){
								var part_payment = (i['part_payment'] % 1 === 0) ? i['part_payment'] : parseFloat(i['part_payment']).toFixed(2);
								var amount = (i['amount'] % 1 === 0) ? currencyFormat(i['amount']) : currencyFormat(parseFloat(i['amount']).toFixed(2));
							    var descp = (i['descp'] != '') ? i['descp'] : '-';
							    var payment_date = (i['payment_date'] != '') ? i['payment_date'] : '-';
								str = "<tr><td><span><a href='"+i['qb_invoice_pdf']+"' target='_blank'>"+i['qb_invoice_DocNumber']+"</a></span></td><td>"+i['quote_id']+"</td><td>"+descp+"</td><td>"+part_payment+"</td><td>"+amount+"</td><td>"+i['qb_invoice_status']+"</td><td>"+payment_date+"</td></tr>";
								$('.invoiceBody').append(str);

								page = page + 1;
								$('#pagevalAllinvoices').data('val',page);
								$('#pagevalAllinvoices').attr('data-val',page);
							});
						}else{
							$(".invoiceBody").append("<div>No Invoices</div>");
						}
						
					}else{
						alert(response.message);
					}
				},
				error: function () {
					alert("Something went wrong");
				}
			});

		};


	// 	$("ul").on("click",".tosearch", function(e){
	// 		var id = $(this).attr('data-id');
	// 		var sid='<?php echo $sid; ?>';
	// 	//var sid=1;
	// 	getContactInvoices(id,sid);
	// });
		// by ankit
		// $(document).ready(function(){
		// 	var id = $('#detailsContact').val();
		// 	var sid='<?php echo $sid; ?>';
		// 	//var sid=1;
		// 	getContactInvoices(id,sid);
		// });
	</script>

	<script type="text/javascript">
		$("div").on("click",".createCommunications", function(e){
			var sid='<?php echo $sid; ?>';
			var id = $(".createCommunications").attr("data-id");
			$.ajax({
				url: base_url+'/communications/create',
				headers: {'Quoteid': id,'SubscriberID':sid,'fromWeb':1},
				success: function (response) {
					$('#modal-bodyCommunicationsCreate').html(response);
				}
			});
			$('#createCommunicationsModal').modal('show');
			e.stopImmediatePropagation();
			return false;
		});
	</script>

	<script>
		function getContactAppointments(){

			var id = $('.activeContact').data('id');
			var sid='<?php echo $sid; ?>';
			var data = '{"contact_id" : '+id+',"subscriber_id" : '+sid+'}';
			$.ajax({
				url: siteBaseUrl+'/contact/appointment-detail',
				type: 'post',
				data: {'json':data},
				success: function (data) {
					var obj = JSON.parse(data);
					$(".appointmentViewer").empty();

					var htmlAppointmentHeader = '<div class="row head"><div class="col"><h5>Date/Time</h5></div><div class="col-6"><h5>Location</h5></div><div class="col"><h5>User Note</h5></div><!-- col --></div>';
					$(".appointmentViewer").append('<a href="javascript:void(0)" class="addAppointment"><img src="<?php echo Url::base(); ?>/image/add_quote.png" ></a>');
					$(".appointmentViewer").append(htmlAppointmentHeader);
					var i=0;
					while(item = obj.data[i++])
					{
						var date=item.date;
						var duration =item.duration;
						var fDuration =+date + +duration;

						// var dateObj = new Date(date*1000);
						// var dataObjFinal=new Date(fDuration*1000);
						// var month = dateObj.getMonth() + 1;
						// var monthName = moment.monthsShort(month - 1);
						// var day = dateObj.getDate();
						// var year = dateObj.getFullYear();
						// var hours =	dateObj.getUTCHours();
						// var min	= dateObj.getUTCMinutes();
						// var fHours =dataObjFinal.getUTCHours();
						// var fMin	= dataObjFinal.getUTCMinutes();

						// if (day.toString().length == 1) {
						// 	day = "0" + day;
						// }
						// if (hours.toString().length == 1) {
						// 	hours = "0" + hours;
						// }
						// if (min.toString().length == 1) {
						// 	min = "0" + min;
						// }
						// if (fHours.toString().length == 1) {
						// 	fHours = "0" + fHours;
						// }
						// if (fMin.toString().length == 1) {
						// 	fMin = "0" + fMin;
						// }

						var d = new Date(parseInt((date*1000)));						
                  		var date = ("0" + d.getDate()).slice(-2) +'-'+ moment.monthsShort(d.getMonth()) +'-'+ d.getFullYear();
                  		var ampm = d.getHours() >= 12 ? 'pm' : 'am';
                  		var hours = d.getHours() % 12;
  						hours = hours ? hours : 12;
                  		var time = ("0" + hours).slice(-2) +':'+ ("0" + (d.getMinutes())).slice(-2) +':'+ ampm ;

                  		var fd = new Date(parseInt((fDuration*1000)));
                  		var fampm = fd.getHours() >= 12 ? 'pm' : 'am';
                  		var fhours = fd.getHours() % 12;
  						fhours = fhours ? fhours : 12;
                  		var ftime = ("0" + fhours).slice(-2) +':'+ ("0" + (fd.getMinutes())).slice(-2) +':'+ fampm ;

						var htmlAppointmentViewer = '<div class="row  body"><div class="col"><h4>'+date+'</h4><small>'+time+' to '+ftime+' </small></div><div class="col-6"><p>'+item.address+'</p></div><div class="col text ellipsis"<p class="text-concat">'+item.note+'</p></div></div>';
						$(".appointmentViewer").append(htmlAppointmentViewer);
					}
				},
			});
		}
		// by ankit
		// $(document).ready(function(){
		// 	var id = $('#detailsContact').val();
		// 	var sid='<?php echo $sid; ?>';
		// 	getContactAppointments(id,sid);
		// 	$("ul").on("click",".tosearch", function(e){
		// 		var id = $(this).attr('data-id');
		// 		var sid='<?php echo $sid; ?>';
		// 		getContactAppointments(id,sid);
		// 	});
		// });
	</script>

	<script  type="text/javascript">
		var base_url=siteBaseUrl+'/';
		$(document).ready(function(e){

			$(document).on("click",".bodyViewerCommunication", function(e){
				var body_id = $(this).attr('body-item');
				var sid='<?php echo $sid; ?>';
				// window.open(base_url+'communications/view?id='+body_id);
				location.href = base_url+'communications/view?id='+body_id;
				// $.ajax({
				// 	url: base_url+'communications/view?id='+body_id,
				// 	headers: { 'QuoteID': body_id,'SubscriberID':sid},
				// 	success: function (response) {
				// 		$('.modal-bodyCommunications').html(response);
				// 	}
				// });
				// $('#myModalCommunicationViewer').modal('show');
			});

		})
	</script>

	<script>
		var contactUrl=base_url+"webservice/create-contact";
		function createContact(url){
			var id =$('#contact_idForm').val();
			var sid =$('#subscriber_idForm').val();
			var form = $('form')[0]; // You need to use standard javascript object here
			var form = $('#contactId')[0];
			var data = new FormData(form);
			$("#error_msg").remove();
			$.ajax({
				url:  url,
				type: "POST",
				data: data,
				success: function(response) {
					var response = JSON.parse(response);
					if(response.success != 0){
						if(response.success == 2){
							var conf = confirm(response.message);
							if (conf == true) {
							  $('.contact_list').empty();
								$.ajax({
									url:base_url+"contact/searchcontact",
									success: function(response) {
										var parsed = JSON.parse(response);
										// console.log(parsed);exit();
										var i=0;
										while(item = parsed.data[i++]){
											if(item.image=="")
											{
												item.image="avtar.png";
											}
											$('.tosearch').removeClass("activeContact")
											var fieldhtmlList ="<li class='tosearch activeContact' data-id='"+item.id+"'><span class='cnt_img'><img src='"+base_url+"uploads/"+item.image+"' height='58px'  width='58px' style='border-radius: 50%;''></span><div class='contact_person_detail'><p class='contact_nameSearch'>"+item.name+"</p><small>"+item.phone+"</small></div></li>";
											$(".contact_list").append(fieldhtmlList);
										}
										$("#addContact").modal('hide');
										$($('li[data-id="'+item.id+'"]')).trigger('click');
										// getContactData();
									},
								});
							}							
						}
					}else{
						$('#contactId').prepend('<div id="error_msg">Please check all fields.</div>');
						setTimeout(function() {
					        $("#error_msg").remove('blind', {}, 500)
					    }, 2000);
					}
				},
				contentType: false,
				processData:false,
			});
		}

		$('#saveContactData').click(function(){
			createContact(contactUrl);
		});


		$('.add_contact').click(function(){
			$('.quoteTypeText').html('');
			$('.quoteTypeText').append("Create Contact");
			$('#blah').attr('src',base_url+'image/avtar.png');
			contactUrl=base_url+"webservice/create-contact";
			$('#addContact').modal('show');
		});

	</script>

<script>
	$('.updateContactIcon').click(function(){
		var id=$(this).attr("contactidupdate");
		contactUrl = base_url+"webservice/update-contact";
		var sid='<?php echo $sid; ?>';
		$('.quoteTypeText').html('');
		$('.quoteTypeText').append("Update Contact");
		$('#addContact').modal('show');
		$(function () {
			$.getJSON(
				base_url+"webservice/contact-details?subscriber_id="+sid+"&contact_id="+id,
				function (data) {
				//console.log(data);
				//console.log(data.data.contact.contact_id);
				$('#contactName').val(data.data.contact.name);
				$('#contactEmail').val(data.data.contact.email);
				$('#contactPhone').val(data.data.contact.phone);
				$('#lat').val(data.data.contact.lat);
				$('#lng').val(data.data.contact.lng);
				$('#address').val(data.data.contact.formatted_addr);
				$('#street').val(data.data.contact.street1);
				$('#suburb').val(data.data.contact.suburb);
				$('#state').val(data.data.contact.state);
				$('#zipCode').val(data.data.contact.postal);
				$('#blah').attr("src",data.data.contact.image);
				$('#contact_idForm').val(data.data.contact.contact_id);
			}
			)
		})
	})



	$("div").on("click",".addAppointment", function(e){
		$('#myModal2').modal({show:true});
		$('#myModal2').removeAttr('tabindex');
		e.stopImmediatePropagation();
		return false;
	})
</script>
<script>
	$(".clientDetail").click(function(){
		//$(".tab-content").append();
	})
</script>
<script>
	$('.profilePicUpdater').unbind('click').click(function(e){
		$('.fileInput').click();
	})
</script>
<script>
	// $(function() {
	// 	$("#datepicker").datepicker()
	// 	.on("input change", function (e) {
	// 		alert($(this).val())
	// 	} );
	// });



	function currencyFormat(amount) {
	  return '$' + amount.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
	}





</script>
