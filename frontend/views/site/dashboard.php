<?php
/*
 *
 * Dashboard PainPad
 * Last Updated : 02-04-2019
 *
 */
use frontend\assets\DashboardAsset;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
DashboardAsset::register($this);

$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = $this->title;

$commonLat = 28.517957;
$commonLng = -81.36591199999999;

$commonAddress = "2000 S Mills Ave, Orlando, FL 32806, USA";
$commonStreet = "2000 South Mills Avenue";
$commonSuburb = "Orlando";
$commonState = "Florida";
$commonZip = "32806";
?>
<style>
      #map {
        height: 100%;
      }
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #floating-panel {
        position: absolute;
        top: 10px;
        left: 25%;
        z-index: 5;
        background-color: #fff;
        padding: 5px;
        border: 1px solid #999;
        text-align: center;
        font-family: 'Roboto','sans-serif';
        line-height: 30px;
        padding-left: 10px;
      }

      #map-canvas, #map-canvas-site,#map-canvas-contact, #static-map-canvas {
        height: 150px;
        margin: 0;
      }
      .centerMarker, .siteCenterMarker{
        position: absolute;
        background: url(<?=Url::base(true) . "/image/mapIcon.png";?>) no-repeat;
        top: 54%;
        left: 50%;
        z-index: 1;
        margin-left: -10px;
        margin-top: -34px;
        height: 34px;
        width: 20px;
        cursor: pointer;
      }
      .pac-container {
        z-index: 1050 !important;
      }

      .existing_client_list{
        max-height: 190px;
        overflow-y: scroll;
      }
      .existing_client_list li{
        cursor: pointer;
      }
      ul.existing_client_list img {
          height: 45px;
          width: 45px;
          border-radius: 50%;
          margin-right: 10px;
      }
      #client_image img {
        height: 60px;
        width: 60px;
        border-radius: 50%;
      }

      #google-map-overlay{
        height : 150px;
        width: 270px;
        background: transparent;
        position: absolute;
        top: 0px;
        left: 0px;
        z-index: 99;
      }

      #map-canvas-site-overlay {
        height : 150px;
        width: 270px;
        background: transparent;
        position: absolute;
        top: 0px;
        left: 0px;
        z-index: 0;
      }

      /*=== Naveen ====*/
      #InteriorQuote .modal-content {
          background: #f6fbff;
      }
      #InteriorQuote .modal-body {
          padding: 0px 0 0;
          background: #fff;
          margin-top: 30px;
      }
      .Detail_prt_div {
          width: 100%;
          margin-top: -18px;
      }
      .Detail_inner .nav.nav-tabs a.nav-link {
          padding: 3px 6px;
          display: inline-block;
      }
      .Detail_inner .nav.nav-tabs {
          float: left;
          width: 100%;
      }
      */
</style>


<div class="container">

    <div class="row">

        <a data-toggle="modal" data-target="#smallShoes" class="col-4 img-sel" style="background: #149de9;cursor: pointer;">
            <img src="<?=Yii::$app->request->baseUrl;?>/image/newquote.png">
            <span class="cap-l"> New Quote </span>
        </a>

        <a href="<?=Url::toRoute(['quote/'], true);?>" class="col img-sel" style="background: #f6f6f6;">
            <img src="<?=Yii::$app->request->baseUrl;?>/image/quotes.png">
            <span class="cap-l" style="color: #000"> Quotes </span>
        </a>

        <a href="<?=Url::toRoute(['contact/'], true);?>" class="col img-sel" style="background: #0f4988;">
            <img src="<?=Yii::$app->request->baseUrl;?>/image/contact.png">
            <span class="cap-l"> Contacts </span>
        </a>

        <a href="#" class="col img-sel" style="background: #72d8ff;">
            <img src="<?=Yii::$app->request->baseUrl;?>/image/calendar.png">
            <span class="cap-l"> Calendar </span>
        </a>
    </div>


    <div class="row">
        <a href="<?=Url::toRoute(['quote/getallinvoices'], true);?>" class="col img-sel" style="background: #0f4988;">
            <img src="<?=Yii::$app->request->baseUrl;?>/image/Invoices.png">
            <span class="cap-l"> Invoices </span>            
        </a>

        <a href="<?=Url::toRoute(['communications/'], true);?>" class="col img-sel" style="background: #72d8ff;">
            <img src="<?=Yii::$app->request->baseUrl;?>/image/communication.png">
            <span class="cap-l"> Communication </span>
        </a>

        <!-- Author : Ankit, Modified/Created at 2020-01-28 -->
        <a href="<?=Url::toRoute(['quote/subscriberorder'], true);?>" class="col img-sel" style="background: #2e5d8c;">
            <img src="<?=Yii::$app->request->baseUrl;?>/image/Invoices.png">
            <span class="cap-l"> Paint Order </span>
        </a>

        <a href="<?=Url::toRoute(['settings/'], true);?>" class="col img-sel" style="background: #149de9;">
            <img src="<?=Yii::$app->request->baseUrl;?>/image/settings.png">
            <span class="cap-l"> Settings </span>
        </a>

        <a href="#" data-toggle="modal" data-target="#smallShoes2" class="col img-sel" style="background: #2e5d8c;">
            <img src="<?=Yii::$app->request->baseUrl;?>/image/help.png">
            <span class="cap-l"> Help </span>
        </a>
    </div>
</div>



<div class="modal fade" id="smallShoes" tabindex="-1" role="dialog" aria-labelledby="modalLabelSmall" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        <img src="image/quoteStatus/status-declined.png" style="height: 30px;top: -10px;position: relative;">
                    </span>
                </button>
            </div>

            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <a href="#" class="col img-sel interior quoteTypeSelect" data-type="1" data-toggle="modal" data-target="#InteriorQuote" data-dismiss="modal">
                            <img src="<?=Yii::$app->request->baseUrl;?>/image/Interior.png">
                            <span class="cap-l"> Interior </span>
                        </a>
                        <a href="#" class="col img-sel exterior quoteTypeSelect" data-type="2" data-toggle="modal" data-target="#InteriorQuote" data-dismiss="modal">
                            <img src="<?=Yii::$app->request->baseUrl;?>/image/Exterior.png">
                            <span class="cap-l"> Exterior </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="smallShoes2" tabindex="-1" role="dialog" aria-labelledby="modalLabelSmall" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        <img src="image/quoteStatus/status-declined.png" style="height: 30px;top: -10px;position: relative;">
                    </span>
                </button>
            </div>

            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <a href="#" class="col img-sel" style="background: #149de9; margin: 5px;">
                            <img src="<?=Yii::$app->request->baseUrl;?>/image/tutorials.png">
                            <span class="cap-l"> Tutorials </span>
                        </a>
                        <a href="#" class="col img-sel" style="background: #0f4988; margin: 5px;">
                            <img src="<?=Yii::$app->request->baseUrl;?>/image/glossary.png">
                            <span class="cap-l"> Glossary </span>
                        </a>
                        <a href="#" class="col img-sel" style="background: #2e5d8c; margin: 5px;" >
                            <img src="<?=Yii::$app->request->baseUrl;?>/image/support.png">
                            <span class="cap-l"> Support</span>
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<input type="hidden" name="company_id" id="company_id" value="<?=Yii::$app->user->identity->company_id;?>">
<!-- The Interior modal -->
<div class="modal fade" id="InteriorQuote" tabindex="-1" role="dialog" aria-labelledby="modalLabelSmall" aria-hidden="true">

    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <div class="modal-header">
                <h3>
                    <img src="<?=Yii::$app->request->baseUrl;?>/image/newinteriorquote.png" alt="">
                    <span class="quoteTypeText">New Interior Quote</span>
                </h3>
                <div class="Interior_rht_headerIcons">
                    <button type="button" id="saveQuote" data-saveType="create">
                        <span aria-hidden="true">
                            <img src="<?=Yii::$app->request->baseUrl;?>/image/tick@3x.png">
                        </span>
                    </button>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            <img src="image/quoteStatus/status-declined.png" style="">
                        </span>
                    </button>
                </div>
            </div>

            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="Detail_prt_div text-center">
                            <div class="Detail_inner">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" role="tab" data-toggle="tab" href="#home">
                                            <h3>
                                                <span>
                                                    <img src="<?=Yii::$app->request->baseUrl;?>/image/client_blk.png" class="clientIcon">
                                                    <img src="<?=Yii::$app->request->baseUrl;?>/image/client_detail.png" class="clientIcon icon1">
                                                </span>
                                                <span class="headingTxt">Client Details</span>
                                            </h3>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" role="tab" data-toggle="tab" href="#menu1">
                                            <h3>
                                                <span>
                                                    <img src="<?=Yii::$app->request->baseUrl;?>/image/siteDetail_blk.png" class="clientIcon">
                                                    <img src="<?=Yii::$app->request->baseUrl;?>/image/siteDetail.png" class="clientIcon icon1">
                                                </span>
                                                <span class="headingTxt">Site Details</span>
                                            </h3>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div  class="tab-content">
                            <div role="tabpanel" id="home" class="tab-pane in active">
                                <div class="client_detail_form">
                                    <div class="form-group">
                                        <label>Quote Description</label>
                                        <textarea class="description_input" id="description" placeholder="Please give your quote a brief description">
                                        </textarea>
                                    </div>
                                </div>
                                <div class="new_exist_client">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active changeQuoteScreen" data-screen="new" role="tab" data-toggle="tab" href="#menu2">
                                                <h4>New Client</h4>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link changeQuoteScreen" data-screen="existing" role="tab" data-toggle="tab" href="#menu3">
                                                <h4>Existing Client</h4>
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div role="tabpanel" id="menu2" class="tab-pane in active">
                                            <div class="container newQuote-clientDetails-holder">
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-group">
                                                            <input type="text" name="clientName" id="clientName" data-tochange="site_clientName" class="form-control name getChange" placeholder="Client name">
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group">
                                                            <input type="text" name="contactName" id="contactName" data-tochange="site_contactName" class="form-control name getChange" Placeholder="Contact name">
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group">
                                                            <input type="text" name="contactEmail" id="contactEmail" data-tochange="site_contactEmail" class="form-control email getChange" Placeholder="Contact email">
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group">
                                                            <input type="text" name="contactPhone" id="contactPhone" data-tochange="site_contactPhone" class="form-control phone getChange" Placeholder="Contact phone">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-8">
                                                        <div class="form-group">
                                                            <input type="hidden" name="lat" id="lat" value="<?=$commonLat;?>">
                                                            <input type="hidden" name="lng" id="lng" value="<?=$commonLng;?>">
                                                            <input type="hidden" name="csi" id="csi" value="<?=Yii::$app->user->id;?>">
                                                            <input type="hidden" name="contactCreateUrl" id="contactCreateUrl" value="<?=Url::toRoute(['webservice/create-contact'], true);?>">
                                                            <input type="hidden" name="contactUpdateUrl" id="contactUpdateUrl" value="<?=Url::toRoute(['webservice/update-contact'], true);?>">
                                                            <input type="hidden" name="quoteCreateUrl" id="quoteCreateUrl" value="<?=Url::toRoute(['webservice/create-quote'], true);?>">
                                                            <input type="hidden" name="quoteUpdateUrl" id="quoteUpdateUrl" value="<?=Url::toRoute(['webservice/update-quote'], true);?>">
                                                            <input type="text" name="address" id="address" data-tochange="search_address" class="form-control Address getChange" Placeholder="Address" value="<?=$commonAddress;?>">
                                                        </div>
                                                        <div class="row">
                                                            <div class="col">
                                                                <div class="form-group">
                                                                    <input type="text" name="street" id="street" data-tochange="site_street" class="form-control Suburb getChange" Placeholder="Street" value="<?=$commonStreet;?>">
                                                                </div>
                                                            </div>
                                                            <div class="col">
                                                                <div class="form-group">
                                                                    <input type="text" name="suburb" id="suburb" data-tochange="site_suburb" class="form-control Suburb getChange" Placeholder="Suburb" value="<?=$commonSuburb;?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-6">
                                                                <div class="form-group">
                                                                    <input type="text" name="state" id="state" data-tochange="site_state" class="form-control Suburb getChange" Placeholder="State" value="<?=$commonState;?>">
                                                                </div>
                                                            </div>
                                                            <div class="col-6">
                                                                <div class="form-group">
                                                                    <input type="text" name="zipCode" id="zipCode" data-tochange="site_zipCode" class="form-control ZipCode getChange" Placeholder="ZipCode" value="<?=$commonZip;?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="map_div">
                                                            <div id="map-canvas"></div>
                                                        </div>

                                                        <button class="btn-primary clear newQuote-clientDetails-clear">Clear</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" id="menu3" class="tab-pane">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-group">
                                                            <input type="text" id="search_existing_client" name="Search" class="form-control search" placeholder="Search">
                                                        </div>
                                                        <ul class="existing_client_list">
                                                            <?php

$default_detail = [];
if (count($contacts['data'])) {
	if (array_key_exists(0, $contacts['data']['contact'])) {

		$default_detail['contact_id'] = $contacts['data']['contact'][0]['contact_id'];
		$default_detail['name'] = $contacts['data']['contact'][0]['name'];
		$default_detail['phone'] = $contacts['data']['contact'][0]['phone'];
		$default_detail['email'] = $contacts['data']['contact'][0]['email'];
		$default_detail['image'] = $contacts['data']['contact'][0]['image'];
		$default_detail['formatted_addr'] = $contacts['data']['contact'][0]['address']['formatted_addr'];
		$default_detail['lat'] = $contacts['data']['contact'][0]['address']['lat'];
		$default_detail['lng'] = $contacts['data']['contact'][0]['address']['lng'];
	}
}

foreach ($contacts['data']['contact'] as $key => $contact) {

	echo '<li class="contact_li" id="contact_' . $contact['contact_id'] . '" data-id="' . $contact['contact_id'] . '" data-name="' . $contact['name'] . '" data-phone="' . $contact['phone'] . '" data-image="' . $contact['image'] . '" data-email="' . $contact['email'] . '" data-street1="' . $contact['address']['street1'] . '" data-street2="' . $contact['address']['street2'] . '" data-suburb="' . $contact['address']['suburb'] . '" data-state="' . $contact['address']['state'] . '" data-postal="' . $contact['address']['postal'] . '" data-country="' . $contact['address']['country'] . '" data-formatted_addr="' . $contact['address']['formatted_addr'] . '" data-lat="' . $contact['address']['lat'] . '" data-lng="' . $contact['address']['lng'] . '" >';
	echo '<img src="' . $contact['image'] . '" />';
	echo '<div class="existing_client_detail"> <h3 class="client_name_list">' . $contact['name'] . '</h3><small class="client_phone_list">' . $contact['phone'] . '</small></div>';
	echo '</li>';
}
?>
                                                        </ul>
                                                    </div>

                                                    <div class="col border-leftright">
                                                        <img data-subscriber_id="<?=Yii::$app->user->id;?>" data-contact_id="<?=$default_detail['contact_id'];?>" style="position: absolute;right: 20px;top: 0;cursor: pointer;" src="/paintpad/image/edit_new.png" class="edit-existing-contact">
                                                        <div class="client_Detail text-center">
                                                            <input type="hidden" id="client_lat" value="<?=$default_detail['lat'];?>">
                                                            <input type="hidden" id="client_lng" value="<?=$default_detail['lng'];?>">
                                                            <input type="hidden" id="selected_client" value="">
                                                            <div id="client_image" class="profile_img">
                                                                <img src="<?=$default_detail['image'];?>" alt="">
                                                            </div>
                                                            <div id="client_name" class="client_name"><?=$default_detail['name'];?></div>
                                                            <div id="client_email" class="client_email"><?=$default_detail['email'];?><br/><?=$default_detail['phone'];?></div>
                                                        </div>
                                                    </div>
                                                    <div class="col address">
                                                        <h4>Address</h4>
                                                        <p id="client_address"><?=$default_detail['formatted_addr'];?></p>
                                                        <div class="map_div">
                                                            <div id="static-map-canvas"></div>
                                                            <div id="google-map-overlay"></div>
                                                            <span class="map-pin">
                                                                <img src="<?=Yii::$app->request->baseUrl;?>/image/location.png" alt="">
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" id="menu1" class="tab-pane fade site-details-holder">
                                <div class="container">
                                    <div class="row existing_menu">
                                        <div class="col-12">
                                            <label class="clientDetail_Check">Use Client Contact Address
                                                <input type="checkbox" id="client_details_same" checked="checked">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div class="col-8 ">
                                            <div class="row">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <input type="hidden" name="site_lat" id="site_lat" value="<?=$commonLat;?>">
                                                        <input type="hidden" name="site_lng" id="site_lng" value="<?=$commonLng;?>">
                                                        <input type="text" id="search_address" name="search_address" class="form-control search_address" Placeholder="Search Address" value="<?=$commonAddress;?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <input type="text" id="site_street" name="site_street" class="form-control Street" Placeholder="Street" value="<?=$commonStreet;?>">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <input type="text" id="site_suburb" name="site_suburb" class="form-control Suburb" Placeholder="Suburb" value="<?=$commonSuburb;?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-6">
                                                    <div class="form-group">
                                                        <input type="text" id="site_state" name="State" class="form-control Suburb" Placeholder="State" value="<?=$commonState;?>">
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="form-group">
                                                        <input type="text" id="site_zipCode" name="Postcode" class="form-control Postcode" Placeholder="Postcode" value="<?=$commonZip;?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="map_div">
                                                <div id="map-canvas-site"></div>
                                                <div id="map-canvas-site-overlay"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col text-right">
                                            <button type="submit" class="clear btn-primary site-details-holder-clear disabled" disabled="disabled">Clear</button>
                                        </div>
                                    </div>
                                    <div class="row existing_client_detail">
                                        <div class="col">
                                            <div class="form-group">
                                                <input type="text" id="site_contactName" name="Name" class="form-control name" Placeholder="Site Contact name">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <input type="text" id="site_contactEmail" name="Email" class="form-control email" Placeholder="Site Contact email">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <input type="text" id="site_contactPhone" name="Name" class="form-control phone" Placeholder="Site Contact phone">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<!-- SHOW QUOTES HOLDER -->
    <div class="show-quotes-holder">
        <img id="close-communication-holder" src="image/quoteStatus/status-declined.png" style="cursor: pointer; height: 30px;top: 20px;position: absolute;right: 20px;">
        <div class="container">
            <div class="row">
                <div class="show-quotes-holder-data" style="margin-top: 80px;width: 100%">


                    <div class="communiation-row">
                        <table class="table table-borderless">

                            <tr><th>From</th><td>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</td></tr>
                            <tr><th>Date</th><td>21-05-2019</td></tr>
                            <tr><th>To</th><td>---</td></tr>
                            <tr><th></th><td>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</td></tr>

                        </table>
                    </div>


                </div>
            </div>
        </div>

    </div>
<!-- SHOW QUOTES HOLDER -->













<!-- EDIT CONTACT POPUP -->

<div class="modal fade" id="edit-contact-popup" tabindex="-1" role="dialog" aria-labelledby="modalLabelSmall" aria-hidden="true">



    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <div class="container">
                    <div class="row">
                        <div class="col-6">
                            <h3>
                                <img src="<?=Yii::$app->request->baseUrl;?>/image/newinteriorquote.png" alt="">
                                <span class="">Edit Contact</span>
                            </h3>
                        </div>
                        <div class="col-6">
                            <div class="action-icons" style="float: right;">
                                <img style="cursor: pointer;" id="save-contactEdit-popup" src="<?=Yii::$app->request->baseUrl;?>/image/tick@3x.png" width="28px" height="28px">
                                <img style="cursor: pointer;" id="close-contactEdit-popup" src="<?=Yii::$app->request->baseUrl;?>/image/cross@3x.png" width="28px" height="28px">
                            </div>

                        </div>
                    </div>
                </div>





            </div>
            <div class="modal-body" style="width: 672px;">
                <div class="container">
                    <div class="row">
                        <div class="Detail_prt_div text-centerCustom">

                        </div>
                        <div  class="tab-content">
                            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'id' => 'contactId']);?>
                            <div class="profile_div">
                                <img src="<?php echo Url::base(); ?>/image/avtar.png"
                                     alt=""
                                     style="pointer-events: none;height: 50px"
                                     id="contactImage-clientPopup"
                                     class='profilePicUpdater'>


                                    <img id="new-contact-image" src="<?php echo Url::base(); ?>/image/edit_profile.png" style="cursor: pointer;">
                                    <input style="display: none;" type="file" id="contact-edit-image" name="image" class="fileInput" onchange="document.getElementById('contactImage-clientPopup').src = window.URL.createObjectURL(this.files[0])">


                            </div>
                            <div role="tabpanel" id="home" class="tab-pane in active">


                                <input type="hidden"
                                       id="subscriber_idForm"
                                       name="subscriber_id"
                                       value ="<?=$sid;?>">

                                <input type="hidden"
                                       name="contact_id"
                                       id="contact_idForm">


                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <input type="text"
                                                   name="name"
                                                   id="contactName-clientPopup"
                                                   data-tochange="site_contactName"
                                                   class="form-control name getChange"
                                                   Placeholder="Contact name">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <input type="text"
                                                   name="email"
                                                   id="contactEmail-clientPopup"
                                                   data-tochange="site_contactEmail"
                                                   class="form-control email getChange"
                                                   Placeholder="Contact email">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <input type="text"
                                                   name="phone"
                                                   id="contactPhone-clientPopup"
                                                   data-tochange="site_contactPhone"
                                                   class="form-control phone getChange"
                                                   Placeholder="Contact phone">
                                        </div>
                                    </div>
                                </div>
                                <div class="new_exist_client">
                                    <ul class="nav " role="tablist">
                                        <li class="nav-item">
                                            <h4>Contact Information</h4>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div role="tabpanel" id="menu2" class="tab-pane in active">
                                            <div class="">
                                                <div class="row">
                                                    <div class="col-8">

                                                        <div class="form-group">
                                                            <input type="hidden"
                                                                   name="lat"
                                                                   id="lat-clientPopup"
                                                                   value="<?=$commonLat;?>">

                                                            <input type="hidden"
                                                                   name="long"
                                                                   id="lng-clientPopup"
                                                                   value="<?=$commonLng;?>">

                                                            <input type="text"
                                                                   name="formatted_addr"
                                                                   id="address-clientPopup"
                                                                   data-tochange="search_address"
                                                                   class="form-control Address getChange"
                                                                   Placeholder="Address"
                                                                   autocomplete="__away"
                                                                   value="<?=$commonAddress;?>">
                                                        </div>

                                                        <div class="row">
                                                            <div class="col">
                                                                <div class="form-group">
                                                                    <input type="text"
                                                                           name="street1"
                                                                           id="street-clientPopup"
                                                                           data-tochange="site_street"
                                                                           class="form-control Suburb getChange"
                                                                           Placeholder="Street"
                                                                           value="<?=$commonStreet;?>">
                                                                </div>
                                                            </div>
                                                            <div class="col">
                                                                <div class="form-group">
                                                                    <input type="text"
                                                                           name="suburb"
                                                                           id="suburb-clientPopup"
                                                                           data-tochange="site_suburb"
                                                                           class="form-control Suburb getChange"
                                                                           Placeholder="Suburb"
                                                                           value="<?=$commonSuburb;?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-6">
                                                                <div class="form-group">
                                                                    <input type="text"
                                                                           name="state"
                                                                           id="state-clientPopup"
                                                                           data-tochange="site_state"
                                                                           class="form-control Suburb getChange"
                                                                           Placeholder="State"
                                                                           value="<?=$commonState;?>">
                                                                </div>
                                                            </div>
                                                            <div class="col-6">
                                                                <div class="form-group">
                                                                    <input type="text"
                                                                           name="postal"
                                                                           id="zipCode-clientPopup"
                                                                           data-tochange="site_zipCode"
                                                                           class="form-control ZipCode getChange"
                                                                           Placeholder="ZipCode"
                                                                           value="<?=$commonZip;?>">
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="col-4">
                                                        <div class="map_div">
                                                            <div id="map-canvas-contact"></div>

                                                            <span class="map-pin">
                                                                <img src="/paintpad/image/location.png" alt="">
                                                            </span>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php ActiveForm::end();?>
                                        <input type="hidden"
                                               name="csi"
                                               id="csi"
                                               value="<?=Yii::$app->user->id;?>">

                                        <input type="hidden"
                                               name="contactCreateUrl"
                                               id="contactCreateUrl"
                                               value="<?=Url::toRoute(['webservice/create-contact'], true);?>">
                                        <input type="hidden"
                                               name="quoteCreateUrl"
                                               id="quoteCreateUrl"
                                               value="<?=Url::toRoute(['webservice/create-quote'], true);?>">
                                        <input type="hidden"
                                               name="quoteUpdateUrl"
                                               id="quoteUpdateUrl"
                                               value="<?=Url::toRoute(['webservice/update-quote'], true);?>">
                                    </div>
                                    <div class="contactPopup_btmBtn">
                                        <button type="button" value="clear" name="clear" class="btn clearContactBtn pull-right">Clear</button>
                                    </div>
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>










