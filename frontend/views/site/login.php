<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
  
body {
  margin:0px;
  padding:0px;
  background: url(<?= Yii::$app->request->baseUrl ?>/image/background.png) no-repeat;
  background-size: cover;
  background-position:center center;
  width: 100%;
}

</style>

    <div class="row">
    <div class="main_box_login">
      <div class="login_box">
         <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

         <div class="login_logo"><img src="<?= Yii::$app->request->baseUrl ?>/image/logo.png" alt="" /></div>
         <div class="login_fild">
          <img src="<?= Yii::$app->request->baseUrl ?>/image/email.png" alt="" />
          <!-- <input type="text" value="email" name="email" class="email_filed" placeholder=" Email" /> -->

          <?= $form->field($model, 'email')->textInput(['autofocus' => true,'class' => 'form-control email_filed', 'placeholder'=>'Email', 'tabindex' => '1'])->label(false) ?>

         </div>
         <div class="login_fild">
          <img src="<?= Yii::$app->request->baseUrl ?>/image/password.png" alt="" /> 
          <!-- <input type="text" value="password" name="password" class="password_filed" placeholder="Password" /> -->

          <?= $form->field($model, 'password')->passwordInput(['autofocus' => true,'class' => 'form-control email_filed', 'placeholder'=>'Password', 'tabindex' => '2'])->label(false) ?>

         </div>
         <div class="forgot_password">
            <p>
                <?= Html::a('Forgot password?', ['/site/request-password-reset']) ?>
            </p>
        </div>
         <div class="btn_login">           
           <?= Html::submitButton('Login', ['class' => 'btn btn-primary login', 'name' => 'Login']) ?>
         </div>
          <?php ActiveForm::end(); ?>

        
  
      </div>

    </div>
    </div>


