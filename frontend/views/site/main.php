<?php

/* @var $this \yii\web\View */
/* @var $content string */

use frontend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage();?>
<!DOCTYPE html>
<html lang="<?=Yii::$app->language;?>">
<head>
    <meta charset="<?=Yii::$app->charset;?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?=Html::csrfMetaTags();?>
    <title><?=Html::encode($this->title);?></title>
    <?php $this->head();?>
    <script type="text/javascript">var siteBaseUrl = '<?=Url::base(true);?>'</script>
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">


    <style type="text/css">
      body{
        font-family: 'Open Sans', sans-serif;

      }

      li.list-item {
          text-align: left;
          padding: 15px 0 15px 30px;
          cursor: pointer;
      }
      li.list-item:hover {
        font-size: 20px
      }
      a.QuotePageStatus.exclude-filter {
          color: #bebebe!important;
          filter: grayscale(1);
      }
      #contacts-suggestions li{
        display: block;
        float: left;
        width: 100%;
        cursor: pointer;
      }
      #contacts-suggestions li.tosearch:hover {
          background: #f9fcfe
      }
      .app-ovelray{
        position: fixed;
        z-index: 100000;
        background: rgba(0, 0, 0, 0.15);
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        display: none;
      }
      .app-ovelray img{
        height: 60px;
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%,-50%);
        filter: brightness(1.1);
      }
      .quoteI-Status img {
          width: 30px;
      }
      .form-group.quote-form-group.lft_btm_buttons {
          position: absolute;
          bottom: -20px;
          left: 2px;
      }
button.btn.quotelftbtns {
    float: left;
    width: 47%;
    padding: 9px 10px;
    font-size: 18px;
    font-family: "Open Sans";
    margin: 50px 0.5% 0;
    background: #1dafec;
    color: #fff;
    position: absolute;
    left: 0;
    bottom: 0;
    font-weight: 300;
    border-radius: 8px;
    font-family: 'Open Sans', sans-serif;

}
#InteriorQuote #menu1 .clear.btn-primary.disabled {
    background-color: #aadaff;
    border: 1px solid #aadaff;
    border-radius: 3px;
    padding: 4px 21px;
    margin: 15px 0 15px;
}
/*
 * COMMUNICATION SECTION
 */
.show-quotes-holder {
    background: white;
    top: 60px;
    right: 0;
    bottom: 0;
    left: 0;
    position: fixed;
    z-index: 5;
    display: none;
}
#header-paintpad{
  z-index: 500;
}
span#page-title-paintpad {
    text-transform: uppercase;
    font-weight: 500;
    margin: 19px 0 0 40px;
    position: absolute;
    left: 0;
}
.communication-row {
    background: #eee;
    border-bottom: 1px solid #aaa;
}
.communication-row-main {
    background: #fff;
    border-bottom: 1px solid #d5d5d5;
    padding: 10px;
    float: left;
    width: 100%;
}
th.com-title {
    width: 10%;
}
span.com-view-more {
    float: right;
    font-weight: 800;
    cursor: pointer;
}

.com-desc-data img{
    width: 200px!important;
    height: 200px!important;
}
    </style>
</head>
<body>
<?php $this->beginBody();?>


<div class="app-ovelray">
    <img src="http://www.enacteservices.com/paintpad/image/loading.gif" style="">
</div>


<div class="wrap">

<div class="container" id="header-paintpad">

  <div class="row">


      <div class="menu" >
          <i style="cursor: pointer;" class="fa fa-times" aria-hidden="true"></i>



          <ul class="listg">
              <li class="list-item"><a href="<?=Url::base(true);?>/dashboard">Dashboard</a></li>
              <li class="list-item"><a href="<?=Url::base(true);?>/quote">Quotes</a></li>
              <li class="list-item"><a href="<?=Url::base(true);?>/contact">Contacts</a></li>
              <li class="list-item"><a href="<?=Url::base(true);?>/calendar">Calendar</a></li>
              <li class="list-item"><a href="<?=Url::base(true);?>/invoices">Invoices</a></li>
              <li class="list-item"><a href="<?=Url::base(true);?>/communication">Communication</a></li>
              <li class="list-item"><a href="<?=Url::base(true);?>/settings">Settings</a></li>
              <li class="list-item"><a href="<?=Url::base(true);?>/help">Help</a></li>
          </ul>
      </div>

      <div class="mainClose">
        <span class="fa fa-bars"><img style="cursor: pointer;" src="<?=Yii::$app->request->baseUrl;?>/image/side_menu_btn.png"></span>
      </div>

      <div class="text-center" style="width: 100%;min-height: 60px;margin-bottom: 10px; background:#fff;">
        <span id="page-title-paintpad">Page title will be here</span>
        <a href="<?php echo Yii::$app->request->baseUrl; ?>/dashboard">
          <img src="<?=Yii::$app->request->baseUrl;?>/image/dasboard_logo.png">
        </a>
        <input type="text" name="com-search" id="com-search">
      </div>

  </div>

</div> <!-- container -->


        <?=$content;?>


</div> <!-- wrap -->
 <?=Html::a('<img src="' . Yii::$app->request->baseUrl . '/image/logout.png"><span>Logout</span>', ['site/logout'], ['data' => ['method' => 'post']]);?>
<!-- <div class="footer">
    <div class="col-2 push-md-5 text-center logout"> -->
      <!-- <a href="#" class=""><img src="image/logout.png"><span>Logout</span></a> -->





<!--     </div>
  <div class="container">



    <div class="row">

    </div>

  </div>
</div> -->
  <footer class="col-md-12"><p class="copy_right">Copyright (c) PaintPad 2018</p></footer>


<?php $this->endBody();?>

<script src="<?=Url::base(true);?>/js/paintpad.js" id="main-js"></script>


<script type="text/javascript">
  $(function(){
    console.log("I'm On : PaintPad");







    /*-------------------------*/

    $('.QuotePageStatus').click(function(){

      if ($(this).hasClass('exclude-filter')) {

        $(this).removeClass('exclude-filter');

      } else {

        if ( $('.exclude-filter').length == 5) {
          return false
        } else {

          $(this).addClass('exclude-filter');

        }

      }
    });




      /*
       * Getting the contacts suggestions
       */


      $('#search_existing_client_quote').focus(function(e){

        e.stopPropagation();

        var contactsHtml = '';
            contactsHtml += "<ul id='contacts-suggestions' style='list-style: none;padding: 0;background: white;height: 200px;overflow-Y: scroll;'>";

              $.each(jsonUsers, function(i,d){


                  contactsHtml +='<li class="tosearch" data-id="'+d.id+'">';
                    contactsHtml +='<span class="cnt_img">';
                      contactsHtml +='<img src="'+d.img+'" height="58px" width="58px" style="border-radius: 50%;float: left;" class="mCS_img_loaded">';
                    contactsHtml +='</span>';
                    contactsHtml +='<div class="contact_person_detail" style="float: left;width: 70%;height: 70px;">';
                      contactsHtml +='<p class="contact_nameSearch">'+d.value+'</p>';
                      contactsHtml +='<small>'+d.phone+'</small>';
                    contactsHtml +='</div>';
                  contactsHtml +='</li>';

                  // id: 25
                  // img: "http://www.enacteservices.com/paintpad/image/profileIcon.png"
                  // label: "ytiyutriu7t"
                  // phone: "12345"
                  // value: "ytiyutriu7t"

                  console.log(d)
              });
            contactsHtml += "</ul>";

          if ($("#contacts-suggestions").length ==0) {
            $(this).after( contactsHtml );
          }
      });



      /*
       * Filter the contacts suggestions
       */

      $("#search_existing_client_quote").on("keyup", function() {

        var value = $(this).val().toLowerCase();

        $("#contacts-suggestions li").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });

      /*
       * Select the contact from suggestion
       */

      $('.quote-form-group').on( 'click', "#contacts-suggestions li", function(){

        var contactName = $(this).find(".contact_nameSearch").text();

        $("#search_existing_client_quote").val( contactName );

        $("#search_existing_client_quote").attr( 'clientid', $(this).data('id') );
        $("#contacts-suggestions").remove();
      });

      $(document).click(function(ev){

        if (ev.target == $("#search_existing_client_quote")[0] || ev.target == $("#search_existing_client_quote")[0] ) {
          return
        }else{
          $("#contacts-suggestions").remove();
        }

      });









  });
</script>
</body>
</html>
<?php $this->endPage();?>
