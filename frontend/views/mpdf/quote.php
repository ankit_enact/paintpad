<?php 
$rooms      = '';
$scope      = '';
$image      = '';
$subtotal   = '';
$gst        = '';
$total      = '';
$deposit    = '';
$balance    = '';
$start      = '';
$completion = '';
$includes   = '';
$end        = '';

$quote_type = 1;

$room_summary = '';
$color_consultant = '';
$paymentOption = 1;
$paymentOptionName = "within 7 days of completion";

$special    = [];
$siteNotes = [];

$consultant_text = 'A Colour Consultant - Professional On-site Colour Consultation - 1hr - You will receive an individual consultation to assist you select the right colours for you. Selections will be added to our Job Specification Sheet';

$ones = array( 
	1 => "one", 
	2 => "two", 
	3 => "three", 
	4 => "four", 
	5 => "five", 
	6 => "six", 
	7 => "seven", 
	8 => "eight", 
	9 => "nine", 
	10 => "ten");

if(isset($data) && count($data)>0){
		// echo '<pre>'; print_r($data);die;
	$scope      = $data['scope'];
	$scope_image      = $data['scope_image'];
	$rooms      = $data['rooms'];
	$sign       = $data['sign'];
	$signDate   = $data['signDate'];
	$confirm_date   = $data['confirm_date'];
	$brand_logo = $data['brand_logo'];
	$image      = $data['image'];
	$accredateImage      = $data['accredateImage'];
	$subtotal   = $data['subtotal'];
	$gst        = $data['gst'];
	$total      = $data['total'];
	$deposit    = $data['deposit'];
	$depositPercentage    = $data['depositPercentage'];
	$paymentOption    = $data['paymentOption'];
	$partPayment    = $data['PartPayment'];
	$balance    = $data['balance'];
	$start      = $data['start'];
	$end        = $data['end'];
	$completion = $data['completion'];
	$includes   = $data['includes'];	
	$special    = $data['spl'];
	$room_summary = $data['rooms_summary'];
	$color_consultant = $data['color_consultant'];

	$siteNotes = $data['siteNotes'];

	$roomDetails = $data['roomDetails'];
	$quoteRoomNotes = $data['quoteRoomNotes'];
	$quoteRoomSpecialItems = $data['quoteRoomSpecialItems'];

	$quote_type = $data['quote_type'];

		//echo '<pre>'; print_r($special); exit;

	} //data

	if(isset($cdata) && count($cdata)>0){
		$cname  = $cdata['cname'];
		$cemail = $cdata['cemail'];
		$cphone = $cdata['cphone'];
	} //cdata

	if(isset($qdata) && count($qdata)>0){
		$qdesc = $qdata['description'];
		$quoteImage = $qdata['quoteImage'];
		$qid   = $qdata['number'];
		$qdate = $qdata['date'];
	} //qdata

	//The remainder will be due within 7 days of completion as per the agreed payment schedule
	if($paymentOption==1){
		$paymentOptionName = "within 7 days of completion";
	}
	elseif($paymentOption==2){
		$paymentOptionName = "within 14 days of completion";
	}
	elseif($paymentOption==3){
		$paymentOptionName = "within 30 days of completion";
	}
	elseif($paymentOption==4){
		$paymentOptionName = "on completion";
	}
	elseif($paymentOption==5){
		$paymentOptionName = "30 Days EOM";
	}
	elseif($paymentOption==6){
		$paymentOptionName = "by COD";
	}
	elseif($paymentOption==7){
		$paymentOptionName = "14 Days EOM";
	}
	else{
		$paymentOptionName = "within 7 days of completion";
	}

	$innerPortionName = ($quote_type == 1)?"ROOMS":"AREAS";

	$quoteTypeName = ($quote_type == 1)?"INTERIOR":"EXTERIOR";

	setlocale(LC_MONETARY, 'en_US.UTF-8');



	?>




	<style type="text/css">
		#invoice_total table
		{
			width:100%;
			border-top: 3px solid #ccc;
			border-spacing:0;
			border-collapse: collapse;      
			margin-top:5mm;
		}

		.brd-non{border-top: 0px solid #ccc !important;
			border-spacing:0 !important;
			border-collapse: inherit !important;  }
			body{font-family: 'open';}
			table {
				border-spacing: 0;
				border-collapse: collapse;
				font-family: 'open';
			}

		</style>
		<!-- description div -->
		<div id="invoice_body" style="">
			<table style="width: 100%">
				<tr style="background:#3ca9e0 !important; width: 100%">
					<td style=" padding: 9px 51px 9px; color:#fff; font-size: 12.5px; font-family:open; padding-left:52px; font-weight: 600; text-transform: uppercase;"><b style="letter-spacing:1.5; padding-left:6px;">Description</b></td>                
				</tr>
			</table>
		</div> 
		<!-- description div -->

		<!-- service div -->
		<div id="invoice_body" style="padding:2px 40px 0 51px">
			<table style="width:100%;">
				
				<?php
				// print_r($$quoteImage);die;
				// $quoteImage = '';
				if(isset($quoteImage) && !empty($quoteImage)){
					$im = $siteURL.'/uploads/'.$quoteImage;

					list($width, $height) = getimagesize($im);
					if ($width > $height) {
					    $scope_width = '30%';
						$imgTD = '<td width="30%" rowspan="6" style="background:;padding: 6px 10px 10px 10px;font-size: 10px; margin-top:5px;text-align:right;color:#585858;"><span style=""><img src="'.$im.'" alt="" style="width:190px; height:120px;border:0px solid #28abe1;margin:5px 0;" ></span></td>';
					} else {
					    $scope_width = '70%'; 
						$imgTD = '<td width="30%" style="background:;padding: 6px 10px 10px 10px;font-size: 10px;color:#585858;"><span style=""><img src="'.$im.'" alt="" style="width:190px;margin-top:5px;height:130px;border:0px solid #28abe1;" ></span></td>';
					}

					
				}else{
					$scope_width = '70%'; 
					$imgTD = '<td width="30%" rowspan="6" style="background:;padding: 6px 10px 10px 10px;font-size: 10px; margin-top:5px;text-align:right;color:#585858;"><span style=""><img src="'.$siteURL.'/image/box4.png" alt="" style="width:190px; height:120px;border:0px solid #28abe1;margin:5px 0;" ></span></td>';
					
				}?>
				<tr style="width: ;">	         
					<td width="<?= $scope_width ?>" style="background:;padding: 4px 10px 1px 10px; font-family:openBold;padding-top:12px;font-size: 11px; letter-spacing:0.4;color:#585858;"><b><?php echo strtoupper($scope); ?></b> 
					</td>
					<?php echo $imgTD; ?>
				</tr>
				<tr>					
					<td style="color:#585858;font-size: 10.5px;letter-spacing:0.4;padding: 1px 0 4px 1px;"><?= $innerPortionName ?>: <?php echo $rooms; ?>				
				</td>
			</tr> 
			<tr>
				<td style="padding-top:6px; padding-bottom: 2px;"><h1 style="font-size: 11px;font-weight:normal;color:#585858;font-family:open;letter-spacing:0.4;padding: 0px 10px 0px 12px;margin-bottom:0px;"><b > YOUR PAINTING SERVICE INCLUDES</b></h1></td>
			</tr>
			<tr>
				<td  style="font-size:10.5px;letter-spacing:0.4;color:#585858;padding: 0px 0 4px 1px;font-family:open;"><?php 
				if(!empty($includes)){
					echo $includes.' to all nominated rooms as specified.';
				}
				?></td>                
			</tr>
			<tr>
				<td style="padding-top:4px;"><h1 style="font-size: 11.5px;letter-spacing:0.4;font-weight:normal;color:#585858;padding: 4px 10px 1px 10px;margin-bottom:0px;font-family:open;"><b> ONE HOUR COLOUR CONSULTATION INCLUDED</b></h1></td>
			</tr>
			<tr>
				<td style="font-size:10px;color:#585858;font-family:open;padding:0px 0 2px;"></td>
			</tr>
		</table>
	</div> <!-- service div -->


		<!-- Footer Section -->
		

		<div id="invoice_body">
			<table style="width: 100%">
				<tr style=" width: 100%">
					<td style=" padding: 10px 40px; font-family:open; color:#fff; height:20px;"></td>                
				</tr>
				<tr style=" width: 100%">
					<td style=" padding: 10px 40px; font-family:open; color:#fff; height:20px;"></td>                
				</tr>
				<tr style=" width: 100%">
					<td style=" padding: 10px 40px; font-family:open; color:#fff; height:20px;"></td>                
				</tr>
				<tr style=" width: 100%">
					<td style=" padding: 10px 40px; font-family:open; color:#fff; height:20px;"></td>                
				</tr>
				<tr style=" width: 100%">
					<td style=" padding: 10px 40px; font-family:open; color:#fff; height:20px;"></td>                
				</tr>
			</table>
		</div>


		<div id="" style="">
			<div style="padding:0 40px 0 51px; background: #f9fafd; border-top:1px solid #eaeaea;">
				<table>
					<tr style=""><td style="text-align: left; font-family:open; vertical-align: middle;padding-bottom: 1px; padding-top: 8px; color:#585858;font-size: 11.5px;letter-spacing: .3px;"><b>ESTIMATED COMPLETION TIME</b></td>
					</tr>
					<tr style=" font-size: 12px;"><td style="text-align: left; font-family:open; vertical-align: middle;padding-bottom: 1px; color:#585858;padding-left: 2px;font-size: 10.5px;"><?= $completion ?></td>
					</tr>
					<tr style=" font-size: 12px;"><td style="text-align: left; padding-left: 2px;font-family:open; vertical-align: middle;padding-bottom: 9px; letter-spacing: 0.2; color:#585858;font-size: 10.5px;">Your fixed price fee includes all labour and materials unless otherwise specified.</td>
					</tr>
				</table>
				
			</div>	
			
			
			<table style="width:100%; margin-top: 20px;">
				<tr style=" width:100%;">
					<td style="padding: 8px 00px 5px 49px;color:#fff; font-size: 11px; font-family:open; vertical-align: top;">
						<div>
							<span>
								<?php
								$tireDetail->image = '';
								if(isset($tireDetail->image) && !empty($tireDetail->image)){ ?>
									<img src="http://www.enacteservices.net/paintpad/image/box-img.png" alt="" style="width:147px;margin-top:8px;transform: translate(0, 0%);"> 
									<!-- <img src="<?php echo $this->adminUrl. '/uploads/'.$tireDetail->image; ?>" alt="" style="width:130px;transform: translate(0, 0%);">  -->
									
								<?php }elseif(isset($brandDetail->logo) && !empty($brandDetail->logo)){ ?>
									<!-- <img src="http://www.enacteservices.net/paintpad/image/box-img.png" alt="" style="width:147px;margin-top:8px;transform: translate(0, 0%);">  -->
									 <img src="<?php echo $brandDetail->logo; ?>" alt="" style="width:100px;transform: translate(0, 0%);">  
								<?php }else{ ?>
									<!-- <img src="http://enacteservices.net/paintpad/image/box-img.png" alt="" style="width:147px;margin-top:8px;transform: translate(0, 0%);">  -->
									<img src="http://www.enacteservices.net/paintpad/image/box-img.png" alt="" style="width:147px;margin-top:8px;transform: translate(0, 0%);"> 
								<?php } ?>
								
							</span>
						</div>
					</td>
					<td style="padding: 28px 10px 10px 0px; width:325px; font-size: 10px; color:#585858;vertical-align: middle; text-align: center;" width="260px">
						
									<?php if(!empty($accredateImage) || $accredateImage != NULL){ ?>
										<table class="" >
											<tr><td style="padding: 10px 00px 10px 5px;font-family:open; color:#fff; font-size: 11px;">
												<img src="<?= $accredateImage ?>" style="width:207px; transform: translate(0, 0%);">
											</td></tr>
										</table>
									<?php } ?>
								
					</td>
					<td style="padding: 15px 0 10px 3px;font-size: 10px;text-align:right;color:#585858;vertical-align: top;">
						<table class="" style="">
							<tr>
								<td style="padding:5px 10px 3px 0px;font-size: 11px;width:120px;letter-spacing: 0.2; font-family:open; color:#585858;">Total of Fixed Price Fee </td>
								<td style="padding:5px 0px 3px 18px;font-size: 11px; width:105px;letter-spacing: 0.6; font-family:open; color:#585858;text-align:left;"><?php echo money_format("%.2n",$subtotal); ?></td>
							</tr>
							<tr>
								<td style="padding:4.5px 10px 3px 0px;font-size: 11px; width:120px;letter-spacing: 0.6;font-family:open; border-top:0.6px solid #d3e7f1; color:#585858">Plus GST </td>
								<td style="padding:3px 0px 3px 18px;font-size: 11px;width:105px;letter-spacing: 0.6; font-family:open; border-top:0.6px solid #d3e7f1; color:#585858;text-align:left;"><?php echo money_format("%.2n",$gst); ?></td>
							</tr>
							<tr>
								<td style="padding:3px 10px 5px 0px;font-size: 11px;width:120px;letter-spacing: 0.6; font-family:open; border-top:0.6px solid #d3e7f1; color:#585858"><b>Total including GST </b></td>
								<td style="padding:3px 0px 5px 18px;font-size: 11px;width:105px;letter-spacing: 0.6; font-family:open; border-top:0.6px solid #d3e7f1; color:#585858;text-align:left;"><b><?php echo money_format("%.2n",$total); ?></b></td>
							</tr>							
							
									<tr>
								<td style="padding:3px 10px 7px 0px;font-size: 11px;letter-spacing: 0.6;width:120px; font-family:open; border-top:0.6px solid #d3e7f1; color:#585858"><b>Deposit </b></td>
								<td style="padding:3px 0px 7px 18px;font-size: 11px;letter-spacing: 0.6;width:105px;font-family:open;  border-top:0.6px solid #d3e7f1; color:#585858;text-align:left;"><b><?php echo money_format("%.2n",$deposit); ?></b></td>
							</tr>
								 	
							<tr style="">
								<td colspan="2" style=" padding:7px 25px 7px 35px; border-top:1px solid #28abe1">
									<table style="width:100%;">
										<tbody>
											<tr>
												<td style="width:39px"></td>
												<td style="color:#28abe1; padding:0px 0px 3px 12px;font-family:open; font-size: 12px; color:#28abe1; letter-spacing: 0.6;"><b>Balance </b></td>
												<td style="color:#28abe1; padding:0px 0px 3px 27px;font-family:open; width:180px;font-size: 12px; color:#28abe1; text-align:left; letter-spacing: 0.6;"><b><?php echo money_format("%.2n",$balance);?> </b></td>
											</tr>
										</tbody>
									</table>
									
								</td>
							</tr>
							<tr>
								<td colspan="2" style="padding:13px 0px 10px 3px; text-align:left;font-size: 9.5px; font-family:open; letter-spacing: 0.5; width:100px; color:#585858"><b>Progress Payments </b>- Refer to Payment Summary</td>
							</tr>
						</table>
					</td>
				</tr>
		</table>
		
	</div>
	



<!-- <pagebreak /> -->
	<div id="" style="background:#28abe1; margin-top: 0px;">
		<div style="padding:0px 55px;">
			<table style="width:100%;">
				<tr style="">
					<td style="padding: 12px 10px 10px 0;color:#fff; font-family:open; font-size: 12px;letter-spacing: .4px;"><b>ACCEPTANCE ADVICE</b></td>
					<td style="padding: 12px 10px 10px 0;color:#fff; font-family:open; font-size: 11px;"><b>&nbsp;</b></td>
					<td style="padding: 12px 0px 10px 0;color:#fff;  font-family:open; font-size: 12px;text-align:right; letter-spacing: 0.8"><b style="margin-right: 18px;letter-spacing: 0.7;">TAX INVOICE</b> ABN 41 147 435 345</td>
				</tr>
			</table>
			<table style="width:100%;">
				<tr style="">
					<td style="padding: 0px 0px 18px 0;color:#FFF;font-size: 9.5px;line-height: 18.5px;font-family:open;">
						<b>Terms:</b> I/we accept the GST inclusive fixed price fee of <b><?php echo money_format("%.2n",$total);?></b> from Demo Painters as outlined in this quote (No. <?php echo $qid; ?>) and wish to establish a date of commencment. I will pay the <?= $depositPercentage ?>% deposit of <b><?php echo money_format("%.2n",$deposit);?></b> either by credit card, over the phone on 1800 686 525 or via Fax to 08 8370 5300. The remainder will be due <?= $paymentOptionName ?> as per the agreed payment schedule. Recommended commencement date: <b><?php echo $start;?></b> .
					</td>
				</tr>
			</table>
		</div>	

	</div>


	<div id="" style="">
		<div style="padding:5px 46px ;box-shadow: none;">
			<table style="width:100%; border: 0px;box-shadow: none;">
				<tr style="">
					<td style="padding: 4px 10px 0px 21px; font-size: 12px;color:#5c5c5e;box-shadow: none; font-family:open;">
						<b style="letter-spacing: 1">AUTHORISATION</b>
					</td>
				</tr>
			</table>
			<div style="padding-left: 18px;">
			<table style="width:100%; padding-left:70px;box-shadow: none;border: 0px !important;">
				
				<tr>
					<td style="width:200px; border-right: 0px solid #fff;padding: 0px 30px 0px 3px; font-family:open; box-shadow: none;">
						<h1 style="font-size: 13px;box-shadow: none; float: left; font-weight: normal;font-family:open; margin-left:-3px;color:#5c5c5e; padding-top:20px;padding-left: 0px;"><?php echo ucfirst($cname); ?></h1>
						<hr style="height:3px; background:#232323; width:98%; float: right; opacity: 1 !important; margin:8px 0 0 40px;">
					</td>		
					<td style="border-bottom: 0px solid #5c5c5e;font-family:open;  box-shadow: none;width:223px;border-right: 0px solid #fff;padding: 0px 20px 4px 0px;text-align:left;">
						<img style="width:100px;" src="<?php echo $sign ?>">
						<hr style="height:3px; width:98%; background:#232323; float: right; opacity: 1 !important; margin:-2px 0 0 0;">
					</td>
					<td style="border-bottom: 0px solid #5c5c5e;box-shadow: none;width:180px;border-right: 0px solid #fff;padding: 4px 26px 4px 0px; text-align:left;">
						<h1 style="font-size: 13px; font-weight: normal;font-family:open; color:#5c5c5e;padding-top:45px; margin-bottom: 0; line-height: 18px;"><?php 
						date_default_timezone_set("UTC");
						if(!empty($confirm_date) || $confirm_date != NULL){
							echo gmdate('d/m/Y',$confirm_date); 
						}
						?></h1>
						<?php 
							if(empty($confirm_date) || is_null($confirm_date)){
								echo '<p style=" width:100%;height:10px; background:#fff !important; border-color:#fff; float: left; color:#fff; opacity: 1 !important; margin:0px 0 0 0;padding-top:2px">dfsf</p>';
								// echo gmdate('d/m/Y');
							}
						 ?>
						<hr style="height:3px; width:97%; background:#232323; float: right; opacity: 1 !important; margin:8px 0 0 0;">
					</td>
				</tr>
				<tr style="">
					<td style="padding: 2px 6px 2px 3px;font-family:open; width:250px;box-shadow: none; font-size: 13px;color:#5c5c5e;">
						NAME
					</td>
					<td style="padding: 2px 6px 2px 0px; font-family:open;  font-size: 13px;box-shadow: none;text-align:left;color:#5c5c5e;">
						SIGNATURE
					</td>
					<td style="padding: 2px 6px 2px 0px;font-family:open; width:180px;box-shadow: none;font-size: 13px;text-align:left;color:#5c5c5e;"> 
						DATE
					</td>
				</tr>
			</table>
		</div>
		</div>
	</div>
	<table>
		<tr>
			<td style="height: 10px"></td>
		</tr>
	</table>
	<!-- Footer Section -->
	<?php
		if(count($siteNotes) <= 0  && $renderType != 0){
		}else{
			echo "<pagebreak />";
		}
	 ?>


	<?php if(count($special) > 0){ ?>
		<!-- description div -->
		<div id="invoice_body">
			<table style="width: 100%">
				<?php
				echo '<tr style="background:#3ca9e0; width: 100%"><td style="padding: 9px 55px 9px; color:#fff; font-size: 12.5px; font-family:open;  font-weight: 600; text-transform: uppercase;"><b>Special Items</b></td></tr>';
					//echo ' <tr><td style="height: 20px"></td></tr>';
				foreach($special as $_special){
					echo "<tr><td style='padding:10px 55px 0px 55px;color:#5c5c5e;font-size: 12px;'>".$_special."</td></tr>";
					}//foreach
					?>
					<tr>
						<td style="height: 10px"></td>
					</tr>
				</table>
			</div> 
			<!-- description div -->
	<?php
		}
	if(count($siteNotes) > 0){ ?>
		<!-- description div -->
		<div id="invoice_body">
			<table style="width: 100%; margin-bottom: 0px; overflow: wrap">
				<thead><tr style="background:#3ca9e0 !important;"><td colspan="4" style=" padding: 11px 55px 10px; color:#fff; font-size: 12.5px; letter-spacing:0.2; text-transform: uppercase;font-family:open;"><b>SITE NOTES</b></td></tr></thead>
			</table>
			<div class="siteNotesTable" style="margin-bottom:5px;padding: 8px 55px 0 55px;">
				<table style="width: 100%; overflow: wrap;'">
					<?php
					$nc = 0;
					$ln = 1;
					echo '<tbody>';
					$countSiteNotes = count($siteNotes);
					foreach($siteNotes as $note){

						$imgElement = '<span style=""><img src="'.$siteURL.'/image/site_note.png" alt="" style="width:146px; height="100px" border:0px solid #28abe1;" ></span>';
						if($note['image']!=''){
							list($width, $height) = getimagesize($note['image']);
							if ($width > $height) {
							    $imgElement = '<span style=""><img src="'.$note['image'].'" alt="" height="100px" style="width:146px;border:0px solid #28abe1; object-fit:cover;" ></span>';
							    $twI = '147';
							} else {
							    $imgElement = '<span style=""><img src="'.$note['image'].'" alt="" height="190px" style="width:125px;border:0px solid #28abe1; object-fit:cover;" ></span>';
							    $twI = '130';
							}
							
						}

						// if(($ln%2) != 0){
							echo "<tr style='width:100%; ' abc='".$ln."'>";
						// } 

							$w = '';
							if($note['image']!=''){
								// $tw = '100%';
								$colSpn = 'colspan="2"';
							}else{
								// $tw = '100%';
								$colSpn = 'colspan="4"';
							}
							$colspan = '';

						echo '<td width="76%" '.$colSpn.' style="vertical-align: top;padding-left: 0px;padding-right: 13px;padding-top: 15px; padding-bottom: 8px;color:#585858;font-size: 12px; padding-bottom: 20px; line-height:27px; font-weight: normal; font-style: normal; line-height:16px;font-family:open;">'.$note['description'].'</td>';
						if($note['image']!=''){
							echo '<td></td><td width="'.$twI.'" style="text-align: right;  vertical-align: top; padding-left: 2px; padding-top: 10px; padding-bottom: 8px;"><div style="float:right;">'.$imgElement.'</div></td>';
						}

						// if($ln > 0 && $ln%2 == 0){
							echo "</tr>";
						// } 
						$ln++;

							}//foreach
							?>
						</tbody>
					</table>
				</div> 
				<!-- description div -->
				<?php
			}
?>


<?php 
if($renderType == 0){
	$bca = 1;
	foreach($roomDetails as $room_id => $rd){
			// if($bca != 1){
		// echo '<pagebreak />'; 
			// }
		?>
						<?php 

			$backgroundStyle="";
			if($bca%2 != 0){
				$backgroundStyle = 'background:#f5f5f7; margin-bottom:8px';
			}
			$bca++;
			?>

			<div id="" style="<?= $backgroundStyle ?>">
				<div style="padding:0 40px;">
					
					<?php
					$components_name = implode(', ', $rd['components_name']);

					if($rd['coats'] == 1){
						$coatStr = "One Coat of Paint";
					}
					else if($rd['coats'] == 2){
						$coatStr = "Two Coats of Paint";
					}
					else if($rd['coats'] == 3){
						$coatStr = "Three Coats of Paint";
					}
					$roomDetailStr = $components_name.", ".$coatStr.", ".$rd['prep_level']." Prep Level";
					?>

					<div style="padding:0 0px 0 13px; margin-bottom: 20px;">
						<table style="width:100%;">
							<thead style="">
								<tr>
									<td colspan="6" style="padding-top: 10px; padding-bottom: 6px;">
										<h1 style="font-size:12px;font-style:normal;text-decoration: none;font-family:open; margin:0 0 0px;padding:20px 0 3px 0;color:#3ca9e0;text-transform: uppercase;"><b><?= $rd['name'] ?></b></h1>
									</td>
								</tr>
								<tr style=" width:100%;background:#3ca9e0 !important;">
									<td style="padding:0px 10px 0px 15px;vertical-align:middle;height:36px;background:#3ca9e0 !important;color:#fff; font-family:open;font-size: 11px;width: 190px"><b>SURFACE</b></td>
									<td style="padding:0px 10px 0px;color:#fff; vertical-align:middle;height:36px;background:#3ca9e0 !important;font-size: 11px;font-family:open; width: 142px"><b>PAINT</b></td>
									<td style="padding:0px 10px 0px;color:#fff; vertical-align:middle;height:36px;background:#3ca9e0 !important;font-size: 11px;font-family:open;width: 112px"><b>SHEEN</b></td>
									<td style="padding:0px 15px 0px 20px;color:#fff;vertical-align:middle;height:36px;background:#3ca9e0 !important; font-size: 11px;font-family:open;"><b>U/C</b></td>
									<td style="padding:0px 20px 0px;color:#fff; vertical-align:middle;height:36px;background:#3ca9e0 !important;font-size: 11px;font-family:open;"><b>COATS</b></td>
									<td style="padding: 0px 20px 0px 10px;color:#fff;vertical-align:middle;height:36px;background:#3ca9e0 !important;width: 150px;font-family:open; font-size: 11px;"><b>COLOUR</b></td>
								</tr>
							</thead>
							<tbody>
								<?php
								foreach($rd["components"] as $rck => $rcv){
									?>
									<tr style=" width:100%;">
										<td style="padding: 8px 10px 0px 15px;color:#585858; font-size: 11px;font-family:open;"><?= $rcv['name'] ?></td>
										<td style="padding: 8px 10px 0px 10px;color:#585858; font-size: 11px;font-family:open;"><?= $rcv['product_name'] ?></td>
										<td style="padding: 8px 10px 0px 10px;color:#585858; font-size: 11px;font-family:open;"><?= $rcv['sheen_name'] ?></td>
										<?php
										if($rcv['isUndercoat'] == 1){
											// echo '<td style="padding: 8px 20px 0px 20px;color:#585858; font-size: 11px;"><img style="width:14px; height:14px" src= "'.$siteURL.'/image/checkbox_grey_pdf.png"></td>';

											echo '<td style="padding: 8px 20px 0px 20px;color:#585858; font-size: 11px;"><img style="width:14px; height:14px" src= "'.$siteURL.'/image/checkpdf.png"></td>';
										}
										else{
											echo '<td style="padding: 8px 20px 0px 20px;color:#585858; font-size: 11px;font-family:open;"></td>';
										}
										?>
										<td style="padding: 8px 20px 0px 20px;color:#585858; font-size: 11px;font-family:open;"><?= ucfirst($ones[$rcv['coats']]) ?></td>
										<td style="padding: 8px 20px 0px 10px;color:#585858; font-size: 11px;font-family:open;"><?= $rcv['color'] ?></td>
									</tr>
									<?php
								}
								?>
							</tbody>
						</table>
						<table>
							<tr>
								<td style="height: 15px"></td>
							</tr>
						</table>
					
					<?php
					if(array_key_exists($room_id, $quoteRoomNotes)){
						?>
						<?php
						if(count($quoteRoomNotes[$room_id]) > 0){
							echo '<table style="width:100%;"><thead><tr><td colspan="4" style="text-align: left; padding-top: 20px; vertical-align: middle; width:100%;color:#585858;font-size: 10px;"><b style="text-transform:uppercase;font-family:open;">'. $rd["name"] .' NOTES</b><br></td></tr></thead><tbody>';
							$nc = 0;
							$ln = 1;
							foreach($quoteRoomNotes[$room_id] as $rnk => $rnv){
								// print_r($rnv);die;
								$imgElement = '<span style=""><img src="'.$siteURL.'/image/site_note.png" alt="" style="width:150px;border:0px solid #28abe1;" ></span>';
								if($rnv['image']!=''){
									list($width, $height) = getimagesize($rnv['image']);
									if ($width > $height) {
									    $imgElement = '<span style=""><img src="'.$rnv['image'].'" alt="" height="100px" style="width:150px;border:0px solid #28abe1;" ></span>';
									} else {
									    $imgElement = '<span style=""><img src="'.$rnv['image'].'"  height="200px" alt="" style="width:150px;border:0px solid #28abe1;" ></span>';
									}
									
								}
								if($nc == 0){
									echo "<tr style='width:100%;'>";
								}
								?>
								
								<td style="text-align: left;vertical-align: top; width: 25%;padding-top: 10px;font-family:open; padding-bottom: 10px;" ><div><?php echo $imgElement ?></div></td>
								<td style="text-align: left; vertical-align: top; padding-top: 13px;padding-bottom: 10px; width: 25%; padding-left: 8px;padding-right: 20px;color:#585858;font-size: 9.5px;font-family:open;"><?php echo $rnv['description'] ?></td>

								<?php
								
								if($nc != 0 || count($quoteRoomNotes[$room_id]) == $ln){
									if(count($quoteRoomNotes[$room_id])%2 != 0 && count($quoteRoomNotes[$room_id]) == $ln){
										echo "<td></td><td></td>";
									}
									echo "</tr>";
									$nc = 0;
								}
								else{
									$nc = 1;
								}
								$ln++;
							}
							echo '</tbody></table>';
						}
						?>
						<?php
					}
					?>

					<?php
					if(array_key_exists($room_id, $quoteRoomSpecialItems)){
						?>
						<table style="width: 100%;">
							<tr>
								<td style="height: 10px"></td>
							</tr>
							<tr style="">
								<td style="text-align: left; vertical-align: middle;padding-bottom: 0px; width:430px;color:#585858;font-size: 12px;letter-spacing: .3px;font-family:open;"><b>SPECIAL ITEMS</b></td>
							</tr>
							<?php
							foreach($quoteRoomSpecialItems[$room_id] as $rsik => $rsiv){
								?>
								<tr style=" font-size: 12px;">
									<td style="text-align: left; vertical-align: middle;padding-bottom: 4px;padding-top: 10px; width:430px;color:#585858;font-size: 12px;font-family:open;"><?= $rsiv ?></td>
								</tr>
								<?php
							}
							?>
							<tr>
								<td style="height: 12px"></td>
							</tr>
						</table>
						<?php
					}
					?>
				</div>	
				</div>
			</div>	


		<?php } ?>

	</div>


	<?php 
}
// exit;
?>