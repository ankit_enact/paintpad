
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
<?php 

	$rooms      = '';
	$scope      = '';
	$image      = '';
	$subtotal   = '';
	$gst        = '';
	$total      = '';
	$deposit    = '';
	$balance    = '';
	$start      = '';
	$completion = '';
	$includes   = '';
	$end        = '';

	$quote_type = 1;

	$room_summary = '';
	$color_consultant = '';
	$paymentOption = 1;
	$paymentOptionName = "within 7 days of completion";

	$special    = [];
	$siteNotes = [];

	$consultant_text = 'A Colour Consultant - Professional On-site Colour Consultation - 1hr - You will receive an individual consultation to assist you select the right colours for you. Selections will be added to our Job Specification Sheet';

	$ones = array( 
		1 => "one", 
		2 => "two", 
		3 => "three", 
		4 => "four", 
		5 => "five", 
		6 => "six", 
		7 => "seven", 
		8 => "eight", 
		9 => "nine", 
		10 => "ten");

	if(isset($data) && count($data)>0){
		//echo '<pre>'; print_r($data);
		$scope      = $data['scope'];
		$rooms      = $data['rooms'];
		$sign       = $data['sign'];
		$signDate   = $data['signDate'];
		$brand_logo = $data['brand_logo'];
		$image      = $data['image'];
		$subtotal   = $data['subtotal'];
		$gst        = $data['gst'];
		$total      = $data['total'];
		$deposit    = $data['deposit'];
		$depositPercentage    = $data['depositPercentage'];
		$paymentOption    = $data['paymentOption'];
		$balance    = $data['balance'];
		$start      = $data['start'];
		$end        = $data['end'];
		$completion = $data['completion'];
		$includes   = $data['includes'];	
		$special    = $data['spl'];
		$room_summary = $data['rooms_summary'];
		$color_consultant = $data['color_consultant'];

		$siteNotes = $data['siteNotes'];

		$roomDetails = $data['roomDetails'];
		$quoteRoomNotes = $data['quoteRoomNotes'];
		$quoteRoomSpecialItems = $data['quoteRoomSpecialItems'];

		$quote_type = $data['quote_type'];

		//echo '<pre>'; print_r($special); exit;

	} //data

	if(isset($cdata) && count($cdata)>0){
		$cname  = $cdata['cname'];
		$cemail = $cdata['cemail'];
		$cphone = $cdata['cphone'];
	} //cdata

	if(isset($qdata) && count($qdata)>0){
		$qdesc = $qdata['description'];
		$qid   = $qdata['number'];
		$qdate = $qdata['date'];
	} //qdata

	//The remainder will be due within 7 days of completion as per the agreed payment schedule
	if($paymentOption==1){
		$paymentOptionName = "within 7 days of completion";
	}
	elseif($paymentOption==2){
		$paymentOptionName = "within 14 days of completion";
	}
	elseif($paymentOption==3){
		$paymentOptionName = "within 30 days of completion";
	}
	elseif($paymentOption==4){
		$paymentOptionName = "on completion";
	}
	elseif($paymentOption==5){
		$paymentOptionName = "30 Days EOM";
	}
	elseif($paymentOption==6){
		$paymentOptionName = "by COD";
	}
	elseif($paymentOption==7){
		$paymentOptionName = "14 Days EOM";
	}
	else{
		$paymentOptionName = "within 7 days of completion";
	}

	$innerPortionName = ($quote_type == 1)?"ROOMS":"AREAS";

	$quoteTypeName = ($quote_type == 1)?"INTERIOR":"EXTERIOR";

	setlocale(LC_MONETARY, 'en_US.UTF-8');
?>

<style type="text/css">

	#invoice_total table
	{
	    width:100%;
	    border-top: 3px solid #ccc;
	    border-spacing:0;
	    border-collapse: collapse;      
	    margin-top:5mm;
	}

	.brd-non{border-top: 0px solid #ccc !important;
	    border-spacing:0 !important;
	    border-collapse: inherit !important;  }

	/* td, p, h1{font-family: 'DejaVu Sans';} */
	td, p, h1{font-family: 'Open Sans', sans-serif;}
	body{font-family: 'Open Sans', sans-serif;}
	table {
		border-spacing: 0;
		border-collapse: collapse;
	}

</style>


<!-- main DIV -->



<div class="" style="width:100%;/*margin:0 auto; width:1000px; overflow:hidden;*/">
	<div style="padding:0 0 0 30px">
	
		<table cellpadding="0" cellspacing="0" style="width:100%;">
			<tr style=" font-size: 12px;">
				<th style="text-align: left;vertical-align: middle;" rowspan="2"><div><span><img src="<?= $image ?>" alt="" style="width:300px;transform: translate(0, 0%);"> </span></div></th>
				<td style="text-align: center; vertical-align: middle;padding-bottom: 4px; background:#000;width:450px;height:50px"><h1 style="color:#fff; font-size: 25px; font-weight: 700; text-align: right;font-family: 'Open Sans', sans-serif;padding-top: 8px;"><?= $quoteTypeName ?> PAINTING QUOTE</h1></td>
			</tr>
			<tr style=" font-size: 12px; text-align: right; float: right;">
				<td style="vertical-align: middle;text-align: right; ">
				    <table class="" style="border:0px; background:#28abe1;width:450px;">
						<tr>
	                        <td style=" border:0px;padding: 15px 8px 15px 15px;color:#fff;font-weight: normal;text-align: left;">
		                        <table class="" style="border:0px;">
		                        <tr>
		                        <td><b style="color:#fff; font-size: 14px; text-transform: uppercase;">Quote No.</b></td>
		                        </tr>
		                        <tr>
		                        <td style="font-size: 12px;color:#fff"><b>  <?php echo $qid; ?></b></td>
		                        </tr>
		                        </table>
	                        </td>
	                        <td style=" border:0px;padding: 15px 5px 15px 5px;color:#fff; font-weight: normal;text-align: left;">
		                        <table class="" style="border:0px; ">
		                        <tr>
		                        <td><b style="color:#fff; font-size: 14px; text-transform: uppercase; ">Quote Date</b></td>
		                        </tr>
		                        <tr>
		                        <td style="font-size: 12px;color:#fff"><b><?php echo $qdate; ?></b></td>
		                        </tr>
		                        </table>
	                        </td>
	                        <td style=" border:0px;padding: 15px 5px 15px 5px;color:#fff; text-align: left;">
		                        <table class="" style="border:0px;">
		                        <tr>
		                        <td><img width= "130px" style="padding: 0;" src="http://enacteservices.com/paintpad/image/paint-pad-logo-wt.png" alt=""></td>
		                        </tr>
		                        <tr>
		                        <td></td>
		                        </tr>
		                        </table>
	                        </td>
	                    </tr>
	                </table>
				</td>
			</tr>		
		</table>
	</div>  <!-- main DIV -->


<!-- client details DIV -->
<div style="padding:0 40px">
	<table>
		<tr>
			<td style="height: 30px"></td>
		</tr>
	</table>
	<table style="width:100%;">
		<tr>
			<td style="margin-bottom: 13px;color:#222222; font-size: 15px; font-weight: bold; text-align: left;"><b><?php echo ucfirst($cname); ?></b></td>
			<td style=" margin-bottom: 13px;color:#222222; font-size: 15px; font-weight: bold; text-align: left;"><b>Site Details</b></td>
			<td style=" margin-bottom: 13px;color:#222222; font-size: 15px; font-weight: bold; text-align: left;"><b>Quoted by</b></td>
		</tr>
		<tr>
			<td style="font-size: 13px;color:#666; font-weight: 100;">Contact: <?php echo $cname; ?></td>
			<td style="font-size: 13px;color:#666">Contact: <?php echo $cname; ?></td>
			<td style="font-size: 13px;color:#666"><?php echo ucfirst($sub_name); ?></td>
		</tr>
		<tr>
			<td style="font-size: 13px;color:#666">Mobile: <?php echo $cphone; ?></td>
			<td style="font-size: 13px;color:#666">Mobile: <?php echo $cphone; ?></td>
			<td style="font-size: 13px;color:#666"><?php echo $sub_phone; ?></td>
		</tr>
		<tr>
			<td style="font-size: 13px;color:#666; padding:0 30px 0 00px;"><?php echo $client_addr; ?></td>
			<td style="font-size: 13px;color:#666;vertical-align: text-top; padding:0 30px 0 0px;"><?php echo $siteAddress; ?></td>
			<td style="font-size: 13px;color:#666;vertical-align: text-top; padding:0 30px 0 0px;"><?php echo $sub_email; ?></td>
		</tr>	

	</table>
	<table>
		<tr>
			<td style="height: 30px"></td>
		</tr>
	</table>
</div> <!-- client details DIV -->

<!-- description div -->
<div id="invoice_body">
    <table style="width: 100%">
	    <tr style="background:#28abe1; width: 100%">
	        <td style=" padding: 8px 40px; color:#fff; font-size: 16px;text-transform: uppercase;"><b>Description</b></td>                
	    </tr>
    </table>
</div> 
<!-- description div -->

<!-- service div -->
<div id="invoice_body" style="padding:0 40px">
    <table style="width:100%; /*background:#fcfcfe*/">
	    <tr>
			<td style="height: 20px"></td>
		</tr>
	    <tr style="width: 100%">	             
	        <td style="background:;padding: 10px 10px 20px 10px;font-size: 12px;color:#222;letter-spacing: .3px;"><b><?php echo strtoupper($scope); ?></b></td>
		</tr>
		<tr>
			<td style="color:#666;font-size: 12px;"><?= $innerPortionName ?>: <?php echo $rooms; ?></td>
	    </tr> 
		<tr>
			<td style="height: 20px"></td>
		</tr>
	    <tr>
	        <td><h1 style="font-size:12px;font-weight:normal;color:#222;padding: 10px 10px 20px 10px;letter-spacing: .3px;"><b> PAINITING SERVICE INCLUDES</b></h1></td>
	    </tr>
	    <tr>
	        <td  style="font-size:12px;color:#666;"><?php echo $includes;?></td>                
	    </tr>
		<tr>
			<td style="height: 20px"></td>
		</tr>
    </table>
</div> <!-- service div -->

<?php
if(count($special) > 0 || count($siteNotes)>0){
	if(count($special) > 0){
?>
	<!-- description div -->
		<div id="invoice_body">
			<table style="width: 100%">
				<?php
					echo '<tr style="background:#28abe1; width: 100%"><td style=" padding: 8px 40px; color:#fff; font-size: 16px;text-transform: uppercase;"><b>Special Instruction</b></td></tr>';
					//echo ' <tr><td style="height: 20px"></td></tr>';
					foreach($special as $_special){
						echo "<tr><td style='padding:10px 40px 0px 40px;color:#666;font-size: 12px;'>".$_special."</td></tr>";
					}//foreach
				?>
				<tr>
					<td style="height: 10px"></td>
				</tr>
			</table>
		</div> 
<!-- description div -->
<?php
	}
	if(count($siteNotes) > 0){
		?>
			<!-- description div -->
				<div id="invoice_body">
					<table style="width: 100%">
						<?php
							$nc = 0;
							$ln = 1;
							echo '<thead><tr style="background:#28abe1; width: 100%"><td colspan="4" style=" padding: 8px 40px; color:#fff; font-size: 16px;text-transform: uppercase;"><b>SITE NOTES</b></td></tr></thead><tbody>';
							foreach($siteNotes as $note){
								$imgElement = '<span style=""><img src="'.$siteURL.'/image/site_note.png" alt="" style="width:150px;border:1px solid #28abe1;" ></span>';
								if($note['image']!=''){
									$imgElement = '<span style=""><img src="'.$note['image'].'" alt="" style="width:150px;border:1px solid #28abe1;" ></span>';
								}
								if($nc == 0){
									echo "<tr>";
								}
								
								echo '<td style="padding-left:40px;text-align: left;vertical-align: top;"><div>'.$imgElement.'</div></td>';
								echo '<td style="text-align:left; vertical-align: top;color:#666;font-size: 11px;">'.$note['description'].'</td>';
							
								if($nc != 0 || count($siteNotes) == $ln){
									if(count($siteNotes)%2 != 0 && count($siteNotes) == $ln){
										echo "<td></td><td></td>";
									}
									echo "</tr>";
									$nc = 0;
								}
								else{
									$nc = 1;
								}
								$ln++;
								
							}//foreach
						?>
						</tbody>
					</table>
				</div> 
		<!-- description div -->
		<?php
	}
}
?>

<?php 
$bca = 1;
foreach($roomDetails as $room_id => $rd){

	$backgroundStyle="";
	if($bca%2 != 0){
		$backgroundStyle = 'background:#f5f5f7;';
	}
	$bca++;
?>

<div id="" style="<?= $backgroundStyle ?>">
	<div style="padding:0 60px;">
		<h1 style="font-size:12px;font-style:normal;text-decoration: none;margin:0;padding:20px 0 0px 0;color:#222;text-transform: uppercase;"><b><?= $rd['name'] ?></b></h1>

		<?php
			$components_name = implode(', ', $rd['components_name']);

			if($rd['coats'] == 1){
				$coatStr = "One Coat of Paint";
			}
			else if($rd['coats'] == 2){
				$coatStr = "Two Coats of Paint";
			}
			else if($rd['coats'] == 3){
				$coatStr = "Three Coats of Paint";
			}
			$roomDetailStr = $components_name.", ".$coatStr.", ".$rd['prep_level']." Prep Level";
		?>

		<p style="font-size:12px;font-weight:normal;font-style:normal;text-decoration: none;margin:0;padding:0px 0 10px 0;color:#666;"><?= $roomDetailStr ?></p>
		<table style="width:100%;">
			<thead style="">
				<tr style=" width:100%;background:#28abe1;">
					<td style="padding:10px 0 10px 30px;color:#fff; font-size: 12px;"><b>SURFACE</b></td>
					<td style="padding: 10px;color:#fff; font-size: 12px;"><b>PAINT</b></td>
					<td style="padding: 10px;color:#fff; font-size: 12px;"><b>SHEEN</b></td>
					<td style="padding: 10px;color:#fff; font-size: 12px;"><b>U/C</b></td>
					<td style="padding: 10px;color:#fff; font-size: 12px;"><b>COATS</b></td>
					<td style="padding: 10px 30px 10px 00px;color:#fff; font-size: 12px;"><b>COLOUR</b></td>
				</tr>
			</thead>
			<tbody>
			<?php
				foreach($rd["components"] as $rck => $rcv){
			?>
					<tr style=" width:100%;">
						<td style="padding: 15px 0 0px 30px;color:#666; font-size: 12px;"><?= $rcv['name'] ?></td>
						<td style="padding: 15px 0 0px 10px;color:#666; font-size: 12px;"><?= $rcv['product_name'] ?></td>
						<td style="padding: 15px 0 0px 10px;color:#666; font-size: 12px;"><?= $rcv['sheen_name'] ?></td>
						<?php
							if($rcv['topcoat_status'] == 1){
								echo '<td style="padding: 15px 0 0px 10px;color:#666; font-size: 12px;"><img src= "'.$siteURL.'/image/checkpdf.png"></td>';
							}
							else{
								echo '<td style="padding: 15px 0 0px 10px;color:#666; font-size: 12px;"></td>';
							}
						?>
						<td style="padding: 15px 0 0px 10px;color:#666; font-size: 12px;"><?= ucfirst($ones[$rcv['coats']]) ?></td>
						<td style="padding: 15px 0 0px 10px;color:#666; font-size: 12px;"><?= $rcv['color'] ?></td>
					</tr>
			<?php
				}
			?>
			</tbody>
		</table>
		<table>
			<tr>
				<td style="height: 20px"></td>
			</tr>
		</table>
		<?php
			if(array_key_exists($room_id, $quoteRoomNotes)){
		?>
			<?php
					if(count($quoteRoomNotes[$room_id]) > 0){
						echo '<table><thead><tr><td colspan="4" style="text-align: left; vertical-align: middle; width:100%;color:#666;font-size: 16px;"><b>NOTES</b><br></td></tr></thead><tbody>';
						$nc = 0;
						$ln = 1;
						foreach($quoteRoomNotes[$room_id] as $rnk => $rnv){
							$imgElement = '<span style=""><img src="'.$siteURL.'/image/site_note.png" alt="" style="width:150px;border:1px solid #28abe1;" ></span>';
							if($rnv['image']!=''){
								$imgElement = '<span style=""><img src="'.$rnv['image'].'" alt="" style="width:150px;border:1px solid #28abe1;" ></span>';
							}
							if($nc == 0){
								echo "<tr>";
							}
			?>
							
							<td style="text-align: left;vertical-align: top;" ><div><?php echo $imgElement ?></div></td>
							<td style="padding-left:15px;text-align: left; vertical-align: top;padding-top: 10px; color:#666;font-size: 11px;"><?php echo $rnv['description'] ?></td>

			<?php
							
							if($nc != 0 || count($quoteRoomNotes[$room_id]) == $ln){
									if(count($quoteRoomNotes[$room_id])%2 != 0 && count($quoteRoomNotes[$room_id]) == $ln){
										echo "<td></td><td></td>";
									}
								echo "</tr>";
								$nc = 0;
							}
							else{
								$nc = 1;
							}
							$ln++;
						}
						echo '</tbody></table>';
					}
			?>
		<?php
			}
		?>

		<?php
			if(array_key_exists($room_id, $quoteRoomSpecialItems)){
		?>
		<table>
			<tr>
				<td style="height: 10px"></td>
			</tr>
			<tr style="">
				<td style="text-align: left; vertical-align: middle;padding-bottom: 5px; width:430px;color:#666;font-size: 12px;letter-spacing: .3px;"><b>SPECIAL ITEMS</b></td>
			</tr>
			<?php
				foreach($quoteRoomSpecialItems[$room_id] as $rsik => $rsiv){
			?>
			<tr style=" font-size: 12px;">
				<td style="text-align: left; vertical-align: middle;padding-bottom: 10px;padding-top: 7px; width:430px;color:#666;font-size: 12px;"><?= $rsiv ?></td>
			</tr>
			<?php
				}
			?>
		</table>
		<?php
			}
		?>
	</div>	
	<table>
		<tr>
			<td style="height: 10px"></td>
		</tr>
	</table>
</div>	


<?php
}
?>





<div id="invoice_body">
    <table style="width: 100%">
	    <tr style="background:#28abe1; width: 100%">
	        <td style=" padding: 10px 40px; color:#fff; font-size: 13px">&nbsp;</td>                
	    </tr>
    </table>
</div>


<table>
	<tr>
		<td style="height: 15px"></td>
	</tr>
</table>







<div id="" style="">
	<div style="padding:0 40px;">
	<table>
		<tr style=""><td style="text-align: left; vertical-align: middle;padding-bottom: 4px; color:#666;font-size: 13px;letter-spacing: .3px;"><b>ESTIMATED COMPLETION TIME</b></td>
		</tr>
		<tr style=" font-size: 12px;"><td style="text-align: left; vertical-align: middle;padding-bottom: 4px; color:#666;font-size: 12px;"><?= $completion ?></td>
		</tr>
		<tr style=" font-size: 12px;"><td style="text-align: left; vertical-align: middle;padding-bottom: 4px; color:#666;font-size: 12px;">Your fixed price fee includes all labour and materials unless otherwise specified.</td>
		</tr>
	</table>
	
	</div>	
	
	
	<table>
		<tr>
			<td style="height: 25px"></td>
		</tr>
	</table>
	
	
	<table style="width:100%;">
		<tr style=" width:100%;">
			<td style="padding: 10px 00px 10px 30px;color:#fff; font-size: 13px;"><div><span><img src="http://enacteservices.com/paintpad/image/box-img.png" alt="" style="width:150px;transform: translate(0, 0%);"> </span></div></td>
			<td style="padding: 10px; font-size: 11px; color:#666;vertical-align: top;">
				<table class="" >
					<tr ><td style="font-size: 11px; color:#666"><b>PAINT COLOURS</b></td></tr>
					<tr><td style="font-size: 11px; color:#666">Refer Above</td></tr>
					<tr><td><img src="<?= $siteURL.'/image/dulux-accredited.png' ?>"></td></tr>
				</table>
			</td>
			<td style="padding: 10px 0 10px 0;font-size: 12px;text-align:right;color:#666;vertical-align: top;">
				<table class="" style="">
					<tr>
						<td style="padding:10px 40px 0px 0;font-size: 12px; color:#666">Total of fix Price Fee -</td>
						<td style="padding:10px 40px 0px 0;font-size: 12px; color:#666"><?php echo money_format("%.2n",$subtotal); ?></td>
					</tr>
					<tr>
						<td style="padding:10px 40px 0px 0;font-size: 12px; color:#666">Plus GST -</td>
						<td style="padding:10px 40px 0px 0;font-size: 12px; color:#666"><?php echo money_format("%.2n",$gst); ?></td>
					</tr>
					<tr>
						<td style="padding:10px 40px 0px 0;font-size: 12px; color:#666"><b>Total Including GST -</b></td>
						<td style="padding:10px 40px 0px 0;font-size: 12px; color:#666"><b><?php echo money_format("%.2n",$total); ?></b></td>
					</tr>
					<tr>
						<td style="padding:10px 40px 10px 0;font-size: 12px; color:#666"><b>Deposit -</b></td>
						<td style="padding:10px 40px 10px 0;font-size: 12px; color:#666"><b><?php echo money_format("%.2n",$deposit); ?></b></td>
					</tr>
					<tr style="background:#28abe1;">
						<td style="color:#fff; padding:10px 40px 10px 0;font-size: 12px; color:#fff"><b>Balance -</b></td>
						<td style="color:#fff; padding:10px 40px 10px 0;font-size: 12px; color:#fff"><b><?php echo money_format("%.2n",$balance);?></b></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr style=" font-size: 12px;text-align: right;">
			<td style="color:#fff; padding:10px">&nbsp;</td>
			<!-- <td style="color:#fff; padding:10px">&nbsp;</td>
			<td style="text-align: right; vertical-align: middle;padding:30px; color:#222222;font-size: 11px;"><div><span><img src="http://enacteservices.com/paintpad/image/on-time-logo.png" alt="" style="width:150px;transform: translate(0, 0%);"> </span></div></td> -->
		</tr>
	</table>
	
	
		<table>
			<tr>
				<td style="height: 10px"></td>
			</tr>
		</table>
</div>









<div id="" style="background:#28abe1;">
	<div style="padding:20px 40px;">
		<table style="width:100%;">
			<tr style="">
				<td style="padding: 10px;color:#fff; font-size: 13px;letter-spacing: .3px;"><b>ACCEPTANCE ADVICE</b></td>
				<td style="padding: 10px;color:#fff; font-size: 13px;"><b>&nbsp;</b></td>
				<td style="padding: 10px;color:#fff; font-size: 13px;text-align:right"><b>TAX INVOICE</b> ABN 41 147 435 345</td>
			</tr>
		</table>
		<table style="width:100%;">
			<tr style="">
				<td style="padding: 10px;color:#FFF;letter-spacing: .3px;font-size: 12px;line-height: 18px;">
				<b>Terms:</b> I/we accept the GST inclusive fixed price fee of <b><?php echo money_format("%.2n",$total);?></b> from Demo Painters as outlined in this quote (No. <?php echo $qid; ?>) and wish to establish a date of commencment. I will pay the <?= $depositPercentage ?>% deposit of <b><?php echo money_format("%.2n",$deposit);?></b> either by credit card over the phone by calling or by electronic funds transfer to the account, BSB: , account number. The remainder will be due <?= $paymentOptionName ?> as per the agreed payment schedule. Recommended commencement date: <b><?php echo $start;?></b> .
				</td>
			</tr>
		</table>
	</div>	

	<table>
		<tr>
			<td style="height: 10px"></td>
		</tr>
	</table>
</div>






<div id="" style="">
	<div style="padding:20px 40px;">
		<table style="width:100%;">
			<tr style="">
				<td style="padding: 10px; font-size: 13px;color:#222222;">
				<b>AUTHORISATION</b>
				</td>
			</tr>
		</table>
		<table style="width:100%;">
			<tr style="">
				<td style="padding: 10px; font-size: 13px;color:#222222;">
				<b>NAME</b>
				</td>
				<td style="padding: 10px; font-size: 13px;text-align:center;color:#222222;">
				<b>SIGNATURE</b>
				</td>
				<td style="padding: 10px;font-size: 13px;text-align:right;color:#222222;">
				<b>DATE</b>
				</td>
			</tr>
			<tr>
				<td style="border-bottom: 2px solid #aeaeae;border-right: 5px solid #fff;padding: 10px;">
					<h1 style="font-size: 13px; font-weight: normal;color:#222222;"><?php echo ucfirst($cname); ?></h1>
				</td>		
				<td style="border-bottom: 2px solid #aeaeae;border-right: 5px solid #fff;padding: 10px;text-align:center;">
					<img style="width:100px;" src="<?php echo $sign ?>">
				</td>
				<td style="border-bottom: 2px solid #aeaeae;border-right: 5px solid #fff;padding: 10px; text-align:right;">
					<h1 style="font-size: 13px; font-weight: normal;color:#222222;"><?php echo date('d/m/Y', strtotime($signDate)) ?></h1>
				</td>
			</tr>
		</table>
	</div>
</div>
<table>
		<tr>
			<td style="height: 10px"></td>
		</tr>
	</table>
</div>

<?php //exit; ?>