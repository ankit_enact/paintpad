<?php

setlocale(LC_MONETARY, 'en_US.UTF-8');
?>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">


	<style type="text/css">

		#invoice_total table
		{
			width:100%;
			border-top: 3px solid #ccc;
			border-spacing:0;
			border-collapse: collapse;      
			margin-top:5mm;
		}

		.brd-non{border-top: 0px solid #ccc !important;
			border-spacing:0 !important;
			border-collapse: inherit !important;  }

			/* td, p, h1{font-family: 'DejaVu Sans';} */
			td, p, h1{font-family: 'Open Sans', sans-serif;}
			body{font-family: 'Open Sans', sans-serif;}
			table {
				border-spacing: 0;
				border-collapse: collapse;
			}

		</style>

<table>
	<tr>
		<td style="height: 10px"></td>
	</tr>
</table>

<?php


$payments = $data['payments'];

if(count($payments)>0){ ?>

<div id="" style="<?= $backgroundStyle ?>">
	<div style="padding:0 40px;">
		<h1 style="font-size:12px;font-style:normal;text-decoration: none;margin:0;padding:20px 0 0px 0;color:#222;text-transform: uppercase;"><b><?= $_materials['name'] ?></b></h1>

		<table style="width:100%;">
			<thead style="">
				<tr style=" width:100%;background:#28abe1;">
					<td style="padding: 10px;color:#fff; font-size: 14px;text-transform: uppercase;"><b>Payment Date</b></td>
					<td style="padding: 10px;color:#fff; font-size: 14px;text-transform: uppercase;"><b>Description</b></td>
					<td style="padding: 10px;color:#fff; font-size: 14px;text-transform: uppercase;"><b>Payment Amount(Inc GST)</b></td>
					<td style="padding: 10px;color:#fff; font-size: 14px;text-transform: uppercase;"><b>Balance(Inc GST)</b></td>
				</tr>
			</thead>
			<tbody>
				<?php
							// print_r($quote);die;
				foreach($payments as $_payments){
								// print_r($rcv);die;
					?>
					<tr style=" width:100%;">
						<td style="padding: 15px 10px;color:#666; font-size: 15px;"><?= $_payments['payment_date'] ?></td>
						<td style="padding: 15px 10px;color:#666; font-size: 15px;"><?= $_payments['descp'] ?></td>
						<td style="padding: 15px 10px;color:#666; font-size: 15px;"><?= money_format("%.2n",$_payments['amount']) ?></td>
						<td style="padding: 15px 10px;color:#666; font-size: 15px;"><?= money_format("%.2n",$_payments['balance']) ?></td>
					</tr>
					<?php
				}
				?>
			</tbody>
		</table>

	</div>	
</div>	


<?php } ?>


<?php


// $payments = $data['payments'];

// if(count($payments)>0){
	
// 	echo '<div style="background:#fff;">';
// 	echo '<div style="padding:0 60px;">';
// 	echo '<h1 style="font-size:12px;font-style:normal;text-decoration: none;margin:0;padding:20px 0 10px 0;color:#222;text-transform: uppercase;"><b>'.$_materials['name'].'</b></h1>';
// 	echo '<TABLE style=" width:100%;">';
// 	echo '<THEAD>';
// 	echo '<TR style="width:100%;background:#28abe1;">';
// 	echo '<TD style="padding:10px 0 10px 30px;color:#fff; font-size: 12px;"><b>Payment Date</b></TD>';
// 	echo '<TD style="padding: 10px;color:#fff; font-size: 12px;"><b>Description</b></TD>';
// 	echo '<TD style="padding: 10px;color:#fff; font-size: 12px;"><b>Payment Amount(Inc GST)</b></TD>';
// 	echo '<TD style="padding: 10px 30px 10px 00px;color:#fff; font-size: 12px;"><b>Balance(Inc GST)</b></TD>';
// 	echo '</TR>';
// 	echo '</THEAD>';
// 	foreach($payments as $_payments){
// 		echo '<TR style=" width:100%;">';
// 		echo '<TD style="padding: 15px 0 0px 30px;color:#666; font-size: 12px;">'.$_payments['payment_date'].'</TD>';
// 		echo '<TD style="padding: 15px 0 0px 10px;color:#666; font-size: 12px;">'.$_payments['descp'].'</TD>';
// 		echo '<TD style="padding: 15px 0 0px 10px;color:#666; font-size: 12px;">'.money_format("%.2n",$_payments['amount']).'</TD>';
// 		echo '<TD style="padding: 15px 0 0px 10px;color:#666; font-size: 12px;">'.money_format("%.2n",$_payments['balance']).'</TD>';
// 		echo '</TR>';
// 	}
// 	echo '</TABLE>';	
// 	echo '</div>';	
// 	echo '</div>';
// }

?>