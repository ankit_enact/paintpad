<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
<?php

		$rooms      = [];
		$image      = '';
		$nop        = '';
		$qty        = '';
		$hrs        = '';
		$start      = '';
		$end        = '';
		$scope      = '';
		$special    = [];
		$siteNotes    = [];

		$rooms      = array();
		$materials  = array();
		$room_summary = '';
		$color_consultant = '';

		$consultant_text = 'A Colour Consultant - Professional On-site Colour Consultation - 1hr - You will receive an individual consultation to assist you select the right colours for you. Selections will be added to our Job Specification Sheet';

	if(isset($data) && count($data)>0){
		$scope      = $data['scope'];
		$brand_logo = $data['brand_logo'];
		$image      = $data['image'];
		$special    = $data['spl'];


		$nop       = $data['nop'];
		$qty       = $data['qty'];
		$hrs       = $data['hrs'];
		$start     = $data['start'];
		$end       = $data['end'];

		$rooms     = $data['rooms'];
		$materials = $data['materials'];
		$special    = $data['spl'];
		$room_summary = $data['rooms_summary'];
		$color_consultant = $data['color_consultant'];
		$specialInstructions = $data['specialInstructions'];
		$siteNotes = $data['siteNotes'];

		//echo count($rooms); exit;
		//echo '<pre>'; print_r($materials); exit;

	} //data
	
	//echo '<pre>'; print_r($rooms);exit;

?>

<style type="text/css">

#invoice_total table
{
    width:100%;
    border-top: 3px solid #ccc;
    border-spacing:0;
    border-collapse: collapse;      
    margin-top:5mm;
}

.brd-non{border-top: 0px solid #ccc !important;
    border-spacing:0 !important;
    border-collapse: inherit !important;  }

td, p, h1{font-family: 'DejaVu Sans';}

/******************************************************************************************************************************/
/*
.ft0{font: 1px 'DejaVu Sans';line-height: 1px;}
.ft6{font: 13px 'DejaVu Sans';color: #ffffff;line-height: 16px;}
.ft7{font: 1px 'DejaVu Sans';line-height: 8px;}
.ft9{font: bold 13px 'DejaVu Sans';line-height: 20px;}
.ft8{font: 13px 'DejaVu Sans';line-height: 20px;}
.ft10{font: 13px 'DejaVu Sans';line-height: 16px;}
.ft11{font: bold 13px 'DejaVu Sans';line-height: 16px;}
.ft12{font: 13px 'DejaVu Sans';line-height: 19px;}
.ft13{font: 1px 'DejaVu Sans';line-height: 4px;}
.ft14{font: 9px 'DejaVu Sans';line-height: 12px;position: relative; bottom: 6px;}

.ftt9{font: 13px 'DejaVu Sans';line-height: 15px;}




.ft1{font: 18px 'DejaVu Sans';color: #0a80c5;line-height: 21px;}
.ft2{font: 13px 'DejaVu Sans';color: #62615e;line-height: 16px;}
.ft3{font: bold 15px 'DejaVu Sans';color: #0a80c5;line-height: 18px;}
.ft4{font: bold 13px 'DejaVu Sans';color: #62615e;line-height: 16px;}
.ft5{font: 1px 'DejaVu Sans';line-height: 10px;}
.ft15{font: italic 11px 'DejaVu Sans';line-height: 14px;}
.ft16{font: 1px 'DejaVu Sans';line-height: 2px;}
.ft17{font: 19px 'DejaVu Sans';color: #0a80c5;line-height: 22px;}
.ft18{font: 1px 'DejaVu Sans';line-height: 3px;}
.ft19{font: 1px 'DejaVu Sans';line-height: 9px;}
.ft20{font: 1px 'DejaVu Sans';line-height: 15px;}
.ft21{font: bold 13px 'DejaVu Sans';color: #0a80c5;line-height: 16px;}
.ft22{font: bold 16px 'DejaVu Sans';color: #1d7db3;line-height: 19px;}






.p0{text-align: left;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p1{text-align: left;padding-left: 110px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p2{text-align: left;padding-left: 225px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p3{text-align: right;padding-right: 133px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p4{text-align: left;padding-left: 2px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
/*.p4 {text-align: right;padding-left: 2px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;padding-right: 10px;}*/
.p5{text-align: left;padding-left: 54px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p6{text-align: left;padding-left: 107px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p7{text-align: left;padding-left: 24px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p8{text-align: left;padding-left: 55px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p9{text-align: left;padding-left: 10px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p10{text-align: left;padding-left: 0px;padding-right: 91px;margin-top: 11px;margin-bottom: 0px;}
.p11{text-align: left;padding-left: 6px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p12{text-align: left;padding-right: 44px;margin-top: 2px;margin-bottom: 0px;}
.p13{text-align: left;margin-top: 0px;margin-bottom: 0px;}
.p14{text-align: left;padding-left: 4px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p15{text-align: left;padding-left: 3px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p16{text-align: left;padding-left: 33px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p17{text-align: left;padding-left: 12px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p18{text-align: right;padding-right: 8px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p19{text-align: right;padding-right: 145px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p20{text-align: left;padding-left: 52px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p21{text-align: left;padding-left: 252px;margin-top: 0px;margin-bottom: 0px;}
.p22{text-align: right;padding-right: 27px;margin-top: 0px;margin-bottom: 0px;}
.p23{text-align: left;padding-left: 460px;margin-top: 35px;margin-bottom: 0px;}
.p24{text-align: left;padding-left: 40px;margin-top: 27px;margin-bottom: 0px;}
.p25{text-align: left;padding-left: 44px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p26{text-align: left;padding-left: 125px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p27{text-align: left;padding-left: 73px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p28{text-align: left;padding-left: 14px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p29{text-align: left;padding-left: 29px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p30{text-align: left;padding-left: 26px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p31{text-align: right;padding-right: 134px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p32{text-align: center;padding-right: 12px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p33{text-align: left;padding-left: 91px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p34{text-align: left;padding-left: 39px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p35{text-align: center;padding-right: 13px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}

.td0{padding: 0px;margin: 0px;width: 197px;vertical-align: middle;}
.td1{padding: 0px;margin: 0px;width: 281px;vertical-align: middle;}
.td2{padding: 0px;margin: 0px;width: 251px;vertical-align: middle;}
.td3{padding: 0px;margin: 0px;width: 239px;vertical-align: middle;}
.td4{padding: 0px;margin: 0px;width: 12px;vertical-align: middle;}
.td5{padding: 0px;margin: 0px;width: 197px;vertical-align: middle;background: #1d7db3;}
.td6{padding: 0px;margin: 0px;width: 281px;vertical-align: middle;background: #1d7db3;}
.td7{padding: 0px;margin: 0px;width: 239px;vertical-align: middle;background: #1d7db3;}
.td8{padding: 0px;margin: 0px;width: 365px;vertical-align: middle;}
.td9{padding: 0px;margin: 0px;width: 352px;vertical-align: middle;}
.td10{padding: 0px;margin: 0px;width: 365px;vertical-align: middle;background: #666666;}
.td11{padding: 0px;margin: 0px;width: 352px;vertical-align: middle;background: #666666;}
.td12{border-left: #1d7db3 1px solid;border-right: #1d7db3 1px solid;padding: 0px;margin: 0px;width: 429px;vertical-align: middle;background: #1d7db3;}
.td13{border-right: #1d7db3 1px solid;padding: 0px;margin: 0px;width: 143px;vertical-align: middle;background: #1d7db3;}
.td14{border-right: #1d7db3 1px solid;padding: 0px;margin: 0px;width: 142px;vertical-align: middle;background: #1d7db3;}
.td15{border-left: #1d7db3 1px solid;border-right: #1d7db3 1px solid;padding: 0px;margin: 0px;width: 178px;vertical-align: middle;background: #1d7db3;}
.td16{border-right: #1d7db3 1px solid;padding: 0px;margin: 0px;width: 250px;vertical-align: middle;background: #1d7db3;}
.td17{border-right: #1d7db3 1px solid;padding: 0px;margin: 0px;width: 71px;vertical-align: middle;background: #1d7db3;}
.td18{border-right: #1d7db3 1px solid;padding: 0px;margin: 0px;width: 70px;vertical-align: middle;background: #1d7db3;}
.td19{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 180px;vertical-align: middle;}
.td20{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 251px;vertical-align: middle;}
.td21{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 144px;vertical-align: middle;}
.td22{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 72px;vertical-align: middle;}
.td23{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 71px;vertical-align: middle;}


.td24{border-left: #000000 1px solid;border-right: #000000 1px solid;padding:5px 5px;margin: 0px;width: 178px;vertical-align: middle;background: #cdcdcd;}
.td25{border-right: #000000 1px solid;border-top: #cdcdcd 1px solid;padding:5px 5px;margin: 0px;width: 250px;vertical-align: middle;background: #cdcdcd;}
.td26{border-right: #000000 1px solid;border-top: #cdcdcd 1px solid;padding:5px 5px;margin: 0px;width: 143px;vertical-align: middle;background: #cdcdcd;}
.td27{border-right: #000000 1px solid;border-top: #cdcdcd 1px solid;padding:5px 5px;margin: 0px;width: 71px;vertical-align: middle;background: #cdcdcd;}
.td28{border-right: #000000 1px solid;border-top: #cdcdcd 1px solid;padding:5px 5px;margin: 0px;width: 70px;vertical-align: middle;background: #cdcdcd;}


.td29{border-left: #000000 1px solid;border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 178px;vertical-align: middle;background: #cdcdcd;}
.td30{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 250px;vertical-align: middle;background: #cdcdcd;}
.td31{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 143px;vertical-align: middle;background: #cdcdcd;}
.td32{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 71px;vertical-align: middle;background: #cdcdcd;}
.td33{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 70px;vertical-align: middle;background: #cdcdcd;}

.td34{border-left: #000000 1px solid;border-right: #000000 1px solid;margin: 0px;width: 178px;vertical-align: middle;}
.td35{border-right: #000000 1px solid;margin: 0px;width: 250px;vertical-align: middle;}
.td36{border-right: #000000 1px solid;margin: 0px;width: 143px;vertical-align: middle;}
.td37{border-right: #000000 1px solid;margin: 0px;width: 71px;vertical-align: middle;}
.td38{border-right: #000000 1px solid;margin: 0px;width: 70px;vertical-align: middle;}

.td39{border-left: #000000 1px solid;border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 178px;vertical-align: middle;}
.td40{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 250px;vertical-align: middle;}
.td41{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 143px;vertical-align: middle;}
.td42{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 71px;vertical-align: middle;}
.td43{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 70px;vertical-align: middle;}
.td44{padding: 0px;margin: 0px;width: 195px;vertical-align: middle;}
.td45{border-right: #000000 1px solid;border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 250px;vertical-align: middle;background: #cdcdcd;}
.td46{border-right: #000000 1px solid;border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 143px;vertical-align: middle;background: #cdcdcd;}
.td47{border-right: #000000 1px solid;border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 71px;vertical-align: middle;background: #cdcdcd;}
.td48{border-right: #000000 1px solid;border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 70px;vertical-align: middle;background: #cdcdcd;}
.td49{padding: 0px;margin: 0px;width: 169px;vertical-align: middle;}
.td50 {padding: 0 0 0 15px;margin: 0px;width: 250px;vertical-align: bottom;}
.td51{padding: 0px;margin: 0px;width: 216px;vertical-align: middle;}
.td52{padding: 0px;margin: 0px;width: 169px;vertical-align: middle;background: #cdcdcd;}
.td53{border-right: #1d7db3 1px solid;padding: 0px;margin: 0px;width: 147px;vertical-align: middle;background: #1d7db3;}
.td54{border-right: #1d7db3 1px solid;padding: 0px;margin: 0px;width: 138px;vertical-align: middle;background: #1d7db3;}
.td55{border-right: #1d7db3 1px solid;padding: 0px;margin: 0px;width: 67px;vertical-align: middle;background: #1d7db3;}
.td56{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 148px;vertical-align: middle;}
.td57{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 68px;vertical-align: middle;}
.td58{border-right: #000000 1px solid;border-top: #cdcdcd 1px solid;padding: 0px;margin: 0px;width: 147px;vertical-align: middle;background: #cdcdcd;}
.td59{border-right: #000000 1px solid;border-top: #cdcdcd 1px solid;padding: 0px;margin: 0px;width: 67px;vertical-align: middle;background: #cdcdcd;}
.td60{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 147px;vertical-align: middle;background: #cdcdcd;}
.td61{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 67px;vertical-align: middle;background: #cdcdcd;}
.td62{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 147px;vertical-align: middle;}
.td63{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 67px;vertical-align: middle;}
.td64{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 147px;vertical-align: middle;}
.td65{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 67px;vertical-align: middle;}
.td66{padding: 0px;margin: 0px;width: 180px;vertical-align: middle;}
.td67{padding: 0px;margin: 0px;width: 148px;vertical-align: middle;}
.td68{padding: 0px;margin: 0px;width: 68px;vertical-align: middle;}
.td69{padding: 0px;margin: 0px;width: 71px;vertical-align: middle;}
.td70{padding: 0px;margin: 0px;width: 54px;vertical-align: middle;}
.td71{padding: 0px;margin: 0px;width: 19px;vertical-align: middle;}
.td72{padding: 0px;margin: 0px;width: 140px;vertical-align: middle;}
.td73{padding: 0px;margin: 0px;width: 183px;vertical-align: middle;}
.td74{padding: 0px;margin: 0px;width: 82px;vertical-align: middle;}
.td75{padding: 0px;margin: 0px;width: 240px;vertical-align: middle;}
.td76{padding: 0px;margin: 0px;width: 11px;vertical-align: middle;}
.td77{padding: 0px;margin: 0px;width: 159px;vertical-align: middle;}
.td78{padding: 0px;margin: 0px;width: 73px;vertical-align: middle;}
.td79{border-left: #666666 1px solid;padding: 0px;margin: 0px;width: 212px;vertical-align: middle;background: #666666;}
.td80{border-right: #666666 1px solid;padding: 0px;margin: 0px;width: 182px;vertical-align: middle;background: #666666;}
.td81{padding: 0px;margin: 0px;width: 82px;vertical-align: middle;background: #666666;}
.td82{border-right: #666666 1px solid;padding: 0px;margin: 0px;width: 239px;vertical-align: middle;background: #666666;}
.td83{border-left: #666666 1px solid;padding: 0px;margin: 0px;width: 53px;vertical-align: middle;background: #666666;}
.td84{border-right: #666666 1px solid;padding: 0px;margin: 0px;width: 18px;vertical-align: middle;background: #666666;}
.td85{padding: 0px;margin: 0px;width: 140px;vertical-align: middle;background: #666666;}
.td86{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 54px;vertical-align: middle;}
.td87{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 19px;vertical-align: middle;}
.td88{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 140px;vertical-align: middle;}
.td89{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 183px;vertical-align: middle;}
.td90{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 82px;vertical-align: middle;}
.td91{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 240px;vertical-align: middle;}




.td92{border-left: #000000 1px solid;border-top: #000000 1px solid;border-bottom: #000000 1px solid;padding: 5px;margin: 0px;width: 53px;vertical-align: middle;background: #cdcdcd;}
.td93{border-right: #000000 1px solid;border-top: #000000 1px solid;border-bottom: #000000 1px solid;padding: 5px;margin: 0px;width: 18px;vertical-align: middle;background: #cdcdcd;}
.td94{border-top: #cdcdcd 1px solid;border-bottom: #000000 1px solid;padding: 5px;margin: 0px;width: 140px;vertical-align: middle;background: #cdcdcd;}
.td95{border-right: #000000 1px solid;border-top: #cdcdcd 1px solid;border-bottom: #000000 1px solid;padding: 5px;margin: 0px;width: 182px;vertical-align: middle;background: #cdcdcd;}
.td96{border-top: #cdcdcd 1px solid;border-bottom: #000000 1px solid;padding: 5px;margin: 0px;width: 82px;vertical-align: middle;background: #cdcdcd;}
.td97{border-right: #000000 1px solid;border-top: #cdcdcd 1px solid;border-bottom: #000000 1px solid;padding: 5px;margin: 0px;width: 239px;vertical-align: middle;background: #cdcdcd;}
.td98{border-left: #000000 1px solid;padding: 5px;margin: 0px;width: 53px;vertical-align: middle;}
.td99{border-right: #000000 1px solid;padding: 5px;margin: 0px;width: 18px;vertical-align: middle;}
.td100{border-right: #000000 1px solid;padding: 5px;margin: 0px;width: 322px;vertical-align: middle;}
.td101{border-right: #000000 1px solid;padding: 5px;margin: 0px;width: 321px;vertical-align: middle;}
.td102{border-left: #000000 1px solid;border-bottom: #000000 1px solid;padding: 5px;margin: 0px;width: 53px;vertical-align: middle;}
.td103{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 5px;margin: 0px;width: 18px;vertical-align: middle;}
.td104{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 5px;width: 322px;vertical-align: middle;}



.td105{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 239px;vertical-align: middle;}
.td106{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 239px;vertical-align: middle;}
.td107{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 182px;vertical-align: middle;}

.tr0{height: 45px;}
.tr1{height: 42px;}
.tr2{height: 21px;}
.tr3{height: 22px;}
.tr4{height: 10px;}
.tr5{height: 38px;}
.tr6{height: 8px;}
.tr7{height: 25px;}
.tr8{height: 17px;}
.tr9{height: 18px;}
.tr10{height: 38px;}
.tr11{height: 4px;}
.tr12{height: 20px;border-bottom: #000000 1px solid;vertical-align: middle !important; padding: 5px !important;}
.tr13{height: 19px;border-bottom: #000000 1px solid;vertical-align: middle !important;padding: 5px !important;}
.tr14{height: 27px;}
.tr15{height: 23px;}
.tr16{height: 2px;}
.tr17{height: 24px;}
.tr18{height: 16px;}
.tr19{height: 26px;}
.tr20{height: 38px;}
.tr21{height: 91px;}
.tr22{height: 3px;}
.tr23{height: 29px;}
.tr24{height: 9px;}
.tr25{height: 15px;}
.tr26{height: 46px;}
.tr27{height: 65px;}
.tr28{height: 37px;}
.tr29{height: 36px;}

.t0{width: 100%;font: 13px 'DejaVu Sans';color: #62615e;}
.t1{width: 100%;font: bold 13px 'DejaVu Sans';}
.t2{width: 100%;margin-top: 9px;font: 13px 'DejaVu Sans';}
.t3{width: 100%;margin-left: 2px;font: 13px 'DejaVu Sans';color: #62615e;}
.t4{width: 100%;margin-top: 5px;font: 13px 'DejaVu Sans';}
.t5{width: 100%;margin-left: 48px;font: bold 13px 'DejaVu Sans';color: #62615e;}
.t6{width: 100%;margin-left: 38px;margin-top: 5px;font: 13px 'DejaVu Sans';}
.t7{width: 100%;font: 13px 'DejaVu Sans';}

.pd-l{padding-left:15px}
.t0 {background: #1d7db3;}
.td24, .td25, .td26, .td27, .td28{border-bottom: #000 1px solid;vertical-align: middle;}

*/


/****/
	td, p, h1{font-family: 'Open Sans', sans-serif;}
	body{font-family: 'Open Sans', sans-serif;}
	table {
		border-spacing: 0;
		border-collapse: collapse;
	}
	
</style>



<div id="invoice_body">
    <table style="width: 100%">
	    <tr style="background:#28abe1; width: 100%">
	        <td style=" padding: 8px 40px; color:#fff; font-size: 16px;text-transform: uppercase;"><b>Job Scope</b></td>                
	    </tr>
    </table>
</div> 


<?php
//echo strtoupper($scope);
?>
<!-- service div -->
<div id="invoice_body" style="padding:0 40px">
    <table style="width:100%; /*background:#fcfcfe*/">
	    <tr>
			<td style="height: 10px"></td>
		</tr>
		<tr style="width: 100%">	             
	        <td style="padding: 10px 0px 5px 0px;font-size: 12px;color:#222;letter-spacing: .3px;"><b><?php echo strtoupper($scope); ?></b></td>
		</tr>
		<tr>
			<td style="color:#666;font-size: 12px;">ROOMS: <?php echo $room_summary; ?></p></td>        
	    </tr> 
		<tr>
			<td style="height: 15px"></td> 
		</tr>
	</table>

	
	
	<table style="width:100%; /*background:#fcfcfe*/">
	    <tr style="width: 100%">
	        <td><h1 style="padding: 10px 10px 20px 10px;font-size: 12px;color:#222;letter-spacing: .3px;"><b> No. Painters: <SPAN class="ft10"><?php echo $nop; ?></SPAN></b></h1></td>
	        <td><h1 style="padding: 10px 10px 20px 10px;font-size: 12px;color:#222;letter-spacing: .3px;text-align:right"><b> Recommended Start: <SPAN class="ft10"><?php echo $start; ?></SPAN></b></h1></td>
	    </tr>
		<tr>
	        <td><h1 style="color:#222;font-size: 12px;"><b> Litres Paint: <SPAN class="ft10"><?php echo $qty; ?></SPAN></b></h1></td>
	        <td><h1 style="color:#222;font-size: 12px;text-align:right"><b> Expected Completion: <SPAN class="ft10"><?php echo $end; ?></SPAN></b></h1></td>
	    </tr>
		<tr>
	        <td><h1 style="color:#222;font-size: 12px;"><b> &nbsp;</b></h1></td>
	        <td><h1 style="color:#222;font-size: 12px;text-align:right"><b> Total Hours: <SPAN class="ft10"><?php echo $hrs; ?></b></h1></td>
	    </tr>
		
    </table>
	<table style="width:100%; /*background:#fcfcfe*/">
	    <tr>
			<td style="height: 15px"></td>
		</tr>
		<?php
		 if($color_consultant == 1){
		?>
	    <tr>	             
	        <td style="color:#666;font-size: 12px;line-height: 18px;"><?php echo $consultant_text;?>
	    </tr>
		 <?php
			}		
		 ?>		
		<tr>
			<td style="height: 15px"></td>
		</tr>
	</table>
</div> <!-- service div -->



		
	<!-- description div -->
<div id="invoice_body">
    <table style="width: 100%">
	    <tr style="background:#28abe1; width: 100%">
		<?php
			if(count($special) > 0 || count($siteNotes)>0){
				if(count($special) > 0){
					echo '<td style=" padding: 8px 40px; color:#fff; font-size: 16px;text-transform: uppercase;"><b>Special Instruction</b></td></tr>';
					//echo ' <tr><td style="height: 20px"></td></tr>';
					foreach($special as $_special){
						echo "<tr><td style='padding:10px 40px 0px 40px;color:#666;font-size: 12px;'>".$_special."</td></tr>";
					}//foreach
				}
			}
		?>
	    </tr>
    </table>
</div> 
<!-- description div -->
	

	
<?php
	$bca = 1;
	foreach($rooms as $_rooms){

		$backgroundStyle="";
		if($bca%2 != 0){
			$backgroundStyle = 'background:#f5f5f7;margin-top:13px;';
		}
		$bca++;
	?>
	<div id="" style="<?php echo $backgroundStyle; ?>">
		<div id="" style="">
			<div style="padding:0 60px;">
				<!--<table>
					<tr>
						<td style="height: 20px"></td>
					</tr>
				</table>-->
				<?php
					echo '<h1 style="font-size:12px;font-style:normal;text-decoration: none;margin:0;padding:20px 0 0px 0;color:#222;text-transform: uppercase;"><b>'.$_rooms['name'].' - Total Hours: '.$_rooms['hrs'].'</b></h1>';
					echo '<p style="font-size:12px;font-weight:normal;font-style:normal;text-decoration: none;margin:0;padding:0px 0 10px 0;color:#666;">Prep Level '.$_rooms['prep_level'].'</p>';
				?>
				<table>
					<tr style=" width:100%;background:#28abe1;">
						<td style="padding:10px 0 10px 30px;color:#fff; font-size: 12px;"><b>COMPONENT</b></td>
						<td style="padding: 10px;color:#fff; font-size: 12px;"><b>PRODUCT</b></td>
						<td style="padding: 10px;color:#fff; font-size: 12px;"><b>COLOUR</b></td>
						<td style="padding: 10px;color:#fff; font-size: 12px;"><b>TIME (H:M)</b></td>
						<td style="padding: 10px 30px 10px 00px;color:#fff; font-size: 12px;"><b>PAINT (L)</b></td>
					</tr>
				<?php
					foreach($_rooms['components'] as $components){
				?>
						<tr style=" width:100%;">
							<td style="padding: 15px 0 0px 30px;color:#666; font-size: 12px;"><?php echo $components['name'];?></td>
							<td style="padding: 15px 0 0px 10px;color:#666; font-size: 12px;"><?php echo $components['product'];?></td>
							<td style="padding: 15px 0 0px 10px;color:#666; font-size: 12px;"><?php echo $components['color'];?></td>
							<td style="padding: 15px 0 0px 10px;color:#666; font-size: 12px;"><?php echo $components['time'];?></td>
							<td style="padding: 15px 0 0px 10px;color:#666; font-size: 12px;"><?php echo $components['paint'];?></td>
						</tr>
				<?php	
					}
				?>
				</table>
				<table>
					<tr>
						<td style="height: 20px"></td>
					</tr>
				</table>

				<table>
				<?php
					$k = 0;
					foreach ($_rooms['siteNotes'] as $csnk => $csnv) {
						$imgElement='&nbsp;';
						if($csnv['image']!=''){
							//$imgElement = '<img src="'.$siteURL.'/notes/'.$csnv['image'].'" alt="" style="width:100px;">';
							$imgElement = '<span style=""><img src="'.$csnv['image'].'" alt="" style="width:150px;border:1px solid #28abe1;" >';
						}
				?>
					<tr style="">
						<th style="text-align: left;vertical-align: middle;" rowspan="2"><div><?php echo $imgElement;?></div></th>
						<?php
						if($k == 0){
							$k=1;
						?>
							<td style="text-align: left; vertical-align: bottom;padding-bottom: 0px;padding-left: 20px; width:430px;color:#666;font-size: 10px;"><b><?php echo $_rooms['name']."- Site Notes" ?></b></td>
						<?php
						}
						?>
					</tr>
					<tr style=" font-size: 12px;">
						<td style="text-align: left; vertical-align: top;padding-top: 10px;padding-left: 20px; width:430px;color:#666;font-size: 10px;"><?php echo $csnv['description'];?></td>
					</tr>
					<?php
					}
					?>
				</table>
				<table>
					<tr>
						<td style="height: 10px"></td>
					</tr>
				</table>
				<?php
					if(count($_rooms['specialItems'])>0){
				?>
					<table>
						<tr>
							<td style="height: 10px"></td>
						</tr>
						<tr style="">
							<td style="text-align: left; vertical-align: middle;padding-bottom: 5px; width:430px;color:#666;font-size: 12px;letter-spacing: .3px;"><b>SPECIAL ITEMS</b></td>
						</tr>
						<?php
							foreach($_rooms['specialItems'] as $csnk => $csnv){
						?>
								<tr style=" font-size: 12px;">
									<td style="text-align: left; vertical-align: middle;padding-bottom: 10px;padding-top: 7px; width:430px;color:#666;font-size: 12px;"><?= $csnv ?></td>
								</tr>
						<?php
							}
						?>
				</table>
				<?php
					}
				?>
			</div>	
			<table>
				<tr>
					<td style="height: 20px"></td>
				</tr>
			</table>
		</div>
	</div>
<?php
	}
?>
				
	
			
<?php

/********* for MATERIALS  *************/

if(count($materials)>0){

	foreach($materials as $_materials){
		/* rooms heading */
		echo '<div style="background:#fff;">';
		echo '<div style="padding:0 60px;">';
		echo '<h1 style="font-size:12px;font-style:normal;text-decoration: none;margin:0;padding:20px 0 10px 0;color:#222;text-transform: uppercase;"><b>'.$_materials['name'].'</b></h1>';
		echo '<TABLE style=" width:100%;">';
			echo '<THEAD>';
			echo '<TR style="width:100%;background:#28abe1;">';
				echo '<TD style="padding:10px 0 10px 30px;color:#fff; font-size: 12px;"><b>QTY</b></TD>';
				echo '<TD style="padding: 10px;color:#fff; font-size: 12px;"><b>Product</b></TD>';
				echo '<TD style="padding: 10px;color:#fff; font-size: 12px;"><b>Sheen</b></TD>';
				echo '<TD style="padding: 10px 30px 10px 00px;color:#fff; font-size: 12px;"><b>Colour</b></TD>';
			echo '</TR>';
			echo '</THEAD>';
				/* rooms component body */
				foreach($_materials['stocks'] as $stocks){
					echo '<TR style=" width:100%;">';
						echo '<TD style="padding: 15px 0 0px 30px;color:#666; font-size: 12px;">'.$stocks['litres'].'</TD>';
						echo '<TD style="padding: 15px 0 0px 10px;color:#666; font-size: 12px;">'.$stocks['product'].'</TD>';
						echo '<TD style="padding: 15px 0 0px 10px;color:#666; font-size: 12px;">'.$stocks['sheen_name'].'</TD>';
						echo '<TD style="padding: 15px 0 0px 10px;color:#666; font-size: 12px;">'.$stocks['Colour'].'</TD>';
					echo '</TR>';
				}//foreach

			echo '</TABLE>';	
			echo '</div>';	
			echo '</div>';	

	}//foreach
}//if
//exit;
?>

<!--
<div id="invoice_body">
    <table style="width: 100%">
	    <tr style="background:#28abe1; width: 100%">
	        <td style=" padding: 10px 40px; color:#fff; font-size: 13px">&nbsp;</td>                
	    </tr>
    </table>
</div>


<table>
	<tr>
		<td style="height: 20px"></td>
	</tr>
</table>







<div id="" style="">
	<div style="padding:0 40px;">
	<table>
		<tr style=""><td style="text-align: left; vertical-align: middle;padding-bottom: 4px; color:#62615e;font-size: 11px;"><b>ESTIMATED COMPLETION TIME</b></td>
		</tr>
		<tr style=" font-size: 12px;"><td style="text-align: left; vertical-align: middle;padding-bottom: 4px; color:#62615e;font-size: 11px;">6 BUSINESS DAYS</td>
		</tr>
		<tr style=" font-size: 12px;"><td style="text-align: left; vertical-align: middle;padding-bottom: 4px; color:#62615e;font-size: 11px;">Your fixed price fee includes all labour and materials unless otherwise specified.</td>
		</tr>
	</table>
	
	</div>	
	
	<table style="width:100%;">
		<tr style=" width:100%;">
			<td style="padding: 10px 00px 10px 30px;color:#fff; font-size: 13px;"><div><span><img src="http://enacteservices.com/paintpad/image/box-img.png" alt="" style="width:150px;transform: translate(0, 0%);"> </span></div></td>
			<td style="padding: 10px; font-size: 13px;">
				<table class="" style="">
					<tr><td><b>PAINT COLOURS</b></td></tr>
					<tr><td>Refer Above</td></tr>
				</table>
			</td>
			<td style="padding: 10px 0 10px 0;font-size: 13px;text-align:right">
				<table class="" style="">
					<tr>
						<td style="padding:10px 0px 10px 0">Total of fix Price Fee -</td>
						<td style="padding:10px 20px 10px 0">$7263.00</td>
					</tr>
					<tr>
						<td style="padding:10px 0px 10px 0">Plus GST -</td>
						<td style="padding:10px 20px 10px 0">$726.40</td>
					</tr>
					<tr>
						<td style="padding:10px 0px 10px 0"><b>Total Including GST -</b></td>
						<td style="padding:10px 20px 10px 0"><b>$7269.40</b></td>
					</tr>
					<tr>
						<td style="padding:10px 0px 10px 0"><b>Deposit -</b></td>
						<td style="padding:10px 20px 10px 0"><b>$7268.40</b></td>
					</tr>
					<tr style="background:#28abe1;">
						<td style="color:#fff; padding:10px 0px 10px 0">Balance -</td>
						<td style="color:#fff; padding:10px 20px 10px 0">$10268.40</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr style=" font-size: 12px;text-align: right;">
		<td style="color:#fff; padding:10px">&nbsp;</td>
		<td style="color:#fff; padding:10px">&nbsp;</td>
		<td style="text-align: right; vertical-align: middle;padding:30px; color:#62615e;font-size: 11px;"><div><span><img src="http://enacteservices.com/paintpad/image/on-time-logo.png" alt="" style="width:150px;transform: translate(0, 0%);"> </span></div></td>
		</tr>
	</table>
	
	
		<table>
			<tr>
				<td style="height: 10px"></td>
			</tr>
		</table>
</div>




<div id="" style="background:#28abe1;">
	<div style="padding:20px 40px;">
		<table style="width:100%;">
			<tr style="">
				<td style="padding: 10px;color:#fff; font-size: 13px;"><b>ACCEPTANCE ADVICE</b></td>
				<td style="padding: 10px;color:#fff; font-size: 13px;"><b>&nbsp;</b></td>
				<td style="padding: 10px;color:#fff; font-size: 13px;text-align:right"><b>TAX INVOICE</b> ABN 41 147 435 345</td>
			</tr>
		</table>
		<table style="width:100%;">
			<tr style="">
				<td style="padding: 10px;color:#FFF; font-size: 13px;">
				<b>Terms:</b> I/we accept the GST inclusive fixed price fee of <?php//echo '<b>$'.$total.'</b>';?> from Demo Painters as outlined in this quote (No. <?php// echo $qid; ?>) and wish to establish a date of commencment. I will pay the <?//= $depositPercentage ?>% deposit of <?php// echo '<b>$'.$deposit.'</b>';?> either by credit card over the phone by calling or by electronic funds transfer to the account, BSB: , account number. The remainder will be due <?//= $paymentOptionName ?> as per the agreed payment schedule. Recommended commencement date: <b><?php// echo $start;?></b> .
				</td>
			</tr>
		</table>
		<table style="width:100%;">
			<tr style="">
				<td style="padding: 10px;color:#FFF; font-size: 13px;">
				<b>AUTHORISATION</b>
				</td>
			</tr>
		</table>
		<table style="width:100%;">
			<tr style="">
				<td style="padding: 10px;color:#FFF; font-size: 13px;">
				<b>NAME</b>
				</td>
				<td style="padding: 10px;color:#FFF; font-size: 13px;text-align:center">
				<b>SIGNATURE</b>
				</td>
				<td style="padding: 10px;color:#FFF; font-size: 13px;text-align:right">
				<b>DATE</b>
				</td>
			</tr>
		</table>
	</div>	
	<table>
		<tr>
			<td style="height: 10px"></td>
		</tr>
	</table>
</div>
-->







<!-- start -->



