<?php

		$rooms      = '';
		$image      = '';
		$subtotal   = '';
		$gst        = '';
		$total      = '';
		$deposit    = '';
		$balance    = '';
		$start      = '';
		$completion = '';
		$includes   = '';
		$room_summary = '';
		$color_consultant = '';

		$consultant_text = 'A Colour Consultant - Professional On-site Colour Consultation - 1hr - You will receive an individual consultation to assist you select the right colours for you. Selections will be added to our Job Specification Sheet';

	if(isset($data) && count($data)>0){
		//echo '<pre>'; print_r($data['rooms']); exit;
		$rooms      = $data['rooms'];
		$brand_logo = $data['brand_logo'];
		$image      = $data['image'];
		$subtotal   = $data['subtotal'];
		$gst        = $data['gst'];
		$total      = $data['total'];
		$deposit    = $data['deposit'];
		$balance    = $data['balance'];
		$start      = $data['start'];
		$completion = $data['completion'];
		$includes   = $data['includes'];	
		$special    = $data['spl'];
		$room_summary = $data['rooms_summary'];
		$color_consultant = $data['color_consultant'];

		/*$room_names = array_column($rooms, 'name');

		echo '<pre>'; print_r($room_names); exit;*/

	} //data

	if(isset($cdata) && count($cdata)>0){
		$cname  = $cdata['cname'];
		$cemail = $cdata['cemail'];
		$cphone = $cdata['cphone'];
	} //cdata

	if(isset($qdata) && count($qdata)>0){
		$qdesc = $qdata['description'];
		$qid   = $qdata['number'];
		$qdate = $qdata['date'];
	} //qdata

?>

<style type="text/css">

#invoice_total table
{
    width:100%;
    border-top: 3px solid #ccc;
    border-spacing:0;
    border-collapse: collapse;      
    margin-top:5mm;
}

.brd-non{border-top: 0px solid #ccc !important;
    border-spacing:0 !important;
    border-collapse: inherit !important;  }

td, p, h1{font-family: Arial, Helvetica, sans-serif;}


</style>

<!-- main DIV -->



<div class="" style="margin:0 auto; width:1000px; overflow:hidden;">
<div>

	<table>
		<tr>
			<td style="height: 15px"></td>
		</tr>
	</table>

	<table cellpadding="0" cellspacing="0" style="border-bottom: 2px solid #e1e1e1; width:100%;">
		<tr style=" font-size: 12px;">
			<td style="text-align: left;vertical-align: bottom;"> <div> <span> <img src="<?php echo $brand_logo; ?>" alt="" style="width:100px;transform: translate(0, 50%);"> </span></div></td>
			<td style="">&nbsp;</td>
			<td style="">&nbsp;</td>
			<td style="text-align: right; vertical-align: bottom;padding-bottom: 4px;"><h1 style="color:#0a80c5; font-size: 19px; font-weight: normal; text-align: right;">INTERIOR PAINTING QUOTE</h1></td>
		</tr>
		
		
		
		
		<tr style=" font-size: 12px; text-align: right; float: right;">
			<td style="height:2px">&nbsp;</td>
			<td style="height:2px">&nbsp;</td>
			<td style="height:2px">&nbsp;</td>
			
			<td style="text-align: right">
			    <table class="" style="border:0px; background:#ececec;">
					<tr>
                        <td style=" border:0px;padding: 5px 8px 0px 5px;color:#525251;font-weight: normal;text-align: left;">
	                        <table class="" style="border:0px; background:#ececec;">
	                        <tr>
	                        <td><b style="color:#0a86c2; font-size: 12px; text-transform: uppercase;">Quote No.</b></td>
	                        </tr>
	                        <tr>
	                        <td style="font-size: 11px;color:#62615e"> <?php echo $qid; ?></td>
	                        </tr>
	                        </table>
                        </td>
                        
                        <td style=" border:0px;padding: 5px 5px 0px 2px;color:#525251; font-weight: normal;text-align: left;">
	                        <table class="" style="border:0px; background:#ececec;">
	                        <tr>
	                        <td><b style="color:#0a86c2; font-size: 12px; text-transform: uppercase; ">Quote Date</b></td>
	                        </tr>
	                        <tr>
	                        <td style="font-size: 11px;color:#62615e"><?php echo $qdate; ?></td>
	                        </tr>
	                        </table>
                        </td>

                        <td style=" border:0px;padding: 5px 5px 0px 5px;color:#525251; text-align: left;">
	                        <table class="" style="border:0px;">
	                        <tr>
	                        <td><img width= "100px" style="padding: 0;" src="<?php echo $image; ?>" alt=""></td>
	                        </tr>
	                        <tr>
	                        <td></td>
	                        </tr>
	                        </table>
                        </td>
                    </tr>
                </table>
			</td>
		</tr>
		
		
	</table>
	

</div>  <!-- main DIV -->





<!-- client details DIV -->
<div>


<table>
		<tr>
			<td style="height: 10px"></td>
		</tr>
</table>

	
	<table>
		
		<tr>
			<td style="width:18%; margin-bottom: 13px;color:#0a80c5; font-size: 13px; font-weight: bold; float: left;width: 100%; text-align: left;"><b><?php echo $cname; ?></b></td>
			<td style="width:30%; margin-bottom: 13px;color:#0a80c5; font-size: 13px; font-weight: bold; float: left;width: 100%; text-align: left;"><b>Site Details</b></td>
			<td style="width:10%; margin-bottom: 13px;color:#0a80c5; font-size: 13px; font-weight: bold; float: left;width: 100%; text-align: left;"><b>Quoted by</b></td>
		</tr>
		<tr>
			<td style="font-size: 11px;color:#62615e"><b>Contact:<?php echo $cname; ?></b></td>
			<td style="font-size: 11px;color:#62615e"><b>Contact:<?php echo $cname; ?></b></td>
			<td style="font-size: 11px;color:#62615e"><b><?php echo ucfirst($sub_name); ?></b></td>
		</tr>
		<tr>
			<td style="font-size: 11px;color:#62615e"><b>Mobile:<?php echo $cphone; ?></b></td>
			<td style="font-size: 11px;color:#62615e"><b>Mobile:<?php echo $cphone; ?></b></td>
			<td style="font-size: 11px;color:#62615e"><?php echo $sub_phone; ?></td>
		</tr>
		<tr>
			<td style="font-size: 11px;color:#62615e"><?php echo $client_addr; ?></td>
			<td style="font-size: 11px;color:#62615e; vertical-align: text-top;"><?php echo $siteAddress; ?></td>
			<td style="font-size: 11px;color:#62615e; vertical-align: text-top;"><?php echo $sub_email; ?></td>
		</tr>	

	</table>

	<table>
		<tr>
			<td style="height: 10px"></td>
		</tr>
</table>

</div> <!-- client details DIV -->





<!-- description div -->
<div id="invoice_body">
    <table style="width: 100%">
	    <tr style="background:#0684c1; width: 100%">
	        <td style=" padding: 10px; color:#fff; font-size: 13px"><b>Description</b></td>                
	    </tr>
    
	    <tr style="width: 100%">
	        <td style="background:#ececec;padding: 10px;font-size: 12px;color:#62615e"><b><?php echo $qdesc; ?></b><br><p><b>ROOMS:</b><?php echo $room_summary; ?></p></td>               
	    </tr>   

    </table>
</div> <!-- description div -->

<!-- service div -->
<div id="invoice_body">

<table>
		<tr>
			<td style="height: 10px"></td>
		</tr>
</table>


    <table style="width:100%">
	    <tr style="">
	        <td><h1 style="font-size:13px;font-weight:normal;color:#1d7db3;">PAINITING SERVICE INCLUDES</h1></td>
	    </tr>
	    <tr style="">
	        <td  style="font-size:11px;color:#62615e;line-height: 20px;"><?php echo $includes;?></td>                
	    </tr>
    </table>

<table>
		<tr>
			<td style="height: 10px"></td>
		</tr>
</table>

</div> <!-- service div -->

<!-- additional details div -->
<div id="" style="background:#ececec;">
<table>
		<tr>
			<td style="height: 10px"></td>
		</tr>
</table>
    <table style="width: 100%; padding: 0 10px">
	    <tr style="">
	        <td style=""><h1 style="font-size:13px;color:#1d7db3;font-weight:normal;font-style:normal;text-decoration: none">ADDITIONAL DETAILS</h1></td>
	    </tr>

	        <?php 

		        if($color_consultant == 1){
		        	echo "<tr><td style='color:#62615e; font-size: 11px; line-height: 20px;'>".$consultant_text."</td></tr>";
		        }
		        if(count($special)>0){
			        foreach($special as $_special){
			        	echo "<tr><td style='color:#62615e; font-size: 11px; line-height: 20px;'>".$_special."</td></tr>";

			        }//foreach
			    }//if

	        ?>

	    
    </table>
    <table>
		<tr>
			<td style="height: 10px"></td>
		</tr>
</table>
</div> <!-- additional details div -->






<!-- estimate div -->
<div id="" style="padding: 10px;">
    <table style="width: 100%">
	    <tr>
	        <td>
	        <b style="font-size:13px;color:#0684c1;">ESTIMATED COMPLETION TIME </b> &nbsp; <b style="font-size:13px;color:#62615e;"><?php echo ucwords($completion);?></b>
	        </td>
	    </tr>
	    <tr>
	        <td style="color:#62615e; font-size: 11px;"><b>Your fixed-price fee includes all labour and materials unless otherwise specified</b></td>      
	    </tr>
    </table>
</div> <!-- estimate div -->






<!-- main DIV -->
<div>
<table>
		<tr>
			<td style="height: 10px"></td>
		</tr>
</table>

    <table class="heading" style="border-top: 2px solid #e1e1e1; width:100%;">

		<tr>
			<td>
			    <div> <img src="http://enacteservices.com/paintpad/image/paintdulux.png" alt="" style="width:80px; "> </div>
			</td>

			<td style="padding-left: 30px;">
				<h1 style="color:#0684c1;font-size: 13px;">PAINT COLOURS</h1></br></br>
		
			    <p style="font-size: 11px;color:#62615e;"><b>Refer to Job Specification</b></p>
			</td>

			<td style="border:0px; text-align: right; padding-top: 20px;">
				<table>
					<tr style="">
				     <td style="border-bottom: 2px solid #c7e3f2;font-size: 13px;color:#62615e;padding-bottom: 8px; padding-right: 11px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Total of fixed price fee</b>&nbsp;&nbsp;&nbsp;<?php echo '$'.$subtotal;?></td>
				    </tr>

				    <tr style="">
				    <td style="border-bottom: 2px solid #c7e3f2;font-size: 13px;color:#62615e;padding-bottom: 8px; padding-right: 11px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Plus GST</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo '$'.$gst;?></td>			       
				    </tr>
 				
				    <tr style="">			   
				    <td style="border-bottom: 2px solid #c7e3f2;font-size: 13px;color:#62615e;padding-bottom: 8px; padding-right: 11px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Total Including GST</b>&nbsp;&nbsp;&nbsp;<?php echo '$'.$total;?></td>	
				    </tr>
			
				    <tr style="">
				    <td style="border-bottom: 2px solid #47a4d1;font-size: 13px;color:#62615e;padding-bottom: 8px; padding-right: 11px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Deposit</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo '$'.$deposit;?></td>			      
				    </tr>
				
				    <tr style="">		
				     <td style="font-size: 13px;color:#007aa9;padding-right: 11px"><b>Balance</b>&nbsp;&nbsp;&nbsp;<?php echo '$'.$balance;?></td>	
				    </tr>
			    </table>
			</td>
		</tr>

    </table>
<table>
		<tr>
			<td style="height: 10px"></td>
		</tr>
</table>
</div>  <!-- main DIV -->









<div style="background:#0684c1;width: 100%">
    <table>
	    <tr style="background:#0684c1;width: 100%;font-size: 13px;  font-weight: bold;">
	        
	        <td style="color:#fff;padding-left: 10px;">ACCEPTANCE ADVICE</td>
	    
	        
	        <td style="color:#fff;padding-left: 300px; text-align: right;">
	        	<table>
					<tr style="width: 100%">
						<td  style="color:#fff; ">TAX INVOICE</td>           
				        <td style="color:#fff;">ABN</td>           
				        <td style=" color:#fff;">10245121</td>  
					</tr>
				</table>
	        </td>


	                 
	    </tr>
    </table>     
</div> 



<table>
		<tr>
			<td style="height: 10px"></td>
		</tr>
</table>


<div id="" style=";padding:10px;">
    <table>	    
	    <tr style="">
	        <td style="color:#62615e; font-size: 11px"><b>Terms:</b> I/we accept the GST inclusive fixed price fee of <?php echo '<b>$'.$total.'</b>';?> from Demo Painters as outlined in this quote (No. <?php echo $qid; ?>) and wish to establish a date of commencment. I will pay the 3.73% deposit of <?php echo '<b>$'.$deposit.'</b>';?> either by credit card over the phone by calling or by electronic funds transfer to the account, BSB: , account number. The remainder will be due within 7 days of completion as per the agreed payment schedule. Recommended commencement date:   DATE.</td>                
	    </tr>
    </table>
</div>

<table>
		<tr>
			<td style="height: 10px"></td>
		</tr>
</table>


<!-- main DIV -->
<div>
    <table class="heading" cellpadding="0" cellspacing="0" style="border-top: 0px solid #e1e1e1; width:100%;">
       
		<tr>
			<td style="padding-bottom: 10px;">
				<h1 style="color:#1d7db3; font-size: 13px; font-weight: bold;">AUTHORISATION</h1>
			</td>
		</tr>

		<tr>
			<td style="width:32%;border-bottom: 2px solid #aeaeae;border-right: 5px solid #fff;padding-bottom: 40px">
				<h1 style="font-size: 13px; font-weight: normal;color:#62615e">NAME</h1>
			</td>		
			<td style="width:32%;border-bottom: 2px solid #aeaeae;border-right: 5px solid #fff;padding-bottom: 40px">
				<h1 style="font-size: 13px; font-weight: normal;color:#62615e">SIGNATURE</h1>
			</td>
			<td style="width:32%; border-bottom: 2px solid #aeaeae;border-left: 5px solid #fff;padding-bottom: 40px">
				<h1 style="font-size: 13px; font-weight: normal;color:#62615e">DATE</h1>
			</td>
		</tr>

    </table>
</div>  <!-- main DIV -->

</div>