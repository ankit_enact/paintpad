<?php

setlocale(LC_MONETARY, 'en_US.UTF-8');
?>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">


	<style type="text/css">

		#invoice_total table
		{
			width:100%;
			border-top: 3px solid #ccc;
			border-spacing:0;
			border-collapse: collapse;      
			margin-top:5mm;
		}

		.brd-non{border-top: 0px solid #ccc !important;
			border-spacing:0 !important;
			border-collapse: inherit !important;  }

			/* td, p, h1{font-family: 'DejaVu Sans';} 
			td, p, h1{font-family: 'Open Sans', sans-serif;}
			body{font-family: 'Open Sans', sans-serif;}*/
			table {
				border-spacing: 0;
				border-collapse: collapse;
			}

		</style>

<table>
	<tr>
		<td style="height: 10px"></td>
	</tr>
</table>

<?php

$payments = $data['PartPayment'];

if(count($payments)>0){ ?>

<div id="" style="<?= $backgroundStyle ?>">
	<table style="width:100%;">
		<thead style="">
			<tr style=" width:100%;background:#28abe1;">
				<td style="padding: 8px 40px;color:#fff; font-size: 14px;text-transform: uppercase;"><b>AGREED PROGRESS PAYMENTS</b></td>
			</tr>
		</thead>
	</table>
	<div style="padding:0 40px;">
		<h1 style="font-size:11px;font-style:normal;text-decoration: none;margin:0;padding:17px 0 15px 0;color:#222;"><b style="padding-right: 20px; float: left; width: 190px">TOTAL PROJECT VALUE &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b> <b style="color:#59595b; font-size: 11px; padding-left: 20px;"><?= money_format("%.2n",$data['total_amount_pp']); ?> (inc. GST)</b></h1>

		<table style="width:100%;">
			<thead style="">
				<tr style=" width:100%;background:#28abe1;">
					<td width="160px" style="padding: 10px 10px;color:#fff; font-size: 12px;text-transform: uppercase;"><b>Payment Date</b></td>
					<td width="160px" style="padding: 10px 10px;color:#fff; font-size: 12px;text-transform: uppercase;"><b>Description</b></td>
					<td style="padding: 10px 10px;color:#fff; font-size: 12;"><b>PAYMENT AMOUNT (inc. GST)</b></td>
					<td width="160" style="padding: 10px 10px;color:#fff; font-size: 12;"><b>BALANCE (inc. GST)</b></td>
				</tr>
			</thead>
			<tbody>
				<?php
				// echo "<pre>"; echo round(1.95283, 2);die;
				// $lastPay = [];
				// foreach($payments as $_payments1){
				// 	if($_payments1['descp'] != "BAL-PP-*123#"){
				// 		$newPay[] = $_payments1;
				// 	}else{
				// 		$_payments1['descp'] = 'Balance';
				// 		$lastPay[] = $_payments1;
				// 	}
					
				// }				
				// $paymentsNew = array_merge($newPay,$lastPay);

				$newPay = [];
				$lastPay = [];
				foreach($payments as $_payments1){
					if($_payments1['final_balance'] == 0){
						$newPay[] = $_payments1;
					}else{
						
						$_payments1['descp'] = 'Balance';				
						$lastPay[] = $_payments1;
					}
					
				}				
				$paymentsNew = array_merge($newPay,$lastPay);


				// echo "<pre>";print_r($paymentsNew);die;
				// echo "<pre>";
				// foreach($paymentsNew as $_payments){
				// 	print_r($paymentsNew);
				// }die('<<-->>');
				$payDone = 0;
				foreach($paymentsNew as $_payments){
					// if($_payments['amount'] > '0'){ 
						?>
					<tr style=" width:100%;">
						<td style="padding: 13px 10px 0px;color:#59595b; font-size: 12px;"><b><?= $_payments['payment_date'] ?></b></td>
						<td style="padding: 13px 10px 0;color:#59595b; font-size: 12px;"><b><?php echo $_payments['descp']; ?></b></td>
						<td style="padding: 13px 10px 0;color:#59595b; font-size: 12px;"><b><?= money_format("%.2n",$_payments['amount']) ?></b></td>


						<?php 
							if(is_numeric($_payments['amount'])){
								$payDone += number_format((float)$_payments['amount'], 2, '.', '');
								// $payDone = ceil($payDone);
							}
							
						 ?>
						<td style="padding:13px 10px 0;color:#59595b; font-size: 12px;"><b>
							<?php
								$_pay = number_format((float)$data['total_amount_pp'], 2, '.', '') - $payDone;
								$_payments['balance'] = number_format((float)$_pay, 2, '.', '');
								
								if($_payments['balance'] < 0.1 || $_payments['balance'] < 0.10){
									$_payments['balance'] = 0.00;
								}
								echo money_format("%.2n",abs($_payments['balance']));
							?>
						</b></td>
					</tr>
				<?php
					// }
						
				} 
				 ?>

				

				
				
			</tbody>
		</table>
		<h1 style="font-size:11px;font-style:normal;text-decoration: none;margin:0;padding:42px 0 0px 0;color:#222;"><b style="padding-right: 20px; float: left; width: 190px;text-transform: uppercase;">Notes</h1>
		<h3 style="color:#59595b;font-size: 11px;font-family:open;  margin:10px 0px 0px; padding:0px;"><?php echo $data['paymentNotes']; ?></h3>

	</div>	
</div>	



<?php }
// exit;  
?>
