<?php
use yii\helpers\Url;
use kartik\typeahead\Typeahead;
use yii\web\JsExpression;
use kartik\tabs\TabsX;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use frontend\assets\ContactAsset;
ContactAsset::register($this);
use app\models\TblAppointmentType;
use app\models\TblMembers;
use yii\helpers\ArrayHelper;
?>
<?php

$dat=[];
$dataSub=[];
foreach($dataContact as $data)
{
  $dat[$data->contact_id]=$data->name;
  $datSub[$data->contact_id]=$data->subscriber_id;
}

?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> 
<style>

	#map {
    height: 142px;
}
	html, body {
		height: 100%;
		margin: 0;
		padding: 0;
	}
	#floating-panel {
		position: absolute;
		top: 10px;
		left: 25%;
		z-index: 5;
		background-color: #fff;
		padding: 5px;
		border: 1px solid #999;
		text-align: center;
		font-family: 'Roboto','sans-serif';
		line-height: 30px;
		padding-left: 10px;
	}

	#map-canvas, #map-canvas-site, #static-map-canvas {
		height: 150px;
		margin: 0;
	}
	.centerMarker, .siteCenterMarker{
		position: absolute;
		background: url(<?= Url::base(true)."/image/mapIcon.png" ?>) no-repeat;
		top: 54%;
		left: 50%;
		z-index: 1;
		margin-left: -10px;
		margin-top: -34px;
		height: 34px;
		width: 20px;
		cursor: pointer;
	}
	.pac-container {
		z-index: 1050 !important;
	}

	.existing_client_list{
		max-height: 190px;
		overflow-y: scroll;
	}
	.existing_client_list li{
		cursor: pointer;
	}
	ul.existing_client_list img {
		height: 45px;
		width: 45px;
		border-radius: 50%;
		margin-right: 10px;
	}
	#client_image img {
		height: 60px;
		width: 60px;
		border-radius: 50%;
	}

	#google-map-overlay{
		height : 150px;
		width: 270px;
		background: transparent;
		position: absolute;
		top: 0px; 
		left: 0px; 
		z-index: 99;
	}

	#map-canvas-site-overlay {
		height : 150px;
		width: 270px;
		background: transparent;
		position: absolute;
		top: 0px; 
		left: 0px; 
		z-index: 0;
	} 

	/*=== Naveen ====*/
	#InteriorQuote .modal-content {
		background: #f6fbff;
	}
	#InteriorQuote .modal-body {
		padding: 0px 0 0;
		background: #fff;
		margin-top: 30px;
	}
	.Detail_prt_div {
		width: 100%;
		margin-top: -18px;
	}
	.Detail_inner .nav.nav-tabs a.nav-link {
		padding: 3px 6px;
		display: inline-block;
	} 
	.Detail_inner .nav.nav-tabs {
		float: left;
		width: 100%;
	}

	.quote_right_sec {
		float: right;
		width: 101%;
		padding: 60px 20px 20px 20px;
	}
	.quoteTr{
		cursor:pointer;
	}
	.mainDiv {
		height:750px;
	}
	.text {
		position: relative;
		font-size: 14px;
		color: black;
		width: 250px; 
	}

	.text-concat {
		position: relative;
		display: inline-block;
		word-wrap: break-word;
		overflow: hidden;
		max-height: 1.6em; 
		line-height: 0.2em;
		text-align:justify;
	}

	.text.ellipsis::after {
		content: "...";
		position: absolute;
		right: -12px; 
		bottom: 4px;
	}
	.cnt_img img{
		border-radius: 50%;
	}
	
	.crop {
		white-space: nowrap;
		width: 12em;
		overflow: hidden;
		width: 400px;
	}
	.crop::after {
		content: "...";
	}
	#blah
	{
	    height: 90px;
	    width: 90px;
	}




	.tabContact.card {
    border: 0;
    box-shadow: none;
    width: 69% !important;
    float: right;
    margin-left: 1px;
}

	/*#myModal a {
    display: none;
	}*/
	
</style>
<?php
	$this->registerCssFile(Yii::getAlias('@web').'/frontend/web/css/style.css',['depends' => [yii\web\JqueryAsset::className()]]);
	$this->registerCssFile(Yii::getAlias('@web').'/frontend/web/css/bootstrap-select.min.css',['depends' => [yii\web\JqueryAsset::className()]]);
	$this->title = "PaintPad||Contact"; 


	$commonLat = 28.517957;
	$commonLng = -81.36591199999999;

	$commonAddress = "2000 S Mills Ave, Orlando, FL 32806, USA";
	$commonStreet = "2000 South Mills Avenue";
	$commonSuburb = "Orlando";
	$commonState = "Florida";
	$commonZip = "32806";
	
	$con=[];
	foreach($model as $c)
	{
		$con[]=$c->contact_id;
		$first = reset($con);
	}
	// print_r($con);
	// 
	$sid = Yii::$app->user->id;
	//echo $sid;
 ?>

 <div class="wrap">
<input id="latlng" type="hidden" value="40.714224,-73.961452">
<input id="submit" type="button" value="Reverse Geocode" style="display:none">	
<input type="hidden" id="detailsContact" value="<?php echo $first;?>">	
<input type="hidden" id="subscriber_id" value="<?php echo $sid;?>">	
<input type="hidden" id="quote_page" value="1">
	
	<div class="quote_detail_wrap">
		<div class="container">
			<div class="quote_detail_inner">
				<!-- Nav tabs -->

				<div class="quote_left_fix text-center" >	
						<input type="text" id="searchContactName" class="form-control searchfield" name="Search" value="" placeholder="Search">
				  	
							 
							<div class="form-group search_div mCustomScrollbar" >
								
								<ul class="contact_list">
								
									<?php										
										foreach($model as $model_contact)
										{											
											if($model_contact->image=="")
											{
												$model_contact->image="avtar.png";
											}
											echo "<li class='tosearch' data-id='".$model_contact->contact_id."'>";
											echo '<span class="cnt_img"><img src="'.Url::base().'/uploads/'.$model_contact->image.'" height="58px"	 width="58px" style="border-radius: 50%;"></span>';
											echo '<div class="contact_person_detail">';
											echo '<p class="contact_nameSearch">'.$model_contact->name.'</p>';
											echo '<small>'.$model_contact->phone.'</small>';
											echo '</div>';
											echo "</li>";
										}
									
									?>
									
									
								</ul><!-- cotact_list -->
								
								
							</div><!-- search_div -->
							
							<a href="#" data-toggle="modal" data-target="#addContact" class="add_contact text-right"><img src="<?php echo Url::base();?>/uploads/contact_blue.png"></a>
						</div><!-- quote_left_fix -->
			<div>
				<ul class="nav nav-tabs nav-justified contact_tabs_list">
					<li class="nav-item">
						<a class="nav-link active" data-toggle="tab" href="#detail" role="tab">Details</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#quotes" role="tab">Quotes</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#communication" role="tab">Communications</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#invoices" role="tab">Invoices</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#appointment" role="tab">Appointments</a>
					</li>
				</ul>
			</div>
			<div class="mainDiv">
				<!-- Tab panels -->
				<div class="tab-content card tabContact">
					<!--Panel 1-->
					<div class="tab-pane fade in show active" id="detail" role="tabpanel">

			
						<div class="quote_right_sec">
							<div class="contact_detail_heading">
								<h3>Personal Profile</h3>
								<a href="#" class="edit text-right updateContactIcon" data-toggle="modal" data-target="#addContact" ><img src="<?php echo Url::base();?>/image/edit_new.png"></a>
							</div><!-- contact_detail_heading -->
							<div class="personal_deatil row">
								<div class="personal_inner left col text-center">
									<span class="personal_profile"><img src="<?php echo Url::base();?>/image/profile.png"></span>
									<h4 class="contact_name">Adam ericesson</h4>
									<h5 class="contact_email">adamericesson@gmail.com</h5>
									<h5 class="contact_phn">0419565575</h5>
								</div><!-- personal_left -->
								<div class="personal_inner mid col">
									<h5>Address</h5>
									<p class="addressClicked">1000 kaley Orlando FL 32804 united States</p>
									<div class="map_location" id="map">
										<span class="location_icon"><img src="<?php echo Url::base();?>/image/location.png"></span>
										<div class="location_map"><img src="<?php echo Url::base();?>/image/contact_map.png"></div>
									</div><!-- MAP LOCATION -->
								</div><!-- personal_left -->
								<div class="personal_inner right col">
									<ul class="personal_count">
										<li>
											<h4 class="contactsIcon">10<h4>
											<span><img src="<?php echo Url::base();?>/image/mail.png"></span>
										</li>
										<li>
											<h4 class="docsIcon">8<h4>
											<span><img src="<?php echo Url::base();?>/image/Invoices_icon.png"></span>
										</li>
										<li>
											<h4 class="quotesIcon">8<h4>
											<span><img src="<?php echo Url::base();?>/image/Quote_icon.png"></span>
										</li>
										<li>
											<h4 class="jssIcon">12<h4>
											<span><img src="<?php echo Url::base();?>/image/calender_new.png"></span>
										</li>
									</ul><!-- personal_count -->
								</div><!-- personal_left -->
							</div><!-- personal_deatail -->
							<div class="contact_detail_heading">
								<h3>Quotes</h3>
								<a class="edit text-right" data-target="#smallShoes" data-toggle="modal"><img src="<?php echo Url::base();?>/image/add_quote_con.png"></a>
							</div><!-- contact_detail_heading -->
							<div class="personal_deatil" id="detailsQuotesShow">
								<ul class="quotes_list">
									<li ><small class="detailsQuotesMonth">May</small><h4 class="detailsQuotesDate">01</h4></li>
									<li><span class="detailsQuotesId">142</span><h3 class="contact_name">Adam Ericesson</h3></li>
									<li><p class="detailsQuotesdescription">Special items Testing </p></li>
									<li><div class="interior"><img src="<?php echo Url::base();?>/image/quotes_Interior_icon.png"><span class="detailsQuotesType">Interiors</span></div></li>
									<li><div class="interior"><img src="<?php echo Url::base();?>/image/quote_price-icon.png"><span class="detailsQuotesprice">$1645.54</span></div></li>
									<li><div class="compt_Action"><img src="<?php echo Url::base();?>/image/complet.png"><span>Completed</span></div></li>
								</ul><!-- quotes_list -->								
							</div><!-- personal_detail -->
						</div><!-- quote_right_sec -->
					</div>
					<!--/.Panel 1-->
					<!--Panel 2-->
					<div class="tab-pane fade" id="quotes" role="tabpanel">

						<div class="quote_right_sec contactQuoteSection">
							<h3>Quotes</h3>
							<a href="#" class="edit text-right"><img src="<?php echo Url::base();?>/image/add_quote_con.png"></a>
							<div class="mCustomScrollbar scrollbarqoute">
						<div class="contactPageQuote contact_quotes  ">
							
						  
							<ul class="quotes_list">
								<li><small>May</small><h4>01</h4></li>
								<li><span>142</span><h3 class="name">Adam Ericesson</h3></li>
								<li><p>Special items Testing </p></li>
								<li><div class="interior"><img src="<?php echo Url::base();?>/image/quotes_Interior_icon.png"><span>Interiors</span></div></li>
								<li><div class="interior"><img src="<?php echo Url::base();?>/image/quote_price-icon.png"><span>$1645.54</span></div></li>
								<li><div class="compt_Action"><img src="<?php echo Url::base();?>/image/complet.png"><span>Completed</span></div></li>
							</ul><!-- quotes_list -->
							<ul class="quotes_list">
								<li><small>May</small><h4>01</h4></li>
								<li><span>142</span><h3 class="name">Adam Ericesson</h3></li>
								<li><p>Special items Testing </p></li>
								<li><div class="interior"><img src="<?php echo Url::base();?>/image/quotes_Interior_icon.png"><span>Interiors</span></div></li>
								<li><div class="interior"><img src="<?php echo Url::base();?>/image/quote_price-icon.png"><span>$1645.54</span></div></li>
								<li><div class="compt_Action"><img src="<?php echo Url::base();?>/image/complet.png"><span>Completed</span></div></li>
							</ul><!-- quotes_list -->
							<ul class="quotes_list">
								<li><small>May</small><h4>01</h4></li>
								<li><span>142</span><h3 class="name">Adam Ericesson</h3></li>
								<li><p>Special items Testing </p></li>
								<li><div class="interior"><img src="<?php echo Url::base();?>/image/quotes_Interior_icon.png"><span>Interiors</span></div></li>
								<li><div class="interior"><img src="<?php echo Url::base();?>/image/quote_price-icon.png"><span>$1645.54</span></div></li>
								<li><div class="compt_Action"><img src="<?php echo Url::base();?>/image/complet.png"><span>Completed</span></div></li>
							</ul><!-- quotes_list -->
							<ul class="quotes_list">
								<li><small>May</small><h4>01</h4></li>
								<li><span>142</span><h3 class="name">Adam Ericesson</h3></li>
								<li><p>Special items Testing </p></li>
								<li><div class="interior"><img src="<?php echo Url::base();?>/image/quotes_Interior_icon.png"><span>Interiors</span></div></li>
								<li><div class="interior"><img src="<?php echo Url::base();?>/image/quote_price-icon.png"><span>$1645.54</span></div></li>
								<li><div class="compt_Action"><img src="<?php echo Url::base();?>/image/complet.png"><span>Completed</span></div></li>
							</ul><!-- quotes_list -->
							<ul class="quotes_list">
								<li><small>May</small><h4>01</h4></li>
								<li><span>142</span><h3 class="name">Adam Ericesson</h3></li>
								<li><p>Special items Testing </p></li>
								<li><div class="interior"><img src="<?php echo Url::base();?>/image/quotes_Interior_icon.png"><span>Interiors</span></div></li>
								<li><div class="interior"><img src="<?php echo Url::base();?>/image/quote_price-icon.png"><span>$1645.54</span></div></li>
								<li><div class="compt_Action"><img src="<?php echo Url::base();?>/image/complet.png"><span>Completed</span></div></li>
							</ul><!-- quotes_list -->
						</div><!-- quote_right_sec -->
					</div>
					  </div>
					</div>
					<!--/.Panel 2-->
					<!--Panel 3-->
					<div class="tab-pane fade" id="communication" role="tabpanel">
						
					
							<div class="mail_box">
								<div><h1 class='pull-right createCommunications'>Create</h1></div>
								
									<div class="quote_right_sec communicationListing">
								<div class="mail_heading">03 May 2018</div><!-- mail_heading -->
								<div class="inner_mail_box">
									<h4 class="mail_sub">P1404 Professional Painting Quote</h4>
									<div class="form-group">
										<label>From</label>
										<p>David Esson</p>
									</div>
									<div class="form-group">
										<label>Date</label>
										<p>Wed Jan 24 2018 11:45</p>
									</div>
									<div class="form-group">
										<label>To</label>
										<p>info@paintpad.com.au</p>
									</div>
									<div class="form-group">
										<label>To</label>
										<p>info@paint.com.au, paintpad@gmail.com</p>
									</div>
									<div class="mail-content-box">
										<p>Bert Blogg hi,</p>
										<p>Great to meet with you,</p>
										<p>Thank you for the opportunity to provide you with our exceptional painting services.</p>
										<p>Please find attached our quotation as well as some supporting information about about our fine service you.</p>
										<a href="#" class="text-right">View More</a>
									</div>
								</div>
							</div>
						
						<!-- mail_box -->
						</div><!-- quote_right_sec -->
					</div><!--/.Panel 3-->
					<div class="tab-pane fade" id="invoices" role="tabpanel">
					
						<div class="quote_right_sec">
							<div class="appointment_box">
								<table class="invoice_table table table-borderless table-condensed table-hover">
									<thead class="invoicehead"> 
										<tr>
											<th>ID</th>
											<th>Quote ID</th>
											<th>Quote Description</th>
											<th>Description</th>
											<th>Amount</th>
											<th>Create/Send</th>
											<th>Paid</th>
											<th>Date</th>
										</tr>
									</thead>
									<tbody class="invoiceBody mCustomScrollbar" id="quotesTbody">
										<tr>
											<td>1632</td>
											<td>4563</td>
											<td>Clone of special items</td>
											<td>Deopsit</td>
											<td>$159.32</td>
											<td>No</td>
											<td>-</td>
											<td>03-04-2018</td>
										</tr>
										<tr>
											<td>1632</td>
											<td>4563</td>
											<td>Clone of special items</td>
											<td>Deopsit</td>
											<td>$159.32</td>
											<td>No</td>
											<td>-</td>
											<td>03-04-2018</td>
										</tr>
										<tr>
											<td>1632</td>
											<td>4563</td>
											<td>Clone of special items</td>
											<td>Deopsit</td>
											<td>$159.32</td>
											<td>No</td>
											<td>-</td>
											<td>03-04-2018</td>
										</tr>
										<tr>
											<td>1632</td>
											<td>4563</td>
											<td>Clone of special items</td>
											<td>Deopsit</td>
											<td>$159.32</td>
											<td>No</td>
											<td>-</td>
											<td>03-04-2018</td>
										</tr>
										<tr>
											<td>1632</td>
											<td>4563</td>
											<td>Clone of special items</td>
											<td>Deopsit</td>
											<td>$159.32</td>
											<td>No</td>
											<td>-</td>
											<td>03-04-2018</td>
										</tr>
										<tr>
											<td>1632</td>
											<td>4563</td>
											<td>Clone of special items</td>
											<td>Deopsit</td>
											<td>$159.32</td>
											<td>No</td>
											<td>-</td>
											<td>03-04-2018</td>
										</tr>
										<tr>
											<td>1632</td>
											<td>4563</td>
											<td>Clone of special items</td>
											<td>Deopsit</td>
											<td>$159.32</td>
											<td>No</td>
											<td>-</td>
											<td>03-04-2018</td>
										</tr>
										<tr>
											<td>1632</td>
											<td>4563</td>
											<td>Clone of special items</td>
											<td>Deopsit</td>
											<td>$159.32</td>
											<td>No</td>
											<td>-</td>
											<td>03-04-2018</td>
										</tr>
										<tr>
											<td>1632</td>
											<td>4563</td>
											<td>Clone of special items</td>
											<td>Deopsit</td>
											<td>$159.32</td>
											<td>No</td>
											<td>-</td>
											<td>03-04-2018</td>
										</tr>
										<tr>
											<td>1632</td>
											<td>4563</td>
											<td>Clone of special items</td>
											<td>Deopsit</td>
											<td>$159.32</td>
											<td>No</td>
											<td>-</td>
											<td>03-04-2018</td>
										</tr>
										<tr>
											<td>1632</td>
											<td>4563</td>
											<td>Clone of special items</td>
											<td>Deopsit</td>
											<td>$159.32</td>
											<td>No</td>
											<td>-</td>
											<td>03-04-2018</td>
										</tr>
									</tbody>
								</table>
							</div><!-- appointment_box -->
						</div><!-- quote_right_sec -->
					</div><!--/.Panel 4-->
					<div class="tab-pane fade" id="appointment" role="tabpanel">
						
						<div class="quote_right_sec">
							<div class="appointment_box">								
								<div class="container appointmentViewer">
									<a href="javascript:void(0)" class="addAppointment"><img src="<?php echo Url::base();?>/image/add_quote.png" ></a>
									<div class="row head">
										<div class="col">
											<h5>Date/Time</h5>
										</div><!-- col -->
										<div class="col-6">
											<h5>Location</h5>
										</div><!-- col -->
										<div class="col">
											<h5>User Note</h5>
										</div><!-- col -->
									</div><!-- row -->
									<div class="row  body">
										<div class="col">
											<h4>03 May 2018</h4>
											<small>9:10 AM to 11:00 PM</small>
										</div><!-- col -->
										<div class="col-6">
											<p>13-22 waddikke Road Lonsdale SA 5160</p>
										</div><!-- col -->
										<div class="col">
											<p>Testing</p>
										</div><!-- col -->
									</div><!-- row -->
									<div class="row  body">
										<div class="col">
											<h4>03 May 2018</h4>
											<small>9:10 AM to 11:00 PM</small>
										</div><!-- col -->
										<div class="col-6">
											<p>13-22 waddikke Road Lonsdale SA 5160</p>
										</div><!-- col -->
										<div class="col">
											<p>Testing</p>
										</div><!-- col -->
									</div><!-- row -->
									<div class="row  body">
										<div class="col">
											<h4>03 May 2018</h4>
											<small>9:10 AM to 11:00 PM</small>
										</div><!-- col -->
										<div class="col-6">
											<p>13-22 waddikke Road Lonsdale SA 5160</p>
										</div><!-- col -->
										<div class="col">
											<p>Testing</p>
										</div><!-- col -->
									</div><!-- row -->
									<div class="row  body">
										<div class="col">
											<h4>03 May 2018</h4>
											<small>9:10 AM to 11:00 PM</small>
										</div><!-- col -->
										<div class="col-6">
											<p>13-22 waddikke Road Lonsdale SA 5160</p>
										</div><!-- col -->
										<div class="col">
											<p>Testing</p>
										</div><!-- col -->
									</div><!-- row -->
									<div class="row  body">
										<div class="col">
											<h4>03 May 2018</h4>
											<small>9:10 AM to 11:00 PM</small>
										</div><!-- col -->
										<div class="col-6">
											<p>13-22 waddikke Road Lonsdale SA 5160</p>
										</div><!-- col -->
										<div class="col">
											<p>Testing</p>
										</div><!-- col -->
									</div><!-- row -->
									<div class="row  body">
										<div class="col">
											<h4>03 May 2018</h4>
											<small>9:10 AM to 11:00 PM</small>
										</div><!-- col -->
										<div class="col-6">
											<p>13-22 waddikke Road Lonsdale SA 5160</p>
										</div><!-- col -->
										<div class="col">
											<p>Testing</p>
										</div><!-- col -->
									</div><!-- row -->
								</div><!-- container -->
							</div><!-- appointment_box -->
						</div><!-- quote_right_sec -->
					</div><!--/.Panel 5-->
				</div>
			</div><!-- tab-content-top -->
			</div><!-- quote_detail_inner -->
		</div><!-- container -->
		
	</div><!-- quote_detail_wrap -->

</div>
<!-- wrap -->
<!-- Modal -->
<div class="modal fade" id="addContact" tabindex="-1" role="dialog" aria-labelledby="modalLabelSmall" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        
        <h3><img src="<?= Yii::$app->request->baseUrl ?>/image/newinteriorquote.png" alt=""><span class="quoteTypeText">CREATE CONTACT</span></h3>
        <div class="Interior_rht_headerIcons">
    	
          <button type="button" id="saveContactData" data-saveType="create">
            <span aria-hidden="true"><img src="<?= Yii::$app->request->baseUrl ?>/image/tick@3x.png"></span>
          </button>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      </div>

      <div class="modal-body">
        <div class="container">
          <div class="row">
            <div class="Detail_prt_div text-center">
            </div><!-- Detail_prt_div -->
            <div  class="tab-content">
            	<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'],'id' =>'contactId']); ?>
			<div class="profile_div"><img src="<?php echo Url::base();?>/image/avtar.png" alt="" style="pointer-events: none" id="blah"><a href="#" class="profile_edit"><img src="<?php echo Url::base();?>/image/edit_profile.png" alt="" style="pointer-events: none">
					<input type="file" name="image" onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])">
			</a>
		</div>
			 
              <div role="tabpanel" id="home" class="tab-pane in active">
              	<input type="hidden" id="subscriber_idForm" name="subscriber_id" value ="<?= $sid?>">
          		<input type="hidden" name="contact_id" id="contact_idForm">
              	<div class="row">
                          <div class="col">
                            <div class="form-group">
                              <input type="text" name="name" id="contactName" data-tochange="site_contactName" class="form-control name getChange" Placeholder="Contact name">
                            </div>
                          </div>
                          <div class="col">
                            <div class="form-group">
                              <input type="text" name="email" id="contactEmail" data-tochange="site_contactEmail" class="form-control email getChange" Placeholder="Contact email">
                            </div>
                          </div>
                          <div class="col">
                            <div class="form-group">
                              <input type="text" name="phone" id="contactPhone" data-tochange="site_contactPhone" class="form-control phone getChange" Placeholder="Contact phone">
                            </div>
                          </div>
                        </div>
                <div class="new_exist_client">
                  <ul class="nav " role="tablist">
                    <li class="nav-item"><h4>Contact Information</h4></li>
                  </ul>
                  <div class="tab-content">
                    <div role="tabpanel" id="menu2" class="tab-pane in active">
                      <div class="">
                        <div class="row">
                          <div class="col-8">
                            <div class="form-group">
                              <input type="hidden" name="lat" id="lat" value="<?= $commonLat ?>">
                              <input type="hidden" name="long" id="lng" value="<?= $commonLng ?>">
                              <input type="text" name="formatted_addr" id="address" data-tochange="search_address" class="form-control Address getChange" Placeholder="Address" value="<?= $commonAddress ?>">
                            </div>
                            <div class="row">
                              <div class="col">
                                <div class="form-group">
                                  <input type="text" name="street1" id="street" data-tochange="site_street" class="form-control Suburb getChange" Placeholder="Street" value="<?= $commonStreet ?>">
                                </div>
                              </div>
                              <div class="col">
                                <div class="form-group">
                                  <input type="text" name="suburb" id="suburb" data-tochange="site_suburb" class="form-control Suburb getChange" Placeholder="Suburb" value="<?= $commonSuburb ?>">
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-6">
                                <div class="form-group">
                                  <input type="text" name="state" id="state" data-tochange="site_state" class="form-control Suburb getChange" Placeholder="State" value="<?= $commonState ?>">
                                </div>
                              </div>
                              <div class="col-6">
                                <div class="form-group">
                                  <input type="text" name="postal" id="zipCode" data-tochange="site_zipCode" class="form-control ZipCode getChange" Placeholder="ZipCode" value="<?= $commonZip ?>">
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-4">
                            <div class="map_div">
                              <div id="map-canvas"></div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                     <?php ActiveForm::end(); ?>
	          			<input type="hidden" name="csi" id="csi" value="<?= Yii::$app->user->id; ?>">
	                  <input type="hidden" name="contactCreateUrl" id="contactCreateUrl" value="<?= Url::toRoute(['webservice/create-contact'], true); ?>">
	                  <input type="hidden" name="quoteCreateUrl" id="quoteCreateUrl" value="<?= Url::toRoute(['webservice/create-quote'], true); ?>">
	                  <input type="hidden" name="quoteUpdateUrl" id="quoteUpdateUrl" value="<?= Url::toRoute(['webservice/update-quote'], true); ?>">
                  </div>
                  <div class="contactPopup_btmBtn"><button type="button" value="clear" name="clear" class="btn clearContactBtn pull-right">Clear</button></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
 
<!-- modal pop up for communication viewing-->		
<!-- Modal Window content -->
<div id="myModal" class="modal fade" tabindex="-1" role="dialog" style="display:none" area-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close communicationViewingClose" data-dismiss="modal">×</button>		
			</div>
			<div class="modal-bodyCommunications">		
			</div>
		</div>
	</div>
</div>

<!-- modal pop up for communication creating-->		
<!-- Modal Window content -->

<!-- <div id="myModal3" class="modal fade" tabindex="-1" role="dialog" style="display:none" area-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close communicationViewingClose" data-dismiss="modal">×</button>		
			</div>
			<div class="modal-bodyCommunicationsCreate">		
			</div>
		</div>
	</div>
</div> -->





<!-- modal pop up for communication creating-->		
<!-- Modal Window content -->

<!-- <div id="createCommunicationsModal" class="modal fade" role="dialog">
  <div class="modal-dialog"> -->

    <!-- Modal content-->
    <!-- <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div id="modal-bodyCommunicationsCreate">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div> -->




<!-- modal pop up for appointment creating -->		
<!-- Modal Window content -->
<div id="myModal2" class="modal fade" tabindex="-1" role="dialog" style="display:none" area-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close communicationViewingClose" data-dismiss="modal">×</button>		
			</div>
			<div class="modal-bodyAppointment">		



						<div class="event-form">
						<div class="NewEventForm">
						<div class="newEventLft_Form">
						<div class="form-group">
						<input type="hidden" class="form-control eventform_fields" name="TblAppointments[subscriber_id]" Placeholder="Customer Name" id="subs"> 
						<?php  echo $form->field($modelAppointment,"contact_id")->widget(Select2::classname(), [
										'data' => $dat,                  
										'options' => ['placeholder' =>'Customer Name','tabindex' => false,'class'=>'form-control eventform_fields','id'=>		   'userDataSelect'],
										'pluginOptions' => [
										'allowClear' => true,
									],         
								]);
						?> 
						</div><!-- form-control -->
						<div class="form-group">
						<div class="EventSelectBtns AppointSelect">
						
						<?= $form->field($modelAppointment,'type')->dropDownList(ArrayHelper::map(TblAppointmentType::find()->all(),'id','name'),['prompt'=>'Appointment Type','class'=>'appointmentType','id'=>'appointmentTypeID'])->label(false) ?>

						</div><!-- appointselect -->
						<div class="EventSelectBtns teamMember">

						 <?= $form->field($modelAppointment,'member_id')->dropDownList(ArrayHelper::map(TblMembers::find()->all(),'id',
					                function($model2) {
					                return $model2['f_name'].' '.$model2['l_name'];
					                   }
					              ),['prompt'=>'Team Member','class'=>'teamMemberSelect','id'=>'memberID'])->label(false) ?>

						</div><!-- appointselect -->
						</div><!-- form-Group -->
						<div class="form-group">
						<div class="evnetFormLft_Calender">

						<input type="hidden" name="calender" id="getDate" >

						<div id="inlineDatepicker1" class="cal2"></div>
						</div><!-- evnetFormLft_Calender -->

						<div class="EventSelectBtns timeSelect">
						<div class="form-group">


						<input type="text" name="dateSelect[From]" id="timepicker1" class="TimeSelect" placeholder="Time From">

						</div><!-- form-group -->
						<div class="form-group">


						<input type="text" name="dateSelect[to]" id="timepicker2" class="TimeSelect" placeholder="Time To">
						</div><!-- form-group -->
						<div class="form-group">
						<label>All Day</label>
						<div class="RadioMainDiv">
								 <?php $modelAppointment->allDay = '0';?>
          			 		     <?= $form->field($modelAppointment, 'allDay')->radioList(array('1'=>'YES','0'=>'NO'))->label(false); ?>
						</div><!-- Radiomaindiv -->
						</div><!-- form-group -->
						</div><!-- EventSelectBtns -->
						</div><!-- form-group -->
						<div class="form-group">

						
				        <?= $form->field($modelAppointment, 'note')->textarea(['rows' => '6','class'=>'Notefield','placeholder'=>'NOTES','id'=>'noteId' ])->label(false) ?>

						</div><!-- form-group -->
						</div><!-- mewEventlft_form -->
						<div class="newEvent_RhtForm">
						<div class="form-group">
					
							 <?= $form->field($modelAppointment, 'formatted_addr')->textInput(['class'=>'form-control eventform_fields location','placeholder'=>'LOCATIONS','id'=>'pac-input'])->label(false) ?>
						</div><!-- form-control -->
						<div class="Map_EventDiv" id="map">
						</div><!-- map_eventDiv -->
						</div><!-- newEvent_RhtForm -->
						</div><!-- neweventform -->
						</div>
			</div>
		</div>
	</div>
</div>

<!-- modal pop up for appointment creating closed-->		
<!-- Modal Window content closed-->



<?php
    $this->registerJs('
        var subsArray = '.json_encode($datSub).';
        console.log("subsArray-------->", subsArray);
        $(document).on("change","#userDataSelect", function() {
          console.log("userDataSelect------->", $("#userDataSelect").find(":selected").attr("value"));
          var selectedSubId = subsArray[$("#userDataSelect").find(":selected").attr("value")];
          console.log("selectedSubId------>", selectedSubId);
          $("#subs").val(selectedSubId)
        })
      ');

?>
<?php  Pjax::begin();
    $this->registerJsFile(Yii::getAlias('@web').'/frontend/web/js/timepicki.js',['depends' => [yii\web\JqueryAsset::className()]]);
    $this->registerCssFile(Yii::getAlias('@web').'/frontend/web/css/timepicki.css',['depends' => [yii\web\JqueryAsset::className()]]);
   
?>
<script>
$(document).ready(function(){
  $('#timepicker1').timepicki();
  $('#timepicker2').timepicki();
});  
</script>
<script src="http://localhost:8888/paintpad/js/jquerry.js"></script>
<script src="http://localhost:8888/paintpad/js/jquery-minn.js"></script>
<script src="http://localhost:8888/paintpad/js/tag-it.js"></script>
<?php Pjax::end();?>
<?php
	//$this->registerJsFile(Yii::getAlias('@web').'/frontend/web/js/jquery2.js',['depends' => [yii\web\JqueryAsset::className()]]);
    $this->registerJsFile(Yii::getAlias('@web').'/frontend/web/js/bootstrap.min.js',['depends' => [yii\web\JqueryAsset::className()]]);
 	$this->registerJsFile(Yii::getAlias('@web').'/frontend/web/js/bootstrap-select.min.js',['depends' => [yii\web\JqueryAsset::className()]]);
    $this->registerJsFile(Yii::getAlias('@web').'/frontend/web/js/jquery.datepick.js',['depends' => [yii\web\JqueryAsset::className()]]);
	//$this->registerCssFile(Yii::getAlias('@web').'/frontend/web/css/clndr.css',['depends' => [yii\web\JqueryAsset::className()]]);
	$this->registerCssFile(Yii::getAlias('@web').'/frontend/web/css/jquery.datepick.css',['depends' => [yii\web\JqueryAsset::className()]])
?>
<script type="text/javascript">


$(document).ready(function(){

	$(".fa-bars").click(function() {
		  $(".menu").removeClass('menuClose');
		  $(".menu").addClass('menuOpen');

		  $(".mainClose").addClass('mainOpen');
		  $(".mainOpen").removeClass('mainClose');

		  $(".fa-bars").hide(500);
		  $(".fa-times").show(500);
	});

	$(".fa-times").click(function() {
		  $(".menu").addClass('menuClose');
		  $(".menu").removeClass('menuOpen');

		  $(".mainOpen").addClass('mainClose');
		  $(".mainClose").removeClass('mainOpen');

		  $(".fa-times").hide(500);
		  $(".fa-bars").show(500);

	});
});



$(document).ready(function(){
	$("#searchContactName").on("keyup", function(){
		$('.noAppointment').remove();
		var searchClient = $(this).val();
		//console.log(searchClient);
		searchClient = searchClient.toLowerCase();
		if(searchClient != ""){
			$('.tosearch').show();
			var flag=0;
			$(".contact_nameSearch").each(function(){
				var thisHtml = $(this).html();
				thisHtml = thisHtml.toLowerCase();
				//console.log(thisHtml);
				if(thisHtml.indexOf(searchClient) < 0 ){
					$(this).parent().parent().hide();
				}
				else
				{
					flag=1;
				}
			});
			if(flag ==0)
			{
				var fieldHtml="<li class='alert alert-danger noAppointment'>NO Contacts</li>";
				$('.contact_list').append(fieldHtml);
			}
		}
		else
		{
			$('.tosearch').show();
		}
    }); 
})

</script>
<script>
	function quotesGet(quotesListing,a){
					var i=0;
					while(item = quotesListing[i++]){

						//console.log(item);
						var date=item.date;
						
						var status  = item.status; 
						var dateObj = new Date(date*1000)
						//console.log(dateObj);
						var month = dateObj.getMonth() + 1; //months from 1-12
						var monthName = moment.months(month - 1); 
						//console.log(monthName);
						var monthName = moment.months(month - 1);  
						var day = dateObj.getDate();
						//console.log(day);
						var types = item.type;
						if(types == 1)
						{
							types = 'Interiors';
							var icons =	'quotes_Interior_icon.png';
						}
						else if(types == 2)
						{
							types = 'Exteriors';
							var icons =	'quotes_exterior_icon.png';
						}

						statusStr 			= quoteStatusArr[status]['status'];
						statusIcon		= quoteStatusArr[status]['statusIcon'];

						fieldHtmlQuote='<ul class="quotes_list quoteTr" data-href="'+base_url+'quote/view/'+item.quote_id+'"><li class="quoteTr" ><small class="detailsQuotesMonth">'+monthName+'</small><h4 class="detailsQuotesDate">'+day+'</h4></li><li><span class="detailsQuotesId">'+item.quote_id+'</span><h3 class="contact_name">'+item.contact.name+'</h3><small>'+item.contact.phone+'</small></li><li><p class="detailsQuotesdescription">'+item.description+'</p></li><li><div class="interior"><img src="<?php echo Url::base();?>/image/'+icons+'"><span class="detailsQuotesType">'+types+'</span></div></li><li><div class="interior"><img src="<?php echo Url::base();?>/image/quote_price-icon.png"><span class="detailsQuotesprice">$'+item.price+'</span></div></li><li><div class="compt_Action"><img src="<?php echo Url::base();?>/image/'+statusIcon+'"><span>'+statusStr+'</span></div></li></ul>'
						// console.log(item);
						//    console.log(item.date);
						if(a==0)
						{
							$("#detailsQuotesShow").append(fieldHtmlQuote);	
						}
						if(a==1)
						{
							$(".contact_quotes").append(fieldHtmlQuote);
						}
				       
				        
					}

	}
	var base_url=siteBaseUrl+'/';
	var quoteStatusArr =
						{
							1:{'status':'Pending','statusIcon':'quote_pending_active.png'},
							2:{'status':'In-progress','statusIcon':'quote_progress_active.png'},
							3:{'status':'Completed','statusIcon':'quote_progress_active.png'},
							4:{'status':'Accepted','statusIcon':'quote_complt_active.png'},
							5:{'status':'Declined','statusIcon':'quote_complt_active.png'},
							6:{'status':'New','statusIcon':'quote_complt_active.png'},
							7:{'status':'Offered','statusIcon':'quote_complt_active.png'},
							8:{'status':'Open','statusIcon':'quote_complt_active.png'},
						} ;
	//console.log(quoteStatusArr);
	function getContactData(id,sid){
		$(function () {
		    $.getJSON(
		       base_url+"webservice/contact-details?subscriber_id="+sid+"&contact_id="+id,
		        function (data) {
		       		//console.log(data);	
		            $("#detailsQuotesShow").empty();	
					// console.log(data.data.contact.image);
					// console.log(base_url+"uploads/");
					if(base_url+"uploads/" == data.data.contact.image){
						data.data.contact.image = base_url+"uploads/avtar.png"
						//console.log(data.data.contact.image)
						//console.log("ravi");
					}

					var quotesListing = data.data.quotes;
					quotesGet(quotesListing,0);


		            var lat=data.data.contact.lat;
		            var lng=data.data.contact.lng;
		            $("#latlng").val(lat+','+lng)
		            $(".contact_name" ).empty();
					$(".contact_email").empty();
					$(".contact_phn").empty();
					$(".personal_profile").empty();	
					$(".contactsIcon").empty();				
					$(".docsIcon").empty();			
					$(".jssIcon").empty();	
					$(".quotesIcon").empty();	
					$(".addressClicked").empty();	
		            $(".contact_name" ).append(data.data.contact.name);
		            $(".contact_email" ).append(data.data.contact.email);
		            $(".contact_phn" ).append(data.data.contact.phone); 
		            $(".personal_profile" ).append("<img src='"+data.data.contact.image+"' height='80px' width='80px' style='border-radius:58px'>"); 	
		            $(".contactsIcon" ).append(data.data.counts.contacts); 		
		            $(".docsIcon" ).append(data.data.counts.docs); 	            
		            $(".jssIcon" ).append(data.data.counts.jss); 	       
		            $(".quotesIcon" ).append(data.data.counts.quotes); 	   
		            $(".addressClicked").append(data.data.contact.formatted_addr);
		            $('.updateContactIcon').attr('contactIdUpdate',data.data.contact.contact_id);
			              
        	});
		});

	}
	$(document).ready(function(){
		var id = $('#detailsContact').val();
		var thi = $('.tosearch[data-id="'+id+'"]');
		thi.removeAttr('class');
		thi.attr('class','tosearch activeContact')	
		var sid='<?php echo $sid;?>';
 		//var sid=1;
 		getContactData(id,sid);
	});

	
	//console.log(base_url);
	$("ul").on("click",".tosearch", function(e){
			var id = $(this).attr('data-id');
	 		$('.tosearch').removeClass("activeContact")
	 		var sid='<?php echo $sid;?>';
	 		//var sid=1;
	 		getContactData(id,sid);
	 		$(this).addClass("activeContact")
	 		// console.log(id);
	 		// console.log(sid);			
		})
		 	
</script>


<script>
	var base_url=siteBaseUrl+'/';
	function getContactDataQuotes(id,sid){
		$(function () {
			    $.getJSON(
			       base_url+"webservice/quotes?subscriber_id="+sid+"&contact_id="+id,
			        function (data) {
			        	//console.log(data);
			        	$('.contact_quotes').empty();
			        	//$('.contact_quotes').append("<div>");		
			      //   	if(data.data.quotes=="")
			      //   	{
			      //   		$('.contact_quotes').append("<h3>No Quotes</h3>");	
			      //   	}
			      //   	else
			      //   	{
			   			// 	$('.contact_quotes').append("<h3>Quotes</h3>");	
			   			// }
			   			var quotesListing = data.data.quotes;
						quotesGet(quotesListing,1);
						//$('.contact_quotes').append("</div>");	
	        		}
        		);
			});
	}
	//var base_url=siteBaseUrl+'/';
	var contact =<?php echo json_encode($con);?>;
	$("ul").on("click",".tosearch", function(e){
	 		var id = $(this).attr('data-id');	 		
	 		var sid='<?php echo $sid;?>';
	 		//var sid=1;
	 		getContactDataQuotes(id,sid);
	 		// console.log(id);
	 		// console.log(sid);
			
  		});


	$(document).ready(function(){
		var id = $('#detailsContact').val();
		var sid='<?php echo $sid;?>';
 	   	//var sid=1;
 		getContactDataQuotes(id,sid);
	});
	
</script>


<script>
	
	var base_url=siteBaseUrl+'/';
	function getContactCommunications(id,sid){
		$(function () {
		    $.getJSON(
		       base_url+"webservice/communication?subscriber_id="+sid+"&contact_id="+id,
		        function (data) {
		        	//console.log(data.data.emails.length);					        	
		   			$(".communicationListing").empty();	
		   			
		   			$(".createCommunications").attr('data-id',id)
		   			//$(".communicationListing").append("<div></div>");
		   			if(data.data.emails.length>0)
		   			{
		   				var i=0;	
			   			while(item = data.data.emails[i++]){
			   				//console.log(item);
			   				//console.log(item);
							var date=item.date;
							//console.log(date);
							var dateObj = new Date(date*1000)
							//console.log(dateObj);
							var month = dateObj.getMonth() + 1; //months from 1-12
							var monthName = moment.months(month - 1); 
							//console.log(monthName); 
							var day = dateObj.getDate();
							var year = dateObj.getFullYear();
							if (day.toString().length == 1) {
			                  	day = "0" + day;
			                }
		// <div class="mail_heading">'+day+" "+monthName+" "+year+'</div>
							var bodyItem =item.body;
							var res = bodyItem.substr(0,90);
							//console.log(res);
			   				var htmlCommunicationField ='<div class="mail_box"><div class="inner_mail_box"><h4 class="mail_sub">'+item.subject+'</h4><div class="form-group"><label>From</label><p>'+item.From+'</p></div><div class="form-group"><label>Date</label><p>Wed Jan 24 2018 11:45</p></div><div class="form-group"><label>To</label><p>info@paintpad.com.au</p></div><div class="form-group"><label>To</label><p>'+item.to+'</p></div><div class="mail-content-box "  body-item="'+item.comm_id+'">'+res+'...</div><span class="bodyViewer small pull-right" body-item="'+item.comm_id+'" data-toggle="modal" data-target="myModal">View more</span></div></div>';
			   				$(".communicationListing").append(htmlCommunicationField);	
			   			}
			   			
		   			}
		   			else
		   			{
		   				$(".communicationListing").append("<div>No Communications</div>");
		   			}
		   					              
        		}
    		);
		});

	};

	$("ul").on("click",".tosearch", function(e){
	 		var id = $(this).attr('data-id');	 		
	 		var sid='<?php echo $sid;?>';
	 		//var sid=1;
			getContactCommunications(id,sid);	
			
  		});


	$(document).ready(function(){
		var id = $('#detailsContact').val();
		var sid='<?php echo $sid;?>';
 		//var sid=1;
 		getContactCommunications(id,sid);
	});


	
</script>

<script>
	var base_url=siteBaseUrl+'/';
	function getContactInvoices(id,sid){
		$(function () {
		    $.getJSON(
		       base_url+"webservice/invoices?subscriber_id="+sid+"&contact_id="+id,
		        function (data) {
		        	//console.log(data);
		        	$('.invoiceBody').empty();
		        	//console.log(data.data.invoices.length);
			        	if(data.data.invoices.length!=0)
			   			{
				   			$(".invoicehead").show();
				        	var i=0; 
			        		while(item = data.data.invoices[i++]){
			        			var date=item.date;
								//console.log(date);
								var dateObj = new Date(date*1000)
								//console.log(dateObj);
								var month = dateObj.getMonth() + 1; //months from 1-12
								//console.log(monthName); 
								var day = dateObj.getDate();
								var year = dateObj.getFullYear();
								if (day.toString().length == 1) {
				                  	day = "0" + day;
				                }
								if (month.toString().length == 1) {
									month = "0" + month;
								}
								newdate = day + "-" + month + "-" + year;
				                var fieldHtmlInvoiceBody='<tr><td>'+item.id+'</td><td>'+item.quote_id+'</td><td>'+item.quote_note+'</td><td>'+item.description+'</td><td>$'+item.amount+'</td><td>'+item.sent+'</td><td>'+item.paid+'</td><td>'+newdate+'</td></tr>';
				                $('.invoiceBody').append(fieldHtmlInvoiceBody);
			        		}
		        	}
		        	else
		        	{
		        		$(".invoicehead").hide();
		        		$(".invoiceBody").append("<div>No Invoices</div>");
		        	}
	        		
        		}
    		);
		});

	};

	
		$("ul").on("click",".tosearch", function(e){
	 		var id = $(this).attr('data-id');	 		
	 		var sid='<?php echo $sid;?>';
	 		//var sid=1;
			getContactInvoices(id,sid);				
  		});

	$(document).ready(function(){
		var id = $('#detailsContact').val();
		var sid='<?php echo $sid;?>';
 		//var sid=1;
 		getContactInvoices(id,sid);
	});
</script>
<script type="text/javascript">
	$("div").on("click",".createCommunications", function(e){
		//alert("ravi");
		var sid='<?php echo $sid;?>';
		var id = $(".createCommunications").attr("data-id");
		$.ajax({
			    url: base_url+'/communications/create',
			    headers: {'Quoteid': id,'SubscriberID':sid,'fromWeb':1},
		        success: function (response) {						 
					//console.log(response)
						$('.modal-bodyCommunications').empty();		
						$('#myModal').modal({show:true});	
						$('.modal-bodyCommunications').html(response);		


		        }
		});	
		e.stopImmediatePropagation();
		return false;
	});
</script>

<script>
	function getContactAppointments(id,sid){
		var data = '{"contact_id" : '+id+',"subscriber_id" : '+sid+'}';
		//console.log(data);
		$.ajax({
						url: siteBaseUrl+'/contact/appointment-detail',
						type: 'post',
						data: {'json':data},
						success: function (data) {
							//console.log(data);
							var obj = JSON.parse(data);
							//console.log(obj);
							$(".appointmentViewer").empty();
		        	
							var htmlAppointmentHeader = '<div class="row head"><div class="col"><h5>Date/Time</h5></div><div class="col-6"><h5>Location</h5></div><div class="col"><h5>User Note</h5></div><!-- col --></div>';
							$(".appointmentViewer").append('<a href="javascript:void(0)" class="addAppointment"><img src="<?php echo Url::base();?>/image/add_quote.png" ></a>');
							$(".appointmentViewer").append(htmlAppointmentHeader);
							var i=0; 
			        		while(item = obj.data[i++])
			        		{
								//console.log(item);
								var date=item.date;
								//console.log(date);
								var duration =item.duration;
								var fDuration =+date + +duration;
								//console.log(duration);
								//console.log(fDuration);
								var dateObj = new Date(date*1000);
								var dataObjFinal=new Date(fDuration*1000);
								//console.log(dataObjFinal);

								var month = dateObj.getMonth() + 1; //months from 1-12
								//console.log(monthName); 
								var monthName = moment.months(month - 1); 
								var day = dateObj.getDate();
								var year = dateObj.getFullYear();
								var hours =	dateObj.getUTCHours();
								var min	= dateObj.getUTCMinutes();
								var fHours =dataObjFinal.getUTCHours();
								var fMin	= dataObjFinal.getUTCMinutes();
								//console.log(hours+":"+min);   
								if (day.toString().length == 1) {
									day = "0" + day;
								}
								if (hours.toString().length == 1) {
									hours = "0" + hours;
								}
								if (min.toString().length == 1) {
									min = "0" + min;
								}
								if (fHours.toString().length == 1) {
									fHours = "0" + fHours;
								}
								if (fMin.toString().length == 1) {
									fMin = "0" + fMin;
								}
								//console.log(fHours+":"+fHours);   
								var htmlAppointmentViewer = '<div class="row  body"><div class="col"><h4>'+day+''+monthName+''+year+'</h4><small>'+hours+':'+min+' to '+fHours+':'+fMin+' </small></div><div class="col-6"><p>'+item.address+'</p></div><div class="col text ellipsis"<p class="text-concat">'+item.note+'</p></div></div>';
								$(".appointmentViewer").append(htmlAppointmentViewer);
			        		}

							
						},
						
					});
	}

$(document).ready(function(){
		var id = $('#detailsContact').val();
		var sid='<?php echo $sid;?>';
 		//var sid=1;
 		getContactAppointments(id,sid);


		$("ul").on("click",".tosearch", function(e){
			var id = $(this).attr('data-id');	 		
			var sid='<?php echo $sid;?>';
			//var sid=1;
			getContactAppointments(id,sid);	
		});

});


</script>


<script>
      // function initMap() {
      //   var map = new google.maps.Map(document.getElementById('map'), {
      //     zoom: 8,
      //     center: {lat: 40.731, lng: -73.997}
      //   });
      //   var geocoder = new google.maps.Geocoder;
      //   var infowindow = new google.maps.InfoWindow;

      //   document.getElementById('latlng').addEventListener('change', function() {
      //     geocodeLatLng(geocoder, map, infowindow);
      //   });
      // }

      // function geocodeLatLng(geocoder, map, infowindow) {
      //   var input = document.getElementById('latlng').value;
      //   var latlngStr = input.split(',', 2);
      //   var latlng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
      //   geocoder.geocode({'location': latlng}, function(results, status) {
      //     if (status === 'OK') {
      //       if (results[0]) {
      //         map.setZoom(11);
      //         var marker = new google.maps.Marker({
      //           position: latlng,
      //           map: map
      //         });
      //         infowindow.setContent(results[0].formatted_address);
      //         infowindow.open(map, marker);
      //       } else {
      //         window.alert('No results found');
      //       }
      //     } else {
      //       window.alert('Geocoder failed due to: ' + status);
      //     }
      //   });
      // }
    </script>
   <!--  <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC2Qg3ywv9Dhg-Ptjv-5fG3I3UEt0rLufQ&callback=initMap">
				 
				 
    </script> -->
<script>
	var base_url=siteBaseUrl+'/';
	$(document).ready(function(e){
		$(function() {
			$("div").on("click",".bodyViewer", function(e){
				var body_id = $(this).attr('body-item');
				var sid='<?php echo $sid;?>';
				//var sid=1;
				$.ajax({
				    url: base_url+'/communications/view?id='+body_id,
				    headers: { 'QuoteID': body_id,'SubscriberID':sid},
			        success: function (response) {	
			        	$('.modal-bodyCommunications').empty();			 
						$('#myModal').modal({show:true});	
						$('.modal-bodyCommunications').html(response);		
			        }
				});
				e.stopImmediatePropagation();
				return false;
			});			
		});
		
	})
</script>

<script>
	var contactUrl=base_url+"webservice/create-contact";
	function createContact(url){
			var id =$('#contact_idForm').val();
			var sid =$('#subscriber_idForm').val();
			var form = $('form')[0]; // You need to use standard javascript object here  
			var form = $('#contactId')[0];
			var data = new FormData(form);
			$.ajax({
				url:  url,
				type: "POST",
				data: data, // Send the object.

				success: function(response) {
					//console.log(response);
					$('.contact_list').empty();	
					$.ajax({
							url:base_url+"contact/searchcontact",
							//type: "POST",
							//data: data, // Send the object.		    
							success: function(response) {
								var parsed = JSON.parse(response);
								//console.log(parsed);
								var i=0;	
								while(item = parsed.data[i++]){
									if(item.image=="")
									{
										item.image="avtar.png";
									}
									var fieldhtmlList ="<li class='tosearch' data-id='"+item.id+"'><span class='cnt_img'><img src='"+base_url+"uploads/"+item.image+"' height='58px'  width='58px' style='border-radius: 50%;''></span><div class='contact_person_detail'><p class='contact_nameSearch'>"+item.name+"</p><small>"+item.phone+"</small></div></li>";
									$(".contact_list").append(fieldhtmlList);    
								}
								$("#addContact").modal('hide');
								getContactData(id,sid);

							},
					});	
													
				},			           
				contentType: false,   
				processData:false,	
			});				 		
	}

	$('#saveContactData').click(function(){
		createContact(contactUrl);
	});


	$('.add_contact').click(function(){
		$('.quoteTypeText').html('');
		$('.quoteTypeText').append("Create Contact");
		$('#blah').attr('src',base_url+'image/avtar.png');
		contactUrl=base_url+"webservice/create-contact";		
	});	
	
</script>

<script>
	$('.updateContactIcon').click(function(){
		var id=$(this).attr("contactidupdate");
		contactUrl = base_url+"webservice/update-contact";	
		var sid='<?php echo $sid?>';
		$('.quoteTypeText').html('');
		$('.quoteTypeText').append("Update Contact");

		$(function () {
		    $.getJSON(
		       base_url+"webservice/contact-details?subscriber_id="+sid+"&contact_id="+id,
		        function (data) {
		        	//console.log(data);
		        	//console.log(data.data.contact.contact_id);
		        	$('#contactName').val(data.data.contact.name);
		        	$('#contactEmail').val(data.data.contact.email);
		        	$('#contactPhone').val(data.data.contact.phone);
		        	$('#lat').val(data.data.contact.lat);
		        	$('#lng').val(data.data.contact.lng);
		        	$('#address').val(data.data.contact.formatted_addr);
		        	$('#street').val(data.data.contact.street1);
		        	$('#suburb').val(data.data.contact.suburb);
		        	$('#state').val(data.data.contact.state);
		        	$('#zipCode').val(data.data.contact.postal);
		        	$('#blah').attr("src",data.data.contact.image);
		        	$('#contact_idForm').val(data.data.contact.contact_id);
		        }
	        )
		})
	})



	$("div").on("click",".addAppointment", function(e){		
		$('#myModal2').modal({show:true});
		$('#myModal2').removeAttr('tabindex');	
		e.stopImmediatePropagation();
		return false;
	})
</script>
<script>
	$(".clientDetail").click(function(){
		//$(".tab-content").append();
	})
</script>

