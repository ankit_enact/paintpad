<?php

use frontend\assets\QuoteAsset;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

QuoteAsset::register($this);

$this->title = 'Quotes';
$this->params['breadcrumbs'][] = $this->title;
$siteURL = Url::base(true);

$commonLat = 28.517957;
$commonLng = -81.36591199999999;
$commonAddress = "2000 S Mills Ave, Orlando, FL 32806, USA";
$commonStreet = "2000 South Mills Avenue";
$commonSuburb = "Orlando";
$commonState = "Florida";
$commonZip = "32806";

?>
<style>
	#map {
		height: 100%;
	}
	html, body {
		height: 100%;
		margin: 0;
		padding: 0;
	}
	#floating-panel {
		position: absolute;
		top: 10px;
		left: 25%;
		z-index: 5;
		background-color: #fff;
		padding: 5px;
		border: 1px solid #999;
		text-align: center;
		font-family: 'Roboto','sans-serif';
		line-height: 30px;
		padding-left: 10px;
	}
	#map-canvas, #map-canvas-site, #static-map-canvas {
		height: 150px;
		margin: 0;
	}
	.centerMarker, .siteCenterMarker{
		position: absolute;
		background: url(<?=Url::base(true) . "/image/mapIcon.png";?>) no-repeat;
		top: 54%;
		left: 50%;
		z-index: 1;
		margin-left: -10px;
		margin-top: -34px;
		height: 34px;
		width: 20px;
		cursor: pointer;
	}
	.pac-container {
		z-index: 1050 !important;
	}
	.existing_client_list {
    max-height: 190px;
    overflow-y: hidden;
    opacity: 0;
    background: #fff;
}
	.existing_client_list li{
		cursor: pointer;
	}
	ul.existing_client_list img {
		height: 45px;
		width: 45px;
		border-radius: 50%;
		margin-right: 10px;
	}
	#client_image img {
		height: 60px;
		width: 60px;
		border-radius: 50%;
	}
	#google-map-overlay{
		height : 150px;
		width: 270px;
		background: transparent;
		position: absolute;
		top: 0px;
		left: 0px;
		z-index: 99;
	}
	#google-map-overlay-2{
		height : 150px;
		width: 100%;
		background: transparent;
		position: absolute;
		top: 0px;
		left: 0px;
		z-index: 99;
	}
	#map-canvas-site-overlay {
		height : 150px;
		width: 270px;
		background: transparent;
		position: absolute;
		top: 0px;
		left: 0px;
		z-index: 0;
	}
	/*=== Naveen ====*/
	#InteriorQuote .modal-content {
		background: #f6fbff;
	}
	#InteriorQuote .modal-body {
		padding: 0px 0 0;
		background: #fff;
		margin-top: 30px;
	}
	.Detail_prt_div {
		width: 100%;
		margin-top: -18px;
	}
	.Detail_inner .nav.nav-tabs a.nav-link {
		padding: 3px 6px;
		display: inline-block;
	}
	.Detail_inner .nav.nav-tabs {
		float: left;
		width: 100%;
	}
	.quote_right_sec {
	    height: calc(100vh - 72px);
	    overflow: auto;
	    display: block;
	}

	.emailCount{
		background-image: url(<?=$siteURL;?>/image/mail.png);
	}
	.contactsCount{
		background-image: url(<?=$siteURL;?>/image/detail.png);
	}
	.quotesCount{
		background-image: url(<?=$siteURL;?>/image/quote.png);
	}
	.docsCount{
		background-image: url(<?=$siteURL;?>/image/calender_new.png);
	}
	.quote_count{
		background-repeat: no-repeat;
		background-position-x: center;
		background-position-y: 65%;
	}

	img#quoteContactImg {
		height: 90px;
		width: 90px;
		border-radius: 50%;
	}

	#static-edit-quote-map-canvas {
		height: 150px;
		margin: 0;
	}
	button.clear.btn-primary.disabled {
		background: #85d4f6!important;
		border-color: #85d4f6!important;
	}
	.rooms_type li img {
		width: 47px;
	}
	#colorsMainDiv li{
		margin: 0 auto;
		float: left;
		width: 20%;
		display: inline;
		min-height: 271px;
	}
	#colorsMainDiv ul{
		width: 100%;
	}
</style>
<div class="quote_detail_wrap">
<div class="container">
	<div class="quote_detail_inner">
		

		<div class="mainDiv quote-view">
			<input type="hidden" id="page_name" value="quoteEdit">
			<input type="hidden" id="subscriber_id" value="<?=Yii::$app->user->identity->user_id;?>">
			<input type="hidden" id="quote_type" value="<?=$quotes['type'];?>" />
			<input type="hidden" name="quote_id" id="quote_id" value="<?=$quotes['quote_id'];?>">
			<input type="hidden" name="company_id" id="company_id" value="<?=Yii::$app->user->identity->company_id;?>">
			<input type="hidden" name="tier_id" id="tier_id">
			<input type="hidden" id="ifPaintDefaultChanged" value="0" />

			<input type="hidden" id="paintDefaultExists" value="0" />
			<input type="hidden" id="roomsExists" value="0" />
			<?php

			 if (count($quotes) > 0) {

				?>
				
				<input type="hidden" name="contact_id_main" id="contact_id_main" value="<?=$quotes['contact']['contact_id'];?>">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs nav-justified">
					<li class="nav-item">
						<a class="nav-link active checkChange quoteDetailMainTabs" data-href="#panel1" role="tab">Quote Detail</a>
					</li>
					<li class="nav-item">
						<a class="nav-link checkChange quoteDetailMainTabs"  data-href="#panel2" role="tab">Paint Defaults</a>
					</li>
					<li class="nav-item">
						<a class="nav-link checkChange quoteDetailMainTabs"  data-href="#panel4" role="tab"><?php  echo ($quotes['type'] == 2) ? "Area" : "Rooms" ?></a>
					</li>
					<li class="nav-item">
						<a class="nav-link checkChange quoteDetailMainTabs"  data-href="#panel5" role="tab">Summary</a>
					</li>
					<li class="nav-item">
						<a class="nav-link checkChange quoteDetailMainTabs"  data-href="#panel6" role="tab">Communication</a>
					</li>
					<li class="nav-item">
						<a class="nav-link checkChange quoteDetailMainTabs"  data-href="#panel7" role="tab">Site Notes</a>
					</li>
				</ul>
				<!-- Tab panels -->
				<div class="tab-content card">
					<!--Panel 1-->
					
					<!-- <input type="hidden" id="ifPaintDefaultChanged" value="0" /> -->
					
					<div class="tab-pane active quoteDetailMainPane" id="panel1" role="tabpanel">
						<div class="quote_left_fix text-center mCustomScrollbar"  id="quoteDetailLeft">
							<div class="paint_lft_form">
								<div class="client_profile_detail">
									<span class="clientImg"><img id="quoteContactImg" src="<?=$siteURL;?>/image/profileIcon.png" alt=""></span>
									<span class="clientName" id="quoteContactName"></span>
									<span class="client_contact" id="quoteContactEmailPhone"></span>
								</div><!-- client_profile_detail -->
								<div class="quote_count_detail">
									<div class="quote_count email emailCount" id="quote_count_contacts"></div>
									<div class="quote_count invoice contactsCount" id="quote_count_jss"></div>
									<div class="quote_count quote quotesCount" id="quote_count_quotes"></div>
									<div class="quote_count addquote docsCount" id="quote_count_docs"></div>
								</div><!-- quote_count_detail -->
							</div>
						</div><!-- quote_left_fix -->
						<div class="quote_right_sec">
							<div class="right-section-inner">
								<div class="text-right top_icon">
									<p id="quoteNameDesc"></p>
									<span class="edit"><a href="javascript:void(0)" id="editQuoteTrigger" data-toggle="modal" data-dismiss="modal"><img src="<?=$siteURL;?>/image/edit_new.png" alt=""></a></span>
									<span class="add_qt"><img src="<?=$siteURL;?>/image/add_quote.png" alt=""></span>
									<span class="copy"><img src="<?=$siteURL;?>/image/copy_new.png" alt=""></span>
									<span class="copy"><img src="<?=$siteURL;?>/image/delete.png" alt=""></span>
								</div><!-- text-right -->
								<div class="Site_address_div">
									
									<h3 class="site-head">Site Address</h3>
									<p id="quoteContactSiteAddress"></p>
									<div class="map_div">
										<div id="static-edit-quote-map-canvas"></div><!-- map-add -->
										<div id="google-map-overlay-2"></div>
										<span class="map-pin"><img src="<?=$siteURL;?>/image/location.png" alt=""></span>
									</div>
								</div><!-- site_address_div -->

								<div class="Site_contact_div">
									<h3 class="site-head">Site Contact</h3>
									<div class="site-contact-detail">
										<p id="quoteSiteName"></p>
										<p id="quoteSiteEmail"></p>
										<p id="quoteSitePhone"></p>

										<label style="float: left;width: 100%;text-align: left;">Client Name</label>
										<p id="quoteClientName"></p>



										<div class="update-status-div">

											<form>
												<p>
													<label style="float: left;width: 100%;text-align: left;">Update Status</label>
													<select name="update_status" id="quote_status" class="data-style btn-new" tabindex="-98" style="height: 48px;">
														<option value="4">Approved</option>
														<option value="5">Declined</option>
														<option value="6">New</option>
														<option value="7">Submitted</option>
														<option value="8">Open</option>
													</select>
													<span><img src="<?=$siteURL;?>/image/accepted.png" alt=""></span>
												</p>
												<p>
												<label  style="float: left;width: 100%;text-align: left;">Customer Sources</label>
												<select name="update_status" id="quote_status" class="data-style btn-new" tabindex="-98" style="height: 48px;">
													<option value="4">Source</option>
													<option value="5">Google</option>
													<option value="6">5AA</option>
													<option value="7">Return Customers</option>
												</select>
												<span><img src="<?=$siteURL;?>/image/accepted.png" alt=""></span>
												</p>
												<p>
													<input type="text" placeholder="Other" class="input-control other-inpt">
												</p>
												<button type="button" id="quote_status_button" class="submit_btn btn" name="Submit">Submit</button>
											</form>
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
					<!--/.Panel 1-->

					<!--Panel 2-->
					<div class="tab-pane quoteDetailMainPane" id="panel2" role="tabpanel">
							<!-- <input type="hidden" name="quote_id" id="quote_id">
							<input type="hidden" name="quote_id" id="quote_id">
							<input type="hidden" name="quote_id" id="quote_id">
							<input type="hidden" name="quote_id" id="quote_id"> -->

							<form id="paintDefaultForm">

								<input type="hidden" name="quote_type" value="<?=$quotes['type'];?>" />
								<input type="hidden" name="quote_id" value="<?=$quotes['quote_id'];?>">
								<input type="hidden" name="ifUpdate" id="ifUpdate" value="<?=$ifUpdate;?>">
								<?php
								$updateId = 0;

								if ($ifUpdate == 1) {
									$updateId = $quotes['brandPref']['id'];
								}
								?>
								<input type="hidden" name="brandPrefId" value="<?=$updateId;?>">

								<div class="quote_left_fix text-center">
									<div class="paint_lft_form mCustomScrollbar">
										<div class="form-group">
											<label>Brand</label>
											<select name="defaultBrand" id="selbrand" class="selbrand changeType" data-changetype="2" data-style="btn-new" tabindex="-98">
												<option value="">Select Brand</option>
												<?php

												$ifFirst = 1;
												foreach ($brands as $key => $bv) {
													if ($ifFirst == 1) {
														$defaultBrand = $bv->brand_id;
														$ifFirst = 0;
													}
													if ($ifUpdate == 1 && $quotes['brandPref']['brand_id'] == $bv->brand_id) {
														echo '<option selected data-logo="' . $bv->logo . '" value="' . $bv->brand_id . '">' . $bv->name . '</option>';
													} else {
														echo '<option data-logo="' . $bv->logo . '" value="' . $bv->brand_id . '">' . $bv->name . '</option>';
													}
												}
												?>
											</select>
											<input type="hidden" id="defaultBrand" value="<?=$defaultBrand;?>">
										</div>
										<div class="form-group">
											<label>Quality / Tier</label>
											<select name="defaultTier" id="seltiers" class="seltiers changeType getPaintDetails" data-changetype="2" data-style="btn-new" tabindex="-98">

												<option value="">Select Quality / Tier</option>
												<?php

												foreach ($brands as $key => $bv) {
													if (count($bv->tblTiers)) {
														$tiers = $bv->tblTiers;
														foreach ($tiers as $key => $tv) {
															if ($ifUpdate == 1) {
																if ($tv->brand_id == $quotes['brandPref']['brand_id']) {
																	if ($quotes['brandPref']['tier_id'] == $tv->tier_id) {
																		echo '<option selected data-brand=' . $tv->brand_id . ' class="tb_' . $tv->brand_id . ' quote_tiers" value="' . $tv->tier_id . '">' . $tv->name . '</option>';
																	} else {
																		echo '<option data-brand=' . $tv->brand_id . ' class="tb_' . $tv->brand_id . ' quote_tiers" value="' . $tv->tier_id . '">' . $tv->name . '</option>';
																	}
																} else {
																	echo '<option style="display:none;" data-brand=' . $tv->brand_id . ' class="tb_' . $tv->brand_id . ' quote_tiers" value="' . $tv->tier_id . '">' . $tv->name . '</option>';
																}
															} else {
																echo '<option data-brand=' . $tv->brand_id . ' class="tb_' . $tv->brand_id . ' quote_tiers" value="' . $tv->tier_id . '">' . $tv->name . '</option>';
															}
														}
													}
												}
												?>
											</select>
										</div>
										<div class="form-group">
											<?php $coatArr = [1 => "One", 2 => "Two", 3 => "Three", 4 => "Four"];?>
											<label>Coats</label>
											<select name="defaultCoat" id="defaultCoat" class="brand changeType" data-style="btn-new" data-changetype="2" tabindex="-98">
												<option value="">Select Coats</option>
												<?php
												foreach ($coatArr as $key => $value) {
													if ($ifUpdate == 1 && $quotes['brandPref']['coats'] == $key) {
														echo '<option selected value="' . $key . '">' . $value . '</option>';
													} else {
														echo '<option value="' . $key . '">' . $value . '</option>';
													}
												}
												?>
											</select>
										</div>

										<div class="form-group">
											<label>Prep. Level</label>
											<?php //$prepLeveArr = [5 => "Test", 10 => "Premium"];?>
											<select name="defaultPrepLevel" id="defaultPrepLevel" class="brand changeType" data-style="btn-new" data-changetype="2" tabindex="-98">
												<option value="">Select Prep. Level</option>
												<?php
												foreach ($prepLevels as $key => $value) {
													// print_r($value);die;
													if ($ifUpdate == 1 && $quotes['brandPref']['prep_level'] == $value['prep_id']) {
														echo '<option selected value="' . $value['prep_id'] . '">' . $value['prep_level'] . '</option>';
													} else {
														echo '<option value="' .$value['prep_id'] . '">' . $value['prep_level'] . '</option>';
													}
												}
												?>
											</select>
										</div>

										<div class="form-group checkbox_div">
											<label class="checkbox_label">Undercoat
												<input type="checkbox" name="defaultUnderCoat" id="underCoatMain" class="changeType" data-changetype="2" value="1" <?php if ($ifUpdate == 1 && $quotes['brandPref']['apply_undercoat'] == 1) {echo 'checked="checked"';}?> >
												<span class="checkmark"></span>
											</label>
										</div>

										<div class="form-group checkbox_div">
											<label class="checkbox_label">Colour Consultant Required
												<input type="checkbox" name="defaultColorConsultant" id="colorConsultanMain" class="changeType" data-changetype="2" value="1" <?php if ($ifUpdate == 1 && $quotes['brandPref']['color_consultant'] == 1) {echo 'checked="checked"';}?>>
												<span class="checkmark"></span>
											</label>
										</div>

										<!-- <div class="tire_img"><img src="<?=$siteURL;?>/image/dulux.png" alt=""></div>tire_img -->
									</div>
								</div>


								<div class="quote_right_sec mCustomScrollbar" id="paintDefaultMain" <?php if ($ifUpdate == 0) {echo 'style="display: none"';}?> >
									<div class="right-section-inner">
										<?php
										if ($ifUpdate == 1) {
											foreach ($groups as $gk => $gv) {

												$sheenOptions = '';
												$productOptions = '';
												$undercoatOptions = '';

												foreach ($undercoat as $uck => $ucv) {
													if (array_key_exists($gk, $quotes['paintDefaults'])) {
														if ($ucv['product_id'] == $quotes['paintDefaults'][$gk]['undercoat']) {
															$undercoatOptions .= '<option selected value="' . $ucv['product_id'] . '" >' . $ucv['productName'] . '</option>';
														} else {
															$undercoatOptions .= '<option value="' . $ucv['product_id'] . '" >' . $ucv['productName'] . '</option>';
														}
													} else {
														$undercoatOptions .= '<option value="' . $ucv['product_id'] . '" >' . $ucv['productName'] . '</option>';
													}
												}

												if (array_key_exists($gk, $topcoat)) {
													foreach ($topcoat[$gk] as $sk => $sv) {

														if (array_key_exists($sk, $quotes['paintDefaults'])) {													
															if ($sk == $quotes['paintDefaults'][$gk]['sheen_id']) {
																$sheenOptions .= '<option selected value="' . $sk . '" >' . $sheens[$sk] . '</option>';
															} else {
																$sheenOptions .= '<option value="' . $sk . '" >' . $sheens[$sk] . '</option>';
															}
														} else {
															$sheenOptions .= '<option value="' . $sk . '" >' . $sheens[$sk] . '</option>';
														}
														if ($sk == $quotes['paintDefaults'][$gk]['sheen_id']) {
															if (array_key_exists($sk, $sheenProducts)) {

																foreach ($sheenProducts as $pk => $pv) {

																	foreach ($pv as $keyPro => $valPro) {
																		if(!empty($keyPro) || !empty($valPro)){

																	// echo "<pre>";print_r($pv);echo "pk=>".$pk."<<-->>";
																			// if (array_key_exists($gk, $quotes['paintDefaults'])) {
																			if ($quotes['paintDefaults'][$gk]['sheen_id'] == $pk) {
																				if ($keyPro == $quotes['paintDefaults'][$gk]['topcoat']) {

																					$productOptions .= '<option style="display:block;" selected class="sheen_'.$pk.'" value="' . $keyPro . '" >' . $valPro . '</option>';
																				} else {
																					$productOptions .= '<option style="display:block;"  class="sheen_'.$pk.'"  value="' . $keyPro . '" >' . $valPro . '</option>';
																				}

																			}else{
																				$productOptions .= '<option style="display:none;"  class="sheen_'.$pk.'"  value="' . $keyPro . '" >' . $valPro . '</option>';
																			}
																		}
																	}



																	// if (array_key_exists($gk, $quotes['paintDefaults'])) {
																	// 	if ($pk == $quotes['paintDefaults'][$gk]['topcoat']) {
																	// 		$productOptions .= '<option style="display:block;" selected class="sheen_'.$sk.'" value="' . $pk . '" >' . $sheenProducts[$sk][$pk] . '</option>';
																	// 	} else {
																	// 		$productOptions .= '<option style="display:block;"  class="sheen_'.$sk.'"  value="' . $pk . '" >' . $sheenProducts[$sk][$pk] . '</option>';
																	// 	}
																	// } else {
																	// 	$productOptions .= '<option style="display:none;"  class="sheen_'.$sk.'"  value="' . $pk . '" >' . $sheenProducts[$sk][$pk] . '</option>';
																	// }
																}
																// echo "<pre><<<--->>>";print_r($productOptions);echo "<<<--->>>";die;
															}
														}
													}
												}
												// echo "<pre><<<---->>";
												// print_r($quotes);
												// echo "<<<--->>>";
												// die;
												// echo $quotes['paintDefaults'][$gk]['custom_name'];die;
												if(!empty($quotes['paintDefaults'][$gk]['color']) && !empty($quotes['paintDefaults'][$gk]['custom_name'])){
													$colorNameSaved = $quotes['paintDefaults'][$gk]['custom_name'];
													$style = "<img src='" . $siteURL . "/image/coloricon.png' alt=''></a>";

												}elseif($quotes['paintDefaults'][$gk]['color']){
													$colorNameSaved = $quotes['paintDefaults'][$gk]['color']['name'];
													echo '<style> #colorSpan_'.$gk.'{ 
														background-color: '.$quotes["paintDefaults"][$gk]["color"]["hex"].';
													} </style>';
													$style = "<img style='visibility:hidden;' src='" . $siteURL . "/image/coloricon.png' alt=''></a>";
												}else{
													$colorNameSaved = $quotes['paintDefaults'][$gk]['custom_name'];
													$style = "<img src='" . $siteURL . "/image/coloricon.png' alt=''></a>";
												}

													// <img src="' . $siteURL . '/image/coloricon.png" alt="">
												echo
												'<div class="job_default_details" data-comp_id="'.$gk.'">' .
												'<h3 class="default_heading">
												<span  id="colorSpan_'.$gk.'">
												<a href="javascript:void(0)" data-toggle="modal" data-target="#colorCeleings" data-dismiss="modal"  onclick="$(\'#colorCeleings\').attr(\'data-comp_id\','.$gk.')" >
												'.$style.'
												</a>
												</span>' . $groups[$gk]['group_name'] . '<small class="info-icon">i</small></h3>' .
												'<div class="fill_default_form">' .
												'<div class="form-group small">' .
												'<input name="component[' . $gk . '][compId]" type="hidden" value="' . $gk . '">' .
												'<input name="component[' . $gk . '][color]" type="text" value="'.$colorNameSaved.'" placeholder="Colour Name" data-color_id="null"  class="form-control colour-name">' .
												'</div>' .
												'<div class="form-group small">' .
												'<select data-changetype="2" name="component[' . $gk . '][sheen]" id="sheen_' . $gk . '" data-group="' . $gk . '" class="sheen sheendd changeType" data-style="btn-new" tabindex="-98">' .
												$sheenOptions .
												'</select>' .
												'</div>' .
												'<div class="form-group long">' .
												'<select data-changetype="2" name="component[' . $gk . '][product]" id="product_' . $gk . '" data-group="' . $gk . '" class="Product changeType" data-style="btn-new" tabindex="-98">' .
												'<option value="">Select Product</option>' .
												$productOptions .
												'</select>' .
												'</div>' .
												'</div>' .
												'<div class="fill_default_form">' .
												'<div class="form-group small">' .
												'<select data-changetype="2" name="component[' . $gk . '][strength]" class="strength changeType" data-style="btn-new" tabindex="-98">';
												// $strengthArr = [1 => "One", 2 => "Two", 3 => "Three", 4 => "Four"];
												foreach ($strengths as $key => $value) {
													if ($ifUpdate == 1 && $quotes['paintDefaults'][$gk]['strength'] == $value['strength_id']) {
														echo '<option selected value="' . $value['strength_id'] . '">' . $value['name'] . '</option>';
													} else {
														echo '<option value="' . $value['strength_id'] . '">' . $value['name'] . '</option>';
													}
												}
												if($quotes['paintDefaults'][$gk]['hasUnderCoat'] == 1){
													$checked = 'checked="checked"';
													$displayStyle = '';
												}else{
													$checked = '';
													$displayStyle = 'style="display:none;"';
												}
												echo '</select>' .
												'</div>' .
												'<div class="form-group small">' .
												'<div class="form-group checkbox_div">' .
												'<label class="checkbox_label">Undercoat' .
												'<input data-changetype="2" name="component[' . $gk . '][underChecks]" data-group="' . $gk . '" class="underChecks changeType" id="undercheck_' . $gk . '" '.$checked.' type="checkbox">' .
												'<span class="checkmark"></span>' .
												'</label>' .
												'</div>' .
												'</div>' .
												'<div class="form-group long" '.$displayStyle.'>' .
												'<select data-changetype="2" name="component[' . $gk . '][undercoat]" id="undercoatdd_' . $gk . '" class="undercoat changeType" data-style="btn-new" tabindex="-98">' .
												'<option value="" >Select Undercoat</option>' .
												$undercoatOptions .
												'</select>' .
												'</div>' .
												'</div>' .
												'</div>';
											}
										}
										?>
									</div>
								</div>


								<div class="quote_right_sec" id="blankDefaultPane" <?php if ($ifUpdate == 1) {echo 'style="display: none"';}?>>
									<h3>You have not choosen the Paint Defaults</h3>
									<h5>Please choose the Paint Defaults in the left panel</h5>
								</div>

							</form>

						</div>
						<!--/.Panel 2-->

						<div class="tab-pane quoteDetailMainPane" id="panel4" role="tabpanel">

							<div class="quote_left_fix text-center">
								<div class="paint_lft_form">
									<h4>Rooms</h4>
									<div class="rooms-tab mCustomScrollbar">
										<ul class="rooms_type" id="rooms_type_left">
										</ul><!-- rooms_type -->
									</div>
									<div class="bottom_buttons">
										<button type="button" class="btn apply_btn" id="roomTypeApplyBtn">Apply</button>
										<button type="button" class="btn reset_btn" id="roomTypeResetBtn">Reset</button>
									</div><!--bottom_buttons-->
								</div><!-- paint_lft_form -->
							</div><!-- quote_left_fix -->


							<div class="quote_right_sec mCustomScrollbar">
								<div class="right-section-inner">
									<a class="add_room_main" id="add_room_main" href="#" data-toggle="modal" data-target="#addRooms"><img src="<?=$siteURL;?>/image/add_room.png" alt=""></a>
									<div id="rooms_tabs" class="rooms_tabs">

										
									</div>
								</div>
							</div>
						</div><!--/.Panel 4-->

						<div class="tab-pane quoteDetailMainPane" id="panel5" role="tabpanel">
							<div class="quote_left_fix text-center">
								<div class="paint_lft_form mCustomScrollbar">
									<div class="client_profile_detail summary-tab text-center">
										<span class="clientImg"><img src="<?=$siteURL;?>/image/profile.png" alt=""></span>
										<span class="clientName">Adam Ericesson</span>
										<div class="clients-summary-add">
											<label>Site Address</label>
											<p>ON, Toronto, Canada, M9C5M1</p>
										</div><!-- clients-summary-add -->
									</div>
									<div class="quote_inclusion">
										<div class="inclusion_heading">
											<h3>Quote Inclusion</h3>
											<a href="#" class="edit_inclusion">Edit</a>
										</div><!-- inclusion_heading -->
										<p id="headerDesc">This quote includes the  paint and painting of:</p>
										<ul class="room_list" id="quoteInclusions">
											<li><p class="text-left">Dining Room</p><span class="text-right">1</span></li>
											<li><p class="text-left">Bathroom</p><span class="text-right">2</span></li>
										</ul><!-- room_list -->
										<p id="footerDesc">Painted with NORGLASS  NORTHANE MARINE EPOXY quality paint, with premium preparation. Price fee includes all labour
										and materials. </p>
										<div class="inclusion_pricing">
											<h3>Pricing</h3>
											<ul class="pricing_detail">
												<li id="sideBarSubTot">
													<p>Sub Total</p>
													<span>$2000.00</span>
												</li>
												<li id="sideBarGst">
													<p>GST</p>
													<span>$100.00</span>
												</li>
												<li id="sideBarTotalWithGst">
													<p><strong>Total Inc. GST</strong></p>
													<span><strong>$2100.00</strong></span>
												</li>
											</ul><!-- pricing_detail -->
										</div><!-- inclusion_pricing -->
									</div><!-- quote_inclusion -->
								</div><!-- paint_lft_form -->
							</div><!-- quote_left_fix -->
						<!-- </div>quote_left_fix -->
							<div class="quote_right_sec">
								<div class="right-section-inner">
								<nav class="add-room-tab">
									<ul class="nav nav-tabs nav-justified">
										<li class="nav-item">
											<a class="nav-link active" data-toggle="tab" href="#summary_detail" role="tab">Details</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" data-toggle="tab" href="#summary_special-item" role="tab">Special Item</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" data-toggle="tab" href="#summary_quantities" role="tab">Quantites</a>
										</li>
										<li class="nav-item">
											<a class="nav-link" data-toggle="tab" href="#summary_Invoices" role="tab">Invoices</a>
										</li>
									</ul>
								</nav>
								<div class="tab-content card">
									<!--Panel 1-->
									<div class="tab-pane fade in show active" id="summary_detail" role="tabpanel">
										<div class="room_detail_form">
											<div class="row">
												<div class="col">
													<div class="payment-top-row">
														<h3 class="default_heading">Payment Schedule <img src="<?=$siteURL;?>/image/add_room.png" alt=""></h3>
														<button type="button" value="Progress Payment" name="progress payment" class="btn-progress">Progress Payment</button>
													</div>
												</div>
											</div>
											<div class="mCustomScrollbar">
												<div class="row">
													<div class="col">
														<table id="summary_detail_table" class="tabs_table table table-borderless table-condensed table-hover">
															<thead>
																<tr>
																	<th>Description</th>
																	<th>Amount($)</th>
																	<th>Part Payment(%)</th>
																	<th>Date</th>
																</tr>
															</thead>
															<tbody>
																<tr>
																	<td><textarea placeholder="Description" class="description"> </textarea></td>
																	<td><input type="text" class="form-control payment-input" name="" Placeholder="2000.00"></td>
																	<td><input type="text" class="form-control percent-input text-center" name="" Placeholder="25"></td>
																	<td><input id="datepicker" placeholder="12/06/2018" class="form-control datepicker-input" /><span class="summary-close-icon">&times;</span></td>
																</tr>
																<tr>
																	<td><strong>Balance</strong></td>
																	<td><p>4200.00</p></td>
																	<td><p class="text-center">-</p></td>
																	<td><input id="datepicker2" placeholder="12/06/2018" class="form-control datepicker-input" /></td>
																</tr>
															</tbody>
														</table>

												</div><!-- col -->
											</div><!-- row -->
											<div class="row Summary_row pay-row">
												<div class="col-12">
													<div class="form-group">
														<h3 class="default_heading">Payment Option</h3>
														<select class="payment-select" id="paymentOptions">
															<!-- <option>On Completion</option>
															<option>7 Days</option>
															<option>14 Days</option>
															<option>On Completion</option>
															<option>30 Days EOM</option> -->
														</select>
													</div><!-- form-group -->
													<div class="form-group last">
														<h3 class="default_heading">Payment Method</h3>
														<select class="payment-select" id="paymentMethods">
															<!-- <option>Cash</option>
															<option>Cheque</option>
															<option>Credit Card</option>
															<option>EFT</option>
															<option>Money Order</option> -->
														</select>
													</div><!-- form-group -->
												</div><!-- col -->
											</div>
											<div class="row Summary_row pay-row">
												<div class="col-12">
													<div class="form-group">
														<h3 class="default_heading">Progress Payment Notes</h3>
														<select class="payment-select" id="paymentNoteOptions">
															<!-- <option>PP1</option>
															<option>PP2 P</option>
															<option>"Thank you - we really appreciate your business"</option>
															<option>Net 30 - Payment 30 days after invoice date.</option>-->
														</select>
														<textarea class="progress-desc" id="progress-desc"></textarea>
													</div><!-- form-group -->
												</div><!-- col -->
											</div>
											<div class="row Summary_row">
												<div class="col">
													<div class="form-group">
														<h3 class="default_heading">Dates</h3>
														<div class="painter_no">
															<label>Number of Painters</label>
															<input type="number" id="noOfPainters" class="form-control painterNo" value="">
															<label>Est Commencement</label>
															<input id="datepicker3" value="" placeholder="12/06/2018" class="form-control datepicker-input" />
														</div><!-- painter_no -->
														<div class="days_no">
															<label>Days to Complete</label>
															<p id="daysToComplete" data-totalHours=""></p>
															<label>Est Completion</label>
															<input id="datepicker4" placeholder="12/06/2018" class="form-control datepicker-input" />
														</div><!-- days_no -->
													</div><!-- form-group -->
												</div><!-- col -->
												<div class="col">
													<div class="form-group">
														<h3 class="default_heading">Acceptance</h3>
														<div class="acceptance_div">
															<div class="form-group">
																<label>Authorised Person</label>
																<input id="authorisedPerson" placeholder="" class="form-control authPerson" type="text" />
															</div><!-- form-group -->
															<div class="form-group">
																<label>Sign below to accept amount, <a onclick="$('#termAndConditions').show();" href="javascript:void(0);" data-toggle="modal" data-target="#termAndConditions">terms & conditions</a> <span id="tnc_on_off" aria-hidden="true"><img style="margin: 0 auto; float: left; width: 22%; display: none;"  src="<?=Yii::$app->request->baseUrl;?>/image/tick@3x.png"></span> </label>
																<textarea class="sign_box"></textarea>
															</div><!-- form-group -->
															<div class="form-group text-right">
																<label>Confirm Per</label>
																<span>Email</span>
																<span>Signature</span>
																<span>Clear</span>
															</div><!-- form-group -->
														</div><!-- acceptance_div -->
													</div><!-- form-group -->
												</div><!-- col -->
											</div><!-- row -->
										</div>
										</div><!-- room_detail_form -->
									</div><!-- Panel 1 -->
									<div class="tab-pane fade" id="summary_special-item" role="tabpanel">
										<div class="special_item_div">
											<div class="form-group">
												<select name="update_status" class="bedroom" id="selectSpecialItemMain" data-style="btn-new" tabindex="-98" >
													<option>Select special item</option>
												</select>
												<div class="info-select-div">
													<span class="info-icon">i</span>
													<label class="checkbox_label select-add-item">
														<input type="checkbox" name="defaultUnderCoat" id="underCoatMain" class="changeType" data-changetype="2" value="1" checked="checked">
														<span class="checkmark"></span>
													</label>
												</div><!-- info-select-div -->
											</div><!-- form-group -->	
											<div class="form-group text-right">
												<a class="add-custome-specialItem-btn" id="add-custome-specialItem-btn" href="javascript:void(0);">Update Price</a>
												<a class="add-custome-specialItem-btn" id="add-custome-specialItem-btn" href="javascript:void(0);">Add Custom Item</a>
											</div><!-- form-group -->											
										</div><!-- special_item_div -->
										<div class="mCustomScrollbar special_item_div-table">
										<table id="special_item_list_main" class="tabs_table table table-borderless table-condensed table-hover">
											<thead>
												<tr>
													<th>Include</th>
													<th>Item</th>
													<th colspan="3">Labour Hours</th>
													<th>Materials</th>
													<th>Qty</th>
													<th></th>
													<th>Price (Inc.GST)</th>
													<th></th>
												</tr>
											</thead>
											<tbody>									
											</tbody>
										</table>
									
										<div class="totalPriceRoomSi" style="display: none;">
											<p class="pull-right">Total(inc. GST) <span id="totalRoomSiPrice"></span></p>
										</div>
									</div>
									</div><!-- Panel 1 -->
									<div class="tab-pane fade" id="summary_quantities" role="tabpanel">
										<div class="quantites_tab_div">
											<nav class="quntites_Tabs">
												<ul class="nav nav-tabs nav-justified">
													<li class="nav-item">
														<a class="nav-link active" data-toggle="tab" href="#bycomponentMain" role="tab">By Component</a>
													</li>
													<li class="nav-item">
														<a class="nav-link" data-toggle="tab" href="#byproductMain" role="tab">By Product</a>
													</li>
												</ul>
											</nav>
											<div class="tab-content card">
												<div class="mCustomScrollbar">
													<!--Panel 1-->
													<div class="tab-pane fade in show active" id="bycomponentMain" role="tabpanel">
														<!-- Substrate Content Here -->
													</div>
													<div class="tab-pane" id="byproductMain" role="tabpanel">
														<!-- Material Content Here -->
													</div>
												</div>
											</div>
										</div><!-- qunatites_tab_div -->
									</div><!-- Panel 1 -->
									<div class="tab-pane fade" id="summary_Invoices" role="tabpanel">
										<div class="invoices_div_main" id="invoices_div_main">
											<!-- Quote Invoices List  -->
										</div><!-- note div main -->
									</div><!-- Panel 1 -->
								</div><!-- tab-content card -->
							</div>
							</div><!-- quote_right_sec -->
						</div><!--/.Panel 5 -->

						<div class="tab-pane quoteDetailMainPane" id="panel6" role="tabpanel">
							<div class="quote_left_fix text-center mCustomScrollbar"  id="quoteCommunicationLeft">
															
							</div>
							<div class="quote_right_sec">
								<div>
									<a class="pull-right createCommunications add_communication" href="#" data-toggle="modal" data-target="#add_communication"><img src="<?php echo Url::base(); ?>/image/add_room.png" alt=""></a>
								</div>
								<div id="quoteCommunicationContent">
									
								</div>
								
							</div><!-- quote_right_sec -->
						</div><!-- panel16 -->




						<div class="tab-pane quoteDetailMainPane" id="panel7" role="tabpanel">
							<div class="quote_left_fix text-center" id="quoteNoteLeft">
								
							</div><!-- quote_left_fix -->
							<div class="quote_right_sec">
								<div class="site_note_div_main">
									<div class="noteTopIcons">
										<a href="javascript:void(0)"><img src="<?=$siteURL;?>/image/note_add.png" alt=""></a>
									</div><!-- noteTopIcons -->
									<ul class="note_list">
										<!-- Quote Site Notes Here -->
									</ul>
								</div><!-- note div main -->
							</div><!-- quote_right_Sec -->
						</div><!-- panel17 -->

					</div><!-- tab-content-top -->
				</div><!-- quote_detail_inner -->

			<?php }?>

		</div>
	</div>
</div>
	<!-- ADD ROOMS -->
	<div class="modal fade" id="addRooms" role="dialog">
		<div class="modal-dialog" style="width: 95%;">

			<!-- Modal content-->
			<div class="modal-content" style="max-height: 550px; overflow: scroll;">
				<div class="modal-body">
					<button type="button" class="close Addroomclose" data-dismiss="modal">
						<span aria-hidden="true"><img src="<?=Yii::$app->request->baseUrl;?>/image/cross@3x.png"></span>
					</button>
					<button type="button" id="saveRoom" data-savetype="update">
						<span aria-hidden="true"><img src="<?=Yii::$app->request->baseUrl;?>/image/tick@3x.png"></span>
					</button>
					<div class="quote_left_fix text-center">

						<div class="paint_lft_form">
							<h4>Room Type</h4>

							<div class="form-group">
								<select name="room_type" class="form-control" id="roomType" data-style="btn-new" tabindex="-98" onchange="$('#room_name').val(($('#roomType option:selected').val() == '40' || $('#roomType option:selected').val() == '176') ? '' : $('#roomType option:selected').text() +' '+ (parseInt(($('#rooms_type_left li[data-type='+$('#roomType option:selected').val()+'] .count_no').text() != '') ? $('#rooms_type_left li[data-type='+$('#roomType option:selected').val()+'] .count_no').text(): 0) + parseInt('1')))">
									<option value="" selected disabled="disabled">Select Type</option>
									<?php
									foreach ($roomTypes as $key => $roomValue) {
										echo '<option value="' . $roomValue['id'] . '">' . $roomValue['name'] . '</option>';
									}
									?>
								</select>
							</div>

							<div class="form-group">
								<input type="text" value="" id="room_name" class="form-control"  placeholder="Room Name">
							</div>
						</div><!-- paint_lft_form -->
						<?php 
						if($quotes['type'] == 2){ ?>

						<div class="paint_lft_form">
							<h4>Components in this area</h4>
							<ul class="room-comp-list">
							</ul>

						</div>
						<?php } ?> 

					</div><!-- quote_left_fix -->


					<div class="quote_right_sec">
						<div class="right-section-inner">
						<nav class="add-room-tab">
							<ul class="nav nav-tabs nav-justified">
								<li class="nav-item">
									<a class="nav-link active roomTabChange" data-href="detail">Details</a>
								</li>
								<li class="nav-item">
									<a class="nav-link roomTabChange" data-href="special-item">Special Item</a>
								</li>
								<li class="nav-item">
									<a class="nav-link roomTabChange" data-href="quantities">Quantites</a>
								</li>
								<li class="nav-item">
									<a class="nav-link roomTabChange" data-href="Note">Note</a>
								</li>
							</ul>
						</nav>


						<div class="tab-content card">

							<!--Panel 1-->
							<div class="tab-pane active roomTabChangePane mCustomScrollbar" id="detail" role="tabpanel">
								<div class="room_detail_form">
									<div class="row">
										<div class="col room_dimesions">
											<div class="form-group dimension_row">
												<label>Dimensions (L   	&times;    W   	&times;   H) <small>(m)</small></label>
												<input type="text" min="0" value="0" placeholder="0" class="form-control number_input rl" id="r_l" name="l[]">
												<input type="text" min="0" value="0" placeholder="0" class="form-control number_input rw" id="r_w" name="w[]">
												<input type="text" min="0" value="0" placeholder="0" class="form-control number_input rh" id="r_h" name="h[]">
												<span class="btn btn-transparent pull-right add_dim_btn" style=""><i class="material-icons">+</i></span>
											</div>
										</div>
										<div class="col">
											<div class="form-group">
												<button class="btn btn-large room_default_btn"   data-toggle="modal" data-target="#RoomDefaults" data-dismiss="modal"><?php  echo ($quotes['type'] == 2) ? "Area" : "Room" ?> Defaults</button>
												<span class="room_default_setting">Dulux, Dulux Superhide (White Base Only), 1 Coat, No Under Coat </span>
											</div>
										</div>
									</div>

									<div id="room_comp_list"></div>
								</div>
							</div>

							<div class="tab-pane roomTabChangePane" id="special-item" role="tabpanel">
								<div class="special_item_div">
									<div class="form-group">
										<select name="specialItemsAll" id="roomSpecialItmesAll"  class="bedroom" data-style="btn-new" tabindex="-98">
											<option selected="" disabled="disabled">Select Item to Add</option>
											<?php if(!empty($specialItems)){
												foreach ($specialItems as $itemKey => $itemVal) {
													echo "<option value='".$itemVal['id']."' data-price='".$itemVal['price']."'>".$itemVal['name']."</option>";
												}
											} ?>
										</select>
										<div class="select-special-item">
											<div class="table_checkbox"><span class="cross_row">ⓘ</span>
												<label class="checkbox_label"><input type="checkbox" checked="checked"><span class="checkmark"></span></label>
											</div>
										</div>
										<div class="special-item-btns">
											<button type="button" value="Update price" class="update-price btn">Update Price</button>
											<button type="button" value="Update price" class="update-price btn">Add Custom Item</button>
										</div>
									</div><!-- form-group -->
									<!-- <p class="or_separator">or</p>
									<div class="form-group">
										<input type="text" class="form-control custom_tem_add" name="" placeholder="Add Custom Item">
									</div> --><!-- form-group -->
								</div><!-- special_item_div -->
								<div class="Special-item-div mCustomScrollbar">
									<table id="special_item_list" class="tabs_table table table-borderless table-condensed table-hover">
										<thead>
											<tr>
												<th>Include</th>
												<th>Item</th>
												<th colspan="3">Labour Hours</th>
												<th>Materials</th>
												<th>Qty</th>
												<th></th>
												<th>Price (ex. GST)</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
											<!-- <tr>
												<td>
													<div class="table_checkbox">
														<label class="checkbox_label">
															<input type="checkbox" checked="checked">
															<span class="checkmark"></span>
														</label>
													</div>
												</td>
												<td><input type="text" class="form-control item_input" name="" placeholder="" value="Dulux"> </td>
												<td>
													<div class="item_price">
														<label style="width: 10%;">$</label>
														<input type="text" value="" Placeholder="0" class="form-control number_item">
													</div>
												</td>
												<td><input type="text" value="" Placeholder="2" class="form-control number_item2"></td>
												<td>$2.00</td>
												<td><span class="cross_row">&times;</span></td>
											</tr> -->
										</tbody>
									</table>
								</div>
							</div><!-- Panel 1 -->



							<div class="tab-pane roomTabChangePane" id="quantities" role="tabpanel">
								<div class="quantites_tab_div">
									<nav class="quntites_Tabs">
										<ul class="nav nav-tabs">
										    <li class=""><a class="active" data-toggle="tab" href="#bycomponent">By Substrate</a></li>
										    <li><a data-toggle="tab" href="#byproduct">By Material</a></li>
										</ul>
									</nav>

									<div class="tab-content card">
									    <div id="bycomponent" class="tab-pane fade in active">
									    	<div class="byproduct-inside mCustomScrollbar">
										        <table id="bycomponent_table" class="tabs_table table table-borderless table-condensed table-hover">
													<thead>
														<tr>
															<th>Component</th>
															<th>Type</th>
															<th>Product</th>
															<th>Colour</th>
															<th>Hours</th>
															<th>Litres</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>Ceilings</td>
															<td>Standards</td>
															<td>DLX CEILING White</td>
															<td>Colorbond Surfmist Single</td>
															<td>1</td>
															<td>1.603</td>
														</tr>
														<tr>
															<td>Ceilings</td>
															<td>Standards</td>
															<td>DLX CEILING White</td>
															<td>Colorbond Surfmist Single</td>
															<td>1</td>
															<td>1.083</td>
														</tr>
														<tr>
															<td></td>
															<td></td>
															<td></td>
															<td><strong class="text-right">Total</strong></td>
															<td>2</td>
															<td>3.686</td>
														</tr>
													</tbody>
												</table>
												<table id="special_item_table" class="tabs_table table table-borderless table-condensed table-hover">
													<thead>
														<tr>
															<th>Special Item</th>
															<th>Qty</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>A Colour Consultant - Professional On-site Colour Consultation - 1hr - You will receive an
																individual consultation to assist you select the right colours for you. Selections will be added to
															our Job Specification Sheet</td>
															<td>2</td>
														</tr>
														<tr>
															<td>Special item for Master Bedroom</td>
															<td>2</td>
														</tr>
													</tbody>
												</table>
											</div>
									    </div>
									    <div id="byproduct" class="tab-pane fade">
									    	<div class="byproduct-inside mCustomScrollbar">
										    	<table id="bycomponent_table" class="tabs_table table table-borderless table-condensed table-hover">
													<thead>
														<tr>
															<th>Material</th>
															<th>Colour</th>
															<th></th>
															<th class="text-right">Qty</th>
															
															<th class="text-right">Price</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td><p>T Pure int ceiling w4l 09</p></td>
															<td>White</td>
															<td></td>
															<td><span class="text-right">1</span></td>
															<td><span class="text-right">$50.18</span></td>
														</tr>
														<tr>
															<td><p>T Pure int ceiling w4l 09</p></td>
															<td>Pink</td>
															<td></td>
															<td><span class="text-right">1</span></td>
															<td><span class="text-right">$50.18</span></td>
														</tr>
														<tr>
															<td><p>T Pure int ceiling w4l 09</p></td>
															<td>Blue</td>
															<td></td>
															<td><span class="text-right">1</span></td>
															<td><span class="text-right">$50.18</span></td>
														</tr>
														<tr>
															<td><p>T Pure int ceiling w4l 09</p></td>
															<td>-</td>
															<td></td>
															<td><span class="text-right">1</span></td>
															<td><span class="text-right">$50.18</span></td>
														</tr>
														<tr>
															<td><p>T Pure int ceiling w4l 09</p></td>
															<td>-</td>
															<td></td>
															<td><span class="text-right">1</span></td>
															<td><span class="text-right">$50.18</span></td>
														</tr>
														<tr class="total-tr">
															<td></td>
															<td></td>
															<td><strong class="text-right">Paint Sub Total</strong></td>
															<td><strong class="text-right">0</strong></td>
															<td><strong class="text-right">$0.00</strong></td>
														</tr>
														<tr class="total-tr">
															<td></td>
															<td></td>
															<td><strong class="text-right">Application</strong></td>
															<td><span class="text-right">0</span></td>
															<td><span class="text-right">$0.00</span></td>
														</tr>
														<tr class="total-tr">
															<td></td>
															<td></td>
															<td><strong class="text-right">Paint Preparation</strong></td>
															<td><span class="text-right">1</span></td>
															<td><span class="text-right">$0.00</span></td>
														</tr>
														<tr class="total-tr">
															<td></td>
															<td></td>
															<td><strong class="text-right">Application Preparation</strong></td>
															<td><span class="text-right">0</span></td>
															<td><span class="text-right">$0.00</span></td>
														</tr>
														<tr class="total-tr">
															<td></td>
															<td></td>
															<td><strong class="text-right">Sub Total</strong></td>
															<td><span class="text-right"></span></td>
															<td><strong class="text-right">$0.00</strong></td>
														</tr>
													</tbody>
												</table>
											</div>
									    </div>
									</div>
								</div><!-- qunatites_tab_div -->
							</div><!-- Panel 1 -->

							<div class="tab-pane roomTabChangePane" id="Note" role="tabpanel">
								<div class="noteTopIcons">
									<a href="#"  data-toggle="modal" data-target="#addNotes" data-dismiss="modal"><img src="<?=$siteURL;?>/image/add_quote.png" alt=""></a>
								</div><!-- noteTopIcons -->
								<div class="mCustomScrollbar">
								<div class="note_div_main">
									
									<ul class="note_list">
										<li>
											<div class="div-row">
												<div class="notes-image">
													<span class="note_img"><img src ="<?=$siteURL;?>/image/homeimg1.png" alt=""></span>
												</div><!-- col-->
												<div class="col-md-8 notes-content">
													<p>Please make sure you don’t spill the paint on the flloor tiles</p>
													<small>18/05/2018 02:32 pm </small>
												</div><!-- col-md-8 -->
												<div class="col text-right">
													<a href="#" class="edit-notes"><img src ="<?=$siteURL;?>/image/edit.png" alt=""></a>
													<a href="#" class="trash"><img src ="<?=$siteURL;?>/image/delete.png" alt=""></a>
												</div><!-- col -->
											</div><!-- row-->
										</li>
									</ul>
								</div><!-- note div main -->
							  </div>
							</div><!-- Panel 1 -->

						</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	<!-- ADD ROOMS -->





	<div class="modal fade" id="termAndConditions" tabindex="-1" role="dialog" aria-labelledby="modalLabelSmall" aria-hidden="true">
		<div class="modal-dialog modal-sm" style="border-radius: 10px; width: 60%;">
			<div class="modal-content">

				<div class="modal-header" style="height: 47px;">
					<span aria-hidden="true"  data-dismiss="modal" aria-label="Close" style="text-align: right; width: 100%;">
						<img src="<?=Yii::$app->request->baseUrl;?>/image/cross@3x.png" style="width: 4%;">
					</span>
				</div>

				<div class="modal-body">
					<div id="tnc_contant">
							<p>
								Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Nam liber te conscient to factor tum poen legum odioque civiuda.
							</p>
						</div>
				</div>
				<div class="modal-footer">
					<button type="button"  class="btn btn-default" id="accept_tnc_btn" onclick="togTncTxt($(this));">Accept</button>
				</div>
			</div>

		</div>
	</div>


	<div class="modal fade" id="InteriorQuote" tabindex="-1" role="dialog" aria-labelledby="modalLabelSmall" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">

				<div class="modal-header">

					<h3><img src="<?=Yii::$app->request->baseUrl;?>/image/newinteriorquote.png" alt=""><span class="quoteTypeText">Edit Interior Quote</span></h3>
					<div class="Interior_rht_headerIcons">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true"><img src="<?=Yii::$app->request->baseUrl;?>/image/cross@3x.png"></span>
						</button>
						<button type="button" id="saveQuote" data-savetype="update">
							<span aria-hidden="true"><img  src="<?=Yii::$app->request->baseUrl;?>/image/tick@3x.png"></span>
						</button>
					</div>
				</div>

				<div class="modal-body">
					<div class="container">
						<div class="row">
							<div class="Detail_prt_div text-center">
								<div class="Detail_inner">
									<ul class="nav nav-tabs" role="tablist">
										<li class="nav-item"><a class="nav-link active" role="tab" data-toggle="tab" href="#home"><h3><span><img src="<?=Yii::$app->request->baseUrl;?>/image/client_blk.png" class="clientIcon"><img src="<?=Yii::$app->request->baseUrl;?>/image/client_detail.png" class="clientIcon icon1"></span> <span class="headingTxt">Client Details</span></h3></a></li>
										<li class="nav-item"><a class="nav-link" role="tab" data-toggle="tab" href="#menu1"><h3><span><img src="<?=Yii::$app->request->baseUrl;?>/image/siteDetail_blk.png" class="clientIcon"><img src="<?=Yii::$app->request->baseUrl;?>/image/siteDetail.png" class="clientIcon icon1"></span> <span class="headingTxt">Site Details</span></h3></a></li>
									</ul>
								</div>
							</div>
							<div  class="tab-content">
								<div role="tabpanel" id="home" class="tab-pane in active">
									<div class="client_detail_form">
										<div class="form-group">
											<label>Quote Description</label>
											<textarea class="description_input" id="description" placeholder="Please give your quote a brief description"></textarea>
										</div>
									</div>
									<div class="new_exist_client">
										<ul class="nav nav-tabs" role="tablist">
											<li class="nav-item">
												<a id="new_client_link" class="nav-link changeQuoteScreen" data-screen="new" role="tab" data-toggle="tab" href="#menu2">
													<h4>New Client</h4>
												</a>
											</li>
											<li class="nav-item">
												<a id="existing_client_link" class="nav-link changeQuoteScreen" data-screen="existing" role="tab" data-toggle="tab" href="#menu3">
													<h4>Existing Client</h4>
												</a>
											</li>
										</ul>

										<div class="tab-content">

											<div role="tabpanel" id="menu2" class="tab-pane">
												<div class="container new-client-details-holder">

													<div class="row">
														<div class="col">
															<div class="form-group">
																<input type="text" name="clientName" id="clientName" data-tochange="site_clientName" class="form-control name getChange" placeholder="Client name">
															</div>
														</div>
														<div class="col">
															<div class="form-group">
																<input type="text" name="contactName" id="contactName" data-tochange="site_contactName" class="form-control name getChange" Placeholder="Contact name">
															</div>
														</div>
														<div class="col">
															<div class="form-group">
																<input type="text" name="contactEmail" id="contactEmail" data-tochange="site_contactEmail" class="form-control email getChange" Placeholder="Contact email">
															</div>
														</div>
														<div class="col">
															<div class="form-group">
																<input type="text" name="contactPhone" id="contactPhone" data-tochange="site_contactPhone" class="form-control phone getChange" Placeholder="Contact phone">
															</div>
														</div>
													</div>

													<div class="row">
														<div class="col-8 ">
															<div class="form-group">
																<input type="hidden" name="lat" id="lat" value="<?=$commonLat;?>">
																<input type="hidden" name="lng" id="lng" value="<?=$commonLng;?>">
																<input type="hidden" name="csi" id="csi" value="<?=Yii::$app->user->id;?>">
																<input type="hidden" name="contactCreateUrl" id="contactCreateUrl" value="<?=Url::toRoute(['webservice/create-contact'], true);?>">
																<input type="hidden" name="contactUpdateUrl" id="contactUpdateUrl" value="<?=Url::toRoute(['webservice/update-contact'], true);?>">
																<input type="hidden" name="quoteCreateUrl" id="quoteCreateUrl" value="<?=Url::toRoute(['webservice/create-quote'], true);?>">
																<input type="hidden" name="quoteUpdateUrl" id="quoteUpdateUrl" value="<?=Url::toRoute(['webservice/update-quote'], true);?>">
																<input type="text" name="address" id="address" data-tochange="search_address" class="form-control Address getChange" Placeholder="Address" value="<?=$commonAddress;?>">
															</div>
															<div class="row">
																<div class="col">
																	<div class="form-group">
																		<input type="text" name="street" id="street" data-tochange="site_street" class="form-control Suburb getChange" Placeholder="Street" value="<?=$commonStreet;?>">
																	</div>
																</div>
																<div class="col">
																	<div class="form-group">
																		<input type="text" name="suburb" id="suburb" data-tochange="site_suburb" class="form-control Suburb getChange" Placeholder="Suburb" value="<?=$commonSuburb;?>">
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-6">
																	<div class="form-group">
																		<input type="text" name="state" id="state" data-tochange="site_state" class="form-control Suburb getChange" Placeholder="State" value="<?=$commonState;?>">
																	</div>
																</div>
																<div class="col-6">
																	<div class="form-group">
																		<input type="text" name="zipCode" id="zipCode" data-tochange="site_zipCode" class="form-control ZipCode getChange" Placeholder="ZipCode" value="<?=$commonZip;?>">
																	</div>
																</div>
															</div>
														</div>
														<div class="col-4">
															<div class="map_div">
																<div id="map-canvas"></div>
															</div>

															<button class="clear btn-primary new-client-details-clear">Clear</button>




														</div>
													</div>

												</div>
											</div>

											<div role="tabpanel" id="menu3" class="tab-pane">
												<div class="container">
													<div class="row">
														<div class="col">
															<div class="form-group">
																<input type="text" id="search_existing_client" name="Search" class="form-control search" placeholder="Search">
															</div>
															<ul class="existing_client_list mCustomScrollbar">
																<?php

																$default_detail = [];
																$default_detail['name'] = "";
																$default_detail['phone'] = "";
																$default_detail['email'] = "";
																$default_detail['image'] = "";
																$default_detail['formatted_addr'] = "";
																$default_detail['lat'] = "";
																$default_detail['lng'] = "";
																if (count($contacts['data'])) {
																	if (array_key_exists(0, $contacts['data']['contact'])) {

																		$default_detail['contact_id'] = $contacts['data']['contact'][0]['contact_id'];
																		$default_detail['name'] = $contacts['data']['contact'][0]['name'];
																		$default_detail['phone'] = $contacts['data']['contact'][0]['phone'];
																		$default_detail['email'] = $contacts['data']['contact'][0]['email'];
																		$default_detail['image'] = $contacts['data']['contact'][0]['image'];
																		$default_detail['formatted_addr'] = $contacts['data']['contact'][0]['address']['formatted_addr'];
																		$default_detail['lat'] = $contacts['data']['contact'][0]['address']['lat'];
																		$default_detail['lng'] = $contacts['data']['contact'][0]['address']['lng'];
																	}

																	foreach ($contacts['data']['contact'] as $key => $contact) {
																		echo '<li class="contact_li" id="contact_' . $contact['contact_id'] . '" data-id="' . $contact['contact_id'] . '" data-name="' . $contact['name'] . '" data-phone="' . $contact['phone'] . '" data-image="' . $contact['image'] . '" data-email="' . $contact['email'] . '" data-street1="' . $contact['address']['street1'] . '" data-street2="' . $contact['address']['street2'] . '" data-suburb="' . $contact['address']['suburb'] . '" data-state="' . $contact['address']['state'] . '" data-postal="' . $contact['address']['postal'] . '" data-country="' . $contact['address']['country'] . '" data-formatted_addr="' . $contact['address']['formatted_addr'] . '" data-lat="' . $contact['address']['lat'] . '" data-lng="' . $contact['address']['lng'] . '" >';
																		echo '<img class="lazy_load_image" data-src="' . $contact['image'] . '" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" />';
																		echo '<div class="existing_client_detail"> <h3 class="client_name_list">' . $contact['name'] . '</h3><small class="client_phone_list">' . $contact['phone'] . '</small></div>';
																		echo '</li>';
																	}
																}
																?>
															</ul>
														</div>
														<div class="col border-leftright">
															<img data-subscriber_id="<?=Yii::$app->user->id;?>"
															data-contact_id="<?=$default_detail['contact_id'];?>"
															style="position: absolute;right: 20px;top: 0;cursor: pointer;"
															src="/paintpad/image/edit_new.png"
															class="edit-existing-contact">

															<div class="client_Detail text-center">
																<input type="hidden" id="client_lat" value="<?=$default_detail['lat'];?>">
																<input type="hidden" id="client_lng" value="<?=$default_detail['lng'];?>">
																<input type="hidden" id="selected_client" value="">
																<div id="client_image" class="profile_img">
																	<img src="<?=$default_detail['image'];?>" alt="">
																</div>
																<div id="client_name" class="client_name"><?=$default_detail['name'];?></div>
																<div id="client_email" class="client_email"><?=$default_detail['email'];?>
																<br/><?=$default_detail['phone'];?></div>
															</div>
														</div>
														<div class="col address">
															<h4>Address</h4>
															<p id="client_address"><?=$default_detail['formatted_addr'];?></p>
															<div class="map_div">
																<div id="static-map-canvas"></div>
																<div id="google-map-overlay"></div>
																<span class="map-pin"><img src="<?=Yii::$app->request->baseUrl;?>/image/location.png" alt=""></span>
															</div>
														</div>
													</div>
												</div>
											</div>

										</div>
									</div>
								</div>

								<div role="tabpanel" id="menu1" class="tab-pane fade">
									<div class="container">
										<div class="row existing_menu">

											<div class="col-12">
												<label class="clientDetail_Check">Clients details Same
													<input type="checkbox" id="client_details_same" checked="checked">
													<span class="checkmark"></span>
												</label>
											</div>

											<div class="col-8 site-details-holder">

												<div class="row">
													<div class="col">
														<div class="form-group">
															<input type="hidden" name="site_lat" id="site_lat" value="<?=$commonLat;?>">
															<input type="hidden" name="site_lng" id="site_lng" value="<?=$commonLng;?>">
															<input type="text" id="search_address" name="search_address" class="form-control search_address" Placeholder="Search Address" value="<?=$commonAddress;?>">
														</div>
													</div>
												</div>

												<div class="row">
													<div class="col">
														<div class="form-group">
															<input type="text" id="site_street" name="site_street" class="form-control Street" Placeholder="Street" value="<?=$commonStreet;?>">
														</div>
													</div>
													<div class="col">
														<div class="form-group">
															<input type="text" id="site_suburb" name="site_suburb" class="form-control Suburb" Placeholder="Suburb" value="<?=$commonSuburb;?>">
														</div>
													</div>
												</div>

												<div class="row">
													<div class="col-6">
														<div class="form-group">
															<input type="text" id="site_state" name="State" class="form-control Suburb" Placeholder="State" value="<?=$commonState;?>">
														</div>
													</div>
													<div class="col-6">
														<div class="form-group">
															<input type="text" id="site_zipCode" name="Postcode" class="form-control Postcode" Placeholder="Postcode" value="<?=$commonZip;?>">
														</div>
													</div>
												</div>

											</div>

											<div class="col-4">
												<div class="map_div">
													<div id="map-canvas-site"></div>
													<div id="map-canvas-site-overlay"></div>
												</div>
											</div>

										</div>

										<div class="row">
											<div class="col text-right">
												<button type="submit" class="clear-site-details clear btn-primary disabled" disabled="disabled">Clear</button>
											</div>
										</div>

										<div class="row existing_client_detail">
											<div class="col">
												<div class="form-group">
													<input type="text" id="site_contactName" name="Name" class="form-control name" Placeholder="Contact name">
												</div>
											</div>
											<div class="col">
												<div class="form-group">
													<input type="text" id="site_contactEmail" name="Email" class="form-control email" Placeholder="Contact email">
												</div>
											</div>
											<div class="col">
												<div class="form-group">
													<input type="text" id="site_contactPhone" name="Name" class="form-control phone" Placeholder="Contact phone">
												</div>
											</div>
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>


	<div class="modal fade" id="colorCeleings" data-comp_id="" tabindex="-1" role="dialog" aria-labelledby="modalLabelSmall" aria-hidden="true">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">

				<div class="modal-header">

					<span class="header-coloricon"><img src="<?=Yii::$app->request->baseUrl;?>/image/coloricon.png" alt=""></span>
					<div class="Interior_rht_headerIcons">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true"><img src="<?=Yii::$app->request->baseUrl;?>/image/bluecross.png" alt=""></span>
						</button>
					</div>
				</div>

				<div class="modal-body">
					<div class="searchColorMainDiv">
						<div class="container">
							<div class="form-group">
								<input type="text" name="searchColor" id="searchColor" placeholder="Search Colors" class="form-control serach-color">
							</div>
							<div class="form-group">
								<select class="searchTag-dropdown">
									<option value="0">-- Select tag --</option>
									<?php
										foreach ($colorTags as $keyTag=> $tag) {
											echo "<option value='".$tag['tag_id']."'>".$tag['name']."</option>";
										}
									 ?>
								</select>
							</div>
							<div class="form-group ortext"><span>OR</span></div>
							<div class="form-group">
								<input type="text" name="customColor" id="customColor" placeholder="Search Colors" class="form-control custom-color">
							</div>
						</div><!-- searchColorMainDiv -->
					</div>
					<div class="colorsMain-div mCustomScrollbar" id="colorsMainDiv" data-page="1" data-company_id="<?=Yii::$app->user->identity->company_id;?>"  data-subscriber_id="<?=Yii::$app->user->identity->user_id;?>">
						<div class="container">
							<ul>
							<!-- <div  class="row"> -->
								<?php
								// echo "<pre>";print_r($colors);die;
								$col = 0;
								foreach ($colors as $key => $sc) {
									// if ($col == 0) {
									// 	echo '<div class="row">';
									// }
									echo '<li class="col"><div  data-dismiss="modal" class="colorPick" style="height:175px;background-color:' . $sc->hex . '" data-color_id="' . $sc->color_id . '" data-hex="' . $sc->hex . '" data-name="' . $sc->name . '" data-tagid="' . $sc->tag_id . '"></div><p class="colorName">' . $sc->name . '</p></li>';
									// echo '<li style="display:inline;"  data-dismiss="modal" class="colorPick" style="height:175px;background-color:' . $sc->hex . '" data-hex="' . $sc->hex . '" data-name="' . $sc->name . '" data-tagid="' . $sc->tag_id . '"><p class="colorName">' . $sc->name . '</p></li>';
									// if ($col == 4 || $key == (count($colors) - 1)) {
									// 	echo '</div>';
									// 	$col = -1;
									// }
									$col++;
								}
								?>
							</ul>
							<!-- </div> -->
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>






	<!-- EDIT CONTACT POPUP -->
	<div class="modal fade" id="edit-contact-popup" tabindex="-1" role="dialog" aria-labelledby="modalLabelSmall" aria-hidden="true">
		<div class="modal-dialog modal-lg" style="max-width: 900px">
			<div class="modal-content">
				<div class="modal-header">
					<div class="container">
						<div class="row">
							<div class="col-6">
								<h3>
									<img src="<?=Yii::$app->request->baseUrl;?>/image/newinteriorquote.png" alt="">
									<span class="">Edit Contact</span>
								</h3>
							</div>
							<div class="col-6">
								<div class="action-icons" style="float: right;">
									<img style="cursor: pointer;" id="save-contactEdit-popup" src="<?=Yii::$app->request->baseUrl;?>/image/tick@3x.png" width="28px" height="28px">
									<img style="cursor: pointer;" id="close-contactEdit-popup" src="<?=Yii::$app->request->baseUrl;?>/image/cross@3x.png" width="28px" height="28px">
								</div>

							</div>
						</div>
					</div>
				</div>
				<div class="modal-body">
					<div class="container">
						<div class="row">
							<div class="Detail_prt_div text-centerCustom">

							</div>
							<div  class="tab-content" style="width: 100%">
								<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'id' => 'contactId']);?>
								<div class="profile_div">
									<img src="<?php echo Url::base(); ?>/image/avtar.png"
									alt=""
									style="pointer-events: none;height: 50px"
									id="contactImage-clientPopup"
									class='profilePicUpdater'>


									<img id="new-contact-image" src="<?php echo Url::base(); ?>/image/edit_profile.png" style="cursor: pointer;">
									<input style="display: none;" type="file" id="contact-edit-image" name="image" class="fileInput" onchange="document.getElementById('contactImage-clientPopup').src = window.URL.createObjectURL(this.files[0])">


								</div>
								<div role="tabpanel" id="home" class="tab-pane in active">


									<input type="hidden"
									id="subscriber_idForm"
									name="subscriber_id"
									value ="<?=$sid;?>">

									<input type="hidden"
									name="contact_id"
									id="contact_idForm">


									<div class="row">
										<div class="col">
											<div class="form-group">
												<input type="text"
												name="name"
												id="contactName-clientPopup"
												data-tochange="site_contactName"
												class="form-control name getChange"
												Placeholder="Contact name">
											</div>
										</div>
										<div class="col">
											<div class="form-group">
												<input type="text"
												name="email"
												id="contactEmail-clientPopup"
												data-tochange="site_contactEmail"
												class="form-control email getChange"
												Placeholder="Contact email">
											</div>
										</div>
										<div class="col">
											<div class="form-group">
												<input type="text"
												name="phone"
												id="contactPhone-clientPopup"
												data-tochange="site_contactPhone"
												class="form-control phone getChange"
												Placeholder="Contact phone">
											</div>
										</div>
									</div>
									<div class="new_exist_client">
										<ul class="nav " role="tablist">
											<li class="nav-item">
												<h4>Contact Information</h4>
											</li>
										</ul>
										<div class="tab-content">
											<div role="tabpanel" id="menu2" class="tab-pane in active">
												<div class="">
													<div class="row">
														<div class="col-8">

															<div class="form-group">
																<input type="hidden"
																name="lat"
																id="lat-clientPopup"
																value="<?=$commonLat;?>">

																<input type="hidden"
																name="long"
																id="lng-clientPopup"
																value="<?=$commonLng;?>">

																<input type="text"
																name="formatted_addr"
																id="address-clientPopup"
																data-tochange="search_address"
																class="form-control Address getChange"
																Placeholder="Address"
																autocomplete="__away"
																value="<?=$commonAddress;?>">
															</div>

															<div class="row">
																<div class="col">
																	<div class="form-group">
																		<input type="text"
																		name="street1"
																		id="street-clientPopup"
																		data-tochange="site_street"
																		class="form-control Suburb getChange"
																		Placeholder="Street"
																		value="<?=$commonStreet;?>">
																	</div>
																</div>
																<div class="col">
																	<div class="form-group">
																		<input type="text"
																		name="suburb"
																		id="suburb-clientPopup"
																		data-tochange="site_suburb"
																		class="form-control Suburb getChange"
																		Placeholder="Suburb"
																		value="<?=$commonSuburb;?>">
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-6">
																	<div class="form-group">
																		<input type="text"
																		name="state"
																		id="state-clientPopup"
																		data-tochange="site_state"
																		class="form-control Suburb getChange"
																		Placeholder="State"
																		value="<?=$commonState;?>">
																	</div>
																</div>
																<div class="col-6">
																	<div class="form-group">
																		<input type="text"
																		name="postal"
																		id="zipCode-clientPopup"
																		data-tochange="site_zipCode"
																		class="form-control ZipCode getChange"
																		Placeholder="ZipCode"
																		value="<?=$commonZip;?>">
																	</div>
																</div>
															</div>

														</div>
														<div class="col-4">
															<div class="map_div">
																<div id="map-canvas-contact" style="height: 125px"></div>

																<span class="map-pin">
																	<img src="/paintpad/image/location.png" alt="">
																</span>

															</div>
														</div>
													</div>
												</div>
											</div>
											<?php ActiveForm::end();?>
											<input type="hidden"
											name="csi"
											id="csi"
											value="<?=Yii::$app->user->id;?>">

											<input type="hidden"
											name="contactCreateUrl"
											id="contactCreateUrl"
											value="<?=Url::toRoute(['webservice/create-contact'], true);?>">
											<input type="hidden"
											name="quoteCreateUrl"
											id="quoteCreateUrl"
											value="<?=Url::toRoute(['webservice/create-quote'], true);?>">
											<input type="hidden"
											name="quoteUpdateUrl"
											id="quoteUpdateUrl"
											value="<?=Url::toRoute(['webservice/update-quote'], true);?>">
										</div>
										<div class="contactPopup_btmBtn">
											<button type="button" value="clear" name="clear" class="btn clearContactBtn pull-right">Clear</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="createCommunicationsModal" class="modal fade" role="dialog" tabindex="-1" style="display:none" area-hidden="true">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div id="modal-bodyCommunicationsCreate">
					<p></p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>

		</div>
	</div>

	<div id="sendInvoiceForm" class="modal fade" role="dialog" tabindex="-1" style="display:none" area-hidden="true">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<a style="margin-right: 23px;" href="javascript:void(0);" id="createQuickbook" class="close" data-dismiss="modal"><img src="/paintpad/image/tick@3x.png" width="23"></a>
				</div>
				<div id="modal-body">
					<div >
						Description : <textarea rows="5" id="invoice_model_desc"></textarea><br>
						Invoice Memo : <textarea rows="5" id="invoice_model_memo"></textarea>
					</div>
					<span id="selectedInvoice"></span>
					<button type="button" id="skipmemoQuickbook" class="" data-dismiss="modal">Skip</button>
				</div>
			</div>

		</div>
	</div>


	<div id="saveQuoteSiteNoteForm" class="modal fade" role="dialog" tabindex="-1" style="display:none" area-hidden="true">
		<div class="modal-dialog" style="width:100%">
			<!-- Modal content-->
			<div class="modal-content">
				<form id="quote_note_form" enctype="multipart/form-data">
					<div class="modal-header">
						<button type="button" class="close Addroomclose" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true"><img src="<?=Yii::$app->request->baseUrl;?>/image/cross@3x.png"></span>
						</button>
						<button type="button" id="saveRoom" data-savetype="update">
							<span aria-hidden="true"><img  src="<?=Yii::$app->request->baseUrl;?>/image/tick@3x.png"></span>
						</button>
					</div>
					<div id="modal-body">
						
					</div>
				</form>
			</div>
		</div>
	</div>
<!-- Modal -->
  <div class="modal fade" id="RoomDefaults" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
			<button type="button" class="close Addroomclose" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true"><img src="<?=Yii::$app->request->baseUrl;?>/image/cross@3x.png"></span>
			</button>
			<button type="button" id="saveRoom" data-savetype="update">
				<span aria-hidden="true"><img  src="<?=Yii::$app->request->baseUrl;?>/image/tick@3x.png"></span>
			</button>
        </div>
        <div class="modal-body">
          <div class="quote_left_fix text-center mCustomScrollbar ">
			<div class="paint_lft_form">
				<div class="form-group">
					<label>Brand</label>
					<select name="defaultBrand" id="selbrand" class="selbrand changeType" data-changetype="2" data-style="btn-new" tabindex="-98">
						<option value="">Select Brand</option>
						<?php

						$ifFirst = 1;
						foreach ($brands as $key => $bv) {
							if ($ifFirst == 1) {
								$defaultBrand = $bv->brand_id;
								$ifFirst = 0;
							}
							if ($ifUpdate == 1 && $quotes['brandPref']['brand_id'] == $bv->brand_id) {
								echo '<option selected data-logo="' . $bv->logo . '" value="' . $bv->brand_id . '">' . $bv->name . '</option>';
							} else {
								echo '<option data-logo="' . $bv->logo . '" value="' . $bv->brand_id . '">' . $bv->name . '</option>';
							}
						}
						?>
					</select>
					<input type="hidden" id="defaultBrand" value="<?=$defaultBrand;?>">
				</div>
				<div class="form-group">
					<label>Quality / Tier</label>
					<select name="defaultTier" id="seltiers" class="seltiers changeType getPaintDetails" data-changetype="2" data-style="btn-new" tabindex="-98">

						<option value="">Select Quality / Tier</option>
						<?php

						foreach ($brands as $key => $bv) {
							if (count($bv->tblTiers)) {
								$tiers = $bv->tblTiers;
								foreach ($tiers as $key => $tv) {
									if ($ifUpdate == 1) {
										if ($tv->brand_id == $quotes['brandPref']['brand_id']) {
											if ($quotes['brandPref']['tier_id'] == $tv->tier_id) {
												echo '<option selected data-brand=' . $tv->brand_id . ' class="tb_' . $tv->brand_id . ' quote_tiers" value="' . $tv->tier_id . '">' . $tv->name . '</option>';
											} else {
												echo '<option data-brand=' . $tv->brand_id . ' class="tb_' . $tv->brand_id . ' quote_tiers" value="' . $tv->tier_id . '">' . $tv->name . '</option>';
											}
										} else {
											echo '<option style="display:none;" data-brand=' . $tv->brand_id . ' class="tb_' . $tv->brand_id . ' quote_tiers" value="' . $tv->tier_id . '">' . $tv->name . '</option>';
										}
									} else {
										echo '<option data-brand=' . $tv->brand_id . ' class="tb_' . $tv->brand_id . ' quote_tiers" value="' . $tv->tier_id . '">' . $tv->name . '</option>';
									}
								}
							}
						}
						?>
					</select>
				</div>
				<div class="form-group">
					<?php $coatArr = [1 => "One", 2 => "Two", 3 => "Three", 4 => "Four"];?>
					<label>Coats</label>
					<select name="defaultCoat" id="defaultCoat" class="brand changeType" data-style="btn-new" data-changetype="2" tabindex="-98">
						<option value="">Select Coats</option>
						<?php
						foreach ($coatArr as $key => $value) {
							if ($ifUpdate == 1 && $quotes['brandPref']['coats'] == $key) {
								echo '<option selected value="' . $key . '">' . $value . '</option>';
							} else {
								echo '<option value="' . $key . '">' . $value . '</option>';
							}
						}
						?>
					</select>
				</div>

				<div class="form-group">
					<label>Prep. Level</label>
					<?php //$prepLeveArr = [5 => "Test", 10 => "Premium"];?>
					<select name="defaultPrepLevel" id="defaultPrepLevel" class="brand changeType" data-style="btn-new" data-changetype="2" tabindex="-98">
						<option value="">Select Prep. Level</option>
						<?php
						foreach ($prepLevels as $key => $value) {
							// print_r($value);die;
							if ($ifUpdate == 1 && $quotes['brandPref']['prep_level'] == $value['prep_id']) {
								echo '<option selected value="' . $value['prep_id'] . '">' . $value['prep_level'] . '</option>';
							} else {
								echo '<option value="' .$value['prep_id'] . '">' . $value['prep_level'] . '</option>';
							}
						}
						?>
					</select>
				</div>

				<div class="form-group checkbox_div">
					<label class="checkbox_label">Undercoat
						<input type="checkbox" name="defaultUnderCoat" id="underCoatMain" class="changeType" data-changetype="2" value="1" <?php if ($ifUpdate == 1 && $quotes['brandPref']['apply_undercoat'] == 1) {echo 'checked="checked"';}?> >
						<span class="checkmark"></span>
					</label>
				</div>

				<div class="form-group checkbox_div">
					<label class="checkbox_label">Colour Consultant Required
						<input type="checkbox" name="defaultColorConsultant" id="colorConsultanMain" class="changeType" data-changetype="2" value="1" <?php if ($ifUpdate == 1 && $quotes['brandPref']['color_consultant'] == 1) {echo 'checked="checked"';}?>>
						<span class="checkmark"></span>
					</label>
				</div>

				<!-- <div class="tire_img"><img src="<?=$siteURL;?>/image/dulux.png" alt=""></div>tire_img -->
			</div>
		</div>


		<div class="quote_right_sec mCustomScrollbar" id="paintDefaultMain" <?php if ($ifUpdate == 0) {echo 'style="display: none"';}?> >
			<div class="right-section-inner">
				<?php
				if ($ifUpdate == 1) {
					foreach ($groups as $gk => $gv) {

						$sheenOptions = '';
						$productOptions = '';
						$undercoatOptions = '';

						foreach ($undercoat as $uck => $ucv) {
							if (array_key_exists($gk, $quotes['paintDefaults'])) {
								if ($ucv['product_id'] == $quotes['paintDefaults'][$gk]['undercoat']) {
									$undercoatOptions .= '<option selected value="' . $ucv['product_id'] . '" >' . $ucv['productName'] . '</option>';
								} else {
									$undercoatOptions .= '<option value="' . $ucv['product_id'] . '" >' . $ucv['productName'] . '</option>';
								}
							} else {
								$undercoatOptions .= '<option value="' . $ucv['product_id'] . '" >' . $ucv['productName'] . '</option>';
							}
						}

						if (array_key_exists($gk, $topcoat)) {
							foreach ($topcoat[$gk] as $sk => $sv) {

								if (array_key_exists($sk, $quotes['paintDefaults'])) {													
									if ($sk == $quotes['paintDefaults'][$gk]['sheen_id']) {
										$sheenOptions .= '<option selected value="' . $sk . '" >' . $sheens[$sk] . '</option>';
									} else {
										$sheenOptions .= '<option value="' . $sk . '" >' . $sheens[$sk] . '</option>';
									}
								} else {
									$sheenOptions .= '<option value="' . $sk . '" >' . $sheens[$sk] . '</option>';
								}
								if ($sk == $quotes['paintDefaults'][$gk]['sheen_id']) {
									if (array_key_exists($sk, $sheenProducts)) {

										foreach ($sheenProducts as $pk => $pv) {

											foreach ($pv as $keyPro => $valPro) {
												if(!empty($keyPro) || !empty($valPro)){

											// echo "<pre>";print_r($pv);echo "pk=>".$pk."<<-->>";
													// if (array_key_exists($gk, $quotes['paintDefaults'])) {
													if ($quotes['paintDefaults'][$gk]['sheen_id'] == $pk) {
														if ($keyPro == $quotes['paintDefaults'][$gk]['topcoat']) {

															$productOptions .= '<option style="display:block;" selected class="sheen_'.$pk.'" value="' . $keyPro . '" >' . $valPro . '</option>';
														} else {
															$productOptions .= '<option style="display:block;"  class="sheen_'.$pk.'"  value="' . $keyPro . '" >' . $valPro . '</option>';
														}

													}else{
														$productOptions .= '<option style="display:none;"  class="sheen_'.$pk.'"  value="' . $keyPro . '" >' . $valPro . '</option>';
													}
												}
											}



											// if (array_key_exists($gk, $quotes['paintDefaults'])) {
											// 	if ($pk == $quotes['paintDefaults'][$gk]['topcoat']) {
											// 		$productOptions .= '<option style="display:block;" selected class="sheen_'.$sk.'" value="' . $pk . '" >' . $sheenProducts[$sk][$pk] . '</option>';
											// 	} else {
											// 		$productOptions .= '<option style="display:block;"  class="sheen_'.$sk.'"  value="' . $pk . '" >' . $sheenProducts[$sk][$pk] . '</option>';
											// 	}
											// } else {
											// 	$productOptions .= '<option style="display:none;"  class="sheen_'.$sk.'"  value="' . $pk . '" >' . $sheenProducts[$sk][$pk] . '</option>';
											// }
										}
										// echo "<pre><<<--->>>";print_r($productOptions);echo "<<<--->>>";die;
									}
								}
							}
						}
						// echo "<pre><<<---->>";
						// print_r($quotes);
						// echo "<<<--->>>";
						// die;
						// echo $quotes['paintDefaults'][$gk]['custom_name'];die;
						if(!empty($quotes['paintDefaults'][$gk]['color']) && !empty($quotes['paintDefaults'][$gk]['custom_name'])){
							$colorNameSaved = $quotes['paintDefaults'][$gk]['custom_name'];
							$style = "<img src='" . $siteURL . "/image/coloricon.png' alt=''></a>";

						}elseif($quotes['paintDefaults'][$gk]['color']){
							$colorNameSaved = $quotes['paintDefaults'][$gk]['color']['name'];
							echo '<style> #colorSpan_'.$gk.'{ 
								background-color: '.$quotes["paintDefaults"][$gk]["color"]["hex"].';
							} </style>';
							$style = "<img style='visibility:hidden;' src='" . $siteURL . "/image/coloricon.png' alt=''></a>";
						}else{
							$colorNameSaved = $quotes['paintDefaults'][$gk]['custom_name'];
							$style = "<img src='" . $siteURL . "/image/coloricon.png' alt=''></a>";
						}

							// <img src="' . $siteURL . '/image/coloricon.png" alt="">
						echo
						'<div class="job_default_details" data-comp_id="'.$gk.'">' .
						'<h3 class="default_heading">
						<span id="colorSpan_'.$gk.'">
						<a href="javascript:void(0)" data-toggle="modal" data-target="#colorCeleings" data-dismiss="modal"  onclick="$(\'#colorCeleings\').attr(\'data-comp_id\','.$gk.')" >
						'.$style.'
						</a>
						</span>' . $groups[$gk]['group_name'] . '<small class="info-icon">i</small></h3>' .
						'<div class="fill_default_form">' .
						'<div class="form-group small">' .
						'<input name="component[' . $gk . '][compId]" type="hidden" value="' . $gk . '">' .
						'<input name="component[' . $gk . '][color]" type="text" value="'.$colorNameSaved.'" placeholder="Colour Name" data-color_id="null"  class="form-control colour-name">' .
						'</div>' .
						'<div class="form-group small">' .
						'<select data-changetype="2" name="component[' . $gk . '][sheen]" id="sheen_' . $gk . '" data-group="' . $gk . '" class="sheen sheendd changeType" data-style="btn-new" tabindex="-98">' .
						$sheenOptions .
						'</select>' .
						'</div>' .
						'<div class="form-group long">' .
						'<select data-changetype="2" name="component[' . $gk . '][product]" id="product_' . $gk . '" data-group="' . $gk . '" class="Product changeType" data-style="btn-new" tabindex="-98">' .
						'<option value="">Select Product</option>' .
						$productOptions .
						'</select>' .
						'</div>' .
						'</div>' .
						'<div class="fill_default_form">' .
						'<div class="form-group small">' .
						'<select data-changetype="2" name="component[' . $gk . '][strength]" class="strength changeType" data-style="btn-new" tabindex="-98">';
						// $strengthArr = [1 => "One", 2 => "Two", 3 => "Three", 4 => "Four"];
						foreach ($strengths as $key => $value) {
							if ($ifUpdate == 1 && $quotes['paintDefaults'][$gk]['strength'] == $value['strength_id']) {
								echo '<option selected value="' . $value['strength_id'] . '">' . $value['name'] . '</option>';
							} else {
								echo '<option value="' . $value['strength_id'] . '">' . $value['name'] . '</option>';
							}
						}
						if($quotes['paintDefaults'][$gk]['hasUnderCoat'] == 1){
							$checked = 'checked="checked"';
							$displayStyle = '';
						}else{
							$checked = '';
							$displayStyle = 'style="display:none;"';
						}
						echo '</select>' .
						'</div>' .
						'<div class="form-group small">' .
						'<div class="form-group checkbox_div">' .
						'<label class="checkbox_label">Undercoat' .
						'<input data-changetype="2" name="component[' . $gk . '][underChecks]" data-group="' . $gk . '" class="underChecks changeType" id="undercheck_' . $gk . '" '.$checked.' type="checkbox">' .
						'<span class="checkmark"></span>' .
						'</label>' .
						'</div>' .
						'</div>' .
						'<div class="form-group long" '.$displayStyle.'>' .
						'<select data-changetype="2" name="component[' . $gk . '][undercoat]" id="undercoatdd_' . $gk . '" class="undercoat changeType" data-style="btn-new" tabindex="-98">' .
						'<option value="" >Select Undercoat</option>' .
						$undercoatOptions .
						'</select>' .
						'</div>' .
						'</div>' .
						'</div>';
					}
				}
				?>
			</div>
		</div>
        </div>
      </div>
      
    </div>
  </div>



  <!-- Modal -->
<div id="addNotes" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
      	<h2>Add Notes</h2>
      	<button type="button" class="close Addroomclose" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true"><img src="<?=Yii::$app->request->baseUrl;?>/image/cross@3x.png"></span>
		</button>
		<button type="button" id="saveRoom" data-savetype="update">
			<span aria-hidden="true"><img  src="<?=Yii::$app->request->baseUrl;?>/image/tick@3x.png"></span>
		</button>
      </div>
      <div class="modal-body">
        <div class="notes-popup-div">
        	<div class="form-group">
        		<label>Note:</label>
        		<textarea  style="" id="quote_notes" name="tbl_quote_notes"></textarea>
        	</div><!-- form-group -->
        	<div class="form-group">
        		<label>Add Image:</label>
        		<div class="add-image">
					<div class="panel">
						<div class="button_outer">
							<div class="btn_upload">
								<input type="file" id="upload_file" name="">
								<span class="uploadImg"><img  src="<?=Yii::$app->request->baseUrl;?>/image/upload.png"></span>
							</div>
						</div>
					</div>
					<div class="error_msg"></div>
					<div class="uploaded_file_view" id="uploaded_view">
						<span class="file_remove">X</span>
					</div>
				</div>
        	</div><!-- form-group -->
        </div><!-- notes-popup-div -->
      </div>
    </div>

  </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

<script type="text/javascript">
	var btnUpload = $("#upload_file"),
		btnOuter = $(".button_outer");
	btnUpload.on("change", function(e){
		var ext = btnUpload.val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
			$(".error_msg").text("Not an Image...");
		} else {
			$(".error_msg").text("");
			btnOuter.addClass("file_uploading");
			setTimeout(function(){
				btnOuter.addClass("file_uploaded");
			},3000);
			var uploadedFile = URL.createObjectURL(e.target.files[0]);
			setTimeout(function(){
				$("#uploaded_view").append('<img src="'+uploadedFile+'" />').addClass("show");
			},2000);
		}
	});
	$(".file_remove").on("click", function(e){
		$("#uploaded_view").removeClass("show");
		$("#uploaded_view").find("img").remove();
		btnOuter.removeClass("file_uploading");
		btnOuter.removeClass("file_uploaded");
	});
</script>





