<?php

/* @var $this yii\web\View */
setlocale(LC_MONETARY,"en_US");
use frontend\assets\QuoteAsset;
use yii\helpers\Url;
QuoteAsset::register($this);

$this->title = 'Invoices';
$this->params['breadcrumbs'][] = $this->title;
$siteURL = Url::base(true);
?>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inview/1.0.0/jquery.inview.min.js"></script> -->
<style type="text/css">

  #ui-id-1 img {
    height: 45px;
    width: 45px;
    border-radius: 50%;
    margin-right: 10px;
    float: left;
  }
  .ui-menu li span{
    font-size:2em;
    padding:0 0 10px 10px;
    margin:0 0 10px 0 !important;
    white-space:nowrap;
  }
  .intActive{
    width: 30px;
    height: 30px;
    background-image: url(../image/quote_interior.png);
    overflow: hidden;
    margin: 0 auto;
    display: block;
  }
  .ui-widget.ui-widget-content {
    border: 1px solid lightgrey;
    background: #f8f8f8;
    padding: 0;
    max-height: 292px !important;
    overflow-y: auto;
  }
  .ui-widget.ui-widget-content li {
    border-bottom: 1px solid #e6e6e6;
    padding: 10px 0px;
  }
  h3.client_name_list {
    font-weight: normal;
    font-size: 16px;
    line-height: 15px;
    margin: 0;
    height: 16px;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  }
  .ui-widget.ui-widget-content li small.client_phone_list {
    color: #b3b2b2;
    font-weight: 300 !important;
    font-size: 13px;
    font-family: 'Open Sans';
  }
  .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active, a.ui-button:active, .ui-button:active, .ui-button.ui-state-active:hover {
    border: 0px solid #e4e4e4;
    background: transparent;
    font-weight: normal;
    color: #000;
    margin: 0;
  }
  .ui-widget.ui-widget-content li:hover {
    background: #efefef;
  }
  .Quotetables td span {
    color: #000000;
    font-size: 16px;
    font-weight: 500;
  }
  .Quotetables th {
    color: #000000;
    font-size: 17px;
    font-weight: 800;
  }
</style>

<div class="container">

  <div class="quote_detail_inner">
    <!-- Nav tabs -->
    <!-- Tab panels -->
    <div class="tab-content card">

      <!--Panel 1-->
      <div class="tab-pane fade in show active" id="detail" role="tabpanel">

        <div class="personal_deatil">
          <table class="Quotetables">
            <img id="loader" style="display: none;" src="https://i.giphy.com/media/3oEjI6SIIHBdRxXI40/giphy.webp">

            <thead>
              <tr>
                <th>Invoice id</th>
                <th>Quote Id</th>
                <th style=" width: 35%; ">Description</th>
                <th>%</th>
                <th>Amount</th>
                <th>Paid</th>
                <th>Date</th>
              </tr>
            </thead>
            <tbody id="allinvoicesTbody" class="mCustomScrollbar">     
              <?php 
              if(count($paymentSchedule) > 0){
                foreach ($paymentSchedule as $key => $value) {

                  $value['part_payment'] = ((int)$value['part_payment'] == $value['part_payment']) ? $value['part_payment'] : number_format($value['part_payment'],2);
                  $value['amount'] = ((int)$value['amount'] == $value['amount']) ? money_format("%n", $value['amount']) : money_format("%n",number_format($value['amount'],2));
                  $value['descp'] = ($value['descp'] != '') ? $value['descp'] : '-';
                  $value['payment_date'] = ($value['payment_date'] != '') ? $value['payment_date'] : '-';
                  ?>

                  <tr>
                    <td><span><a href="<?php echo $siteURL.'/qb_invoices/'.$value['qb_invoice_pdf']; ?>" target="_blank"><?php echo $value['qb_invoice_DocNumber']; ?></a></span></td>
                    <td><span><?php echo $value['quote_id']; ?></span></td>
                    <td style='width: 35%;'><span><?php echo $value['descp']; ?></span></td>
                    <td><span><?php echo $value['part_payment']; ?></span></td>
                    <td><span><?php echo $value['amount']; ?></span></td>
                    <td><span><?php echo $value['qb_invoice_status']; ?></span></td>
                    <td><span><?php echo $value['payment_date']; ?></span></td>
                  </tr>
                  <?php 
                }
              }else{
                ?>
                <tr>
                  <td><span>No Invoices.</span></td>
                </tr>
              <?php  }?>               
            </tbody>
            <span id="pagevalAllinvoices" data-value="1" data-company="<?= $company_id; ?>"></span>

          </table>

        </div><!-- personal_detail -->
      </div>
      <!--/.Panel 1-->
    </div><!-- tab-content-top -->
  </div><!-- quote_detail_inner -->
</div>

