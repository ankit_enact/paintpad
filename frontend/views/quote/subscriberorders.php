<?php

/* @var $this yii\web\View */
setlocale(LC_MONETARY,"en_US");
use frontend\assets\QuoteAsset;
use yii\helpers\Url;

$this->title = 'Paint Orders';
$this->params['breadcrumbs'][] = $this->title;
$siteURL = Url::base(true);
date_default_timezone_set("Asia/Kolkata");

QuoteAsset::register($this);
?>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inview/1.0.0/jquery.inview.min.js"></script> -->
<style type="text/css">

  #ui-id-1 img {
    height: 45px;
    width: 45px;
    border-radius: 50%;
    margin-right: 10px;
    float: left;
  }
  .ui-menu li span{
    font-size:2em;
    padding:0 0 10px 10px;
    margin:0 0 10px 0 !important;
    white-space:nowrap;
  }
  .intActive{
    width: 30px;
    height: 30px;
    background-image: url(../image/quote_interior.png);
    overflow: hidden;
    margin: 0 auto;
    display: block;
  }
  .ui-widget.ui-widget-content {
    border: 1px solid lightgrey;
    background: #f8f8f8;
    padding: 0;
    max-height: 292px !important;
    overflow-y: auto;
  }
  .ui-widget.ui-widget-content li {
    border-bottom: 1px solid #e6e6e6;
    padding: 10px 0px;
  }
  h3.client_name_list {
    font-weight: normal;
    font-size: 16px;
    line-height: 15px;
    margin: 0;
    height: 16px;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  }
  .ui-widget.ui-widget-content li small.client_phone_list {
    color: #b3b2b2;
    font-weight: 300 !important;
    font-size: 13px;
    font-family: 'Open Sans';
  }
  .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active, a.ui-button:active, .ui-button:active, .ui-button.ui-state-active:hover {
    border: 0px solid #e4e4e4;
    background: transparent;
    font-weight: normal;
    color: #000;
    margin: 0;
  }
  .ui-widget.ui-widget-content li:hover {
    background: #efefef;
  }
.Quotetables td span {
    color: #000000;
    font-size: 14px;
    font-weight: 500;
}
  .Quotetables th {
    color: #000000;
    font-size: 17px;
    font-weight: 800;
  }
  .Quotetables td {
    vertical-align: top;
    padding: 12px 10px 12px 16px;
  }
.Quotetables th:last-child {
    text-align: left;
    padding-left: 5px;
}
.Quotetables td span.time {
    font-size: 12px;
    width: 100%;
    color: #888;
    padding: 0;
    margin: 0;
}
.Quotetables td {
    vertical-align: middle;
}
.Quotetables th:nth-child(4), .Quotetables td:nth-child(4) {
    text-align: center;
}
.Quotetables td:nth-child(4) span{
    color: #888;
    font-size: 12px;
}

#subscriber_order_detail .modal-header {
    display: inline-block;
    text-align: right;
}
#subscriber_order_detail h5#exampleModalLongTitle {
    float: left;
    text-align: left;
    font-size: 16px;
    color: #19aeef;
}
a.subscriber-order-pdf {
    margin-right: 34px;
    padding-top: 4px !important;
}
#subscriber_order_detail  .modal-header .close {
    margin-top: -3px;
    z-index: 9;
    color: #20b8ef;
    float: right !important;
    right: -20px;
}
.Quotetables th {
    color: #000000;
    font-size: 15px;
    font-weight: 800;
}
.subscriber-order-modal-body {
    max-height: calc(100vh - 350px);
    overflow-y: auto;
    padding: 15px 15px;
    min-height: 500px;
}
#subscriber_order_detail .modal-dialog.modal-dialog-centered {
    width: 850px;
    max-width: 850px;
}
table.subscriber-order-tables.Quotetables th {
    background: #efefef;
    font-size: 14px;
}
#subscriber_order_detail .Quotetables thead, #subscriber_order_detail .Quotetables tbody {
    width: calc( 100% - 1em );
    height: auto;
}
#subscriber_order_detail .Quotetables td {
    padding: 12px 10px 12px 8px;
}
#subscriber_order_detail .Quotetables td, #subscriber_order_detail .Quotetables th {
    text-align: center;
}
#subscriber_order_detail .Quotetables td:first-child, #subscriber_order_detail .Quotetables th:first-child {
    text-align: left;
}
#subscriber_order_detail  .Quotetables tbody tr {
    border-bottom: 0;
}

#subscriber_order_detail  .Quotetables td span.deliverTo {
    float: left;
    width: 100%;
    background: #fff;
    padding: 6px 10px;
    text-align: left;
    border-radius: 3px;
}
#subscriber_order_detail  .Quotetables td span.notes {
    float: left;
    width: 100%;
    background: #f1f1f1;
    padding: 6px 10px;
    text-align: left;
    border-radius: 3px;
    min-height: 70px;
}
#subscriber_order_detail table.subscriber-order-tables.Quotetables.modal-first th:first-child, #subscriber_order_detail table.subscriber-order-tables.Quotetables.modal-first td:first-child {
    width: 15%;
}
#subscriber_order_detail .modal-header .close:focus {
    outline: 0;
}
.Quotetables th {
    background: #efefef;
    padding: 14px 10px;
}
</style>

<div class="container">

  <div class="quote_detail_inner">
    <!-- Nav tabs -->
    <!-- Tab panels -->
    <div class="tab-content card">

      <!--Panel 1-->
      <div class="tab-pane fade in show active" id="detail" role="tabpanel">

        <div class="personal_deatil">
          <table class="Quotetables">
            <img id="loader" style="display: none;" src="https://i.giphy.com/media/3oEjI6SIIHBdRxXI40/giphy.webp">

            <thead>
              <tr>
                <th>Quote No</th>
                <th>Client</th>
                <th style=" width: 35%; ">Notes</th>
                <th>I/E</th>
                <th>Date</th>
              </tr>
            </thead>
            <tbody id="paintorderTbody" class="mCustomScrollbar">     
              <?php 
              if(count($paintOrder) > 0){
                foreach ($paintOrder as $key => $value) {
                  // echo "<pre>";print_r($value);die;
                  $deliver_to = ($value['deliver_detail'] != '' || $value['deliver_detail'] != null) ? $value['deliver_detail']->name : 'N/A';
                  $type_icon = ($value['quote_type'] != 1) ? Url::base(true).'/image/quote_exterior.png' : Url::base(true).'/image/quote_interior.png';                
                  $value['quote_type'] = ($value['quote_type'] != 1) ? 'Exterior' : 'Interior';               
                                
                  $date = date("d/m/Y",$value['created_at']);
                  $time = date("h:i A",$value['created_at']);
                  ?>
                  <tr data-paintdetail='<?php echo json_encode($value["paint_order_detail"]); ?>' data-note="<?php echo $value['notes']; ?>" data-deliverto="<?php echo $value["deliver_detail"]->name; ?>" data-pdfurl="<?php echo $value['pdf_url']; ?>"  data-toggle="modal" data-target="#subscriber_order_detail">
                    <td><span><?php echo $value['quote_id']; ?></span></td>
                    <td><span><?php echo $deliver_to; ?></span></td>
                    <td style='width: 35%;'><span><?php echo $value['notes']; ?></span></td>
                    <td><span><img style="width: 25%" src="<?php echo $type_icon; ?>"><br></span><span><?php echo $value['quote_type']; ?></span></td>
                    <td><span><?php echo $date.'<br><span class="time">'.$time.'</span>'; ?></span></td>
                  </tr>
                  <?php 
                }
              }else{
                ?>
                <tr>
                  <td><span>No Order</span></td>
                </tr>
              <?php  }?>               
            </tbody>
            <span id="pagevalPaintoreder" data-value="1" data-company="<?= Yii::$app->user->identity['company_id']; ?>" data-subscriber="<?= Yii::$app->user->identity['user_id']; ?>"></span>

          </table>

        </div><!-- personal_detail -->
      </div>
      <!--/.Panel 1-->
    </div><!-- tab-content-top -->
  </div><!-- quote_detail_inner -->
</div>

<div class="modal fade" id="subscriber_order_detail" tabindex="-1" role="dialog" aria-labelledby="subscriber_order_detail" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle"><?php echo $this->title; ?></h5>
        <a href="#" class="subscriber-order-pdf" target="_blank"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <img style="width: 25%;" src="<?php echo Url::base(true).'/image/quoteStatus/status-declined.png'; ?>">
        </button>
      </div>
      <div class="subscriber-order-modal-body">
        <!-- content here -->
      </div>
    </div>
  </div>
</div>


