<?php

/* @var $this \yii\web\View */
/* @var $content string */

use frontend\assets\AppAsset;
use yii\helpers\Html;

AppAsset::register($this);
?>
<?php $this->beginPage();?>
<!DOCTYPE html>
<html lang="<?=Yii::$app->language;?>">
<head>
    <meta charset="<?=Yii::$app->charset;?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?=Html::csrfMetaTags();?>
    <title><?=Html::encode($this->title);?></title>
    <?php $this->head();?>

</head>

<?php $this->beginBody();?>


<div class="wrapper">

    <div class="login_main">

        <div class="container">
            <?=$content;?>
        </div>

   </div>


</div>

<footer>
    <p class="copy_right">Copyright (c) PaintPad 2018</p>
</footer>

<?php $this->endBody();?>
</body>
</html>
<?php $this->endPage();?>
