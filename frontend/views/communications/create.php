<?php

use yii\helpers\Html;
use frontend\assets\CalendarAsset;
CalendarAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\TblCommunications */

$this->title = 'Communications';
 $this->params['breadcrumbs'][] = ['label' => 'Tbl Communications', 'url' => ['index']];
 $this->params['breadcrumbs'][] = $this->title;

?>
<div class="tbl-communications-create">


    <h1 style="margin-top: 5%;">Communication</h1>

    <?php  //echo "<pre>";print_r($docsAll);die; ?>

    <?= $this->render('_form', [
        'model' => $model, 'bcc_to' =>$bcc_to, 'cc_email' => $cc_email, 'emails' =>$emails, 'head' => $head, 'to' => $to, 'subject' => $subject, 'pdf' => $pdf, 'type' => $type, 'jsspdfs' => $jsspdfs , 'attachments' => $attachments, 'body' => $body,'docsAll'=>$docsAll, 'newsletter_id' => $newsletter_id             
    ]) ?>

</div>
