<div class="container">
	<div class="row">
		<div class="col">
			<div class="communications-data">
				<!-- Communication data will be here -->
			</div>
		</div>
	</div>
</div>


<div class="modal" id="com-view-popup" style="width: 100%;">
	<div class="modal-dialog" style="max-width: 1000px;min-height: 500px;">
		<div class="modal-content">
			<div class="modal-body" style="max-width: 1000px;min-height: 500px;">
				<div class="row">
					<div class="col-3">
						<b>
							<span class="com-date-1" style="font-weight: 600;"></span>,
							<b style="font-weight: 600;">BY</b>
							<span class="com-by-data" style="font-weight: 600;"></span>
						</b>
						<br>
						<span class="badge badge-secondary">Email</span>
					</div>
					<div class="col-9">
						<div class="com-outer" style="box-shadow: 2px 4px 5px #bebfc0;float: left;padding: 25px;">
							<img class="hide-com-show" src="image/quoteStatus/status-declined.png" style="height: 30px;top: 2px;position: absolute;right: 20px;cursor: pointer;">
							<p>
								<b>From: </b>
								<span class="com-from-data"></span>
							</p>
							<p>
								<b>Date: </b>
								<span class="com-date-data"></span>
							</p>
							<p>
								<b>To: </b>
								<span class="com-to-data"></span>
							</p>
							<div class="devider" style="background: #cfd0d1;height: 4px;width: 100%;float: left;margin-bottom: 10px"></div>
							<p class="com-desc-data"></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
