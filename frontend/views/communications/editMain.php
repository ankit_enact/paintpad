<?php

use yii\helpers\Html;
use frontend\assets\CalendarAsset;
CalendarAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\TblCommunications */

$this->title = 'Communications';
 $this->params['breadcrumbs'][] = ['label' => 'Tbl Communications', 'url' => ['index']];
 $this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-communications-create">

    <h1>Communications</h1>

    <?= $this->render('edit', [
        'model' => $model, 'bcc_to' =>$bcc_to, 'emails' =>$emails, 'head' => $head, 'to' => $to, 'subject' => $subject, 'pdf' => $pdf, 'type' => $type, 'jsspdfs' => $jsspdfs              
    ]) ?>

</div>
