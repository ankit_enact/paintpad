<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\TblNewsletterTemplates;
use dosamigos\ckeditor\CKEditor;
use kartik\select2\Select2;

use kartik\file\FileInput;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model app\models\TblEmails */
/* @var $form yii\widgets\ActiveForm */

//echo Yii::getAlias('@jui'); exit;



if(isset($head) && $head == 1){

  echo '<style>.mainClose{ display: none; } .text-center{ display: none; } .footer{ display: none; }.menu{ display: none; }.select2-container--default .select2-selection--multiple {margin-top: 5px;}</style>';

}
?>

<div class="loader_overlay">
  <div id='loading' class="loader"></div>
</div>

<style type="text/css">
@import url('https://fonts.googleapis.com/css?family=Open+Sans&display=swap');
    .select2-results__option[aria-selected] {
        cursor: pointer;
        border-bottom: 1px solid #aaa !important;
    }
    .select2-container--default .select2-results__option--highlighted[aria-selected] {
        background-color: transparent !important;
        color: #333 !important;
        border-bottom: 1px solid #aaa !important;
    }
    .select2-container--default .select2-selection--single .select2-selection__clear {
        cursor: pointer;
        float: right;
        font-weight: 100 !important;
        font-size: 28px !important;
        top: -1px !important;
    }

    .select2-container--default .select2-selection--single .select2-selection__arrow {
        top: 3px !important;
    }
    #ui-id-1, #ui-id-2, #ui-id-3 {
        height: 170px !important;
        overflow-y: scroll !important;
        -webkit-overflow-scrolling: touch !important;
    } 

    ul.tagit li.tagit-choice-editable {
        margin: 5px 3px 0 2px !important;
    }

    #cc_email, #bcc_email {
        position: relative;
        width: 100%;
        float: left;
    }

    .close_cc, .close_bcc {
        position: absolute;
        right: 10px;
        font-size: 20px;
        top: 6px;
    }

    .loader_overlay {
        display: none;
        float: left;
        width: 100%;
        position: fixed;
        height: 100%;
        background: rgba(0,0,0,0.5);
        z-index: 99;
        top: 0;
        left: 0;
        text-align: center;
    }
    .loader {
        border: 9px solid #f3f3f3;
        border-radius: 50%;
        border-top: 9px solid #3498db;
        width: 80px;
        height: 80px;
        -webkit-animation: spin 2s linear infinite;
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
        -webkit-transform: translate(-50%, -50%);
        -moz-transform: translate(-50%, -50%);
    }

    / Safari /
    @-webkit-keyframes spin {
      0% { -webkit-transform: rotate(0deg); }
      100% { -webkit-transform: rotate(360deg); }
  }

  @keyframes spin {
      0% { transform: rotate(0deg); }
      100% { transform: rotate(360deg); }
  }


  .tbl-emails-form {
    margin-top: 30px;
}

.select2-container--default .select2-results__option[aria-selected=true] {
    background-color: #ddd;
    display: none;
}

div#filee {
    float: left;
    width: 100%;
    position: relative;
}
div#filee .abcd {
    float: none;
    position: relative;
    width: 50%;
    border: 1px solid #e0e0e0;
    margin: 0 0 10px;
    padding: 10px;
    background: #f9f9f9;
    display: table;
}
div#filee .abcd > p {
    float: left;
    width: 80%;
    margin: 0;
    font-size: 13px;
    color: #10b1f1;
    font-weight: bold;
}
div#filee .abcd p#img {
    background: transparent;
    font-size: 15px;
    float: right;
    width: auto;
    color: #8e8e8e;
    font-size: 23px;
    font-weight: bold;
    font-style: initial;
    padding: 0 7px;
    line-height: 20px;
}
.save-div div#filediv {
    float: left;
    width: 100%;
    padding: 0 0 12px;
}
input#add_more {
    background: #06abec;
    border: 1px solid #0599d4;
    color: #fff;
    padding: 4px 10px;
    font-size: 15px;
}
.save-div button.btn.btn-success {
    background: #06abec;
    border-color: #06abec;
    font-size: 15px;
    border-radius: 4px;
}


.tbl-emails-create h1 {
    display: none;
}
.cke_top {
    /*background: #00AEEF;*/
    background: #eee;    
}
.selectRow {
    display : block;
    padding : 20px;
}
.select2-container {
    width: 200px;
}
.customclass
{
  color:red;
}
.abcd img {
    max-width: 100%;
}
.abcd {
    width: 20%;
    float: left;
    border: 3px solid #eee;
    margin: 2px;
}
div#filediv {
    float: left;
    width: 25%;
    /*padding: 0 15px;*/
} 
p#img {
    font-size: 16px;
    text-align: center;
    cursor: pointer;
    background: #000;
    color: red;
    margin:0px !important;
}
.save-div {
    float: left;
    width: 100%;
    margin-top: 20px;
}
.save-div div#filediv {
    float: left;
    width: 100%;
    padding: 0;
}

.select2-container--default .select2-selection--multiple {
  margin-bottom: 10px;
  width: 100%;
}

/*input[type="file"] {
    display: block !important;
    width: 100% !important;
    height: 100% !important;
    opacity: 0 !important;
    overflow: hidden !important;
    z-index: 100 !important;
    margin-top: -20px;
    padding-top: -20px;
    top: -20px;
    }*/

    .select2-container--default .select2-selection--single {
      width: 300px;
  }

  .select2-container--default .select2-selection--single {
    background-color: #fff;
    border: 1px solid #aaa;
    border-radius: 4px;
    position: relative;
}
.select2-container--default .select2-selection--single .select2-selection__arrow b {
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    right: 6px !important;
    margin-left: 0;
    margin-top: -2px;
    position: absolute;
    top: 50%;
    width: 0;
}


input#tblcommunications-subject {
    border: 1px solid #aaa;
    background: #fff;
}
.select2-dropdown.select2-dropdown--below {
  width: 300px !important;
}

.select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
    margin-left: 4px !important;
    float: right !important;
}

.select2-container--open .select2-dropdown--below {
  border-bottom: none;
}
span#cc, span#bcc {
    /*width: 38px;
    height: 38px;
    box-shadow: 0 1px 5px #dcdcdc;*/
    background: #fff;
    border-radius: 50%;    
    display: inline-block;
    text-align: center;
    font-size: 11px;
    font-weight: normal;
    font-style: initial;
    color: #717171;
    line-height: 40px;
    margin-left: 12px;
    font-family: 'Open Sans'; 
    letter-spacing: 1px;
}


.copies {
    float: left;
    width: 100%;
    text-align: right;    
    height: 40px;
}


.select2-results__option.select2-results__message{

  display: none;
}
label.attachment {
    background: #06abec;
    position: relative;
    width: 160px;
    text-align: center;
    overflow: hidden;
    color: #fff;
    height: 35px;
    margin-bottom: 10px;
    border-radius: 4px;
}
.attachment input#file {
    opacity: 0;
    z-index: 1;
}

label.attachment span {
    color: #fff;
    font-weight: normal;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    z-index: 0;
    padding: 6px 10px;
    float: left;
}

.form-group.field-tblcommunications-cc_email {
    margin-bottom: 0;
}
.help-block {
    display: block;
    margin-top: 2px;
    margin-bottom: 2px;
    color: #737373;
}
.form-group.field-tblcommunications-receiver_email.required {
    margin-bottom: 0;
}
.form-group.field-tblcommunications-bcc_email {
    margin-bottom: 0;
}
.form-group.field-tblcommunications-subject {
    margin: 10px 0 20px;
    float: left;
    width: 100%;
}
.form-group.field-tblcommunications-body {
    float: left;
    width: 100%;
}
.form-group.field-tblcommunications-newsletter_id {
    float: left;
    width: 100%;
    margin-bottom: 20px;
}
.select2-container--default .select2-selection--multiple {
    margin-bottom: 4px;
    width: 100%;
}

/*********/

ul.tagit li.tagit-choice-editable {
    padding: 3px 23px 3px 7px;
    font-size: 14px;
    border-radius: 2px;
    margin: 4px 3px 0 0;
}

ul.tagit {
    padding: 0px 3px;
    overflow: auto;
    margin-left: inherit;
    margin-right: inherit;
    float: left;
    width: 100%;
    height: auto;
    overflow: hidden;
}
input.ui-widget-content.ui-autocomplete-input {
    font-size: 14px;
}
.ui-menu .ui-menu-item .ui-menu-item-wrapper {
    font-size: 14px;
    padding: 7px 4px;
    border-bottom: 1px solid #e6e6e6;
}
.ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active {
    border: 0px solid #aaaaaa/*{borderColorActive}*/;
    background: #efefef;
    font-weight: normal/*{fwDefault}*/;
    color: #212121/*{fcActive}*/;
}
ul.tagit li.tagit-choice .tagit-close {
    cursor: pointer;
    position: absolute;
    right: .1em;
    top: 50%;
    margin-top: -1px;
    line-height: 0;
}
.ui-menu .ui-menu-item {
    border-bottom: 1px solid #ccc;
}
.ui-menu .ui-menu-item a {
    font-size: 13px;
    padding: 6px 5px !important;
    float: left;
    width: 100%;
}
.ui-state-hover, .ui-widget-content .ui-state-hover, .ui-widget-header .ui-state-hover, .ui-state-focus, .ui-widget-content .ui-state-focus, .ui-widget-header .ui-state-focus {
    border: 0px solid #999999/*{borderColorHover}*/;
    background: #f3f3f3 !important;
    font-weight: normal/*{fwDefault}*/;
    color: #212121/*{fcHover}*/;
}
.form-control {
  font-size: 16px;
  height: 40px;
}
.select2-container--default .select2-selection--single .select2-selection__rendered {
  line-height: 34px !important;
}
.select2-container--default .select2-selection--single {
  height: 39px !important;
}
.select2-container--default .select2-selection--single .select2-selection__arrow {
  height: 29px !important;
}

@media only screen and (max-width: 1024px) {
    .loader {
        border: 9px solid #f3f3f3;
        border-radius: 50%;
        border-top: 9px solid #3498db;
        width: 80px;
        height: 80px;
        -webkit-animation: spin 2s linear infinite;
        position: absolute;
        left: 46% !important;
        top: 44.8% !important;
        transform: translate(-50%, -50%);
        -webkit-transform: translate(-50%, -50%);
        -moz-transform: translate(-50%, -50%);
    }
}

</style>
<div class="wrap">
    <div class="container">
        <div class="tbl-emails-form">

           <div class="copies">
             <span class="cc" id="cc">Cc</span>
             <span class="bcc" id="bcc">Bcc</span>
         </div>



         <?php $form = ActiveForm::begin(['options'=>['id'=>'comm','enctype'=>'multipart/form-data']]); 

            //echo '<label class="control-label">Receiver Email</label>';
            //https://stackoverflow.com/questions/43607718/adding-custom-tags-style-in-select2-multiselect-dropdown

            /*echo $form->field($model, 'receiver_email')->widget(Select2::classname(), [
                'name' => 'receiver_email',
                'data' => $emails,
                'options' => [
                    'placeholder' => 'Select Email ...',
                    'multiple' => true,
                    

                ],
            ]);*/

            // echo '<pre>'; print_r($newsletter_id);

            //echo '<label class="control-label">Receiver Email</label>';

             //echo $form->field($model, 'receiver_email[]')->dropDownList($emails,['multiple'=>'multiple'])->label(false);

            $defaultEmail       = (isset($to)) ? $to : '';
            $subject            = (isset($subject)) ? $subject : '';
            $typeid             = (isset($type)) ? $type : 0;
            $newsletter_id      = (!empty($newsletter_id)) ? $newsletter_id : '';

             // echo '<pre>'; print_r($newsletter_id);die;

            ?>

            

            <?php echo $form->field($model, 'receiver_email')->textInput(['maxlength' => true,'placeholder'=>'To','id'=>'receiver_email' ,'autocomplete'=>'singleFieldTags','value'=>$defaultEmail])->label(false) ?>

            <div id="cc_email">
              <?php echo $form->field($model, 'cc_email')->textInput(['maxlength' => true,'placeholder'=>'To','id'=>'cc_email_input','value'=>$cc_email])->label(false) ?>     
              <span class="close_cc">&times;</span>          
          </div>

          <div id="bcc_email">
              <?php echo $form->field($model, 'bcc_email')->textInput(['maxlength' => true,'placeholder'=>'To','id'=>'bcc_email_input','value'=>$bcc_to])->label(false) ?>              
              <span class="close_bcc">&times;</span> 
          </div>


          <?= $form->field($model, 'subject')->textInput(['maxlength' => true,'placeholder'=>'Subject','value'=>$subject])->label(false) ?>

          <?php //echo $newsletter_id;die('<<<<----') ?>

          <?= $form->field($model, 'newsletter_id')->dropDownList(
             ArrayHelper::map(TblNewsletterTemplates::find()->where([ 'temp_type' => $typeid])->all(),'temp_id','temp_name'),['value'=> $newsletter_id,'class'=>'form-contol','onchange'=>'
             $.post( "'.Yii::$app->urlManager->createUrl('tbl-emails/template?id=').'"+$(this).val(), function( data ) {
                console.log("data");
                console.log(data);                 

                var editor = CKEDITOR.instances[ "tblcommunications-body" ];
                editor.setData(data);

            });'

        ])->label(false);

        ?>

        <?= $form->field($model, 'body')->widget(CKEditor::className(), [
            'options' => ['rows' => 6,],
            'preset' => 'advance',
            'clientOptions' => [
              'extraPlugins' => '',
              'height' => 200,

                  //Here you give the action who will handle the image upload 
              'filebrowserUploadUrl' => Yii::$app->urlManager->createUrl('tbl-emails/ckeditor_image_upload'),

              'toolbarGroups' => [
                  ['name' => 'undo'],
                  ['name' => 'basicstyles', 'groups' => ['basicstyles', 'cleanup']],
                  ['name' => 'paragraph', 'groups' => ['list', 'indent', 'blocks', 'align', 'bidi' ]],
                  ['name' => 'styles'],
                  ['name' => 'links', 'groups' => ['links', 'insert']],
              ]

          ]

      ])->label(false) ?>
        <?php  if (strpos($_SERVER['REDIRECT_URL'], 'communications/edit') !== false) { ?>
            <input type="hidden" name="comm_id_edit" value="<?php echo $_REQUEST['cid']; ?>">
        
        <?php  }
        if(!empty($attachments)){ 
            $attachmentsExplode = explode(',', $attachments);
            ?>

            <div  class="form-group field-tblcommunications-body1">
                <?php foreach ($attachmentsExplode as $key => $imgdata) { 
                    ?>
                    <p style="border: 1px solid #ccc;list-style: none;padding: 3px 10px;margin: 0;">
                        &nbsp;&nbsp;<?php echo $imgdata; ?> <input type="hidden" name="attachment_old[]" value="<?php echo $imgdata; ?>">
                        <a href="javascript:void(0)" class="pull-right" onclick="removeCheckMdl('<?php echo $imgdata; ?>',$(this).closest('p'))">&times;</a>
                    </p>
                <?php } ?>
            </div>

        <?php } ?>


        <?php 


            if(isset($pdf) && $pdf !=''){

                if(strpos($pdf, ',') !== false){
                    $allPdf = explode(',', $pdf);
                    foreach ($allPdf as $key => $pdf) {
                        $pdf = explode('quotepdf/', $pdf);
                        $pdf = $pdf[1];
                          //echo 'here'.$pdf; exit;
                        if($key > 0){
                            $n = $key;
                        }else{
                            $n = '';
                        }

                        // echo '<div id="filee"><div class="abcd"><p src="">'.$pdf.'</p><p id="img" class="pdfdel">&times;</p></div></div>
                        echo '<div id="filee"><div class="abcd"><p src="">'.$pdf.'</p></div></div>
                        <input type="hidden" name="attachment_old[]" value="'.$pdf.'">';


                    }
                }else{
                    $pdf = explode('quotepdf/', $pdf);

                    $pdf = $pdf[1];
                    // echo '<div id="filee"><div class="abcd"><p src="">'.$pdf.'</p><p id="img" class="pdfdel">&times;</p></div></div>
                    echo '<div id="filee"><div class="abcd"><p src="">'.$pdf.'</p></div></div>
                        <input type="hidden" name="attachment_old[]" value="'.$pdf.'">';
                    
                }
                

            }else{

              echo '<div id="filee"></div>';
          }

          echo '<div id="filee">';
          foreach($jsspdfs as $k=>$v){
            // echo '<div class="abcd"><p>'.$v.'</p><p id="img" cntr="'.$k.'" class="jssdel">&times;</p></div>
            echo '<div class="abcd"><p>'.$v.'</p></div>
                        <input type="hidden" name="attachment_old[]" value="'.$v.'">';                            
                        }//foreach
                        echo '</div>';

                        ?> 
                        <div class="form-group">
                            <div id="filediv"> 
                                <label class="attachment">
                                  <a href="javascript:void(0);" data-toggle="modal" data-target="#libraryDocModal"><span>Attach Files</span></a>
                              </label> 
                          </div>

                      </div>

                      <div class="save-div">
                        <div class="form-group">
                            
                          <input type="hidden" id="add_more" class="upload"/>
                      </div>

                      <div class="form-group">
                        <?= Html::submitButton('Send', ['class' => 'btn btn-success']) ?>
                    </div>
                </div>

                <!-- Modal -->
                <div class="modal fade" id="libraryDocModal" role="dialog">
                    <div class="modal-dialog modal-lg" style="min-width: 668px;">

                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h2 class="modal-title" style="text-align: center;">Library document</h2>
                      </div>
                      <div class="modal-body" style="overflow: auto; max-height:calc(100vh - 200px);">
                        <div  class="form-group field-tblcommunications-bod" >
                            <?php 

                            if(!empty($attachments)){ 
                                $attachmentsExplode = explode(',', $attachments);
                            }   

                            // echo "<pre>";print_r($docsAll);die; 

                            ?>
                            <label class='attachment'><input name='TblCommunications[attachment][]' type='file' id='file'  accept="application/msword, text/plain, application/pdf, image/*" multiple /><span>Choose File </span></label>
                            <div class="accordion collapse-main-div" id="accordionExample">
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h2 class="mb-0">
                                            <!-- <p class="card-heading">Library Documents </p>   -->
                                            <button type="button" class="btn btn-link" id="libCollapsBtn" data-toggle="collapse" data-target="#collapseOne">
                                              <i class="fa fa-plus"></i> Library Documents 
                                             </button>                  
                                        </h2>
                                    </div>
                                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                        <div class="card-body">
                            <?php if($docsAll){
                                // echo "<pre>";print_r($docsAll);die;
                                foreach ($docsAll as $key => $img) { 
                                    if($img->file_path_name != ''){
                                        ?>
                                        <p style="border: 1px solid #ccc;list-style: none;padding: 3px 10px;margin: 0;">
                                            <label class="container">
                                                <?php echo $img->file_name; ?>
                                              <input 
                                              type="checkbox"  class="docLib" data-id="" data-fname = "<?php echo $img->file_name; ?>" data-val= "<?php echo $img->file_path_name; ?>" value="<?php echo $img->file_name; ?>"
                                              <?php
                                                if($attachmentsExplode){
                                                    if(in_array($img->file_path_name, $attachmentsExplode)){
                                                        echo "checked='checked'";
                                                    }

                                                }
                                               ?>
                                              >
                                              <span class="checkmark"></span>
                                            </label>
                                            
                                        </p>
                                        <?php 
                                    }
                                }
                            }
                            ?>
                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- End Modal -->

        <?php ActiveForm::end(); ?>

    </div>
</div>
</div>



<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css" rel="stylesheet"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.full.min.js"></script>



<script type="text/javascript">  

    //removeCheckMdl 

    function removeCheckMdl(vals,closeP){
        $(closeP).remove(); 
        // alert(vals);
        $('#libraryDocModal p').find('input[data-val="'+vals+'"]').removeAttr('checked');
    }

    //removeCheckMdl

</script>

<script type="text/javascript">  

    $(function(){

        $('.close_cc').on('click', function(){
          $(this).parent().hide();
          $('#cc').show();
      });
        $('.close_bcc').on('click', function(){
          $(this).parent().hide();
          $('#bcc').show();
      });




    //Hide/Show Cc/Bccif no tags are selected in them
    $('#tblcommunications-subject').on('click',function(){
      //hide Cc
      if(!$('#cc_email_input').val().length){
        //console.log('It has no tags');
        $('#cc_email').hide();
        $('#cc').show();
    }
      //hide Bcc
      if(!$('#bcc_email_input').val().length){
        //console.log('It has no tags');
        $('#bcc_email').hide();
        $('#bcc').show();
    }
});

    var sampleTags = <?php echo json_encode($emails) ?>;
    var sampleTags1 = <?php echo json_encode($emails) ?>;
    var sampleTags2 = <?php echo json_encode($emails) ?>;

    // for receiver email
    $('#receiver_email').tagit({
        autocomplete: { autoFocus :true},
        availableTags: sampleTags,
        animate:false,
        placeholderText: 'To',
        beforeTagAdded: function(event, ui) {
            if(!isEmail(ui.tagLabel))
            {
               //alert('Please enter a valid email');
               $('.tagit-new input').val('');
               return false;
           }
       },    
       afterTagAdded: function(event, ui) {
        showHidePlaceholder($('#receiver_email'));
    },
    afterTagRemoved: function(event, ui) {
        showHidePlaceholder($('#receiver_email'));
    }

});

    // for Cc email
    $('#cc_email_input').tagit({
        autocomplete: { autoFocus :true},
        availableTags: sampleTags1,
        animate:false,
        placeholderText: 'Cc',
        beforeTagAdded: function(event, ui) {
            if(!isEmail(ui.tagLabel))
            {
               //alert('Please enter a valid email');
               $('.tagit-new input').val('');
               return false;
           }
       },           
       afterTagAdded: function(event, ui) {
        showHidePlaceholder($('#cc_email_input'));
    },
    afterTagRemoved: function(event, ui) {
        showHidePlaceholder($('#cc_email_input'));
    }

});

     // for Bcc email
     $('#bcc_email_input').tagit({
        autocomplete: { autoFocus :true},
        availableTags: sampleTags2,
        animate:false,
        placeholderText: 'Bcc',
        beforeTagAdded: function(event, ui) {
            if(!isEmail(ui.tagLabel))
            {
               //alert('Please enter a valid email');
               $('.tagit-new input').val('');
               return false;
           }
       },           
       afterTagAdded: function(event, ui) {
        showHidePlaceholder($('#bcc_email_input'));
    },
    afterTagRemoved: function(event, ui) {
        showHidePlaceholder($('#bcc_email_input'));
    }

});

 });

    function showHidePlaceholder($tagit){
       var $input = $tagit.data("ui-tagit").tagInput,
       placeholderText = $tagit.data("ui-tagit").options.placeholderText;

       if ($tagit.tagit("assignedTags").length > 0) {
        $input.removeAttr('placeholder');
    } else {
        $input.attr('placeholder', placeholderText);
    }
}
function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}


$("#tblcommunications-newsletter_id").select2({
   tags: true,
   placeholder: "Select Template",
   allowClear: true, 
   closeOnSelect: true,
   minimumResultsForSearch: -1,
});

/************ Remove PDF on cancel ***************/

$('document').ready(function() {

  $('.pdfdel').on('click',function(){
    $('#filee').append("<input type='hidden' name='TblCommunications[pdfindx]' value='0'/>");
    $(this).parent().remove();
});
  $('.pdfdel').on('click',function(){
    $('#filee').append("<input type='hidden' name='TblCommunications[pdfindx]' value='0'/>");
    $(this).parent().remove();
});

  $(".jssdel").click(function() {

    //alert($(this).attr('cntr'));
    var indxx = $(this).attr('cntr');

    if(indxx == 0){
      $('#filee').append("<input type='hidden' name='TblCommunications[jssindx]''/>");
  }else{
      $('#filee').append("<input type='hidden' name='TblCommunications[jssindx1]' value='"+indxx+"'/>");
  }

  $(this).parent().remove();
});


});


$(document).ready(function() {

    $.post('<?php echo Yii::$app->urlManager->createUrl('tbl-emails/defaulttemp?id='.$typeid)?>', function( data ) {
      // console.log("data");
      // console.log(data);                 
      
      var editor = CKEDITOR.instances[ "tblcommunications-body" ];
      editor.setData(data);

  });

});




var abc = 0;      // Declaring and defining global increment variable.

$(document).ready(function() {

    $('.loader_overlay').hide();  

    $('#cc_email').hide();
    $('#bcc_email').hide();

    $('.docLib').click(function(){
        if(!$(this).is(':checked')){
            var f = $(".field-tblcommunications-body input[value='"+$(this).data("val")+"'], .field-tblcommunications-body1 input[value='"+$(this).data("val")+"']").closest('p').remove();
        }else{
            var encUrl = "http://www.enacteservices.net/paintpad/uploads/"+$(this).data("val");
            var valD = $(this).data("val");
            $('.field-tblcommunications-body').append('<p style="border: 1px solid #ccc;list-style: none;padding: 3px 10px;margin: 0;">&nbsp;&nbsp;'+$(this).data("fname")+' <input type="hidden" name="attachment_old[]" value="'+$(this).data("val")+'"><a href="javascript:void(0)" class="pull-right" data-val="'+$(this).data("val")+'" onclick="removeCheckMdl(\''+valD+'\',$(this).closest(\'p\'))">&times;</a></p>');

        }
    });


    $('#cc').click(function(){
        $(this).hide();
        $('#cc_email').show();
    });
    $('#bcc').click(function(){
        $(this).hide();
        $('#bcc_email').show();
    });

    $(document).on('click','#libCollapsBtn',function(){
        $('#libCollapsBtn i').toggleClass("fa-plus fa-minus");
    });

    // Following function will executes on change event of file input to select different file.
    $('body').on('change', '#file', function() {
        console.log(this.files);
        if (this.files && this.files[0]) {
            for(cf=0; cf< this.files.length ; cf++){
                abc += 1; // Incrementing global variable by 1.
                var z = abc - 1;
                var x = $(this).parent().find('#previewimg' + z).remove();
                $('#filee').append("<div id='abcd" + abc + "' indx='" + z + "' class='abcd'><p id='previewimg" + abc + "'>"+this.files[cf].name+"</</div>");
                    $('.modal-body .field-tblcommunications-bod .attachment').hide();
                    var delImgFun = 'delImg("#abcd'+ abc+'")';
                    // var tt = '';
                    // alert(tt);
                    $("#abcd" + abc).append($("<p id='img' class='del' data-id='#abcd"+ abc+"'>&times;</p>").click(function() {
                    // $("#abcd"+abc).remove();
                    $(this).parent('div').remove();
                     var attr = $(this).parent().attr('indx');
                    // alert(attr);
                    $('#filee').append("<input type='hidden' name='TblCommunications[indexx][]' value='" + attr + "'/>");
                    // $(this).parent().remove();
                }));
                }
                $('.modal-body .field-tblcommunications-bod').prepend("<label class='attachment'><input name='TblCommunications[attachment][]' type='file' id='file' accept='application/msword, text/plain, application/pdf, image/*' multiple /><span>Choose File </span></label>")
            
        }
    });


    // $(document).on('click','.del',function() {
    //     console.log($(this).closest('#'));
    //     console.log($($(this).closest('div')).textContent());
    // });

    // To Preview Image
    function imageIsLoaded(e) {
        $('#previewimg' + abc).attr('src', e.target.result);
    };

    $('#upload').click(function(e) {
        var name = $(":attachment").val();
        if (!name) {
            alert("First Image Must Be Selected");
            e.preventDefault();
        }
    });


    $('form#comm').on('beforeSubmit', function () {

        /*** for TO email section ***/
        var to_flag = true;
    //console.log($('#receiver_email').val());
    var s = $('#receiver_email').val();
    var a = s.split(',')
    console.log(a)

    for (i = 0; i < a.length; i++) {
      if(a[i].indexOf('@') == -1){
         console.log(a[i]);         
         to_flag = false;
     }
    } //for

    if( to_flag == false ){
      alert('The address in the "To" field was not recognized. Please make sure that all addresses are properly formed.');
      return false;
  }

  /*** for Cc email section ***/
  var cc_flag = true;
    //console.log($('#cc_email_input').val());
    var s1 = $('#cc_email_input').val();

    if( s1 !== null && s1 !== '' ){

        var a1 = s1.split(',');    
        console.log('a1');
        console.log(a1)

        for (j = 0; j < a1.length; j++) {
          if(a1[j].indexOf('@') == -1){
             console.log(a1[j]);         
             cc_flag = false;
         }
        } //for
    }

    if( cc_flag == false ){
        alert('The address in the "Cc" field was not recognized. Please make sure that all addresses are properly formed.');
        return false;
    }


    /*** for Bcc email section ***/
    var bcc_flag = true;
    //console.log($('#cc_email_input').val());
    var s2 = $('#bcc_email_input').val();

    if( s2 !== null && s2 !== '' ){

        var a2 = s2.split(',');
        console.log(a2);

        for (k = 0; k < a2.length; k++) {
          if(a2[k].indexOf('@') == -1){
             console.log(a2[k]);         
             bcc_flag = false;
         }
        } //for      
    }

    if( bcc_flag == false ){
        alert('The address in the "Bcc" field was not recognized. Please make sure that all addresses are properly formed.');
        return false;
    }

    $('.loader_overlay').show();

    return true;

});


});

</script>
<?php  if(!empty($body)){ ?>
    <script type="text/javascript">  
        $(document).ready(function() {
            setTimeout(function(){              
                var editor = CKEDITOR.instances[ "tblcommunications-body" ];
                editor.setData(<?= json_encode($body); ?>);
            }, 3000); 
        });
    </script>
<?php } ?>

<?php  if(!empty($cc_email)){ ?>
    <script type="text/javascript">  
        $(document).ready(function() {
            setTimeout(function(){      
                $("#cc").trigger('click');
            }, 3000); 
        });
    </script>
<?php } ?>

<?php  if(!empty($bcc_to)){ ?>
    <script type="text/javascript">  
        $(document).ready(function() {
            setTimeout(function(){     
                $("#bcc").trigger('click');
            }, 3000); 
        });
    </script>
<?php } ?>

<?php 
if (strpos($_SERVER['REDIRECT_URL'], 'communications/edit') !== false) {
    $Url = $_SERVER['REDIRECT_URL'];
    $newUrl = str_replace("edit","create",$Url);
    ?>
    <script type="text/javascript">  
        $(document).ready(function() {
            setTimeout(function(){ 
                $("#comm").attr("action", "<?= $newUrl ?>");
            }, 3000); 
        });
    </script>
<?php } ?>
