<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\TblNewsletterTemplates;
use dosamigos\ckeditor\CKEditor;
use kartik\select2\Select2;

use kartik\file\FileInput;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model app\models\TblEmails */
/* @var $form yii\widgets\ActiveForm */

//echo Yii::getAlias('@jui'); exit;

//echo '<pre>'; print_r($emails); exit;

if(isset($head) && $head == 1){
 
  echo '<style>.mainClose{ display: none; } .text-center{ display: none; } .footer{ display: none; }.menu{ display: none; }.select2-container--default .select2-selection--multiple {margin-top: 5px;}</style>';

}
?>

<div class="loader_overlay">
  <div id='loading' class="loader"></div>
</div>

<style type="text/css">

.select2-results__option[aria-selected] {
cursor: pointer;
border-bottom: 1px solid #aaa !important;
}
.select2-container--default .select2-results__option--highlighted[aria-selected] {
background-color: transparent !important;
color: #333 !important;
border-bottom: 1px solid #aaa !important;
}
.select2-container--default .select2-selection--single .select2-selection__clear {
cursor: pointer;
float: right;
font-weight: 100 !important;
font-size: 28px !important;
top: -1px !important;
}

.select2-container--default .select2-selection--single .select2-selection__arrow {
    top: 3px !important;
}
#ui-id-1, #ui-id-2, #ui-id-3 {
    height: 170px !important;
    overflow-y: scroll !important;
    -webkit-overflow-scrolling: touch !important;
} 

ul.tagit li.tagit-choice-editable {
    margin: 5px 3px 0 2px !important;
}

#cc_email, #bcc_email {
    position: relative;
    width: 100%;
    float: left;
}

.close_cc, .close_bcc {
    position: absolute;
    right: 10px;
    font-size: 20px;
    top: 6px;
}

.loader_overlay {
    display: none;
    float: left;
    width: 100%;
    position: fixed;
    height: 100%;
    background: rgba(0,0,0,0.5);
    z-index: 99;
    top: 0;
    left: 0;
    text-align: center;
}
.loader {
    border: 9px solid #f3f3f3;
    border-radius: 50%;
    border-top: 9px solid #3498db;
    width: 80px;
    height: 80px;
    -webkit-animation: spin 2s linear infinite;
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    -webkit-transform: translate(-50%, -50%);
    -moz-transform: translate(-50%, -50%);
}

/ Safari /
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}


.tbl-emails-form {
    margin-top: 30px;
}

.select2-container--default .select2-results__option[aria-selected=true] {
    background-color: #ddd;
    display: none;
}

div#filee {
    float: left;
    width: 100%;
    position: relative;
}
div#filee .abcd {
    float: none;
    position: relative;
    width: 50%;
    border: 1px solid #e0e0e0;
    margin: 0 0 10px;
    padding: 10px;
    background: #f9f9f9;
    display: table;
}
div#filee .abcd > p {
    float: left;
    width: 80%;
    margin: 0;
    font-size: 13px;
    color: #10b1f1;
    font-weight: bold;
}
div#filee .abcd p#img {
    background: transparent;
    font-size: 15px;
    float: right;
    width: auto;
    color: #8e8e8e;
    font-size: 23px;
    font-weight: bold;
    font-style: initial;
    padding: 0 7px;
    line-height: 20px;
}
.save-div div#filediv {
    float: left;
    width: 100%;
    padding: 0 0 12px;
}
input#add_more {
    background: #06abec;
    border: 1px solid #0599d4;
    color: #fff;
    padding: 4px 10px;
    font-size: 15px;
}
.save-div button.btn.btn-success {
    background: #06abec;
    border-color: #06abec;
    font-size: 15px;
    border-radius: 4px;
}


.tbl-emails-create h1 {
    display: none;
}
.cke_top {
    /*background: #00AEEF;*/
    background: #eee;    
}
.selectRow {
    display : block;
    padding : 20px;
}
.select2-container {
    width: 200px;
}
.customclass
{
  color:red;
}
.abcd img {
    max-width: 100%;
}
.abcd {
    width: 20%;
    float: left;
    border: 3px solid #eee;
    margin: 2px;
}
div#filediv {
    float: left;
    width: 25%;
   /*padding: 0 15px;*/
} 
p#img {
    font-size: 16px;
    text-align: center;
    cursor: pointer;
    background: #000;
    color: red;
    margin:0px !important;
}
.save-div {
    float: left;
    width: 100%;
    margin-top: 20px;
}
.save-div div#filediv {
    float: left;
    width: 100%;
    padding: 0;
}

.select2-container--default .select2-selection--multiple {
  margin-bottom: 10px;
  width: 100%;
}

/*input[type="file"] {
    display: block !important;
    width: 100% !important;
    height: 100% !important;
    opacity: 0 !important;
    overflow: hidden !important;
    z-index: 100 !important;
    margin-top: -20px;
    padding-top: -20px;
    top: -20px;
}*/

.select2-container--default .select2-selection--single {
  width: 300px;
}

.select2-container--default .select2-selection--single {
    background-color: #fff;
    border: 1px solid #aaa;
    border-radius: 4px;
    position: relative;
}
.select2-container--default .select2-selection--single .select2-selection__arrow b {
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    right: 6px !important;
    margin-left: 0;
    margin-top: -2px;
    position: absolute;
    top: 50%;
    width: 0;
}


input#tblcommunications-subject {
    border: 1px solid #aaa;
    background: #fff;
}
.select2-dropdown.select2-dropdown--below {
  width: 300px !important;
}

.select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
    margin-left: 4px !important;
    float: right !important;
}

.select2-container--open .select2-dropdown--below {
  border-bottom: none;
}
span#cc, span#bcc {
    /*width: 38px;
    height: 38px;
    box-shadow: 0 1px 5px #dcdcdc;*/
    background: #fff;
    border-radius: 50%;    
    display: inline-block;
    text-align: center;
    font-size: 11px;
    font-weight: normal;
    font-style: initial;
    color: #717171;
    line-height: 40px;
    margin-left: 12px;
    font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
    letter-spacing: 1px;
}


.copies {
    float: left;
    width: 100%;
    text-align: right;    
    height: 40px;
}


.select2-results__option.select2-results__message{

  display: none;
}
label.attachment {
    background: #06abec;
    position: relative;
    width: 160px;
    text-align: center;
    overflow: hidden;
    color: #fff;
    height: 35px;
    margin-bottom: 10px;
    border-radius: 4px;
}
.attachment input#file {
    opacity: 0;
    z-index: 1;
}

label.attachment span {
    color: #fff;
    font-weight: normal;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    z-index: 0;
    padding: 6px 10px;
    float: left;
}

.form-group.field-tblcommunications-cc_email {
    margin-bottom: 0;
}
.help-block {
    display: block;
    margin-top: 2px;
    margin-bottom: 2px;
    color: #737373;
}
.form-group.field-tblcommunications-receiver_email.required {
    margin-bottom: 0;
}
.form-group.field-tblcommunications-bcc_email {
    margin-bottom: 0;
}
.form-group.field-tblcommunications-subject {
    margin: 10px 0 20px;
    float: left;
    width: 100%;
}
.form-group.field-tblcommunications-body {
    float: left;
    width: 100%;
}
.form-group.field-tblcommunications-newsletter_id {
    float: left;
    width: 100%;
    margin-bottom: 20px;
}
.select2-container--default .select2-selection--multiple {
    margin-bottom: 4px;
    width: 100%;
}

/*********/

ul.tagit li.tagit-choice-editable {
    padding: 3px 23px 3px 7px;
    font-size: 14px;
    border-radius: 2px;
    margin: 4px 3px 0 0;
}

ul.tagit {
    padding: 0px 3px;
    overflow: auto;
    margin-left: inherit;
    margin-right: inherit;
    float: left;
    width: 100%;
    height: auto;
    overflow: hidden;
}
input.ui-widget-content.ui-autocomplete-input {
    font-size: 14px;
}
.ui-menu .ui-menu-item .ui-menu-item-wrapper {
    font-size: 14px;
    padding: 7px 4px;
    border-bottom: 1px solid #e6e6e6;
}
.ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active {
    border: 0px solid #aaaaaa/*{borderColorActive}*/;
    background: #efefef;
    font-weight: normal/*{fwDefault}*/;
    color: #212121/*{fcActive}*/;
}
ul.tagit li.tagit-choice .tagit-close {
    cursor: pointer;
    position: absolute;
    right: .1em;
    top: 50%;
    margin-top: -1px;
    line-height: 0;
}
.ui-menu .ui-menu-item {
    border-bottom: 1px solid #ccc;
}
.ui-menu .ui-menu-item a {
    font-size: 13px;
    padding: 6px 5px !important;
    float: left;
    width: 100%;
}
.ui-state-hover, .ui-widget-content .ui-state-hover, .ui-widget-header .ui-state-hover, .ui-state-focus, .ui-widget-content .ui-state-focus, .ui-widget-header .ui-state-focus {
    border: 0px solid #999999/*{borderColorHover}*/;
    background: #f3f3f3 !important;
    font-weight: normal/*{fwDefault}*/;
    color: #212121/*{fcHover}*/;
}
.form-control {
  font-size: 16px;
  height: 40px;
}
.select2-container--default .select2-selection--single .select2-selection__rendered {
  line-height: 34px !important;
}
.select2-container--default .select2-selection--single {
  height: 39px !important;
}
.select2-container--default .select2-selection--single .select2-selection__arrow {
  height: 29px !important;
}

@media only screen and (max-width: 1024px) {
.loader {
    border: 9px solid #f3f3f3;
    border-radius: 50%;
    border-top: 9px solid #3498db;
    width: 80px;
    height: 80px;
    -webkit-animation: spin 2s linear infinite;
    position: absolute;
    left: 46% !important;
    top: 44.8% !important;
    transform: translate(-50%, -50%);
    -webkit-transform: translate(-50%, -50%);
    -moz-transform: translate(-50%, -50%);
}
}

</style>
<div class="wrap">
    <div class="container">
        <div class="tbl-emails-form">

           <div class="copies">
             <span class="cc" id="cc">Cc</span>
             <span class="bcc" id="bcc">Bcc</span>
           </div>

            <?php $form = ActiveForm::begin(['options'=>['id'=>'comm','enctype'=>'multipart/form-data']]); 

            //echo '<label class="control-label">Receiver Email</label>';
            //https://stackoverflow.com/questions/43607718/adding-custom-tags-style-in-select2-multiselect-dropdown
            
            /*echo $form->field($model, 'receiver_email')->widget(Select2::classname(), [
                'name' => 'receiver_email',
                'data' => $emails,
                'options' => [
                    'placeholder' => 'Select Email ...',
                    'multiple' => true,
                    

                ],
            ]);*/

            //echo '<pre>'; print_r($emails); exit;

            //echo '<label class="control-label">Receiver Email</label>';

             //echo $form->field($model, 'receiver_email[]')->dropDownList($emails,['multiple'=>'multiple'])->label(false);

                $defaultEmail = (isset($to)) ? $to : '';
                $subject      = (isset($subject)) ? $subject : '';
                $typeid       = (isset($type)) ? $type : 0;

            ?>

            <?php echo $form->field($model, 'receiver_email')->textInput(['maxlength' => true,'placeholder'=>'To','id'=>'receiver_email' ,'autocomplete'=>'singleFieldTags','value'=>$defaultEmail])->label(false) ?>

            <div id="cc_email">
              <?php echo $form->field($model, 'cc_email')->textInput(['maxlength' => true,'placeholder'=>'To','id'=>'cc_email_input'])->label(false) ?>     
              <span class="close_cc">&times;</span>          
            </div>

            <div id="bcc_email">
              <?php echo $form->field($model, 'bcc_email')->textInput(['maxlength' => true,'placeholder'=>'To','id'=>'bcc_email_input','value'=>$bcc_to])->label(false) ?>              
              <span class="close_bcc">&times;</span> 
            </div>
            

            <?= $form->field($model, 'subject')->textInput(['maxlength' => true,'placeholder'=>'Subject','value'=>$subject])->label(false) ?>

            <?php //echo '<label class="control-label">Select Template</label>'; ?>

            <?= $form->field($model, 'newsletter_id')->dropDownList(
                 ArrayHelper::map(TblNewsletterTemplates::find()->where([ 'temp_type' => $typeid])->all(),'temp_id','temp_name'),
                ['class'=>'form-contol','onchange'=>'
                  $.post( "'.Yii::$app->urlManager->createUrl('tbl-emails/template?id=').'"+$(this).val(), function( data ) {
                    console.log("data");
                    console.log(data);                 
                    
                    console.log( CKEDITOR.instances[instanceName] );
                    var editor = CKEDITOR.instances[ "tblcommunications-body" ];
                    editor.setData(data);

                  });'

                ])->label(false);

            ?>

            <?= $form->field($model, 'body')->widget(CKEditor::className(), [
                'options' => ['rows' => 6,],
                'preset' => 'advance',
                'clientOptions' => [
                  'extraPlugins' => '',
                  'height' => 200,

                  //Here you give the action who will handle the image upload 
                  'filebrowserUploadUrl' => Yii::$app->urlManager->createUrl('tbl-emails/ckeditor_image_upload'),

                  'toolbarGroups' => [
                      ['name' => 'undo'],
                      ['name' => 'basicstyles', 'groups' => ['basicstyles', 'cleanup']],
                      ['name' => 'paragraph', 'groups' => ['list', 'indent', 'blocks', 'align', 'bidi' ]],
                      ['name' => 'styles'],
                      ['name' => 'links', 'groups' => ['links', 'insert']],
                  ]

                ]

            ])->label(false) ?>

            <!-- <?= $form->field($model, 'attachment[]')->fileInput(['multiple' => true, 'maxlength' => true]) ?> -->

            <?php 

              /*echo $form->field($model, 'attachment[]')->widget(FileInput::classname(), [
                    'options' => ['multiple' => true, 'maxlength' => true],
                    'pluginOptions' => [
                      'previewFileType' => 'any',
                      'showPreview' => true,
                      'showCaption' => true,
                      'showRemove' => true,
                      'showUpload' => false,
                      'uploadClass' => 'hide',
                      'overwriteInitial'=>true,
                      'initialPreviewAsData'=>true,
                      'fileActionSettings' =>['showUpload' => false],
                      'maxFileCount' => 5,
                      'uploadUrl'=> '/file-upload-batch/2',
                    'fileActionSettings' => [
                        'showZoom' => true,
                        'showRemove' => true,
                        'showUpload' => false,
                    ],

                ]                  
                ]);*/

                // if(isset($pdf) && $pdf !=''){

                //   $pdf = explode('quotepdf/', $pdf);
                //   //echo '<pre>'; print_r($pdf); exit;

                //   $pdf = $pdf[1];
                //   //echo 'here'.$pdf; exit;

                //   echo '<div id="filee"><div class="abcd"><p src="">'.$pdf.'</p><p id="img" class="pdfdel">&times;</p></div></div>';

                // }else{

                //   echo '<div id="filee"></div>';
                // }

                //  show jss default files if type is 2 

                // //echo " => ".$type." <=";
                // //if($type == 2){
                //     echo '<div id="filee">';
                //         //echo '<pre>'; print_r($jsspdfs); exit;
                //         foreach($jsspdfs as $k=>$v){
                //             echo '<div class="abcd"><p>'.$v.'</p><p id="img" cntr="'.$k.'" class="jssdel">&times;</p></div>';                            
                //         }//foreach
                //     echo '</div>';
                //}

            ?> 

            <!-- <div id="filee"></div> -->
            <div class="form-group">
              
              <div id="filediv"> 
                <label class="attachment">
                  <input name="TblCommunications[attachment][]" type="file" id="file" multiple/>
                  <span>Attach Files</span>
                </label> 
              </div>

            </div>

            <div class="save-div">
            <div class="form-group">
              <input type="hidden" id="add_more" class="upload"/>
            </div>
            
            <div class="form-group">
                <?= Html::submitButton('Send', ['class' => 'btn btn-success']) ?>
            </div>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>


<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css" rel="stylesheet"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.full.min.js"></script>

<script type="text/javascript">  

$(function(){

    $('.close_cc').on('click', function(){
      $(this).parent().hide();
      $('#cc').show();
    });
    $('.close_bcc').on('click', function(){
      $(this).parent().hide();
      $('#bcc').show();
    });




    //Hide/Show Cc/Bccif no tags are selected in them
    $('#tblcommunications-subject').on('click',function(){
      //hide Cc
      if(!$('#cc_email_input').val().length){
        //console.log('It has no tags');
        $('#cc_email').hide();
        $('#cc').show();
      }
      //hide Bcc
      if(!$('#bcc_email_input').val().length){
        //console.log('It has no tags');
        $('#bcc_email').hide();
        $('#bcc').show();
      }
    });

    //var sampleTags = ['c++', 'java', 'php', 'coldfusion', 'javascript', 'asp', 'ruby', 'python', 'c', 'scala', 'groovy', 'haskell', 'perl', 'erlang', 'apl', 'cobol', 'go', 'lua'];

    var sampleTags = <?php echo json_encode($emails) ?>;
    var sampleTags1 = <?php echo json_encode($emails) ?>;
    var sampleTags2 = <?php echo json_encode($emails) ?>;

    // for receiver email
    $('#receiver_email').tagit({
        autocomplete: { autoFocus :true},
        availableTags: sampleTags,
        animate:false,
        placeholderText: 'To',
        beforeTagAdded: function(event, ui) {
            if(!isEmail(ui.tagLabel))
            {
               //alert('Please enter a valid email');
               $('.tagit-new input').val('');
               return false;
            }
        },    
        afterTagAdded: function(event, ui) {
            showHidePlaceholder($('#receiver_email'));
        },
        afterTagRemoved: function(event, ui) {
            showHidePlaceholder($('#receiver_email'));
        }

    });

    // for Cc email
    $('#cc_email_input').tagit({
        autocomplete: { autoFocus :true},
        availableTags: sampleTags1,
        animate:false,
        placeholderText: 'Cc',
        beforeTagAdded: function(event, ui) {
            if(!isEmail(ui.tagLabel))
            {
               //alert('Please enter a valid email');
               $('.tagit-new input').val('');
               return false;
            }
        },           
        afterTagAdded: function(event, ui) {
            showHidePlaceholder($('#cc_email_input'));
        },
        afterTagRemoved: function(event, ui) {
            showHidePlaceholder($('#cc_email_input'));
        }

    });

     // for Bcc email
    $('#bcc_email_input').tagit({
        autocomplete: { autoFocus :true},
        availableTags: sampleTags2,
        animate:false,
        placeholderText: 'Bcc',
        beforeTagAdded: function(event, ui) {
            if(!isEmail(ui.tagLabel))
            {
               //alert('Please enter a valid email');
               $('.tagit-new input').val('');
               return false;
            }
        },           
        afterTagAdded: function(event, ui) {
            showHidePlaceholder($('#bcc_email_input'));
        },
        afterTagRemoved: function(event, ui) {
            showHidePlaceholder($('#bcc_email_input'));
        }

    });
    
});

function showHidePlaceholder($tagit){
   var $input = $tagit.data("ui-tagit").tagInput,
       placeholderText = $tagit.data("ui-tagit").options.placeholderText;

    if ($tagit.tagit("assignedTags").length > 0) {
        $input.removeAttr('placeholder');
    } else {
        $input.attr('placeholder', placeholderText);
    }
}
function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}


$("#tblcommunications-newsletter_id").select2({
   tags: true,
   placeholder: "Select Template",
   allowClear: true, 
   closeOnSelect: true,
   minimumResultsForSearch: -1,
});

/************ Remove PDF on cancel ***************/

$('document').ready(function() {

  $('.pdfdel').on('click',function(){
    $('#filee').append("<input type='hidden' name='TblCommunications[pdfindx]' value='0'/>");
    $(this).parent().remove();
  });

  $(".jssdel").click(function() {

    //alert($(this).attr('cntr'));
    var indxx = $(this).attr('cntr');

    if(indxx == 0){
      $('#filee').append("<input type='hidden' name='TblCommunications[jssindx]''/>");
    }else{
      $('#filee').append("<input type='hidden' name='TblCommunications[jssindx1]' value='"+indxx+"'/>");
    }

    $(this).parent().remove();
  });


});


$(document).ready(function() {

    $.post('<?php echo Yii::$app->urlManager->createUrl('tbl-emails/defaulttemp?id='.$typeid)?>', function( data ) {
      console.log("data");
      console.log(data);                 
      
      var editor = CKEDITOR.instances[ "tblcommunications-body" ];
      editor.setData(data);

    });
       
});


var abc = 0;      // Declaring and defining global increment variable.

$(document).ready(function() {

    $('.loader_overlay').hide();  

    $('#cc_email').hide();
    $('#bcc_email').hide();
/* 
    if($('#bcc_email_input').val() == ""){
        $('#bcc_email_input').hide();
    }
   */

  $('#cc').click(function(){
    $(this).hide();
    $('#cc_email').show();
  });
  $('#bcc').click(function(){
    $(this).hide();
    $('#bcc_email').show();
  });


/*$('#add_more').hide();
$('#add_more').attr('disabled', 'disabled');*/

//  To add new input file field dynamically, on click of "Add More Files" button below function will be executed.
/*$('#add_more').click(function() {
  $(this).before($("<div/>", {
  id: 'filediv'
  }).fadeIn('slow').append($("<input/>", {
  name: 'TblCommunications[attachment][]',
  type: 'file',
  id: 'file'
  })));

  $(this).attr('disabled', 'disabled');
});*/

    // Following function will executes on change event of file input to select different file.
    $('body').on('change', '#file', function() {
        console.log(this.files);
        if (this.files && this.files[0]) {
            for(cf=0; cf< this.files.length ; cf++){
                abc += 1; // Incrementing global variable by 1.
                var z = abc - 1;
                var x = $(this).parent().find('#previewimg' + z).remove();
                //$('#filee').append("<div id='abcd" + abc + "' indx='" + z + "' class='abcd'><img id='previewimg" + abc + "' src=''/></div>");
                $('#filee').append("<div id='abcd" + abc + "' indx='" + z + "' class='abcd'><p id='previewimg" + abc + "'>"+this.files[cf].name+"</</div>");
                //var name = this.files[cf].name;
                //$('#previewimg' + abc).text(name);
                /*
                    var reader = new FileReader();
                    reader.onload = imageIsLoaded;
                    reader.readAsDataURL(this.files[cf]);
                    reader.fileName = this.files[cf].name;
                    reader.onloadend = function () {
                        console.log(reader.fileName); // here you can access the original file name
                        $('#previewimg' + abc).text(reader.fileName);
                    }
                */
                $('.attachment').hide();

                $("#abcd" + abc).append($("<p id='img' class='del'>&times;</p>").click(function() {
                    var attr = $(this).parent().attr('indx');
                    //alert(attr);
                    $('#filee').append("<input type='hidden' name='TblCommunications[indexx][]' value='" + attr + "'/>");
                    $(this).parent().remove();
                }));
            }

            $('#add_more').before($("<div/>", {
                id: 'filediv'
            }).fadeIn('slow').append($("<label class='attachment'><input name='TblCommunications[attachment][]' type='file' id='file' multiple /><span>Attach File</span></label>")));

            /*$('#add_more').show();
            $('#add_more').removeAttr("disabled");*/

            
            
        }
    });


    // To Preview Image
    function imageIsLoaded(e) {
        $('#previewimg' + abc).attr('src', e.target.result);
    };

    $('#upload').click(function(e) {
        var name = $(":attachment").val();
        if (!name) {
            alert("First Image Must Be Selected");
            e.preventDefault();
        }
    });


  $('form#comm').on('beforeSubmit', function () {

    /*** for TO email section ***/
    var to_flag = true;
    //console.log($('#receiver_email').val());
    var s = $('#receiver_email').val();
    var a = s.split(',')
    console.log(a)

    for (i = 0; i < a.length; i++) {
      if(a[i].indexOf('@') == -1){
         console.log(a[i]);         
         to_flag = false;
      }
    } //for

    if( to_flag == false ){
      alert('The address in the "To" field was not recognized. Please make sure that all addresses are properly formed.');
      return false;
    }

    /*** for Cc email section ***/
    var cc_flag = true;
    //console.log($('#cc_email_input').val());
    var s1 = $('#cc_email_input').val();

      if( s1 !== null && s1 !== '' ){

        var a1 = s1.split(',');    
        console.log('a1');
        console.log(a1)

        for (j = 0; j < a1.length; j++) {
          if(a1[j].indexOf('@') == -1){
             console.log(a1[j]);         
             cc_flag = false;
          }
        } //for
      }

      if( cc_flag == false ){
        alert('The address in the "Cc" field was not recognized. Please make sure that all addresses are properly formed.');
        return false;
      }


    /*** for Bcc email section ***/
    var bcc_flag = true;
    //console.log($('#cc_email_input').val());
    var s2 = $('#bcc_email_input').val();

      if( s2 !== null && s2 !== '' ){

        var a2 = s2.split(',');
        console.log(a2);

        for (k = 0; k < a2.length; k++) {
          if(a2[k].indexOf('@') == -1){
             console.log(a2[k]);         
             bcc_flag = false;
          }
        } //for      
      }

      if( bcc_flag == false ){
        alert('The address in the "Bcc" field was not recognized. Please make sure that all addresses are properly formed.');
        return false;
      }

    $('.loader_overlay').show();

    return true;

  });

  
});

</script>
