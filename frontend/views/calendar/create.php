<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\event */

$this->title = 'Create Event';
$this->params['breadcrumbs'][] = ['label' => 'Events', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-create">

    

    <?= $this->render('_formevent', [
        'model' => $model,
         'dataContact'=>$dataContact,
    ]) ?>

</div>
