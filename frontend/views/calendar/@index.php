<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use app\models\TblContacts;
use app\models\TblAppointmentType;
/* @var $this yii\web\View */
/* @var $searchModel app\models\eventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Events';
$this->params['breadcrumbs'][] = $this->title;
?>
  <link rel="stylesheet" href="<?php Yii::getAlias('@web')?>css/clndr.css">
<style>
    #modalevent .modal-dialog {
    max-width: 900px;
    height: auto;
  

}
    .cal1 .clndr .clndr-table tr .day .day-contents {
        box-sizing: border-box;
        padding: 8px;!important
        font-size: 12px;!important
        text-align: center;!important
        line-height: 70px;!important
    }
    .cal1 .clndr .clndr-table tr .day {
        width: 100%;!important
        height: inherit;!important
    }
    .cal1 .clndr .clndr-table tr .empty, .cal1 .clndr .clndr-table tr .adjacent-month, .cal1 .clndr .clndr-table tr .my-empty, .cal1 .clndr .clndr-table tr .my-adjacent-month {
        width: 100%;!important
        height: inherit;!important
        background: #eee;!important
    }
    .cal1 .clndr .clndr-table tr {
        height: 45px;!important
    }
    td.day.today { 
        background-color: #fff !important;
    }
 .cal1 {
    width: 100%;
    padding-top: 10px;
    position: relative;
    background: #fff;
    margin-top: 60px;
    padding: 10px 0px 0;
    border-radius: 7px;
}
    td.day.today div{
        width: 70%;
        margin: 7px auto;
        border: 1px solid #000;
        border-radius: 28px;
        height: 30px;
        line-height: 13px;
        background-color: #9AD6E3;
    }
    #modalevent .modal-header {
        text-align: left !important;
        float: left;
        width: 100%;
        display: inline-block;
        color: #1dafed;
        /* font-family: "Open Sans"; */
    }
    .quote_left_fix {
    float: left;
    background: #f9f9f9;
    padding: 20px;
    box-sizing: border-box;
    box-shadow: 3px 0px 6px #eeeeee;
    -webkit-box-shadow: 3px 0px 6px #eeeeee;
    -moz-box-shadow: 3px 0px 6px #eeeeee;
    width: 30%;
    min-height: 660px;
    position: relative;
}
.quote_right_sec {
    float: right;
    width: 70%;
    padding: 60px 20px 20px 20px;
}
.fc-toolbar .fc-left {
    float: left;
    position: absolute;
    left: 35px;
    width: 100%;
    top: 20px;
}
.fc-toolbar .fc-left .btn-group {
    float: left;
    width: 293px;
}
.fc-toolbar .fc-left .btn.btn-default {
    border: 1px solid #1dafed;
    width: 33.3%;
    float: left;
    color: #1dafed !important;
    background: #fff;
    box-shadow: none;
}
    
.fc-toolbar .fc-left .btn-group .btn.btn-default.active {
    background: #1dafed;
    color: #fff  !important;
}
.cal1 .clndr .clndr-table tr .empty, .cal1 .clndr .clndr-table tr .adjacent-month, .cal1 .clndr .clndr-table tr .my-empty, .cal1 .clndr .clndr-table tr .my-adjacent-month {
    border-left: 0px solid #000000;
    border-top: 0px solid #000000;
    width: 100%;
    height: inherit;
    background: #fff;
    color: #d4d4d4;
}
.cal1 .clndr .clndr-table tr .day {
    border-left: 0px solid #000000;
    border-top: 0px solid #000000;
    width: 100%;
    height: inherit;
}
.cal1 .clndr .clndr-table .header-days .header-day {
    vertical-align: middle;
    text-align: center;
    border-left: 0px solid #000000;
    border-top: 0px solid #000000;
    color: #fff;
    background: #fff;
    color: #1dafed;
    font-weight: bold;
}
.cal1 .clndr .clndr-table tr .day:last-child {
    border-right: 0px solid #000000;
}
.cal1 .clndr .clndr-table .header-days .header-day:last-child {
    border-right: 0px solid #000000;
}
.cal1 .clndr .clndr-table tr:last-child .day, .cal1 .clndr .clndr-table tr:last-child .my-day {
    border-bottom: 0px solid #000000;
}
.cal1 .clndr .clndr-table tr .empty:last-child, .cal1 .clndr .clndr-table tr .adjacent-month:last-child, .cal1 .clndr .clndr-table tr .my-empty:last-child, .cal1 .clndr .clndr-table tr .my-adjacent-month:last-child {
    border-right: 0px solid #000000;
}
.cal1 .clndr .clndr-table tr .day.event, .cal1 .clndr .clndr-table tr .day.my-event {
    background: #fff;
    position: relative;
}
.cal1 .clndr .clndr-table tr .day .day-contents {
    box-sizing: border-box;
    padding: 8px;
    font-size: 12px;
    text-align: center;
}
td.day.past.event .day-contents::after {
    content: "";
    background: #1dafed;
    width: 4px;
    height: 4px;
    position: absolute;
    bottom: 10px;
    left: 50%;
    border-radius: 10px;
    margin-left: -2px;
}
.cal1 .clndr .clndr-controls .clndr-control-button {
    float: left;
    width: 10%;
}
.cal1 .clndr .clndr-controls .clndr-control-button.rightalign {
    text-align: center;
    width: 10%;
    float: right;
}

td.day.today.calendar-day-2018-08-31.calendar-dow-5 .day-contents {
    background: #1dafed;
    border: 0;
    color: #fff;
}
.cal1 .clndr .clndr-controls .month {
    float: left;
    width: 80%;
    text-align: center;
}
.cal1 .clndr .clndr-table tr .day.event:hover, .cal1 .clndr .clndr-table tr .day.my-event:hover {
    background: #d8d8d8;
    color: #fff;
}
button.fc-month-button.btn.btn-default {
    border-top-left-radius: 4px;
    border-bottom-left-radius: 4px;
}
button.fc-agendaDay-button.btn.btn-default {
    border-top-right-radius: 4px;
    border-bottom-right-radius: 4px;
}
.CalenderMainPage .quote_right_sec {
    padding-top: 0;
}
.CalenderMainPage .quote_right_sec p {
    margin: 0;
    padding: 0;
    border: 0;
}
.CalenderMainPage .quote_right_sec p button#modalEventButton {
    position: absolute;
    top: -47px;
    right: 70px;
    background: transparent;
    border: 0;
    padding: 0;
    color: #000;
    font-size: 14px;
    line-height: 25px;
}
.CalenderMainSearch {
    float: left;
    width: 100%;
    background: #fff;
    border-radius: 4px;
}
.CalenderMainSearch .form-group {
    float: left;
    width: 100%;
    margin: 0;
    border-bottom: 1px solid #f9f9f9;
    padding-bottom: 0;
}
.CalenderMainSearch .form-group input.SearchEvent {
    border: 0;
    float: left;
    width: 100%;
    padding: 6px 10px;
}
ul.SelectedEventlist {
    float: left;
    width: 100%;
    padding: 0;
    list-style: none;
    padding: 10px 10px;
    height: 190px;
    overflow-y: auto;
}
ul.SelectedEventlist li {
    float: left;
    width: 100%;
    border-bottom: 1px solid #e4e3e3;
    padding: 7px 5px 5px;
}
ul.SelectedEventlist li h3 {
    color: #1dafed;
    font-size: 14px;
    text-align: left;
    font-weight: 500;
    float: left;
    width: 100%;
}
ul.SelectedEventlist li span {
    float: left;
    width: 100%;
    font-size: 13px;
    text-align: left;
}
ul.SelectedEventlist li:last-child {
    border-bottom: 0;
}
.fullcalendar .fc-row.panel-default th {
    border: 0 !important;
}
.fullcalendar .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
    border-bottom-width: 0px;
} 
.fullcalendar .table-bordered {
    border: 0px solid #eceeef;
}
.fullcalendar  td.fc-head-container {
    border: 0 !important;
}
.fc-row.panel-default {
    border-right: 0px !important;
}
td.fc-sun, td.fc-sat {
    background: #f5f4f4;
}
.fc .fc-row .fc-content-skeleton table, .fc .fc-row .fc-content-skeleton td, .fc .fc-row .fc-helper-skeleton td {
    background: 0 0;
    border-color: transparent;
    padding: 0 10px 0;
}
td.fc-day.fc-fri.fc-today.alert.alert-info , .alert-info{
    background: #fff;
}
.fc-today span.fc-day-number {
    background: #fd4141;
    width: 32px;
    height: 32px;
    margin-top: 10px;
    text-align: center;
    color: #fff;
    padding: 0;
    border-radius: 30px;
    line-height: 31px;
    font-size: 14px;
}

    </style>
<?php
 $calenderEvents=[];
 foreach($appointment as $single)
 {
    $calenderEvents[date('Y-m-d',$single->date)]=date('Y-m-d',$single->date);
}
?>
<div class="container">
    <div class="CalenderMainPage">
      <div class="quote_left_fix text-center">
        <div class="cal1"></div>
        <div class="CalenderMainSearch">
          <div class="form-group">
            <input type="text" placeholder="Search" class="SearchEvent" id="search">
          </div>
          <div id="searchNote">
          <ul class="SelectedEventlist">
            <?php
          
                foreach($appointment as $value)
                {
                   // date_default_timezone_set('Asia/Kolkata');
                    $date =$value->date;
                    $time=$value->duration;
                    $ftime=$date+$time;
                    $newDateTimeFormat=date('Y-m-d',$date);
                    $newDateformat = date('d-M',$date);
                    $newTimeformat=date('H:i', $date);
                    $duration=date('H:i',$ftime);
                   

                   // echo $newTimeformat;
                    echo  '<li class="tosearch" id="'.$newDateTimeFormat.'">';
                    echo "<h3>".$newDateformat."(".$newTimeformat."- ".$duration.")</h3>";
                    $contact=$value->contact_id;
                    $contactName=TblContacts::find()->select('name')->where(['contact_id'=>$value->contact_id])->all();
                        foreach( $contactName as $name)
                        {
                            
                            echo '<span class="contactName" id="'.$value->contact_id.'">'.$name->name.'</span>';

                        }
                    $appointmentType=TblAppointmentType::find()->select('name')->where(['id'=>$value->type])->all();
                        foreach($appointmentType as $type)
                        {
                            echo '<span>'.$type->name.'</span>';
                        }   
                    echo "</li>";                 
               }
            ?>
            
          </ul>
      </div>
        </div><!--Calendermain search -->
      </div><!-- quote_left_fix text-center -->
    <div class="quote_right_sec">
    <div class="event-index">
    <?php Pjax::begin(['timeout' => 5000,'id'=>'eventpjax']); ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
     <?php

           Modal::begin([
              'header'=>'Create Event',
              'id'=>'modalevent',
              'size'=>'modal-sl',

            ]);
              echo "<div id='modalEvent'></div>";
              Modal::end();
         ?>
     
       
        <?php //echo $this->render('/site/event/_searchevent', ['model' => $searchModel]); ?>

        <p>
           <?php 
            echo  Html::button('<img src ="/paintpad/frontend/add_quote.png">', ['value'=>Url::to( Url::base().'/calender/create'),'class'=>'btn btn-primary pull-right' ,'id'=>'modalEventButton']);?>
        </p>

        <?= \yii2fullcalendar\yii2fullcalendar::widget(array(
          //'events'=> $event,
            'events' => Url::to(['/calender/events']),
            'header'=>['right'=>'prev,next,today','left'=>'month,agendaWeek,agendaDay']
      ));?>
       
    </div>
  </div>
  </div><!-- calenderMainPage-->
</div>

<script>
     $(function()
{
  //
  $('#modalEventButton').click(function(){
    $('#modalevent').modal('show')
      .find('#modalEvent')
      .load ($(this).attr('value'));
  });
});


$("#modalEventButton").click(function(){
    $("#modalevent").removeAttr("tabindex");
});


$("#search").on("keyup", function(){
    $('.noAppointment').remove();
    var searchClient = $(this).val();
    //console.log(searchClient);
    searchClient = searchClient.toLowerCase();
    if(searchClient != ""){
        $('.tosearch').show();
        var flag=0;
        $(".contactName").each(function(){
            var thisHtml = $(this).html();
            thisHtml = thisHtml.toLowerCase();
            //console.log(thisHtml);
            if(thisHtml.indexOf(searchClient) < 0 ){
                $(this).parent().hide();
            }
            else
            {
                flag=1;
            }

        });
        if(flag ==0)
        {
            var fieldHtml="<li class='alert alert-danger noAppointment'>NO Appointments</li>";
            $('.SelectedEventlist').append(fieldHtml);
        }
    }
    else
    {
      $('.tosearch').show();
    }
});

    </script>
<script>
    var date = <?php echo json_encode($calenderEvents);?>;
    //  console.log(date);
    var fieldhtml= "<span>rk</span>";
     $.each(date, function(key, value){
    //And diplay it in the result div
    console.log("calendar-day-"+key)
        $(".calendar-day-"+key).append(fieldhtml);
       
    });

        // /console.log(date);


        //var classKeys =  Object.keys(date);


        // console.log('.calendar-day-'+k)
        // var currentDateEvent = $('table.clndr-table').find('tr td.calendar-day-'+k);
        // var classElem = currentDateEvent.attr('class');
        // console.log(classElem);
        // $("."+classElem).append(".");
        //  $("."+classElem).each(function ()
        //  {
        //         $("."+classElem).append(".");
        //  });
        // if(currentDateEvent.length > 0) {
        //     console.log(currentDateEvent.length);
        //     var currentAttr = currentDateEvent.attr('class');
        //     console.log("currentAttr------->", currentAttr);    
        // }
                    
                
               
     

</script>

<?php 
    //$this->registerJsFile(Yii::getAlias('@web').'/frontend/web/js/jquery.js',['depends' => [yii\web\JqueryAsset::className()]]);
    $this->registerJsFile(Yii::getAlias('@web').'/frontend/web/js/underscore.js',['depends' => [yii\web\JqueryAsset::className()]]);
    $this->registerJsFile(Yii::getAlias('@web').'/frontend/web/js/moment.js',['depends' => [yii\web\JqueryAsset::className()]]);
    $this->registerJsFile(Yii::getAlias('@web').'/frontend/web/js/clndr.js',['depends' => [yii\web\JqueryAsset::className()]]);
    $this->registerJsFile(Yii::getAlias('@web').'/frontend/web/js/demo.js',['depends' => [yii\web\JqueryAsset::className()]]);  
?>
<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
<script src="https://npmcdn.com/bootstrap@4.0.0-alpha.5/dist/js/bootstrap.min.js"></script>

<?php Pjax::end();?>
