<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\TblAppointmentType;
use app\models\TblMembers;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;

?>



<?php

$dat=[];
$dataSub=[];
foreach($dataContact as $data)
{
  $dat[$data->contact_id]=$data->name;
  $datSub[$data->contact_id]=$data->subscriber_id;
}

?>

<?php $form = ActiveForm::begin(['id' => 'myCalender','options'=>['onsubmit'=>'return validateData()']]); ?>
<div class="event-form">
  <div class="NewEventForm">
     <div class="newEventLft_Form">
       <div class="form-group">
         <input type="hidden" class="form-control eventform_fields" name="TblAppointments[subscriber_id]" Placeholder="Customer Name" id="subs"> 
         <?php  echo $form->field($model,"contact_id")->widget(Select2::classname(), [
                  'data' => $dat,                  
                  'options' => ['placeholder' =>'Customer Name','tabindex' => false,'class'=>'form-control eventform_fields','id'=>'userDataSelect'],
                  'pluginOptions' => [
                  'allowClear' => true,
                  ],         
              ]);
          ?>
        </div><!-- form-control -->
        <div class="form-group">
            <div class="EventSelectBtns AppointSelect">
              <?= $form->field($model,'type')->dropDownList(ArrayHelper::map(TblAppointmentType::find()->all(),'id','name'),['prompt'=>'Appointment Type','class'=>'appointmentType','id'=>'appointmentTypeID'])->label(false) ?>
              
            </div><!-- appointselect -->
            <div class="EventSelectBtns teamMember">
            
               <?= $form->field($model,'member_id')->dropDownList(ArrayHelper::map(TblMembers::find()->all(),'id',
                function($model2) {
                return $model2['f_name'].' '.$model2['l_name'];
                   }
              ),['prompt'=>'Team Member','class'=>'teamMemberSelect','id'=>'memberID'])->label(false) ?>
            </div><!-- appointselect -->
        </div><!-- form-Group -->
        <div class="form-group">
          <div class="evnetFormLft_Calender">
               
                    <input type="hidden" name="calender" id="getDate" >
                <div id="inlineDatepicker1" class="cal2"></div>
          </div><!-- evnetFormLft_Calender -->

          <div class="EventSelectBtns timeSelect">
            <div class="form-group">
          
             
              <input type="text" name="dateSelect[From]" id="timepicker1" class="TimeSelect" placeholder="Time From">

            </div><!-- form-group -->
            <div class="form-group">
            
              
               <input type="text" name="dateSelect[to]" id="timepicker2" class="TimeSelect" placeholder="Time To">
            </div><!-- form-group -->
            <div class="form-group">
              <label>All Day</label>
              <div class="RadioMainDiv">
                 <?php $model->allDay = '0';?>
                 <?= $form->field($model, 'allDay')->radioList(array('1'=>'YES','0'=>'NO'))->label(false); ?>
              </div><!-- Radiomaindiv -->
            </div><!-- form-group -->
          </div><!-- EventSelectBtns -->
        </div><!-- form-group -->
        <div class="form-group">
        
         <?= $form->field($model, 'note')->textarea(['rows' => '6','class'=>'Notefield','placeholder'=>'NOTES','id'=>'noteId' ])->label(false) ?>
        </div><!-- form-group -->
      </div><!-- mewEventlft_form -->
      <div class="newEvent_RhtForm">
        <div class="form-group">
           <?= $form->field($model, 'formatted_addr')->textInput(['class'=>'form-control eventform_fields location','placeholder'=>'LOCATIONS','id'=>'pac-input'])->label(false) ?>
           
        </div><!-- form-control -->
        <div class="Map_EventDiv" id="map">
        </div><!-- map_eventDiv -->
      </div><!-- newEvent_RhtForm -->
  </div><!-- neweventform -->
</div>
<div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success','id'=>'closeproduct','class'=>'btn btn-success pull-right']) ?>
    </div>
<?php ActiveForm::end(); ?> 
<?php
  $this->registerJs('
      var subsArray = '.json_encode($datSub).';
      console.log("subsArray-------->", subsArray);
      $(document).on("change","#userDataSelect", function() {
        console.log("userDataSelect------->", $("#userDataSelect").find(":selected").attr("value"));
        var selectedSubId = subsArray[$("#userDataSelect").find(":selected").attr("value")];
        console.log("selectedSubId------>", selectedSubId);
        $("#subs").val(selectedSubId)
      })
  ');
?>


<script>
  $(function() {
    $('#inlineDatepicker').datepick({onSelect: showDate,dateFormat: 'DD, MM d, yyyy'});
  });

  function showDate(date) {
    var dateObj = new Date(date)
    var month = dateObj.getUTCMonth() + 1; //months from 1-12
    var day = dateObj.getUTCDate();
    var year = dateObj.getUTCFullYear();

   if (day.toString().length == 1) {
        day = "0" + day;
    }
    if (month.toString().length == 1) {
        month = "0" + month;
    }
    newdate = year + "-" + month + "-" + day;

    $("#getDate").val(newdate)
  }
</script>
<?php  Pjax::begin();
    $this->registerJsFile(Yii::getAlias('@web').'/frontend/web/js/timepicki.js',['depends' => [yii\web\JqueryAsset::className()]]);
    $this->registerJsFile(Yii::getAlias('@web').'/frontend/web/js/jquery.plugin.min.js',['depends' => [yii\web\JqueryAsset::className()]]);
    $this->registerJsFile(Yii::getAlias('@web').'/frontend/web/js/jquery.datepick.js',['depends' => [yii\web\JqueryAsset::className()]]);

    $this->registerCssFile(Yii::getAlias('@web').'/frontend/web/css/timepicki.css',['depends' => [yii\web\JqueryAsset::className()]]);
    $this->registerCssFile(Yii::getAlias('@web').'/frontend/web/css/clndr.css',['depends' => [yii\web\JqueryAsset::className()]]);
    $this->registerCssFile(Yii::getAlias('@web').'/frontend/web/css/jquery.datepick.css',['depends' => [yii\web\JqueryAsset::className()]]);

?>
<script>
$(document).ready(function(){
  $('#timepicker1').timepicki();
  $('#timepicker2').timepicki();
});  
</script>

<?php Pjax::end();?>

<script>
   $(function() {
    //$('#inlineDatepicker').datepick({onSelect: showDate,dateFormat: 'DD, MM d, yyyy'});
    // $(".day").click(function() {
    //   var date = $("#getDate").val();
    // }
    //console.log(<?php //echo $dateShow;?>);
    calendars.clndr3 = $('#inlineDatepicker1').clndr({
        daysOfTheWeek: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
       // events: [{"date":"2018-09-09"}],
        clickEvents: {
            click: function (target) {
                //console.log('Cal-1 clicked: ', target);
                var dateClicked = target.date._d;
              
                var dateObj = new Date(dateClicked)
                var month = dateObj.getMonth() + 1; //months from 1-12
                var day = dateObj.getDate();
                var year = dateObj.getFullYear();

                if (day.toString().length == 1) {
                  day = "0" + day;
                }
                if (month.toString().length == 1) {
                  month = "0" + month;
                }
                newdate = year + "-" + month + "-" + day;
                $('.day').removeClass('dateSelected');
                $(".calendar-day-"+newdate).addClass("dateSelected");
                console.log(newdate);
                $("#getDate").val(newdate);

            },
            today: function (target) {
                console.log('Cal-1 today');
            },
            nextMonth: function (target) {
                console.log('Cal-1 next month',target);
                //$("#w0").fullCalendar( 'gotoDate', target._d ) ;
            },
            previousMonth: function (target) {
                console.log('Cal-1 previous month',target);
                 //$("#w0").fullCalendar( 'gotoDate', target._d ) ;
            },
            onMonthChange: function () {
                console.log('Cal-1 month changed');
            },
            nextYear: function () {
                console.log('Cal-1 next year');
            },
            previousYear: function () {
                console.log('Cal-1 previous year');
            },
            onYearChange: function () {
                console.log('Cal-1 year changed');
            },
            nextInterval: function () {
                console.log('Cal-1 next interval');
            },
            previousInterval: function () {
                console.log('Cal-1 previous interval');
            },
            onIntervalChange: function () {
                console.log('Cal-1 interval changed');
            }
        },
        multiDayEvents: {
            singleDay: 'date',
            endDate: 'endDate',
            startDate: 'startDate'
        },
        showAdjacentMonths: true,
        adjacentDaysChangeMonth: false
    });
    
  });

</script>
<script>
  function initAutocomplete() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -33.8688, lng: 151.2195},
          zoom: 13,
          mapTypeId: 'roadmap'
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
      }

    </script>

 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC2Qg3ywv9Dhg-Ptjv-5fG3I3UEt0rLufQ&libraries=places&callback=initAutocomplete"
         async defer></script>


 
  