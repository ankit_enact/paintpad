<?php
use app\models\TblAppointments;
use app\models\TblAppointmentType;

		if(count($event)>0)
		{
			echo "<ul class='SelectedEventlist'>";
			$eve=[];
			$contact_id=[];
			foreach($event as $event)
			{
				$eve[$event->contact_id]=$event->name;	
				//print_r($event->contact_id);
				$events=TblAppointments::find()->where(['contact_id'=>$event->contact_id])->all();
                foreach($events as $appointmentData)
                {
                	//print_r($appointmentData);
                	
                    $date =$appointmentData->date; 
                    $time=$appointmentData->duration;
                    $ftime=$date+$time;
                    $newDateformat = date('d-M',$date);
                    $newTimeformat=date('H:i', $date);
                    $duration=date('H:i',$ftime);
                 	$newDateTimeFormat=date('Y-m-d',$date);
                    echo  '<li id="tosearch" class="tosearch date'.$newDateTimeFormat.'">';
                    echo "<h3>".$newDateformat."(".$newTimeformat."- ".$duration.")</h3>";
                    echo '<span id="'.$event->contact_id.'">'.$event->name.'</span>';
                    $appointmentType=TblAppointmentType::find()->select('name')->where(['id'=>$appointmentData->type])->all();
                    foreach($appointmentType as $type)
                        {
                            echo '<span>'.$type->name.'</span>';
                        } 
                     echo "</li>"; 
                }			
			}
			echo "</ul>";
		}
		else
		{
			echo "<h3 class='alert-danger'>No Appointments</h3>";
		}

?>
<script>
	var event=<?php echo json_encode($ev);?>;
	$.each(event, function(key, value){
        $('.date'+value).click(function(){
            console.log(value);
            $("#w0").fullCalendar( 'gotoDate',value) ;
            var thisD = new Date(value);
            calendars.clndr1.setMonth(thisD.getMonth());
            calendars.clndr1.setYear(thisD.getFullYear());
        });
    });
	</script>