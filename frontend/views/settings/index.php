<?php
use app\models\TblAppointmentType;
use app\models\TblMembers;
use frontend\assets\ContactAsset;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
ContactAsset::register($this);
// use frontend\assets\DashboardAsset;
// DashboardAsset::register($this);
use yii\widgets\ActiveForm;
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->

<?php
$this->registerCssFile(Yii::getAlias('@web') . '/frontend/web/css/style.css', ['depends' => [yii\web\JqueryAsset::className()]]);
$this->registerCssFile(Yii::getAlias('@web') . '/frontend/web/css/bootstrap-select.min.css', ['depends' => [yii\web\JqueryAsset::className()]]);
$this->title = "PaintPad||Settings";

$subscriber_id = Yii::$app->user->id;
$company_id = Yii::$app->user->identity->company_id;

?>

<style type="text/css">
  #myModal-adddoc-edit .modal-body, #myModal-prep-level-edit .modal-body, #myModal-special-item-update .modal-body{
    height: 200px;
    overflow: scroll;
  }
  .wrap .wrap .container{
    height: 400px;
    overflow: scroll;
  }
</style>

<form id="loggedData">
  <input type="hidden" value="<?= $subscriber_id ?>" id="subscriber_id" name="subscriber_id">
  <input type="hidden" value="<?= $company_id ?>" id="company_id" name="company_id">
</form>

 <div class="wrap">
    <div class="container">
      <div class="quote_detail_inner">
        <div class="tab-content card">
          <div class="tab-pane fade in show active" id="detail" role="tabpanel">
            <div class="quote_left_fix text-center">
              <div class="settings-left ">
                <div class="left-rows">
                  <div class="profile-div-main">
                    <span class="profile-img"><img src="image/avtar.png" alt="profile"></span>
                    <span class="edit-icon"><img src="image/edit_profile.png" alt="edit"></span>
                  </div><!-- profile-image -->
                  <span class="profile-name" id="company_name"></span>
                </div><!-- left-rows -->
                <div class="left-rows">
                  <div class="profile-info">
                    <a href="javascript:void(0);" id="company_email"></a>
                    <a href="javascript:void(0);" id="company_site"></a>
                    <a href="javascript:void(0);" id="company_phone"></a>
                  </div><!-- profile-info -->
                </div><!-- left-rows -->
                <div class="left-rows">
                  <div class="profile-info address">
                    <p id="company_address">
                      
                    </p>
                    <ul class="personal_count">
                      <li>
                        <h4 class="contactsIcon"></h4><h4>
                        <span><img src="image/mail.png"></span>
                      </h4></li>
                      <li>
                        <h4 class="docsIcon"></h4><h4>
                        <span><img src="image/people_subscribe@2x.png"></span>
                      </h4></li>
                      <li>
                        <h4 class="quotesIcon"></h4><h4>
                        <span><img src="image/Quote_icon.png"></span>
                      </h4></li>
                      <li>
                        <h4 class="jssIcon"></h4><h4>
                        <span><img src="image/Invoices_icon.png"></span>
                      </h4>
                      </li>
                    </ul><!-- personal_count -->
                  </div><!-- profile-info -->
                </div><!-- left-rows -->
              </div><!-- settings-left -->
              </div><!-- quote_left_fix text-center -->
              <div class="quote_right_sec quotePage_rhtSec">
                <div class="setting-page-main ">
                  <div class="settings-collapse">
                    <div class="accordion" id="accordionExample">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h2 class="mb-0">
                                    <span class="card-heading">Settings </span>  
                                    <a href="#" id="edit_company" data-toggle="modal" data-target="#myModal-settings"><img src="image/edit.png" alt="edit"></a>
                                    <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapseOne">
                                       <i class="fa fa-plus"></i>
                                     </button>                  
                                </h2>
                            </div>
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body" id="settingsMain">
                                    <!-- setting-lists -->                                
                                  </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h2 class="mb-0">
                                    <span class="card-heading">Library Documents </span>  
                                    <a href="#" data-toggle="modal" data-edit="0" class="library-doc-modal" data-always_link="" data-target="#myModal-adddoc-edit" data-file_id="0" data-actual_name=""  data-desc="" data-url="" data-file_name="" data-uploaded_by=""><img src="image/add_pay.png" alt="edit"></a>
                                    <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapseTwo">
                                       <i class="fa fa-plus"></i>
                                     </button>                  
                                </h2>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                <div class="card-body">
                                    <table class="invoice_table table table-borderless table-condensed table-hover">
                                      <thead class="invoicehead" id="pagevalAllinvoices" data-value="0">
                                          <tr>
                                            <th>ID</th>
                                            <th>File Name</th>
                                            <th>Description</th>
                                            <th>Uploaded By</th>
                                            <th>Uploaded Date</th>
                                          </tr>
                                      </thead>
                                      <tbody class="invoiceBody setting-body" id="library-doc" style="">
                                         <!--  Document Library Content Here -->
                                      </tbody>
                                  </table>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingThree">
                                <h2 class="mb-0">
                                    <span class="card-heading">Prep. Levels</span>  

                                    <a href="javascript:void(0);" data-edit="0" data-toggle="modal" data-target="#myModal-prep-level-edit" data-uplift_cost="" data-uplift_time="" data-id="0" data-is_default="" class="prep-level-modal"><img src="image/add_pay.png" alt="edit"></a>
                                    <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapseThree">
                                       <i class="fa fa-plus"></i>
                                     </button>                                                      
                                </h2>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                <div class="card-body">
                                    <table class="invoice_table table special-item-table  table-borderless table-condensed table-hover">
                                      <thead class="invoicehead"  data-value="0">
                                          <tr>
                                            <th>Name</th>
                                            <th>% Uplift to paint cost</th>
                                            <th>% Uplift to paint time</th>
                                          </tr>
                                      </thead>
                                      <tbody class="invoiceBody setting-body" id="prepLevelList" style="">
                                        <!-- data of prep level -->
                                      </tbody>
                                  </table>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingFour">
                                <h2 class="mb-0">
                                    <span class="card-heading">Special Items</span>  
                                    <a href="#" data-toggle="modal" data-target="#myModal-special-item "><img src="image/add_pay.png" alt="edit"></a>
                                    <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapseFour">
                                       <i class="fa fa-plus"></i>
                                     </button>                  
                                </h2>
                            </div>
                            <div id="collapseFour" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                <div class="card-body">
                                    <table id="Special-items-table" class="invoice_table table special-item-table  table-borderless table-condensed table-hover">
                                      <thead class="invoicehead"  data-value="0">
                                          <tr>
                                            <th>Name</th>
                                            <th>Price (Exc. GST)</th>
                                          </tr>
                                      </thead>
                                      <tbody class="invoiceBody setting-body" id="special-item-list" style="">
                                        <!-- Special Items Data Here -->
                                      </tbody>
                                  </table>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingFive">
                                <h2 class="mb-0">
                                    <span class="card-heading">Quote Email Settings</span>  
                                    <a href="#" data-toggle="modal" data-target="#myModal-quote-email"><img src="image/edit.png" alt="edit"></a>
                                    
                                    <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapseFive">
                                       <i class="fa fa-plus"></i>
                                     </button>                  
                                </h2>
                            </div>
                            <div id="collapseFive" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body">
                                    <ul class="setting-lists quote-email-setting" id="QES-setting">
                                      <li>
                                        <p>Server Email Address</p>
                                        <span id="QES-serverEmail"></span>
                                      </li>
                                      <li>
                                        <p>Quote Email Subject</p>
                                        <span id="QES-mailSubject"></span>
                                      </li>
                                      <li>
                                        <p>BCC to</p>
                                        <span id="QES-mailBcc"></span>
                                      </li>
                                      <li>
                                        <p>Attached Documents</p>
                                        <span  id="QES-mailAttachments"></span>
                                      </li>
                                    </ul><!-- setting-lists -->
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingSix">
                                <h2 class="mb-0">
                                    <span class="card-heading">JSS Email Settings</span>  
                                    <a href="#" data-toggle="modal" data-target="#myModal-jss-email"><img src="image/edit.png" alt="edit"></a>
                                    
                                    <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapseSix">
                                       <i class="fa fa-plus"></i>
                                     </button>                  
                                </h2>
                            </div>
                            <div id="collapseSix" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body">
                                    <ul class="setting-lists quote-email-setting"  id="JES-setting">
                                      <li>
                                        <p>Server Email Address</p>
                                        <span id="JES-serverEmail"></span>
                                      </li>
                                      <li>
                                        <p>JSS Email Subject</p>
                                        <span id="JES-mailSubject"></span>
                                      </li>
                                      <li>
                                        <p>BCC to</p>
                                        <span id="JES-mailBcc"></span>
                                      </li>
                                      <li>
                                        <p>Attached Documents</p>
                                        <span  id="JES-mailAttachments"></span>
                                      </li>
                                    </ul><!-- setting-lists -->
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingSeven">
                                <h2 class="mb-0">
                                    <span class="card-heading">Customers Sources</span>  
                                    <a href="#" data-toggle="modal" data-target="#myModal-customer-sources"><img src="image/add_pay.png" alt="edit"></a>
                                    <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapseSeven">
                                       <i class="fa fa-plus"></i>
                                     </button>                  
                                </h2>
                            </div>
                            <div id="collapseSeven" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                <div class="card-body">
                                    <table id="customer-table" class="invoice_table table special-item-table  table-borderless table-condensed table-hover">
                                      <thead class="invoicehead"  data-value="0">
                                          <tr>
                                            <th>Name</th>
                                          </tr>
                                      </thead>
                                      <tbody class="invoiceBody setting-body" id="customerSourceList" style="">
                                       
                                      </tbody>
                                  </table>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingEight">
                                 <h2 class="mb-0">
                                    <span class="card-heading">Payment Notes</span>  
                                    <a href="#" data-toggle="modal" data-target="#myModal-payment-notes"><img src="image/add_pay.png" alt="edit"></a>
                                    <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapseEight">
                                       <i class="fa fa-plus"></i>
                                     </button>                  
                                </h2>
                            </div>
                            <div id="collapseEight" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                <div class="card-body">
                                    <table id="customer-table" class="invoice_table table special-item-table  table-borderless table-condensed table-hover">
                                      <thead class="invoicehead"  data-value="0">
                                          <tr>
                                            <th>Name</th>
                                          </tr>
                                      </thead>
                                      <tbody class="invoiceBody setting-body" id="paymentNotesList" style="">
                                        <!-- Payment Notes Data here -->
                                      </tbody>
                                  </table>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingEight">
                                 <h2 class="mb-0">
                                    <span class="card-heading">Paint Stores</span>  
                                    <a href="#" data-toggle="modal" data-target="#myModal-payment-notes"><img src="image/add_pay.png" alt="edit"></a>
                                    <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapseNine">
                                       <i class="fa fa-plus"></i>
                                     </button>                  
                                </h2>
                            </div>
                            <div id="collapseNine" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                <div class="card-body">
                                    <table id="customer-table" class="invoice_table table special-item-table  table-borderless table-condensed table-hover">
                                      <thead class="invoicehead"  data-value="0">
                                          <tr>
                                            <th>Name</th>
                                          </tr>
                                      </thead>
                                      <tbody class="invoiceBody setting-body" id="paymentStoresList" style="">
                                        <!-- Payment Stores Data here -->
                                      </tbody>
                                  </table>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingEight">
                                 <h2 class="mb-0">
                                    <span class="card-heading">Paint Order Notes</span>  
                                    <a href="#" data-toggle="modal" data-target="#myModal-payment-notes"><img src="image/add_pay.png" alt="edit"></a>
                                    <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapseTen">
                                       <i class="fa fa-plus"></i>
                                     </button>                  
                                </h2>
                            </div>
                            <div id="collapseTen" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                <div class="card-body">
                                    <table id="customer-table" class="invoice_table table special-item-table  table-borderless table-condensed table-hover">
                                      <thead class="invoicehead"  data-value="0">
                                          <tr>
                                            <th>Name</th>
                                          </tr>
                                      </thead>
                                      <tbody class="invoiceBody setting-body" id="paymentOrderNotesList" style="">
                                        <!-- Payment Order Notes Data here -->
                                      </tbody>
                                  </table>
                                </div>
                            </div>
                        </div>
                    </div><!-- accordionExample -->
                </div><!-- accordionExample -->
                <div class="card-header last-card">
                    <h2 class="mb-0">
                        <button type="button" class="btn btn-link">
                           <span>Calender Sync</span>
                         </button>                  
                    </h2>
                    <div class="calender-sync-div">
                      <h4>Office 365</h4>
                      <div class="office-toogle">
                        <label class="switch">
                          <input type="checkbox">
                          <span class="slider"></span>
                        </label>
                      </div><!-- office-toogle -->
                    </div><!-- calender-sync-div -->
                </div>
                </div><!-- setting-page-main -->
              </div><!-- quote_right_sec quotePage_rhtSec -->
          </div><!--  tab-pane -->
        </div><!-- tab-content card -->
      </div><!-- quote_detail_inner -->
    </div><!-- container -->
  </div><!-- wrap -->


  <footer class="col-md-12"><div class="container"><p class="footer-logo"><img src="image/logo.png"></p></div></footer>


<!-- Modal Modal-settings -->
<div id="myModal-settings" class="modal fade setting-modals" role="dialog">
  <div class="modal-dialog">
<form id="companySettings">
  <input type="hidden" name="gst" value="0">
  <input type="hidden" name="users_settings_id" value="">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close modal-close" data-dismiss="modal">&times;</button>
        <button type="button" id="submitCompanySetting" class="close modal-tick" data-dismiss=""><img src="image/accepted@3x.png"></button>
        <h4 class="modal-title">Settings</h4>
      </div>
      <div class="modal-body" style="height: 450px; overflow: scroll;">
        <div class="setting-form ">
          <div class="form-group">
            <label>Application Cost</label>
            <p><input type="text" class="form-control input-box"  name="application_cost"><span class="input-right">/H</span></p>
          </div><!-- form-group -->
          <div class="form-group">
            <label>Profile Mark Up %</label>
            <p><input type="text" class="form-control input-box"  name="profit_markup"><span class="input-right">%</span></p>
          </div><!-- form-group -->
          <div class="form-group">
            <label>Gross Margin %</label>
            <p><input type="text" class="form-control input-box"  name="gross_margin_percent"><span class="input-right">%</span></p>
          </div><!-- form-group -->
          <div class="form-group">
            <label>Default Deposit Percent</label>
            <p><input type="text" class="form-control input-box"  name="default_deposit_percent"><span class="input-right">%</span></p>
          </div><!-- form-group -->
          <div class="form-group">
            <label>Terms & Conditions</label>
            <p>
              <select name="terms_condition" class="selectpicker1" style="width: 100%;">
              </select>
            </p>
          </div><!-- form-group -->
           <div class="form-group quickbook-div">
            <label>Quickbooks</label>
            <p>
              <button type="button" name="disconnect" value="Disconnect" class="btn disconnect">Disconnect</button>
            </p>
          </div><!-- form-group -->
          <div class="form-group">
            <label>Account Name</label>
            <p><input type="text" class="form-control input-box"  name="account_name"></p>
          </div><!-- form-group -->
          <div class="form-group">
            <label>Account BSB</label>
            <p><input type="text" class="form-control input-box"  name="account_BSB"></p>
          </div><!-- form-group -->
          <div class="form-group">
            <label>Account Number</label>
            <p><input type="text" class="form-control input-box"  name="account_number"></p>
          </div><!-- form-group -->
          <div class="form-group">
            <label>Payment Phone Number</label>
            <p><input type="text" class="form-control input-box"  name="payment_phone_number"></p>
          </div><!-- form-group -->
           <div class="form-group">
            <label>Percentage Increase</label>
            <p><input type="text" class="form-control input-box"  name="price_increase"><span class="input-right">%</span></p>
          </div><!-- form-group -->
          <div class="form-group">
            <label>Colour Consultant Item</label>
            <p>
              <select name="colour_consultant_item" class="selectpicker1" style="width: 100%">
              </select>
            </p>
          </div><!-- form-group -->
        </div><!-- setting-form -->
      </div>
    </div>
</form>
  </div>
</div>



<!-- Modal Modal-EDIT-ADD DOCUMENT -->
<div id="myModal-adddoc-edit" class="modal fade setting-modals" role="dialog">
  <div class="modal-dialog">

    <form method="POST" enctype="mulipart/form-data" id="addDcoForm">
    <!-- Modal content-->
      <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close modal-close" data-dismiss="modal">&times;</button>
            <button type="submit" id="libDocSubmit" class="close modal-tick"><img src="image/accepted@3x.png"></button>
            <h4 class="modal-title">Add Document</h4>
          </div>
          <div class="modal-body">
            <div class="setting-form ">
              <div class="form-group">
                <label>File Name</label>
                <p><input type="text" class="form-control input-box" placeholder="" value="" name="file_name"></p>
                <input type="hidden" value="" name="doc_id">
              </div><!-- form-group -->
              <div class="form-group">
                <label>File Description</label>
                <p><textarea  class="form-control input-box desc" value="" name="desc"></textarea></p>
              </div><!-- form-group -->
              <div class="form-group">
                <label>Always Link</label>
                <p>
                  <label class="always-link">Yes
                    <input type="radio" checked="checked" id="docRadio1" name="always_link" value="1">
                    <span class="checkmark"></span>
                  </label>
                  <label class="always-link">No
                    <input type="radio" name="always_link" id="docRadio0"  value="0">
                    <span class="checkmark"></span>
                  </label>
                  <ul class="image-preview-div">
                    <li class="child-priview-div" id="previewFileName">test_file_2</li>
                    <li class="child-priview-div"><a id="file_review" target="_blank" href="javascript:void(0);">Preview</a></li>
                  </ul><!-- image-preview-div -->
                </p>
              </div><!-- form-group -->
              <div class="form-group">
                <label>Upload File</label>
                <p>
                  <input type="file" name="image_0" id="image_0" class="" />
                </p>
              </div><!-- form-group -->
              <div class="form-group text-right" id="delDocDiv">
                
              </div><!-- form-group -->
            </div><!-- setting-form -->
          </div>
      </div>
    </form>

  </div>
</div>

<!-- Modal Modal-UPDATE PREP LEVEL -->
<div id="myModal-prep-level-edit" class="modal fade setting-modals" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close modal-close" data-dismiss="modal">&times;</button>
        <button type="button" class="close modal-tick" data-dismiss="modal"><img src="image/accepted@3x.png"></button>
        <h4 class="modal-title">Update Prep. Level</h4>
      </div>
      <div class="modal-body">
        <div class="setting-form ">
          <div class="form-group">
            <label>Prep. Level</label>
            <input type="hidden" name="id" value="0">
            <p><input type="text" class="form-control input-box" placeholder="" value="" name="name"></p>
          </div><!-- form-group -->
          <div class="form-group">
            <label>% upflit to paint cost</label>
            <p><input type="text" class="form-control input-box" placeholder="" value="" name="uplift_cost"><span class="input-right">%</span></p>
          </div><!-- form-group -->
          <div class="form-group">
            <label>% upflit to paint time</label>
            <p><input type="text" class="form-control input-box" placeholder="" value="" name="uplift_time"><span class="input-right">%</span></p>
          </div><!-- form-group -->
          <div class="form-group">
            <label>% upflit to paint time</label>
            <p>
              <label class="">
              <input type="checkbox"  name="is_default">
              <!-- <span class="checkmark"></span> -->
            </label>
            </p>
          </div><!-- form-group -->
        </div><!-- setting-form -->
      </div>
    </div>

  </div>
</div>


<!-- Modal Modal-SPECIAL ITEM -->
<div id="myModal-special-item" class="modal fade setting-modals" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close modal-close" data-dismiss="modal">&times;</button>
        <button type="button" class="close modal-tick" data-dismiss="modal"><img src="image/accepted@3x.png"></button>
        <h4 class="modal-title">Add Special Item</h4>
      </div>
      <div class="modal-body">
        <div class="setting-form ">
          <div class="form-group">
            <label>Name</label>
            <p><input type="text" class="form-control input-box" placeholder="" name="Name"></p>
          </div><!-- form-group -->
          <div class="form-group">
            <label>Price (Exc. GST)</label>
            <p><input type="text" class="form-control input-box" placeholder="" name="Price"><span class="input-right">$</span></p>
          </div><!-- form-group -->
        </div><!-- setting-form -->
      </div>
    </div>

  </div>
</div>
<!-- Modal Modal-SPECIAL ITEM UPDATE -->
<div id="myModal-special-item-update" class="modal fade setting-modals" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close modal-close" data-dismiss="modal">&times;</button>
        <button type="button" class="close modal-tick" data-dismiss="modal"><img src="image/accepted@3x.png"></button>
        <h4 class="modal-title">Update Special Item</h4>
      </div>
      <div class="modal-body">
        <div class="setting-form ">
          <div class="form-group">
            <label>Name</label>
            <p><input type="text" class="form-control input-box" placeholder="" value="Make sure you cover the floor tiles" name="Name"></p>
          </div><!-- form-group -->
          <div class="form-group">
            <label>Price (Exc. GST)</label>
            <p><input type="text" class="form-control input-box" placeholder="" value="15.75" name="Price"><span class="input-right">$</span></p>
          </div><!-- form-group -->
          <div class="form-group text-right">
            <button type="button" name="delete" class="btn dlt-btn"><i class="fa fa-trash-o"></i> Delete</button>
          </div><!-- form-group -->
        </div><!-- setting-form -->
      </div>
    </div>

  </div>
</div>

<!-- Modal Modal-Quote Email Settings -->
<div id="myModal-quote-email" class="modal fade setting-modals" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close modal-close" data-dismiss="modal">&times;</button>
        <button type="button" class="close modal-tick" data-dismiss="modal"><img src="image/accepted@3x.png"></button>
        <h4 class="modal-title">Quote Email Settings</h4>
      </div>
      <div class="modal-body">
        <div class="setting-form ">
          <div class="form-group">
            <label>BCC to</label>
            <p>
              <input type="text" class="form-control input-box" placeholder="" value="paintpadquote@gmail.com" name="BCC to">
              <small><i>Separate address with a comma (,)</i></small>
            </p>
          </div><!-- form-group -->
          <div class="form-group">
            <label>Email Subject</label>
            <p><input type="text" class="form-control input-box" placeholder="" value="15.75" name="Price"><span class="input-right">$</span></p>
          </div><!-- form-group -->
          <div class="form-group">
            <label>Attached Documents</label>
            <p>
              <select class="selectpicker">
                <option>-- Select Document --</option>
                <option>Clean up work site</option>
                <option>hello test</option>
              </select>
              <span class="doc-name">test 1</span>
              <span class="doc-name">test 1</span>
              <span class="doc-name">test 1</span>
              <span class="doc-name">test 1</span>
            </p>
          </div><!-- form-group -->
        </div><!-- setting-form -->
      </div>
    </div>

  </div>
</div>

<!-- Modal Modal-Quote Email Settings -->
<div id="myModal-jss-email" class="modal fade setting-modals" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close modal-close" data-dismiss="modal">&times;</button>
        <button type="button" class="close modal-tick" data-dismiss="modal"><img src="image/accepted@3x.png"></button>
        <h4 class="modal-title">JSS Email Settings</h4>
      </div>
      <div class="modal-body">
        <div class="setting-form ">
          <div class="form-group">
            <label>BCC to</label>
            <p>
              <input type="text" class="form-control input-box" placeholder="" value="paintpadquotejss1@gmail.com" name="BCC to">
              <small><i>Separate address with a comma (,)</i></small>
            </p>
          </div><!-- form-group -->
          <div class="form-group">
            <label>Email Subject</label>
            <p><input type="text" class="form-control input-box" placeholder="" value="15.75" name="Price"><span class="input-right">$</span></p>
          </div><!-- form-group -->
          <div class="form-group">
            <label>Attached Documents</label>
            <p>
              <select class="selectpicker">
                <option>-- Select Document --</option>
                <option>Clean up work site</option>
                <option>hello test</option>
              </select>
              <span class="doc-name">test 1</span>
              <span class="doc-name">test 1</span>
              <span class="doc-name">test 1</span>
              <span class="doc-name">test 1</span>
            </p>
          </div><!-- form-group -->
        </div><!-- setting-form -->
      </div>
    </div>

  </div>
</div>



<!-- Modal Modal-Customer Sources -->
<div id="myModal-customer-sources" class="modal fade setting-modals" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close modal-close" data-dismiss="modal">&times;</button>
        <button type="button" class="close modal-tick" data-dismiss="modal"><img src="image/accepted@3x.png"></button>
        <h4 class="modal-title">Add Customer Soruce</h4>
      </div>
      <div class="modal-body">
        <div class="setting-form ">
          <div class="form-group">
            <label>Name</label>
            <p>
              <input type="text" class="form-control input-box" placeholder="" value="" name="Name">
            </p>
          </div><!-- form-group -->
        </div><!-- setting-form -->
      </div>
    </div>

  </div>
</div>

<!-- Modal Modal-Customer Sources -->
<div id="myModal-customer-sources-update" class="modal fade setting-modals" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close modal-close" data-dismiss="modal">&times;</button>
        <button type="button" class="close modal-tick" data-dismiss="modal"><img src="image/accepted@3x.png"></button>
        <h4 class="modal-title">Update Customer Soruce</h4>
      </div>
      <div class="modal-body">
        <div class="setting-form ">
          <div class="form-group">
            <label>Name</label>
            <p>
              <input type="text" class="form-control input-box" placeholder="" value="" name="Name">
            </p>
          </div><!-- form-group -->
          <div class="form-group text-right">
            <button type="button" name="delete" class="btn dlt-btn"><i class="fa fa-trash-o"></i> Delete</button>
          </div><!-- form-group -->
        </div><!-- setting-form -->
      </div>
    </div>

  </div>
</div>

<!-- Modal Modal-Customer Sources -->
<div id="myModal-payment-notes" class="modal fade setting-modals" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close modal-close" data-dismiss="modal">&times;</button>
        <button type="button" class="close modal-tick" data-dismiss="modal"><img src="image/accepted@3x.png"></button>
        <h4 class="modal-title">Add Payment Notes</h4>
      </div>
      <div class="modal-body">
        <div class="setting-form ">
          <div class="form-group">
            <label>Name</label>
            <p>
              <input type="text" class="form-control input-box" placeholder="" value="" name="Name">
            </p>
          </div><!-- form-group -->
        </div><!-- setting-form -->
      </div>
    </div>

  </div>
</div>

<!-- Modal Modal-Customer Sources -->
<div id="myModal-payment-notes-update" class="modal fade setting-modals" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close modal-close" data-dismiss="modal">&times;</button>
        <button type="button" class="close modal-tick" data-dismiss="modal"><img src="image/accepted@3x.png"></button>
        <h4 class="modal-title">Update Payment Notes</h4>
      </div>
      <div class="modal-body">
        <div class="setting-form ">
          <div class="form-group">
            <label>Name</label>
            <p>
              <input type="text" class="form-control input-box" placeholder="" value="" name="Name">
            </p>
          </div><!-- form-group -->
          <div class="form-group text-right">
            <button type="button" name="delete" class="btn dlt-btn"><i class="fa fa-trash-o"></i> Delete</button>
          </div><!-- form-group -->
        </div><!-- setting-form -->
      </div>
    </div>

  </div>
</div>



  <!-- <script src="js/jquery.js"></script> -->
  <!-- <script src="js/yii.js"></script> -->
  <script src="js/map-function.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script src="js/bootstrap-select.min.js"></script>
  <script src="js/jquery-ui.min.js"></script>
  <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inview/1.0.0/jquery.inview.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inview/1.0.0/jquery.inview.min.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script src="js/custom.js"></script>
  <script src="js/paintpad.js"></script>
  <script src="js/tag-it.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  <script src="http://enacteservices.net/paintpad/js/paintpad.js" id="main-js"></script>
  <!-- <script src="js/custom-file-input.js"></script> -->

  
 <script>
    $(document).ready(function(){
      initPage();
    });


    function initPage(){
      var subscriber_id = $('#subscriber_id').val();
      var company_id = $('#company_id').val();

      var data = '{"company_id":"'+company_id+'", "subscriber_id" : '+subscriber_id+'}';
    $.ajax({
      url: siteBaseUrl+'/webservice/all-company-data',
      type: 'post',
      data: {'json':data},
      success: function (response) {
        var response = JSON.parse(response);
        console.log(response);
        // exit();


        if(response.success == 1){
          // Company Detail
          $("#company_name").text(response.data.compDetail.name);
          $("#company_email").text(response.data.compDetail.email);
          $("#company_site").text(response.data.compDetail.website);
          $("#company_phone").text(response.data.compDetail.phone);
          $("#company_address").html(response.data.compDetail.address+' <br> '+response.data.compDetail.suburb + response.data.compDetail.suburb+' <br> '+response.data.compDetail.country);

          // Company Settings
          var application_cost = (response.data.companySetting.application_cost != "") ? response.data.companySetting.application_cost+"/H" : "-";
          var gross_margin_percent = (response.data.companySetting.gross_margin_percent != "") ? response.data.companySetting.gross_margin_percent : "0";
          var gst = (response.data.companySetting.gst != "") ? response.data.companySetting.gst : "0";
          var users_settings_id = (response.data.companySetting.users_settings_id != "") ? response.data.companySetting.users_settings_id : "";
          // var tNc = (response.data.doc.file_name != "") ? response.data.doc.file_name : "-";
          var tNc =  "-";
          var dataForModel = '<option value="0">--- Select Document ---</option>';
          if(response.data.companySetting.docList != ""){
            $.each(response.data.docList, function(v,i){
              if(i['file_id'] == response.data.companySetting.terms_condition){
                tNc = i['file_name'];
                dataForModel += '<option selected value="'+i['file_id']+'">'+i['file_name']+'</option>'
              }else{
                dataForModel += '<option value="'+i['file_id']+'">'+i['file_name']+'</option>'
              }
            });
            $("#myModal-settings [name='terms_condition']").html(dataForModel);
          }else{
            tNc = '-';
          }
          var account_name = (response.data.companySetting.account_name != "") ? response.data.companySetting.account_name : "-";
          var account_number = (response.data.companySetting.account_number != "") ? response.data.companySetting.account_number: "-";
          var price_increase = (response.data.companySetting.price_increase != "") ? response.data.companySetting.price_increase: "0";
          //color cunsult item code here
          var colorCunsultCode =  "-";
          var colorCunsultItems =  "";
          if(response.data.specialItems != ""){
            $.each(response.data.specialItems, function(v,i){
              if(i['id'] == response.data.companySetting.colour_consultant_item){
                colorCunsultCode = i['name'];
                colorCunsultItems += '<option selected value="'+i['id']+'">'+i['name']+'</option>';
              }else{
                colorCunsultItems += '<option value="'+i['id']+'">'+i['name']+'</option>';
              }
            });
            $("#myModal-settings [name='colour_consultant_item']").html(colorCunsultItems);
          }else{
            colorCunsultCode = '-';
          }
          var profit_markup = (response.data.companySetting.profit_markup != "") ? response.data.companySetting.profit_markup: "0";
          var default_deposit_percent = (response.data.companySetting.default_deposit_percent != "") ? response.data.companySetting.default_deposit_percent: "0";
          var account_BSB = (response.data.companySetting.account_BSB != "") ? response.data.companySetting.account_BSB: "-";
          var payment_phone_number = (response.data.companySetting.payment_phone_number != "") ? response.data.companySetting.payment_phone_number: "-";

          var str = '<ul class="setting-lists">';
          str += '<li><p>Application Cost</p><span id="application_cost" data-users_settings_id="'+users_settings_id+'" data-gst="'+gst+'" data-val="'+ application_cost +'">'+ application_cost +'</span></li>';
          str += '<li><p>Gross Margin %</p><span id="gross_margin_percent" data-val="'+ gross_margin_percent +'">'+ gross_margin_percent +'%</span></li>';
          str += '<li><p>Terms & Conditions Doc</p><span id="tNc" data-val="'+ tNc +'">'+ tNc +'</span></li>';
          str += '<li><p>Account Name</p><span id="account_name" data-val="'+ account_name +'">'+ account_name +'</span></li>';
          str += '<li><p>Account Number</p><span id="account_number" data-val="'+ account_number +'">'+ account_number +'</span></li>';
          str += '<li><p>Percentage Increase</p><span id="price_increase" data-val="'+ price_increase +'">'+ price_increase +'%</span></li>';
          str += '<li><p style="width:100%;">Colour Consultant Item</p><span style="width:100%;text-align:left;" id="colorCunsultCode" data-val="'+ colorCunsultCode +'">'+colorCunsultCode+'</span></li>';
          str += '</ul>';
          str += '<ul class="setting-lists">';
          str += '<li><p>Profit Mark Up %</p><span id="profit_markup" data-val="'+ profit_markup +'">'+ profit_markup +'%</span></li>';
          str += '<li><p>Default Deposit Percentage</p><span id="default_deposit_percent" data-val="'+ default_deposit_percent +'">'+ default_deposit_percent +'%</span></li>';
          str += '<li><p></p><span></span></li>';
          str += '<li><p>Account BSB</p><span id="account_BSB" data-val="'+ account_BSB +'">'+ account_BSB +'</span></li>';
          str += '<li><p>Payment Phone Number</p><span id="payment_phone_number" data-val="'+ payment_phone_number +'">'+ payment_phone_number +'</span></li></ul>';
          $('#settingsMain').html(str);
                            
          $("#company_address").html(response.data.compDetail.address+' <br> '+response.data.compDetail.suburb + response.data.compDetail.suburb+' <br> '+response.data.compDetail.country);

          $(".docsIcon").text(response.data.compPeopleCount);
          $(".quotesIcon").text(response.data.quoteCount);
          $(".jssIcon").text(response.data.appointmentCount);
          $(".contactsIcon").text(response.data.communicationCount);

          // Customers Source
          var str = '';
          $.each(response.data.customerSources, function(d,i){

            str += '<tr data-toggle="modal" data-target="#myModal-customer-sources-update" data-csid="'+i['cs_id']+'"><td>'+i['cs_source']+'</td></tr>';
          });
          $("#customerSourceList").html(str);

          // Library doc List
          var str = '';
          $.each(response.data.docList, function(d,i){
            var d = new Date(parseInt((i['uploaded_date']*1000)));
              
            var date = ("0" + d.getDate()).slice(-2)+'/'+ ("0" + (d.getMonth() + 1)).slice(-2) +'/'+ d.getFullYear() ;
            str += '<tr data-toggle="modal" data-edit="1"  class="library-doc-modal" data-url="'+i['url']+'" data-target="#myModal-adddoc-edit"  data-always_link="'+i['always_link']+'" data-file_id="'+i['file_id']+'" data-desc="'+i['desc']+'" data-actual_name="'+i['actual_name']+'" data-file_name="'+i['file_name']+'" data-uploaded_by="'+i['uploaded_by']+'"><td>'+i['file_id']+'</td><td>'+i['file_name']+'</td><td>'+i['desc']+'</td><td>'+i['uploaded_by']+'</td><td>'+date+'</td></tr>';
          });
          $("#library-doc").html(str);

          // Prep Level List
          var str = '';
          $.each(response.data.prepLevel, function(d,i){
            
            str += '<tr data-toggle="modal" data-edit="1"  class="prep-level-modal" data-name="'+i['name']+'"  data-id="'+i['id']+'" data-is_default="'+i['is_default']+'"  data-target="#myModal-prep-level-edit"  data-uplift_cost="'+i['uplift_cost']+'" data-uplift_time="'+i['uplift_time']+'"><td>'+i['name']+'</td><td>'+i['uplift_cost']+'%</td><td>'+i['uplift_time']+'%</td></tr>';
          });
          $("#prepLevelList").html(str);

          // Special Items List
          var str = '';
          $.each(response.data.specialItems, function(d,i){
            
            str += '<tr data-toggle="modal" data-edit="1" data-id="'+i['id']+'" data_name="'+i['name']+'" data-price="'+i['price']+'" data-target="#myModal-special-item-update "><td>'+i['name']+'</td><td>$'+i['price']+'</td></tr>';

          });
          $("#special-item-list").html(str);

          // Quote Email Settings
          $('#QES-serverEmail').text(response.data.quoteEmailSettingsDetail.server_email_address);
          $('#QES-serverEmail').attr('data-email_setting_id',response.data.quoteEmailSettingsDetail.email_setting_id);
          $('#QES-mailSubject').text(response.data.quoteEmailSettingsDetail.email_subject);
          $('#QES-mailBcc').text(response.data.quoteEmailSettingsDetail.bcc_to);
          var str = '';
          $.each(response.data.quoteEmailSettingsDetail.doc, function(d,i){
            str += '<span class="attach" data-file_id="'+i['file_id']+'">'+i['file_name']+'</span>';
          });
          $("#QES-mailAttachments").html(str);

          // JSS Email Settings
          $('#JES-serverEmail').text(response.data.jssEmailSettingsDetail.server_email_address);
          $('#JES-serverEmail').attr('data-email_setting_id',response.data.jssEmailSettingsDetail.email_setting_id);
          $('#JES-mailSubject').text(response.data.jssEmailSettingsDetail.email_subject);
          $('#JES-mailBcc').text(response.data.jssEmailSettingsDetail.bcc_to);
          var str = '';
          $.each(response.data.jssEmailSettingsDetail.doc, function(d,i){
            str += '<span class="attach" data-file_id="'+i['file_id']+'">'+i['file_name']+'</span>';
          });
          $("#JES-mailAttachments").html(str);


          // Payments Notes List
          var str = '';
          $.each(response.data.paymentNotes, function(d,i){
            str += '<tr data-toggle="modal"  data-edit="1" data-id="'+i['id']+'" data-target="#myModal-payment-notes-update"><td>'+i['name']+'</td></tr>';

          });
          $("#paymentNotesList").html(str);

          // Paint Stores List
          var str = '';
          $.each(response.data.allStores, function(d,i){
            str += '<tr data-toggle="modal"  data-edit="1" data-deliver_id="'+i['deliver_id']+'" data-address="'+i['address']+'"  data-country="'+i['country']+'" data-postcode="'+i['postcode']+'" data-state="'+i['state']+'" data-suburb="'+i['suburb']+'" data-name="'+i['name']+'" data-phone="'+i['phone']+'" data-mobile="'+i['mobile']+'" data-email="'+i['email']+'" data-company="'+i['company']+'" data-target="#myModal-payment-notes-update"><td>'+i['name']+'</td></tr>';

          });
          $("#paymentStoresList").html(str);

          // Paint Order Notes List
          var str = '';
          $.each(response.data.paint_order_notes, function(d,i){
            str += '<tr data-toggle="modal"  data-edit="1" data-id="'+i['id']+'" data-target="#myModal-payment-notes-update"><td>'+i['name']+'</td></tr>';

          });
          $("#paymentOrderNotesList").html(str);


        }else{
          alert(response.message);
        }
        $('#loader').hide();
      },
      error: function () {
        $('#loader').hide();
        alert("Something went wrong");
      }
    });
      
    }

    // Company Setting Form Modal
    $('#edit_company').click(function(){
      var appli_cost = ($('#application_cost').data('val')).replace('/H','');
      $("#myModal-settings [name='application_cost']").val(appli_cost);
      $("#myModal-settings [name='gst']").val($('#application_cost').data('gst'));
      $("#myModal-settings [name='users_settings_id']").val($('#application_cost').data('users_settings_id'));
      $("#myModal-settings [name='gross_margin_percent']").val($('#gross_margin_percent').data('val'));
      $("#myModal-settings [name='profit_markup']").val($('#profit_markup').data('val'));
      $("#myModal-settings [name='default_deposit_percent']").val($('#default_deposit_percent').data('val'));

      $('#myModal-settings [name=terms_condition] option').filter(function() { 
          return ($(this).val() == $('#terms_condition').data('val')); 
      }).prop('selected', true);

      $("#myModal-settings [name='account_name']").val(($('#account_name').data('val')).replace("-",""));
      $("#myModal-settings [name='account_BSB']").val(($('#account_BSB').data('val')).replace("-",""));
      $("#myModal-settings [name='account_number']").val(($('#account_number').data('val')).replace("-",""));
      $("#myModal-settings [name='payment_phone_number']").val($('#payment_phone_number').data('val'));
      $("#myModal-settings [name='price_increase']").val($('#price_increase').data('val'));

      $('#myModal-settings [name=colour_consultant_item] option').filter(function() { 
          return ($(this).val() == $('#colour_consultant_item').data('val')); 
      }).prop('selected', true);
          
    });

    // Library Document Form Modal
    $(document).on('click','.library-doc-modal',function(){
      
      $("#myModal-adddoc-edit [name='file_name']").val($(this).data('file_name'))
      $("#myModal-adddoc-edit [name='doc_id']").val($(this).data('file_id'))
      if($(this).data('always_link') == 1){
        $("#myModal-adddoc-edit #docRadio1").attr('checked', 'checked');
      }else{
        $("#myModal-adddoc-edit #docRadio0").attr('checked', 'checked');
      }
      $("#myModal-adddoc-edit [name='desc']").val($(this).data('desc'))

      if($(this).data('edit') == 1){
        $('#previewFileName').text($(this).data('actual_name'));
        $('#file_review').attr('href',$(this).data('url'));
        $('.image-preview-div').show();

        $('#delDocDiv').html('<a id="delDoc" href="javascript:void(0);" data-id="'+$(this).data('file_id')+'" type="button" class="btn dlt-btn"><i class="fa fa-trash-o"></i> Delete</a>');
        $('#delDocDiv').show();

      }else{
        $("#myModal-adddoc-edit [name='desc']").text("");
        $('.image-preview-div').hide();
        $('#delDocDiv').hide();
      }

          
    });

    // Prep level modal
    $(document).on('click','.prep-level-modal',function(){
      $("#myModal-prep-level-edit [name='name']").val($(this).data('name'))
      $("#myModal-prep-level-edit [name='id']").val($(this).data('id'))
      if($(this).data('is_default') == 1){
        $("#myModal-prep-level-edit input[name='is_default']").prop('checked',true);
      }else{
        $("#myModal-prep-level-edit input[name='is_default']").prop('checked',false);
      }
      $("#myModal-prep-level-edit [name='uplift_cost']").val($(this).data('uplift_cost'))
      $("#myModal-prep-level-edit [name='uplift_time']").val($(this).data('uplift_time'))
      if($(this).data('edit') == 1){
       $("#myModal-prep-level-edit .modal-title").text('Update Prep. Level');
      }else{
        $("#myModal-prep-level-edit .modal-title").text('Add Prep. Level');
      }   
    });
  // Save company setting
  $('#submitCompanySetting').click(function(){
    $('#loader').show();
      var data = '{"payment_phone_number" :"'+$("#myModal-settings [name='payment_phone_number']").val()+'","account_BSB" : "'+$("#myModal-settings [name='account_BSB']").val()+'","application_cost" : "'+$("#myModal-settings [name='application_cost']").val()+'","account_number" : "'+$("#myModal-settings [name='account_number']").val()+'","company_id" : "'+<?php echo $company_id; ?>+'","colour_consultant_item" : "'+$("#myModal-settings [name='colour_consultant_item']").val()+'","gst" : "'+$("#myModal-settings [name='gst']").val()+'","account_name" : "'+$("#myModal-settings [name='account_name']").val()+'","default_deposit_percent" : "'+$("#myModal-settings [name='default_deposit_percent']").val()+'","users_settings_id" : "'+$("#myModal-settings [name='users_settings_id']").val()+'","terms_condition" : "'+$("#myModal-settings [name='terms_condition']").val()+'","profit_markup" : "'+$("#myModal-settings [name='profit_markup']").val()+'","price_increase" : "'+$("#myModal-settings [name='price_increase']").val()+'","gross_margin_percent" : "'+$("#myModal-settings [name='gross_margin_percent']").val()+'"}';
    $.ajax({
      url: siteBaseUrl+'/webservice/save-company-setting',
      type: 'post',
      data: {'json':data},
      success: function (response) {
        var response = JSON.parse(response);
        if(response.success == 1){

          // Company Settings
          var application_cost = (response.data.companySetting.application_cost != "") ? response.data.companySetting.application_cost+"/H" : "-";
          var gross_margin_percent = (response.data.companySetting.gross_margin_percent != "") ? response.data.companySetting.gross_margin_percent : "0";

          $('#myModal-settings [name=terms_condition] option').filter(function() { 
              return ($(this).val() == response.data.companySetting.terms_condition); 
          }).prop('selected', true);

          var tNc = $('#myModal-settings [name=terms_condition] option:selected').text();

          var account_name = (response.data.companySetting.account_name != "") ? response.data.companySetting.account_name : "-";
          var account_number = (response.data.companySetting.account_number != "") ? response.data.companySetting.account_number: "-";
          var price_increase = (response.data.companySetting.price_increase != "") ? response.data.companySetting.price_increase: "0";

          $('#myModal-settings [name=colour_consultant_item] option').filter(function() { 
              return ($(this).val() == response.data.companySetting.colour_consultant_item); 
          }).prop('selected', true);

          var colorCunsultCode = $('#myModal-settings [name=colour_consultant_item] option:selected').text();

          var profit_markup = (response.data.companySetting.profit_markup != "") ? response.data.companySetting.profit_markup: "0";
          var default_deposit_percent = (response.data.companySetting.default_deposit_percent != "") ? response.data.companySetting.default_deposit_percent: "0";
          var account_BSB = (response.data.companySetting.account_BSB != "") ? response.data.companySetting.account_BSB: "-";
          var payment_phone_number = (response.data.companySetting.payment_phone_number != "") ? response.data.companySetting.payment_phone_number: "-";

          var str = '<ul class="setting-lists">';
          str += '<li><p>Application Cost</p><span id="application_cost" data-val="'+ application_cost +'">'+ application_cost +'</span></li>';
          str += '<li><p>Gross Margin %</p><span id="gross_margin_percent" data-val="'+ gross_margin_percent +'">'+ gross_margin_percent +'%</span></li>';
          str += '<li><p>Terms & Conditions Doc</p><span id="tNc" data-val="'+ tNc +'">'+ tNc +'</span></li>';
          str += '<li><p>Account Name</p><span id="account_name" data-val="'+ account_name +'">'+ account_name +'</span></li>';
          str += '<li><p>Account Number</p><span id="account_number" data-val="'+ account_number +'">'+ account_number +'</span></li>';
          str += '<li><p>Percentage Increase</p><span id="price_increase" data-val="'+ price_increase +'">'+ price_increase +'%</span></li>';
          str += '<li><p style="width:100%;">Colour Consultant Item</p><span style="width:100%;text-align:left;" id="colorCunsultCode" data-val="'+ colorCunsultCode +'">'+colorCunsultCode+'</span></li>';
          str += '</ul>';
          str += '<ul class="setting-lists">';
          str += '<li><p>Profit Mark Up %</p><span id="profit_markup" data-val="'+ profit_markup +'">'+ profit_markup +'%</span></li>';
          str += '<li><p>Default Deposit Percentage</p><span id="default_deposit_percent" data-val="'+ default_deposit_percent +'">'+ default_deposit_percent +'%</span></li>';
          str += '<li><p></p><span></span></li>';
          str += '<li><p>Account BSB</p><span id="account_BSB" data-val="'+ account_BSB +'">'+ account_BSB +'</span></li>';
          str += '<li><p>Payment Phone Number</p><span id="payment_phone_number" data-val="'+ payment_phone_number +'">'+ payment_phone_number +'</span></li></ul>';
          $('#settingsMain').html(str);
        }else{
          alert(response.message);
        }
        $('#loader').hide();
        $('#companySettings').modal('hide');
      },
      error: function () {
        $('#loader').hide();
        alert("Something went wrong");
      }
    });
  });

  // Save Library Document
  // $('#libDocSubmit').click(function(){
  $("#addDcoForm").on('submit', function(e){
    $('#loader').show();
    e.preventDefault();
    var doc_idd = $("#myModal-adddoc-edit [name='doc_id']").val();
    var data = '{"file_name" :"'+$("#myModal-adddoc-edit [name='file_name']").val()+'","always_link" : "'+$("#myModal-adddoc-edit [name='always_link']").val()+'","desc" : "'+$("#myModal-adddoc-edit [name='desc']").val()+'","company_id" : "'+<?php echo $company_id; ?>+'","subscriber_id" : "'+<?php echo $subscriber_id; ?>+'","doc_id" : "'+doc_idd+'"}';
    var formData = new FormData(this);
    formData.append('json', data);
    $.ajax({
      url: siteBaseUrl+'/webservice/save-doc',
      type: 'post',
      // data: {'json':data},
      data : formData,
      dataType: 'json',
      contentType: false,
      cache: false,
      processData:false,
      success: function (response) {
        // var response = JSON.parse(response);
        if(response.success == 1){
          var str = '';
          $.each(response.data.docList, function(d,i){
            var d = new Date(parseInt((i['uploaded_date']*1000)));
            var date = ("0" + d.getDate()).slice(-2)+'/'+ ("0" + (d.getMonth() + 1)).slice(-2) +'/'+ d.getFullYear() ;
            str = '<tr data-toggle="modal" data-edit="1"  class="library-doc-modal" data-url="'+i['url']+'" data-target="#myModal-adddoc-edit"  data-always_link="'+i['always_link']+'" data-file_id="'+i['file_id']+'" data-desc="'+i['desc']+'" data-actual_name="'+i['actual_name']+'" data-file_name="'+i['file_name']+'" data-uploaded_by="'+i['uploaded_by']+'"><td>'+i['file_id']+'</td><td>'+i['file_name']+'</td><td>'+i['desc']+'</td><td>'+i['uploaded_by']+'</td><td>'+date+'</td></tr>';
          });

          if(doc_idd > 0){
            $("#library-doc tr[data-file_id='"+doc_idd+"']").replaceWith(str);
          }else{
            $("#library-doc").append(str);
          }
          
        }else{
          alert(response.message);
        }
        $('#loader').hide();
        $('#myModal-adddoc-edit').modal('hide').removeClass('show');
        $('.modal-backdrop').remove();
      },
      error: function () {
        $('#loader').hide();
        alert("Something went wrong");
      }
    });
  });

  // Delete Library Document

  $(document).on('click','#delDoc', function(e){
    var r = confirm("Please confirm to delete this document.");
    if (r == true) {
     
      e.preventDefault();
      var company_id = <?php echo $company_id; ?>;
      var doc_id  = $(this).data('id');
      var data = '{"doc_id" : "'+doc_id+'","company_id" : "'+company_id+'"}';
      $.ajax({
          type: 'POST',
          url: siteBaseUrl+'/webservice/delete-document',
          data : {'json':data},
          dataType: 'json',
          success: function(response){ 
              if(response.success == 1){ 
              // alert(response.message);
              $('#library-doc tr[data-file_id="'+doc_id+'"]').remove();
              $('#myModal-adddoc-edit').modal('hide').removeClass('show');
              $('.modal-backdrop').remove();

              }else{
                  alert(response.message);
              }
          }
      });

    }
  });

  
  </script>



  <script type="text/javascript">
    $(function(){
      // console.log("I'm On : PaintPad");

      /*-------------------------*/

      $('.QuotePageStatus').click(function(){

        if ($(this).hasClass('exclude-filter')) {

          $(this).removeClass('exclude-filter');

        } else {

          if ( $('.exclude-filter').length == 5) {
            return false
          } else {

            $(this).addClass('exclude-filter');

          }

        }
      });




        /*
         * Getting the contacts suggestions
         */


        $('#search_existing_client_quote').focus(function(e){

          e.stopPropagation();

          var contactsHtml = '';
              contactsHtml += "<ul id='contacts-suggestions' style='list-style: none;padding: 0;background: white;height: 200px;overflow-Y: scroll;'>";

                $.each(jsonUsers, function(i,d){


                    contactsHtml +='<li class="tosearch" data-id="'+d.id+'">';
                      contactsHtml +='<span class="cnt_img">';
                        contactsHtml +='<img src="'+d.img+'" height="58px" width="58px" style="border-radius: 50%;float: left;" class="mCS_img_loaded">';
                      contactsHtml +='</span>';
                      contactsHtml +='<div class="contact_person_detail" style="float: left;width: 70%;height: 70px;">';
                        contactsHtml +='<p class="contact_nameSearch">'+d.value+'</p>';
                        contactsHtml +='<small>'+d.phone+'</small>';
                      contactsHtml +='</div>';
                    contactsHtml +='</li>';

                    // id: 25
                    // img: "http://www.enacteservices.com/paintpad/image/profileIcon.png"
                    // label: "ytiyutriu7t"
                    // phone: "12345"
                    // value: "ytiyutriu7t"

                    console.log(d)
                });
              contactsHtml += "</ul>";

            if ($("#contacts-suggestions").length ==0) {
              $(this).after( contactsHtml );
            }
        });



        /*
         * Filter the contacts suggestions
         */

        $("#search_existing_client_quote").on("keyup", function() {

          var value = $(this).val().toLowerCase();

          $("#contacts-suggestions li").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
          });
        });

        /*
         * Select the contact from suggestion
         */

        $('.quote-form-group').on( 'click', "#contacts-suggestions li", function(){

          var contactName = $(this).find(".contact_nameSearch").text();

          $("#search_existing_client_quote").val( contactName );

          $("#search_existing_client_quote").attr( 'clientid', $(this).data('id') );
          $("#contacts-suggestions").remove();
        });

        $(document).click(function(ev){

          if (ev.target == $("#search_existing_client_quote")[0] || ev.target == $("#search_existing_client_quote")[0] ) {
            return
          }else{
            $("#contacts-suggestions").remove();
          }

        });


    });
  </script>


<!-- 19-feb -->
  <script>
    $(document).ready(function(){
        // Add minus icon for collapse element which is open by default
        $(".collapse.show").each(function(){
          $(this).prev(".card-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
        });
        
        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function(){
          $(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
        }).on('hide.bs.collapse', function(){
          $(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
        });
    });
  </script>

 