<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle {
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $css = [
		'css/bootstrap.min.css',
		'css/demo.css',
		'css/style.css',
		'css/calendarindex.css',
		'css/jquery.mCustomScrollbar.css',
		'css/jquery.tagit.css',
		'css/bootstrap-select.min.css',
	];
	public $js = [
		'https://unpkg.com/sweetalert/dist/sweetalert.min.js',
		'js/custom.js',
		'js/paintpad.js',
		'js/tag-it.js',
		'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js',

	];
	public $depends = [
		'yii\web\YiiAsset',
		'yii\bootstrap\BootstrapAsset',
	];
}
