<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class QuoteAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/jquery-ui.css'
    ];
    public $js = [
        //'https://code.jquery.com/jquery-3.3.1.slim.min.js',
        'js/map-function.js',
        // 'https://maps.google.com/maps/api/js?key=AIzaSyC2Qg3ywv9Dhg-Ptjv-5fG3I3UEt0rLufQ&libraries=places&callback=initMap',
        'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js',
        'https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js',
        'https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js',
        'js/bootstrap-select.min.js',
        //'https://unpkg.com/sweetalert/dist/sweetalert.min.js',
        'js/jquery-ui.min.js',
        'js/jquery.mCustomScrollbar.concat.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/jquery.inview/1.0.0/jquery.inview.js',
        'https://cdnjs.cloudflare.com/ajax/libs/jquery.inview/1.0.0/jquery.inview.min.js',
        //'js/custom.js',
        //'js/bootstrap.min.js',
        /*'js/fontAwesome.js',
        'js/jquery-minn.js',   
        'js/jquery.ui.addresspicker.js',
        'js/custom.js',
        'js/tag-it.js',*/
        
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
