<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class DashboardAsset extends AssetBundle {
	public $basePath = '@webroot';
	public $baseUrl = '@web';
	public $css = [
	];
	public $js = [
		// 'https://code.jquery.com/jquery-3.3.1.slim.min.js',
		'js/map-function.js',
		'https://maps.google.com/maps/api/js?key=AIzaSyC2Qg3ywv9Dhg-Ptjv-5fG3I3UEt0rLufQ&libraries=places&callback=initMap',
		'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js',
		'https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js',
		'https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js',
	];
	public $depends = [
		'yii\web\YiiAsset',
		'yii\bootstrap\BootstrapAsset',
	];
}
