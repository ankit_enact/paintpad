<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class CalendarAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        /*'css/demo.css',*/
        'css/bootstrap.min.css',
        'css/demo.css',
        /*'css/site.css',*/        
        'css/style.css',
        'css/jquery.ui.all.css',
        'css/jquery.ui.theme.css',
        'css/jquery.mCustomScrollbar.css',
        'css/jquery.tagit.css',  

    ];
    public $js = [
        'js/jquerry.js',
        'https://maps.google.com/maps/api/js?key=AIzaSyC2Qg3ywv9Dhg-Ptjv-5fG3I3UEt0rLufQ&libraries=places',
        'js/bootstrap.min.js',
        'js/fontAwesome.js',
        'js/jquery-minn.js',   
        'js/jquery.ui.addresspicker.js',
        'js/custom.js',        
        'js/tag-it.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
