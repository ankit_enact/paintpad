<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class ContactAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/jquery-ui.css',
        'frontend/web/css/style.css',
        'frontend/web/css/bootstrap-select.min.css',
        'frontend/web/css/timepicki.css',
        'frontend/web/css/jquery.datepick.css',
        //'css/contactCss.css',
    ];
    public $js = [
        'js/jquery-ui.min.js',
        'frontend/web/js/timepicki.js',
        'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js',
        'frontend/web/js/bootstrap-select.min.js',
        'frontend/web/js/jquery.datepick.js',
       
        'js/map-function-contact.js',
        'js/map-function.js',
        'https://maps.google.com/maps/api/js?key=AIzaSyC2Qg3ywv9Dhg-Ptjv-5fG3I3UEt0rLufQ&libraries=places&callback=initMap',
        'js/jquery.mCustomScrollbar.concat.min.js', 
        //'frontend/web/js/custom-contact.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
