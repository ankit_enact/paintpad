<?php

namespace frontend\controllers;

use Yii;
use app\models\TblCommunications;
use app\models\TblCommunicationsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\models\TblNewsletterTemplates;
use app\models\TblContacts;
use app\models\TblQuotes;
use app\models\TblAttachment;

use yii\helpers\ArrayHelper;


/**
 * CommunicationsController implements the CRUD actions for TblCommunications model.
 */
class CommunicationsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TblCommunications models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TblCommunicationsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TblCommunications model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $headers = apache_request_headers();        
        //echo "<pre>"; print_r($headers); exit;

        (isset($headers) && isset($headers['Quoteid']) && $headers['Quoteid'] !='') ? $head = 1 : $head = 0;

        return $this->renderPartial('view', [
            'model' => $this->findModel($id),
            'head'  => $head,
        ]);
    }

    /**
     * 
     s a new TblCommunications model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    { 

        $quoteId   = '';
        $contactId = '';
        
        $r_emails   = '';
        $cc_emails  = '';
        $bcc_emails = '';
        $to         = '';
        $subject    = '';
        $pdf        = '';
        $type       = 0;


        $iArr = array();
        $emailArr = array(); 

        $rrEmails = array();
        $crrEmails = array();
        $bcrrEmails = array();

        $headers = apache_request_headers();        
        //echo "<pre>"; print_r($headers); exit;


        if(isset($headers) && isset($headers['Quoteid']) && $headers['Quoteid'] !='' && isset($headers['Subscriberid']) && $headers['Subscriberid'] !=''){

            //echo "<pre>"; print_r($headers); exit;

            $quoteId = $headers['Quoteid'];
            $id = $headers['Subscriberid'];

            $to      = isset($headers['To']) ? $headers['To'] : '';
            $subject = isset($headers['Subject']) ? $headers['Subject'] : '';
            $pdf     = isset($headers['Attachment']) ? $headers['Attachment'] : '';
            $type    = isset($headers['Type']) ? $headers['Type'] : 0;

            $pdf = stripcslashes($headers['Attachment']);

              $session = Yii::$app->session;

                $session->set('sub_id', $id);
                $session->set('quote_id', $quoteId);
                $session->set('head', '1');
                $session->set('pdf', $pdf);
                $session->set('type', $type);

             $head = 1;

        }else{

             $head = 0;
            
            $session = Yii::$app->session;
            $quote_id = $session->get('quote_id');

            if(isset($quote_id) && !empty($quote_id)){
                $quoteId = $quote_id;
            }else{
                //$quoteId = '';
                $quoteId = 3;
            }

            $id = Yii::$app->user->getId();
            
        }

        if ($quoteId != '' && ($modelQuote = TblQuotes::findOne($quoteId)) !== null) {

            $contactId = $modelQuote->contact_id;               
        }

        $model = new TblCommunications();        

        //$emails = ArrayHelper::map(TblContacts::find()->select('contact_id, email')->where(['subscriber_id' => $id])->all(), 'email', 'email');        
        //$emails = TblContacts::find()->select('contact_id, email')->asArray()->where(['subscriber_id' => $id])->all();
        
        //$emails = ArrayHelper::map(TblContacts::find()->select('contact_id, email')->where(['subscriber_id' => $id])->all(), 'email', 'email');

        $emails = TblContacts::find()->select('subscriber_id,email')->where(['subscriber_id' => $id])->all();

        //echo '<pre>'; print_r($emails); exit;

        if(count($emails)>0){

            foreach($emails as $_emails){

                $emailArr[] = $_emails->email;
            }//foreach

        }

        //echo '<pre>'; print_r(Yii::$app->request->post()); exit;

        if ($model->load(Yii::$app->request->post())) {

            $modell = Yii::$app->request->post();

            if(isset($modell['TblCommunications']['receiver_email']) && count($modell['TblCommunications']['receiver_email'])>0){
                //$r_emails = implode(',', $modell['TblCommunications']['receiver_email']);
                $r_emails = $modell['TblCommunications']['receiver_email'];

                 $searchString1 = ',';
                 if( strpos($r_emails, $searchString1) !== false ) {

                    $recEmail = explode(',', $modell['TblCommunications']['receiver_email']);

                    foreach($recEmail as $_remails){
                      $rrEmails[] = $_remails;
                    }//foreach
                 }else{

                    $rrEmails = $modell['TblCommunications']['receiver_email'];
                 }

            }

            //for CC mail
            if(isset($modell['TblCommunications']['cc_email']) && count($modell['TblCommunications']['cc_email'])>0 && $modell['TblCommunications']['cc_email'] !=''){
                //$cc_emails = implode(',', $modell['TblCommunications']['cc_email']);
                $cc_emails = $modell['TblCommunications']['cc_email'];

                 $searchString2 = ',';
                 if( strpos($cc_emails, $searchString2) !== false ) {

                    $cEmail = explode(',', $modell['TblCommunications']['cc_email']);

                    foreach($cEmail as $_cEmail){
                      $crrEmails[] = $_cEmail;
                    }//foreach
                 }else{

                    $crrEmails = $modell['TblCommunications']['cc_email'];
                 }

            }

            //for BCC mail
            if(isset($modell['TblCommunications']['bcc_email']) && count($modell['TblCommunications']['bcc_email'])>0 && $modell['TblCommunications']['bcc_email'] !=''){
                //$bcc_emails = implode(',', $modell['TblCommunications']['bcc_email']);
                $bcc_emails = $modell['TblCommunications']['bcc_email'];

                 $searchString3 = ',';
                 if( strpos($bcc_emails, $searchString3) !== false ) {

                    $bcEmail = explode(',', $modell['TblCommunications']['bcc_email']);

                    foreach($bcEmail as $_bcEmail){
                      $bcrrEmails[] = $_bcEmail;
                    }//foreach
                 }else{
                    $bcrrEmails = $modell['TblCommunications']['bcc_email'];
                 }
            }

            $host_name = isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : $_SERVER['HTTP_HOST'];                  
            $siteURL = 'http://' . $host_name . '/' . Yii::$app->request->baseUrl . '/quotepdf';     

            

            $model->attachment = UploadedFile::getInstances($model, 'attachment');

            $files = '';

            if ($model->attachment) {

               $pdf = ($session->get('pdf')) ? $session->get('pdf') : '';

               if(isset($modell['TblCommunications']['indexx']) && count($modell['TblCommunications']['indexx'])>0){
                    $iArr = $modell['TblCommunications']['indexx'];
                }

                $cc = 0;
                foreach ($model->attachment as $attachment) {

                    if(in_array($cc, $iArr)){
                    }else{

                        $path = 'attachments/' . $attachment->baseName . '.' . $attachment->extension;
                        $count = 0;
                        {
                            while(file_exists($path)) {

                               $time= time();
                               $path = 'attachments/' .$time.'_'.$count.'.' . $attachment->extension;
                               $count++;
                            }
                        }
                        $attachment->saveAs($path);
                        $files[] = $path;

                    }
                    $cc++;
                } 

                $model->attachment = implode(',', $files);

                $value = Yii::$app->mailer->compose()
                             ->setFrom(['parshantenact@gmail.com'=>'enact'])
                             ->setTo($rrEmails)
                             ->setCc($crrEmails)
                             ->setBcc($bcrrEmails)
                             ->setSubject($model->subject)
                             ->setHtmlBody($model->body);
               if(!empty($files)) {
                    foreach($files as $file) {
                        $value->attach($file);       
                    }
               }              

               if($pdf != ''){
                    if(isset($modell['TblCommunications']['pdfindx']) && $modell['TblCommunications']['pdfindx'] == 0){                        
                    }else{
                        $value->attach($pdf);
                    }   
               }     

               $typeid = ($session->get('type')) ? $session->get('type') : '';

               if($typeid != '' && $typeid == 2){

                    if(isset($modell['TblCommunications']['jssindx']) && $modell['TblCommunications']['jssindx'] == 0){                        
                    }else{
                        $file1 = $siteURL.'/WorkcoverCertRegis_MA.pdf';
                        $value->attach($file1);
                    }   

                    if(isset($modell['TblCommunications']['jssindx1']) && $modell['TblCommunications']['jssindx1'] == 1){                        
                    }else{
                        $file2 = $siteURL.'/Dulux_Accredited_Painters.pdf';
                        $value->attach($file2);
                    }   

               }   

                 
                $value->send();

            }else{

                $pdf    = ($session->get('pdf')) ? $session->get('pdf') : '';
                //echo 'here1'.$pdf; exit;

                $model->attachment = '';

                $value = Yii::$app->mailer->compose()
                             ->setFrom(['parshantenact@gmail.com'=>'enact'])
                             ->setTo($rrEmails)
                             ->setCc($crrEmails)
                             ->setBcc($bcrrEmails)
                             ->setSubject($model->subject)
                             ->setHtmlBody($model->body);

               if($pdf != ''){
                    if(isset($modell['TblCommunications']['pdfindx']) && $modell['TblCommunications']['pdfindx'] == 0){                        
                    }else{
                        $value->attach($pdf);
                    }   
               }     

               $typeid = ($session->get('type')) ? $session->get('type') : '';

               if($typeid != '' && $typeid == 2){

                    if(isset($modell['TblCommunications']['jssindx']) && $modell['TblCommunications']['jssindx'] == 0){                        
                    }else{
                        $file1 = $siteURL.'/WorkcoverCertRegis_MA.pdf';
                        $value->attach($file1);
                    }   

                    if(isset($modell['TblCommunications']['jssindx1']) && $modell['TblCommunications']['jssindx1'] == 1){                        
                    }else{
                        $file2 = $siteURL.'/Dulux_Accredited_Painters.pdf';
                        $value->attach($file2);
                    }   

               }                  
                 
                $value->send();

            }

            $session = Yii::$app->session;

            $quoteId = ($session->get('quote_id')) ? $session->get('quote_id') : $quoteId;
            $id      = ($session->get('sub_id')) ? $session->get('sub_id') : $id;
            $head    = ($session->get('head')) ? $session->get('head') : 0;

            // echo 'here==>'.$id;
            // echo 'here1==>'.$quoteId; 

           /* echo 'here2==>'.$head; 
            exit;*/

            date_default_timezone_set("UTC");
            $milliseconds = strtotime(gmdate("Y-m-d H:i:s"));

            
            if( count($rrEmails) >1 ) {
                $rrEmails = implode(', ',$rrEmails);
            }

            $model->contact_id     = $contactId;
            $model->subscriber_id  = $id;
            $model->quote_id       = $quoteId;
            $model->receiver_email = $rrEmails;
            $model->cc_email       = $cc_emails;
            $model->bcc_email      = $bcc_emails;

            $model->date           = $milliseconds;
            $model->created_at     = $milliseconds;
            $model->updated_at     = $milliseconds;

            $model->save();

            if($head == 1){
              return $this->redirect(['mailsent']);
            }else{
              return $this->redirect(['view', 'id' => $model->comm_id]);

            }
            
            
        }


        $jsspdfs = array('WorkcoverCertRegis_MA.pdf','Dulux_Accredited_Painters.pdf');

        //echo 'here'.$subject; exit;

        return $this->render('create', [
            'model' => $model, 'emails' =>$emailArr, 'head' => $head, 'to' => $to, 'subject' => $subject, 'pdf' => $pdf, 'type' => $type, 'jsspdfs' => $jsspdfs
        ]);
   
    }


    /**
     * Updates an existing TblCommunications model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->comm_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TblCommunications model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TblCommunications model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TblCommunications the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TblCommunications::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

public function actionCkeditor_image_upload()
    {       
        $funcNum = $_REQUEST['CKEditorFuncNum'];

        $message = '';

        if($_FILES['upload']) {

          if (($_FILES['upload'] == "none") OR (empty($_FILES['upload']['name']))) {
          $message = Yii::t('app', "Please Upload an image.");
          }

          else if ($_FILES['upload']["size"] == 0 OR $_FILES['upload']["size"] > 5*1024*1024)
          {
          $message = Yii::t('app', "The image should not exceed 5MB.");
          }

          else if ( ($_FILES['upload']["type"] != "image/jpg") 
                    AND ($_FILES['upload']["type"] != "image/jpeg") 
                    AND ($_FILES['upload']["type"] != "image/png"))
          {
          $message = Yii::t('app', "The image type should be JPG , JPEG Or PNG.");
          }

          else if (!is_uploaded_file($_FILES['upload']["tmp_name"])){

          $message = Yii::t('app', "Upload Error, Please try again.");
          }

          else {
            //you need this (use yii\db\Expression;) for RAND() method 
            $random = rand(0, 9876543210);

            $extension = pathinfo($_FILES['upload']['name'], PATHINFO_EXTENSION);

            //Rename the image here the way you want
            $name = date("m-d-Y-h-i-s", time())."-".$random.'.'.$extension; 

            // Here is the folder where you will save the images
            $folder = 'uploads/ckeditor_images/';  

            $url = Yii::$app->urlManager->createAbsoluteUrl($folder.$name);

            move_uploaded_file( $_FILES['upload']['tmp_name'], $folder.$name );

          }

          echo '<script type="text/javascript">window.parent.CKEDITOR.tools.callFunction("'
               .$funcNum.'", "'.$url.'", "'.$message.'" );</script>';

        }

    }//actionCkeditor_image_upload

    public function actionList(){

        $headers = apache_request_headers();        

        //echo "<pre>"; print_r($headers); exit;

        if(isset($headers) && isset($headers['Qid']) && $headers['Qid'] !=''){

            //echo "<pre>"; print_r($headers); exit;

            $head = 1;

            $quoteId = $headers['Qid'];

            if ($quoteId != '') {

                $modelComm = TblCommunications::find()
                                ->joinWith('commQuote')
                                ->joinWith('commSubscriber')
                                ->where(['tbl_communications.quote_id' => $quoteId])
                                ->all();

                //echo "<pre>"; print_r($modelComm); exit;

                return $this->render('list', [
                    'modelComm' => $modelComm, 'head' => $head
                ]);

            }

        }else{

            //echo 'here'; exit;

            $head = 0;
            
            $quoteId = 3;

            if ($quoteId != '') {

                $modelComm = TblCommunications::find()
                                ->joinWith('commQuote')
                                ->joinWith('commSubscriber')
                                ->where(['tbl_communications.quote_id' => $quoteId])
                                ->all();

                //echo "<pre>"; print_r($modelComm); exit;

                return $this->render('list', [
                    'modelComm' => $modelComm, 'head' => $head
                ]);

            }
            
        }

    }//actionList

    public function actionMailsent()
    {
        return $this->render('mailsent');
    }

}
