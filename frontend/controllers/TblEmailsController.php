<?php

namespace frontend\controllers;

use Yii;
use app\models\TblEmails;
use app\models\TblEmailsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\models\TblNewsletterTemplates;
use app\models\TblContacts;

use yii\helpers\ArrayHelper;

/**
 * TblEmailsController implements the CRUD actions for TblEmails model.
 */
class TblEmailsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {            
        if ($action->id == 'ckeditor_image_upload') {
        $this->enableCsrfValidation = false;
    }

      return parent::beforeAction($action);
    }


    /**
     * Lists all TblEmails models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TblEmailsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    /**
     * Displays a single TblEmails model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TblEmails model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {      

        $id = Yii::$app->user->getId();

       // echo $id; exit;
        $model = new TblEmails();
        

        $emails = ArrayHelper::map(TblContacts::find()->select('contact_id, email')->where(['subscriber_id' => $id])->all(), 'email', 'email');


        /*$emails = ArrayHelper::map(TblContacts::find()->select('contact_id, email')->where(['subscriber_id' => $id])->all(), 'email', function($model) {
                return $model['email'].'-'.$model['contact_id'];
            });*/

        //echo "<pre>"; print_r($emails); exit;
        //echo "<pre>"; print_r(Yii::$app->request->post()); exit;

        if ($model->load(Yii::$app->request->post())) {

            $modell = Yii::$app->request->post();

            /*ob_start();
            echo '<pre>'; print_r($modell['TblEmails']['receiver_email']); exit;*/

            if(isset($modell['TblEmails']['receiver_email']) && count($modell['TblEmails']['receiver_email'])>0){

                $r_emails = implode(',', $modell['TblEmails']['receiver_email']);
            }

            $model->attachment = UploadedFile::getInstances($model, 'attachment');

             $files = '';



/*            if( $model->attachment ){

                if ( $model->attachment )
                {
                    foreach ($model->attachment as $file) 
                    {   
                        $time = time();

                        $file->saveAs('attachments/' .$time. '.' . $file->extension);
                        $files = 'attachments/' .$time. '.' . $file->extension;
                        $model->attachment = $files;
                    }

                }

            }*/


            //echo __DIR__; exit;

            if ($model->attachment) {

                foreach ($model->attachment as $attachment) {
                    $path = 'attachments/' . $attachment->baseName . '.' . $attachment->extension;
                    $count = 0;
                    {
                        while(file_exists($path)) {

                            $time= time();
                           $path = 'attachments/' .$time.'_'.$count.'.' . $attachment->extension;
                           $count++;
                        }
                    }
                    $attachment->saveAs($path);
                    $files[] = $path;
                } 

                $model->attachment = implode(',', $files);

                $value = Yii::$app->mailer->compose()
                             ->setFrom(['parshantenact@gmail.com'=>'enact'])
                             ->setTo($model->receiver_email)
                             ->setSubject($model->subject)
                             ->setHtmlBody($model->content);
               if(!empty($files)) {
                    foreach($files as $file) {
                        $value->attach($file);       
                    }
               }              
                 
              $value->send();



            }else{

                $value = Yii::$app->mailer->compose()
                             ->setFrom(['parshantenact@gmail.com'=>'enact'])
                             ->setTo($model->receiver_email)
                             ->setSubject($model->subject)
                             ->setHtmlBody($model->content)
                             ->send();
            }

            $model->receiver_email = $r_emails;

            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model, 'emails' =>$emails
        ]);
    }

    /**
     * Updates an existing TblEmails model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TblEmails model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TblEmails model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TblEmails the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TblEmails::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionTemplate()
    {
        $id = $_REQUEST['id'];

        if(isset($id) && !empty($id)){
           
             //echo 'id= '.$id; exit;          

            if (($templateModel = TblNewsletterTemplates::findOne($id)) !== null) {
                //echo '<pre>'; print_r($templateModel); exit;
                return $templateModel->temp_body;
            }
        }
    }//actionTemplate

    public function actionDefaulttemp()
    {
        $id = $_REQUEST['id'];

        if(isset($id) && !empty($id)){
           
             //echo 'id= '.$id; exit;          

            //if (($templateModel = TblNewsletterTemplates::findOne($id)) !== null) {

            if (($templateModel = TblNewsletterTemplates::find()->where(['temp_type'=>$id])->limit(1)->all()) !== null) {

                //echo '<pre>'; print_r($templateModel); exit;

                //echo $templateModel[0]->temp_body; exit;
                return $templateModel[0]->temp_body;
            }
        }
    }//actionTemplate


    public function actionCkeditor_image_upload()
    {       
        $funcNum = $_REQUEST['CKEditorFuncNum'];

        $message = '';

        if($_FILES['upload']) {

          if (($_FILES['upload'] == "none") OR (empty($_FILES['upload']['name']))) {
          $message = Yii::t('app', "Please Upload an image.");
          }

          else if ($_FILES['upload']["size"] == 0 OR $_FILES['upload']["size"] > 5*1024*1024)
          {
          $message = Yii::t('app', "The image should not exceed 5MB.");
          }

          else if ( ($_FILES['upload']["type"] != "image/jpg") 
                    AND ($_FILES['upload']["type"] != "image/jpeg") 
                    AND ($_FILES['upload']["type"] != "image/png"))
          {
          $message = Yii::t('app', "The image type should be JPG , JPEG Or PNG.");
          }

          else if (!is_uploaded_file($_FILES['upload']["tmp_name"])){

          $message = Yii::t('app', "Upload Error, Please try again.");
          }

          else {
            //you need this (use yii\db\Expression;) for RAND() method 
            $random = rand(0, 9876543210);

            $extension = pathinfo($_FILES['upload']['name'], PATHINFO_EXTENSION);

            //Rename the image here the way you want
            $name = date("m-d-Y-h-i-s", time())."-".$random.'.'.$extension; 

            // Here is the folder where you will save the images
            $folder = 'uploads/ckeditor_images/';  

            $url = Yii::$app->urlManager->createAbsoluteUrl($folder.$name);

            move_uploaded_file( $_FILES['upload']['tmp_name'], $folder.$name );

          }

          echo '<script type="text/javascript">window.parent.CKEDITOR.tools.callFunction("'
               .$funcNum.'", "'.$url.'", "'.$message.'" );</script>';

        }

    }//actionCkeditor_image_upload


}
