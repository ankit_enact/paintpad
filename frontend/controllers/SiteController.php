<?php
namespace frontend\controllers;

use app\models\TblContacts;
use common\models\LoginForm;
use common\models\User;
use frontend\models\ContactForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use Yii;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;
use yii\web\Session;
use yii\widgets\ActiveForm;

/**
 * Site controller
 */
class SiteController extends Controller {
	/**
	 * {@inheritdoc}
	 */
	/*public function behaviors()
	{
	return [
	'access' => [
	'class' => AccessControl::className(),
	'only' => ['logout', 'signup'],
	'rules' => [
	[
	'actions' => ['signup'],
	'allow' => true,
	'roles' => ['?'],
	],
	[
	'actions' => ['logout'],
	'allow' => true,
	'roles' => ['@'],
	],
	],
	],
	'verbs' => [
	'class' => VerbFilter::className(),
	'actions' => [
	'logout' => ['post'],
	],
	],
	];
	}*/

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['logout', 'signup', 'about', 'dashboard'],
				'rules' => [
					[
						'actions' => ['signup'],
						'allow' => true,
						'roles' => ['?'],
					],
					[
						'actions' => ['logout', 'dashboard'],
						'allow' => true,
						'roles' => ['@'],
					],
					[
						'actions' => ['about'],
						'allow' => true,
						'roles' => ['@'],
						'matchCallback' => function ($rule, $action) {
							return User::isUserAdmin(Yii::$app->user->identity->username);
						},
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'logout' => ['post'],
				],
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function actions() {
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	/**
	 * Displays homepage.
	 *
	 * @return mixed
	 */
	public function actionIndex() {
		return $this->render('index');
	}

	/**
	 * Logs in a user.
	 *
	 * @return mixed
	 */
	public function actionLogin() {

		$this->layout = 'main_login';

		/*check if user close the browser*/

		$session = Yii::$app->session;
		// check if a session is already open
		if ($session->isActive) {

			$session->close();
			Yii::$app->user->logout();
		}

		/*check if user close the browser*/

		if (!Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new LoginForm();
		if ($model->load(Yii::$app->request->post()) && $model->login()) {
			//return $this->goBack();
			//echo "<pre>";print_r($model);exit;
			return $this->redirect(['site/dashboard']);
		} else {
			$model->password = '';

			return $this->render('login', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Logs out the current user.
	 *
	 * @return mixed
	 */
	public function actionLogout() {
		Yii::$app->user->logout();

		return $this->goHome();
	}

	/**
	 * Displays contact page.
	 *
	 * @return mixed
	 */
	public function actionContact() {
		$model = new ContactForm();
		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
				Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
			} else {
				Yii::$app->session->setFlash('error', 'There was an error sending your message.');
			}

			return $this->refresh();
		} else {
			return $this->render('contact', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Displays about page.
	 *
	 * @return mixed
	 */
	public function actionAbout() {
		$this->layout = 'main_sidebar';

		return $this->render('about');
	}

	/**
	 * Signs user up.
	 *
	 * @return mixed
	 */
	public function actionSignup() {
		$model = new SignupForm();
		if ($model->load(Yii::$app->request->post())) {
			if ($user = $model->signup()) {
				if (Yii::$app->getUser()->login($user)) {
					return $this->goHome();
				}
			}
		}

		return $this->render('signup', [
			'model' => $model,
		]);
	}

	/**
	 * Requests password reset.
	 *
	 * @return mixed
	 */
	public function actionRequestPasswordReset() {
		$model = new PasswordResetRequestForm();
		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			if ($model->sendEmail()) {
				Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

				return $this->goHome();
			} else {
				Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
			}
		}

		return $this->render('requestPasswordResetToken', [
			'model' => $model,
		]);
	}

	/**
	 * Resets password.
	 *
	 * @param string $token
	 * @return mixed
	 * @throws BadRequestHttpException
	 */
	public function actionResetPassword($token) {
		try {
			$model = new ResetPasswordForm($token);
		} catch (InvalidParamException $e) {
			throw new BadRequestHttpException($e->getMessage());
		}

		if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
			Yii::$app->session->setFlash('success', 'New password saved.');

			return $this->goHome();
		}

		return $this->render('resetPassword', [
			'model' => $model,
		]);
	}

	public function actionDashboard() {

		$contacts = TblContacts::listSubscriberContacts();
		return $this->render('dashboard', ['contacts' => $contacts]);

	} //actionLoader

	public function actionValidateForm() {
		Yii::$app->response->format = Response::FORMAT_JSON;

		$model = new TblContacts();
		$model->load(Yii::$app->request->post());
		return ActiveForm::validate($model);
	}

	public function actionUpload() {
		if (isset($_POST)) {
			$files = $this->SaveTempAttachments($_FILES);
			$result = ['attachment' => $files];
			Yii::$app->response->format = trim(Response::FORMAT_JSON);
			return $result;
		}

	}

	function SaveTempAttachments($attachments) {
		$files = "";
		$allwoedFiles = ['jpg', 'gif', 'png', 'doc', 'docx', 'pdf', 'xlsx', 'rar', 'zip', 'xlsx', 'xls', 'txt', 'csv', 'rtf', 'one', 'pptx', 'ppsx', 'pot'];

		if (!empty($attachments)) {
			if (count($attachments['attachment']['name']) > 0) {
				//Loop through each file
				for ($i = 0; $i < count($attachments['attachment']['name']); $i++) {
					//Get the temp file path
					$tmpFilePath = $attachments['attachment']['tmp_name'][$i];

					//Make sure we have a filepath
					if ($tmpFilePath != "") {
						//save the filename
						$shortname = $attachments['attachment']['name'][$i];
						$size = $attachments['attachment']['size'][$i];
						$ext = substr(strrchr($shortname, '.'), 1);
						if (in_array($ext, $allwoedFiles)) {
							//save the url and the file
							$newFileName = Yii::$app->security->generateRandomString(40) . "." . $ext;
							//Upload the file into the temp dir
							/*if (move_uploaded_file($tmpFilePath, Helper::UPLOAD_FOLDER . '/' . Helper::TEMP_FOLDER . '/' . $newFileName)) {
							$files[] =['fileName'=>$newFileName,'type'=>$ext,'size'=>(($size/1000)),'originalName'=>$shortname];
							}*/

							$uploads = 'attachments';

							if (move_uploaded_file($tmpFilePath, $uploads . '/' . $newFileName)) {
								$files[] = ['fileName' => $newFileName, 'type' => $ext, 'size' => (($size / 1000)), 'originalName' => $shortname];
							}

						}
					}
				}
			}

		}
		return $files;
	}

}
