<?php

namespace frontend\controllers;

use Yii;
use app\models\UserDetails;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\Model;
use yii\filters\auth\QueryParamAuth;
use yii\web\UploadedFile;
use yii\helpers\Url;

use common\models\LoginForm;
use common\models\SignupForm;
use common\models\User;
use app\models\TblUsers;
use app\models\TblUsersDetails;
use app\models\TblContacts;
use app\models\TblAddress;
use app\models\TblQuotes;
use app\models\TblCommunications;
use app\models\TblAppointments;
use app\models\TblAppointmentType;
use app\models\TblMembers;
use app\models\TblInvoices;
use app\models\TblBrands;
use app\models\TblTiers;
use app\models\TblStrengths;
use app\models\TblSheen;
use app\models\TblColorTags;
use app\models\TblColors;
use app\models\TblComponents;
use app\models\TblComponentGroups;
use app\models\TblComponentType;
use app\models\TblSpecialItems;
use app\models\TblRoomTypes;
use app\models\TblNotes;
use app\models\TblBrandPref;
use app\models\TblPaintDefaults;

use app\models\TblRooms;
use app\models\TblRoomPref;
use app\models\TblRoomCompPref;
use app\models\TblRoomDimensions;
use app\models\TblRoomSpecialItems;
use app\models\TblRoomNotes;


use app\models\TblProductStock;

use app\models\TblPaymentSchedule;
use app\models\TblSummary;
use app\models\TblQuoteSpecialItems;

use app\models\TblUsersSettings;
use app\models\TblPrepLevel;

use app\models\TblPaymentMethod;
use app\models\TblPaymentOptions;


class WebserviceController extends Controller
{

    /**
     * @inheritdoc
     */


    public $totals;


    public function behaviors(){

		return [
		    'verbs' => [
		        'class' => VerbFilter::className(),
		        'actions' => [
		            'login'                => ['POST'],
		            'create-contact'       => ['POST','GET'], //createContact
		            'contacts'             => ['GET'],
		            'contact-details'      => ['GET'],	
		            'create-quote'         => ['POST'],	//createQuote
		            'communication'        => ['GET'],
		            'appointments'         => ['GET'],		 
		            'create-appointment'   => ['POST'], 
		            'get-appointment-init' => ['GET'],
		            'update-contact'       => ['POST'],	//updateContact
		            'invoices'             => ['GET'],
		            'update-appointment'   => ['POST'],
		            'quotes'               => ['GET'],
		            'brands'               => ['GET'],
		            'tiers'                => ['GET'],
					'tiers-component'      => ['GET'],
		            'product-colors'       => ['GET'],
		            'components'           => ['GET'],
		            'special-items'        => ['GET'],
		            'rooms'                => ['GET'],
		            'tags'                 => ['GET'],
		            'create-note'          => ['POST'],
		            'update-quote'         => ['POST'],	//updateQuote
		            'update-status'        => ['POST'],
		            'generate-pdf'         => ['POST'],
		            'generate-jsspdf'      => ['POST'],
		            'save-paint-defaults'  => ['POST'],	//savePaintDefaults
		            'get-paint-defaults'   => ['POST'],
		            'save-room-details'    => ['POST'], //saveRoomDetails
		            'upload-images'		   => ['POST'],
		            'get-room-details'	   => ['POST'],	//getRoomDetails
		            'delete-room'		   => ['POST'], //deleteRoom
		            'save-quote-notes'	   => ['POST'],
		            'get-quote-notes'	   => ['POST'],
		            'get-room-quantities'  => ['POST'], //GetRoomQuantities
		            'get-room-qty'  => ['POST','GET'],
		            'delete-quote-notes'   => ['POST'],

		        	'get-quote-summary' => ['POST'],	//GetQuoteSummary
		        	'save-quote-summary' => ['POST'],	//SaveQuoteSummary
		        	'save-quote-specialitems' => ['POST'], //saveQuoteSpecialItems
		        	'get-quote-specialitems' => ['POST'],

					'generate-quotepdf-web' => ['POST'],
					'generate-jsspdf-web' => ['POST'],	//GenerateJsspdfWeb

					'get-payment-methods' => ['GET'],
					'get-payment-options' => ['GET'],	 //GetPaymentOptions

					'save-pricing' => ['POST'],	 //SavePricing
					'status-room' => ['POST'],	 //StatusRoom

					'get-quote-room-status' => ['POST'],	 //getQuoteRoomStatus
					'save-quote-room-status' => ['POST'],	 //saveQuoteRoomStatus
		        ],
		    ],
		    /*'authenticator' =>[
		    	'class'=> QueryParamAuth::className(),
		    ],*/
		];
    }

    public function actionLogin()
    {
	    //echo "<pre>"; print_r($_POST); exit;

	    $email = !empty(Yii::$app->request->post('email'))?Yii::$app->request->post('email'):'';
	    $password = !empty(Yii::$app->request->post('pwd'))?Yii::$app->request->post('pwd'):'';
	    $response = [];

	    // validate
	    if(empty($email) || empty($password)){
	      $response = [
	        'success' => '0',
	        'message' => 'Email & Password cannot be blank!',
	        'data' => '',
	      ];
	    }
	    else{
	        // search in the database, there is no email referred
	        $user = \common\models\User::findByEmail($email);
	        // if the email exists then
	        if(!empty($user)){
	          // check, valid not password, if valid then make response success
	          if($user->validatePassword($password)){

				$model = TblUsersDetails::find()
				        ->joinWith('user')
				        ->joinWith('address')		        		       
				        ->where([ `TblUsers`.'email' => $email])
				        ->all();

				//echo "<pre>"; print_r($model); exit;       

				$uArr = array(); 

				$arr = array('setting'=>(object) array());

				foreach ($model as $user) {

					$uArr['subscriber_id'] = $user['user_id'];
					$uArr['name']          = $user['name'];
					$uArr['image']         = $user['logo'];

				    // get data from User relation model
				    $email = $user->user->email;

				    // get data from Address relation model
				    $street1 = $user->address->street1;
				    $street2 = $user->address->street2;
				    $suburb  = $user->address->suburb;


				    $uArr['contact']['email']   = $email;
					$uArr['contact']['link']    = $user['website_link'];
					$uArr['contact']['phone']   = $user['phone'];
					$uArr['contact']['address'] = $street1 . ' ' . $street2 . ' ' . $suburb;

					$uArr['counts']['jss']      = 10;
					$uArr['counts']['contacts'] = 15;
					$uArr['counts']['quotes']   = 8;
					$uArr['counts']['docs']     = 12;

					$uArr['settings']           = (object) array();
					$uArr['prep']               = (object) array();
					$uArr['spl_items']          = (object) array();


				}

				$response = [
					'success' => '1',
					'message' => 'Login successful!',
					'data' => $uArr,
				];


	          }
	          // If the password is wrong then make a response like this
	          else{
	            $response = [
	              'success' => '0',
	              'message' => 'Password incorrect!',
	              'data' => '',
	            ];
	          }
	        }
	        // If the email is not found make a response like this
	        else{
	          $response = [
	            'success' => '0',
	            'message' => 'Email doesnot exist in database!',
	            'data' => '',
	          ];
	        }
	    }
	    //return $response;
	    ob_start();
	    echo json_encode($response);
	}


	public function actionCreateContact()
    {
    	$siteURL = Url::base(true). '/uploads';
		
		
		//echo "<pre>";print_r($_POST);exit;

    	/* mandaory fields */
	    $sub_id  = !empty(Yii::$app->request->post('subscriber_id'))?Yii::$app->request->post('subscriber_id'):'';
	    $name    = !empty(Yii::$app->request->post('name'))?Yii::$app->request->post('name'):'';
	    $email   = !empty(Yii::$app->request->post('email'))?Yii::$app->request->post('email'):'';
		$phone   = !empty(Yii::$app->request->post('phone'))?Yii::$app->request->post('phone'):'';
		$force   = '';
		if(isset($_POST['force'])){
			$force   = Yii::$app->request->post('force');
		}

	    /* optional fields */
	    $street1 = !empty(Yii::$app->request->post('street1'))?Yii::$app->request->post('street1'):'';
	    $street2 = !empty(Yii::$app->request->post('street2'))?Yii::$app->request->post('street2'):'';
	    $suburb  = !empty(Yii::$app->request->post('suburb'))?Yii::$app->request->post('suburb'):'';
	    $state   = !empty(Yii::$app->request->post('state'))?Yii::$app->request->post('state'):'';
		$postal  = !empty(Yii::$app->request->post('postal'))?Yii::$app->request->post('postal'):'';
		$country = !empty(Yii::$app->request->post('country'))?Yii::$app->request->post('country'):'';
		$frm_add = !empty(Yii::$app->request->post('formatted_addr'))?Yii::$app->request->post('formatted_addr'):'';
		$lat     = !empty(Yii::$app->request->post('lat'))?Yii::$app->request->post('lat'):'';
		$long    = !empty(Yii::$app->request->post('long'))?Yii::$app->request->post('long'):'';

		$uploads = UploadedFile::getInstancesByName("image");

	    $response = [];

	    $arr = array();

	    // validate
	    if(empty($sub_id) || empty($name) || empty($email) || empty($phone) || $force==""){
	      $response = [
	        'success' => 0,
	        'message' => 'Fields cannot be blank!',
	        'data' => [],
	      ];
	    }
	    else{

	        // search in the database, there is no email referred
	        $user = \common\models\User::findIdentity($sub_id);

	        // if the email exists then
	        if(!empty($user)){

				date_default_timezone_set("UTC");
				$milliseconds = strtotime(gmdate("Y-m-d H:i:s"));

		    	$addressModel = new TblAddress();
				$contactModel = new TblContacts();


				$contact = $contactModel::findByEmail($email);


				if($contact && $force == 0){
					$response = [
						'success' => 2,
						'message' => 'This Email already exists. Do you want to add contact with duplicate email?',
						'data' => [],
					];
				}else{

				/* Insert data in Address table */

			        $addressModel->street1           = $street1;
			        $addressModel->street2           = $street2;
			        $addressModel->suburb            = $suburb;
			        $addressModel->client_state      = $state;
			        $addressModel->country_id        = $country;
			        $addressModel->postal_code       = $postal;
			        $addressModel->formatted_address = $frm_add;
			        $addressModel->lat               = $lat;
			        $addressModel->lng               = $long;
			        $addressModel->created_at        = $milliseconds;
			        $addressModel->updated_at        = $milliseconds;


			        if($addressModel->save(false)){

			        	$addId = $addressModel->address_id;

			        	$image = '';

			        	if(!empty($uploads)){

							foreach ($uploads as $file){					    		

						        if(isset($file->size)){		           
						            $varr = time().rand(1,100);
						            $file->saveAs('uploads/' . $file->baseName . '_' . $varr .'.' . $file->extension);
						        }

						        $image = $file->baseName . '_' . $varr .'.' . $file->extension;

							} //foreach
			        	}



						/* Insert data in Contact table */	

			        		$contactModel->subscriber_id = $sub_id;
			        		$contactModel->name          = $name;
			        		$contactModel->email         = $email;
			        		$contactModel->phone         = $phone;
			        		$contactModel->image         = $image;
			        		$contactModel->address_id    = $addId;
			        		$contactModel->created_at    = $milliseconds;
			        		$contactModel->updated_at    = $milliseconds;

			        		if($contactModel->save(false)){

								$contactId = $contactModel->contact_id;

							    	$arr['contact_id']     = $contactId;
							    	$arr['name']           = $name;
							    	$arr['email']          = $email;
							    	$arr['phone']          = $phone;
							    	$arr['street1']        = $street1;
							    	$arr['street2']        = $street2;
							    	$arr['suburb']         = $suburb;
							    	$arr['state']          = $state;
							    	$arr['postal']         = $postal;
							    	$arr['country']        = $country;
							    	$arr['formatted_addr'] = $frm_add;
							    	$arr['lat']            = $lat;
							    	$arr['lng']            = $long;
							    	$arr['image']          = $siteURL.'/'.$image;

								      $response = [
								        'success' => 1,
								        'message' => 'Contact data saved successfully!',
								        'data' => $arr,
								      ];

							}
			        }			

				}

	        }
	        // If the email is not found make a response like this
	        else{
	          $response = [
	            'success' => 0,
	            'message' => 'Subscriber doesnot exist in database!',
	            'data' => [],
	          ];
	        }

	    }

	    ob_start();
	    echo json_encode($response);    	

    }//actionCreateContact


	public function actionContacts()
    {

    	/* mandaory fields */
	    $sub_id  = !empty($_GET['subscriber_id'])?$_GET['subscriber_id']:'';

	    //echo 'here-'.$sub_id; exit;    

	    $response = [];

	    $arr = array();

	    // validate
	    if(empty($sub_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Fields cannot be blank!',
	        'data' => '',
	      ];
	    }
	    else{

	        // search in the database, there is no email referred
	        $subscriber = \common\models\User::findIdentity($sub_id);

	        if(!empty($subscriber)){

	        	$siteURL = Url::base(true). '/uploads';

				$model = TblContacts::find()
				        ->joinWith('address')       		       
				        ->where([ 'subscriber_id' => $sub_id])
				        ->all();

				//echo "<pre>"; print_r($model); exit;        

				$newarr = array();

				foreach($model as $result){

					$newarr['contact_id'] = $result->contact_id;
					$newarr['name']       = $result->name;
					$newarr['phone']      = $result->phone;
					$newarr['image']      = $siteURL . '/' . $result->image;
					$newarr['email']      = $result->email;

						if(isset($result->address)){

						    // get data from Address relation model
						    $street1        = $result->address->street1;
						    $street2        = $result->address->street2;
						    $suburb         = $result->address->suburb;
						    $state          = $result->address->client_state;
						    $postal         = $result->address->postal_code;
						    $country        = $result->address->country_id;
						    $formatted_addr = $result->address->formatted_address;
						    $lat            = $result->address->lat;
						    $lng            = $result->address->lng;
						    $is_checked     = $result->address->is_checked;

								$newarr['address'] = $street1 . ' ' . $street2 . ' ' . $suburb . ' ' . $state . ' ' . $postal . '' . $country;   

								$newarr['lat']     = $lat;
								$newarr['lng']     = $lng;
								$newarr['country']        = $country;
								$newarr['formatted_addr'] = $formatted_addr;
								$newarr['postal']         = $postal;
								$newarr['state']          = $state;
								$newarr['street1']        = $street1;
								$newarr['street2']        = $street2;
								$newarr['suburb']         = $suburb;
								$newarr['s_checked']      = $is_checked;

						}else{

							$newarr['address']        = "";  
							$newarr['lat']            = "";  
							$newarr['lng']            = "";   
							$newarr['country']        = "";   
							$newarr['formatted_addr'] = "";
							$newarr['postal']     	   = "";
							$newarr['state']          = "";
							$newarr['street1']        = "";
							$newarr['street2']        = "";
							$newarr['suburb']         = "";
							$newarr['lng']            = "";
						}

					 $arr['contact'][] = $newarr;
				}			
				 
				
		          $response = [
		            'success' => '1',
		            'message' => 'Contacts Data!',
		            'data' => $arr,
		          ];

	        }
	        // If the email is not found make a response like this
	        else{
	          $response = [
	            'success' => '0',
	            'message' => 'Subscriber doesnot exist in database!',
	            'data' => '',
	          ];
	        }	

	    }

	    ob_start();
	    echo json_encode($response);    	

    }//actionContacts


	public function actionContactDetails()
    {

    	/* mandaory fields */
	    $sub_id  = !empty($_GET['subscriber_id'])?$_GET['subscriber_id']:'';
	    $con_id  = !empty($_GET['contact_id'])?$_GET['contact_id']:'';

	    //echo 'here-'.$sub_id; exit;    

	    $response = [];

	    $arr = array();

	    // validate
	    if(empty($sub_id) || empty($con_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Fields cannot be blank!',
	        'data' => '',
	      ];
	    }
	    else{

	        // search in the database, there is no email referred
	        $subscriber = \common\models\User::findIdentity($sub_id);

	        if(!empty($subscriber)){

	        	$siteURL = Url::base(true). '/uploads';

				$model = TblContacts::find()						
				        ->joinWith('address')
				        ->joinWith(['tblQuotes' => function($q){ $q->joinWith('siteAddress')->orderBy('quote_id DESC')->limit(2)->offset(0);}])
				        ->where([ 'tbl_contacts.subscriber_id' => $sub_id])
				        ->andWhere(['tbl_contacts.contact_id'=>$con_id])
				        ->orderBy('quote_id DESC')
				        ->all();

					//echo "<pre>"; print_r($model); exit;        

					$uArr = array(); 

					foreach($model as $contact){

					    $name  = $contact['name'];
					    $cId   = $contact['contact_id'];
						$email = $contact['email'];
						$phone = $contact['phone'];
						$image = $siteURL . '/' . $contact['image'];


					    // get data from Address relation model
					    $street1        = $contact->address->street1;
					    $street2        = $contact->address->street2;
					    $suburb         = $contact->address->suburb;
					    $state          = $contact->address->client_state;
					    $postal         = $contact->address->postal_code;
					    $country        = $contact->address->country_id;
					    $formatted_addr = $contact->address->formatted_address;
					    $lat            = $contact->address->lat;
					    $lng            = $contact->address->lng;			
					    $is_checked     = $contact->address->is_checked;			



						if(isset($contact->tblQuotes) && !empty($contact->tblQuotes) ){

								$qArr = array();

								foreach($contact->tblQuotes as $quotes){
									
									$qArr['contact']['name']    = $name;
									$qArr['contact']['phone']   = $phone;	
									$qArr['contact']['email']   = $email;
									$qArr['contact']['image']   = $image;
									$qArr['contact']['formatted_addr'] = $formatted_addr;	
									$qArr['contact']['lat']     = $lat;
									$qArr['contact']['lng']     = $lng;
									$qArr['contact']['country'] = $country;	
									$qArr['contact']['postal']  = $postal;
									$qArr['contact']['state']   = $state;
									$qArr['contact']['street1'] = $street1;	
									$qArr['contact']['street2'] = $street2;
									$qArr['contact']['suburb']  = $suburb;	


										$qArr['date']        = $quotes->created_at;
										$qArr['description'] = $quotes->description;
										$qArr['note']        = $quotes->note;
										$qArr['price']       = $quotes->price;
										$qArr['quote_id']    = $quotes->quote_id;
										$qArr['status']      = $quotes->status;			
										$qArr['type']        = $quotes->type;


										if(isset($quotes->siteAddress) && !empty($quotes->siteAddress)){

											$qArr['site']['name']    = $name;
											$qArr['site']['phone']   = $phone;	
											$qArr['site']['email']   = $email;	

											$qArr['site']['formatted_addr'] = $quotes->siteAddress->formatted_address;	
											$qArr['site']['lat']     = $quotes->siteAddress->lat;
											$qArr['site']['lng']     = $quotes->siteAddress->lng;
											$qArr['site']['country'] = $quotes->siteAddress->country_id;
											$qArr['site']['postal']  = $quotes->siteAddress->postal_code;
											$qArr['site']['state']   = $quotes->siteAddress->client_state;
											$qArr['site']['street1'] = $quotes->siteAddress->street1;
											$qArr['site']['street2'] = $quotes->siteAddress->street2;
											$qArr['site']['suburb']  = $quotes->siteAddress->suburb;
											$qArr['site']['s_checked'] = $quotes->siteAddress->is_checked;

										}else{

											$qArr['site']['name']    = $name;
											$qArr['site']['phone']   = $phone;	
											$qArr['site']['email']   = $email;											
											$qArr['site']['formatted_addr'] = $formatted_addr;	
											$qArr['site']['lat']     = $lat;
											$qArr['site']['lng']     = $lng;
											$qArr['site']['country'] = $country;	
											$qArr['site']['postal']  = $postal;
											$qArr['site']['state']   = $state;
											$qArr['site']['street1'] = $street1;	
											$qArr['site']['street2'] = $street2;
											$qArr['site']['suburb']  = $suburb;	

										}

										$qArr['counts']['jss'] = 10;
										$qArr['counts']['contacts'] = 15;
										$qArr['counts']['quotes'] = 8;
										$qArr['counts']['docs'] = 12;

									$finalArr[] = $qArr;
							}

						}else{
								$finalArr = [];
						}


					} //foreach

							$uArr['contact']['name'] = $name;
							$uArr['contact']['email'] = $email;
							$uArr['contact']['phone'] = $phone;
							$uArr['contact']['street1'] = $street1;
							$uArr['contact']['street2'] = $street2;
							$uArr['contact']['suburb'] = $suburb;
							$uArr['contact']['state'] = $state;
							$uArr['contact']['postal'] = $postal;
							$uArr['contact']['country'] = $country;
							$uArr['contact']['formatted_addr'] = $formatted_addr;
							$uArr['contact']['lat'] = $lat;
							$uArr['contact']['lng'] = $lng;
							$uArr['contact']['image'] = $image;
							$uArr['contact']['contact_id'] = $con_id;

							$uArr['counts']['jss'] = 10;
							$uArr['counts']['contacts'] = 15;
							$uArr['counts']['quotes'] = 8;
							$uArr['counts']['docs'] = 12;

							$uArr['quotes'] = $finalArr;				 
								
						          $response = [
						            'success' => '1',
						            'message' => 'Contacts Details!',
						            'data' => $uArr,
						          ];

	        }
	        // If the email is not found make a response like this
	        else{
	          $response = [
	            'success' => '0',
	            'message' => 'Subscriber doesnot exist in database!',
	            'data' => '',
	          ];
	        }	

	    }

	    ob_start();
	    echo json_encode($response);    	

    }//actionContactDetails


	public function actionCreateQuote()
    {

    	$siteURL = Url::base(true). '/uploads';

    	/* mandaory fields */
	    $cnt_id  = !empty(Yii::$app->request->post('contact_id'))?Yii::$app->request->post('contact_id'):'';
	    $sub_id  = !empty(Yii::$app->request->post('subscriber_id'))?Yii::$app->request->post('subscriber_id'):'';

	    /* optional fields */
		$description      = !empty(Yii::$app->request->post('description'))?Yii::$app->request->post('description'):'';
		$client_name      = !empty(Yii::$app->request->post('client_name'))?Yii::$app->request->post('client_name'):'';
	    $s_name           = !empty(Yii::$app->request->post('s_name'))?Yii::$app->request->post('s_name'):'';
	    $s_email          = !empty(Yii::$app->request->post('s_email'))?Yii::$app->request->post('s_email'):'';
	    $s_phone          = !empty(Yii::$app->request->post('s_phone'))?Yii::$app->request->post('s_phone'):'';
	    $s_street1        = !empty(Yii::$app->request->post('s_street1'))?Yii::$app->request->post('s_street1'):'';
	    $s_street2        = !empty(Yii::$app->request->post('s_street2'))?Yii::$app->request->post('s_street2'):'';
	    $s_suburb         = !empty(Yii::$app->request->post('s_suburb'))?Yii::$app->request->post('s_suburb'):'';
	    $s_state          = !empty(Yii::$app->request->post('s_state'))?Yii::$app->request->post('s_state'):'';
	    $s_postal         = !empty(Yii::$app->request->post('s_postal'))?Yii::$app->request->post('s_postal'):'';
	    $s_country        = !empty(Yii::$app->request->post('s_country'))?Yii::$app->request->post('s_country'):'';
	    $s_formatted_addr = !empty(Yii::$app->request->post('s_formatted_addr'))?Yii::$app->request->post('s_formatted_addr'):'';
	    $s_lat            = !empty(Yii::$app->request->post('s_lat'))?Yii::$app->request->post('s_lat'):'';
		$s_long           = !empty(Yii::$app->request->post('s_long'))?Yii::$app->request->post('s_long'):'';
		$s_checked        = !empty(Yii::$app->request->post('s_checked'))?Yii::$app->request->post('s_checked'):0;


		$type             = !empty(Yii::$app->request->post('type'))?Yii::$app->request->post('type'):'';

	    $response = [];

        $uArr = array();

	    // validate
	    if(empty($cnt_id) || empty($sub_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Fields cannot be blank!',
	        'data' => [],
	      ];
	    }
	    else{
		//echo $s_checked." ghj"; exit;
	        // search in the database, there is no email referred
	        $subscriber = \common\models\User::findIdentity($sub_id);

	        if(!empty($subscriber)){

				date_default_timezone_set("UTC");
				$milliseconds = strtotime(gmdate("Y-m-d H:i:s"));
	        	/* insert in DB */

        		$model = TblContacts::find()	        				   						
	        				->joinWith('address') 		        							
    						->where(['contact_id' => $cnt_id])		    						
    						->all();

	        	$quote_id = '';

	        	if( (!empty($s_street1) && $s_street1 != '') || (!empty($s_street2) && $s_street2 != '') || (!empty($s_suburb) && $s_suburb != '')  || (!empty($s_state) && $s_state != '') || (!empty($s_postal) &&    $s_postal != '') || (!empty($s_country) && $s_country != '') || (!empty($s_formatted_addr) && $s_formatted_addr != '') || (!empty($s_lat) && $s_lat != '') || (!empty($s_long) && $s_long != '') || (!empty($s_checked)) ){

				    $cname  = ($s_name  != '') ? $s_name  : $model[0]['name'];
					$cemail = ($s_email != '') ? $s_email : $model[0]['email'];
					$cphone = ($s_phone != '') ? $s_phone : $model[0]['phone'];

	        		$addressModel = new TblAddress();

			        $addressModel->street1           = $s_street1;
			        $addressModel->street2           = $s_street2;
			        $addressModel->suburb            = $s_suburb;
			        $addressModel->client_state      = $s_state;
			        $addressModel->country_id        = $s_country;
			        $addressModel->postal_code       = $s_postal;
			        $addressModel->formatted_address = $s_formatted_addr;
			        $addressModel->lat               = $s_lat;
			        $addressModel->lng               = $s_long;
			        $addressModel->s_name            = $cname;
			        $addressModel->s_email           = $cemail;
			        $addressModel->s_phone           = $cphone;
			        $addressModel->is_checked        = $s_checked;
			        $addressModel->created_at        = $milliseconds;
			        $addressModel->updated_at        = $milliseconds;

				        if($addressModel->save(false)){

				        	$addId = $addressModel->address_id;

					        	$quotesModel = new TblQuotes();

					        	$quotesModel->contact_id       = $cnt_id;
					        	$quotesModel->subscriber_id    = $sub_id;
					        	$quotesModel->description      = $description;
					        	$quotesModel->type             = $type;
					        	$quotesModel->site_address_id  = $addId;	
					        	$quotesModel->contact_email    = $s_email;
					        	$quotesModel->contact_name     = $s_name;
								$quotesModel->contact_number   = $s_phone;
								$quotesModel->client_name	   = $client_name;
								$quotesModel->price			   = 0;

						        $quotesModel->created_at       = $milliseconds;
						        $quotesModel->updated_at       = $milliseconds;

						        if($quotesModel->save(false)){

						        	$quote_id = $quotesModel->quote_id;


										//echo "<pre>"; print_r($model); exit;

					    				foreach($model as $contact){
					    					
										    $name  = $contact['name'];
											$email = $contact['email'];
											$phone = $contact['phone'];
											$image = $siteURL . '/' . $contact['image'];

										    // get data from Address relation model
										    $street1        = $contact->address->street1;
										    $street2        = $contact->address->street2;
										    $suburb         = $contact->address->suburb;
										    $state          = $contact->address->client_state;
										    $postal         = $contact->address->postal_code;
										    $country        = $contact->address->country_id;
										    $formatted_addr = $contact->address->formatted_address;
										    $lat            = $contact->address->lat;
										    $lng            = $contact->address->lng;

					    					$uArr['quote_id']    = $quote_id;
					    					$uArr['description'] = $description;
					    					$uArr['status']      = 0;
					    					$uArr['type']        = $type;

					    					/* user details */
											$uArr['contact']['name']           = $name;
											$uArr['contact']['email']          = $email;
											$uArr['contact']['phone']          = $phone;
											$uArr['contact']['street1']        = $street1;
											$uArr['contact']['street2']        = $street2;
											$uArr['contact']['suburb']         = $suburb;
											$uArr['contact']['state']          = $state;
											$uArr['contact']['postal']         = $postal;
											$uArr['contact']['country']        = $country;
											$uArr['contact']['formatted_addr'] = $formatted_addr;
											$uArr['contact']['lat']            = $lat;
											$uArr['contact']['lng']            = $lng;
											$uArr['contact']['image']          = $image;
											$uArr['contact']['contact_id']     = $cnt_id;

											/* site details */
											$uArr['site']['name']           = $cname;
											$uArr['site']['email']          = $cemail;
											$uArr['site']['phone']          = $cphone;
											$uArr['site']['street1']        = $s_street1;
											$uArr['site']['street2']        = $s_street2;
											$uArr['site']['suburb']         = $s_suburb;
											$uArr['site']['state']          = $s_state;
											$uArr['site']['postal']         = $s_postal;
											$uArr['site']['country']        = $s_country;
											$uArr['site']['formatted_addr'] = $s_formatted_addr;
											$uArr['site']['lat']            = $s_lat;
											$uArr['site']['lng']            = $s_long;
											$uArr['site']['s_checked']      = $s_checked;
											
											/* counts */
											$uArr['counts']['jss']      = 10;
											$uArr['counts']['contacts'] = 15;
											$uArr['counts']['quotes']   = 8;
											$uArr['counts']['docs']     = 12;

					    				}//foreach

						        }
						}

		        	}else{

								$model = TblContacts::find()	        				   						
										->joinWith('address') 		        							
										->where(['contact_id' => $cnt_id])		    						
										->all();

								//echo '<pre>'; print_r($model); exit;
								/*echo 'here'.$model[0]->address->address_id;
								exit;*/

								$cname  = $model[0]['name'];
								$cemail = $model[0]['email'];
								$cphone = $model[0]['phone'];

								
							    $sstreet1        = $model[0]->address->street1;
							    $sstreet2        = $model[0]->address->street2;
							    $ssuburb         = $model[0]->address->suburb;
							    $sstate          = $model[0]->address->client_state;
							    $spostal         = $model[0]->address->postal_code;
							    $scountry        = $model[0]->address->country_id;
							    $sformatted_addr = $model[0]->address->formatted_address;
							    $slat            = $model[0]->address->lat;
							    $slng            = $model[0]->address->lng;

					        		$addressModel = new TblAddress();

							        $addressModel->street1           = $sstreet1;
							        $addressModel->street2           = $sstreet2;
							        $addressModel->suburb            = $ssuburb;
							        $addressModel->client_state      = $sstate;
							        $addressModel->country_id        = $scountry;
							        $addressModel->postal_code       = $spostal;
							        $addressModel->formatted_address = $sformatted_addr;
							        $addressModel->lat               = $slat;
							        $addressModel->lng               = $slng;
							        $addressModel->s_name            = $cname;
							        $addressModel->s_email           = $cemail;
							        $addressModel->s_phone           = $cphone;
							        $addressModel->created_at        = $milliseconds;
							        $addressModel->updated_at        = $milliseconds;

								        if($addressModel->save(false)){
								        	$addrId = $addressModel->address_id;
								        }

					        	$quotesModel = new TblQuotes();

					        	$quotesModel->contact_id          = $cnt_id;
					        	$quotesModel->subscriber_id       = $sub_id;
					        	$quotesModel->description         = $description;
					        	$quotesModel->type                = $type;
					        	$quotesModel->site_address_id     = $addrId;	
					        	$quotesModel->contact_email       = $cemail;
					        	$quotesModel->contact_name        = $cname;
					        	$quotesModel->contact_number      = $cphone;

						        $quotesModel->created_at          = $milliseconds;
						        $quotesModel->updated_at          = $milliseconds;

						        if($quotesModel->saved(false)){

					        		$quote_id = $quotesModel->quote_id;

						        		/*$model = TblContacts::find()	        				   						
							        				->joinWith('address') 		        							
						    						->where(['contact_id' => $cnt_id])
						    						->all();*/

											//echo "<pre>"; print_r($model); exit;

						    				foreach($model as $contact){
						    					
											    $name  = $contact['name'];
												$email = $contact['email'];
												$phone = $contact['phone'];
												$image = $siteURL . '/' . $contact['image'];

											    // get data from Address relation model
											    $street1        = $contact->address->street1;
											    $street2        = $contact->address->street2;
											    $suburb         = $contact->address->suburb;
											    $state          = $contact->address->client_state;
											    $postal         = $contact->address->postal_code;
											    $country        = $contact->address->country_id;
											    $formatted_addr = $contact->address->formatted_address;
											    $lat            = $contact->address->lat;
											    $lng            = $contact->address->lng;
											    $s_checked      = $contact->address->is_checked;

						    					$uArr['quote_id']    = $quote_id;
						    					$uArr['description'] = $description;
						    					$uArr['status']      = 0;
						    					$uArr['type']        = $type;

						    					/* user contact */
												$uArr['contact']['name'] 		   = $name;
												$uArr['contact']['email']          = $email;
												$uArr['contact']['phone']          = $phone;
												$uArr['contact']['street1']        = $street1;
												$uArr['contact']['street2']        = $street2;
												$uArr['contact']['suburb']         = $suburb;
												$uArr['contact']['state']          = $state;
												$uArr['contact']['postal'] 		   = $postal;
												$uArr['contact']['country'] 	   = $country;
												$uArr['contact']['formatted_addr'] = $formatted_addr;
												$uArr['contact']['lat'] 		   = $lat;
												$uArr['contact']['lng']            = $lng;
												$uArr['contact']['image']          = $image;
												$uArr['contact']['contact_id']     = $cnt_id;

												$uArr['site']['name']           = $name;
												$uArr['site']['email']          = $email;
												$uArr['site']['phone']          = $phone;
												$uArr['site']['street1']        = $street1;
												$uArr['site']['street2']        = $street2;
												$uArr['site']['suburb']         = $suburb;
												$uArr['site']['state']          = $state;
												$uArr['site']['postal']         = $postal;
												$uArr['site']['country']        = $country;
												$uArr['site']['formatted_addr'] = $formatted_addr;
												$uArr['site']['lat']            = $lat;
												$uArr['site']['lng']            = $lng;	
												$uArr['site']['s_checked']      = $s_checked;																					


												$uArr['counts']['jss']      = 10;
												$uArr['counts']['contacts'] = 15;
												$uArr['counts']['quotes']   = 8;
												$uArr['counts']['docs']     = 12;

						    				}//foreach
						        }

		        	}

				          $response = [
				            'success' => '1',
				            'message' => 'Quote successfully saved!',
				            'data' => $uArr,
				          ];

	        }
	        // If the email is not found make a response like this
	        else{
	          $response = [
	            'success' => '0',
	            'message' => 'Subscriber doesnot exist in database!',
	            'data' => '',
	          ];
	        }	

	  	}

	    ob_start();
	    echo json_encode($response);    

	} //actionCreateQuote

	public function actionCommunication(){

    	/* mandaory fields */
	    $sub_id  = !empty($_GET['subscriber_id'])?$_GET['subscriber_id']:'';
	    $cnt_id  = !empty($_GET['contact_id'])?$_GET['contact_id']:'';
	    $quoteId = !empty($_GET['quote_id'])?$_GET['quote_id']:'';

	    //echo 'here-'.$sub_id; exit;    

	    $response = [];

	    $arr = array();

	    // validate
	    if(empty($sub_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Fields cannot be blank!',
	        'data' => '',
	      ];
	    }
	    else{

	        // search in the database, there is no email referred
	        $subscriber = \common\models\User::findIdentity($sub_id);

	        if(!empty($subscriber)){

	        	/* fetch communication data here */      	
				/*if(!empty($cnt_id) && $cnt_id != ''){

					$commModel = TblCommunications::find()			      		       
								->where(['subscriber_id' => $subscriber])
								->where(['contact_id' => $cnt_id])
								->all();				
				}else{
					$commModel = TblCommunications::find()			      		       
								->where(['subscriber_id' => $subscriber])
								->all();
				}*/

				$commModel1 = TblCommunications::find()->where(['subscriber_id' => $subscriber]);

					if($cnt_id)	    				
					{
						$commModel1->andWhere(['contact_id'=>$cnt_id]);
					}
					if($quoteId)
					{
						$commModel1->andWhere(['quote_id'=>$quoteId]);
					}

						$commModel = $commModel1->all();



				if(count($commModel) > 0){
					//echo "<pre>"; print_r($commModel); exit;

					foreach($commModel as $communication){

						$arr['comm_id'] = $communication->comm_id;
						$arr['From']    = $communication->comm_from;
						$arr['date']    = (string)$communication->date;
						$arr['to']      = $communication->comm_to;
						$arr['cc']      = $communication->cc_email;
						$arr['bcc']     = $communication->bcc_email;
						$arr['subject'] = $communication->subject;
						$arr['body']    = strip_tags($communication->body);

						$finalArr[] = $arr;

					} //foreach

					$commArr['emails'] = $finalArr;

				}else{
					$commArr['emails'] = [];					
				}

			      $response = [
			        'success' => '1',
			        'message' => 'Communication Listing!',
			        'data' => $commArr,
			      ];

			}
	        // If the email is not found make a response like this
	        else{
	          $response = [
	            'success' => '0',
	            'message' => 'Subscriber doesnot exist in database!',
	            'data' => '',
	          ];
	        }			

	    }

	    ob_start();
	    echo json_encode($response);    

	} //actionCommunication


	public function actionCreateAppointment(){ 

    	/* mandaory fields */
	    $cnt_id  = !empty(Yii::$app->request->post('contact_id'))?Yii::$app->request->post('contact_id'):'';
	    $sub_id  = !empty(Yii::$app->request->post('subscriber_id'))?Yii::$app->request->post('subscriber_id'):'';
	    $date    = !empty(Yii::$app->request->post('date'))?Yii::$app->request->post('date'):'';
	    $note    = !empty(Yii::$app->request->post('note'))?Yii::$app->request->post('note'):'';
	    $type    = !empty(Yii::$app->request->post('type'))?Yii::$app->request->post('type'):'';
	    $mem_id  = !empty(Yii::$app->request->post('member_id'))?Yii::$app->request->post('member_id'):'';
	    $all_day = !empty(Yii::$app->request->post('allDay'))?Yii::$app->request->post('allDay'):'';

	    /* optional fields */
	    $duration = !empty(Yii::$app->request->post('duration'))?Yii::$app->request->post('duration'):'';
	    $scope    = !empty(Yii::$app->request->post('scope'))?Yii::$app->request->post('scope'):'';
	    $quote_id = !empty(Yii::$app->request->post('quote_id'))?Yii::$app->request->post('quote_id'):'';
	    $street1  = !empty(Yii::$app->request->post('street1'))?Yii::$app->request->post('street1'):'';
	    $street2  = !empty(Yii::$app->request->post('street2'))?Yii::$app->request->post('street2'):'';
	    $suburb   = !empty(Yii::$app->request->post('suburb'))?Yii::$app->request->post('suburb'):'';
	    $state    = !empty(Yii::$app->request->post('state'))?Yii::$app->request->post('state'):'';
	    $postal   = !empty(Yii::$app->request->post('postal'))?Yii::$app->request->post('postal'):'';
	    $country  = !empty(Yii::$app->request->post('country'))?Yii::$app->request->post('country'):'';
	    $frm_addr = !empty(Yii::$app->request->post('formatted_addr'))?Yii::$app->request->post('formatted_addr'):'';
	    $lat      = !empty(Yii::$app->request->post('lat'))?Yii::$app->request->post('lat'):'';
	    $long     = !empty(Yii::$app->request->post('long'))?Yii::$app->request->post('long'):'';

	    $response = [];

        $appArr = array();

	    // validate
	    if(empty($cnt_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Contact id cannot be blank!',
	        'data' => '',
	      ];
	    }elseif(empty($sub_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Subscriber id cannot be blank!',
	        'data' => '',
	      ];
	    }elseif(empty($note)){
	      $response = [
	        'success' => '0',
	        'message' => 'Note cannot be blank!',
	        'data' => '',
	      ];	    	
	    }elseif(empty($type)){
	      $response = [
	        'success' => '0',
	        'message' => 'Type cannot be blank!',
	        'data' => '',
	      ];	    	
	    }elseif(empty($mem_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Member id cannot be blank!',
	        'data' => '',
	      ];	    	
	    }elseif(empty($date)){
	      $response = [
	        'success' => '0',
	        'message' => 'Date cannot be blank!',
	        'data' => '',
	      ];	    	
	    }
	    else{
	        // search in the database, there is no email referred
	        $subscriber = \common\models\User::findIdentity($sub_id);

	        if(!empty($subscriber)){


			date_default_timezone_set("UTC");
			$milliseconds = strtotime(gmdate("Y-m-d H:i:s"));


				if($all_day == 1){					
					$duration = 0;
				}else{

					$all_day = 0;
				}

	        	$appModel = new TblAppointments();

		        	$appModel->contact_id     = $cnt_id;
		        	$appModel->subscriber_id  = $sub_id;
		        	$appModel->quote_id       = $quote_id;
		        	$appModel->note           = $note;
		        	$appModel->street1        = $street1;	
		        	$appModel->street2        = $street2;
		        	$appModel->suburb         = $suburb;
		        	$appModel->state          = $state;
		        	$appModel->postal         = $postal;
		        	$appModel->country        = $country;
		        	$appModel->formatted_addr = $frm_addr;
		        	$appModel->lat            = $lat;
		        	$appModel->lng            = $long;			        	
		        	$appModel->date           = $date;
		        	$appModel->duration       = $duration;
		        	$appModel->type           = $type;
		        	$appModel->member_id      = $mem_id;  
		        	$appModel->scope          = $scope;      	
		        	$appModel->allDay         = $all_day;      
			        $appModel->created_at     = $milliseconds;
			        $appModel->updated_at     = $milliseconds;

			        if($appModel->save(false)){

			        	$app_id = $appModel->id;

				        	$appArr['allDay']     = $all_day;
				        	$appArr['id']         = $app_id;
				        	$appArr['date']       = $date;
				        	$appArr['duration']   = $duration;
				        	$appArr['quote_id']   = $quote_id;
				        	$appArr['note']       = $note;
				        	$appArr['location']   = $frm_addr;
				        	$appArr['type']       = $type;
				        	$appArr['member_id']  = $mem_id;
				        	$appArr['scope']      = $scope;
				        	$appArr['lat']        = $lat;
				        	$appArr['lng']        = $long;

								$response = [
									'success' => '1',
									'message' => 'Appointment saved successfully!',
									'data' => $appArr,
								];		        	
			        }

	        }else{

	          $response = [
	            'success' => '0',
	            'message' => 'Subscriber doesnot exist in database!',
	            'data' => '',
	          ];

	        }
	    }

	    ob_start();
	    echo json_encode($response);    

	} //actionCreateAppointment

	public function actionGetAppointmentInit(){

    	/* mandaory fields */
	    $sub_id  = !empty($_GET['subscriber_id'])?$_GET['subscriber_id']:'';

	    //echo 'here-'.$sub_id; exit;    

	    $response = [];

	    $memFinalArr = array();
	    $appFinalArr = array();

	    // validate
	    if(empty($sub_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Fields cannot be blank!',
	        'data' => '',
	      ];
	    }
	    else{

	        // search in the database, there is no email referred
	        $subscriber = \common\models\User::findIdentity($sub_id);

	        if(!empty($subscriber)){

				$memModel = TblMembers::find()
				        ->joinWith('subscriber')		
				        ->where(['tbl_members.subscriber_id' => '1'])		        	       
				        ->all();

				//echo "<pre>"; print_r($memModel); exit;

				$memArr = array();

				if(count($memModel) > 0){

					foreach($memModel as $model){

						$memArr['id']     = $model->id;
						$memArr['email']  = $model->email;
						$memArr['f_name'] = $model->f_name;
						$memArr['l_name'] = $model->l_name;
						$memArr['phone']  = $model->phone;

						$memFinalArr[] = $memArr;

					} //foreach

				} //memArr


				$appTypeModel = TblAppointmentType::find()->all();

				//echo "<pre>"; print_r($appTypeModel); exit;

				$apArr = array();			
				if(count($appTypeModel) > 0){
					
					foreach($appTypeModel as $appointment){
						$apArr['id']   = $appointment->id;
						$apArr['name'] = $appointment->name;

						$appFinalArr[] = $apArr;
					} //foreach
				}

					$finalArr['types']   = $appFinalArr; 
					$finalArr['members'] = $memFinalArr; 

					$response = [
						'success' => '1',
						'message' => 'Init Data',
						'data'    => $finalArr,
					];

	        }else{

	          $response = [
	            'success' => '0',
	            'message' => 'Subscriber doesnot exist in database!',
	            'data' => '',
	          ];

	        }
	    }

	    ob_start();
	    echo json_encode($response);    	    

	} //GetAppointmentInit

	public function actionAppointments(){

    	/* mandaory fields */
	    $sub_id  = !empty($_GET['subscriber_id'])?$_GET['subscriber_id']:'';

	    $cnt_id = !empty($_GET['conatct_id'])?$_GET['conatct_id']:'';
	    $month  = !empty($_GET['month'])?$_GET['month']:'';
	    $year   = !empty($_GET['year'])?$_GET['year']:'';

	    //echo 'here-'.$sub_id; exit;    

	    $response = [];
	    
	    $appFinalArr = array();

	    // validate
	    if(empty($sub_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Fields cannot be blank!',
	        'data' => '',
	      ];
	    }
	    else{

	        // search in the database, there is no email referred
	        $subscriber = \common\models\User::findIdentity($sub_id);

	        if(!empty($subscriber)){

        	$connection = \Yii::$app->db;

	        	if($month !='' && $year != ''){
	        	 $model = $connection->createCommand('SELECT t1.*, t2.`name` AS customerName FROM tbl_appointments as t1 LEFT JOIN `tbl_contacts` as t2 ON t1.`contact_id` = t2.`contact_id` WHERE MONTH(from_unixtime(t1.`date`)) = '.$month.' AND YEAR(from_unixtime(t1.`date`)) = '.$year.' AND 
	        	 	t1.`subscriber_id` ='.$sub_id.' ORDER by t1.`id` DESC');
	        	}else{
	        	 $model = $connection->createCommand('SELECT t1.*, t2.`name` AS customerName FROM tbl_appointments as t1 LEFT JOIN `tbl_contacts` as t2 ON t1.`contact_id` = t2.`contact_id` WHERE t1.`subscriber_id` ='.$sub_id.' ORDER by id DESC');
	        	}
				
				$users = $model->queryAll();

				//echo "<pre>"; print_r($users); exit;

					$apArr = array();			

					if(count($users) > 0){
						
						foreach($users as $_users){

								$apArr['allDay']       = $_users['allDay'];
								$apArr['id']           = $_users['id'];
								$apArr['date']         = $_users['date'];
								$apArr['duration']     = $_users['duration'];
								$apArr['quote_id']     = $_users['quote_id'];							
								$apArr['location']     = $_users['formatted_addr'];
								$apArr['note']         = $_users['note'];
								$apArr['lat']          = $_users['lat'];
								$apArr['lng']          = $_users['lng'];								
								$apArr['customerName'] = $_users['customerName'];

									$appType = TblAppointmentType::findOne($_users['type']);

										if($appType){
											$type = $appType->name;						
										}else{
											$type = '';
										}

										$apArr['type_id'] = $_users['type'];
										$apArr['type']    = $type;

									$member = TblMembers::findOne($_users['member_id']);

										if($member){
											$memberName = $member->f_name . ' ' . $member->l_name;						
										}else{
											$memberName = '';
										}

										$apArr['member_id']  = $_users['member_id'];
										$apArr['memberName'] = $memberName;
										$apArr['contact_id'] = $_users['contact_id'];

								$appFinalArr[] = $apArr;

						} //foreach
					}
						
						$finalArr['appointments'] = $appFinalArr; 

						$response = [
							'success' => '1',
							'message' => 'Appointment Data',
							'data'    => $finalArr,
						];

	        }else{

	          $response = [
	            'success' => '0',
	            'message' => 'Subscriber doesnot exist in database!',
	            'data' => '',
	          ];

	        }
	    }

	    ob_start();
	    echo json_encode($response);    	    

	} //actionAppointments


	public function actionUpdateContact(){

    	//echo "<pre>"; print_r($_FILES); exit;

		date_default_timezone_set("UTC");
		$milliseconds = strtotime(gmdate("Y-m-d H:i:s"));

		$siteURL = Url::base(true). '/uploads';

    	/* mandaory fields */
    	$cnt_id  = !empty(Yii::$app->request->post('contact_id'))?Yii::$app->request->post('contact_id'):'';
	    $sub_id  = !empty(Yii::$app->request->post('subscriber_id'))?Yii::$app->request->post('subscriber_id'):'';
	    $name    = !empty(Yii::$app->request->post('name'))?Yii::$app->request->post('name'):'';
	    $email   = !empty(Yii::$app->request->post('email'))?Yii::$app->request->post('email'):'';
	    $phone   = !empty(Yii::$app->request->post('phone'))?Yii::$app->request->post('phone'):'';

	    /* optional fields */
	    $street1 = !empty(Yii::$app->request->post('street1'))?Yii::$app->request->post('street1'):'';
	    $street2 = !empty(Yii::$app->request->post('street2'))?Yii::$app->request->post('street2'):'';
	    $suburb  = !empty(Yii::$app->request->post('suburb'))?Yii::$app->request->post('suburb'):'';
	    $state   = !empty(Yii::$app->request->post('state'))?Yii::$app->request->post('state'):'';
		$postal  = !empty(Yii::$app->request->post('postal'))?Yii::$app->request->post('postal'):'';
		$country = !empty(Yii::$app->request->post('country'))?Yii::$app->request->post('country'):'';
		$frm_add = !empty(Yii::$app->request->post('formatted_addr'))?Yii::$app->request->post('formatted_addr'):'';
		$lat     = !empty(Yii::$app->request->post('lat'))?Yii::$app->request->post('lat'):'';
		$long    = !empty(Yii::$app->request->post('long'))?Yii::$app->request->post('long'):'';

		$uploads = UploadedFile::getInstancesByName("image");

	    $response = [];

	    $arr = array();

	    // validate
	    if(empty($cnt_id) || empty($sub_id) || empty($name) || empty($email) || empty($phone)){
	      $response = [
	        'success' => '0',
	        'message' => 'Fields cannot be blank!',
	        'data' => '',
	      ];
	    }
	    else{

	        // search in the database, there is no email referred
	        $user = \common\models\User::findIdentity($sub_id);

	        // if the email exists then
	        if(!empty($user)){

				date_default_timezone_set("UTC");
				$milliseconds = strtotime(gmdate("Y-m-d H:i:s"));

					$contact = TblContacts::findOne($cnt_id);

					if($contact){

						$addrId = $contact->address_id;

						$address = TblAddress::findOne($addrId);

							if($address){

								$address->street1           = $street1;
								$address->street2           = $street2;
								$address->suburb            = $suburb;
								$address->client_state      = $state;
								$address->postal_code       = $postal;
								$address->country_id        = $country;
								$address->formatted_address = $frm_add;
								$address->lat               = $lat;
								$address->lng               = $long;
								$address->updated_at        = $milliseconds;

								$address->save(false);

							}

								$contact->name       = $name;
								$contact->email      = $email;
								$contact->phone      = $phone;
								$contact->updated_at = $milliseconds;									

					        	$image = '';

					        	if(!empty($uploads)){

									foreach ($uploads as $file){					    		

								        if(isset($file->size)){		           
								            $varr = time().rand(1,100);
								            $file->saveAs('uploads/' . $file->baseName . '_' . $varr .'.' . $file->extension);
								        }

								        $image = $file->baseName . '_' . $varr .'.' . $file->extension;

									} //foreach

									$contact->image = $image;
					        	}

								$contact->save(false);		

								//echo "<pre>"; print_r($contact); exit;
								$contact->name = $name;
								$contact->save(false);

								if($image!=''){
									$image = $siteURL.'/'.$image;
								}

						    	$arr['contact_id']     = $cnt_id;
						    	$arr['name']           = $name;
						    	$arr['email']          = $email;
						    	$arr['phone']          = $phone;
						    	$arr['street1']        = $street1;
						    	$arr['street2']        = $street2;
						    	$arr['suburb']         = $suburb;
						    	$arr['state']          = $state;
						    	$arr['postal']         = $postal;
						    	$arr['country']        = $country;
						    	$arr['formatted_addr'] = $frm_add;
						    	$arr['lat']            = $lat;
						    	$arr['lng']            = $long;
						    	$arr['image']          = $image;

							      $response = [
							        'success' => '1',
							        'message' => 'Contact successfully updated!',
							        'data' => $arr,
							      ];

					}else{

			          $response = [
			            'success' => '0',
			            'message' => 'Contact doesnot exist in database!',
			            'data' => '',
			          ];	

					}

			}else{

	          $response = [
	            'success' => '0',
	            'message' => 'Subscriber doesnot exist in database!',
	            'data' => '',
	          ];				

			}

	    }

	    ob_start();
	    echo json_encode($response);    	    

	} //actionUpdateContact


	public function actionInvoices()
    {

    	/* mandaory fields */
	    $sub_id  = !empty($_GET['subscriber_id'])?$_GET['subscriber_id']:'';
	    $cnt_id  = !empty($_GET['contact_id'])?$_GET['contact_id']:'';

	    //echo 'here-'.$sub_id; exit;    

	    $response = [];

	    $arr = array();

	    // validate
	    if(empty($sub_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Fields cannot be blank!',
	        'data' => '',
	      ];
	    }
	    else{

	        // search in the database, there is no email referred
	        $subscriber = \common\models\User::findIdentity($sub_id);

	        if(!empty($subscriber)){

	        	if($cnt_id != ''){

					$model = TblInvoices::find()	
					        ->where([ 'subscriber_id' => $sub_id])
					        ->andWhere(['contact_id'=>$cnt_id])
					        ->orderBy('invoice_id DESC')
					        ->all();

	        	}else{

					$model = TblInvoices::find()	
					        ->where([ 'subscriber_id' => $sub_id])					       
					        ->orderBy('invoice_id DESC')
					        ->all();
	        	}

					//echo "<pre>"; print_r($model); exit;        

					$uArr = array();

					$finalArr = array(); 

					if(count($model)>0){

						foreach($model as $invoice){
						    $inv_id      = $invoice->invoice_id;
							$quote_id    = $invoice->quote_id;
							$quote_note  = $invoice->quote_note;
							$description = $invoice ->description;
							$amount      = $invoice ->amount;
						    $sent        = $invoice->sent;
							$paid        = $invoice->paid;
							$date        = $invoice->date;
							$created_at  = $invoice->date;
							$updated_at  = $invoice->date;		

								$uArr['id']          = $inv_id;
								$uArr['quote_id']    = $quote_id;
								$uArr['quote_note']  = $quote_note;
								$uArr['description'] = $description;
								$uArr['amount']      = $amount;
								$uArr['sent']        = $sent;
								$uArr['paid']        = $paid;
								$uArr['date']        = $date;
								$uArr['created_at']  = $created_at;
								$uArr['updated_at']  = $updated_at;

									$finalArr['invoices'][] = $uArr;

						} //foreach

					}else{

						$finalArr['invoices'] = array();
					}

					$response = [
						'success' => '1',
						'message' => 'Invoice Details!',
						'data' => $finalArr,
					];

	        }
	        // If the email is not found make a response like this
	        else{
	          $response = [
	            'success' => '0',
	            'message' => 'Subscriber doesnot exist in database!',
	            'data' => '',
	          ];
	        }	

	    }

	    ob_start();
	    echo json_encode($response);    	

    }//actionInvoices

	public function actionUpdateAppointment(){

    	/* mandaory fields */
	    $app_id  = !empty(Yii::$app->request->post('appointment_id'))?Yii::$app->request->post('appointment_id'):'';
	    $cnt_id  = !empty(Yii::$app->request->post('contact_id'))?Yii::$app->request->post('contact_id'):'';
	    $sub_id  = !empty(Yii::$app->request->post('subscriber_id'))?Yii::$app->request->post('subscriber_id'):'';	    
	    $note    = !empty(Yii::$app->request->post('note'))?Yii::$app->request->post('note'):'';
	    $type    = !empty(Yii::$app->request->post('type'))?Yii::$app->request->post('type'):'';
	    $mem_id  = !empty(Yii::$app->request->post('member_id'))?Yii::$app->request->post('member_id'):'';
	    $all_day = !empty(Yii::$app->request->post('allDay'))?Yii::$app->request->post('allDay'):0;

	    /* optional fields */
	    $date     = !empty(Yii::$app->request->post('date'))?Yii::$app->request->post('date'):'';
	    $duration = !empty(Yii::$app->request->post('duration'))?Yii::$app->request->post('duration'):'';
	    $scope    = !empty(Yii::$app->request->post('scope'))?Yii::$app->request->post('scope'):'';
	    $quote_id = !empty(Yii::$app->request->post('quote_id'))?Yii::$app->request->post('quote_id'):'';
	    $street1  = !empty(Yii::$app->request->post('street1'))?Yii::$app->request->post('street1'):'';
	    $street2  = !empty(Yii::$app->request->post('street2'))?Yii::$app->request->post('street2'):'';
	    $suburb   = !empty(Yii::$app->request->post('suburb'))?Yii::$app->request->post('suburb'):'';
	    $state    = !empty(Yii::$app->request->post('state'))?Yii::$app->request->post('state'):'';
	    $postal   = !empty(Yii::$app->request->post('postal'))?Yii::$app->request->post('postal'):'';
	    $country  = !empty(Yii::$app->request->post('country'))?Yii::$app->request->post('country'):'';
	    $frm_addr = !empty(Yii::$app->request->post('formatted_addr'))?Yii::$app->request->post('formatted_addr'):'';
	    $lat      = !empty(Yii::$app->request->post('lat'))?Yii::$app->request->post('lat'):'';
	    $long     = !empty(Yii::$app->request->post('long'))?Yii::$app->request->post('long'):'';

	    $response = [];

        $appArr = array();

	    // validate
	    if(empty($app_id)){
			$response = [
				'success' => '0',
				'message' => 'Appointment id cannot be blank!',
				'data' => '',
			];
	    }elseif(empty($cnt_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Contact id cannot be blank!',
	        'data' => '',
	      ];
	    }elseif(empty($sub_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Subscriber id cannot be blank!',
	        'data' => '',
	      ];
	    }elseif(empty($note)){
	      $response = [
	        'success' => '0',
	        'message' => 'Note cannot be blank!',
	        'data' => '',
	      ];	    	
	    }elseif(empty($type)){
	      $response = [
	        'success' => '0',
	        'message' => 'Type cannot be blank!',
	        'data' => '',
	      ];	    	
	    }elseif(empty($mem_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Member id cannot be blank!',
	        'data' => '',
	      ];	    	
	    }elseif(empty($date)){
	      $response = [
	        'success' => '0',
	        'message' => 'Date cannot be blank!',
	        'data' => '',
	      ];	    	
	    }
	    else{

	        // search in the database, there is no email referred
	        $subscriber = \common\models\User::findIdentity($sub_id);

	        if(!empty($subscriber)){

				date_default_timezone_set("UTC");
				$milliseconds = strtotime(gmdate("Y-m-d H:i:s"));

				if($all_day == 1){					
					$duration = 0;
				}else{
					$all_day = 0;
				}

					$appointment = TblAppointments::findOne($app_id);

					if($appointment){

						//echo '<pre>'; print_r($appointment); exit;

			        	$appointment->contact_id     = $cnt_id;
			        	$appointment->subscriber_id  = $sub_id;
			        	$appointment->quote_id       = $quote_id;
			        	$appointment->note           = $note;
			        	$appointment->street1        = $street1;	
			        	$appointment->street2        = $street2;
			        	$appointment->suburb         = $suburb;
			        	$appointment->state          = $state;
			        	$appointment->postal         = $postal;
			        	$appointment->country        = $country;
			        	$appointment->formatted_addr = $frm_addr;
			        	$appointment->lat            = $lat;
			        	$appointment->lng            = $long;			        	
			        	$appointment->date           = $date;
			        	$appointment->duration       = $duration;
			        	$appointment->type           = $type;
			        	$appointment->member_id      = $mem_id;  
			        	$appointment->scope          = $scope; 
			        	$appointment->allDay         = $all_day;      					        
				        $appointment->updated_at     = $milliseconds;

				        	if($appointment->save(false)){

				        		$appArr['allDay']     = $all_day;
					        	$appArr['id']         = $app_id;
					        	$appArr['date']       = $date;
					        	$appArr['duration']   = $duration;
					        	$appArr['quote_id']   = $quote_id;
					        	$appArr['note']       = $note;
					        	$appArr['location']   = $frm_addr;
					        	$appArr['type']       = $type;
					        	$appArr['member_id']  = $mem_id;
					        	$appArr['scope']      = $scope;
					        	$appArr['lat']        = $lat;
					        	$appArr['lng']        = $long;

									$response = [
										'success' => '1',
										'message' => 'Appointment saved successfully!',
										'data' => $appArr,
									];		  
				        	}
					}

	        }else{

	          $response = [
	            'success' => '0',
	            'message' => 'Subscriber doesnot exist in database!',
	            'data' => '',
	          ];

	        }
	    }

	    ob_start();
	    echo json_encode($response);    

	} //actionUpdateAppointment

	public function actionQuotes()
    {

    	/* mandaory fields */
	    $sub_id  = !empty($_GET['subscriber_id'])?$_GET['subscriber_id']:'';
	    $con_id  = !empty($_GET['contact_id'])?$_GET['contact_id']:'';

	    $type   = !empty($_GET['type'])?$_GET['type']:'';
	    $status = !empty($_GET['status'])?$_GET['status']:'';

	    $from   = !empty($_GET['from'])?$_GET['from']:'';
	    $to     = !empty($_GET['to'])?$_GET['to']:'';

		$quoteId = !empty($_GET['quote_id'])?$_GET['quote_id']:'';

		$page   = isset($_GET['page'])?$_GET['page']:'';

	    //echo 'here-'.$sub_id; exit;    

	    $response = [];

	    $arr = array();

	    $paintDefaultExists = 0;
	    $roomsExists = 0;
	    // validate
	    if(empty($sub_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Fields cannot be blank!',
	        'data' => '',
	      ];
	    }
	    else{

	        // search in the database, there is no email referred
	        $subscriber = \common\models\User::findIdentity($sub_id);

	        if(!empty($subscriber)){

	        	$siteURL = Url::base(true). '/uploads';

	        	/*if($con_id != ''){
					$model = TblQuotes::find()				
					        ->joinWith('contact')	      		       
					        ->where([ 'tbl_quotes.subscriber_id' => $sub_id])
					        ->andWhere(['tbl_quotes.contact_id'=>$con_id])				        
					        ->orderBy('quote_id DESC')
					        ->all();
	        	}else{
					$model = TblQuotes::find()				
					        ->joinWith('contact')	      		       
					        ->where([ 'tbl_quotes.subscriber_id' => $sub_id])					        
					        ->orderBy('quote_id DESC')
					        ->all();
	        	}*/

					//$modelQuery = TblQuotes::find()->joinWith('paintDefaults')->joinWith('brandPref')->joinWith('rooms')->joinWith('contact')->joinWith('siteAddress')->where([ 'tbl_quotes.subscriber_id' => $sub_id]);
					
					$modelQuery = TblQuotes::find()->joinWith('paintDefaults')->joinWith('brandPref')->joinWith('rooms')->joinWith('contact')->joinWith('siteAddress')->where([ 'tbl_quotes.subscriber_id' => $sub_id]);

					if($con_id != '')
					{
						$modelQuery->andWhere(['tbl_quotes.contact_id'=>$con_id]);
					}
					if($status)	    				
					{
						$statusIDs = array_map('intval', explode(',', $status));
							
						$modelQuery->andWhere(['in','tbl_quotes.status',$statusIDs]);
					}
					if($type)
					{
						$typeIDs = array_map('intval', explode(',', $type));		
							
						$modelQuery->andWhere(['in', 'tbl_quotes.type', $typeIDs]);
					}

					if($quoteId)
					{
						$modelQuery->andWhere(['tbl_quotes.quote_id'=>$quoteId]);
					}

					if( $from !='' && $to != ''){

						$modelQuery->andWhere('tbl_quotes.created_at>='.$from)->andWhere('tbl_quotes.created_at<='.$to);
					}

					/*---------- execute query below ---------*/

					$offset = ($page==0) ? 0 : (50*$page);

					//echo $offset;exit;

					$model = $modelQuery->orderBy('quote_id DESC')->groupBy('tbl_quotes.quote_id')->limit(50)->offset($offset)->all();

					//echo "<pre>"; print_r($model); exit;


	        	if(count($model) > 0){

					$uArr = array(); 
					$cArr = array();

					foreach($model as $quotes){
						$paintDefaultExists = 0;
						$roomsExists = 0;

					    $quoteId = $quotes->quote_id;
						$date    = $quotes->created_at;
						$type    = $quotes->type;
						$note    = $quotes->note;
						$price   = $quotes->price;
						$client_name   = $quotes->client_name;
						$status  = $quotes->status;
						$description  = $quotes->description;
						$contactId  = $quotes->contact_id;


    					$finalArr['quote_id']    = $quoteId;
    					$finalArr['description'] = $description;
    					$finalArr['status']      = $status;
    					$finalArr['date']        = $date;
    					$finalArr['note']        = $note;
    					$finalArr['type']        = $type;
						$finalArr['price']       = $price;
						$finalArr['client_name'] = $client_name;

						if(isset($quotes->contact) && !empty($quotes->contact) ){								

							$addr = TblAddress::findOne($quotes->contact->address_id);

								$cArr['email'] 			= $quotes->contact->email;
								$cArr['name']  			= $quotes->contact->name;
								$cArr['phone'] 			= $quotes->contact->phone;	
								$cArr['image'] 			= $siteURL.'/'.$quotes->contact->image;	
								$cArr['formatted_addr'] = $addr->formatted_address;	
								$cArr['lat']     		= $addr->lat;
								$cArr['lng']     		= $addr->lng;								
								$cArr['country'] 		= $addr->country_id;
								$cArr['postal']  		= $addr->postal_code;
								$cArr['state']   	 	= $addr->client_state;	
								$cArr['street1'] 	 	= $addr->street1;
								$cArr['street2'] 	 	= $addr->street2;
								$cArr['suburb']  	 	= $addr->suburb;
								$cArr['s_checked']	 	= $addr->is_checked;
								$cArr['contact_id']  	= $contactId;		
						}

						if(isset($quotes->siteAddress) && !empty($quotes->siteAddress) ){	

							$sname  = ($quotes->siteAddress->s_name != '')  ? $quotes->siteAddress->s_name  : $quotes->contact->name;
							$semail = ($quotes->siteAddress->s_email != '') ? $quotes->siteAddress->s_email : $quotes->contact->email;
							$sphone = ($quotes->siteAddress->s_phone != '') ? $quotes->siteAddress->s_phone : $quotes->contact->phone;

							$finalArr['site']['country'] 		= $quotes->siteAddress->country_id;
							$finalArr['site']['email']          = $semail;
							$finalArr['site']['formatted_addr'] = $quotes->siteAddress->formatted_address;	
							$finalArr['site']['lat']     		= $quotes->siteAddress->lat;
							$finalArr['site']['lng']     		= $quotes->siteAddress->lng;
							$finalArr['site']['name']    		= $sname;	
							$finalArr['site']['phone']   		= $sphone;
							$finalArr['site']['postal']  		= $quotes->siteAddress->postal_code;
							$finalArr['site']['state']   		= $quotes->siteAddress->client_state;	
							$finalArr['site']['street1'] 		= $quotes->siteAddress->street1;
							$finalArr['site']['street2'] 		= $quotes->siteAddress->street2;
							$finalArr['site']['suburb']  		= $quotes->siteAddress->suburb;	
							$finalArr['site']['s_checked']		= $quotes->siteAddress->is_checked;	

						}

						if(isset($quotes->brandPref) && !empty($quotes->brandPref) ){
							if(count($quotes->brandPref) > 0){
								$paintDefaultExists = 1;
							}
						}

						if(isset($quotes->rooms) && !empty($quotes->rooms) ){
							if(count($quotes->rooms) > 0){
								$roomsExists = 1;
							}
						}

						/*$finalArr['quote_id']    = $quoteId;
						$finalArr['date']        = $date;
						$finalArr['type']        = $type;
						$finalArr['note']        = $note;
						$finalArr['price']       = $price;
						$finalArr['status']      = $status;
						$finalArr['description'] = $description;*/

						$finalArr['contact']  = $cArr;

						$finalArr['counts']['jss']      = 10;
						$finalArr['counts']['contacts'] = 15;
						$finalArr['counts']['quotes']   = 8;
						$finalArr['counts']['docs']     = 12;

						$finalArr['paintDefaultExists'] = $paintDefaultExists;
						$finalArr['roomsExists'] = $roomsExists;

						$arr['quotes'][] = $finalArr;

					} //foreach						 

					$response = [
						'success' => '1',
						'message' => 'Contacts Details!',
						'data' => $arr,
					];
	        	}else{

	        		    $arr['quotes'] = array();

	        			$response = [
							'success' => '1',
							'message' => 'Contacts Details!',
							'data' => $arr,
						];
	        	}

	        }
	        // If the email is not found make a response like this
	        else{
	          $response = [
	            'success' => '0',
	            'message' => 'Subscriber doesnot exist in database!',
	            'data' => '',
	          ];
	        }	

	    }

	    ob_start();
	    echo json_encode($response);    	

    }//actionQuotes


	public function actionBrands()
    {
    	$data = array();
    		

		$brandModel = TblBrands::find()
						->joinWith('tblTiers')
		                ->all();

		if(count($brandModel)>0){

			//echo "<pre>"; print_r($brandModel); exit;

			$tArr = array();

			foreach($brandModel as $model){

				$arr['brand_id'] =  $model->brand_id;
				$arr['name']     =  $model->name;
				$arr['image']    =  $model->logo;						

					//echo "<pre>"; print_r($model->tblTiers[1]); exit;

					$tFinArr = array();

					if(count($model->tblTiers) > 0){			

						foreach($model->tblTiers as $tiermodel){

							$tArr['tier_id'] = $tiermodel->tier_id;
							$tArr['name']    = $tiermodel->name;
							$tArr['image']   = $model->logo;

							$tFinArr[] = $tArr;
						} //foreach

					}

					$arr['tiers']    =  $tFinArr;

					$fArr[] = $arr;		
					//$fArr[] = $tFinArr;		

			}//foreach

			$strModel = TblStrengths::find()
							->select(['strength_id', 'name'])
			              	->all();

				if(count($strModel)>0){

					//echo "<pre>"; print_r($brandModel); exit;
					$si = 0;
					foreach($strModel as $smodel){
						$sArr['id']   = $smodel->strength_id;
						$sArr['name'] = $smodel->name;
						$sArr['default'] = ($si == 0)?1:0;
						$finalArr[] = $sArr;
						$si++;
					}
				}else{
					$finalArr[] = array();

				}

				//coats
				$coatsArr[0]["id"] = 1;
				$coatsArr[0]["name"] = 'One';
				$coatsArr[1]["id"] = 2;
				$coatsArr[1]["name"] = 'Two';
				$coatsArr[2]["id"] = 3;
				$coatsArr[2]["name"] = 'Three';

				//prep
				$prepArr[0]["id"] = 1;
				$prepArr[0]["name"] = 'Test';
				$prepArr[1]["id"] = 2;
				$prepArr[1]["name"] = 'Premium';

				$data['brands']        = $fArr;				
				$data['tint_strength'] = $finalArr;
				$data['coats']         = $coatsArr;
				$data['prep']          = $prepArr;

				
				$response = [
					'success' => '1',
					'message' => 'Brands and Strength data!',
					'data' => $data,
				];

		}else{
	          $response = [
	            'success' => '0',
	            'message' => 'There are no brands exist in database!',
	            'data' => '',
	          ];

		}

	    ob_start();
	    echo json_encode($response);    	

    }//actionBrands


	public function actionTiers()
    {
    	/* mandaory fields */
	    $brand_id = !empty($_GET['brand_id'])?$_GET['brand_id']:'';
	    $type_id  = !empty($_GET['type_id'])?$_GET['type_id']:'';

	    $response = [];
	    $arr      = array();
	    $data     = array();

	    // validate
	    if(empty($brand_id) && empty($type_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Fields cannot be blank!',
	        'data' => '',
	      ];
	    }
	    else{

			$tierModel = TblTiers::find()
							->joinWith('brand')
					        ->where(['tbl_tiers.brand_id' => $brand_id])
					        ->andWhere(['tbl_tiers.type_id'=>$type_id])				        
					        ->orderBy('tier_id DESC')
					        ->all();

			if(count($tierModel)>0){

				//echo "<pre>"; print_r($tierModel); exit;

				foreach($tierModel as $model){

					$arr['tier_id'] =  $model->brand_id;
					$arr['name']    =  $model->name;
					$arr['image']   =  $model->brand->logo;

					$fArr[] = $arr;

				}
					$data['tiers'] = $fArr;

				$response = [
					'success' => '1',
					'message' => 'Tiers data!',
					'data' => $data,
				];

			}else{
		          $response = [
		            'success' => '0',
		            'message' => 'There are no tiers exist in database!',
		            'data' => '',
		          ];

			}
		}

	    ob_start();
	    echo json_encode($response);    	

    }//actionTiers



	public function actionTiersComponent()
    {

	    $response = [];
	    $arr      = array();
	    $data     = array();

    	/* mandaory fields */
	    $tier_id = !empty($_GET['tier_id'])?$_GET['tier_id']:'';
		$type_id = isset($_GET['type_id'])?$_GET['type_id']:'';
		$quote_id = isset($_GET['quote_id'])?$_GET['quote_id']:'';
	   
	    // validate
	    if(empty($tier_id) || $type_id==''){
	      $response = [
	        'success' => '0',
	        'message' => 'Fields cannot be blank!',
	        'data' => '',
	      ];
	    }else{    	
			
			$grpModels = TblComponentGroups::find()
						->joinWith(
								['tblTierCoats' => 
									function($q) use ($tier_id){
										$q->where(['tbl_tier_coats.tier_id' => $tier_id]);
									}
								])
						->joinWith(['tblTierCoats.sheen','tblTierCoats.product'])
						->where(['in', ['tbl_component_groups.type_id'], [$type_id,3]])
						->andWhere(['tbl_component_groups.enabled' => 1])
						->all();

			//echo '<pre>'; print_r($grpModels); exit;

			$finalArr = array();
			if(count($grpModels) > 0){

				foreach($grpModels as $model){

					$dummy = array();
					
					//echo '<pre>'; print_r($top_coat);

					$arr['id']                  = $model->group_id;
					$arr['name']                = $model->name;

					if(isset($model->tblTierCoats) && count($model->tblTierCoats)>0){

						$i = 0;
						$top_coat =  array();
						$under_coat =  array();
						$sFinalArr = array();
						$sArr = array();
						$myArr = array();

						//$tci = 0;
						$uci = 0;
						$si = 0;

						$sheenDefaultTC = [];

						foreach($model->tblTierCoats as $sheendata){

							if($sheendata->top_coats == 1){
								$top_coat['name'] = $sheendata->product->name;
								$top_coat['product_id'] = $sheendata->product->product_id;
								//$top_coat['default'] = $sheendata->enabled;
								//$top_coat['default'] = (!array_key_exists($sheendata->sheen_id, $sheenDefaultTC))?1:0;
								$top_coat['default'] = $sheendata->product_default;

								$myArr[$sheendata->sheen_id]['sheen_name'] = $sheendata->sheen->name;
								$myArr[$sheendata->sheen_id]['default'] = $sheendata->sheen_default;
								$myArr[$sheendata->sheen_id]['top_coats'][] = $top_coat;

								
								$sheenDefaultTC[$sheendata->sheen_id]=$sheendata->sheen_id;
							}
							else{
								$unders['name'] = $sheendata->product->name;
								$unders['product_id'] = $sheendata->product->product_id;
								//$unders['default'] = $sheendata->enabled;
								//$unders['default'] = ($uci==0)?1:0;
								$unders['default'] = $sheendata->product_default;

								$under_coat[] = $unders;
								$uci++;
							}

						} //foreach

						foreach($myArr as $sheen_id => $mySheen){
							$sArr['sheen_id'] = $sheen_id;
							$sArr['name'] = $mySheen['sheen_name'];
							//$sArr['default'] = ($si==0)?1:0;
							$sArr['default'] = $mySheen['default'];
							$sArr['top_coats'] = $mySheen['top_coats'];
							$sFinalArr[] = $sArr;
							$si++;
						}


					}else{
						$sFinalArr = array();
					}
					$arr['tt_image'] 	  = "https://www.familyhandyman.com/wp-content/uploads/2017/05/FH14FEB_PTCEIL_04.jpg"; 
					$arr['tt_text'] 	  = "A ceiling is an overhead interior surface that covers the upper limits of a room. It is not generally considered a structural element, but a finished surface concealing the underside of the roof structure or the floor of a storey above. Ceilings can be decorated to taste, and there are many fine examples of frescoes and artwork on ceilings especially in religious buildings."; 
					$arr['tt_url'] 	  = "https://en.wikipedia.org/wiki/Ceiling"; 
					$arr['sheens'] = $sFinalArr;
					$arr['under_coat'] = $under_coat;
					//echo '<pre>'; print_r($myArr); exit;

					$finalArr[] = $arr;

				} //foreach
				//echo '<pre>'; print_r($finalArr); exit;

			} //if

				$data['tier_types'] = $finalArr;

				$response = [
					'success' => '1',
					'message' => 'Component array!',
					'data' => $data,
				];

	    }

	    ob_start();
	    echo json_encode($response);

	} //actionTiersComponent

	public function actionProductColors()
    {

	    $response = [];
	    $arr      = array();
	    
    	/* mandaory fields */
	    $page      = isset($_GET['page'])?$_GET['page']:'';
	    $tag_id    = !empty($_GET['tag_id'])?$_GET['tag_id']:'';
	    $searchVal = !empty($_GET['search'])?$_GET['search']:'';


	    // validate
	    if($page==''){
	      $response = [
	        'success' => '0',
	        'message' => 'Page cannot be blank!',
	        'data' => '',
	      ];
	    }else{    	

	    	if($tag_id !='' && $searchVal!=''){
	    		
	    		$offset = ($page==0) ? 0 : (50*$page);

				$colorsModel = TblColorTags::find()
								->joinWith(['tblColors'=>function($q) use($offset,$searchVal){ $q->andFilterWhere(['like', 'tbl_colors.name', $searchVal])->orderBy('tag_id ASC')->limit(50)->offset($offset);}]) 
								->where(['not in', ['tbl_colors.tag_id'], [1]])
								->andWhere(['tbl_colors.tag_id'=>$tag_id])
								->andWhere(['not in', ['tbl_colors.color_id'], [0]])
								->all();	

	    	}elseif($tag_id !='' ){
	    		
	    		$offset = ($page==0) ? 0 : (50*$page);

				$colorsModel = TblColorTags::find()								
								->joinWith(['tblColors' => function($q) use ($offset){ $q->orderBy('tag_id ASC')->limit(50)->offset($offset);}])
								->where(['not in', ['tbl_colors.tag_id'], [1]])
								->andWhere(['tbl_colors.tag_id'=>$tag_id])
								->andWhere(['not in', ['tbl_colors.color_id'], [0]])
								->all();

	    	}else{

	    		$offset = ($page==0) ? 0 : (50*$page);

				$colorsModel = TblColorTags::find()
						->joinWith(['tblColors' => function($q) use ($page,$searchVal,$offset){ $q->andFilterWhere(['like', 'tbl_colors.name', $searchVal])->orderBy('tag_id ASC')->limit(50)->offset($offset);}])
						->where(['not in', ['tbl_colors.tag_id'], [1]])
						->andWhere(['not in', ['tbl_colors.color_id'], [0]])
						->all();
	    	}
		

			if(count($colorsModel)>0){

				//echo "<pre>"; print_r($colorsModel); exit;	

				$finalArr['colors'] = array();				

				foreach($colorsModel as $model){

					$tagId       = $model->tag_id;
					$tagName     = $model->name;

					$colorId     = '';
					$colorName   = '';
					$colorHex    = '';
					$colorNumber = '';

					if(isset($model->tblColors) && count($model->tblColors)>0){

						foreach($model->tblColors as $colors){

							$colorId     = $colors->color_id;
							$colorName   = $colors->name;
							$colorHex    = $colors->hex;
							$colorNumber = $colors->number;

								$arr['id']       = $colorId;
								$arr['name']     = $colorName;
								$arr['hex']      = $colorHex;
								$arr['number']   = $colorNumber;
								$arr['tag_id']   = $tagId;
								$arr['tag_name'] = $tagName;	

							$finalArr['colors'][] = $arr;	

						}//foreach

					}

				} //foreach

				//echo "<pre>"; print_r($data); exit;

					$response = [
						'success' => '1',
						'message' => 'Colors data!',
						'data' => $finalArr,
					];

			}else{

				$finalArr['colors'] = array();
				$response = [
					'success' => '0',
					'message' => 'No data found!',
					'data' => $finalArr,
				];
			}//else

	    }

	    ob_start();
	    echo json_encode($response);    	

	} //actionProductColors

	public function actionComponents(){

    	/* mandaory fields */
		$type_id = !empty($_GET['type_id'])?$_GET['type_id']:'';
		$quote_id = !empty($_GET['quote_id'])?$_GET['quote_id']:'';

	    $response = [];	    
	    $arr      = array();
	    $myArr = array();

	    // validate
	    if(empty($type_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Type id cannot be blank!',
	        'data' => '',
	      ];
	    }
	    else{

			$cFinalArr = [];
			/*
			tbl_components
			tbl_component_groups
			tbl_component_type
			tbl_paint_defaults
			tbl_colors
			*/
			
			$components = TblComponents::find()
			->joinWith(['group.tblPaintDefaultsComp' => 
				function($q) use ($quote_id){
					$q->where(['tbl_paint_defaults.quote_id' => $quote_id]);
					$q->joinWith(['color']);
				}
			])
			->joinWith(['tblComponentTypes'])
			->all();
			//echo "<pre>"; print_r($components); exit;

			foreach($components as $k => $comp){
				$cArr = [];
				$cArr['id']           		= $comp->comp_id;
				$cArr['comp_name']    		= $comp->name;
				$cArr['price_method_id']    = $comp->price_method_id;
				$cArr['calc_method_id'] 	= $comp->calc_method_id;
				$cArr['tt_image'] 			= $comp->tt_image;
				$cArr['tt_text'] 			= $comp->tt_text;
				$cArr['tt_url'] 			= $comp->tt_url;

				$cArr['savedData']['color_id'] 				= null;
				$cArr['savedData']['isUndercoat'] 			= 0;
				$cArr['savedData']['isSelected'] 			= 0;
				$group = $comp->group;
				
				if($group->tblPaintDefaultsComp){
					$tblPaintDefaultsComp = $group->tblPaintDefaultsComp;
					$cArr['savedData']['isUndercoat'] = $tblPaintDefaultsComp->hasUnderCoat;
					$cArr['savedData']['isSelected'] = 0;
					if($tblPaintDefaultsComp->color){
						$color = $tblPaintDefaultsComp->color;
						$cArr['savedData']['color_id']['id'] = $color->color_id;
						$cArr['savedData']['color_id']['name'] = $color->name;
						$cArr['savedData']['color_id']['hex'] = $color->hex;
						$cArr['savedData']['color_id']['tag_id'] = $color->tag_id;
						$cArr['savedData']['color_id']['number'] = $color->number;
					}
				}					

				//echo "<pre>"; print_r($cArr); exit;

				foreach($comp->tblComponentTypes as $kct => $compType){
					$o = [];
					$o['id'] = $compType->comp_type_id;
					$o['name'] = $compType->name;
					$o['work_rate'] = $compType->work_rate;
					$o['spread_ratio'] = $compType->spread_ratio;
					$o['default'] = $compType->is_default;
					$cArr['options'][] = $o;
				}
				
				$cFinalArr['components'][] = $cArr;
			}

			$response = [
				'success' => '1',
				'message' => 'Component data!',
				'data' => $cFinalArr,
			];
			
			/*
				$connection = \Yii::$app->db;
	        	$model = $connection->createCommand('Select tct.comp_type_id, tct.comp_id, tct.name AS typename, tct.work_rate, tct.spread_ratio, tct.is_default, tct.thickness,tct.excl_area, tc.name,tc.price_method_id, tc.calc_method_id, tcg.enabled, tcg.type_id from tbl_components as tc, tbl_component_groups as tcg, tbl_component_type as tct where tcg.type_id='.$type_id.' AND tcg.group_id=tc.group_id AND tct.comp_id=tc.comp_id AND tcg.enabled=1 ORDER BY tct.comp_id');
				
				$components = $model->queryAll();

				if(count($components)>0){
					//echo "<pre>"; print_r($components); exit;

					$cArr      = array();
					$compArr   = array();
					$cFinalArr = array();

						foreach($components as $_comp){
							//echo $_comp['name'].'</br>';n

							$cArr['id']           = $_comp['comp_type_id'];
							$cArr['name']         = $_comp['typename'];
							$cArr['work_rate']    = $_comp['work_rate'];
							$cArr['spread_ratio'] = $_comp['spread_ratio'];
							$cArr['default']      = $_comp['is_default'];

							$myArr[$_comp['comp_id']]['id']              = $_comp['comp_id'];
							$myArr[$_comp['comp_id']]['comp_name']       = $_comp['name'];
							$myArr[$_comp['comp_id']]['price_method_id'] = $_comp['price_method_id'];
							$myArr[$_comp['comp_id']]['calc_method_id']  = $_comp['calc_method_id'];


							$myArr[$_comp['comp_id']]['tt_image'] 	  = "https://www.familyhandyman.com/wp-content/uploads/2017/05/FH14FEB_PTCEIL_04.jpg"; 
							$myArr[$_comp['comp_id']]['tt_text'] 	  = "A ceiling is an overhead interior surface that covers the upper limits of a room. It is not generally considered a structural element, but a finished surface concealing the underside of the roof structure or the floor of a storey above. Ceilings can be decorated to taste, and there are many fine examples of frescoes and artwork on ceilings especially in religious buildings."; 
							$myArr[$_comp['comp_id']]['tt_url'] 	  = "https://en.wikipedia.org/wiki/Ceiling"; 

							$myArr[$_comp['comp_id']]['options'][]       = $cArr;

						}//foreach

						foreach($myArr as $comp_id => $myComp){

							$compArr['id']              = $comp_id;
							$compArr['comp_name']       = $myComp['comp_name'];
							$compArr['price_method_id'] = $myComp['price_method_id'];
							$compArr['calc_method_id']  = $myComp['calc_method_id'];

							$compArr['tt_image']  = $myComp['tt_image'];
							$compArr['tt_text']  = $myComp['tt_text'];
							$compArr['tt_url']  = $myComp['tt_url'];
							
							$compArr['options'] = $myComp['options'];

							$cFinalArr['components'][] = $compArr;
						} //foreach


						//echo "<pre>"; print_r($myArr); exit;

						$response = [
							'success' => '1',
							'message' => 'Component data!',
							'data' => $cFinalArr,
						];

				}else{

					$cFinalArr['components'] = array();;
					$response = [
						'success' => '0',
						'message' => 'No Component found!',
						'data'    => $cFinalArr,
					];

				}
			*/
		}

	    ob_start();
	    echo json_encode($response);  

	}//actionComponents


	public function actionSpecialItems(){

		$response = [];
		$arr      = array();

		$specialModel = TblSpecialItems::find()
						 	    ->all();

		if(count($specialModel)>0){
			//echo "<pre>"; print_r($specialModel); exit;

			foreach($specialModel as $_model){

				$arr['id']    = $_model->item_id;
				$arr['name']  = $_model->name;
				$arr['price'] = $_model->price;

					$fArr['items'][] = $arr;
			}

			$response = [
				'success' => '1',
				'message' => 'Items data!',
				'data'    => $fArr,
			];

		}else{

			$fArr['items'] = array();
			$response = [
				'success' => '0',
				'message' => 'No Items found!',
				'data'    => $fArr,
			];
		}		 	            
		
	    ob_start();
	    echo json_encode($response);  

	}//actionSpecialItems

	public function actionRooms(){

    	/* mandaory fields */
	    $type_id = !empty($_GET['type_id'])?$_GET['type_id']:'';
	    $siteURL = Url::base(true);

	    $response = [];	    
	    $arr      = array();
	    
	    // validate
	    if(empty($type_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Type id cannot be blank!',
	        'data' => '',
	      ];
	    }
	    else{

			$roomsModel = TblRoomTypes::find()
									->where(['type_id' => $type_id])
							 	    ->all();

			if(count($roomsModel)>0){
				//echo "<pre>"; print_r($roomsModel); exit;

				foreach($roomsModel as $_model){

					$arr['id']       = $_model->room_id;
					$arr['name']     = $_model->name;
					$arr['multiple'] = $_model->multiples;
					$arr['blue_image'] =( $_model->blue_image != "" || !is_null($_model->blue_image) )? $siteURL.'/image/RoomImages/blue/'.$_model->blue_image :$siteURL.'/image/RoomImages/blue/default.png';
					$arr['white_image'] = ( $_model->white_image != "" || !is_null($_model->white_image) )? $siteURL.'/image/RoomImages/white/'.$_model->white_image :$siteURL.'/image/RoomImages/blue/default.png';


					$fArr['rooms'][] = $arr;
				}

				$response = [
					'success' => '1',
					'message' => 'Rooms data!',
					'data'    => $fArr,
				];

			}else{

				$fArr['rooms'] = array();
				$response = [
					'success' => '0',
					'message' => 'No Rooms found!',
					'data'    => $fArr,
				];
			}		 	      
		}      
		
	    ob_start();
	    echo json_encode($response);  

	}//actionRooms



	public function actionTags(){

		$response = [];

		$arr = array();

		$tagsModel = TblColorTags::find()
						->where(['not in', ['tbl_color_tags.tag_id'], [1]])															
						->all();	

		if(count($tagsModel)>0){

			//echo "<pre>"; print_r($tagsModel);

			foreach($tagsModel as $model){

				$arr['tag_id'] = $model->tag_id;
				$arr['name']   = $model->name;

				$tArr['tags'][] = $arr;
			}

			$response = [
				'success' => '1',
				'message' => 'Tags found!',
				'data'    => $tArr,
			];


		}else{

			$tArr['tags'] = array();
			$response = [
				'success' => '0',
				'message' => 'No tags found!',
				'data'    => $tArr,
			];

		}

	    ob_start();
	    echo json_encode($response);  

	}//actionTags



	public function actionCreateNote()
    {
    	//echo "<pre>"; print_r($_FILES); exit;
    	//echo "<pre>"; print_r(Yii::$app->request->post()); exit;
    	$siteURL = Url::base(true). '/uploads';

    	/* mandaory fields */
	    $quote_id = !empty(Yii::$app->request->post('quote_id'))?Yii::$app->request->post('quote_id'):'';
	    $note     = !empty(Yii::$app->request->post('note'))?Yii::$app->request->post('note'):'';	    

		$uploads = UploadedFile::getInstancesByName("image");

	    $response = [];
	    $arr = array();

	    // validate
	    if(empty($quote_id) || empty($note)){
	      $response = [
	        'success' => '0',
	        'message' => 'Fields cannot be blank!',
	        'data' => '',
	      ];
	    }
	    else{

			$quoteExist = TblQuotes::findOne($quote_id);

				if($quoteExist){

					date_default_timezone_set("UTC");
					$milliseconds = strtotime(gmdate("Y-m-d H:i:s"));
				    	
						$noteModel = new TblNotes();

						try{

				        	$image = '';

				        	if(!empty($uploads)){
								foreach ($uploads as $file){				    		

							        if(isset($file->size)){		           
							            $varr = time().rand(1,100);
							            $file->saveAs('uploads/' . $file->baseName . '_note_' . $varr .'.' . $file->extension);
							        }

							        $image = $file->baseName . '_note_' . $varr .'.' . $file->extension;

								} //foreach
				        	}//if

							/* Insert data in Contact table */	

				        		$noteModel->quote_id   = $quote_id;
				        		$noteModel->note       = $note;
				        		$noteModel->image      = $image;
				        		$noteModel->created_at = $milliseconds;
				        		$noteModel->updated_at = $milliseconds;

				        		if($noteModel->save(false)){

									$noteId = $noteModel->note_id;

										$finalImage = ($image) ? $siteURL.'/'.$image : '';

								    	$arr['note_id']  = $noteId;
								    	$arr['quote_id'] = $quote_id;						    	
								    	$arr['note']     = $note;
								    	$arr['image']    = $finalImage;

									      $response = [
									        'success' => '1',
									        'message' => 'Note saved successfully!',
									        'data' => $arr,
									      ];
								}		

						}
						catch (\yii\base\Exception $exception) {
						  $response = [
					        'success' => '0',
					        'message' => 'There is an error saving your data.',
					        'data' => $arr,
					      ];
						}

				}else{

				  $response = [
			        'success' => '0',
			        'message' => 'QuoteId doesnot exist.',
			        'data' => $arr,
			      ];
				} //else

	    } //else

	    ob_start();
	    echo json_encode($response);    	

    }//actionCreateNote



	public function actionUpdateQuote()
    {

		$siteURL = Url::base(true). '/uploads';

    	/* mandaory fields */
	    $cnt_id   = !empty(Yii::$app->request->post('contact_id'))?Yii::$app->request->post('contact_id'):'';
	    $sub_id   = !empty(Yii::$app->request->post('subscriber_id'))?Yii::$app->request->post('subscriber_id'):'';
	    $quote_id = !empty(Yii::$app->request->post('quote_id'))?Yii::$app->request->post('quote_id'):'';

	    /* optional fields */
		$description      = !empty(Yii::$app->request->post('description'))?Yii::$app->request->post('description'):'';
		$client_name      = !empty(Yii::$app->request->post('client_name'))?Yii::$app->request->post('client_name'):'';
	    $s_name           = !empty(Yii::$app->request->post('s_name'))?Yii::$app->request->post('s_name'):'';
	    $s_email          = !empty(Yii::$app->request->post('s_email'))?Yii::$app->request->post('s_email'):'';
	    $s_phone          = !empty(Yii::$app->request->post('s_phone'))?Yii::$app->request->post('s_phone'):'';
	    $s_street1        = !empty(Yii::$app->request->post('s_street1'))?Yii::$app->request->post('s_street1'):'';
	    $s_street2        = !empty(Yii::$app->request->post('s_street2'))?Yii::$app->request->post('s_street2'):'';
	    $s_suburb         = !empty(Yii::$app->request->post('s_suburb'))?Yii::$app->request->post('s_suburb'):'';
	    $s_state          = !empty(Yii::$app->request->post('s_state'))?Yii::$app->request->post('s_state'):'';
	    $s_postal         = !empty(Yii::$app->request->post('s_postal'))?Yii::$app->request->post('s_postal'):'';
	    $s_country        = !empty(Yii::$app->request->post('s_country'))?Yii::$app->request->post('s_country'):'';
	    $s_formatted_addr = !empty(Yii::$app->request->post('s_formatted_addr'))?Yii::$app->request->post('s_formatted_addr'):'';
	    $s_lat            = !empty(Yii::$app->request->post('s_lat'))?Yii::$app->request->post('s_lat'):'';
		$s_long           = !empty(Yii::$app->request->post('s_long'))?Yii::$app->request->post('s_long'):'';
		$is_checked       = !empty(Yii::$app->request->post('s_checked'))?Yii::$app->request->post('s_checked'):'';

	    $response = [];

        $uArr = array();

	    // validate
	    if(empty($cnt_id) || empty($sub_id) || empty($quote_id)){
	      $response = [
	        'success' => '0',
	        'message' => 'Fields cannot be blank!',
	        'data' => '',
	      ];
	    }
	    else{

	        // search in the database, there is no email referred
	        $subscriber = \common\models\User::findIdentity($sub_id);

	        if(!empty($subscriber)){

				date_default_timezone_set("UTC");
				$milliseconds = strtotime(gmdate("Y-m-d H:i:s"));
	        	/* insert in DB */

				$model = TblContacts::find()	        				   						
						->joinWith('address') 		        							
						->where(['contact_id' => $cnt_id])		    						
						->all();

				//echo '<pre>'; print_r($model); exit;
				/*echo 'here'.$model[0]->address->address_id;
				exit;*/

				if( (!empty($s_street1) && $s_street1 != '') || (!empty($s_street2) && $s_street2 != '') || (!empty($s_suburb) && $s_suburb != '')  || (!empty($s_state) && $s_state != '') || (!empty($s_postal) &&    $s_postal != '') || (!empty($s_country) && $s_country != '') || (!empty($s_formatted_addr) && $s_formatted_addr != '') || (!empty($s_lat) && $s_lat != '') || (!empty($s_long) && $s_long != '') || (!empty($is_checked)))
				{

				    $cname  = ($s_name  != '') ? $s_name  : $model[0]['name'];
					$cemail = ($s_email != '') ? $s_email : $model[0]['email'];
					$cphone = ($s_phone != '') ? $s_phone : $model[0]['phone'];

	        		$quoteExist = TblQuotes::findOne($quote_id);

					if($quoteExist){

						$addrId = $quoteExist->site_address_id;
						$type   = $quoteExist->type;

						if($addrId == null || $addrId == 'null' || $addrId == ''){
							$addrId = $model[0]->address->address_id;
						}

						$addressModel = TblAddress::findOne($addrId);


						$addressModel->street1           = $s_street1;
						$addressModel->street2           = $s_street2;
						$addressModel->suburb            = $s_suburb;
						$addressModel->client_state      = $s_state;
						$addressModel->country_id        = $s_country;
						$addressModel->postal_code       = $s_postal;
						$addressModel->formatted_address = $s_formatted_addr;
						$addressModel->lat               = $s_lat;
						$addressModel->lng               = $s_long;
						$addressModel->s_name            = $cname;
						$addressModel->s_email           = $cemail;
						$addressModel->is_checked        = $is_checked;
						$addressModel->s_phone           = $cphone;
						$addressModel->updated_at        = $milliseconds;

							if($addressModel->save(false)){

								$quoteExist->contact_id       = $cnt_id;
								$quoteExist->subscriber_id    = $sub_id;
								$quoteExist->description      = $description;
								$quoteExist->site_address_id  = $addrId;	
								$quoteExist->contact_email    = $s_email;
								$quoteExist->contact_name     = $s_name;
								$quoteExist->contact_number   = $s_phone;
								$quoteExist->client_name	  = $client_name;

								//$quoteExist->created_at       = $milliseconds;
								$quoteExist->updated_at       = $milliseconds;

								if($quoteExist->save(false)){


									//echo "<pre>"; print_r($model); exit;

									foreach($model as $contact){
										
										$name  = $contact['name'];
										$email = $contact['email'];
										$phone = $contact['phone'];
										$image = $siteURL . '/' . $contact['image'];

										// get data from Address relation model
										$street1        = $contact->address->street1;
										$street2        = $contact->address->street2;
										$suburb         = $contact->address->suburb;
										$state          = $contact->address->client_state;
										$postal         = $contact->address->postal_code;
										$country        = $contact->address->country_id;
										$formatted_addr = $contact->address->formatted_address;
										$lat            = $contact->address->lat;
										$lng            = $contact->address->lng;

										$uArr['quote_id']    = $quote_id;
										$uArr['description'] = $description;
										$uArr['status']      = 0;
										$uArr['type']		 = $type;	

										/* user details */
										$uArr['contact']['name']           = $name;
										$uArr['contact']['email']          = $email;
										$uArr['contact']['phone']          = $phone;
										$uArr['contact']['street1']        = $street1;
										$uArr['contact']['street2']        = $street2;
										$uArr['contact']['suburb']         = $suburb;
										$uArr['contact']['state']          = $state;
										$uArr['contact']['postal']         = $postal;
										$uArr['contact']['country']        = $country;
										$uArr['contact']['formatted_addr'] = $formatted_addr;
										$uArr['contact']['lat']            = $lat;
										$uArr['contact']['lng']            = $lng;
										$uArr['contact']['image']          = $image;
										$uArr['contact']['contact_id']     = $cnt_id;

										/* site details */
										$uArr['site']['name']           = $cname;
										$uArr['site']['email']          = $cemail;
										$uArr['site']['phone']          = $cphone;
										$uArr['site']['street1']        = $s_street1;
										$uArr['site']['street2']        = $s_street2;
										$uArr['site']['suburb']         = $s_suburb;
										$uArr['site']['state']          = $s_state;
										$uArr['site']['postal']         = $s_postal;
										$uArr['site']['country']        = $s_country;
										$uArr['site']['formatted_addr'] = $s_formatted_addr;
										$uArr['site']['lat']            = $s_lat;
										$uArr['site']['lng']            = $s_long;
										$uArr['site']['s_checked']      = $is_checked;
										
										/* counts */
										$uArr['counts']['jss']      = 10;
										$uArr['counts']['contacts'] = 15;
										$uArr['counts']['quotes']   = 8;
										$uArr['counts']['docs']     = 12;

									}//foreach

									/*
									// Updating Quote Price
									$mydata['quote_id'] = $quote_id;
									$quotePrice = $this->getQuotePrice($mydata);
									$totlIncGst = $quotePrice['totlIncGst'];
									$quoteExist->price          = $totlIncGst;
									$quoteExist->save(false);
									*/

								}

							}

						}else{
				          $response = [
				            'success' => '0',
				            'message' => 'Quote doesnot exist in database!',
				            'data' => '',
				          ];
				        }	

				}else{

					$cname  = $model[0]['name'];
					$cemail = $model[0]['email'];
					$cphone = $model[0]['phone'];

					$quotesModel = TblQuotes::findOne($quote_id);

					$addrId = $quotesModel->site_address_id;
					$type   = $quotesModel->type;

					if($addrId == null || $addrId == 'null' || $addrId == ''){
						$addrId = $model[0]->address->address_id;
					}			        	

					$quotesModel->contact_id          = $cnt_id;
					$quotesModel->subscriber_id       = $sub_id;
					$quotesModel->description         = $description;
					$quotesModel->site_address_id     = $addrId;	
					$quotesModel->contact_email       = $s_email;
					$quotesModel->contact_name        = $s_name;
					$quotesModel->contact_number      = $s_phone;

					$quotesModel->created_at          = $milliseconds;
					$quotesModel->updated_at          = $milliseconds;

					if($quotesModel->save(false)){

						/*$model = TblContacts::find()	        				   						
									->joinWith('address') 		        							
									->where(['contact_id' => $cnt_id])
									->all();*/

							//echo "<pre>"; print_r($model); exit;

						foreach($model as $contact){
							
							$name  = $contact['name'];
							$email = $contact['email'];
							$phone = $contact['phone'];
							$image = $siteURL . '/' . $contact['image'];

							// get data from Address relation model

							$addressId      = $contact->address->address_id;
							$street1        = $contact->address->street1;
							$street2        = $contact->address->street2;
							$suburb         = $contact->address->suburb;
							$state          = $contact->address->client_state;
							$postal         = $contact->address->postal_code;
							$country        = $contact->address->country_id;
							$formatted_addr = $contact->address->formatted_address;
							$lat            = $contact->address->lat;
							$lng            = $contact->address->lng;
							$s_checked      = $contact->address->is_checked;

							$uArr['quote_id']    = $quote_id;
							$uArr['description'] = $description;
							$uArr['status']      = 0;
							$uArr['type']        = $type;

							/* user contact */
							$uArr['contact']['name'] = $name;
							$uArr['contact']['email'] = $email;
							$uArr['contact']['phone'] = $phone;
							$uArr['contact']['street1'] = $street1;
							$uArr['contact']['street2'] = $street2;
							$uArr['contact']['suburb'] = $suburb;
							$uArr['contact']['state'] = $state;
							$uArr['contact']['postal'] = $postal;
							$uArr['contact']['country'] = $country;
							$uArr['contact']['formatted_addr'] = $formatted_addr;
							$uArr['contact']['lat'] = $lat;
							$uArr['contact']['lng'] = $lng;
							$uArr['contact']['image'] = $image;
							$uArr['contact']['contact_id'] = $cnt_id;

							$uArr['site']['name'] = $cname;
							$uArr['site']['email'] = $cemail;
							$uArr['site']['phone'] = $cphone;
							$uArr['site']['street1'] = $street1;
							$uArr['site']['street2'] = $street2;
							$uArr['site']['suburb'] = $suburb;
							$uArr['site']['state'] = $state;
							$uArr['site']['postal'] = $postal;
							$uArr['site']['country'] = $country;
							$uArr['site']['formatted_addr'] = $formatted_addr;
							$uArr['site']['lat'] = $lat;
							$uArr['site']['lng'] = $lng;
							$uArr['site']['s_checked'] = $s_checked;																						


							$uArr['counts']['jss']      = 10;
							$uArr['counts']['contacts'] = 15;
							$uArr['counts']['quotes']   = 8;
							$uArr['counts']['docs']     = 12;


						}//foreach

						$addrModel = new TblAddress();

						$addrModel->street1           = $street1;
						$addrModel->street2           = $street2;
						$addrModel->suburb            = $suburb;
						$addrModel->client_state      = $state;
						$addrModel->country_id        = $country;
						$addrModel->postal_code       = $postal;
						$addrModel->formatted_address = $formatted_addr;
						$addrModel->lat               = $lat;
						$addrModel->lng               = $lng;
						$addrModel->s_name            = $cname;
						$addrModel->s_email           = $cemail;
						$addrModel->s_phone           = $cphone;											        	        
						$addrModel->updated_at        = $milliseconds;

						$addrModel->save(false);

						/*
						// Updating Quote Price
						$mydata['quote_id'] = $quote_id;
						$quotePrice = $this->getQuotePrice($mydata);
						$totlIncGst = $quotePrice['totlIncGst'];
						$quotesModel->price          = $totlIncGst;
						$quotesModel->save(false);
						*/
					}

				}

				$response = [
					'success' => '1',
					'message' => 'Quote successfully updated!',
					'data' => $uArr,
				];

	        }
	        // If the email is not found make a response like this
	        else{
	          $response = [
	            'success' => '0',
	            'message' => 'Subscriber doesnot exist in database!',
	            'data' => '',
	          ];
	        }	

	  	}

	    ob_start();
	    echo json_encode($response);    

	} //actionUpdateQuote


	public function actionUpdateStatus(){

	    $quoteId = !empty(Yii::$app->request->post('quote_id'))?Yii::$app->request->post('quote_id'):'';
	    $status  = !empty(Yii::$app->request->post('status'))?Yii::$app->request->post('status'):'';
	    
	    // validate
	    if(empty($quoteId) || empty($status)){
	      $response = [
	        'success' => '0',
	        'message' => 'Fields cannot be blank!'
	      ];
	    }
	    else{
			$quoteExist = TblQuotes::findOne($quoteId);

			if($quoteExist){
				$quoteExist->status = $status;

				if($quoteExist->save(false)){
				      $response = [
				        'success' => '1',
				        'message' => 'Quote status updated successfully.'
				      ];
				}
			}else{
		      $response = [
		        'success' => '0',
		        'message' => 'Quote does not exist.'
		      ];
			}
	    }

	    ob_start();
	    echo json_encode($response);    	    

	} //actionUpdateStatus

	public function actionGeneratePdf(){

		$response = [];

        $modelQuery        = array();
        $data              = array();
        $qdata             = array();
        $cdata             = array();
        $quote_description = '';
        $client_name       = '';
        $client_email      = '';
        $client_phone      = '';
        $client_addr       = '';
        $sub_email         = '';
        $sub_name          = '';
        $sub_phone         = '';
        $siteAddress       = '';

		/*print_r(json_decode(file_get_contents("php://input"), true));
		exit;
		*/

        //$mydata = json_decode(file_get_contents("php://input"), true);
        $mydata = ($_POST['json'] != '')?$_POST['json']:'';

        if($mydata != ''){
        	$mydata = json_decode($_POST['json'], true);
	        $qid = $mydata['qid'];
	        //echo 'qid'.$mydata['qid']; exit;

	        if(!empty($qid)){
	          $modelQuery = TblQuotes::find()->joinWith('contact')->joinWith('siteAddress')->where([ 'tbl_quotes.quote_id'=>$qid])->orderBy('quote_id DESC')->all();
	          //echo "<pre>"; print_r($modelQuery); exit;

	          if(count($modelQuery)>0){

	                $subId = $modelQuery[0]->subscriber_id;

	                $userData = TblUsers::find()
	                                ->joinWith('tblUsersDetails')                             
	                                ->where([ 'tbl_users.user_id' => $subId])
	                                ->all();

	                //echo '<pre>'; print_r($userData); exit;

	                    if(count($userData)>0){

	                        $sub_email         = $userData[0]->email;
	                        $sub_name          = $userData[0]->tblUsersDetails[0]->name;
	                        $sub_phone         = $userData[0]->tblUsersDetails[0]->phone;

	                    }

	                    $quote_description = $modelQuery[0]->description;
	                    $quote_date        = date('d/m/Y',$modelQuery[0]->created_at);
	                    $client_name       = $modelQuery[0]->contact->name;
	                    $client_email      = $modelQuery[0]->contact->email;
	                    $client_phone      = $modelQuery[0]->contact->phone;
	                    $client_addid      = $modelQuery[0]->contact->address_id;

	                    if($client_addid != ''){
	                        $model = TblAddress::find()
	                                   ->select('formatted_address')
	                                   ->where(['address_id' => $client_addid])
	                                   ->one();

	                        $client_addr = $model->formatted_address;
	                    }                   

	                    $siteAddress       = $modelQuery[0]->siteAddress->formatted_address;    

	                      /* client data */
	                      $cdata['cname']  = $client_name;
	                      $cdata['cemail'] = $client_email;
	                      $cdata['cphone'] = $client_phone;

	                      /* quote data */
	                      $qdata['number']      = $qid;
	                      $qdata['description'] = $quote_description;
	                      $qdata['date']        = $quote_date;

	          }

	        }

	        $header = 'INTERIOR PAINTING QUOTE';
	        $data   = $mydata;

	        //$mpdf   = new \Mpdf\Mpdf(['','','','','margin_left'=>8,'margin_right'=>8,'','','','','']);
	        $mpdf   = new \Mpdf\Mpdf(['margin_left'=>8,'margin_right'=>8,'margin_top'=>8]);

	        $mpdf->SetHTMLFooter('
	        <table width="100%">
	            <tr>
	                <td width="33%"></td>
	                <td width="33%" align="center">{PAGENO}/{nbpg}</td>
	                <td width="33%" style="text-align: right;"></td>
	            </tr>
	        </table>');

	        
	        $mpdf->WriteHTML($this->renderPartial('/mpdf/quote',array('header'=>$header, 'cdata'=>$cdata, 'qdata'=>$qdata, 'data'=>$data, 'sub_email'=>$sub_email, 'sub_name'=>$sub_name, 'sub_phone'=>$sub_phone,'siteAddress'=>$siteAddress,'client_addr'=>$client_addr)));

	        $filename = 'quote'.$qid.'_'.time().'.pdf';

		    $mpdf->Output('quotepdf/'.$filename,'F');

		    	$siteURL = Url::base(true). '/quotepdf';

		        $path = $siteURL.'/'.$filename;

				$response = [
					'success' => '1',
					'message' => $path
				];

		}else{

			$response = [
				'success' => '0',
				'message' => 'Data cannot be blank.'
			];
		}


	    ob_start();
	    echo json_encode($response);    	    

	} //actionGeneratePdf



	public function actionGenerateJsspdf(){

        $modelQuery        = array();
        $data              = array();
        $qdata             = array();
        $cdata             = array();
        $quote_description = '';
        $client_name       = '';
        $client_email      = '';
        $client_phone      = '';
        $client_addr       = '';
        $sub_email         = '';
        $sub_name          = '';
        $sub_phone         = '';
        $siteAddress       = '';


        $siteURL = Url::base(true);
        $totalArr = [];
        $mydata = json_decode($_POST['json'], true);

        if($mydata != ''){

        	$imageNoteDesc = [];
			//if(count($mydata['siteNotes'])>0){

				if($mydata['imageCount'] > 0){

					for($i = 0; $i < $mydata['imageCount']; $i++){

						$uploads = UploadedFile::getInstancesByName("image_".$i);
						foreach ($uploads as $ik => $file){

							if(isset($file->size)){
								$imageName = $file->name;
								/*
									$nameArr = explode('.', $file->name);
									$nameArrCount = count($nameArr);
									$ext = $nameArr[$nameArrCount-1];
									$imageName = '';
									$varr = time().rand(1,100);
									if($nameArrCount > 2){
										unset($nameArr[$nameArrCount-1]);
										$imageName = implode('.', $nameArr);
										$imageName .= '_'.$varr.'.'.$ext;
									}
									else{
										$imageName = $nameArr[0].'_'.$varr.'.'.$ext;
									}
								*/
								//echo $imageName;exit();
								$file->saveAs('notes/' . $imageName);
								/*
									if(array_key_exists($ik, $mydata['siteNotes'])) {
										$imageNoteDesc[$ik]['image'] = $siteURL.'/notes/'.$imageName;
										$imageNoteDesc[$ik]['desc'] = $mydata['siteNotes'][$ik]['description'];
									}
								*/

							}

						} //foreach
					}
				}
			//}

			//echo "test upload";exit();

	        $qid        = $mydata['qid'];
	        $image      = $mydata['image'];
	        $brand_logo = $mydata['brand_logo'];
	        $header = 'Job Specification';
	        
	        //echo 'qid'.$mydata['qid']; exit;

	        if(!empty($qid)){

	            $modelQuery = TblQuotes::find()->joinWith('contact')->joinWith('siteAddress')->where([ 'tbl_quotes.quote_id'=>$qid])->orderBy('quote_id DESC')->all();
	            //echo "<pre>"; print_r($modelQuery); exit;

	            if(count($modelQuery)>0){

	                $subId = $modelQuery[0]->subscriber_id;

	                $userData = TblUsers::find()
	                                ->joinWith('tblUsersDetails')                             
	                                ->where([ 'tbl_users.user_id' => $subId])
	                                ->all();

	                //echo '<pre>'; print_r($userData); exit;

	                    if(count($userData)>0){
	                        $sub_email         = $userData[0]->email;
	                        $sub_name          = $userData[0]->tblUsersDetails[0]->name;
	                        $sub_phone         = $userData[0]->tblUsersDetails[0]->phone;
	                    }

	                    $quote_description = $modelQuery[0]->description;
	                    $quote_date        = date('d/m/Y',$modelQuery[0]->created_at);

	                    $client_name       = $modelQuery[0]->contact->name;
	                    $client_email      = $modelQuery[0]->contact->email;
	                    $client_phone      = $modelQuery[0]->contact->phone;
	                    $siteAddress       = $modelQuery[0]->siteAddress->formatted_address;           

	                    $client_addid      = $modelQuery[0]->contact->address_id;

	                    if($client_addid != ''){
	                        $model = TblAddress::find()
	                                   ->select('formatted_address')
	                                   ->where(['address_id' => $client_addid])
	                                   ->one();

	                        $client_addr = $model->formatted_address;
	                    }                   



	            }
	        }


	        $data   = $mydata;

	        $mpdf   = new \Mpdf\Mpdf(['margin_left'=>8,'margin_right'=>8,'margin_top'=>8]);

	        $mpdf->SetHTMLHeader('<div>
	                            <table>
	                                <tr>
	                                    <td style="height: 15px"></td>
	                                </tr>
	                            </table>                           
								<table cellpadding="0" cellspacing="0" style="border-bottom: 2px solid #e1e1e1; width:100%;">
									<tr style=" font-size: 12px;">
										<td style="text-align: left;vertical-align: bottom;"> <div> <span> <img src="'.$brand_logo.'" alt="" style="width:100px;transform: translate(0, 50%);"> </span></div></td>
										<td style="">&nbsp;</td>
										<td style="">&nbsp;</td>
										<td style="text-align: right; vertical-align: bottom;padding-bottom: 4px;"><h1 style="color:#0a80c5; font-size: 19px; font-weight: normal; text-align: right;">JOB SPECIFICATION</h1></td>
									</tr>									
									<tr style=" font-size: 12px; text-align: right; float: right;">
										<td style="height:2px">&nbsp;</td>
										<td style="height:2px">&nbsp;</td>
										<td style="height:2px">&nbsp;</td>										
										<td style="text-align: right">
										    <table class="" style="border:0px; background:#ececec;">
												<tr>
							                        <td style=" border:0px;padding: 5px 8px 0px 5px;color:#525251;font-weight: normal;text-align: left;">
								                        <table class="" style="border:0px; background:#ececec;">
								                        <tr>
								                        <td><b style="color:#0a86c2; font-size: 12px; text-transform: uppercase;">Quote No.</b></td>
								                        </tr>
								                        <tr>
								                        <td style="font-size: 11px;color:#62615e">'.$qid.'</td>
								                        </tr>
								                        </table>
							                        </td>							                        
							                        <td style=" border:0px;padding: 5px 5px 0px 2px;color:#525251; font-weight: normal;text-align: left;">
								                        <table class="" style="border:0px; background:#ececec;">
								                        <tr>
								                        <td><b style="color:#0a86c2; font-size: 12px; text-transform: uppercase; ">Quote Date</b></td>
								                        </tr>
								                        <tr>
								                        <td style="font-size: 11px;color:#62615e">'.$quote_date.'</td>
								                        </tr>
								                        </table>
							                        </td>
							                        <td style=" border:0px;padding: 5px 5px 0px 5px;color:#525251; text-align: left;">
								                        <table class="" style="border:0px;">
								                        <tr>
								                        <td><img width= "100px" style="padding: 0;" src="'.$image.'" alt=""></td>
								                        </tr>
								                        <tr>
								                        <td></td>
								                        </tr>
								                        </table>
							                        </td>
							                    </tr>
							                </table>
										</td>
									</tr>											
								</table>
	                        </div> 
	                        <div>
	                            <table>
	                                    <tr>
	                                        <td style="height: 10px"></td>
	                                    </tr>
	                            </table>    
	                            <table>     
	                                <tr>
	                                    <td style="width:18%; margin-bottom: 13px;color:#0a80c5; font-size: 13px; font-weight: bold; float: left;width: 100%; text-align: left;"><b>'.ucfirst($client_name).'</b></td>
	                                    <td style="width:30%; margin-bottom: 13px;color:#0a80c5; font-size: 13px; font-weight: bold; float: left;width: 100%; text-align: left;"><b>Site Details</b></td>
	                                    <td style="width:10%; margin-bottom: 13px;color:#0a80c5; font-size: 13px; font-weight: bold; float: left;width: 100%; text-align: left;"><b>Quoted by</b></td>
	                                </tr>
	                                <tr>
	                                    <td style="font-size: 11px;color:#62615e"><b>Contact: '.$client_name.'</b></td>
	                                    <td style="font-size: 11px;color:#62615e"><b>Contact: '.$client_name.'</b></td>
	                                    <td style="font-size: 11px;color:#62615e"><b>'.ucfirst($sub_name).'</td>
	                                </tr>
	                                <tr>
	                                    <td style="font-size: 11px;color:#62615e"><b>Mobile: '.$client_phone.'</b></td>
	                                    <td style="font-size: 11px;color:#62615e"><b>Mobile: '.$client_phone.'</b></td>
	                                    <td style="font-size: 11px;color:#62615e">'.$sub_phone.'</td>
	                                </tr>
	                                <tr>
	                                    <td style="font-size: 11px;color:#62615e">'.$client_addr.'</td>
	                                    <td style="font-size: 11px;color:#62615e;vertical-align: text-top;">'.$siteAddress.'</td>
	                                    <td style="font-size: 11px;color:#62615e;vertical-align: text-top;">'.$sub_email.'</td>
	                                </tr>   
	                            </table>
	                            <table>
	                                <tr>
	                                    <td style="height: 10px"></td>
	                                </tr>
                        		</table>
	                        </div>
	                        ');





	        $mpdf->SetHTMLFooter('
	        <table width="100%">
	            <tr>
	                <td width="33%"></td>
	                <td width="33%" align="center">{PAGENO}/{nbpg}</td>
	                <td width="33%" style="text-align: right;"></td>
	            </tr>
	        </table>');


	        $mpdf->AddPage('', // L - landscape, P - portrait 
	                '', '', '', '',
	                5, // margin_left
	                5, // margin right
	               65, // margin top
	               30, // margin bottom
	                0, // margin header
	                0); // margin footer

	        //$mpdf->WriteHTML('Hello World');

	        $mpdf->WriteHTML($this->renderPartial('/mpdf/jssnewcopy',array('header'=>$header, 'data'=>$data, 'quote_description'=>$quote_description, 'qid'=>$qid, 'quote_date'=>$quote_date,'siteURL'=>$siteURL)));

	        $filename = 'jss'.$qid.'_'.time().'.pdf';

		    $mpdf->Output('quotepdf/'.$filename,'F');

		    	$siteURL = Url::base(true). '/quotepdf';

		        $path = $siteURL.'/'.$filename;

			        $response = [
						'success' => '1',
						'message' => $path
					];

		}else{

			$response = [
				'success' => '0',
				'message' => 'Data cannot be blank.'
			];
		}


	    ob_start();
	    echo json_encode($response);    	    

	} //actionGenerateJsspdf



	/*
	* Author : Jotpal Singh
	* Date : 2018/10/02
	* Comment : To save Paint Defauts in Brand Pref and Paint Defaults Table
	*/
	public function actionSavePaintDefaults(){
		$response = [];
		if(empty($_POST['json'])){
			$response = [
				'success' => '0',
				'message' => 'Fields cannot be blank!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}

		$mydata = ($_POST['json'] != '')?$_POST['json']:json_encode(array());
		$mydata = json_decode($mydata,true);

		//echo("<pre>");print_r($mydata);exit();

		if(empty($mydata['quote_id'])){
			$response = [
				'success' => '0',
				'message' => 'Fields cannot be blank!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}
		$quote_id = $mydata['quote_id'];
		$brandPref = new TblBrandPref();
		if($mydata['brand_pref_id']!=0){	// To check if update or create
			$brandPref = TblBrandPref::findOne($mydata['brand_pref_id']);
			TblPaintDefaults::deleteAll('quote_id = '.$mydata['quote_id']);
		}
		else{
			$brandPref = new TblBrandPref();
		}
		// Statics
		$brandPref->name = '';
		$brandPref->device_id = '';
		$brandPref->unique_id = '';
		$brandPref->flag = 0;
		// Dynamic
		$brandPref->quote_id = $mydata['quote_id'];
		$brandPref->tier_id = $mydata['tier_id'];
		$brandPref->brand_id = $mydata['tbl_brand_pref']['brand_id'];
		$brandPref->coats = $mydata['tbl_brand_pref']['coats'];
		$brandPref->prep_level = $mydata['tbl_brand_pref']['prep_level'];
		$brandPref->apply_undercoat = $mydata['tbl_brand_pref']['apply_undercoat'];
		$brandPref->color_consultant = $mydata['tbl_brand_pref']['color_consultant'];
		$brandPref->created_at = strtotime(gmdate('Y-m-d H:i:s'));
		$brandPref->updated_at = strtotime(gmdate('Y-m-d H:i:s'));


		if($brandPref->validate()){
			$brandPref->save();
			$success=1;
			$message='Data saved successfully.';
			$brand_pref_id = $brandPref->id;
		}
		else{
			$brand_pref_id = 0;
			$success=0;
			$message='Unable to save Brand Pref Data.';
		}

		foreach ($mydata['paint_default'] as $key => $component) {
			$paintDefaults = new TblPaintDefaults();
			// Static
			$paintDefaults->name = '';
			$paintDefaults->device_id = '';
			$paintDefaults->unique_id = '';
			$paintDefaults->flag = 0;
			// Dynamic
			$paintDefaults->quote_id = $mydata['quote_id'];
			$paintDefaults->tier_id = $mydata['tier_id'];
			$paintDefaults->color_id = $component['color_id'];
			$paintDefaults->custom_name = $component['custom_name'];			
			$paintDefaults->comp_id = $component['comp_id'];
			$paintDefaults->sheen_id = $component['sheen_id'];
			$paintDefaults->topcoat = $component['topcoat'];
			$paintDefaults->strength = $component['strength'];
			$paintDefaults->hasUnderCoat = $component['hasUnderCoat'];
			$paintDefaults->undercoat = $component['undercoat'];
			$paintDefaults->created_at = strtotime(gmdate('Y-m-d H:i:s'));
			$paintDefaults->updated_at = strtotime(gmdate('Y-m-d H:i:s'));
			if($paintDefaults->validate()){
				$success=1;
				$message='Data saved successfully.';
				$paintDefaults->save();
			}
			else{
				$success=0;
				$message='Unable to save Paint Default data.';
			}
		}

		//echo json_encode($mydata);exit;
		
		$quotePrice = $this->getQuotePrice($mydata);
		$totlIncGst = $quotePrice['totlIncGst'];
		TblQuotes::updateAll(['price' => $totlIncGst], ['=', 'quote_id', $quote_id]);

		
		echo json_encode(['success'=>$success,'message'=>$message,'data'=>array('brand_pref_id'=>$brand_pref_id)]);
		exit();
	}

	/*
	* Author : Jotpal Singh
	* Date : 2018/10/02
	* Comment : To get Paint Defauts in Brand Pref and Paint Defaults Table
	*/
	public function actionGetPaintDefaults(){

		/*$quoteId = !empty(Yii::$app->request->get('quote_id'))?Yii::$app->request->get('quote_id'):'';
		$type_id = !empty(Yii::$app->request->get('type_id'))?Yii::$app->request->get('type_id'):'';
		$sub_id = !empty(Yii::$app->request->get('subscriber_id'))?Yii::$app->request->get('subscriber_id'):'';*/


		$mydata = (isset($_POST['json']) && $_POST['json']!="")?$_POST['json']:json_encode(array());

		$mydata = json_decode($mydata,true);

		if(empty($mydata['quote_id']) || empty($mydata['type_id']) || empty($mydata['subscriber_id'])){
			$response = [
				'success' => 0,
				'message' => 'Fields cannot be blank!',
				'data' => []
			];
			echo json_encode($response);exit();
		}

		$quoteId=$mydata['quote_id'];
		$type_id=$mydata['type_id'];
		$sub_id=$mydata['subscriber_id'];

		$response = [];
		$bpArr = [];
		$pdArr = [];
		$tiers = [];
		$finalArr = [];
		$brand_id = '';
		$tier_id = '';

		$connection = Yii::$app->getDb();
		$command = $connection->createCommand("SELECT tq.*,tq.quote_id as main_quote_id ,tbp.* , tbp.id as brand_pref_id, tbp.quote_id as brand_pref_quote_id ,tpd.*,tpd.id as paint_default_id ,tc.*,tc.name as color_name FROM tbl_quotes as tq LEFT JOIN tbl_brand_pref as tbp ON tq.quote_id=tbp.quote_id LEFT JOIN tbl_paint_defaults as tpd ON tq.quote_id=tpd.quote_id LEFT JOIN tbl_colors AS tc ON tpd.color_id=tc.color_id WHERE tq.quote_id=".$quoteId);
		$components = $command->queryAll();


		

		foreach ($components as $key => $comp) {
			if($comp['brand_pref_quote_id']){

				$pdMArr = [];

				$tier_id = $bpArr['tier_id']        = $comp['tier_id'];
				$brand_id = $bpArr['brand_id']       = $comp['brand_id'];

				$bpArr['id']             = $comp['brand_pref_id'];
				$bpArr['coats']          = $comp['coats'];
				$bpArr['prep_level']       = $comp['prep_level'];
				$bpArr['apply_undercoat']       = $comp['apply_undercoat'];
				$bpArr['color_consultant']      = $comp['color_consultant'];


				$pdMArr['tier_id']        = $comp['tier_id'];
				$pdMArr['id']             = $comp['paint_default_id'];
				$pdMArr['comp_id'] 		  = $comp['comp_id']; 

				$pdMArr['tt_image'] 	  = "https://www.familyhandyman.com/wp-content/uploads/2017/05/FH14FEB_PTCEIL_04.jpg"; 
				$pdMArr['tt_text'] 		  = "A ceiling is an overhead interior surface that covers the upper limits of a room. It is not generally considered a structural element, but a finished surface concealing the underside of the roof structure or the floor of a storey above. Ceilings can be decorated to taste, and there are many fine examples of frescoes and artwork on ceilings especially in religious buildings."; 
				$pdMArr['tt_url'] 		  = "https://en.wikipedia.org/wiki/Ceiling"; 

				$pdMArr['sheen_id']       = $comp['sheen_id'];
				$pdMArr['topcoat']        = $comp['topcoat'];
				$pdMArr['strength']       = $comp['strength'];
				if($comp['color_id']){
					$pdMArr['color_id']   = ["id"=>$comp['color_id'], "name"=>$comp['color_name'], "hex"=>$comp['hex'], "tag_id"=>$comp['tag_id'], "number"=>$comp['number']];
				}
				else{
					$pdMArr['color_id']   = $comp['color_id'];
				}
				$pdMArr['hasUnderCoat']   = $comp['hasUnderCoat'];  
				$pdMArr['undercoat']      = $comp['undercoat'];
				$pdMArr['custom_name']    = $comp['custom_name'];
				$pdArr[$comp['comp_id']] = $pdMArr;

			}
		}

		/*echo"<pre>";print_r($bpfArr);
		echo"<pre>";print_r($pdArr);exit();*/

/*
		$modelQuery = TblQuotes::find()->joinWith('paintDefaults')->joinWith('brandPref')->where([ 'tbl_quotes.quote_id' => $quoteId]);
		$quotes = $modelQuery->one();
		//echo '<pre>'; print_r($quotes); exit;
		if(isset($quotes->brandPref) && !empty($quotes->brandPref) ){
			if(!empty($quotes->brandPref)){
				$bpf = $quotes->brandPref;

				$tier_id = $bpfArr['tier_id']        = $bpf->tier_id;
				$brand_id = $bpfArr['brand_id']       = $bpf->brand_id;

				$bpfArr['id']             = $bpf->id;
				$bpfArr['name']           = $bpf->name;				
				$bpfArr['coats']          = $bpf->coats;
				$bpfArr['prep_level']       = $bpf->prep_level;
				$bpfArr['apply_undercoat']       = $bpf->apply_undercoat;
				$bpfArr['color_consultant']      = $bpf->color_consultant;
				$bpArr=$bpfArr;
			}
		}

		if(isset($quotes->paintDefaults) && !empty($quotes->paintDefaults) ){
			if(!empty($quotes->paintDefaults)){
				$paintDefaults = $quotes->paintDefaults;
				foreach ($paintDefaults as $key => $pds) {
					$pdMArr['tier_id']        = $pds->tier_id;
					$pdMArr['id']             = $pds->id;
					$pdMArr['name']           = $pds->name;
					$pdMArr['comp_id'] 		  = $pds->comp_id; 
					$pdMArr['sheen_id']       = $pds->sheen_id;
					$pdMArr['topcoat']        = $pds->topcoat;
					$pdMArr['strength']       = $pds->strength;
					$pdMArr['color_id']       = $pds->color_id;
					$pdMArr['hasUnderCoat']   = $pds->hasUnderCoat;  
					$pdMArr['undercoat']      = $pds->undercoat;
					$pdMArr['custom_name']    = $pds->custom_name;
					$pdArr[$pds->comp_id] = $pdMArr;
				}
			}
		}
*/
		if($brand_id!=''){
			$tierModel = TblTiers::find()
							->joinWith('brand')
					        ->where(['tbl_tiers.brand_id' => $brand_id])
					        ->andWhere(['tbl_tiers.type_id'=>$type_id])				        
					        ->orderBy('tier_id DESC')
					        ->all();

			if(count($tierModel)>0){
				foreach($tierModel as $model){
					$tierArr = [];
					$tierArr['tier_id'] =  $model->tier_id;
					$tierArr['name']    =  $model->name;
					$tierArr['image']   =  $model->brand->logo;
					$tiers[] = $tierArr;
				}
			}
		}

		if($tier_id!=''){

			$grpModels = TblComponentGroups::find()
						->joinWith(
								['tblTierCoats' => 
									function($q) use ($tier_id){
										$q->where(['tbl_tier_coats.tier_id' => $tier_id]);
									}
								])
						->joinWith(['tblTierCoats.sheen','tblTierCoats.product'])
						->where(['in', ['tbl_component_groups.type_id'], [$type_id,3]])
						->andWhere(['tbl_component_groups.enabled' => 1])
						->all();

			if(count($grpModels) > 0){

				foreach($grpModels as $model){

					$dummy = array();

					$arr['id']                  = $model->group_id;
					$arr['name']                = $model->name;

					if(isset($model->tblTierCoats) && count($model->tblTierCoats)>0){

						$i = 0;
						$top_coat =  array();
						$under_coat =  array();
						$sFinalArr = array();
						$sArr = array();
						$myArr = array();
						foreach($model->tblTierCoats as $sheendata){

							if($sheendata->top_coats == 1){
								$top_coat['name'] = $sheendata->product->name;
								$top_coat['product_id'] = $sheendata->product->product_id;
								$top_coat['default'] = $sheendata->enabled;
								$myArr[$sheendata->sheen_id]['sheen_name'] = $sheendata->sheen->name;
								$myArr[$sheendata->sheen_id]['top_coats'][] = $top_coat;
							}
							else{
								$unders['name'] = $sheendata->product->name;
								$unders['product_id'] = $sheendata->product->product_id;
								$unders['default'] = $sheendata->enabled;
								$under_coat[] = $unders;
							}

						} //foreach

						foreach($myArr as $sheen_id => $mySheen){
							$sArr['sheen_id'] = $sheen_id;
							$sArr['name'] = $mySheen['sheen_name'];
							$sArr['top_coats'] = $mySheen['top_coats'];
							$sFinalArr[] = $sArr;
						}


					}else{

						$sFinalArr = array();
					}
					$arr['sheens'] = $sFinalArr;
					$arr['under_coat'] = $under_coat;

					$finalArr[] = $arr;

				} //foreach
				//echo '<pre>'; print_r($finalArr); exit;

			} //if

		}

		$data['tiers'] = $tiers;
		$data['brand_pref'] = $bpArr;
		$data['paint_default'] = $pdArr;
		$data['tier_types'] = $finalArr;

	    $response = [
				'success' => 1,
				'message' => 'Data found successfully!',
				'data' => $data
			];
		echo json_encode($response);exit();

	}

	/*
	* Author : Jotpal Singh
	* Date : 2018/10/04
	* Comment : To save Room Details in Rooms, Room Pref, Room Comp Pref and Room Dimesnions
	*/
	public function actionSaveRoomDetails(){

		$response = [];
		if(empty($_POST['json'])){
			$response = [
				'success' => '0',
				'message' => 'Fields cannot be blank!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}

		$mydata = ($_POST['json'] != '')?$_POST['json']:json_encode(array());
		$mydata = json_decode($mydata,true);

		if(empty($mydata['quote_id'])){
			$response = [
				'success' => '0',
				'message' => 'Fields cannot be blank!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}

		$quote_id =$mydata['quote_id'];
		$room = new TblRooms();
		$room_id = $mydata['tbl_rooms']['room_id'];
		if($room_id != 0){	// To check if update or create
			$room = TblRooms::findOne($mydata['tbl_rooms']['room_id']);

			TblRoomPref::deleteAll('room_id = '.$room_id);
			TblRoomCompPref::deleteAll('room_id = '.$room_id);
			TblRoomDimensions::deleteAll('room_id = '.$room_id);
			TblRoomSpecialItems::deleteAll('room_id = '.$room_id);
		}
		else{
			$room = new TblRooms();
		}
		// Statics

		// Dynamic
		$room->quote_id = $quote_id;
		$room->room_name = $mydata['tbl_rooms']['room_name'];
		$room->room_type_id = $mydata['tbl_rooms']['room_type_id'];
		$room->is_custom = $mydata['tbl_rooms']['is_custom'];
		if(array_key_exists('include', $mydata['tbl_rooms'])){
			$room->include = $mydata['tbl_rooms']['include'];
		}
		else{
			$room->include = 1;
		}
		if($room_id == 0){  $room->created_at = strtotime(gmdate('Y-m-d H:i:s')); }
		$room->updated_at = strtotime(gmdate('Y-m-d H:i:s'));

		if($room->validate()){
			$room->save();

			if($room_id != 0){	// To check if update or create
				
			}
			else{
				//TblBrandPref::find()->where([""])
			}

			$success=1;
			$message='Data saved successfully.';
			$room_id = $room->rooms_id;

			foreach ($mydata['tbl_room_pref'] as $key => $component) {
				$roomPref = new TblRoomPref() ;
				// Static
				$roomPref->name = '';
				$roomPref->device_id = '';
				$roomPref->unique_id = '';
				$roomPref->flag = 0;
				// Dynamic
				$roomPref->quote_id = $quote_id;
				$roomPref->comp_id = $component['comp_id'];
				$roomPref->room_id = $room_id;
				$roomPref->color_id = ($component['color_id'] == '' || is_null($component['color_id']) || empty($component['color_id']) ) ? 0 : $component['color_id'];
				$roomPref->isSelected = $component['isSelected'];
				$roomPref->isUndercoat = $component['isUndercoat'];
				$roomPref->created_at = strtotime(gmdate('Y-m-d H:i:s'));
				$roomPref->updated_at = strtotime(gmdate('Y-m-d H:i:s'));
				if($roomPref->validate()){
					$success=1;					
					$roomPref->save();
					$roomPrefId = $roomPref->id;
					if(count($component['room_comp_pref'])){
						foreach ($component['room_comp_pref'] as $key => $crcp) {
							$roomCompPref = new TblRoomCompPref();

							/*
							$roomCompPref->name = '';
							$roomCompPref->device_id = '';
							$roomCompPref->unique_id = '';
							$roomCompPref->flag = 0;
							*/

							$roomCompPref->room_id = $room_id;
							$roomCompPref->tbl_room_pref_id = $roomPrefId;
							$roomCompPref->option_id = $crcp['option_id'];
							$roomCompPref->option_value = $crcp['option_value'];
							$roomCompPref->created_at = strtotime(gmdate('Y-m-d H:i:s'));
							$roomCompPref->updated_at = strtotime(gmdate('Y-m-d H:i:s'));

							if($roomCompPref->validate()){
								$roomCompPref->save();
							}
							else{
								$success=0;
								$message='Unable to save Room Prefernce Components data.';
							}
						}
					}
				}
				else{
					$success=0;
					$message='Unable to save Room Prefernce data.';
				}
			}

			foreach ($mydata['tbl_room_dimensions'] as $key => $trd) {
				$roomDimensions = new TblRoomDimensions();
				$roomDimensions->name = '';
				$roomDimensions->device_id = '';
				$roomDimensions->unique_id = '';
				$roomDimensions->flag = 0;

				$roomDimensions->room_id = $room_id;
				$roomDimensions->length = $trd['length'];
				$roomDimensions->width = $trd['width'];
				$roomDimensions->height = $trd['height'];
				$roomDimensions->selected = $trd['selected'];
				$roomDimensions->created_at = strtotime(gmdate('Y-m-d H:i:s'));
				$roomDimensions->updated_at = strtotime(gmdate('Y-m-d H:i:s'));

				if($roomDimensions->validate()){
					$success=1;					
					$roomDimensions->save();
				}
				else{
					$success=0;
					$message='Unable to save Room Dimensions data.';
				}
			}


			if(array_key_exists('tbl_room_special_items', $mydata)){
				foreach ($mydata['tbl_room_special_items'] as $key => $trsi) {
					$roomSpecialItems = new TblRoomSpecialItems();
					$roomSpecialItems->name = '';
					$roomSpecialItems->device_id = '';
					$roomSpecialItems->unique_id = '';
					$roomSpecialItems->flag = 0;

					$roomSpecialItems->room_id = $room_id;
					$roomSpecialItems->item_name = $trsi['item_name'];
					$roomSpecialItems->price = $trsi['price'];
					$roomSpecialItems->qty = $trsi['qty'];
					$roomSpecialItems->include = $trsi['include'];
					$roomSpecialItems->created_at = strtotime(gmdate('Y-m-d H:i:s'));
					$roomSpecialItems->updated_at = strtotime(gmdate('Y-m-d H:i:s'));

					if($roomSpecialItems->validate()){
						$success=1;					
						$roomSpecialItems->save();
					}
					else{
						$success=0;
						$message='Unable to save Room Special items.';
					}
				}
			}

			if(array_key_exists('tbl_room_notes', $mydata)){
				foreach ($mydata['tbl_room_notes'] as $key => $trn) {
					$roomNotes = new TblRoomNotes();
					$roomNotes->name = '';
					$roomNotes->device_id = '';
					$roomNotes->unique_id = '';
					$roomNotes->flag = 0;

					$roomNotes->room_id = $room_id;
					$roomNotes->description = $trn['description'];
					$roomNotes->image = $trn['image'];
					$roomNotes->created_at = $trn['created_at'];
					$roomNotes->updated_at = strtotime(gmdate('Y-m-d H:i:s'));

					if($roomNotes->validate()){
						$success=1;
						$roomNotes->save();
					}
					else{
						$success=0;
						$message='Unable to save Room notes.';
					}
				}
			}
			$quotePrice = $this->getQuotePrice($mydata);
			$totlIncGst = $quotePrice['totlIncGst'];
			TblQuotes::updateAll(['price' => $totlIncGst], ['=', 'quote_id', $quote_id]);
		}
		else{
			//echo "<pre>";print_r($room);exit();
			$room_id = 0;
			$success=0;
			$message='Unable to save Rooms Data.';
		}

		echo json_encode(['success'=>$success,'message'=>$message,'data'=>array('room_id'=>$room_id)]);
		exit();
	}

	/*
	* Author : Jotpal Singh
	* Date : 2018/10/05
	* Comment : To upload multiple Images and return url in response
	*/
	public function actionUploadImages(){
		$siteURL = Url::base(true);
        $imagePath = [];

        if(empty($_POST['json'])){
			$response = [
				'success' => 0,
				'message' => 'Fields cannot be blank!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}

		$mydata = ($_POST['json'] != '')?$_POST['json']:json_encode(array());
		$mydata = json_decode($mydata,true);

        if($mydata != ''){
			$imageNoteDesc = [];
			if($mydata['imageCount'] > 0){

				for($i = 0; $i < $mydata['imageCount']; $i++){

					$uploads = UploadedFile::getInstancesByName("image_".$i);
					foreach ($uploads as $ik => $file){

						if(isset($file->size)){
							$imageName = $file->name;
							
								$nameArr = explode('.', $file->name);
								$nameArrCount = count($nameArr);
								$ext = $nameArr[$nameArrCount-1];
								$imageName = '';
								$varr = time().rand(1,100);
								if($nameArrCount > 2){
									unset($nameArr[$nameArrCount-1]);
									$imageName = implode('.', $nameArr);
									$imageName .= '_'.$varr.'.'.$ext;
								}
								else{
									$imageName = $nameArr[0].'_'.$varr.'.'.$ext;
								}
							
							//echo $imageName;exit();
							$file->saveAs('uploads/' . $imageName);
							$imagePath[] = $siteURL.'/uploads/'.$imageName;
						}

					} //foreach
				}
			}
		}

		$response = [
			'success' => 1,
			'message' => 'Images uploaded',
			'data'=>$imagePath
		];
		echo json_encode($response);exit();
	}

	/*
	* Author : Jotpal Singh
	* Date : 2018/10/05
	* Comment : To get saved component of rooms
	*/
	public function actionGetRoomDetails(){

		if(empty($_POST['json'])){
			$response = [
				'success' => 0,
				'message' => 'Fields cannot be blank!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}
		$siteURL = Url::base(true);
		$mydata = ($_POST['json'] != '')?$_POST['json']:json_encode(array());
		$mydata = json_decode($mydata,true);

	    $type_id = !empty($mydata['type_id'])?$mydata['type_id']:'';
	    $quote_id = !empty($mydata['quote_id'])?$mydata['quote_id']:'';

	    $response = [];	    
	    $arr      = array();
	   
	    $roomsDetailArr = array();
	    
	    $roomCountWithType = array();
	    // validate
		if($quote_id == "" || $type_id == ""){
			$response = [
				'success' => 0,
				'message' => 'Fields cannot be blank!',
				'data' => [],
			];
		}
	    else{
	    	$quoteRooms = TblRooms::find()
						->joinWith('tblRoomPrefs.tblRoomCompPrefs')
						->joinWith('tblRoomPrefs.color')
						->joinWith('tblRoomDimensions')
						->joinWith('tblRoomSpecialItems')
						->joinWith('tblRoomNotes')
						->joinWith('roomType')
						->where(['tbl_rooms.quote_id' => $quote_id])
						->all();
			
			//echo "<pre>"; print_r($quoteRooms); exit;

			if(!empty($quoteRooms)){

				foreach ($quoteRooms as $key => $roomDetails) {
					$cFinalArr = array();
					$myArr = array();
					$cFinalArr['roomDetails'] = [];
					$cFinalArr['roomDimensions'] = [];
					$cFinalArr['roomSpecialItems'] = [];
					$cFinalArr['roomNotes'] = [];
					$cFinalArr['components'] = [];
				
					$selectedComp = [];
					$selectedCompOpt = [];
					$tblRoomPrefs = $roomDetails->tblRoomPrefs;
					if(count($tblRoomPrefs) > 0){
						foreach ($tblRoomPrefs as $key => $comp) {
							$selectedComp[$comp->comp_id]['comp_id'] = $comp->comp_id;
							if($comp->color_id == 0){
								$selectedComp[$comp->comp_id]['color_id'] = null;
							}
							else{
								$tblRoomCompColor = $comp->color;
								$selectedComp[$comp->comp_id]['color_id']['name'] = $tblRoomCompColor->name ;
								$selectedComp[$comp->comp_id]['color_id']['hex'] =  $tblRoomCompColor->hex ;
								$selectedComp[$comp->comp_id]['color_id']['tag_id'] =  $tblRoomCompColor->tag_id ;
								$selectedComp[$comp->comp_id]['color_id']['number'] =  $tblRoomCompColor->number ;
							}
							$selectedComp[$comp->comp_id]['isSelected'] = $comp->isSelected;
							$selectedComp[$comp->comp_id]['isUndercoat'] = $comp->isUndercoat;
							$selectedComp[$comp->comp_id]['comp_id'] = $comp->comp_id;

							$selectedCompOpt[$comp->comp_id] = [];
							$tblRoomCompPrefs = $comp->tblRoomCompPrefs;
							if (count($tblRoomCompPrefs) > 0) {
								foreach ($tblRoomCompPrefs as $key => $trcp) {
									$selectedCompOpt[$comp->comp_id][$trcp->option_id]['option_id'] = $trcp->option_id;
									$selectedCompOpt[$comp->comp_id][$trcp->option_id]['option_value'] = $trcp->option_value;
								}
							}
								
						}
					}

					//echo "<pre>"; print_r($selectedComp); exit;
					$roomTypeD = $roomDetails->roomType;
					$roomTypeDetails['id'] = $roomTypeD->room_id;
					$roomTypeDetails['name'] = $roomTypeD->name;
					$roomTypeDetails['type_id'] = $roomTypeD->type_id;
					$roomTypeDetails['multiples'] = $roomTypeD->multiples;
					$roomTypeDetails['blue_image'] =( $roomTypeD->blue_image != "" || !is_null($roomTypeD->blue_image) )? $siteURL.'/image/RoomImages/blue/'.$roomTypeD->blue_image :$siteURL.'/image/RoomImages/blue/default.png';
					$roomTypeDetails['white_image'] = ( $roomTypeD->white_image != "" || !is_null($roomTypeD->white_image) )? $siteURL.'/image/RoomImages/white/'.$roomTypeD->white_image :$siteURL.'/image/RoomImages/blue/default.png';

					$cFinalArr['roomDetails']['room_id'] = $roomDetails->rooms_id;
					$cFinalArr['roomDetails']['quote_id'] = $roomDetails->quote_id;
					$cFinalArr['roomDetails']['room_name'] = $roomDetails->room_name;
					$cFinalArr['roomDetails']['include'] = $roomDetails->include;
					$cFinalArr['roomDetails']['room_type'] = $roomTypeDetails;
					$cFinalArr['roomDetails']['is_custom'] = $roomDetails->is_custom;
					

					if(array_key_exists($roomDetails->room_type_id, $roomCountWithType)){
						$roomCountWithType[$roomDetails->room_type_id]["count"]=$roomCountWithType[$roomDetails->room_type_id]["count"] + 1;
					}
					else{
						$roomCountWithType[$roomDetails->room_type_id]["count"]=1;
					}
					$roomCountWithType[$roomDetails->room_type_id]["id"]=$roomDetails->room_type_id;
					$roomCountWithType[$roomDetails->room_type_id]["name"]=$roomTypeD->name;
					$roomCountWithType[$roomDetails->room_type_id]['type_id'] = $roomTypeD->type_id;
					$roomCountWithType[$roomDetails->room_type_id]['multiples'] = $roomTypeD->multiples;

					$roomCountWithType[$roomDetails->room_type_id]['blue_image'] = ( $roomTypeD->blue_image != "" || !is_null($roomTypeD->blue_image) )? $siteURL.'/image/RoomImages/blue/'.$roomTypeD->blue_image :$siteURL.'/image/RoomImages/blue/default.png'; 
					$roomCountWithType[$roomDetails->room_type_id]['white_image'] = ( $roomTypeD->white_image != "" || !is_null($roomTypeD->white_image) )? $siteURL.'/image/RoomImages/white/'.$roomTypeD->white_image :$siteURL.'/image/RoomImages/blue/default.png'; ;

					foreach ($roomDetails->tblRoomDimensions as $key => $rd) {
						$crd = [];
						$crd['length'] = $rd->length;
						$crd['width'] = $rd->width;
						$crd['height'] = $rd->height;
						$crd['selected'] = $rd->selected;
						$cFinalArr['roomDimensions'][] = $crd;
					}

					foreach ($roomDetails->tblRoomSpecialItems as $key => $rsi) {
						$crd = [];
						$crd['id'] = $rsi->id;
						$crd['item_name'] = $rsi->item_name;
						$crd['price'] = $rsi->price;
						$crd['qty'] = $rsi->qty;
						$crd['include'] = $rsi->include;
						$cFinalArr['roomSpecialItems'][] = $crd;
					}

					foreach ($roomDetails->tblRoomNotes as $key => $rn) {
						$crd = [];
						$crd['id'] = $rn->id;
						$crd['created_at'] = $rn->created_at;
						$crd['image'] = $rn->image;
						$crd['description'] = $rn->description;
						$cFinalArr['roomNotes'][] = $crd;
					}

		        	$connection = \Yii::$app->db;

		        	$model = $connection->createCommand('SELECT tct.comp_type_id, tct.comp_id, tct.name AS typename, tct.work_rate, tct.spread_ratio, tct.is_default, tct.thickness,tct.excl_area, tc.name,tc.price_method_id, tc.calc_method_id, tcg.enabled, tcg.type_id from tbl_components as tc, tbl_component_groups as tcg, tbl_component_type as tct where tcg.type_id='.$type_id.' AND tcg.group_id=tc.group_id AND tct.comp_id=tc.comp_id AND tcg.enabled=1 ORDER BY tct.comp_id');
					
					$components = $model->queryAll();

					if(count($components)>0){
						//echo "<pre>"; print_r($selectedCompOpt); exit;

							foreach($components as $_comp){

								$cArr      = array();
								$compArr   = array();
								//echo $_comp['name'].'</br>';n

								$cArr['id']           = $_comp['comp_type_id'];
								$cArr['name']         = $_comp['typename'];
								$cArr['work_rate']    = $_comp['work_rate'];
								$cArr['spread_ratio'] = $_comp['spread_ratio'];

								$cArr['text'] = "";

								if(array_key_exists($_comp['comp_id'], $selectedComp)){
									$myArr[$_comp['comp_id']]['savedData']              = $selectedComp[$_comp['comp_id']];
									//echo("<pre>");print_r($myArr);exit();

									if( array_key_exists($_comp['comp_type_id'], $selectedCompOpt[$_comp['comp_id']] ) ){
										if($_comp['price_method_id'] == 1 || $_comp['price_method_id'] == 3){
											//$cArr['default']      = $selectedComp[$_comp['comp_id']][$_comp['comp_type_id']]['option_id'];
											$cArr['default']      = 1;
										}
										else{
											$cArr['default']    = $_comp['is_default'];
											$cArr['text'] 		= $selectedCompOpt[$_comp['comp_id']][$_comp['comp_type_id']]['option_value'];
										}

										//unset( $myArr[$_comp['comp_id']]['savedData'][$_comp['comp_type_id']] );
										//unset( $selectedComp[$_comp['comp_id']][$_comp['comp_type_id']] );
									}
									else{
										$cArr['default']      = $_comp['is_default'];
									}
								}
								else{
									$myArr[$_comp['comp_id']]['savedData']   = [];
									$cArr['default']      = $_comp['is_default'];
								}

								$myArr[$_comp['comp_id']]['id']              = $_comp['comp_id'];
								$myArr[$_comp['comp_id']]['comp_name']       = $_comp['name'];
								$myArr[$_comp['comp_id']]['price_method_id'] = $_comp['price_method_id'];
								$myArr[$_comp['comp_id']]['calc_method_id']  = $_comp['calc_method_id'];
								$myArr[$_comp['comp_id']]['tt_image'] 	  = "https://www.familyhandyman.com/wp-content/uploads/2017/05/FH14FEB_PTCEIL_04.jpg"; 
								$myArr[$_comp['comp_id']]['tt_text'] 	  = "A ceiling is an overhead interior surface that covers the upper limits of a room. It is not generally considered a structural element, but a finished surface concealing the underside of the roof structure or the floor of a storey above. Ceilings can be decorated to taste, and there are many fine examples of frescoes and artwork on ceilings especially in religious buildings."; 
								$myArr[$_comp['comp_id']]['tt_url'] 	  = "https://en.wikipedia.org/wiki/Ceiling"; 
								$myArr[$_comp['comp_id']]['options'][]       = $cArr;


							}//foreach

							foreach($myArr as $comp_id => $myComp){
								$compArr['id']              = $comp_id;
								$compArr['comp_name']       = $myComp['comp_name'];
								$compArr['price_method_id'] = $myComp['price_method_id'];
								$compArr['calc_method_id']  = $myComp['calc_method_id'];
								$compArr['savedData']		= $myComp['savedData'];
								$compArr['tt_image']		= $myComp['tt_image'];
								$compArr['tt_text']			= $myComp['tt_text'];
								$compArr['tt_url']			= $myComp['tt_url'];
								$compArr['options'] = $myComp['options'];

								$cFinalArr['components'][] = $compArr;
							} //foreach


							//echo "<pre>"; print_r($cFinalArr); exit;

							$roomsDetailArr['rooms'][] = $cFinalArr;

					}else{

						$cFinalArr = array();;
						$response = [
							'success' => 1,
							'message' => 'No Component found!',
							'data'    => $cFinalArr,
						];

					}
				}
				$roomCountWithType = array_values($roomCountWithType);
				$roomsDetailArr['roomCountWithType']=$roomCountWithType;

				$response = [
								'success' => 1,
								'message' => 'Component data!',
								'data' => $roomsDetailArr,
							];

			}
			else{
				$cFinalArr = array();;
				$response = [
					'success' => 1,
					'message' => 'No Rooms found!',
					'data'    => [],
				];
			}
		}

		ob_start();
		echo json_encode($response);  
	}

	public function actionDeleteRoom(){

		if(empty($_POST['json'])){
			$response = [
				'success' => 0,
				'message' => 'Fields cannot be blank!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}
		$siteURL = Url::base(true);
		$mydata = ($_POST['json'] != '')?$_POST['json']:json_encode(array());
		$mydata = json_decode($mydata,true);

		$room_id = !empty($mydata['room_id'])?$mydata['room_id']:'';
		if($room_id != ''){
			TblRooms::deleteAll('rooms_id= '.$room_id);
			TblRoomPref::deleteAll('room_id = '.$room_id);
			TblRoomCompPref::deleteAll('room_id = '.$room_id);
			TblRoomDimensions::deleteAll('room_id = '.$room_id);
			TblRoomSpecialItems::deleteAll('room_id = '.$room_id);

			$cFinalArr = array();;
			$response = [
				'success' => 1,
				'message' => 'Room Deleted',
				'data'    => [],
			];

			unset($mydata["room_id"]);
			$quote_id = $mydata["quote_id"];
			$quotePrice = $this->getQuotePrice($mydata);
			$totlIncGst = $quotePrice['totlIncGst'];
			TblQuotes::updateAll(['price' => $totlIncGst], ['=', 'quote_id', $quote_id]);
		}
		else{
			$cFinalArr = array();;
			$response = [
				'success' => 0,
				'message' => 'No Rooms found!',
				'data'    => [],
			];
		}
		echo json_encode($response);
		exit;
	}


	public function actionStatusRoom(){

		//echo 12;exit;
		$response = [];
		if(empty($_POST['json'])){
			$response = [
				'success' => 0,
				'message' => 'Fields cannot be blank!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}

		$mydata = ($_POST['json'] != '')?$_POST['json']:json_encode(array());
		$mydata = json_decode($mydata,true);

		//echo "<pre>";print_r($mydata);exit();

		if( empty($mydata['room_id']) ){
			$response = [
				'success' => 0,
				'message' => 'Fields cannot be blank!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}
		else
		{
			$room_id = $mydata['room_id'];
			$include = (isset($mydata['include']) )?$mydata['include']:0;
			TblRooms::updateAll(['include' => $include], ['=', 'rooms_id', $room_id]);
			$response = [
				'success' => 1,
				'message' => 'Rooms Updated!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}
	}

	public function actionSaveQuoteNotes()
	{
		$response = [];
		if(empty($_POST['json'])){
			$response = [
				'success' => '0',
				'message' => 'Fields cannot be blank!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}

		$mydata = ($_POST['json'] != '')?$_POST['json']:json_encode(array());
		$mydata = json_decode($mydata,true);
		$siteURL = Url::base(true);

		//echo("<pre>");print_r($mydata);exit();

		if( empty($mydata['quote_id']) || empty($mydata['tbl_quote_notes'])  || empty($mydata['tbl_quote_notes'])){
			$response = [
				'success' => 0,
				'message' => 'Fields cannot be blank!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}
		else{
			$quote_id = $mydata['quote_id'];
			$noteData = [];
			
			$imageNoteDesc = [];
			$imagePath = "";
			if($mydata['imageCount'] > 0){

				for($i = 0; $i < $mydata['imageCount']; $i++){

					$uploads = UploadedFile::getInstancesByName("image_".$i);
					foreach ($uploads as $ik => $file){

						if(isset($file->size)){
							$imageName = $file->name;
							
								$nameArr = explode('.', $file->name);
								$nameArrCount = count($nameArr);
								$ext = $nameArr[$nameArrCount-1];
								$imageName = '';
								$varr = time().rand(1,100);
								if($nameArrCount > 2){
									unset($nameArr[$nameArrCount-1]);
									$imageName = implode('.', $nameArr);
									$imageName .= '_'.$varr.'.'.$ext;
								}
								else{
									$imageName = $nameArr[0].'_'.$varr.'.'.$ext;
								}
							
							//echo $imageName;exit();
							$file->saveAs('uploads/' . $imageName);
							$imagePath = $siteURL.'/uploads/'.$imageName;
						}

					} //foreach
				}
			}

			foreach ($mydata['tbl_quote_notes'] as $key => $trn) {
				$roomNotes = new TblNotes();
				$roomNotes->name = '';
				$roomNotes->device_id = '';
				$roomNotes->unique_id = '';
				$roomNotes->flag = 0;

				$roomNotes->quote_id = $quote_id;
				$roomNotes->description = $trn['description'];
				$roomNotes->image = $imagePath;
				$roomNotes->created_at = $trn['created_at'];
				$roomNotes->updated_at = strtotime(gmdate('Y-m-d H:i:s'));

				if($roomNotes->validate()){
					$success=1;
					$roomNotes->save();
					$message='Quote notes saved.';

					$noteData[$key]['description'] = $trn['description'];
					$noteData[$key]['image'] = $imagePath;
					$noteData[$key]['created_at'] = $trn['created_at'];
					$noteData[$key]['updated_at'] = strtotime(gmdate('Y-m-d H:i:s'));
					$noteData[$key]['id'] = $roomNotes->id;
				}
				else{
					$success=0;
					$message='Unable to save Quote notes.';
				}
			}
			$response = [
				'success' => $success,
				'message' => $message,
				'data'=>['notes'=>$noteData]
			];
			echo json_encode($response);exit();
		}
	}

	public function actionDeleteQuoteNotes(){
		$response = [];
		if(empty($_POST['json'])){
			$response = [
				'success' => 0,
				'message' => 'Fields cannot be blank!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}

		$mydata = ($_POST['json'] != '')?$_POST['json']:json_encode(array());
		$mydata = json_decode($mydata,true);

		//echo("<pre>");print_r($mydata);exit();

		if( empty($mydata['note_id']) ){
			$response = [
				'success' => 0,
				'message' => 'Fields cannot be blank!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}
		else{
			TblNotes::deleteAll('id = '.$mydata['note_id']);
			$response = [
				'success' => 1,
				'message' => 'Notes is deleted',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}
	}

	public function actionGetQuoteNotes()
	{
		$response = [];
		if(empty($_POST['json'])){
			$response = [
				'success' => 0,
				'message' => 'Fields cannot be blank!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}

		$mydata = ($_POST['json'] != '')?$_POST['json']:json_encode(array());
		$mydata = json_decode($mydata,true);

		//echo("<pre>");print_r($mydata);exit();

		if( empty($mydata['quote_id']) ){
			$response = [
				'success' => 0,
				'message' => 'Fields cannot be blank!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}
		else{
			$quote_id = $mydata['quote_id'];
			$quoteData = TblNotes::find()->where(['quote_id' => $quote_id])->all();
			$noteData = [];
			foreach ($quoteData as $key => $trn) {

				$noteData[$key]['id'] = $trn->id;
				$noteData[$key]['description'] = $trn->description;
				$noteData[$key]['image'] = $trn->image;
				$noteData[$key]['created_at'] = $trn->created_at;
				$noteData[$key]['updated_at'] = $trn->updated_at;
			}
			$response = [
				'success' => 1,
				'message' => "Notes data",
				'data'=>['notes'=>$noteData]
			];
			echo json_encode($response);exit();
		}
	}


	public function actionGetRoomQuantities()
	{
		//$_POST['json'] = '{"room_id":9}';
		//$_POST['json'] = '{"quote_id":533}';
		
		$response = [];
		if(empty($_POST['json'])){
			$response = [
				'success' => 0,
				'message' => 'Fields cannot be blank!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}

		$mydata = ($_POST['json'] != '')?$_POST['json']:json_encode(array());
		$mydata = json_decode($mydata,true);

		//echo "<pre>";print_r($mydata);exit();

		if( empty($mydata['room_id']) && empty($mydata['quote_id'])){
			$response = [
				'success' => 0,
				'message' => 'Fields cannot be blank!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}
		else{

			$quote_id = $mydata['quote_id'];

			$quotePrice = $this->getQuotePrice($mydata);

			/*
			"roomDimesionArr"=>$roomDimesionArr,
			"prep_level"=>$prep_level,
			"totalPrice"=>$totalPrice,
			"totalLitres"=>$totalLitres,
			"totalHours"=>$totalHours,
			"totalHoursPrice"=>$totalHoursPrice,
			"applicationPreparation1"=>$applicationPreparation1, 
			"applicationPreparation2"=>$applicationPreparation2, 
			"finalSubTotal"=>$finalSubTotal, 
			"finalMargin"=>$finalMargin, 
			"totalSIP"=>$totalSIP,
			"totl"=>$totl, 
			"gst"=>$gst, 
			"totlIncGst"=>$totlIncGst,
			"quantityComponentArr" => $components,
			"quantityProductArr" => $quantityProductArr,
			"color_consultant" => $color_consultant,
			*/


			$totalQty = $quotePrice['totalQty'];
			$roomDimesionArr = $quotePrice['roomDimesionArr'];
			$prep_level = $quotePrice['prep_level'];
			$totalPrice = $quotePrice['totalPrice'];
			$totalLitres = $quotePrice['totalLitres'];
			$totalHours = $quotePrice['totalHours'];
			$totalHoursPrice = $quotePrice['totalHoursPrice'];
			$paintPreparation = $quotePrice['paintPreparation'];
			$applicationPreparation1 = $quotePrice['applicationPreparation1'];
			$applicationPreparation2 = $quotePrice['applicationPreparation2'];
			$finalSubTotal = $quotePrice['finalSubTotal'];
			$finalMargin = $quotePrice['finalMargin'];
			$totalSIP = $quotePrice['totalSIP'];
			$totl = $quotePrice['totl'];
			$gst = $quotePrice['gst'];
			$totlIncGst = $quotePrice['totlIncGst'];

			$quantityComponentArr = $quotePrice['quantityComponentArr'];
			$quantityProductArr = $quotePrice['quantityProductArr'];
			$color_consultant = $quotePrice['color_consultant'];

			$materialArr = $quotePrice['materialArr'];
			$substrateArr = $quotePrice['substrateArr'];

			//echo "<pre>";print_r($substrateArr);exit;

			$total['paintSubTotal']['qty'] = $totalQty;
			$total['paintSubTotal']['price'] = $totalPrice;

			$total['appliaction']['qty'] = $totalHours;
			$total['appliaction']['price'] = $totalHoursPrice;

			$total['paintPreparation']['qty'] = 1;
			$total['paintPreparation']['price'] = $paintPreparation;

			$total['appPreparation']['qty'] = $applicationPreparation1;
			$total['appPreparation']['price'] = $applicationPreparation2;

			$total['subTotal']['qty'] = 0;
			$total['subTotal']['price'] = $finalSubTotal;

			$specialItemTotal['margin'] = $finalMargin;
			$specialItemTotal['totalExGst'] = $totl;
			$specialItemTotal['gst'] = $gst;
			$specialItemTotal['grandTotal'] = $totlIncGst;

			$response = [
				'success' => 1,
				'message' => "Quantity",
				'data'=>['material'=>$materialArr,'substrate'=>$substrateArr,'total'=>$total,'specialItemTotal'=>$specialItemTotal]
			];
			echo json_encode($response);exit();
		}
	}

	public function actionGetQuoteSummary(){

		$siteURL = Url::base(true);
		$response = [];
		if(empty($_POST['json'])){
			$response = [
				'success' => 0,
				'message' => 'Fields cannot be blank!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}

		$mydata = ($_POST['json'] != '')?$_POST['json']:json_encode(array());
		$mydata = json_decode($mydata,true);

		//echo "<pre>";print_r($mydata);exit();

		if( empty($mydata['quote_id']) || empty($mydata['sub_id'])){
			$response = [
				'success' => 0,
				'message' => 'Fields cannot be blank!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}
		else{
			$data['payments'] = [];
			$data['quoteInclusions'] = [];
			$data['balanceDate']=0;

			$data['summary_id'] = 0;
			$data['noOfPainters'] = 1;
			$data['daysToComplete'] = 0;
			$data['dateEstCommencement'] = 0;
			$data['dateEstCompletion'] = 0;
			$data['authorisedPerson'] = "";

			$data['totalHours'] = 0;
			$data['totalExGst'] = 0;
			$data['gst'] = 0;
			$data['totalInGst'] = 0;

			$data['totalSIP'] = 0;
			$data['pricing']['applicationCost'] = 0;
			$data['pricing']['profitMarkup'] = 0;
			$data['pricing']['defaultDepositPercent'] = 0;
			$data['pricing']['gstPercentage'] = 0;
			$data['pricing']['grossMarginPercent'] = 0;
			//$data['pricing_sub_total'] = 0;
			//$data['pricing_total'] = 0;


			$quote_id = $mydata['quote_id'];
			$sub_id = $mydata['sub_id'];

			$quotePrice = $this->getQuotePrice($mydata);

			if($quotePrice["roomFound"] == 0){
				$response = [
					'success' => 0,
					'message' => 'No Rooms found for this quote.',
					'data'=>[]
				];
				echo json_encode($response);exit();
			}

			$paymentOptions = TblPaymentOptions::find()->where(['enabled'=>1])->all();

			$payments = [];
			if($paymentOptions){
				foreach($paymentOptions as $k => $paymentOption){
					$p['id'] = $paymentOption->id;
					$p['name'] = $paymentOption->name;
					$payments[] = $p;
				}
			}

			$data['paymentOptions'] = $payments;

			$paymentMethods = TblPaymentMethod::find()->where(['enabled'=>1])->all();

			$payments = [];
			if($paymentMethods){
				foreach($paymentMethods as $k => $paymentMethod){
					$p['id'] = $paymentMethod->id;
					$p['name'] = $paymentMethod->name;
					$payments[] = $p;
				}
			}

			$data['paymentMethods'] = $payments;

			if($quotePrice){

				$userSettings = TblUsersSettings::find()->where(['user_id' => $sub_id])->one();

				//echo"<pre>";print_r($userSettings);exit;

				$data['totalHours'] = $quotePrice['totalHours'];
				$data['totalExGst'] = $quotePrice['totl'];
				$data['finalSubTotal'] = $finalSubTotal = $quotePrice['finalSubTotal'];
				$data['gst'] = $quotePrice['gst'];
				$data['totalInGst'] = $quotePrice['totlIncGst'];
				$data['totalSIP'] = $quotePrice['totalSIP'];

				$data['pricing']['applicationCost'] = $userSettings->application_cost;
				$data['pricing']['profitMarkup'] = $profitMarkup = $userSettings->profit_markup;
				$data['pricing']['defaultDepositPercent'] = $userSettings->default_deposit_percent;
				$data['pricing']['gstPercentage'] = $userSettings->gst;

				$quoteDetail = TblQuotes::find()->where(['quote_id' => $sub_id])->one();

				$data['pricing']['grossMarginPercent'] = ($quoteDetail->gross_margin_percent != "")?$quoteDetail->gross_margin_percent:0;
				
				if($data['pricing']['grossMarginPercent'] != 0){
					$cost = $finalSubTotal;
					$grossMarginPercentage = $data['pricing']['grossMarginPercent'];
					$data['pricing']['grossMargin'] = $grossMargin = ($grossMarginPercentage * $cost) / (100 - $grossMarginPercentage);
					$data['pricing']['pricingSubTotal'] = $pricingSubTotal = $cost + $grossMargin;
					$data['pricing']['pricingTotal'] = $pricingTotal = $pricingSubTotal + $data['totalSIP'] + $data['gst'];
				}
				else{
					$cost = $finalSubTotal;

					$data['pricing']['grossMargin'] = $grossMargin = $cost*$profitMarkup/100;

					$data['pricing']['pricingSubTotal'] = $pricingSubTotal = $cost + $grossMargin;

					$data['pricing']['grossMarginPercent'] = $grossMarginPercent = ($grossMargin * 100)/$pricingSubTotal;

					$data['pricing']['pricingTotal'] = $pricingTotal = $pricingSubTotal + $data['totalSIP'] + $data['gst'];
				}

			}

			//echo "<pre>";print_r($quotePrice);exit();

			$roomType = TblRoomTypes::find()
					->joinWith(
								['tblRooms' => 
									function($q) use ($quote_id){
										$q->where(['tbl_rooms.quote_id' => $quote_id,'tbl_rooms.include' => 1]);
									}
								])
					->all();
			//echo "<pre>";print_r($roomType);exit();

			if(count($roomType)){
				foreach ($roomType as $key => $rt) {
					$crt = [];
					$crt['name'] = $rt->name;
					$crt['count'] = count($rt->tblRooms);
					$data['quoteInclusions'][] = $crt;
				}
			}

			$paymentSchedule = TblPaymentSchedule::find()->where(["quote_id"=>$quote_id])->all();

			if(count($paymentSchedule) > 0){
				foreach ($paymentSchedule as $key => $ps) {
					$psArr = [];
					$psArr['payment_schedule_id'] = $ps->id;
					$psArr['descp'] = $ps->descp;
					$psArr['amount'] = $ps->amount;
					$psArr['part_payment'] = $ps->part_payment;
					$psArr['date'] = $ps->created_at;
					$psArr['payment_date'] = $ps->payment_date;

					$data["payments"][] = $psArr;
				}
			}else{
				$psArr = [];
				$psArr['payment_schedule_id'] = 0;
				$psArr['descp'] = 'Initial Payment';
				$psArr['amount'] = 0;
				$psArr['part_payment'] = 30;
				$psArr['date'] = 0;
				$psArr['payment_date'] = 0;

				$data["payments"][] = $psArr;
			}

			$summary = TblSummary::find()->where(["quote_id"=>$quote_id])->one();

			//echo "<pre>";print_r($summary);exit();
			if(count($summary) > 0){
				$data["summary_id"] = $summary->summary_id;
				$data["noOfPainters"] = $summary->painter;
				$data["daysToComplete"] = $summary->days;
				$data["dateEstCommencement"] = $summary->est_comm;
				$data["dateEstCompletion"] = $summary->est_comp;
				$data['payment_method'] = $summary->payment_method;
				$data['payment_option'] = $summary->payment_option;
				$data["authorisedPerson"] = $summary->authorised_person;
				$data["balanceDate"] = $summary->dateBalance;
				if($summary->sign != ""){
					$data["sign"] = $siteURL.$summary->sign;
				}
				else{
					$data["sign"] = "";
				}
			}

			$data["headerDesc"]="Lorem Ipsum is simply dummy text of the printing and typesetting industry.";
    		$data["footerDesc"]="Lorem Ipsum is simply dummy text of the printing and typesetting industry.";

			$response = [
				'success' => 1,
				'message' => 'Summary',
				'data'=>$data
			];
			echo json_encode($response);exit();

		}
	}

	public function actionSaveQuoteSummary(){
		$response = [];
		if(empty($_POST['json'])){
			$response = [
				'success' => 0,
				'message' => 'Fields cannot be blank!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}

		$mydata = ($_POST['json'] != '')?$_POST['json']:json_encode(array());
		$mydata = json_decode($mydata,true);

		//echo "<pre>";print_r($mydata);exit();

		if( empty($mydata['quote_id'])){
			$response = [
				'success' => 0,
				'message' => 'Fields cannot be blank!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}
		else{
			$quote_id = $mydata['quote_id'];
			if(array_key_exists('status', $mydata)) {
				$status = $mydata['status'];
				TblQuotes::updateAll(['status' => $status], ['=', 'quote_id', $quote_id]);
			}
			if(array_key_exists('payments', $mydata)) {

				TblPaymentSchedule::deleteAll('quote_id = '.$quote_id);
				foreach ($mydata['payments'] as $key => $ps) {
					$tblPaymentSchedule = new TblPaymentSchedule();

					$tblPaymentSchedule->quote_id = $quote_id;
					$tblPaymentSchedule->descp = $ps['descp'];
					$tblPaymentSchedule->amount = $ps['amount'];
					$tblPaymentSchedule->part_payment = $ps['part_payment'];
					$tblPaymentSchedule->payment_date = $ps['payment_date'];
					$tblPaymentSchedule->created_at = strtotime(gmdate('Y-m-d H:i:s'));
					$tblPaymentSchedule->updated_at = strtotime(gmdate('Y-m-d H:i:s'));

					if($tblPaymentSchedule->validate()){
						$tblPaymentSchedule->save();
						$success=1;
						$message="Data saved";
					}
					else{
						$success=0;
						$message="Unable to save PaymentSchedule";
					}
				}
			}

			$imagePath="";
			$summary_id = 0;
			$uploads = UploadedFile::getInstancesByName("image_0");
			if(count($uploads) > 0){
				foreach ($uploads as $ik => $file){

					if(isset($file->size)){
						$imageName = $file->name;
						
							$nameArr = explode('.', $file->name);
							$nameArrCount = count($nameArr);
							$ext = $nameArr[$nameArrCount-1];
							$imageName = '';
							$varr = time().rand(1,100);
							if($nameArrCount > 2){
								unset($nameArr[$nameArrCount-1]);
								$imageName = implode('.', $nameArr);
								$imageName .= '_'.$varr.'.'.$ext;
							}
							else{
								$imageName = $nameArr[0].'_'.$varr.'.'.$ext;
							}
						
						//echo $imageName;exit();
						$file->saveAs('uploads/' . $imageName);
						$imagePath = $siteURL.'/uploads/'.$imageName;
					}

				} //foreach
			}


			if(array_key_exists('summary', $mydata)){
				$summaryDetail =  $mydata['summary'];
				if($summaryDetail['summary_id'] == 0){
					$summary = new TblSummary();
					$summary->quote_id = $quote_id;

					$summary->painter = $summaryDetail['painter'];
					$summary->days = $summaryDetail['days'];
					$summary->est_comm = $summaryDetail['est_comm'];
					$summary->est_comp = $summaryDetail['est_comp'];
					$summary->payment_method = $summaryDetail['payment_method'];
					$summary->payment_option = $summaryDetail['payment_option'];
					$summary->authorised_person = $summaryDetail['authorised_person'];

					if($imagePath != ""){
						$summary->sign = $imagePath;
					}
					else{
						$summary->sign = "";
					}

					$summary->amount = $summaryDetail['amount'];
					$summary->dateBalance = $summaryDetail['dateBalance'];
					$summary->created_at = strtotime(gmdate('Y-m-d H:i:s'));
					$summary->updated_at = strtotime(gmdate('Y-m-d H:i:s'));



					if($summary->validate()){
						$summary->save();
						$summary_id = $summary->summary_id;
						$success=1;
						$message="Data saved";
					}
					else{
						$success=0;
						$message="Unable to save Summary";
					}
				}
				else{
					$summary_id = $summaryDetail['summary_id'];
					$summary = TblSummary::findOne($summaryDetail['summary_id']);
					$summary->painter = $summaryDetail['painter'];
					$summary->days = $summaryDetail['days'];
					$summary->est_comm = $summaryDetail['est_comm'];
					$summary->est_comp = $summaryDetail['est_comp'];
					$summary->payment_method = $summaryDetail['payment_method'];
					$summary->payment_option = $summaryDetail['payment_option'];
					$summary->authorised_person = $summaryDetail['authorised_person'];

					if($imagePath !=""){
						$summary->sign = $imagePath;
					}

					$summary->amount = $summaryDetail['amount'];
					$summary->dateBalance = $summaryDetail['dateBalance'];
					$summary->updated_at = strtotime(gmdate('Y-m-d H:i:s'));

					if($summary->validate()){
						$summary->save();
						$success=1;
						$message="Data saved";
					}
					else{
						$success=0;
						$message="Unable to save Summary";
					}

				}
			}

			$response = [
				'success' => $success,
				'message' => $message,
				'data'=>['summary_id'=>$summary_id]
			];
			echo json_encode($response);exit();

		}
	}

	public function actionSaveQuoteSpecialitems(){

		$response = [];
		if(empty($_POST['json'])){
			$response = [
				'success' => 0,
				'message' => 'Fields cannot be blank!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}

		$mydata = ($_POST['json'] != '')?$_POST['json']:json_encode(array());
		$mydata = json_decode($mydata,true);

		//echo "<pre>";print_r($mydata);exit();

		if( empty($mydata['quote_id'])){
			$response = [
				'success' => 0,
				'message' => 'Fields cannot be blank!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}
		else{
			$quote_id = $mydata['quote_id'];
			TblQuoteSpecialItems::deleteAll('quote_id = '.$quote_id);
			if(array_key_exists('tbl_quote_special_items', $mydata)){
				foreach ($mydata['tbl_quote_special_items'] as $key => $trsi) {
					$quoteSpecialItems = new TblQuoteSpecialItems();
					$quoteSpecialItems->name = '';
					$quoteSpecialItems->device_id = '';
					$quoteSpecialItems->unique_id = '';
					$quoteSpecialItems->flag = 0;

					$quoteSpecialItems->quote_id = $quote_id;
					$quoteSpecialItems->item_name = $trsi['item_name'];
					$quoteSpecialItems->price = $trsi['price'];
					$quoteSpecialItems->qty = $trsi['qty'];
					$quoteSpecialItems->include = $trsi['include'];
					$quoteSpecialItems->created_at = strtotime(gmdate('Y-m-d H:i:s'));
					$quoteSpecialItems->updated_at = strtotime(gmdate('Y-m-d H:i:s'));

					if($quoteSpecialItems->validate()){
						$success=1;					
						$quoteSpecialItems->save();
						$message='Special Item saved.';
					}
					else{
						$success=0;
						$message='Unable to save Quote Special items.';
					}
				}
				$quotePrice = $this->getQuotePrice($mydata);
				$totlIncGst = $quotePrice['totlIncGst'];
				TblQuotes::updateAll(['price' => $totlIncGst], ['=', 'quote_id', $quote_id]);
			}
			$response = [
				'success' => $success,
				'message' => $message,
				'data'=>[]
			];
		}

		echo json_encode($response);exit();

	}

	public function actionGetQuoteSpecialitems(){

		$response = [];
		if(empty($_POST['json'])){
			$response = [
				'success' => 0,
				'message' => 'Fields cannot be blank!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}

		$mydata = ($_POST['json'] != '')?$_POST['json']:json_encode(array());
		$mydata = json_decode($mydata,true);

		//echo "<pre>";print_r($mydata);exit();

		$data['quoteSpecialItems'] = [];

		if( empty($mydata['quote_id'])){
			$response = [
				'success' => 0,
				'message' => 'Fields cannot be blank!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}
		else{
			$quote_id = $mydata['quote_id'];
			$tblQuoteSpecialItems = TblQuoteSpecialItems::find()->where('quote_id = '.$quote_id)->all();

			foreach ($tblQuoteSpecialItems as $key => $rsi) {
				$crd = [];
				$crd['id'] = $rsi->id;
				$crd['item_name'] = $rsi->item_name;
				$crd['price'] = $rsi->price;
				$crd['qty'] = $rsi->qty;
				$crd['include'] = $rsi->include;
				$data['quoteSpecialItems'][] = $crd;
			}
		}

		$response = [
			'success' => 1,
			'message' => 'Special Items found for this quote',
			'data'=>$data
		];

		echo json_encode($response);exit();
	}

	public function actionGenerateQuotepdfWeb(){


		$response = [];
		if(empty($_POST['json'])){
			$response = [
				'success' => 0,
				'message' => 'Fields cannot be blank!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}

		$mydata = ($_POST['json'] != '')?$_POST['json']:json_encode(array());
		$mydata = json_decode($mydata,true);

		//echo "<pre>";print_r($mydata);exit();

		if( empty($mydata['quote_id'])){
			$response = [
				'success' => 0,
				'message' => 'Fields cannot be blank!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}
		else{


			$quote_id = $mydata['quote_id'];

			$quotePrice = $this->getQuotePrice($mydata);

			$total = $quotePrice['totl'];
			$gst = $quotePrice['gst'];
			$totlIncGst = $quotePrice['totlIncGst'];

			/// Special Items
			$paymentSchedule = TblPaymentSchedule::find()->where(["quote_id"=>$quote_id])->all();
			$deposit = 0;
			if(count($paymentSchedule) > 0){
				foreach ($paymentSchedule as $key => $ps) {
					$deposit += $ps->amount;
				}
			}

			$balance = $totlIncGst - $deposit;

			$deposit = number_format((float)$deposit, 2, '.', '');
			$balance = number_format((float)$balance, 2, '.', '');
			$total = number_format((float)$total, 2, '.', '');
			$gst = number_format((float)$gst, 2, '.', '');
			$totlIncGst = number_format((float)$totlIncGst, 2, '.', '');

			$quantityComponentArr = $quotePrice['quantityComponentArr'];
			$quantityProductArr = $quotePrice['quantityProductArr'];

				// echo "quantityComponentArr";
				// echo"<pre>";print_r($quantityComponentArr);
				// echo "quantityProductArr";
				// echo"<pre>";print_r($quantityProductArr);exit;

			/// Fetch special items for this quote
			$roomSpecialItems =  TblRooms::find()
				->joinWith('tblRoomSpecialItems')
				->where(['quote_id'=>$quote_id])
				->all();

				//echo"<pre>";print_r($roomSpecialItems);exit;

			$specialItems = [];
			foreach($roomSpecialItems as $key => $dict){
				$room_id = $dict->rooms_id;
				$room_name = $dict->room_name;

				foreach($dict->tblRoomSpecialItems as $rsi){
					$specialItems[] = $room_name." - ".$rsi->item_name; 
				}
					
			}
			
			$quoteSpecialItems =  TblQuoteSpecialItems::find()->where(['quote_id'=>$quote_id])->all();

			foreach($quoteSpecialItems as $key => $dict){
				$specialItems[] = $dict->item_name;
			}


			/// Fetching quote site notes
			$notes =  TblNotes::find()->where(['quote_id'=>$quote_id])->all();
			$siteNotes = [];
			$i = 0;
			foreach($notes as $key => $dict){
				$siteNotes[$i]['image'] = $dict->image;
				$siteNotes[$i]['description'] = $dict->description;
				$i++;
			}

			// Check Color
			$colorConsultant = 0;
			$tier = "";
			$coat = "One";
			$prepLevel = "Premium";
			$selectedBrand = "";
			//$brandPref = TblBrandPref::find()->where(['quote_id' => $quote_id])->one();

			$brandPref = TblBrandPref::find()
						->joinWith('brand')
						->joinWith('tiers')
						->where(['tbl_brand_pref.quote_id' => $quote_id])
						->one();
			
			//echo "<pre>";print_r($brandPref);exit;
			if($brandPref){
				$colorConsultant = $brandPref->color_consultant;
				$coat = $brandPref->coats;
				if($coat == 1) $coat = "ONE";
				if($coat == 2) $coat = "TWO";
				if($coat == 3) $coat = "THREE";
				if($coat == 4) $coat = "FOUR";

				$prepLevel = ($brandPref->prep_level == 1)?"TEST":"PREMIUM";

				$tier = $brandPref->tiers->name;
			}
			
			$totalRooms = count($roomSpecialItems);

			$roomText = ($totalRooms > 1)?"Rooms":"Room";

			$quoteScope = "PAINTING ".$totalRooms." ".$roomText." WITH ".$tier.", ".$coat." COAT OF PAINT, PREP LEVEL ".$prepLevel;

			$selectedCompNames = [];
			$roomNames = [];
			foreach($quantityComponentArr as $key => $qca){
				if($qca['option_value']!="" || ($qca['price_method_id']==1 || $qca['price_method_id']==3) ){
					if(!in_array($qca["components_name"], $selectedCompNames)){
						$selectedCompNames[] = $qca["components_name"];
					}
				}
				if(!in_array($qca["room_name"], $roomNames)){
					$roomNames[] = $qca["room_name"];
				}
			}

			$tblSummary = TblSummary::find()->where(['quote_id'=>$quote_id])->one();

			$days = 0;
			$est_comm = "";
			$est_comp = "";
			$authorised_person = "";
			if($tblSummary){
				$days = $tblSummary->days;
				$est_comm = date('D, d/m/Y',$tblSummary->est_comm);
				$est_comp = date('D, d/m/Y',$tblSummary->est_comp);
				$authorised_person = $tblSummary->authorised_person;
			}

			$completionDays = ($days>1)?$days." BUSINESS DAYS":$days." BUSINESS DAY";

			$selectedComponentsNames = implode(", ",$selectedCompNames);
			$selectedRoomNames = implode(", ",$roomNames);

			$dataForJson = [];

			$dataForJson['qid']=$quote_id;
			$dataForJson['scope']=$quoteScope;
			$dataForJson['includes']=$selectedComponentsNames;
			$dataForJson['spl']=$specialItems;
			$dataForJson['brand_logo']="http://enacteservices.com/paintpad/image/Main_logo.jpg";
			$dataForJson['image']="http://enacteservices.com/paintpad/image/logo.png";
			$dataForJson['rooms']=$selectedRoomNames;
			$dataForJson['subtotal']=$total;
			$dataForJson['gst']=$gst;
			$dataForJson['total']=$totlIncGst;
			$dataForJson['deposit']=$deposit;
			$dataForJson['balance']=$balance;
			$dataForJson['start']=$est_comm;
			$dataForJson['completion']=$completionDays;
			$dataForJson['color_consultant']=$colorConsultant;
			$dataForJson['end']=$est_comp;
			$dataForJson['siteNotes']=$siteNotes;

			$mydata = json_encode($dataForJson);

	        $response = [];
			$modelQuery        = array();
			$data              = array();
			$qdata             = array();
			$cdata             = array();
			$quote_description = '';
			$client_name       = '';
			$client_email      = '';
			$client_phone      = '';
			$client_addr       = '';
			$sub_email         = '';
			$sub_name          = '';
			$sub_phone         = '';
			$siteAddress       = '';

			/*print_r(json_decode(file_get_contents("php://input"), true));
			exit;
			*/

			//$mydata = json_decode(file_get_contents("php://input"), true);
			//$mydata = ($_POST['json'] != '')?$_POST['json']:'';

			if($mydata != ''){
				$mydata = json_decode($mydata, true);
				$qid = $mydata['qid'];
				//echo 'qid'.$mydata['qid']; exit;

				if(!empty($qid)){
				$modelQuery = TblQuotes::find()->joinWith('contact')->joinWith('siteAddress')->where([ 'tbl_quotes.quote_id'=>$qid])->orderBy('quote_id DESC')->all();
				//echo "<pre>"; print_r($modelQuery); exit;

				if(count($modelQuery)>0){

						$subId = $modelQuery[0]->subscriber_id;

						$userData = TblUsers::find()
										->joinWith('tblUsersDetails')                             
										->where([ 'tbl_users.user_id' => $subId])
										->all();

						//echo '<pre>'; print_r($userData); exit;

							if(count($userData)>0){

								$sub_email         = $userData[0]->email;
								$sub_name          = $userData[0]->tblUsersDetails[0]->name;
								$sub_phone         = $userData[0]->tblUsersDetails[0]->phone;

							}

							$quote_description = $modelQuery[0]->description;
							$quote_date        = date('d/m/Y',$modelQuery[0]->created_at);
							$client_name       = $modelQuery[0]->contact->name;
							$client_email      = $modelQuery[0]->contact->email;
							$client_phone      = $modelQuery[0]->contact->phone;
							$client_addid      = $modelQuery[0]->contact->address_id;

							if($client_addid != ''){
								$model = TblAddress::find()
										->select('formatted_address')
										->where(['address_id' => $client_addid])
										->one();

								$client_addr = $model->formatted_address;
							}                   

							$siteAddress       = $modelQuery[0]->siteAddress->formatted_address;    

							/* client data */
							$cdata['cname']  = $client_name;
							$cdata['cemail'] = $client_email;
							$cdata['cphone'] = $client_phone;

							/* quote data */
							$qdata['number']      = $qid;
							$qdata['description'] = $quote_description;
							$qdata['date']        = $quote_date;

				}

				}

				$header = 'INTERIOR PAINTING QUOTE';
				$data   = $mydata;

				//$mpdf   = new \Mpdf\Mpdf(['','','','','margin_left'=>8,'margin_right'=>8,'','','','','']);
				$mpdf   = new \Mpdf\Mpdf(['margin_left'=>8,'margin_right'=>8,'margin_top'=>8]);

				$mpdf->SetHTMLFooter('
				<table width="100%">
					<tr>
						<td width="33%"></td>
						<td width="33%" align="center">{PAGENO}/{nbpg}</td>
						<td width="33%" style="text-align: right;"></td>
					</tr>
				</table>');

				
				$mpdf->WriteHTML($this->renderPartial('/mpdf/quote',array('header'=>$header, 'cdata'=>$cdata, 'qdata'=>$qdata, 'data'=>$data, 'sub_email'=>$sub_email, 'sub_name'=>$sub_name, 'sub_phone'=>$sub_phone,'siteAddress'=>$siteAddress,'client_addr'=>$client_addr)));

				$filename = 'quote'.$qid.'_'.time().'.pdf';

				$mpdf->Output('quotepdf/'.$filename,'F');

					$siteURL = Url::base(true). '/quotepdf';

					$path = $siteURL.'/'.$filename;

					$response = [
						'success' => '1',
						'message' => $path
					];

			}else{

				$response = [
					'success' => '0',
					'message' => 'Data cannot be blank.'
				];
			}


			ob_start();
			echo json_encode($response);

		}
	}

	
	public function actionGenerateJsspdfWeb(){
		$response = [];
		if(empty($_POST['json'])){
			$response = [
				'success' => 0,
				'message' => 'Fields cannot be blank!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}

		$mydata = ($_POST['json'] != '')?$_POST['json']:json_encode(array());
		$mydata = json_decode($mydata,true);

		//echo "<pre>";print_r($mydata);exit();

		if( empty($mydata['quote_id'])){
			$response = [
				'success' => 0,
				'message' => 'Fields cannot be blank!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}
		else
		{
			$rooms = [];
			$stocks = [];

			$totalLitres = 0;
			$totalHours = 0;

			$quote_id = $mydata['quote_id'];

			$quotePrice = $this->getQuotePrice($mydata);

			//echo "<pre>";print_r($quotePrice);exit;

			$total = $quotePrice['totl'];
			$gst = $quotePrice['gst'];
			$totlIncGst = $quotePrice['totlIncGst'];
			$totalLitres = $quotePrice['totalLitres'];


			$totalHours = $quotePrice['totalHours'];
			$minutes = ceil($totalHours * 60);	//2
			$hrs = $minutes/60;
			$hrs = (int)$hrs;
			$min = $minutes%60;
			$min = (int)$min;
			$time = $hrs.'.'.$min;
			$time = number_format((float)$time, 2, '.', '');
			$readableTotalHours = str_replace('.', ':', $time);


			$prep_level = $quotePrice['prep_level'];
			$quantityComponentArr = $quotePrice['quantityComponentArr'];
			$quantityProductArr = $quotePrice['quantityProductArr'];

			//echo "132";exit;
			//echo "<pre>";print_r($quantityComponentArr);exit;

			$rooms = [];
			$roomNames = [];
			$brand_name = "";
			$roomIds = [];

			foreach($quantityComponentArr as $key => $dict){

				///#1. Current component
				$colorName = "";
				$colorId = 0;

				$productname = $dict["products_name"];
				$brand_name = $dict["brand_name"];

				if ($dict["topcoat_status"] != 0) {
					$sheen = $dict["sheen_id"];

					$productname = $productname.", ".$dict["sheen_name"];

					$colorId = 0;
					$colorName = "";
					if($dict["color_id"]){
						$colorId = $dict["color_id"];
						$colorName = $dict["color_name"];
					}
				}
					
				$component = [];
				$component["name"] = $dict["components_name"]." - ".$dict["comp_type_name"];
				//$component["name"] = $dict["components_name"];
				$component["product"] = $productname;
				$component["comp_id"] = $dict["comp_id"];
				$component["color"] = $colorName;

				$minutes = ceil($dict["hours"] * 60);
				$minutes = (int) $minutes;
				$hrs = $minutes/60;
				$hrs = (int) $hrs;
				$min = $minutes%60;
				$min = (int)$min;
				if($min<10){
					$min = "0".$min;
				}
				$component["time"] = $hrs.":".$min;

				$component["paint"] = number_format((float)$dict["litre"], 2, '.', '');
				$component["topcoat_status"] = $dict["topcoat_status"];

				$roomId = $dict["room_id"];

				

				if(array_key_exists($roomId,$rooms)){
					/// Modify existing room

					//echo "<pre>";print_r($rooms);exit;

					$room = $rooms[$roomId];
					$components = $room["components"];
					
					$canAdd = 1;

					
					//echo "<pre>";print_r($component);exit;

					foreach($components as $key => $comp){
						if ( $comp["comp_id"] == $component["comp_id"] && $comp["topcoat_status"]  == $component["topcoat_status"] ) {
							$canAdd = 1;
							break;
						}
					}
					
					if ($canAdd) {

						$components[] = $component;

						//echo"<pre>";print_r($components);exit;

						$room["components"] = $components;
						
						$componentsTotalTime = 0;
//						echo "<pre>";print_r($components);exit;
						foreach($components as $key => $cmpt){

							$time = $cmpt["time"];
							$time = str_replace(':', '.', $time);
							$time = number_format((float)$time, 2, '.', '');

							$times = explode('.',$time);
							$hours = $times[0];
							$minutes = $times[1];

							$totalHours = $hours + $minutes/60;
							$componentsTotalTime += $totalHours;
						}

						$minutes = ceil($componentsTotalTime * 60);
						$hrs = $minutes/60;
						$hrs = (int)$hrs;

						$min = $minutes%60;
						$min = (int)$min;

						$time = $hrs.'.'.$min;
						$time = number_format((float)$time, 2, '.', '');
						$time = str_replace('.', ':', $time);
						$room["hrs"] = $time;

						//echo"<pre>";print_r($room);exit;

						$rooms[$roomId] = $room;
					}
				}
				else{

					$roomNames[$dict["room_name"]] = $dict["room_name"];
					$roomIds[$roomId] = $roomId;
					//echo "<pre>";print_r($roomIds);exit;
					$rooms[$roomId]["room_id"] = $roomId;
					$rooms[$roomId]["name"] = $dict["room_name"];
					$rooms[$roomId]["hrs"] = $component["time"];
					if($prep_level == 1){
						$rooms[$roomId]["prep_level"] = "Test";
					}
					else{
						$rooms[$roomId]["prep_level"] = "Premium";
					}
					
					$rooms[$roomId]["components"][] = $component;
				}
					
				
			}

			//echo "<pre>";print_r($rooms);exit;
			$quoteRoomNotes = [];
			if(count($roomIds) > 0){
				$roomNotes = TblRoomNotes::find()->where(['in', 'room_id', $roomIds])->all();

				foreach($roomNotes as $key => $rn ){
					$quoteRoomNotes[$rn->room_id][] = $rn;
				}
			}

			$quoteRoomSpecialItems = [];
			if(count($roomIds) > 0){
				$roomSpecialItems = TblRoomSpecialItems::find()->where(['in', 'room_id', $roomIds])->andWhere(['=', 'include', 1])->all();
				foreach($roomSpecialItems as $key => $rsi ){
					$quoteRoomSpecialItems[$rsi->room_id][] = $rsi;
				}
			}

			//echo "<pre>";print_r($quantityProductArr);exit;

			foreach($quantityProductArr as $key => $dict){
				$colorName = "";
				if ( $dict["topcoat_status"] != 0) {
					$colorId = 0;
					$colorName = "";
					if($dict["color_id"]){
						$colorId = $dict["color_id"];
						$colorName = $dict["color_name"];
					}
				}


				$result1 = [];

				$roomId = $dict["room_id"];

				/// #2. Stocks
				if(array_key_exists($dict["paint_product_id"],$stocks)){
					$existingStock = $stocks[$dict["paint_product_id"]];
					$productName = $existingStock["product"];
					$productName = $productName.", ".$dict["stock_qty"]."x".$dict["stock_litres"]."L";

					$stockLitre = $existingStock["litres"];
					$stockLitre = str_replace("L","",$stockLitre);
					$existingStock["litres"] = $stockLitre+($dict["stock_qty"]*$dict["stock_litres"])."L";

					$existingStock["product"] = $productName;
					$stocks[$dict["paint_product_id"]] = $existingStock;
				}
				else{
					/// Add new stock
					$productName = $dict["products_name"].", ".$dict["stock_qty"]."x".$dict["stock_litres"]."L";
					//$stock["litres"] = number_format((float)$dict["totalLitres"], 2, '.', '')."L";
					$stock["litres"] = $dict["stock_qty"]*$dict["stock_litres"]."L";
					//$stock["litre_quant"] = $dict["stock_qty"]*$dict["stock_litres"];
					$stock["product"] = $productName;
					$stock["Colour"] = $colorName;
					$stock["product_id"] = $dict["paint_product_id"];
					$stock["sheen_id"] = $dict["paint_sheen_id"];
					$stocks[$dict["paint_product_id"]] = $stock;
					
				}
			}

			//echo "<pre>";print_r($stocks);exit;

			$materials[0]["name"] = $brand_name;
			$materials[0]["stocks"] = $stocks;

			/// Rooms name separated by comma
			$roomsSummary = implode(', ',$roomNames);

			$specialItems = [];

			$roomDimesionArr = $quotePrice['roomDimesionArr'];

			//echo "<pre>";print_r($rooms);exit;

			foreach($rooms as $roomId => $curRoom){
				$newRoom = $curRoom;
				//$roomDim = $roomDimesionArr[$roomId];

				$area = "";
				$A = 0;

				foreach($roomDimesionArr[$roomId] as $rdKwy => $roomDim){
					$length = $roomDim["length"];
					$width = $roomDim["width"];
					$height = $roomDim["height"];

					$A +=( $length * $width );
				}
				
				if($A > 0){
					$area = $A;
				}
				
				$ceilingsArea = $area;
				$ceilingsArea = number_format((float)$ceilingsArea, 2, '.', '');

				$area = "";
				$A = 0;
				
				/// Formula
				/*
				* Walls Area = ((2L1 + 2W1) * H1) + ((2L2 + 2W2) * H2)
				* While calculating walls area which is checked will be excluded
				* Example: If L is checked then formula will be (2W * H) only
				*/
				$roomDim = $roomDimesionArr[$roomId];

				foreach($roomDim as $key => $dimension){
					$L = $dimension["length"];
					$W = $dimension["width"];
					$H = $dimension["height"];

					if ($dimension["selected"] == -1 || is_null($dimension["selected"])) {
						$A = (2*$L + 2*$W) * $H;
					}
					else {
						$selectedUnit = $dimension["selected"];
						if ($selectedUnit == 0) {
							$A += (2*$L + 2*$W) * $H;
						}
						else if ($selectedUnit == 2) {
							$A += (2*$W) * $H;
						}
						else {
							$A += (2*$W) * $H;
						}
					}
				}

				if ($A > 0) {
					$area = $A;
				}
				
				$wallsArea = $area;
				$wallsArea = number_format((float)$wallsArea, 2, '.', '');

				$components = $curRoom["components"];

				//echo "<pre>";print_r($components);exit;

				foreach($components as $key => $component){
					if ($component["comp_id"] == 1) {
						/// Ceilings
						$components[$key]["name"] .= " ".$ceilingsArea." m²";
					}
					else if ($component["comp_id"] == 7) {
						/// Walls
						$components[$key]["name"] .= " ".$wallsArea." m²";
					}
				}

				$newRoom["components"] = $components;

				$rooms[$roomId]["components"] = $components;

				$notes = $quoteRoomNotes[$roomId];

				$siteNotes = [];
				if(count($notes) > 0){
					foreach($notes as $n => $note){
						$sn = [];
						$sn['image'] = $note->image;
						$sn['description'] = $note->description;
						$siteNotes[] = $sn;
					}
				}

				$newRoom["siteNotes"] = $siteNotes;
				$rooms[$roomId]["siteNotes"] = $siteNotes;

				$specialItems = [];
				
				if(array_key_exists($roomId, $quoteRoomSpecialItems) ){
					$items = $quoteRoomSpecialItems[$roomId];

					//echo "<pre>";print_r($items);exit;
					foreach ($items as $key => $item) {
						if ($item->include) {
							$specialItems[] =  $curRoom["name"]." - ".$item->item_name;
						}
					}
				}

				$newRoom["specialItems"] = $specialItems;
				$rooms[$roomId]["specialItems"] = $specialItems;

				$newRooms[] = $newRoom;
			}

			//echo "<pre>";print_r($newRooms);exit;

			$specialItems = [];
			if(count($roomIds) > 0){
				$quoteSpecialItems = TblQuoteSpecialItems::find()->where(['quote_id'=> $quote_id,'include'=> 1])->all();
				foreach($quoteSpecialItems as $key => $qsi ){
					$specialItems[] = $qsi->item_name;
				}
			}

			//echo $quote_id." as";exit;
			/// Fetching quote site notes

			$siteNotes = [];
			if(count($roomIds) > 0){
				$quoteSpecialItems = TblNotes::find()->where(['quote_id' => $quote_id])->all();
				foreach($quoteSpecialItems as $key => $note ){
					$sn = [];
					$sn["image"] =  $note["image"];
					$sn["description"] = $note["description"];
					$siteNotes[] = $sn;
				}
			}

			$colorConsultant = $quotePrice['color_consultant'];

			// Check Color
			$colorConsultant = 0;
			$tier = "";
			$coat = "One";
			$prepLevel = "Premium";
			$selectedBrand = "";
			//$brandPref = TblBrandPref::find()->where(['quote_id' => $quote_id])->one();

			$brandPref = TblBrandPref::find()
						->joinWith('brand')
						->joinWith('tiers')
						->where(['tbl_brand_pref.quote_id' => $quote_id])
						->one();
			
			//echo "<pre>";print_r($brandPref);exit;
			if($brandPref){
				$colorConsultant = $brandPref->color_consultant;
				$coat = $brandPref->coats;
				if($coat == 1) $coat = "ONE";
				if($coat == 2) $coat = "TWO";
				if($coat == 3) $coat = "THREE";
				if($coat == 4) $coat = "FOUR";

				$prepLevel = ($brandPref->prep_level == 1)?"TEST":"PREMIUM";

				$tier = $brandPref->tiers->name;
			}
			
			$totalRooms = count($roomIds);

			$roomText = ($totalRooms > 1)?"Rooms":"Room";

			$quoteScope = "PAINTING ".$totalRooms." ".$roomText." WITH ".$tier.", ".$coat." COAT OF PAINT, PREP LEVEL ".$prepLevel;

			$tblSummary = TblSummary::find()->where(['quote_id'=>$quote_id])->one();

			$days = 0;
			$est_comm = "";
			$est_comp = "";
			$authorised_person = "";
			$nop = 1;
			if($tblSummary){
				$days = $tblSummary->days;
				$nop = $tblSummary->painter;
				$est_comm = date('D, d/m/Y',$tblSummary->est_comm);
				$est_comp = date('D, d/m/Y',$tblSummary->est_comp);
				$authorised_person = $tblSummary->authorised_person;
			}

			$completionDays = ($days>1)?$days." BUSINESS DAYS":$days." BUSINESS DAY";

			$dataForJson = [];

			$dataForJson['qid']=$quote_id;
			$dataForJson['scope']=$quoteScope;
			$dataForJson['brand_logo']="http://enacteservices.com/paintpad/image/Main_logo.jpg";
			$dataForJson['image']="http://enacteservices.com/paintpad/image/logo.png";
			$dataForJson['nop']=$nop;
			$dataForJson['qty']=number_format((float)$totalLitres, 2, '.', '').'L';
			$dataForJson['hrs']=$readableTotalHours;
			$dataForJson['start']=$est_comm;
			$dataForJson['end']=$est_comp;
			$dataForJson['spl']=$specialItems;
			$dataForJson['rooms']=$newRooms;
			$dataForJson['materials']=$materials;
			$dataForJson['rooms_summary']=$roomsSummary;
			$dataForJson['color_consultant']=$colorConsultant;
			$dataForJson['siteNotes']=$siteNotes;
			$dataForJson['specialInstructions']=$specialItems;

			//echo "<pre>";print_r($dataForJson);exit;

			$_POST['json'] = json_encode($dataForJson);

			/*{@"qid": [DataHelper instance].currentQuote.quote_id,
				@"scope": [[DataHelper instance] quoteScope],
				@"brand_logo": @"http://enacteservices.com/paintpad/image/Main_logo.jpg",
				@"image": @"http://enacteservices.com/paintpad/image/logo.png",
				@"nop" : summaryPaymentFooter.tfPainter.text,
				@"qty": [NSString stringWithFormat:@"%.1fL", totalLitres],
				@"hrs": [self humanReadableFromHours:totalHours],
				@"start": summaryPaymentFooter.tfEstCommencement.text,
				@"end": summaryPaymentFooter.tfEstCompletion.text,
				@"spl": [specialItems copy],
				@"rooms": newRooms,
				@"materials": materials,
				@"rooms_summary": roomsSummary,
				@"color_consultant": colorConsultant,
				@"siteNotes": [siteNotes copy],
				@"imageCount": @(imageCount),
				@"specialInstructions": [specialItems copy]};*/

			
				$modelQuery        = array();
				$data              = array();
				$qdata             = array();
				$cdata             = array();
				$quote_description = '';
				$client_name       = '';
				$client_email      = '';
				$client_phone      = '';
				$client_addr       = '';
				$sub_email         = '';
				$sub_name          = '';
				$sub_phone         = '';
				$siteAddress       = '';
		
				$siteURL = Url::base(true);
				$totalArr = [];
				$mydata = json_decode($_POST['json'], true);
		
				if($mydata != ''){
		
					$imageNoteDesc = [];
		
					$qid        = $mydata['qid'];
					$image      = $mydata['image'];
					$brand_logo = $mydata['brand_logo'];
					$header = 'Job Specification';
					
					//echo 'qid'.$mydata['qid']; exit;
		
					if(!empty($qid)){
		
						$modelQuery = TblQuotes::find()->joinWith('contact')->joinWith('siteAddress')->where([ 'tbl_quotes.quote_id'=>$qid])->orderBy('quote_id DESC')->all();
						//echo "<pre>"; print_r($modelQuery); exit;
		
						if(count($modelQuery)>0){
		
							$subId = $modelQuery[0]->subscriber_id;
		
							$userData = TblUsers::find()
											->joinWith('tblUsersDetails')                             
											->where([ 'tbl_users.user_id' => $subId])
											->all();
		
							//echo '<pre>'; print_r($userData); exit;
		
							if(count($userData)>0){
								$sub_email         = $userData[0]->email;
								$sub_name          = $userData[0]->tblUsersDetails[0]->name;
								$sub_phone         = $userData[0]->tblUsersDetails[0]->phone;
							}
	
							$quote_description = $modelQuery[0]->description;
							$quote_date        = date('d/m/Y',$modelQuery[0]->created_at);
	
							$client_name       = $modelQuery[0]->contact->name;
							$client_email      = $modelQuery[0]->contact->email;
							$client_phone      = $modelQuery[0]->contact->phone;
							$siteAddress       = $modelQuery[0]->siteAddress->formatted_address;           
	
							$client_addid      = $modelQuery[0]->contact->address_id;
	
							if($client_addid != ''){
								$model = TblAddress::find()
											->select('formatted_address')
											->where(['address_id' => $client_addid])
											->one();
	
								$client_addr = $model->formatted_address;
							}
						}
					}

					$data   = $mydata;

					//echo "<pre>";print_r($data);exit;
		
					$mpdf   = new \Mpdf\Mpdf(['margin_left'=>8,'margin_right'=>8,'margin_top'=>8]);
		
					$mpdf->SetHTMLHeader('<div>
										<table>
											<tr>
												<td style="height: 15px"></td>
											</tr>
										</table>                           
										<table cellpadding="0" cellspacing="0" style="border-bottom: 2px solid #e1e1e1; width:100%;">
											<tr style=" font-size: 12px;">
												<td style="text-align: left;vertical-align: bottom;"> <div> <span> <img src="'.$brand_logo.'" alt="" style="width:100px;transform: translate(0, 50%);"> </span></div></td>
												<td style="">&nbsp;</td>
												<td style="">&nbsp;</td>
												<td style="text-align: right; vertical-align: bottom;padding-bottom: 4px;"><h1 style="color:#0a80c5; font-size: 19px; font-weight: normal; text-align: right;">JOB SPECIFICATION</h1></td>
											</tr>									
											<tr style=" font-size: 12px; text-align: right; float: right;">
												<td style="height:2px">&nbsp;</td>
												<td style="height:2px">&nbsp;</td>
												<td style="height:2px">&nbsp;</td>										
												<td style="text-align: right">
													<table class="" style="border:0px; background:#ececec;">
														<tr>
															<td style=" border:0px;padding: 5px 8px 0px 5px;color:#525251;font-weight: normal;text-align: left;">
																<table class="" style="border:0px; background:#ececec;">
																<tr>
																<td><b style="color:#0a86c2; font-size: 12px; text-transform: uppercase;">Quote No.</b></td>
																</tr>
																<tr>
																<td style="font-size: 11px;color:#62615e">'.$qid.'</td>
																</tr>
																</table>
															</td>							                        
															<td style=" border:0px;padding: 5px 5px 0px 2px;color:#525251; font-weight: normal;text-align: left;">
																<table class="" style="border:0px; background:#ececec;">
																<tr>
																<td><b style="color:#0a86c2; font-size: 12px; text-transform: uppercase; ">Quote Date</b></td>
																</tr>
																<tr>
																<td style="font-size: 11px;color:#62615e">'.$quote_date.'</td>
																</tr>
																</table>
															</td>
															<td style=" border:0px;padding: 5px 5px 0px 5px;color:#525251; text-align: left;">
																<table class="" style="border:0px;">
																<tr>
																<td><img width= "100px" style="padding: 0;" src="'.$image.'" alt=""></td>
																</tr>
																<tr>
																<td></td>
																</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>											
										</table>
									</div> 
									<div>
										<table>
												<tr>
													<td style="height: 10px"></td>
												</tr>
										</table>    
										<table>     
											<tr>
												<td style="width:18%; margin-bottom: 13px;color:#0a80c5; font-size: 13px; font-weight: bold; float: left;width: 100%; text-align: left;"><b>'.ucfirst($client_name).'</b></td>
												<td style="width:30%; margin-bottom: 13px;color:#0a80c5; font-size: 13px; font-weight: bold; float: left;width: 100%; text-align: left;"><b>Site Details</b></td>
												<td style="width:10%; margin-bottom: 13px;color:#0a80c5; font-size: 13px; font-weight: bold; float: left;width: 100%; text-align: left;"><b>Quoted by</b></td>
											</tr>
											<tr>
												<td style="font-size: 11px;color:#62615e"><b>Contact: '.$client_name.'</b></td>
												<td style="font-size: 11px;color:#62615e"><b>Contact: '.$client_name.'</b></td>
												<td style="font-size: 11px;color:#62615e"><b>'.ucfirst($sub_name).'</td>
											</tr>
											<tr>
												<td style="font-size: 11px;color:#62615e"><b>Mobile: '.$client_phone.'</b></td>
												<td style="font-size: 11px;color:#62615e"><b>Mobile: '.$client_phone.'</b></td>
												<td style="font-size: 11px;color:#62615e">'.$sub_phone.'</td>
											</tr>
											<tr>
												<td style="font-size: 11px;color:#62615e">'.$client_addr.'</td>
												<td style="font-size: 11px;color:#62615e;vertical-align: text-top;">'.$siteAddress.'</td>
												<td style="font-size: 11px;color:#62615e;vertical-align: text-top;">'.$sub_email.'</td>
											</tr>   
										</table>
										<table>
											<tr>
												<td style="height: 10px"></td>
											</tr>
										</table>
									</div>
									');
		
		
					$mpdf->SetHTMLFooter('
					<table width="100%">
						<tr>
							<td width="33%"></td>
							<td width="33%" align="center">{PAGENO}/{nbpg}</td>
							<td width="33%" style="text-align: right;"></td>
						</tr>
					</table>');
		
		
					$mpdf->AddPage('', // L - landscape, P - portrait 
							'', '', '', '',
							5, // margin_left
							5, // margin right
						   65, // margin top
						   30, // margin bottom
							0, // margin header
							0); // margin footer
		
					//$mpdf->WriteHTML('Hello World');
		
					$mpdf->WriteHTML($this->renderPartial('/mpdf/jssnewcopy',array('header'=>$header, 'data'=>$data, 'quote_description'=>$quote_description, 'qid'=>$qid, 'quote_date'=>$quote_date,'siteURL'=>$siteURL)));
		
					$filename = 'jss'.$qid.'_'.time().'.pdf';
		
					$mpdf->Output('quotepdf/'.$filename,'F');
		
						$siteURL = Url::base(true). '/quotepdf';
		
						$path = $siteURL.'/'.$filename;
		
							$response = [
								'success' => '1',
								'message' => $path
							];
		
				}else{
		
					$response = [
						'success' => '0',
						'message' => 'Data cannot be blank.'
					];
				}

				ob_start();
				echo json_encode($response);
		}
	}

	public function actionSavePricing(){
		$response = [];
		if(empty($_POST['json'])){
			$response = [
				'success' => 0,
				'message' => 'Fields cannot be blank!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}

		$mydata = ($_POST['json'] != '')?$_POST['json']:json_encode(array());
		$mydata = json_decode($mydata,true);

		//echo "<pre>";print_r($mydata);exit();

		if( empty($mydata['quote_id']) || empty($mydata['gross_margin_percent'])){
			$response = [
				'success' => 0,
				'message' => 'Fields cannot be blank!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}
		else
		{
			$quote_id = $mydata['quote_id'];
			$gross_margin_percent = $mydata['gross_margin_percent'];
			TblQuotes::updateAll(['gross_margin_percent' => $gross_margin_percent], ['=', 'quote_id', $quote_id]);
			$response = [
				'success' => 1,
				'message' => 'Quote Updated!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}
	}

	public function actionGetPaymentMethods(){
		$response = [];

		$paymentMethods = TblPaymentMethod::find()->where(['enabled'=>1])->all();

		$payments = [];
		if($paymentMethods){
			foreach($paymentMethods as $k => $paymentMethod){
				$p['id'] = $paymentMethod->id;
				$p['name'] = $paymentMethod->name;
				$payments[] = $p;
			}
		}

		$response = [
			'success' => 0,
			'message' => 'Payment Methods',
			'data'=>['paymentMethod'=>$payments]
		];
		echo json_encode($response);exit();
	}

	public function actionGetPaymentOptions(){
		$response = [];

		$paymentOptions = TblPaymentOptions::find()->where(['enabled'=>1])->all();

		$payments = [];
		if($paymentOptions){
			foreach($paymentOptions as $k => $paymentOption){
				$p['id'] = $paymentOption->id;
				$p['name'] = $paymentOption->name;
				$payments[] = $p;
			}
		}

		$response = [
			'success' => 1,
			'message' => 'Payment Options',
			'data'=>['paymentOption'=>$payments]
		];
		echo json_encode($response);exit();
	}

	public function actionGetQuoteRoomStatus(){
		$response = [];
		if(empty($_POST['json'])){
			$response = [
				'success' => 0,
				'message' => 'Fields cannot be blank!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}

		$mydata = ($_POST['json'] != '')?$_POST['json']:json_encode(array());
		$mydata = json_decode($mydata,true);

		//echo "<pre>";print_r($mydata);exit();

		$data['quoteSpecialItems'] = [];

		if( empty($mydata['quote_id'])){
			$response = [
				'success' => 0,
				'message' => 'Fields cannot be blank!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}
		else{
			$quote_id = $mydata['quote_id'];

			$rooms = TblRooms::find()->where(["quote_id" => $quote_id])->all();

			$quoteRooms = [];
			foreach($rooms as $k => $r){
				$cr = [];
				$cr['name'] = $r->room_name;
				$cr['id'] = $r->rooms_id;
				$cr['include'] = $r->include;
				$quoteRooms[] = $cr;
			}

			$response = [
				'success' => 1,
				'message' => 'Rooms Found!',
				'data'=>["quoteRooms"=>$quoteRooms]
			];
			echo json_encode($response);exit();
		}
	}

	public function actionSaveQuoteRoomStatus(){
		$response = [];
		if(empty($_POST['json'])){
			$response = [
				'success' => 0,
				'message' => 'Fields cannot be blank!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}

		$mydata = ($_POST['json'] != '')?$_POST['json']:json_encode(array());
		
		$mydata = json_decode($mydata,true);

		//echo "<pre>";print_r($mydata);exit();

		$data['quoteSpecialItems'] = [];

		if( empty($mydata['quote_id']) || empty($mydata['room_ids'])){
			$response = [
				'success' => 0,
				'message' => 'Fields cannot be blank!',
				'data'=>[]
			];
			echo json_encode($response);exit();
		}
		else{
			$quote_id = $mydata['quote_id'];
			$room_ids = $mydata['room_ids'];
			//$room_ids = json_decode($mydata['room_ids'],true);

			TblRooms::updateAll(['include' => 0], ['IN', 'quote_id', $quote_id]);
			
			if(count( $room_ids ) > 0){
				TblRooms::updateAll(['include' => 1], ['IN', 'rooms_id', $room_ids]);
			}

			$data['payments'] = [];
			$data['quoteInclusions'] = [];
			$data['balanceDate']=0;

			$data['summary_id'] = 0;
			$data['noOfPainters'] = 1;
			$data['daysToComplete'] = 0;
			$data['dateEstCommencement'] = 0;
			$data['dateEstCompletion'] = 0;
			$data['authorisedPerson'] = "";

			$data['totalHours'] = 0;
			$data['totalExGst'] = 0;
			$data['gst'] = 0;
			$data['totalInGst'] = 0;

			$data['totalSIP'] = 0;
			$data['pricing']['applicationCost'] = 0;
			$data['pricing']['profitMarkup'] = 0;
			$data['pricing']['defaultDepositPercent'] = 0;
			$data['pricing']['gstPercentage'] = 0;
			$data['pricing']['grossMarginPercent'] = 0;
			//$data['pricing_sub_total'] = 0;
			//$data['pricing_total'] = 0;


			$quote_id = $mydata['quote_id'];
			$sub_id = $mydata['sub_id'];

			$quotePrice = $this->getQuotePrice($mydata);
			if($quotePrice["roomFound"] == 0){
				$response = [
					'success' => 0,
					'message' => 'No Rooms found for this quote.',
					'data'=>[]
				];
				echo json_encode($response);exit();
			}

			$totlIncGst = $quotePrice['totlIncGst'];
			TblQuotes::updateAll(['price' => $totlIncGst], ['=', 'quote_id', $quote_id]);

			$paymentOptions = TblPaymentOptions::find()->where(['enabled'=>1])->all();

			$payments = [];
			if($paymentOptions){
				foreach($paymentOptions as $k => $paymentOption){
					$p['id'] = $paymentOption->id;
					$p['name'] = $paymentOption->name;
					$payments[] = $p;
				}
			}

			$data['paymentOptions'] = $payments;

			$paymentMethods = TblPaymentMethod::find()->where(['enabled'=>1])->all();

			$payments = [];
			if($paymentMethods){
				foreach($paymentMethods as $k => $paymentMethod){
					$p['id'] = $paymentMethod->id;
					$p['name'] = $paymentMethod->name;
					$payments[] = $p;
				}
			}

			$data['paymentMethods'] = $payments;

			if($quotePrice){

				$userSettings = TblUsersSettings::find()->where(['user_id' => $sub_id])->one();

				//echo"<pre>";print_r($userSettings);exit;

				$data['totalHours'] = $quotePrice['totalHours'];
				$data['totalExGst'] = $quotePrice['totl'];
				$data['finalSubTotal'] = $finalSubTotal = $quotePrice['finalSubTotal'];
				$data['gst'] = $quotePrice['gst'];
				$data['totalInGst'] = $quotePrice['totlIncGst'];
				$data['totalSIP'] = $quotePrice['totalSIP'];

				$data['pricing']['applicationCost'] = $userSettings->application_cost;
				$data['pricing']['profitMarkup'] = $profitMarkup = $userSettings->profit_markup;
				$data['pricing']['defaultDepositPercent'] = $userSettings->default_deposit_percent;
				$data['pricing']['gstPercentage'] = $userSettings->gst;

				$quoteDetail = TblQuotes::find()->where(['quote_id' => $sub_id])->one();

				$data['pricing']['grossMarginPercent'] = ($quoteDetail->gross_margin_percent != "")?$quoteDetail->gross_margin_percent:0;
				
				if($data['pricing']['grossMarginPercent'] != 0){
					$cost = $finalSubTotal;
					$grossMarginPercentage = $data['pricing']['grossMarginPercent'];
					$data['pricing']['grossMargin'] = $grossMargin = ($grossMarginPercentage * $cost) / (100 - $grossMarginPercentage);
					$data['pricing']['pricingSubTotal'] = $pricingSubTotal = $cost + $grossMargin;
					$data['pricing']['pricingTotal'] = $pricingTotal = $pricingSubTotal + $data['totalSIP'] + $data['gst'];
				}
				else{
					$cost = $finalSubTotal;

					$data['pricing']['grossMargin'] = $grossMargin = $cost*$profitMarkup/100;

					$data['pricing']['pricingSubTotal'] = $pricingSubTotal = $cost + $grossMargin;

					$data['pricing']['grossMarginPercent'] = $grossMarginPercent = ($grossMargin * 100)/$pricingSubTotal;

					$data['pricing']['pricingTotal'] = $pricingTotal = $pricingSubTotal + $data['totalSIP'] + $data['gst'];
				}

			}

			//echo "<pre>";print_r($quotePrice);exit();

			$roomType = TblRoomTypes::find()
					->joinWith(
								['tblRooms' => 
									function($q) use ($quote_id){
										$q->where(['tbl_rooms.quote_id' => $quote_id,'tbl_rooms.include' => 1]);
									}
								])
					->all();
			//echo "<pre>";print_r($roomType);exit();

			if(count($roomType)){
				foreach ($roomType as $key => $rt) {
					$crt = [];
					$crt['name'] = $rt->name;
					$crt['count'] = count($rt->tblRooms);
					$data['quoteInclusions'][] = $crt;
				}
			}

			$paymentSchedule = TblPaymentSchedule::find()->where(["quote_id"=>$quote_id])->all();

			if(count($paymentSchedule) > 0){
				foreach ($paymentSchedule as $key => $ps) {
					$psArr = [];
					$psArr['payment_schedule_id'] = $ps->id;
					$psArr['descp'] = $ps->descp;
					$psArr['amount'] = $ps->amount;
					$psArr['part_payment'] = $ps->part_payment;
					$psArr['date'] = $ps->created_at;
					$psArr['payment_date'] = $ps->payment_date;

					$data["payments"][] = $psArr;
				}
			}else{
				$psArr = [];
				$psArr['payment_schedule_id'] = 0;
				$psArr['descp'] = 'Initial Payment';
				$psArr['amount'] = 0;
				$psArr['part_payment'] = 30;
				$psArr['date'] = 0;
				$psArr['payment_date'] = 0;

				$data["payments"][] = $psArr;
			}

			$summary = TblSummary::find()->where(["quote_id"=>$quote_id])->one();

			//echo "<pre>";print_r($summary);exit();
			if(count($summary) > 0){
				$data["summary_id"] = $summary->summary_id;
				$data["noOfPainters"] = $summary->painter;
				$data["daysToComplete"] = $summary->days;
				$data["dateEstCommencement"] = $summary->est_comm;
				$data["dateEstCompletion"] = $summary->est_comp;
				$data['payment_method'] = $summary->payment_method;
				$data['payment_option'] = $summary->payment_option;
				$data["authorisedPerson"] = $summary->authorised_person;
				$data["balanceDate"] = $summary->dateBalance;
				if($summary->sign != ""){
					$data["sign"] = $siteURL.$summary->sign;
				}
				else{
					$data["sign"] = "";
				}
			}

			$data["headerDesc"]="Lorem Ipsum is simply dummy text of the printing and typesetting industry.";
    		$data["footerDesc"]="Lorem Ipsum is simply dummy text of the printing and typesetting industry.";

			$response = [
				'success' => 1,
				'message' => 'Summary',
				'data'=>$data
			];
			echo json_encode($response);exit();
		}
	}

	public function findNumbers($ind, $denom, $n, $vals=array()){
		global $x;
		if ($n == 0){
			foreach ($vals as $key => $qty){
				for(; $qty>0; $qty--){
					if($x == ""){
						$x = 0;
					}
					$this->totals[$x][] = $denom[$key];
				}
			}
			$x++;
			return;
		}

		if ($ind == count($denom)) {
			return ;
		}
		$currdenom = $denom[$ind];
		for ($i=0;$i<=($n/$currdenom);$i++){
			$vals[$ind] = $i;
			$this->findNumbers($ind+1,$denom,$n-($i*$currdenom),$vals);
		}
	}


	public function getQuotePrice($mydata){
		$connection = \Yii::$app->db;

			$roomFound = 0;
			if(array_key_exists("room_id", $mydata)){
				$roomIds = [$mydata['room_id']];
				$roomIdsStr = $mydata['room_id'];
			}
			else{
				$quoteRoomsQry = "SELECT rooms_id from tbl_rooms WHERE quote_id = ".$mydata['quote_id']." AND include = 1";
				$roomsQry = $connection->createCommand($quoteRoomsQry);
				$roomsData = $roomsQry->queryAll();

				if(count($roomsData)){
					$roomIds = array_column($roomsData, 'rooms_id');
					$roomIdsStr = implode(',', $roomIds);
				}
				else{
					$roomDimesionArr = [];
					$totalQty = [];
					$prep_level = 0;
					$totalLitres = 0;
					$totalPrice = 0;
					$totalHours = 0;
					$totalHoursPrice = 0;
					$paintPreparation = 0;
					$applicationPreparation1 = 0;
					$applicationPreparation2 = 0;
					$finalSubTotal = 0;
					$finalMargin = 0;
					$totl = 0;
					$gst = 0;
					$totalSIP = 0;
					$totlIncGst = 0;
					$components = 0;
					$quantityProductArr = [];
					$color_consultant = 0;
	
					$substrateArr = [];
					$materialArr = [];

					return array(
						"roomFound" => 0,
						"roomDimesionArr"=>$roomDimesionArr,
						"totalQty"=>$totalQty,
						"prep_level"=>$prep_level,
						"totalPrice"=>$totalPrice,
						"totalLitres"=>$totalLitres,
						"totalHours"=>$totalHours,
						"totalHoursPrice"=>$totalHoursPrice,
						"paintPreparation"=>$paintPreparation,
						"applicationPreparation1"=>$applicationPreparation1, 
						"applicationPreparation2"=>$applicationPreparation2, 
						"finalSubTotal"=>$finalSubTotal, 
						"finalMargin"=>$finalMargin, 
						"totalSIP"=>$totalSIP,
						"totl"=>$totl, 
						"gst"=>$gst, 
						"totlIncGst"=>$totlIncGst,
						"quantityComponentArr" => $components,
						"quantityProductArr" => $quantityProductArr,
						"color_consultant" => $color_consultant,
	
						"substrateArr" => $substrateArr,
						"materialArr" => $materialArr
					);
				}
			}


			$totalHours = 0;
			$totalHoursPrice = 0;
			$applicationPreparation1 = 0;
			$applicationPreparation2 = 0;
			$finalSubTotal = 0;
			$finalMargin = 0;
			$totl = 0;
			$gst = 0;
			$totlIncGst = 0;
			$components = 0;
			$quantityProductArr = [];
			$totalLitres = 0;
			$color_consultant = 0;
			$totalQty = 0;

			$roomAllData = "SELECT tbl_brand_pref.color_consultant, tbl_brand_pref.coats, tbl_brands.name as brand_name, tbl_rooms.quote_id,tbl_rooms.room_name,tbl_rooms.rooms_id as room_id ,tbl_components.name as components_name ,tbl_products.name as products_name,
								tbl_room_comp_pref.option_value,tbl_room_comp_pref.option_id,tbl_room_comp_pref.option_value,tbl_room_pref.comp_id,tbl_room_pref.isSelected,
								tbl_components.group_id,tbl_components.price_method_id,tbl_components.area as comp_area,tbl_products.spread_rate,tbl_products.product_id  as paint_product_id,
								tbl_component_type.work_rate,tbl_component_type.spread_ratio,tbl_component_type.thickness, tbl_component_type.name as comp_type_name, tbl_room_pref.color_id ,
								tbl_paint_defaults.sheen_id as paint_sheen_id,tbl_sheen.name as sheen_name, tbl_paint_defaults.hasUnderCoat, tbl_colors.name as color_name, tbl_colors.hex as color_hex, tbl_colors.tag_id as color_tag_id, 
								tbl_colors.number as color_number, case when (tbl_paint_defaults.undercoat != tbl_products.product_id) then '1' else '0' end as topcoat_status
			
							FROM tbl_rooms 
				
							LEFT JOIN tbl_room_pref ON tbl_rooms.rooms_id=tbl_room_pref.room_id
							LEFT JOIN tbl_room_comp_pref ON tbl_room_pref.id=tbl_room_comp_pref.tbl_room_pref_id
							LEFT JOIN tbl_components ON tbl_room_pref.comp_id=tbl_components.comp_id
							LEFT JOIN tbl_paint_defaults ON (tbl_components.group_id=tbl_paint_defaults.comp_id AND tbl_rooms.quote_id = tbl_paint_defaults.quote_id)
							LEFT JOIN tbl_products ON (tbl_paint_defaults.topcoat=tbl_products.product_id OR tbl_paint_defaults.undercoat = tbl_products.product_id)
							LEFT JOIN tbl_sheen ON (tbl_paint_defaults.sheen_id=tbl_sheen.sheen_id)
							LEFT JOIN tbl_brand_pref ON (tbl_paint_defaults.quote_id=tbl_brand_pref.quote_id)
							LEFT JOIN tbl_brands ON (tbl_brand_pref.brand_id=tbl_brands.brand_id)
							LEFT JOIN tbl_component_type ON tbl_room_comp_pref.option_id=tbl_component_type.comp_type_id
							LEFT JOIN tbl_colors ON tbl_room_pref.color_id=tbl_colors.color_id
			
			
							WHERE tbl_rooms.rooms_id IN (".$roomIdsStr.") AND  option_id IS NOT NULL ORDER BY tbl_rooms.rooms_id ASC";

			//echo $roomAllData;exit;

			$model = $connection->createCommand($roomAllData);

			$components = $model->queryAll();

			if($components)
			{
				$color_consultant = $components->color_consultant;
				//echo "<pre>";print_r($components);exit();

				$roomDim = TblRoomDimensions::find()->where([ 'in', 'room_id', $roomIds ] )->all();

				$roomDimesionArr = [];

				foreach ($roomDim as $key => $rd) {
					$cc = count($roomDimesionArr[$rd->room_id]);
					$roomDimesionArr[$rd->room_id][$cc]['room_id'] = $rd->room_id;
					$roomDimesionArr[$rd->room_id][$cc]['length'] = $rd->length;
					$roomDimesionArr[$rd->room_id][$cc]['width'] = $rd->width;
					$roomDimesionArr[$rd->room_id][$cc]['height'] = $rd->height;
				}

				//echo "<pre>";print_r($roomDimesionArr);exit();

				$quantityComponentArr = [];
				$ifWallExist = [];
				$ifFeatureWallExist = [];
				foreach ($components as $key => $comp) {


					//echo $comp["coats"];exit;

					
					$area = 0;
					$quote_id = $comp['quote_id'];

					if($comp["comp_area"] == "V*F"){
						$area = $comp["option_value"] * $comp["thickness"];

						if($comp["comp_id"] == 8){
							$ifFeatureWallExist[$comp["room_id"]]['exist'] = 1;
							$ifFeatureWallExist[$comp["room_id"]]['comp_key'] = $key;

							if( array_key_exists( $comp['room_id'] ,$ifWallExist) ){
								$roomWallCompKey = $ifWallExist[$comp['room_id']]['comp_key'];
								$roomWallArea = $components[$roomWallCompKey]['area'];
								if($roomWallArea > $area){
									$deductedRoomWallArea = $roomWallArea-$area;
									$components[$roomWallCompKey]['area'] = $deductedRoomWallArea;
									$components[$roomWallCompKey]['litre'] = $deductedRoomWallArea/( $components[$roomWallCompKey]['spread_rate']  * $components[$roomWallCompKey]['spread_ratio'] );
									$components[$roomWallCompKey]['hours'] = $deductedRoomWallArea/$components[$roomWallCompKey]['work_rate'];
								}
							}
						}
						
						$litre = ($area/( $comp['spread_rate'] * $comp['spread_ratio'] )) * $comp["coats"];
						$hours = ($comp['option_value'] * ($comp['work_rate']/60))  * $comp["coats"] ;
						
					}
					elseif ($comp["comp_area"] == "L*W") {
						
						foreach($roomDimesionArr[$comp['room_id']] as $rdkey => $dim){
							$area += $dim['length'] * $dim['width'];
						}
						$litre = ($area/($comp['spread_rate']  * $comp['spread_ratio'] ) ) * $comp["coats"] ;
						$hours = ($area/$comp['work_rate'])  * $comp["coats"]  ;
					}
					elseif ($comp["comp_area"] == "2*(L*H)+2*(W*H)") {
						$i = 0;

						if($comp["comp_id"] == 7){
							$ifWallExist[$comp["room_id"]]['exist'] = 1;
							$ifWallExist[$comp["room_id"]]['comp_key'] = $key;
						}

						foreach($roomDimesionArr[$comp['room_id']] as $rdkey => $dim){
							if($i == 0){
								$val1 = 2*($dim['length'] * $dim['height']);
								$val2 = 2*($dim['width'] * $dim['height']);
								$area += $val1 + $val2;
							}

							if($i > 1){
								if($dim['selected'] == 0){
									$area += 2*($dim['length']*$dim['height'])+2*($dim['width']*$dim['height']);
								}
								elseif($dim['selected'] == 1){
									$area += 2*($dim['length']*$dim['height']);
								}
								else{
									$area += 2*($dim['width']*$dim['height']);
								}
							}
							
							$i++;
						}
						
						if($comp["comp_id"] == 7){
							//echo "<pre>";print_r($ifFeatureWallExist);exit;
							if( array_key_exists( $comp['room_id'] ,$ifFeatureWallExist) ){
								$featureWallCompKey = $ifFeatureWallExist[$comp['room_id']]['comp_key'];
								$featureWallArea = $components[$featureWallCompKey]['area'];
								if($area > $featureWallArea){
									$area = $area-$featureWallArea;
								}
							}
						}

						$litre = ($area/( $comp['spread_rate']  * $comp['spread_ratio'] ) ) * $comp["coats"];
						$hours = ($area/$comp['work_rate']) * $comp["coats"] ;
					}
					else{
						// ((2L+2W)*T) / 1000
						foreach($roomDimesionArr[$comp['room_id']] as $rdkey => $dim){
							$val1 = 2*($dim['length']);
							$val2 = 2*($dim['width']);

							$val3 = ($val1 + $val2);
							$val4 = $val3 * $comp['thickness'];

							$area = ($val4 /1000);

							break;
						}

						$litre = ($area/($comp["spread_rate"] * $comp['spread_ratio']) ) * $comp["coats"] ;
						$hours = ($area/$comp['work_rate']) * $comp["coats"] ;
					}

					$components[$key]['area'] = $area;
					$components[$key]['litre'] = $litre;
					$components[$key]['hours'] = $hours;

					$totalLitres += $litre;
					$totalHours += $hours;

					if(array_key_exists($comp['paint_product_id'], $quantityComponentArr) ){
						$quantityComponentArr[$comp['paint_product_id']] = $quantityComponentArr[$comp['paint_product_id']] + $litre;
					}
					else{
						$quantityComponentArr[$comp['paint_product_id']] = $litre;
					}
					
				}

				//echo $totalLitres."  :  ".$totalHours;exit;
				//echo "<pre>";print_r($components);exit();

				foreach ($components as $key => $compData) {

					$substrate = [];

					$substrate['substrate'] = $compData['components_name'];
					if($compData['price_method_id'] == 2){
						$substrate['type'] = $compData['comp_type_name']." X ".$compData['option_value'] ;
					}
					else{
						$substrate['type'] = $compData['comp_type_name'];
					}

					$substrate['room_name'] = $compData['room_name'];

					$substrate['material'] = $compData['products_name'];
					if($compData['color_id'] == 0){
						$substrate['color'] = null;
					}
					else{
						$substrate['color']['id'] = $compData['color_id'];
						$substrate['color']['name'] = $compData['color_name'];
						$substrate['color']['hex'] = $compData['color_hex'];
						$substrate['color']['tag_id'] = $compData['color_tag_id'];
						$substrate['color']['number'] = $compData['color_number'];
					}
						
					$substrate['hours'] = $compData['hours'];
					$substrate['litre'] = $compData['litre'];

					$substrateArr[$key]=$substrate;
				}

				//echo '<pre>';print_r($substrateArr);exit();

				$productIds = [];
				$sheenIds = [];
				$groupNamesEncountered = [];
				$groupsFiltered = [];

				foreach ($components as $key => $group) { // Reffered from getProductFromLocalDb
					$products_name = $group['products_name'];
					$paint_product_id = $group['paint_product_id'];
					$paint_sheen_id = $group['paint_sheen_id'];

					if(!in_array($paint_product_id, $productIds)){
						$productIds[] = $paint_product_id;
					}

					if(!in_array($paint_sheen_id, $sheenIds)){
						$sheenIds[] = $paint_sheen_id;
					}


					if(!in_array($products_name, $groupNamesEncountered)  ) {
						$groupNamesEncountered[] = $products_name;
						$groupsFiltered[]=$group;
					}				
				}


				foreach ($groupsFiltered as $key => $maingroup) {
					$paint_product_id = $maingroup['paint_product_id'];
					$cctl = $quantityComponentArr[$paint_product_id];
					$groupsFiltered[$key]['totalLitres'] = $cctl;
				}

				//echo '<pre>';print_r($groupsFiltered);exit();

				//getProductStocks

				$productIdsStr = implode(",", $productIds);

				$connection = \Yii::$app->db;

				$productStockQry = "SELECT tbl_product_stock.product_id,tbl_product_stock.sheen_id,tbl_product_stock.litres,tbl_product_stock.code,tbl_product_stock.name,tbl_product_stock.price, tbl_products.name as product_name from tbl_product_stock, tbl_products where tbl_product_stock.product_id=tbl_products.product_id and tbl_products.product_id IN (".$productIdsStr.") ORDER BY litres ASC";

				$model = $connection->createCommand($productStockQry);

				$productStock = $model->queryAll();

				//echo "<pre>";print_r($productStock);exit;
				//echo "<pre>";print_r($groupsFiltered);exit();

				$arrayOfStocksRequired = [];

				$materialArr = [];
				$quantityProductArr = [];

				foreach ($groupsFiltered as $key => $gfd) {

					$arrayOfStockItems = [];
					
					//echo "<pre>";print_r($gfd);
					//echo "<pre>";print_r($productStock);

					$product_id = $gfd['paint_product_id'];
					$sheen_id = $gfd['paint_sheen_id'];

					foreach ($productStock as $key => $ps) {
						if($gfd['topcoat_status'] == 1){
							if($ps["product_id"] == $product_id && $ps['sheen_id'] == $sheen_id){
								$arrayOfStockItems[]=$ps;
							}
						}
						else{
							if($ps["product_id"] == $product_id ){
								$arrayOfStockItems[]=$ps;
							}
						}
					}

					//echo "<pre>";print_r($arrayOfStockItems);exit;

					if( count($arrayOfStockItems)!=0 ){

						usort($arrayOfStockItems, function($a, $b) { //Sort the array using a user defined function
							return $a['litres'] < $b['litres'] ? -1 : 1; //Compare the scores
						});
	
						//echo "<pre>";print_r($arrayOfStockItems);exit();
	
	
						$litresRequired = $gfd['totalLitres'];
	
	
						//first check how many max size buckets we need
	
	
						$maxSizeStockItem = $arrayOfStockItems[count($arrayOfStockItems)-1];
	
						//echo "<pre>";print_r($maxSizeStockItem);
	
						$maxSizeBucketAvailable = $maxSizeStockItem['litres'];					
	
						$maxSizeBucketsReq = $litresRequired/$maxSizeBucketAvailable;
						
						$maxSizeBucketsReq = (int) $maxSizeBucketsReq;
	
						if($maxSizeBucketsReq > 0){
							$gfd['stock_name'] = $maxSizeStockItem['name'];
							$gfd['stock_qty'] = $maxSizeBucketsReq;
							$gfd['stock_price'] = $maxSizeStockItem['price'];
							$gfd['stock_litres'] = $maxSizeStockItem['litres'];
	
							$arrayOfStocksRequired = $gfd;
	
							$cMArr = [];
							$cMArr['material'] = $gfd['stock_name'];
							$cMArr['price'] = $gfd['stock_price'] * $maxSizeBucketsReq;
							$cMArr['qty'] = $maxSizeBucketsReq;

							if($gfd['color_id'] == 0){
								$cMArr['color'] = null;
							}
							else{
								$cMArr['color']['id'] = $gfd['color_id'];
								$cMArr['color']['name'] = $gfd['color_name'];
								$cMArr['color']['hex'] =  $gfd['color_hex'];
								$cMArr['color']['tag_id'] = $gfd['color_tag_id'];
								$cMArr['color']['number'] = $gfd['color_number'];
							}
							$materialArr[] = $cMArr;
	
							$quantityProductArr[] = $gfd;
							
							$quantityProductArr[count($quantityProductArr)-1]['material'] = $gfd['stock_name'];
							$quantityProductArr[count($quantityProductArr)-1]['price'] = $gfd['stock_price'];
							$quantityProductArr[count($quantityProductArr)-1]['qty'] = $gfd['stock_qty'];

							if($gfd['color_id'] == 0){
								$quantityProductArr[count($quantityProductArr)-1]['color'] = null;
							}
							else{
								$quantityProductArr[count($quantityProductArr)-1]['color']['id'] = $gfd['color_id'];
								$quantityProductArr[count($quantityProductArr)-1]['color']['name'] = $gfd['color_name'];
								$quantityProductArr[count($quantityProductArr)-1]['color']['hex'] = $gfd['color_hex'];
								$quantityProductArr[count($quantityProductArr)-1]['color']['tag_id'] = $gfd['color_tag_id'];
								$quantityProductArr[count($quantityProductArr)-1]['color']['number'] = $gfd['color_number'];
							}

							$totalQty += $maxSizeBucketsReq;
							$totalPrice += $cMArr['price'];
	
						}
	
						//echo "<pre>";print_r($arrayOfStocksRequired);exit();
	
						$pendingLitres = $litresRequired - ($maxSizeBucketsReq*$maxSizeBucketAvailable);
						
						$availableStocks = [];
						$nearUpper = "";
						$nearUpperPrice = "";
						$nearUpperStockItem = [];
	
						$nearLower = "";
						$nearLowerPrice = "";
						$nearLowerStockItem = [];
	
						$litreStock = [];
	
						$stockCombinations = [];
						// /echo $pendingLitres;
						//echo "<pre>";print_r($arrayOfStockItems);exit;
	
						foreach ($arrayOfStockItems as $key => $stockItem) {
							$litres = $stockItem['litres'];
							$availableStocks[]= $litres;
	
							//echo $pendingLitres;exit();
	
							if ($litres > $pendingLitres && $nearUpperPrice=="") {
								$nearUpper       = $litres;
								$nearUpperPrice  = $stockItem["price"];
								$nearUpperStockItem = $stockItem;
							}
							if ($litres < $pendingLitres) {
								$nearLower       = $litres;
								$nearLowerPrice  = $stockItem["price"];
								$nearLowerStockItem = $stockItem;
							}
	
							$litreStock[$stockItem['litres']] = $stockItem;
	
						}
	
						//echo "<pre>";print_r($nearUpperStockItem);exit();
	
						$totalNearLowerPrice = 0;
	
						$pendingLitres = ceil($pendingLitres);
	
						//echo $pendingLitres;exit();
	
						$availableStocks = array_unique($availableStocks);
						$availableStocks = array_values($availableStocks);
	
	
						// compare pending litre less than lowest then pick the first lowest
	
						//echo("<pre>");print_r($productStock);exit;
	
						$smallestStock = $arrayOfStockItems[0];
						$lowestStockLitre = $smallestStock['litres'];

						//echo $lowestStockLitre;exit;
	
						$highestStock = $arrayOfStockItems[count($arrayOfStockItems)-1];
						$highestStockPrice = $highestStock['price'];
	
						//echo $highestStockPrice;
	
						// get the economical combination
	
						//echo "<pre>";print_r($pendingLitres);
	
						//echo "<pre>";print_r($lowestStockLitre);
	

						if($pendingLitres < $lowestStockLitre){
							$cMArr = [];
							$packagecount = 1;
							$cMArr['material'] = $smallestStock['name'];
							$cMArr['price'] = $smallestStock['price'] * $packagecount;
							$cMArr['qty'] = $packagecount;
							if($gfd['color_id'] == 0){
								$cMArr['color'] = null;
							}
							else{
								$cMArr['color']['id'] = $gfd['color_id'];
								$cMArr['color']['name'] = $gfd['color_name'];
								$cMArr['color']['hex'] =  $gfd['color_hex'];
								$cMArr['color']['tag_id'] = $gfd['color_tag_id'];
								$cMArr['color']['number'] = $gfd['color_number'];
							}
							$materialArr[] = $cMArr;
	
							$quantityProductArr[] = $gfd;

							$quantityProductArr[count($quantityProductArr)-1]['stock_name'] = $smallestStock['stock_name'];
							$quantityProductArr[count($quantityProductArr)-1]['stock_price'] = $smallestStock['price'];
							$quantityProductArr[count($quantityProductArr)-1]['stock_qty'] = $packagecount;
							$quantityProductArr[count($quantityProductArr)-1]['stock_litres'] = $smallestStock['litres'];

							$quantityProductArr[count($quantityProductArr)-1]['material'] = $gfd['stock_name'];
							$quantityProductArr[count($quantityProductArr)-1]['price'] = $gfd['stock_price'];
							$quantityProductArr[count($quantityProductArr)-1]['qty'] = $gfd['stock_qty'];
							if($gfd['color_id'] == 0){
								$quantityProductArr[count($quantityProductArr)-1]['color'] = null;
							}
							else{
								$quantityProductArr[count($quantityProductArr)-1]['color']['id'] = $gfd['color_id'];
								$quantityProductArr[count($quantityProductArr)-1]['color']['name'] = $gfd['color_name'];
								$quantityProductArr[count($quantityProductArr)-1]['color']['hex'] = $gfd['color_hex'];
								$quantityProductArr[count($quantityProductArr)-1]['color']['tag_id'] = $gfd['color_tag_id'];
								$quantityProductArr[count($quantityProductArr)-1]['color']['number'] = $gfd['color_number'];
							}
	
							$totalQty += $packagecount;
							$totalPrice += $cMArr['price'];
						}
						else{
							$this->totals= [];
							//echo "<pre>";print_r($availableStocks);print_r($pendingLitres);exit();
							$this->findNumbers(0, $availableStocks, $pendingLitres);
							//echo "<pre>";print_r($availableStocks);exit();
	
							$combPrice = [];
							$combinations = $this->totals;

							//echo "<pre>";print_r($combinations);
	
							if(count($combinations) > 0){
								//echo "<pre>";print_r($combinations);exit();
	
								foreach ($combinations as $ckey => $cComb) {
									$lPrice = 0;
									$sameProduct = [];
									foreach ($cComb as $lkey => $lComb) {
										$lPrice += $litreStock[$lComb]['price'];
										$stockCombinations[$ckey][] = $litreStock[$lComb];
									}
									$combPrice[$ckey] = $lPrice;
								}
	
								//echo "<pre>";print_r($combPrice);
	
								$minPriceKey = array_keys($combPrice, min($combPrice));
								$minPriceValue = min($combPrice); 
	
								//echo "<pre>";print_r($minPriceKey);
								//echo "<pre>";print_r($minPriceValue);
	
								// check if economical is costlt then max bucket sixe then use the max bucket and change the quantity of greatest bucket size
	
								//echo $minPriceValue." < ".$nearUpperPrice;exit();
	
								if($minPriceValue < $nearUpperPrice){
	
									$similarStocksQty = array_count_values($combinations[$minPriceKey[0]]);
									$minCombinationStock = $stockCombinations[$minPriceKey[0]];
	
									//echo "<pre>";print_r($stockCombinations);
									//echo "<pre>";print_r($minCombinationStock);
	
									foreach ($minCombinationStock as $key => $cs) {
										$cMArr = [];
										$packagecount = $similarStocksQty[$cs['litres']];
										$cMArr['material'] = $cs['name'];
										$cMArr['price'] = $cs['price'] * $packagecount;
										$cMArr['qty'] = $packagecount;
	
										if($gfd['color_id'] == 0){
											$cMArr['color'] = null;
										}
										else{
											$cMArr['color']['id'] = $gfd['color_id'];
											$cMArr['color']['name'] = $gfd['color_name'];
											$cMArr['color']['hex'] =  $gfd['color_hex'];
											$cMArr['color']['tag_id'] = $gfd['color_tag_id'];
											$cMArr['color']['number'] = $gfd['color_number'];
										}
	
										$materialArr[] = $cMArr;
										
										$quantityProductArr[] = $gfd;

										$quantityProductArr[count($quantityProductArr)-1]['stock_name'] = $gfd['stock_name'];
										$quantityProductArr[count($quantityProductArr)-1]['stock_price'] = $gfd['stock_price'];
										$quantityProductArr[count($quantityProductArr)-1]['stock_qty'] = $packagecount;
										$quantityProductArr[count($quantityProductArr)-1]['stock_litres'] = $gfd['stock_litres'];

										$quantityProductArr[count($quantityProductArr)-1]['material'] = $gfd['stock_name'];
										$quantityProductArr[count($quantityProductArr)-1]['price'] = $gfd['stock_price'];
										$quantityProductArr[count($quantityProductArr)-1]['qty'] = $gfd['stock_qty'];
										if($gfd['color_id'] == 0){
											$quantityProductArr[count($quantityProductArr)-1]['color'] = null;
										}
										else{
											$quantityProductArr[count($quantityProductArr)-1]['color']['id'] = $gfd['color_id'];
											$quantityProductArr[count($quantityProductArr)-1]['color']['name'] = $gfd['color_name'];
											$quantityProductArr[count($quantityProductArr)-1]['color']['hex'] = $gfd['color_hex'];
											$quantityProductArr[count($quantityProductArr)-1]['color']['tag_id'] = $gfd['color_tag_id'];
											$quantityProductArr[count($quantityProductArr)-1]['color']['number'] = $gfd['color_number'];
										}
	
										$totalQty += $packagecount;
										$totalPrice += $cMArr['price'];
									}
								}
								else{
									//echo "<pre>";print_r($nearUpperStockItem);exit;

									$cMArr = [];
									$packagecount = 1;
									$cMArr['material'] = $nearUpperStockItem['name'];
									$cMArr['price'] = $nearUpperStockItem['price'];
									$cMArr['qty'] = $packagecount;
	
									if($gfd['color_id'] == 0){
										$cMArr['color'] = null;
									}
									else{
										$cMArr['color']['id'] = $gfd['color_id'];
										$cMArr['color']['name'] = $gfd['color_name'];
										$cMArr['color']['hex'] =  $gfd['color_hex'];
										$cMArr['color']['tag_id'] = $gfd['color_tag_id'];
										$cMArr['color']['number'] = $gfd['color_number'];
									}
	
									$totalQty += $packagecount;
									$totalPrice += $cMArr['price'];
	
									$materialArr[] = $cMArr;

									$gfd['sto'] = 
									$quantityProductArr[] = $gfd;

									$quantityProductArr[count($quantityProductArr)-1]['stock_name'] = $nearUpperStockItem['name'];
									$quantityProductArr[count($quantityProductArr)-1]['stock_price'] = $nearUpperStockItem['price'];
									$quantityProductArr[count($quantityProductArr)-1]['stock_qty'] = $packagecount;
									$quantityProductArr[count($quantityProductArr)-1]['stock_litres'] = $nearUpperStockItem['litres'];

									$quantityProductArr[count($quantityProductArr)-1]['material'] = $cMArr['material'];
									$quantityProductArr[count($quantityProductArr)-1]['price'] = $cMArr['price'];
									$quantityProductArr[count($quantityProductArr)-1]['qty'] = $cMArr['qty'];
									if($gfd['color_id'] == 0){
										$quantityProductArr[count($quantityProductArr)-1]['color'] = null;
									}
									else{
										$quantityProductArr[count($quantityProductArr)-1]['color']['id'] = $gfd['color_id'];
										$quantityProductArr[count($quantityProductArr)-1]['color']['name'] = $gfd['color_name'];
										$quantityProductArr[count($quantityProductArr)-1]['color']['hex'] = $gfd['color_hex'];
										$quantityProductArr[count($quantityProductArr)-1]['color']['tag_id'] = $gfd['color_tag_id'];
										$quantityProductArr[count($quantityProductArr)-1]['color']['number'] = $gfd['color_number'];
									}
								}
							}
							else{
								$cMArr = [];
								$packagecount = 1;

								$cMArr['material'] = $nearUpperStockItem['name'];
								$cMArr['price'] = $nearUpperStockItem['price'];
								$cMArr['qty'] = $packagecount;
	
								if($gfd['color_id'] == 0){
									$cMArr['color'] = null;
								}
								else{
									$cMArr['color']['id'] = $gfd['color_id'];
									$cMArr['color']['name'] = $gfd['color_name'];
									$cMArr['color']['hex'] =  $gfd['color_hex'];
									$cMArr['color']['tag_id'] = $gfd['color_tag_id'];
									$cMArr['color']['number'] = $gfd['color_number'];
								}

								$gfd['stock_name'] = $cMArr['material'] ;
								$gfd['stock_qty'] = $cMArr['qty'];
								$gfd['stock_price'] = $cMArr['price'];
								$gfd['stock_litres'] = $maxSizeStockItem['litres'];
								
								$quantityProductArr[] = $gfd;

								$quantityProductArr[count($quantityProductArr)-1]['stock_name'] = $gfd['stock_name'];
								$quantityProductArr[count($quantityProductArr)-1]['stock_price'] = $gfd['stock_price'];
								$quantityProductArr[count($quantityProductArr)-1]['stock_qty'] = $packagecount;
								$quantityProductArr[count($quantityProductArr)-1]['stock_litres'] = $gfd['stock_litres'];

								$quantityProductArr[count($quantityProductArr)-1]['material'] = $gfd['stock_name'];
								$quantityProductArr[count($quantityProductArr)-1]['price'] = $gfd['stock_price'];
								$quantityProductArr[count($quantityProductArr)-1]['qty'] = $gfd['stock_qty'];

								if($gfd['color_id'] == 0){
									$quantityProductArr[count($quantityProductArr)-1]['color'] = null;
								}
								else{
									$quantityProductArr[count($quantityProductArr)-1]['color']['id'] = $gfd['color_id'];
									$quantityProductArr[count($quantityProductArr)-1]['color']['name'] = $gfd['color_name'];
									$quantityProductArr[count($quantityProductArr)-1]['color']['hex'] = $gfd['color_hex'];
									$quantityProductArr[count($quantityProductArr)-1]['color']['tag_id'] = $gfd['color_tag_id'];
									$quantityProductArr[count($quantityProductArr)-1]['color']['number'] = $gfd['color_number'];
								}
	
								$totalQty += $packagecount;
								$totalPrice += $cMArr['price'];
	
								$materialArr[] = $cMArr;
							}
						}

					}
				}
				//echo "<pre>";print_r($materialArr);exit();
				//exit();
				// Get Prep Level from QuoteId

				$brandPref = TblBrandPref::find()->where(['quote_id' => $quote_id])->one();
				$prep_level = $brandPref->prep_level;
				if($prep_level == 1){
					$preplvl = 5;
				}
				else{
					$preplvl = 10;
				}

				$totalHoursPrice = $totalHours * 100;

				$paintprp = ($totalPrice * $preplvl)/100;

				$paintPreparation = $paintprp;

				$applicationPreparation1 = ($totalHours*$preplvl)/100;

				$applicationPreparation2 = (($totalHours*$preplvl)/100)*100;

				$finalSubTotal = $totalPrice + ($totalHours * 100) + $paintprp + ((($totalHours*$preplvl)/100)*100);

				$totalSIP = 0;

				//$roomSpecialItems = TblRoomSpecialItems::find()->where([ "room_id"=>$mydata['room_id'] ])->all();

				if(array_key_exists("room_id", $mydata)){
					$roomSpecialItems = TblRoomSpecialItems::find()->where([ "room_id"=>$mydata['room_id'] ])->all();
				}
				else{
					$quote_id = $mydata['quote_id'];
					//$roomIds = array_column($roomsData, 'rooms_id');
					$roomSpecialItems = TblRoomSpecialItems::find()
					->where(['=', 'tbl_room_special_items.include', 1])
					->andWhere(['in', 'tbl_room_special_items.room_id', $roomIds])
					->all();
				}
				foreach ($roomSpecialItems as $key => $rsi) {
					$totalSIP += $rsi->price;
				}
				
				$finalSubTotal += $totalSIP;

				$finalMargin = ($finalSubTotal * 59)/100;

				$totl = $finalSubTotal + $finalMargin;

				$gst = $totl/10;

				$totlIncGst = $totl + ($totl/10);

			}
			else{
				$prep_level = 0;
				$totalLitres = 0;
				$totalPrice = 0;
				$totalHours = 0;
				$totalHoursPrice = 0;
				$applicationPreparation1 = 0;
				$applicationPreparation2 = 0;
				$finalSubTotal = 0;
				$finalMargin = 0;
				$totl = 0;
				$gst = 0;
				$totlIncGst = 0;
				$components = 0;
				$quantityProductArr = [];
				$color_consultant = 0;

				$substrateArr = [];
				$materialArr = [];
			}

			//echo '<pre>';print_r($quantityProductArr);exit();

			return array(
					"roomFound" => 1,
					"roomDimesionArr"=>$roomDimesionArr,
					"totalQty"=>$totalQty,
					"prep_level"=>$prep_level,
					"totalPrice"=>$totalPrice,
					"totalLitres"=>$totalLitres,
					"totalHours"=>$totalHours,
					"totalHoursPrice"=>$totalHoursPrice,
					"paintPreparation"=>$paintPreparation,
					"applicationPreparation1"=>$applicationPreparation1, 
					"applicationPreparation2"=>$applicationPreparation2, 
					"finalSubTotal"=>$finalSubTotal, 
					"finalMargin"=>$finalMargin, 
					"totalSIP"=>$totalSIP,
					"totl"=>$totl, 
					"gst"=>$gst, 
					"totlIncGst"=>$totlIncGst,
					"quantityComponentArr" => $components,
					"quantityProductArr" => $quantityProductArr,
					"color_consultant" => $color_consultant,

					"substrateArr" => $substrateArr,
					"materialArr" => $materialArr
				);
	}

}