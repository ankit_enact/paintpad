<?php

namespace frontend\controllers;

use Yii;
use app\models\TblContacts;
use app\models\TblContactsSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use app\models\TblAppointments;
use app\models\TblUsers;


/**
 * ContactController implements the CRUD actions for TblContacts model.
 */
class ContactController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
     return [
         'access' => [
             'class' => AccessControl::className(),
             'only' => ['index', 'searchcontact','appointment-detail','all-contacts'],
             'rules' => [
                [
                    'actions' => ['index','searchcontact','appointment-detail','all-contacts'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
                [
                    'actions' => ['appointment-detail','all-contacts'],
                    'allow' => true,
                    'roles' => ['?'],
                ],
                [
                    'actions' => ['about'],
                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {
                        return User::isUserAdmin(Yii::$app->user->identity->username);
                    }
                ],
            ],
        ],
        'verbs' => [
            'class' => VerbFilter::className(),
            'actions' => [
                'logout' => ['post'],
            ],
        ],
    ];
}




    // public function beforeAction($action)
    // {      
    //     if ($this->action->id == 'index') {
    //         Yii::$app->controller->enableCsrfValidation = false;
    //     }
    //     return true; 
    // }



    /**
     * Lists all TblContacts models.
     * @return mixed
     */

    public function actionIndex()
    {
       //echo "<pre>";
        $model = TblContacts::find()->where(['subscriber_id' => Yii::$app->user->id])->orderBy('name','ASC')->all();
        // $model = TblContacts::find()->where(['subscriber_id' => Yii::$app->user->id])->orderBy('name','ASC')->limit(50)->offset(0)->all();
        $contacts = TblContacts::listSubscriberContacts();
        $modelAppointment = new TblAppointments();
        $dataContact = TblContacts::find()->select('name,contact_id,subscriber_id')->all();
        // echo count($contacts);
        // exit();
        return $this->render('index',
            [
                'model'=>$model,
                'contacts'=>$contacts,
                'modelAppointment'=>$modelAppointment,
                'dataContact'=>$dataContact,

            ]);
    } //actionIndex


    public function actionAllContacts(){

        $mydata = ($_POST['json'] != '')?$_POST['json']:json_encode(array());
        $mydata = json_decode($mydata,true);
        $page  = !empty($mydata['page'])?$mydata['page']:0;    
        $response = [];
        $offset = ($page==0) ? 0 : (50*$page);

        $connection = \Yii::$app->db;
        $contactsQry = 'SELECT * from tbl_contacts WHERE subscriber_id = "'.Yii::$app->user->id.'" ORDER BY name ASC LIMIT '.$offset.',50';
        $contactsQryPrep = $connection->createCommand($contactsQry);
        $contactsData = $contactsQryPrep->queryAll();
        if(count($contactsData)){
            $response = [
                'success' => '1',
                'message' => 'Contacts All',
                'data' => $contactsData,
            ];
        }else{
            $response = [
                'success' => '1',
                'message' => 'No more contact',
                'data' => [],
            ];
        }

        ob_start();
        echo json_encode($response);  
    }//actionAllContacts



    public function actionSearchcontact()
    {
        $model = TblContacts::find()->where(['subscriber_id' => Yii::$app->user->id])->orderBy('name','ASC')->all();
       //echo "<pre>";
        //print_r($model);
        $list=[];
        foreach($model as $m)
        {
            $li['id'] = $m->contact_id;
            $li['name']=$m->name;
            $li['phone']=$m->phone;
            $li['image']=$m->image;
            $list[]=$li;
            
            //print_r(count($list));
        }
        $response = [
            'success' => '1',
            'message' => 'Contacts Details!',
            'data' => $list,
        ];
        ob_start();
        echo json_encode($response);exit();
         //echo json_encode($list);exit();
    }



    public function actionAppointmentDetail()
    {
        //echo "<pre>";
        // print_r($_POST);
        // exit();
        $mydata = ($_POST['json'] != '')?$_POST['json']:json_encode(array());
        $mydata = json_decode($mydata,true);
        //print_r($mydata['contact_id']);
        //exit();
        $sub_id  = !empty($mydata['subscriber_id'])?$mydata['subscriber_id']:'';
        $con_id  = !empty($mydata['contact_id'])?$mydata['contact_id']:'';    
       //echo $sub_id.'---'.$con_id ; 
       // $sub_id=2;
        $response = [];

        $arr = array();

        // validate
        if(empty($sub_id) || empty($con_id)){
          $response = [
            'success' => '0',
            'message' => 'Fields cannot be blank!',
            'data' => '',
        ];
    }
    else
    {
       $subscriber = \common\models\User::findIdentity($sub_id);
       if(!empty($subscriber)){
        $model = TblAppointments::find()
        ->joinWith('contact')                          
        ->where([ 'tbl_contacts.subscriber_id' => $sub_id])
        ->andWhere(['tbl_contacts.contact_id'=>$con_id])
        ->orderBy('created_at','DESC')
        ->all();

        $uArr = array(); 
        $fArray = [];

        foreach($model as $contact)
        { 

            $address_id=$contact->id;
            $addressNote=$contact->note;
            $address=$contact->formatted_addr;
            $address_date=$contact->date;
            $address_duration=$contact->duration;

            $uArr['id']=$address_id;
            $uArr['note']=$addressNote;
            $uArr['address']=$address;
            $uArr['date']=$address_date;
            $uArr['duration']=$address_duration;
            $fArray[]=$uArr;
        }



        $response = [
            'success' => '1',
            'message' => 'Contacts Details!',
            'data' => $fArray,
        ];


    }
    else
    {
        $response = [
            'success' => '0',
            'message' => 'Subscriber doesnot exist in database!',
            'data' => '',
        ];
    }
}
ob_start();
echo json_encode($response);  

}

    /**
     * Displays a single TblContacts model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */


    /**
     * Creates a new TblContacts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */


    /**
     * Updates an existing TblContacts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    /**
     * Deletes an existing TblContacts model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */


    /**
     * Finds the TblContacts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TblContacts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */


}
