<?php

namespace frontend\controllers;

use Yii;
use app\models\TblContacts;
use app\models\TblContactsSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;
use yii\web\UploadedFile;


use yii\filters\AccessControl;

use app\models\TblBrands;
use app\models\TblBrandsSearch;
use app\models\TblAddress;
use app\models\TblQuotes;
use app\models\TblColors;
use app\models\TblSheen;
use app\models\TblProducts;
use app\models\TblStrengths;
use app\models\TblBrandPref;
use app\models\TblPaintDefaults;
use app\models\TblRoomTypes;
use app\models\TblColorTags;
use app\models\TblPrepLevel;
use app\models\TblSpecialItems;
use yii\helpers\Url;



/**
 * ContactController implements the CRUD actions for TblContacts model.
 */
class QuoteController extends Controller
{

	public function behaviors()
    {
     return [
         'access' => [
             'class' => AccessControl::className(),
             'only' => ['index', 'view'],
             'rules' => [
               [
                  'actions' => ['index', 'view'],
                  'allow' => true,
                  'roles' => ['@'],
              ],
              [
                  'actions' => ['about'],
                  'allow' => true,
                  'roles' => ['@'],
                  'matchCallback' => function ($rule, $action) {
                     return User::isUserAdmin(Yii::$app->user->identity->username);
                 }
             ],
         ],
     ],
     'verbs' => [
       'class' => VerbFilter::className(),
       'actions' => [
          'logout' => ['post'],
      ],
  ],
];
}


    /**
     * Lists all TblContacts models.
     * @return mixed
     */
    public function actionIndex()
    {
        $contacts = TblContacts::listSubscriberContacts();
        /* mandaory fields */
        $sub_id  = Yii::$app->user->id;
        $con_id  = !empty($_GET['contact_id'])?$_GET['contact_id']:'';

        $type   = !empty($_GET['type'])?$_GET['type']:'';
        $status = !empty($_GET['status'])?$_GET['status']:'';

        $from   = !empty($_GET['from'])?$_GET['from']:'';
        $to     = !empty($_GET['to'])?$_GET['to']:'';

        $quoteId = !empty($_GET['quote_id'])?$_GET['quote_id']:'';

        $page   = isset($_GET['page'])?$_GET['page']:'';

        //echo 'here-'.$sub_id; exit;    

        $response = [];

        $arr = array();

        // validate
        if(empty($sub_id)){
          $response = [
            'success' => '0',
            'message' => 'Fields cannot be blank!',
            'data' => '',
        ];
    }
    else{

            // search in the database, there is no email referred
        $subscriber = \common\models\User::findIdentity($sub_id);

        if(!empty($subscriber)){

            $siteURL = Url::base(true);

            $modelQuery = TblQuotes::find()->joinWith('contact')->joinWith('contact.address')->joinWith('siteAddress')->where([ 'tbl_quotes.subscriber_id' => $sub_id]);
            $modelQuery->andWhere(['<>','tbl_quotes.status', 0]);

            if($con_id != '')                       
            {
                $modelQuery->andWhere(['tbl_quotes.contact_id'=>$con_id]);
            }
            if($status)                     
            {
                $statusIDs = array_map('intval', explode(',', $status));

                $modelQuery->andWhere(['in','tbl_quotes.status',$statusIDs]);
            }
            if($type)                       
            {           
                $typeIDs = array_map('intval', explode(',', $type));        

                $modelQuery->andWhere(['in', 'tbl_quotes.type', $typeIDs]);
            }

            if($quoteId)
            {
                $modelQuery->andWhere(['tbl_quotes.quote_id'=>$quoteId]);
            }

            if( $from !='' && $to != ''){

                $modelQuery->andWhere('tbl_quotes.created_at>='.$from)->andWhere('tbl_quotes.created_at<='.$to);
            }

            /*---------- execute query below ---------*/

            $offset = ($page==0) ? 0 : (50*$page);

            $model = $modelQuery->orderBy('quote_id DESC')->limit(50)->offset($offset)->all();

                //echo "<pre>"; print_r($model); exit;

            if(count($model) > 0){

                $uArr = array(); 
                $cArr = array();

                foreach($model as $quotes){

                    $quoteId = $quotes->quote_id;
                    $date    = $quotes->created_at;
                    $type    = $quotes->type;
                    $note    = $quotes->note;
                    $price   = $quotes->price;
                    $status  = $quotes->status;
                    $description  = $quotes->description;
                    $contactId  = $quotes->contact_id;


                    $finalArr['quote_id']    = $quoteId;
                    $finalArr['description'] = $description;
                    $finalArr['status']      = $status;
                    $finalArr['date']        = $date;
                    $finalArr['note']        = $note;
                    $finalArr['type']        = $type;
                    $finalArr['price']       = $price;

                    if(isset($quotes->contact) && !empty($quotes->contact) ){                               

                        $addr = TblAddress::findOne($quotes->contact->address_id);
                            //$addr = $quotes->contact->address;
                            //echo "<pre>"; print_r($addr); exit;
                        $cArr['email']          = $quotes->contact->email;
                        $cArr['name']           = $quotes->contact->name;
                        $cArr['phone']          = $quotes->contact->phone;  
                        $cArr['image']          = ($quotes->contact->image != "")?$siteURL.'/'.$quotes->contact->image:""; 
                        $cArr['formatted_addr'] = $addr->formatted_address; 
                        $cArr['lat']            = $addr->lat;
                        $cArr['lng']            = $addr->lng;                               
                        $cArr['country']        = $addr->country_id;
                        $cArr['postal']         = $addr->postal_code;
                        $cArr['state']          = $addr->client_state;  
                        $cArr['street1']        = $addr->street1;
                        $cArr['street2']        = $addr->street2;
                        $cArr['suburb']         = $addr->suburb;
                        $cArr['s_checked']      = $addr->is_checked;
                        $cArr['contact_id']     = $contactId;       
                    }

                    if(isset($quotes->siteAddress) && !empty($quotes->siteAddress) ){   

                        $sname  = ($quotes->siteAddress->s_name != '')  ? $quotes->siteAddress->s_name  : $quotes->contact->name;
                        $semail = ($quotes->siteAddress->s_email != '') ? $quotes->siteAddress->s_email : $quotes->contact->email;
                        $sphone = ($quotes->siteAddress->s_phone != '') ? $quotes->siteAddress->s_phone : $quotes->contact->phone;

                        $finalArr['site']['country']        = $quotes->siteAddress->country_id;
                        $finalArr['site']['email']          = $semail;
                        $finalArr['site']['formatted_addr'] = $quotes->siteAddress->formatted_address;  
                        $finalArr['site']['lat']            = $quotes->siteAddress->lat;
                        $finalArr['site']['lng']            = $quotes->siteAddress->lng;
                        $finalArr['site']['name']           = $sname;   
                        $finalArr['site']['phone']          = $sphone;
                        $finalArr['site']['postal']         = $quotes->siteAddress->postal_code;
                        $finalArr['site']['state']          = $quotes->siteAddress->client_state;   
                        $finalArr['site']['street1']        = $quotes->siteAddress->street1;
                        $finalArr['site']['street2']        = $quotes->siteAddress->street2;
                        $finalArr['site']['suburb']         = $quotes->siteAddress->suburb; 
                        $finalArr['site']['s_checked']      = $quotes->siteAddress->is_checked; 

                    }

                    $finalArr['contact']  = $cArr;

                    $finalArr['counts']['jss']      = 10;
                    $finalArr['counts']['contacts'] = 15;
                    $finalArr['counts']['quotes']   = 8;
                    $finalArr['counts']['docs']     = 12;

                    $arr['quotes'][] = $finalArr;

                    } //foreach                      

                    $response = [
                        'success' => '1',
                        'message' => 'Contacts Details!',
                        'data' => $arr,
                    ];
                }else{

                    $arr['quotes'] = array();

                    $response = [
                        'success' => '0',
                        'message' => 'No Quotes for this user!',
                        'data' => $arr,
                    ];
                }

            }
            // If the email is not found make a response like this
            else{
              $response = [
                'success' => '0',
                'message' => 'Subscriber doesnot exist in database!',
                'data' => [],
            ];
        }   

    }
        //echo "<pre>"; print_r($response);exit;
    ob_start();
        //echo json_encode($response);   

    return $this->render('index',['response'=>$response,'contacts'=>$contacts]);
    } //actionIndex

    public function actionView(){

      $contacts = TblContacts::listSubscriberContacts();

      $quoteId = !empty($_GET['quote_id'])?$_GET['quote_id']:'';


      $sub_id = Yii::$app->user->id;

      $siteURL = Url::base(true);

      $modelQuery = TblQuotes::find()->joinWith('paintDefaults')->joinWith('brandPref')->joinWith('contact')->joinWith('contact.address')->joinWith('siteAddress')->where([ 'tbl_quotes.subscriber_id' => $sub_id]);

      if($quoteId)
      {
          $modelQuery->andWhere(['tbl_quotes.quote_id'=>$quoteId]);
      }
      else{

      }

      $tier_id = '';
      $uArr = []; 
      $cArr = [];
      $pdArr = [];
      $bpArr = [];

      $components = [];
      $undercoat = [];
      $topcoat = [];
      $sheens = [];
      $sheenProducts = [];
      $groups = [];
      $ifUpdate = 0;
      $roomTypes = [];
      $type_id = 1;
      $strengths = [];

      /*---------- execute query below ---------*/

      $quotes = $modelQuery->one();

		//echo "<pre>";print_r($quotes);exit();

      if(!is_null($quotes) ){

         $quoteId = $quotes->quote_id;
         $date    = $quotes->created_at;
         $type_id = $type    = $quotes->type;
         $note    = $quotes->note;
         $price   = $quotes->price;
         $status  = $quotes->status;
         $description  = $quotes->description;
         $contactId  = $quotes->contact_id;


         $finalArr['quote_id']    = $quoteId;
         $finalArr['description'] = $description;
         $finalArr['status']      = $status;
         $finalArr['date']        = $date;
         $finalArr['note']        = $note;
         $finalArr['type']        = $type;
         $finalArr['price']       = $price;

         if(isset($quotes->contact) && !empty($quotes->contact) ){

				//$addr = TblAddress::findOne($quotes->contact->address_id);
            $addr = $quotes->contact->address;
				//echo "<pre>"; print_r($addr); exit;
            $cArr['email']          = $quotes->contact->email;
            $cArr['name']           = $quotes->contact->name;
            $cArr['phone']          = $quotes->contact->phone;  
            $cArr['image']          = ($quotes->contact->image != "")?$siteURL.'/'.$quotes->contact->image:""; 
            $cArr['formatted_addr'] = $addr->formatted_address; 
            $cArr['lat']            = $addr->lat;
            $cArr['lng']            = $addr->lng;                               
            $cArr['country']        = $addr->country_id;
            $cArr['postal']         = $addr->postal_code;
            $cArr['state']          = $addr->client_state;  
            $cArr['street1']        = $addr->street1;
            $cArr['street2']        = $addr->street2;
            $cArr['suburb']         = $addr->suburb;
            $cArr['s_checked']      = $addr->is_checked;
            $cArr['contact_id']     = $contactId;
        }

        if(isset($quotes->brandPref) && !empty($quotes->brandPref) ){
            if(!empty($quotes->brandPref)){
               $ifUpdate = 1;
               $bpf = $quotes->brandPref;
               $bpfArr['id']             = $bpf->id;
               $bpfArr['name']           = $bpf->name;
               $bpfArr['tier_id']        = $bpf->tier_id;
               $bpfArr['brand_id']       = $bpf->brand_id;
               $bpfArr['coats']          = $bpf->coats;
               $bpfArr['prep_level']       = $bpf->prep_level;
               $bpfArr['apply_undercoat']       = $bpf->apply_undercoat;
               $bpfArr['color_consultant']      = $bpf->color_consultant;
               $bpArr=$bpfArr;
           }
       }

       if(isset($quotes->paintDefaults) && !empty($quotes->paintDefaults) ){
        if(!empty($quotes->paintDefaults)){
           $paintDefaults = $quotes->paintDefaults;
           foreach ($paintDefaults as $key => $pds) {
              $tier_id = $pdMArr['tier_id']        = $pds->tier_id;
              $pdMArr['id']           = $pds->id;
              $pdMArr['name']           = $pds->name;
              $pdMArr['comp_id'] 		  = $pds->comp_id; 
              $pdMArr['sheen_id']       = $pds->sheen_id;
              $pdMArr['topcoat']        = $pds->topcoat;                               
              $pdMArr['strength']       = $pds->strength;
              if($pds->color_id != 0 ){
                $pdMArr['color'] = [];
                $color = TblColors::find()->where(['color_id' => $pds->color_id])->one();  
                // print_r($color);die;              
                $pdMArr['color']['color_id'] = $color['color_id'];
                $pdMArr['color']['name'] = $color['name'];
                $pdMArr['color']['hex'] = $color['hex'];
                $pdMArr['color']['tag_id'] = $color['tag_id'];
              }else{
                $pdMArr['color'] = $pds->color_id;
              }
              $pdMArr['hasUnderCoat']   = $pds->hasUnderCoat;  
              $pdMArr['undercoat']      = $pds->undercoat;
              $pdMArr['custom_name']    = $pds->custom_name;
              $pdArr[$pds->comp_id] = $pdMArr;
          }
      }
  }

  if(isset($quotes->siteAddress) && !empty($quotes->siteAddress) ){   

    $sname  = ($quotes->siteAddress->s_name != '')  ? $quotes->siteAddress->s_name  : $quotes->contact->name;
    $semail = ($quotes->siteAddress->s_email != '') ? $quotes->siteAddress->s_email : $quotes->contact->email;
    $sphone = ($quotes->siteAddress->s_phone != '') ? $quotes->siteAddress->s_phone : $quotes->contact->phone;

    $finalArr['site']['country']        = $quotes->siteAddress->country_id;
    $finalArr['site']['email']          = $semail;
    $finalArr['site']['formatted_addr'] = $quotes->siteAddress->formatted_address;  
    $finalArr['site']['lat']            = $quotes->siteAddress->lat;
    $finalArr['site']['lng']            = $quotes->siteAddress->lng;
    $finalArr['site']['name']           = $sname;   
    $finalArr['site']['phone']          = $sphone;
    $finalArr['site']['postal']         = $quotes->siteAddress->postal_code;
    $finalArr['site']['state']          = $quotes->siteAddress->client_state;   
    $finalArr['site']['street1']        = $quotes->siteAddress->street1;
    $finalArr['site']['street2']        = $quotes->siteAddress->street2;
    $finalArr['site']['suburb']         = $quotes->siteAddress->suburb; 
    $finalArr['site']['s_checked']      = $quotes->siteAddress->is_checked; 

}

$finalArr['contact']  = $cArr;
$finalArr['paintDefaults']  = $pdArr;
$finalArr['brandPref']  = $bpArr;

$finalArr['counts']['jss']      = 10;
$finalArr['counts']['contacts'] = 15;
$finalArr['counts']['quotes']   = 8;
$finalArr['counts']['docs']     = 12;


if($tier_id!=''){
    $connection = Yii::$app->getDb();
    $command = $connection->createCommand("
        SELECT tbl_component_groups.group_id, tbl_component_groups.name as group_name,tbl_component_groups.tt_image, tbl_component_groups.tt_text,  tbl_component_groups.tt_url, tbl_sheen.sheen_id  as sheen_id, tbl_sheen.name, tbl_products.product_id as product_id, tbl_products.name as productName, tbl_tier_coats.top_coats FROM tbl_component_groups LEFT JOIN tbl_tier_coats ON tbl_component_groups.group_id = tbl_tier_coats.comp_group_id LEFT JOIN tbl_sheen ON tbl_tier_coats.sheen_id = tbl_sheen.sheen_id LEFT JOIN tbl_products ON tbl_tier_coats.product_id = tbl_products.product_id WHERE (tbl_component_groups.type_id IN ('".$type."', 3)) AND (tbl_component_groups.enabled=1) AND (tbl_tier_coats.tier_id ='".$tier_id."')");
    $components = $command->queryAll();

    // echo "<pre>";print_r($components);die;

    foreach ($components as $key => $comp) {
       $groups[$comp['group_id']]['group_id'] = $comp['group_id'];
       $groups[$comp['group_id']]['group_name'] = $comp['group_name'];
       $groups[$comp['group_id']]['tt_image'] = $comp['tt_image'];
       $groups[$comp['group_id']]['tt_text'] = $comp['tt_text'];
       $groups[$comp['group_id']]['tt_url'] = $comp['tt_url'];
       if($comp['sheen_id'] == 0){
          $undercoat[] = $comp;
      }
      else{
          $topcoat[$comp['group_id']][$comp['sheen_id']][] = $comp;
          $sheens[$comp['sheen_id']] = $comp['name'];
          $sheenProducts[$comp['sheen_id']][$comp['product_id']] = $comp['productName'];
      }
  }
}

$roomsModel = TblRoomTypes::find()->where('type_id IN ('.$type.',3)')->all();

$customRoom = [];
        // $i=1;
  if(!empty($roomsModel)){

    $customRoom = [];
        foreach($roomsModel as $_model){

          if($_model['room_id'] == 40 || $_model['room_id'] == 176){
            $customRoom['id']       = $_model['room_id'];
            $customRoom['name']     = $_model['name'];
            $customRoom['multiple'] = $_model['multiples'];
            if(!empty($_model['blue_image'])){
              $arr['blue_image'] = $siteURL.'/image/RoomImages/blue/'.$_model['blue_image'];
            }else{
              $arr['blue_image'] = $siteURL.'/image/RoomImages/blue/default.png';
            }
            //// White Icons /////
            if(!empty($_model['white_image'])){
              $arr['white_image'] = $siteURL.'/image/RoomImages/white/'.$_model['white_image'] ;
            }else{
              $arr['white_image'] = $siteURL.'/image/RoomImages/white/default.png';
            }
            // $fArr['rooms'][] = $arr;
            $roomTypes[] = $customRoom;
          }
          
        }
        

        // $i=1;

        foreach($roomsModel as $_model){
          
          if($_model['room_id'] != 40 && $_model['room_id'] != 176){
            
            $arr['id']       = $_model['room_id'];
            $arr['name']     = $_model['name'];
            $arr['multiple'] = $_model['multiples'];
            //// Blue Icons /////
            if(!empty($_model['blue_image'])){

              $arr['blue_image'] = $siteURL.'/image/RoomImages/blue/'.$_model['blue_image'];
            }else{
              $arr['blue_image'] = $siteURL.'/image/RoomImages/blue/default.png';
            }
            //// White Icons /////
            if(!empty($_model['white_image'])){
              $arr['white_image'] = $siteURL.'/image/RoomImages/white/'.$_model['white_image'] ;
            }else{
              $arr['white_image'] = $siteURL.'/image/RoomImages/white/default.png';
            }

            $roomTypes[] = $arr;
          }
          
        }
        // print_r($roomTypes);die;
      }

}else{
 $finalArr = array();
}

$brands = TblBrands::find()
->with(
 ['tblTiers' => function($q)  use ($type_id){
    $q->where('tbl_tiers.type_id IN ('.$type_id.',3)')->andWhere(['tbl_tiers.enabled'=>1]);
}]
)
->orderBy('brand_order_id ASC')
->where(['tbl_brands.enabled' => 1])
->all();
		///echo "<pre>"; print_r($finalArr); exit;

$company_id = Yii::$app->user->identity->company_id;
$prepLevels = TblPrepLevel::find()
->where(['company_id' => $company_id])
->all();

$colors = TblColors::find()->where('color_id != 0')->limit(50)->all();

$colorTags = TblColorTags::find()->where('tag_id != 1')->all();
$strengths = TblStrengths::find()->all();

$specialModel = TblSpecialItems::find()->where(['company_id' => $company_id])->all();

    if(count($specialModel)>0){
      //echo "<pre>"; print_r($specialModel); exit;
      $specialItems = [];
      foreach($specialModel as $_model){

        $arr['id']    = $_model->item_id;
        $arr['name']  = $_model->name;
        $arr['price'] = $_model->price;

          $specialItems[] = $arr;
      }
    }

		// echo "<pre>";print_r($finalArr);exit();

		//echo "<pre>";print_r($groups);print_r($undercoat);print_r($topcoat);print_r($sheens);print_r($sheenProducts);exit();

return $this->render('view',[
    'quotes'=>$finalArr,
    'contacts'=>$contacts,
    'brands'=>$brands,
    'colors'=>$colors,
    'components'=>$components,
    'undercoat'=>$undercoat,
    'topcoat'=>$topcoat,
    'sheens'=>$sheens,
    'sheenProducts'=>$sheenProducts,
    'groups'=>$groups,
    'ifUpdate'=>$ifUpdate,
    'siteURL'=>$siteURL,
    'roomTypes'=>$roomTypes,
    'colorTags'=>$colorTags,
    'strengths'=>$strengths,
    'specialItems'=>$specialItems,
    'prepLevels'=>$prepLevels
]);

}

public function actionGetallinvoices(){
    $userData = Yii::$app->user->identity;
    $company_id = $userData['company_id'];

    $contacts = TblContacts::listSubscriberContacts();
    // $company_id = !empty($_GET['company_id'])?$_GET['company_id']:'1';
    $contact_id = !empty($_GET['contact_id'])?$_GET['contact_id']:0;
    $siteURL = Url::base(true);
    $response = [];

    $page       = 0;
    $offset     = ($page == 0 || empty($page)) ? 0 : (20*$page);

    $connection = \Yii::$app->db;
    if(!empty($contact_id) && $contact_id > 0 ){
        $allPayments = 'SELECT * from tbl_payment_schedule WHERE qb_invoice_id <> "" AND quote_id IN (SELECT quote_id FROM tbl_quotes WHERE contact_id = "'.$contact_id.'" AND status <> 0 ORDER BY quote_id ASC )  LIMIT '.$offset.',20';
    }else{
        $allPayments = 'SELECT * from tbl_payment_schedule WHERE qb_invoice_id <> "" AND quote_id IN (SELECT quote_id FROM tbl_quotes WHERE subscriber_id IN(SELECT user_id FROM tbl_users WHERE company_id = "'.$company_id.'") AND status <> 0 ORDER BY quote_id ASC ) LIMIT '.$offset.',20';
    }

    $model11 = $connection->createCommand($allPayments);
    $paymentSchedule = $model11->queryAll();
    return $this->render('allinvoices',['paymentSchedule'=>$paymentSchedule,'company_id'=>$company_id]);

}


public function actionSubscriberorder(){

    $userData = Yii::$app->user->identity;
    $sub_id = $userData['user_id'];

    $company_id = $userData['company_id'];

    $page       = ($mydata['page'])?$mydata['page']:'';
    $offset     = ($page==0 || empty($page)) ? 0 : (20*$page);
    $filter     = (empty($mydata['filter'])) ? '' : $mydata['filter'];


    $connection = \Yii::$app->db;

    if( $filter!="" ){

        $q1 = 'SELECT * from tbl_paint_order WHERE quote_id LIKE "%'.$filter.'%" OR deliver_to_person_name LIKE "%'.$filter.'%" OR notes LIKE "%'.$filter.'%" AND quote_id IN (SELECT quote_id FROM tbl_quotes WHERE   subscriber_id = "'.$sub_id.'" ORDER BY quote_id ASC ) GROUP by paint_order_id ORDER BY created_at DESC LIMIT '.$offset.',20';

    }else{
        $q1 = 'SELECT * from tbl_paint_order WHERE quote_id IN (SELECT quote_id FROM tbl_quotes WHERE subscriber_id = "'.$sub_id.'" ORDER BY quote_id ASC ) GROUP by paint_order_id ORDER BY created_at DESC LIMIT '.$offset.',20';
    }



    $model11 = $connection->createCommand($q1);
    $paintOrderSub = $model11->queryAll();
    $allData = [];

    $paintOrderDetail = [];
    foreach ($paintOrderSub as $keySub => $valueSub) {

        $curPaintOrderDetail = [];

        $curPaintOrderDetail['paint_order_id'] = $valueSub['paint_order_id'];
        $curPaintOrderDetail['deliver_detail'] = ($valueSub['deliver_detail']=='' || $valueSub['deliver_detail']==null)?new \stdClass():json_decode($valueSub['deliver_detail']);
        $curPaintOrderDetail['notes'] = $valueSub['notes'];
        $curPaintOrderDetail['quote_type'] = $valueSub['quote_type'];
        $curPaintOrderDetail['pdf_url'] = $valueSub['pdf_url'];
        $curPaintOrderDetail['created_at'] = $valueSub['created_at'];
        $curPaintOrderDetail['quote_id'] = $valueSub['quote_id'];

        $order_id = $valueSub['paint_order_id'];
        $q2 = 'SELECT * from tbl_paint_order WHERE paint_order_id = "'.$order_id.'"';
        $model22 = $connection->createCommand($q2);
        $paintOrder = $model22->queryAll();


        $od = [];
        foreach($paintOrder as $key => $po ){
            $pod = [];
            $pod['product_id'] = $po['product_id'];
            $pod['stock_id'] = $po['stock_id'];
            $pod['quantity'] = $po['quantity'];
            $pod['brand'] = $po['brand'];
            $pod['product'] = $po['product'];
            $pod['sheen'] = $po['sheen'];
            $pod['color'] = $po['colour'];
            $pod['size'] = $po['size'];
            $pod['isUndercoat'] = $po['isUndercoat'];

            if(empty($po['liters']) || $po['liters'] == null){
                $pod['litres'] = 0;
            }else{
                $pod['litres'] = $po['liters'];
            }
            $od[] = $pod;
        }
        $curPaintOrderDetail['paint_order_detail'] = $od;
        $paintOrderDetail[] = $curPaintOrderDetail;
    }

    return $this->render('subscriberorders',['paintOrder'=>$paintOrderDetail,'company_id'=>$company_id]);

}

public function actionSavePaintDefaults(){
		//echo "<pre>";print_r($_POST);exit();
  $brandPref = new TblBrandPref();
  if($_POST['ifUpdate']!=0){
     $brandPref = TblBrandPref::findOne($_POST['brandPrefId']);
     TblPaintDefaults::deleteAll('quote_id = '.$_POST['quote_id']);
 }
 else{
     $brandPref = new TblBrandPref();
 }
 $brandPref->name = '';
 $brandPref->device_id = '';
 $brandPref->unique_id = '';
 $brandPref->quote_id = $_POST['quote_id'];
 $brandPref->brand_id = $_POST['defaultBrand'];
 $brandPref->tier_id = $_POST['defaultTier'];
 $brandPref->coats = $_POST['defaultCoat'];
 $brandPref->prep_level = $_POST['defaultPrepLevel'];

 $brandPref->apply_undercoat = (array_key_exists('defaultUnderCoat',$_POST))?$_POST['defaultUnderCoat']:0;
 $brandPref->color_consultant = (array_key_exists('defaultColorConsultant',$_POST))?$_POST['defaultColorConsultant']:0;
 $brandPref->flag = 1;
 $brandPref->created_at = strtotime(gmdate('Y-m-d H:i:s'));
 $brandPref->updated_at = strtotime(gmdate('Y-m-d H:i:s'));

 if($brandPref->validate()){
     $brandPref->save();
     $message='Data saved successfully.';
     $success=1;
     if(array_key_exists('component', $_POST)){
        foreach ($_POST['component'] as $key => $component) {
           $paintDefaults = new TblPaintDefaults();
           $paintDefaults->name = '';
           $paintDefaults->custom_name = '';
           $paintDefaults->device_id = '';
           $paintDefaults->unique_id = '';
           $paintDefaults->color_id = 1;

           $paintDefaults->quote_id = $_POST['quote_id'];
           $paintDefaults->tier_id = $_POST['defaultTier'];
           $paintDefaults->comp_id = $component['compId'];
           $paintDefaults->sheen_id = $component['sheen'];
           $paintDefaults->topcoat = $component['product'];
           $paintDefaults->strength = $component['strength'];
					//$paintDefaults->color_id = $component['color'];
           $paintDefaults->hasUnderCoat = (array_key_exists('underChecks',$_POST))?$_POST['underChecks']:0;
           $paintDefaults->undercoat = $component['undercoat'];
           $paintDefaults->created_at = strtotime(gmdate('Y-m-d H:i:s'));
           $paintDefaults->updated_at = strtotime(gmdate('Y-m-d H:i:s'));
           $paintDefaults->flag = 1;
           if($paintDefaults->validate()){
              $success=1;
              $paintDefaults->save();
          }
          else{
              $success=0;
              $message='Unable to save data.';
						//print_r($paintDefaults);exit();
          }
      }
  }
}
else{
 $success=0;
 $message='Unable to save data.';
			//print_r($brandPref);exit();
}





echo json_encode(['success'=>$success,'message'=>$message]);

}

public function actionGetPaintComponents(){
  $tier_id = $_GET['tier_id'];
  $type_id = $_GET['type_id'];
  $connection = Yii::$app->getDb();
  $command = $connection->createCommand("
      SELECT tbl_component_groups.group_id, tbl_component_groups.name as group_name,tbl_component_groups.tt_image, tbl_component_groups.tt_text,  tbl_component_groups.tt_url, tbl_sheen.sheen_id  as sheen_id, tbl_sheen.name, tbl_products.product_id as product_id, tbl_products.name as productName, tbl_tier_coats.top_coats FROM tbl_component_groups LEFT JOIN tbl_tier_coats ON tbl_component_groups.group_id = tbl_tier_coats.comp_group_id LEFT JOIN tbl_sheen ON tbl_tier_coats.sheen_id = tbl_sheen.sheen_id LEFT JOIN tbl_products ON tbl_tier_coats.product_id = tbl_products.product_id WHERE (tbl_component_groups.type_id IN ('".$type_id."', 3)) AND (tbl_component_groups.enabled=1) AND (tbl_tier_coats.tier_id ='".$tier_id."')");

  $components = $command->queryAll();
		//echo '<pre>';print_r($components);exit();

  $undercoat = [];
  $topcoat = [];
  $sheens = [];
  $sheenProducts = [];
  $groups = [];
  foreach ($components as $key => $comp) {
     $groups[$comp['group_id']]['group_id'] = $comp['group_id'];
     $groups[$comp['group_id']]['group_name'] = $comp['group_name'];
     $groups[$comp['group_id']]['tt_image'] = $comp['tt_image'];
     $groups[$comp['group_id']]['tt_text'] = $comp['tt_text'];
     $groups[$comp['group_id']]['tt_url'] = $comp['tt_url'];
     if($comp['sheen_id'] == 0){
        $undercoat[] = $comp;
    }
    else{
        $topcoat[$comp['group_id']][$comp['sheen_id']][] = $comp;
        $sheens[$comp['sheen_id']] = $comp['name'];
        $sheenProducts[$comp['sheen_id']][$comp['product_id']] = $comp['productName'];
    }
}

		//echo '<pre>';print_r($topcoat);exit();
echo(json_encode(array('success'=>1,'components'=>$components,'undercoat'=>$undercoat,'topcoat'=>$topcoat,'sheens'=>$sheens,'sheenProducts'=>$sheenProducts,'groups'=>$groups)));
exit;

}


public function actionUploadCsv(){
  $connection = \Yii::$app->db;


    

if(isset($_POST['importSubmit'])){
    
    // Allowed mime types
    $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
    
    // Validate whether selected file is a CSV file
    if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'], $csvMimes)){
        
        // If the file is uploaded
        if(is_uploaded_file($_FILES['file']['tmp_name'])){
            
            // Open uploaded CSV file with read-only mode
            $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
            
            // Skip the first line
            fgetcsv($csvFile);
            
            // Parse data from CSV file line by line
            while(($line = fgetcsv($csvFile)) !== FALSE){
                // Get row data
                $name   = $line[0];
                $email  = $line[1];
                $phone  = $line[2];
                $status = $line[3];
                
                // Check whether member already exists in the database with the same email
                $prevQuery = "SELECT id FROM members WHERE email = '".$line[1]."'";
                $prevResult1 = $connection->createCommand($prevQuery);
                $prevResult = $prevResult1->query();
                // $prevResult = $db->query($prevQuery);
                
                if(count($prevResult) > 0){
                    // Update member data in the database
                    $query = "UPDATE members SET name = '".$name."', phone = '".$phone."', status = '".$status."', modified = NOW() WHERE email = '".$email."'";
                }else{
                    // Insert member data in the database
                    $query = "INSERT INTO members (name, email, phone, created, modified, status) VALUES ('".$name."', '".$email."', '".$phone."', NOW(), NOW(), '".$status."')";
                }
                $query1 = $connection->createCommand($query);
                $query = $query1->query();
            }
            
            // Close opened CSV file
            fclose($csvFile);
            
            $qstring = '?status=succ';
        }else{
            $qstring = '?status=err';
        }
    }else{
        $qstring = '?status=invalid_file';
    }
    // Redirect to the listing page
header("Location: upload-csv".$qstring);
}


  return $this->render('uploadcsv');exit();

    $userData = Yii::$app->user->identity;
    $sub_id = $userData['user_id'];

    $company_id = $userData['company_id'];

    $page       = ($mydata['page'])?$mydata['page']:'';
    $offset     = ($page==0 || empty($page)) ? 0 : (20*$page);
    $filter     = (empty($mydata['filter'])) ? '' : $mydata['filter'];


    $connection = \Yii::$app->db;

    if( $filter!="" ){

        $q1 = 'SELECT * from tbl_paint_order WHERE quote_id LIKE "%'.$filter.'%" OR deliver_to_person_name LIKE "%'.$filter.'%" OR notes LIKE "%'.$filter.'%" AND quote_id IN (SELECT quote_id FROM tbl_quotes WHERE   subscriber_id = "'.$sub_id.'" ORDER BY quote_id ASC ) GROUP by paint_order_id ORDER BY created_at DESC LIMIT '.$offset.',20';

    }else{
        $q1 = 'SELECT * from tbl_paint_order WHERE quote_id IN (SELECT quote_id FROM tbl_quotes WHERE subscriber_id = "'.$sub_id.'" ORDER BY quote_id ASC ) GROUP by paint_order_id ORDER BY created_at DESC LIMIT '.$offset.',20';
    }



    $model11 = $connection->createCommand($q1);
    $paintOrderSub = $model11->queryAll();
    $allData = [];

    $paintOrderDetail = [];
    foreach ($paintOrderSub as $keySub => $valueSub) {

        $curPaintOrderDetail = [];

        $curPaintOrderDetail['paint_order_id'] = $valueSub['paint_order_id'];
        $curPaintOrderDetail['deliver_detail'] = ($valueSub['deliver_detail']=='' || $valueSub['deliver_detail']==null)?new \stdClass():json_decode($valueSub['deliver_detail']);
        $curPaintOrderDetail['notes'] = $valueSub['notes'];
        $curPaintOrderDetail['quote_type'] = $valueSub['quote_type'];
        $curPaintOrderDetail['pdf_url'] = $valueSub['pdf_url'];
        $curPaintOrderDetail['created_at'] = $valueSub['created_at'];
        $curPaintOrderDetail['quote_id'] = $valueSub['quote_id'];

        $order_id = $valueSub['paint_order_id'];
        $q2 = 'SELECT * from tbl_paint_order WHERE paint_order_id = "'.$order_id.'"';
        $model22 = $connection->createCommand($q2);
        $paintOrder = $model22->queryAll();


        $od = [];
        foreach($paintOrder as $key => $po ){
            $pod = [];
            $pod['product_id'] = $po['product_id'];
            $pod['stock_id'] = $po['stock_id'];
            $pod['quantity'] = $po['quantity'];
            $pod['brand'] = $po['brand'];
            $pod['product'] = $po['product'];
            $pod['sheen'] = $po['sheen'];
            $pod['color'] = $po['colour'];
            $pod['size'] = $po['size'];
            $pod['isUndercoat'] = $po['isUndercoat'];

            if(empty($po['liters']) || $po['liters'] == null){
                $pod['litres'] = 0;
            }else{
                $pod['litres'] = $po['liters'];
            }
            $od[] = $pod;
        }
        $curPaintOrderDetail['paint_order_detail'] = $od;
        $paintOrderDetail[] = $curPaintOrderDetail;
    }

    return $this->render('subscriberorders',['paintOrder'=>$paintOrderDetail,'company_id'=>$company_id]);

}


public function actionGetRoomComponents(){
   $connection = Yii::$app->getDb();
   $command = $connection->createCommand("
       SELECT tct.comp_type_id, tct.comp_id, tct.name as component_type_name, tct.work_rate, tct.spread_ratio, tct.is_default, tct.thickness,tct.excl_area, tc.name, tc.tt_image, tc.tt_text, tc.tt_url, tc.price_method_id, tc.calc_method_id, tcg.enabled, tcg.type_id FROM tbl_components AS tc, tbl_component_groups AS tcg, tbl_component_type AS tct WHERE tcg.type_id = 1 AND tcg.group_id=tc.group_id AND tct.comp_id=tc.comp_id AND tcg.enabled=1 ORDER BY tct.comp_type_id");
   $roomCompList = $command->queryAll();

		//echo("<pre>");print_r($components);exit();
   $result = [];

   foreach ($roomCompList as $key => $comp) {
      $roomComp[$comp['comp_id']] = $comp;
      $roomCompDetail[$comp['comp_id']][] = $comp;
  }

  echo(json_encode(array('success'=>1,'roomCompList'=>$roomCompList,'roomComp'=>$roomComp,'roomCompDetail'=>$roomCompDetail)));
  exit;    	
}


    /**
     * Finds the TblContacts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TblContacts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TblContacts::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
