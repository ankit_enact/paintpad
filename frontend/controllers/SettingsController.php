<?php

namespace frontend\controllers;

use app\models\TblCommunications;
use app\models\TblCommunicationsSearch;
use app\models\TblCompanyEmailSettings;
use app\models\TblCompanyDocuments;
use app\models\TblAppointments;
use app\models\TblUsersDetails;
use app\models\TblUsers;
use app\models\TblContacts;
use app\models\TblQuotes;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * CommunicationsController implements the CRUD actions for TblCommunications model.
 */
class SettingsController extends Controller {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * Lists all TblCommunications models.
	 * @return mixed
	 */
	public function actionIndex() {
		//echo "<pre>";
        // $model = TblContacts::find()->where(['subscriber_id' => Yii::$app->user->id])->orderBy('name','ASC')->all();
        // // $model = TblContacts::find()->where(['subscriber_id' => Yii::$app->user->id])->orderBy('name','ASC')->limit(50)->offset(0)->all();
        // $contacts = TblContacts::listSubscriberContacts();
        // $modelAppointment = new TblAppointments();
        // $dataContact = TblContacts::find()->select('name,contact_id,subscriber_id')->all();
        // echo count($contacts);
        // exit();
        return $this->render('index');
        // return $this->render('index',
        //     [
        //         'model'=>$model,
        //         'contacts'=>$contacts,
        //         'modelAppointment'=>$modelAppointment,
        //         'dataContact'=>$dataContact,

        //     ]);
	}

	/**
	 * Displays a single TblCommunications model.
	 * @param integer $id
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView($id) {
		$headers = apache_request_headers();
		// echo "<pre>"; print_r($id); exit;

		(isset($headers) && isset($headers['Quoteid']) && $headers['Quoteid'] != '') ? $head = 1 : $head = 0;

		// echo "<pre>"; print_r($this->findModel($id)); exit;
		return $this->renderPartial('view', [
			'model' => $this->findModel($id),
			'head' => $head,
		]);
	}


	/**
	 *
	 * s a new TblCommunications model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		

		$quoteId = '';
		$contactId = '';

		$r_emails = '';
		$cc_emails = '';
		$bcc_emails = '';
		$to = '';
		$subject = '';
		$pdf = '';
		$type = 0;

		$iArr = array();
		$emailArr = array();

		$rrEmails = array();
		$crrEmails = array();
		$bcrrEmails = array();

		$headers = apache_request_headers();


		// for quote mail
		// $dummy = '{"Accept":"text\/html,application\/xhtml+xml,application\/xml;q=0.9,*\/*;q=0.8","Accept-Encoding":"gzip, deflate","Accept-Language":"en-us","Attachment":"http:\/\/enacteservices.net\/paintpad\/quotepdf\/quote1722_1583230805.pdf","Attachment2":"http:\/\/enacteservices.net\/paintpad\/quotepdf\/progresspaymentQuote1722_1583230805.pdf","Companyid":"1","Connection":"keep-alive","Host":"enacteservices.net","Quoteid":"1722","Subject":"P1722 Professional Painting Quote - Sahibzada Ajit Singh Nagar","Subscriberid":"1","To":"gtrdf@gun.gun,ankit.enacteservices@gmail.com,ankitsukhija@enacteservices.com","Type":"1","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\/5.0 (Macintosh; Intel Mac OS X 10_15) AppleWebKit\/605.1.15 (KHTML, like Gecko)"}';

		// $headers = json_decode($dummy);
		// $headers = (array) $headers;

		// dummy data for test

		// $data = json_encode($headers);
		// $url = $_SERVER['DOCUMENT_ROOT'].'/paintpad/responseCheck.php';
		// file_put_contents($url,$data);
		// echo "<pre>";
// print_r($_FILES);
// print_r($_POST);
// die;
		if(isset($_POST['comm_id_edit']) && !empty($_POST['comm_id_edit'])){
			$commData = TblCommunications::findOne($_POST['comm_id_edit']);

			$session = Yii::$app->session;

			$session->set('sub_id', $commData['subscriber_id']);
			$session->set('quote_id', $commData['quote_id']);
			$session->set('contact_id', $commData['contact_id']);
			$sub_id_comp = $commData['subscriber_id'];


		}

		if (isset($headers) && isset($headers['Quoteid']) && $headers['Quoteid'] != '' && isset($headers['Subscriberid']) && $headers['Subscriberid'] != '' && $headers['Companyid'] != '') {

			

			$sub_id_comp = $headers['Subscriberid'];
			$quoteId = $headers['Quoteid'];
			$id = $headers['Subscriberid'];

			$to = isset($headers['To']) ? $headers['To'] : '';
			$subject = isset($headers['Subject']) ? $headers['Subject'] : '';			
			$pdf = isset($headers['Attachment']) ? $headers['Attachment'] : '';

			if(isset($headers['Attachment2']) && !empty($headers['Attachment2'])){
				$pdf =  $pdf.','.$headers['Attachment2'];
			}

			$type = isset($headers['Type']) ? $headers['Type'] : 0;
			$company_id = isset($headers['Companyid']) ? $headers['Companyid'] : 0;
			$contactId = isset($headers['Contactid']) ? $headers['Contactid'] : 0;

			$pdf = stripcslashes($pdf);

			$session = Yii::$app->session;

			$session->set('sub_id', $id);
			$session->set('quote_id', $quoteId);
			$session->set('head', '1');
			$session->set('pdf', $pdf);
			$session->set('type', $type);
			$session->set('company_id', $company_id);
			$session->set('contact_id', $contactId);

			$head = 1;

		} else {

			$head = 0;

			$session = Yii::$app->session;
			$quote_id = $session->get('quote_id');
			$company_id = $session->get('company_id');
			$quoteId = $quote_id;
			$id = Yii::$app->user->getId();

		}

		if($company_id){
			$company_id = $company_id;
		}else{
			$userDet = TblUsers::find()->select('company_id')->where(['user_id'=>$sub_id_comp])->one();
			$company_id = $userDet['company_id'];
		}

		if ($quoteId != 0 && ($modelQuote = TblQuotes::findOne($quoteId)) !== null) {
			$contactId = $modelQuote->contact_id;
			$session->set('contact_id', $contactId);
		} else {
			$contactId = $session->get('contact_id');
		}

		$model = new TblCommunications();

		$emails = TblContacts::find()->select('subscriber_id,email')->where(['subscriber_id' => $id])->all();

		if (count($emails) > 0) {

			foreach ($emails as $_emails) {

				$emailArr[] = strtolower($_emails->email);
			} //foreach

		}

		if ($model->load(Yii::$app->request->post())) {

			$modell = Yii::$app->request->post();
			if (isset($modell['TblCommunications']['newsletter_id']) && count($modell['TblCommunications']['newsletter_id']) > 0) {
				$newsletter_id = $modell['TblCommunications']['newsletter_id'];

			}else{
				$newsletter_id = '';
			}

			if (isset($modell['TblCommunications']['receiver_email']) && count($modell['TblCommunications']['receiver_email']) > 0) {
				$r_emails = $modell['TblCommunications']['receiver_email'];

				$searchString1 = ',';
				if (strpos($r_emails, $searchString1) !== false) {

					$recEmail = explode(',', $modell['TblCommunications']['receiver_email']);

					foreach ($recEmail as $_remails) {
						$rrEmails[] = strtolower($_remails);
					} //foreach
				} else {

					$rrEmails = strtolower($modell['TblCommunications']['receiver_email']);
				}

			}

			//for CC mail
			if (isset($modell['TblCommunications']['cc_email']) && count($modell['TblCommunications']['cc_email']) > 0 && $modell['TblCommunications']['cc_email'] != '') {
				$cc_emails = $modell['TblCommunications']['cc_email'];

				$searchString2 = ',';
				if (strpos($cc_emails, $searchString2) !== false) {

					$cEmail = explode(',', $modell['TblCommunications']['cc_email']);

					foreach ($cEmail as $_cEmail) {
						$crrEmails[] = strtolower($_cEmail);
					} //foreach
				} else {

					$crrEmails = strtolower($modell['TblCommunications']['cc_email']);
				}

			}

			//for BCC mail
			if (isset($modell['TblCommunications']['bcc_email']) && count($modell['TblCommunications']['bcc_email']) > 0 && $modell['TblCommunications']['bcc_email'] != '') {
				$bcc_emails = $modell['TblCommunications']['bcc_email'];

				$searchString3 = ',';
				if (strpos($bcc_emails, $searchString3) !== false) {

					$bcEmail = explode(',', $modell['TblCommunications']['bcc_email']);

					foreach ($bcEmail as $_bcEmail) {
						$bcrrEmails[] = strtolower($_bcEmail);
					} //foreach
				} else {
					$bcrrEmails = strtolower($modell['TblCommunications']['bcc_email']);
				}
			}

			$siteURL = Url::base(true) . '/quotepdf';

			$model->attachment = UploadedFile::getInstances($model, 'attachment');

			$files = [];
			$filesWithRealName = [];

			if ($model->attachment) {

				$pdf = ($session->get('pdf')) ? $session->get('pdf') : '';
				
				if (isset($modell['TblCommunications']['indexx']) && count($modell['TblCommunications']['indexx']) > 0) {
					$iArr = $modell['TblCommunications']['indexx'];
				}

				$cc = 0;
				foreach ($model->attachment as $attachment) {

					if (in_array($cc, $iArr)) {
					} else {

						$path = 'uploads/' . $attachment->baseName . '.' . $attachment->extension;
						$count = 0;
						// {
							while (file_exists($path)) {

								$time = time();
								$imgNm = $time . '_' . $count . '.' . $attachment->extension;
								$path = 'uploads/' . $imgNm;
								$count++;
							}
						// }
						$attachment->saveAs($path);

						if($this->checkJpgExtension($imgNm) == 'jpg' || $this->checkJpgExtension($imgNm) == 'jpeg'){
							$correctOrientation = $this->correctImageOrientation($_SERVER['DOCUMENT_ROOT'].'/paintpad/uploads/'.$imgNm);
						}

						$pathForSend = Url::base(true).'/'. $path;
						$files[] = $pathForSend;
						$filesWithRealName[] = $attachment->baseName . '.' . $attachment->extension;
						
					}
					$cc++;
				}


					// echo "<pre>";print_r($_POST['attachment_old']);die;
				if(isset($_POST['attachment_old']) && !empty($_POST['attachment_old'])){
					$filesWithRealName = array_merge($filesWithRealName,$_POST['attachment_old']);

					foreach ($_POST['attachment_old'] as $file) {
						$oldAttachments = $this->allocateFilePath($file,$quoteId);		
						$files[] = $oldAttachments;
					}
					
				}

				$model->attachment = '';
				if ($filesWithRealName) {
					$model->attachment = implode(',', $filesWithRealName);

				}

				$value = Yii::$app->mailer->compose()
					->setFrom(['parshantenact@gmail.com' => 'enact'])
					->setTo($rrEmails)
					->setCc($crrEmails)
					->setBcc($bcrrEmails)
					->setSubject($model->subject)
					->setHtmlBody($model->body);

				$tblCompanyDocuments = TblCompanyDocuments::find()->select('file_path_name')->where( ["company_id" => $company_id,"always_link" => 1 ] )->all();
				if ($tblCompanyDocuments) {
					foreach ($tblCompanyDocuments as $docComp) {
						if ($docComp->file_path_name) {
							$docPath = Url::base(true) . '/uploads/' . $docComp->file_path_name;
							$files[] = $docPath;
						}
					}
				}

				foreach ($files as $key => $file) {
					$value->attach($file);
				}
				$value->send();
			} else {
				$pdf = ($session->get('pdf')) ? $session->get('pdf') : '';			

				$model->attachment = '';

				$value = Yii::$app->mailer->compose()
					->setFrom(['parshantenact@gmail.com' => 'enact'])
					->setTo($rrEmails)
					->setCc($crrEmails)
					->setBcc($bcrrEmails)
					->setSubject($model->subject)
					->setHtmlBody($model->body);

				if(isset($_POST['attachment_old']) && !empty($_POST['attachment_old'])){
					$oldAttachments = array();
					$file = '';
					// echo "<pre>";print_r($_POST['attachment_old']);die;
					foreach ($_POST['attachment_old'] as $file) {
						$attachment[] = $file;
						$oldAttachments = $this->allocateFilePath($file,$quoteId);		
						$files[] = $oldAttachments;
						// $value->attach($oldAttachments);
						// echo $oldAttachments;die;
					}
					if ($attachment) {
						$model->attachment = implode(',', $attachment);
					}
				}

				$tblCompanyDocuments = TblCompanyDocuments::find()->select('file_path_name')->where( ["company_id" => $company_id,"always_link" => 1 ] )->all();
				if ($tblCompanyDocuments) {
					foreach ($tblCompanyDocuments as $docComp) {
						if ($docComp->file_path_name) {
							$docPath = Url::base(true) . '/uploads/' . $docComp->file_path_name;
							$files[] = $docPath;
						}
					}
				}

				foreach ($files as $key => $file) {
					$value->attach($file);
				}

				$value->send();
			}

			$session = Yii::$app->session;

			$quoteId = ($session->get('quote_id')) ? $session->get('quote_id') : $quoteId;
			$id = ($session->get('sub_id')) ? $session->get('sub_id') : $id;
			$head = ($session->get('head')) ? $session->get('head') : 0;

			date_default_timezone_set("UTC");
			$milliseconds = strtotime(gmdate("Y-m-d H:i:s"));

			if (count($rrEmails) > 1) {
				$rrEmails = implode(', ', $rrEmails);
			}


			$model->contact_id = $contactId;
			$model->subscriber_id = $id;
			$model->quote_id = $quoteId;
			$model->receiver_email = $rrEmails;
			$model->cc_email = $cc_emails;
			$model->bcc_email = $bcc_emails;
			$model->newsletter_id = $newsletter_id;

			$model->date = $milliseconds;
			$model->created_at = $milliseconds;
			$model->updated_at = $milliseconds;

			$model->save();

			if ($session->get('type') == 1) {
				$quote = TblQuotes::find()->where(["quote_id" => $quoteId])->one();
				if ($quote) {
					if ($quote->status != 7) {
						$quote->status = 7;
						if ($quote->validate()) {
							$quote->save();
						}
					}
				}
			}

				return $this->redirect(['mailsent']);
		}

		$jsspdfs = [];
		$companyEmailSettings = TblCompanyEmailSettings::find()->where(["tbl_company_email_settings.company_id" => $company_id, "type_id" => $type])->joinWith('tblEmailSettingsDocs.companyDocuments')->one();

		if ($companyEmailSettings) {

			$bcc_to = strtolower($companyEmailSettings->bcc_to);
			$subject = $companyEmailSettings->quote_email_subject;
			if ($companyEmailSettings->tblEmailSettingsDocs) {
				$emailSettingsDocs = $companyEmailSettings->tblEmailSettingsDocs;

				foreach ($emailSettingsDocs as $esd => $esv) {
					if ($esv->companyDocuments) {
						// echo "<pre>";print_r($esv);die;
						$companyDocuments = $esv->companyDocuments;
						// $jsspdfs[] = $companyDocuments->file_name;
						$jsspdfs[] = $companyDocuments->file_path_name;
					}
				}
			}
		}
		// echo "<pre>"; print_r($jsspdfs);die;

		$companyDocuments = TblCompanyDocuments::find()->where(["company_id" => $company_id])->all();

		foreach ($companyDocuments as $cdk => $cdv) {
					if ($cdv) {
						$docsAll[] = $cdv;
					}
				}

		return $this->renderAjax('create', [
			'model' => $model, 'bcc_to' => $bcc_to, 'emails' => $emailArr, 'head' => $head, 'to' => $to, 'subject' => $subject, 'pdf' => $pdf, 'type' => $type, 'jsspdfs' => $jsspdfs,'docsAll'=>$docsAll
		]);

	}

	public function actionEdit($cid='') {
		if (Yii::$app->request->post()) {

		}

		$quoteId = '';
		$contactId = '';

		$r_emails = '';
		$cc_emails = '';
		$bcc_emails = '';
		$to = '';
		$subject = '';
		$pdf = '';
		$type = 0;

		$iArr = array();
		$emailArr = array();

		$rrEmails = array();
		$crrEmails = array();
		$bcrrEmails = array();

		$headers = apache_request_headers();

		if (isset($headers) && isset($headers['Quoteid']) && $headers['Quoteid'] != '' && isset($headers['Subscriberid']) && $headers['Subscriberid'] != '' && $headers['Companyid'] != '') {

			echo "<pre>"; print_r($headers); exit;

			$quoteId = $headers['Quoteid'];
			$id = $headers['Subscriberid'];

			$to = isset($headers['To']) ? $headers['To'] : '';
			$subject = isset($headers['Subject']) ? $headers['Subject'] : '';


			$pdf = isset($headers['Attachment']) ? $headers['Attachment'] : '';
			if(isset($headers['Attachment2']) && !empty($headers['Attachment2'])){
				$pdf =  $pdf.','.$headers['Attachment2'];
			}
			$type = isset($headers['Type']) ? $headers['Type'] : 0;
			$company_id = isset($headers['Companyid']) ? $headers['Companyid'] : 0;
			$contactId = isset($headers['Contactid']) ? $headers['Contactid'] : 0;


			$pdf = stripcslashes($headers['Attachment']);

			$session = Yii::$app->session;

			$session->set('sub_id', $id);
			$session->set('quote_id', $quoteId);
			$session->set('head', '1');
			$session->set('pdf', $pdf);
			$session->set('type', $type);
			$session->set('company_id', $company_id);
			$session->set('contact_id', $contactId);

			$head = 1;

		} else {

			// echo "<pre>";


			$head = 0;

			$session = Yii::$app->session;
			$quote_id = $session->get('quote_id');
			$company_id = $session->get('company_id');
			$quoteId = $quote_id;

			$id = Yii::$app->user->getId();

		}

		if ($quoteId != 0 && ($modelQuote = TblQuotes::findOne($quoteId)) !== null) {
			$contactId = $modelQuote->contact_id;
			$session->set('contact_id', $contactId);
		} else {
			$contactId = $session->get('contact_id');
		}

		$model = new TblCommunications();

		$emails = TblContacts::find()->select('subscriber_id,email')->where(['subscriber_id' => $id])->all();

		
		if (count($emails) > 0) {

			foreach ($emails as $_emails) {

				$emailArr[] = $_emails->email;
			} //foreach

		}

		$jsspdfs = [];

		$data = TblCommunications::findOne($cid);


		if(!empty($data['receiver_email'])){
			$to = $data['receiver_email'];
		}

		if(!empty($data['newsletter_id'])){
			$newsletter_id = $data['newsletter_id'];
		}

		if(!empty($data['subject'])){
			$subject = $data['subject'];
		}

		if(!empty($data['attachment'])){
			$attachments = $data['attachment'];
		}

		if(!empty($data['body'])){
			$body = $data['body'];
		}

		if(!empty($data['cc_email'])){
			$cc_email = $data['cc_email'];
		}

		if(!empty($data['bcc_email'])){
			$bcc_to = $data['bcc_email'];
		}
		
		$userDet = TblUsers::find()->select('company_id')->where(["user_id" => $data['subscriber_id']])->one();
		$companyDocuments = TblCompanyDocuments::find()->where(["company_id" => $userDet['company_id']])->all();
		// $companyDocuments = TblCompanyDocuments::find()->where(["company_id" => 1])->all();
		foreach ($companyDocuments as $cdk => $cdv) {
			if ($cdv) {
				$docsAll[] = $cdv;
			}
		}



		return $this->renderAjax('create', [
			'model' => $model, 'bcc_to' => $bcc_to, 'cc_email' => $cc_email, 'emails' => $emailArr, 'head' => $head, 'to' => $to, 'subject' => $subject, 'pdf' => $pdf, 'type' => $type, 'jsspdfs' => $jsspdfs, 'attachments' => $attachments, 'body' => $body, 'docsAll' => $docsAll, 'newsletter_id' => $newsletter_id
		]);


	}

	public function actionEditable($id) {
		$headers = apache_request_headers();
		$model = TblCommunications::findOne($id);


		$pdfs = array();
		$jsspdfs = array();
		$bcc_to = $model->bcc_email;
		$to = $model->receiver_email;
		$subject = $model->subject;
		$type = 1;
		$head = 1;

		$emails = TblContacts::find()->select('subscriber_id,email')->where(['subscriber_id' => $model->subscriber_id])->all();

		

		if (count($emails) > 0) {
			foreach ($emails as $_emails) {
				$emailArr[] = strtolower($_emails->email);
			} //foreach
		}

		return $this->renderPartial('editMain', [
		// return $this->renderPartial('editMain', [
		'model' => $this->findModel($id),
		// 'head' => $head,
		'bcc_to' => $bcc_to, 'emails' => $emailArr, 'head' => $head, 'to' => $to, 'subject' => $subject, 'pdf' => $pdf, 'type' => $type, 'jsspdfs' => $jsspdfs,
		]);
	}

	/**
	 * Updates an existing TblCommunications model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate($id) {
		$model = $this->findModel($id);

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->comm_id]);
		}

		return $this->render('update', [
			'model' => $model,
		]);
	}

	/**
	 * Deletes an existing TblCommunications model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();

		return $this->redirect(['index']);
	}

	/**
	 * Finds the TblCommunications model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return TblCommunications the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = TblCommunications::findOne($id)) !== null) {
			// echo "<pre>"; print_r($model);die;
			return $model;
		}

		throw new NotFoundHttpException('The requested page does not exist.');
	}

	public function actionckeditorImageUpload() {
		$funcNum = $_REQUEST['CKEditorFuncNum'];

		$message = '';

		if ($_FILES['upload']) {

			if (($_FILES['upload'] == "none") OR (empty($_FILES['upload']['name']))) {
				$message = Yii::t('app', "Please Upload an image.");
			} else if ($_FILES['upload']["size"] == 0 OR $_FILES['upload']["size"] > 5 * 1024 * 1024) {
				$message = Yii::t('app', "The image should not exceed 5MB.");
			} else if (($_FILES['upload']["type"] != "image/jpg")
				AND ($_FILES['upload']["type"] != "image/jpeg")
				AND ($_FILES['upload']["type"] != "image/png")) {
				$message = Yii::t('app', "The image type should be JPG , JPEG Or PNG.");
			} else if (!is_uploaded_file($_FILES['upload']["tmp_name"])) {

				$message = Yii::t('app', "Upload Error, Please try again.");
			} else {
				//you need this (use yii\db\Expression;) for RAND() method
				$random = rand(0, 9876543210);

				$extension = pathinfo($_FILES['upload']['name'], PATHINFO_EXTENSION);

				//Rename the image here the way you want
				$name = date("m-d-Y-h-i-s", time()) . "-" . $random . '.' . $extension;

				// Here is the folder where you will save the images
				$folder = 'uploads/ckeditor_images/';

				$url = Yii::$app->urlManager->createAbsoluteUrl($folder . $name);

				move_uploaded_file($_FILES['upload']['tmp_name'], $folder . $name);

			}

			echo '<script type="text/javascript">window.parent.CKEDITOR.tools.callFunction("'
				. $funcNum . '", "' . $url . '", "' . $message . '" );</script>';

		}

	} //actionCkeditor_image_upload

	public function actionList() {

		$headers = apache_request_headers();

		//echo "<pre>"; print_r($headers); exit;

		if (isset($headers) && isset($headers['Qid']) && $headers['Qid'] != '') {

			//echo "<pre>"; print_r($headers); exit;

			$head = 1;

			$quoteId = $headers['Qid'];

			if ($quoteId != '') {

				$modelComm = TblCommunications::find()
					->joinWith('commQuote')
					->joinWith('commSubscriber')
					->where(['tbl_communications.quote_id' => $quoteId])
					->all();

				//echo "<pre>"; print_r($modelComm); exit;

				return $this->render('list', [
					'modelComm' => $modelComm, 'head' => $head,
				]);

			}

		} else {

			//echo 'here'; exit;

			$head = 0;

			$quoteId = 3;

			if ($quoteId != '') {

				$modelComm = TblCommunications::find()
					->joinWith('commQuote')
					->joinWith('commSubscriber')
					->where(['tbl_communications.quote_id' => $quoteId])
					->all();

				//echo "<pre>"; print_r($modelComm); exit;

				return $this->render('list', [
					'modelComm' => $modelComm, 'head' => $head,
				]);

			}

		}

	} //actionList

	public function actionMailsent() {
		return $this->redirect(['index']);
		// return $this->render('mailsent');
	}


    //////// Correct image Orientation (Rotation) //////////

    public function correctImageOrientation($filename) {
    	// print_r($filename);die;
      // if (function_exists('exif_read_data')) {
      // 	if(@exif_read_data($filename)){
     	
	     //    $exif = exif_read_data($filename);
	     //    if($exif && isset($exif['Orientation'])) {
	     //      $orientation = $exif['Orientation'];
	     //      if($orientation != 1){
	     //        $img = imagecreatefromjpeg($filename);
	     //        $deg = 0;
	     //        switch ($orientation) {
	     //          case 3:
	     //            $deg = 180;
	     //            break;
	     //          case 6:
	     //            $deg = 270;
	     //            break;
	     //          case 8:
	     //            $deg = 90;
	     //            break;
	     //        }
	     //        if ($deg) {
	     //          $img = imagerotate($img, $deg, 0);       
	     //        }
	     //        // then rewrite the rotated image back to the disk as $filename
	     //        return imagejpeg($img, $filename, 95);
	     //      } // if there is some rotation necessary
	     //    } // if have the exif orientation info
      //   }else{
      // 		return 1;
      // 	}

      // } // if function exists    
    }


    //////// End Correct image Orientation (Rotation) //////////


    //// Check File Extension //////

    public function checkJpgExtension($file) {
	 $extension = end(explode(".", $file));
	 return $extension ? $extension : false;
	}


    //// End Check File Extension //////
    


    //// Allocate path //////

    function allocateFilePath($file,$quoteId) {
	    $file_parts = pathinfo($file);
		if($file_parts['extension'] == 'pdf' || $file_parts['extension'] == 'PDF'){
			$forJss = "jss".$quoteId;
			$forQuote = "quote".$quoteId;
			$forProgresspaymentQuote = "progresspaymentQuote".$quoteId;
			$file1 = " ".$file;
			// $file = str_replace(' ','%20', $file);
			if(strpos($file1,$forJss)){
				$oldAttachments = 'quotepdf/'.$file;
			}elseif (strpos($file1,$forQuote)) {
				$oldAttachments = 'quotepdf/'.$file;
			}elseif (strpos($file1,$forProgresspaymentQuote)) {
				$oldAttachments = 'quotepdf/'.$file;
			}elseif (strpos($file1,"orderProduc")) {
				$oldAttachments = 'quotepdf/'.$file;
			}else{
				$oldAttachments = 'uploads/'.$file;
			}
		}elseif($file_parts['extension'] != ''){
			$oldAttachments = 'uploads/'.$file;
		}
		// echo Url::base(true).'/'.$oldAttachments;
		return Url::base(true).'/'.$oldAttachments;
	}


    //// End Check File Extension //////
    

}
