<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

</head>

<?php $this->beginBody() ?>

<!--wrapper-->
<div class="wrapper">
    <!-- login_page -->
    <div class="login_main">

        <div class="container">
            <?= $content ?>
        </div>

   </div>
    <!--login_page_close-->
    
</div>
<!--wrapper_close-->
<footer><p class="copy_right">Copyright (c) PaintPad 2018</p></footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
