<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <script type="text/javascript">var siteBaseUrl = '<?= Url::base(true); ?>'</script>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">

<div class="container">

  <div class="row">

    <div class="menu" >
          <i class="fa fa-times" aria-hidden="true"></i>             
          <ul class="listg">
              <li class="list-item">Ayran</li>
              <li class="list-item">Çay</li>
              <li class="list-item">Kahve</li>
          </ul>
      </div>
  
      <div class="mainClose">
        <span class="fa fa-bars"><img src="<?= Yii::$app->request->baseUrl ?>/image/side_menu_btn.png"></span>
      </div>
 
      <div class="text-center" style="width: 100%;min-height: 60px;margin-bottom: 10px; background:#fff;"><a href="<?php echo Yii::$app->request->baseUrl;?>/dashboard"><img src="<?= Yii::$app->request->baseUrl ?>/image/dasboard_logo.png"></a></div>
    
    </div>

  </div> <!-- container -->


        <?= $content ?>

    <div class="col-2 push-md-5 text-center logout">
      <!-- <a href="#" class=""><img src="image/logout.png"><span>Logout</span></a> -->



      <?= Html::a('<img src="'.Yii::$app->request->baseUrl.'/image/logout.png"><span>Logout</span>', ['site/logout'], ['data' => ['method' => 'post']]) ?></a>

    </div>
</div> <!-- wrap -->

<div class="footer">

  <div class="container">

    

    <div class="row">
      <footer class="col-md-12"><p class="copy_right">Copyright (c) PaintPad 2018</p></footer>
    </div>

  </div>
</div>



<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
