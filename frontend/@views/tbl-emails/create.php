<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TblEmails */

$this->title = 'Create Tbl Emails';
$this->params['breadcrumbs'][] = ['label' => 'Tbl Emails', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-emails-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model, 'emails' =>$emails
    ]) ?>

</div>
