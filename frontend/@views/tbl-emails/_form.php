<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\TblNewsletterTemplates;
use dosamigos\ckeditor\CKEditor;
use kartik\select2\Select2;


/* @var $this yii\web\View */
/* @var $model app\models\TblEmails */
/* @var $form yii\widgets\ActiveForm */

?>
<style type="text/css">
.tbl-emails-create h1 {
    display: none;
}
.cke_top {
    background: #00AEEF;
}
</style>
<div class="wrap">
 <div class="container">
    <div class="tbl-emails-form">

        <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>

        <!--<?= $form->field($model, 'receiver_name')->textInput(['maxlength' => true]) ?>

         <?= $form->field($model, 'receiver_email')->textInput(['maxlength' => true]) ?> -->

        <?php

            echo $form->field($model, 'receiver_email')->widget(Select2::classname(), [
                'name' => 'receiver_email',
                'data' => $emails,
                'options' => [
                    'placeholder' => 'Select Email ...',
                    'multiple' => true
                ],
            ]);

        ?>

        <?= $form->field($model, 'subject')->textInput(['maxlength' => true]) ?>

        <?php echo '<label class="control-label">Select Template</label>'; ?>

        <?= $form->field($model, 'newsletter_id')->dropDownList(
             ArrayHelper::map(TblNewsletterTemplates::find()->all(),'temp_id','temp_name'),
            ['prompt' => 'Select','class'=>'form-contol','onchange'=>'
              $.post( "'.Yii::$app->urlManager->createUrl('tbl-emails/template?id=').'"+$(this).val(), function( data ) {
                console.log("data");
                console.log(data);
              
                
                var editor = CKEDITOR.instances[ "tblemails-content" ];
                editor.setData(data);

              });'

            ])->label(false);

        ?>

        <?= $form->field($model, 'content')->widget(CKEditor::className(), [
            'options' => ['rows' => 6],
            'preset' => 'advanced',
            'clientOptions' => [
              'extraPlugins' => '',
              'height' => 500,
              //Here you give the action who will handle the image upload 
              'filebrowserUploadUrl' => Yii::$app->urlManager->createUrl('tbl-emails/ckeditor_image_upload'),
              'toolbarGroups' => [
                  ['name' => 'undo'],
                  ['name' => 'basicstyles', 'groups' => ['basicstyles', 'cleanup']],
                  ['name' => 'paragraph', 'groups' => ['list', 'indent', 'blocks', 'align', 'bidi' ]],
                  ['name' => 'styles'],
                  ['name' => 'links', 'groups' => ['links', 'insert']]
              ]
            ]

        ]) ?>

        <?= $form->field($model, 'attachment[]')->fileInput(['multiple' => true, 'maxlength' => true]) ?>

        <div class="form-group">
            <?= Html::submitButton('Send', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>