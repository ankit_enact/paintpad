<?php

use yii\helpers\Html;
use yii\grid\GridView;

use kartik\tabs\TabsX;
use yii\helpers\Url;
use yii\widgets\Pjax;

use yii\bootstrap\Tabs;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\db\ActiveRecord;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TblContactsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tbl Contacts';
$this->params['breadcrumbs'][] = $this->title;

?>

<style type="text/css">
  
.abcd img {
    max-width: 100%;
}
.abcd {
    width: 20%;
    float: left;
    border: 3px solid #eee;
    margin: 2px;
}
div#filediv {
    float: left;
    width: 25%;
    padding: 0 15px;
} 
p#img {
    font-size: 16px;
    text-align: center;
    cursor: pointer;
    background: #000;
    color: red;
    margin:0px !important;
}
.save-div {
    float: left;
    width: 100%;
    margin-top: 20px;
}
.save-div div#filediv {
    float: left;
    width: 100%;
    padding: 0;
}

.quote_left_fix{
  height: 0px;
  overflow: overlay;
}

</style>

<?php

$host_name = isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : $_SERVER['HTTP_HOST'];          
$siteURL = 'http://' . $host_name . '/' . Yii::$app->request->baseUrl . '/uploads';   

$cntct_li = '';

//get first user data
$user_name  = '';
$user_email = '';
$user_phone = '';
$user_addr  = '';
$user_image = '';

$finalQuotes = array();

if(isset($model) && count($model)>0){

  /* create li for contact details */

  foreach($model as $_model){

        if(empty($_model->image)){
          $image = 'noImage.png';
        }else{
          $image = $_model->image;
        }

      $cntct_li .= '<li cid="'.$_model->contact_id.'" class="ciid">';
        $cntct_li .= '<span class="cnt_img"><img height="55" width="55" src="'.$siteURL.'/'.$image.'"></span>';
          $cntct_li .= '<div class="contact_person_detail">';
            $cntct_li .= '<p>'.ucfirst($_model->name).'</p>';
            $cntct_li .= '<small>'.$_model->phone.'</small>';
          $cntct_li .= '</div>';
      $cntct_li .= '</li>';
  }//foreach

$user_name  = $model[0]->name;
$user_email = $model[0]->email;
$user_phone = $model[0]->phone;
$user_addr  = $model[0]->address->street1;
$user_image = $model[0]->image;

if(empty($user_image)){
  $user_image = 'noImage.png';
}else{
  $user_image = $model[0]->image;
}


// array for quotes
if(isset($model[0]->tblQuotes) && count($model[0]->tblQuotes)>0){
  $qarray = array_reverse($model[0]->tblQuotes);
  $finalQuotes = getQuotes($qarray,2);
}

// array for communications
if(isset($model[0]->tblCommunications) && count($model[0]->tblCommunications)>0){
  $carray = array_reverse($model[0]->tblCommunications);
}

// array for appointments
if(isset($model[0]->tblAppointments) && count($model[0]->tblAppointments)>0){
  $aarray = array_reverse($model[0]->tblAppointments);
}

// array for invoices
if(isset($model[0]->tblInvoices) && count($model[0]->tblInvoices)>0){
  $iarray = array_reverse($model[0]->tblInvoices);
}

}

/* function to return latest two QUOTES */

function getQuotes( $an_array, $elements ) {
  return array_slice( $an_array, 0, $elements );
} //getQuotes


?>

<style>
  .ui-menu {
    z-index: 9999;
  }

  .row {
      margin-right: -15px;
      margin-left: -15px;
      float: left;
      width: 100%;
  } 

</style>

<div class="wrap">

  <div class="quote_detail_wrap">
    <div class="container">
      <div class="quote_detail_inner">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs nav-justified contact_tabs_list">
          <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#detail" role="tab">Details</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#quotes" role="tab">Quotes</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#communication" role="tab">Communications</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#invoices" role="tab">Invoices</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#appointment" role="tab">Appointments</a>
          </li>
        </ul>
        <!-- Tab panels -->
        <div class="tab-content card">
          <!--Panel 1-->
          <div class="tab-pane fade in show active" id="detail" role="tabpanel">
            <div class="quote_left_fix text-center">
              <div class="form-group search_div">
                <input type="text" class="form-control search contact_search" name="Search" Placeholder="Search">
				<div class="mCustomScrollbar mscrollbar">
                <ul class="contact_list">
                  <?php
                    echo $cntct_li;
                  ?>
                </ul><!-- cotact_list -->
				</div>
                <a href="#" data-toggle="modal" data-target="#addContact" class="add_contact text-right"><img src="<?php echo $siteURL; ?>/contact_blue.png"></a>

                  

              </div><!-- search_div -->
            </div><!-- quote_left_fix -->
            <div class="quote_right_sec">
              <div class="contact_detail_heading">
                <h3>Personal Profile</h3>
                <a href="#" class="edit text-right"><img src="<?php echo $siteURL;?>/edit_new.png"></a>
              </div><!-- contact_detail_heading -->
              <div class="personal_deatil row personal_div1">
                <div class="personal_inner left col text-center">
                  <span class="personal_profile"><img height="55" width="55" src="<?php echo $siteURL.'/'.$user_image ?>"></span>
                  <h4 class="contact_name"><?php echo ucfirst($user_name); ?></h4>
                  <a href="#" class="contact_email"><?php echo $user_email; ?></a>
                  <a href="#" class="contact_phn"><?php echo $user_phone; ?></a>
                </div><!-- personal_left -->
                <div class="personal_inner mid col">
                  <h5>Address</h5>
                  <p><?php echo $user_addr; ?></p>
                  <div class="map_location">
                    <span class="location_icon"><img src="<?php echo $siteURL;?>/location.png"></span>
                    <div class="location_map">
                    <!-- <img src="<?php //echo $siteURL;?>/contact_map.png"> -->
                    <iframe width="216" height="144" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.it/maps?q=<?php echo $user_addr; ?>&output=embed"></iframe>
                    </div>
                  </div><!-- MAP LOCATION -->
                </div><!-- personal_left -->
                <div class="personal_inner right col">
                  <ul class="personal_count">
                    <li>
                      <h4>10<h4>
                      <span><img src="<?php echo $siteURL;?>/mail.png"></span>
                    </li>
                    <li>
                      <h4>8<h4>
                      <span><img src="<?php echo $siteURL;?>/Invoices_icon.png"></span>
                    </li>
                    <li>
                      <h4>8<h4>
                      <span><img src="<?php echo $siteURL;?>/Quote_icon.png"></span>
                    </li>
                    <li>
                      <h4>12<h4>
                      <span><img src="<?php echo $siteURL;?>/calender_new.png"></span>
                    </li>
                  </ul><!-- personal_count -->
                </div><!-- personal_left -->
              </div><!-- personal_deatail -->
              <div class="contact_detail_heading">
                <h3>Quotes</h3>
                <a href="#" class="edit text-right"><img src="<?php echo $siteURL;?>/add_quote_con.png"></a>
              </div><!-- contact_detail_heading -->
              <div class="personal_deatil personal_div2">
              <?php

                  foreach($finalQuotes as $_finalQuotes){

                    $date = $_finalQuotes['created_at'];
                    $date = gmdate('F d Y', $date);
                    $date = explode(' ',$date);


                      //for type
                      $type = $_finalQuotes['type'];
                      switch ($type) {
                          case "0":
                              $finaltype = "Interiors";
                              break;
                          case "1":
                              $finaltype =  "Exteriors";
                              break;
                          default:
                              $finaltype =  "Interiors";
                      }

                      //for status
                      $status = $_finalQuotes['status'];
                      switch ($status) {
                          case "1":
                              $finalstatus = "Pending";
                              break;
                          case "2":
                              $finalstatus =  "In-progress";
                              break;
                          case "3":
                              $finalstatus =  "Completed";
                              break;
                          default:
                              $finalstatus =  "Pending";
                      }

                      echo '<ul class="quotes_list">';
                        echo '<li><small>'.$date[0].'</small><h4>'.$date[1].'</h4></li>';
                        echo '<li><span>142</span><h3 class="name">'.$user_name.'</h3></li>';
                        echo '<li><p>'.$_finalQuotes['description'].'</p></li>';
                        //echo '<li><div class="interior"><img src="'.$siteURL.'/quotes_Interior_icon.png"><span>Interiors</span></div></li>';
                        echo '<li><div class="interior"><img src="'.$siteURL.'/quotes_Interior_icon.png"><span>'.$finaltype.'</span></div></li>';
                        echo '<li><div class="interior"><img src="'.$siteURL.'/quote_price-icon.png"><span>$1645.54</span></div></li>';
                        //echo '<li><div class="compt_Action"><img src="'.$siteURL.'/complet.png"><span>Completed</span></div></li>';
                        echo '<li><div class="compt_Action"><img src="'.$siteURL.'/complet.png"><span>'.$finalstatus.'</span></div></li>';
                      echo '</ul>';

                  }//foreach


              ?>

              </div><!-- personal_detail -->
            </div><!-- quote_right_sec -->
          </div>
          <!--/.Panel 1-->
          <!--Panel 2-->
          <div class="tab-pane fade" id="quotes" role="tabpanel">
            <div class="quote_left_fix text-center">
              <div class="form-group search_div">
                <input type="text" class="form-control search contact_search" name="Search" Placeholder="Search">
				<div class="mCustomScrollbar mscrollbar">
					<ul class="contact_list">
					  <?php
						echo $cntct_li;
					  ?>
					</ul><!-- cotact_list -->
				</div>
              </div><!-- search_div -->
            </div><!-- quote_left_fix -->
            <div class="quote_right_sec contact_quotes">
              <h3>Quotes</h3>
              <?php

                  foreach($finalQuotes as $_finalQuotes){

                    $date = $_finalQuotes['created_at'];
                    $date = gmdate('F d Y', $date);
                    $date = explode(' ',$date);

                    //for type
                    $type = $_finalQuotes['type'];
                    switch ($type) {
                        case "0":
                            $finaltype = "Interiors";
                            break;
                        case "1":
                            $finaltype =  "Exteriors";
                            break;
                        default:
                            $finaltype =  "Interiors";
                    }

                    //for status
                    $status = $_finalQuotes['status'];
                    switch ($status) {
                        case "1":
                            $finalstatus = "Pending";
                            break;
                        case "2":
                            $finalstatus =  "In-progress";
                            break;
                        case "3":
                            $finalstatus =  "Completed";
                            break;
                        default:
                            $finalstatus =  "Pending";
                    }

                    echo '<ul class="quotes_list">';
                      echo '<li><small>'.$date[0].'</small><h4>'.$date[1].'</h4></li>';
                      echo '<li><span>142</span><h3 class="name">'.$user_name.'</h3></li>';
                      echo '<li><p>'.$_finalQuotes['description'].'</p></li>';
                      //echo '<li><div class="interior"><img src="'.$siteURL.'/quotes_Interior_icon.png"><span>Interiors</span></div></li>';
                      echo '<li><div class="interior"><img src="'.$siteURL.'/quotes_Interior_icon.png"><span>'.$finaltype.'</span></div></li>';
                      echo '<li><div class="interior"><img src="'.$siteURL.'/quote_price-icon.png"><span>$1645.54</span></div></li>';
                      //echo '<li><div class="compt_Action"><img src="'.$siteURL.'/complet.png"><span>Completed</span></div></li>';
                      echo '<li><div class="compt_Action"><img src="'.$siteURL.'/complet.png"><span>'.$finalstatus.'</span></div></li>';
                    echo '</ul>';

                  }//foreach

              ?>

            </div><!-- quote_right_sec -->
          </div>
          <!--/.Panel 2-->
          <!--Panel 3-->
          <div class="tab-pane fade" id="communication" role="tabpanel">
            <div class="quote_left_fix text-center">
              <div class="form-group search_div">
                <input type="text" class="form-control search contact_search" name="Search" Placeholder="Search">
                <ul class="contact_list">
                  <?php
                    echo $cntct_li;
                  ?>
                </ul><!-- cotact_list -->
              </div><!-- search_div -->
            </div><!-- quote_left_fix -->
            <div class="quote_right_sec comm_sec">

            <?php

            if(count($carray)>0){

                $host_name = isset($_SERVER['HTTP_X_FORWARDED_HOST']) ? $_SERVER['HTTP_X_FORWARDED_HOST'] : $_SERVER['HTTP_HOST'];          
                $siteURL = 'http://' . $host_name . '/' . Yii::$app->request->baseUrl;  

                foreach($carray as $_modelComm){

                  echo '<div class="mail_box">';

                    echo '<div class="mail_heading">'.date('d F Y',$_modelComm->created_at).'</div>';

                      echo '<div class="inner_mail_box">';
                        echo '<h4 class="mail_sub">'.ucwords($_modelComm->commQuote->description).'</h4>';

                          echo '<div class="form-group">';
                          echo '<label>From</label>';
                          echo '<p>'.$_modelComm->commSubscriber->username.'</p>';
                          echo '</div>';/* form-group */

                          echo '<div class="form-group">';
                          echo '<label>Date</label>';
                          echo '<p>'.date('d F Y',$_modelComm->created_at).'</p>';
                          echo '</div>';/* form-group */


                          echo '<div class="form-group">';
                          echo '<label>To</label>';
                          echo '<p>'.$_modelComm->receiver_email.'</p>';
                          echo '</div>';/* form-group */


                          echo '<div class="form-group">';
                          echo '<label>To</label>';
                          echo '<p>'.$_modelComm->receiver_email.'</p>';
                          echo '</div>';/* form-group */



                          echo '<div class="mail-content-box">';
                                echo $_modelComm->body;
                          echo '<a href="#" class="text-right">View More</a>';
                          echo '</div>';/* mail-content-box */

                      echo '</div>'; /*inner_mail_box*/

                  echo '</div>';

                }//foreach

              } //if

            ?>

            </div><!-- quote_right_sec -->
          </div><!--/.Panel 3-->
          <div class="tab-pane fade" id="invoices" role="tabpanel">
            <div class="quote_left_fix text-center">
              <div class="form-group search_div">
                <input type="text" class="form-control search contact_search" name="Search" Placeholder="Search">
                <ul class="contact_list">
                  <?php
                    echo $cntct_li;
                  ?>
                </ul><!-- cotact_list -->
              </div><!-- search_div -->
            </div><!-- quote_left_fix -->
            <div class="quote_right_sec">
              <div class="appointment_box invoices_list">
                <table class="invoice_table table table-borderless table-condensed table-hover">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Quote ID</th>
                      <th>Quote Description</th>
                      <th>Description</th>
                      <th>Amount</th>
                      <th>Create/Send</th>
                      <th>Paid</th>
                      <th>Date</th>
                    </tr>
                  </thead>
                  <tbody>

                  <?php 
                  if(count($iarray)>0){

                    foreach($iarray as $_iarray){

                      echo '<tr>';
                        echo '<td>'.$_iarray->invoice_id.'</td>';
                        echo '<td>'.$_iarray->quote_id.'</td>';
                        echo '<td>'.$_iarray->description.'</td>';
                        echo '<td>Deopsit</td>';
                        echo '<td>$159.32</td>';
                        echo '<td>No</td>';
                        echo '<td>-</td>';
                        echo '<td>03-04-2018</td>';
                      echo '</tr>';

                    }//foreach

                  }


                  ?>


                  </tbody>
                </table>
              </div><!-- appointment_box -->
            </div><!-- quote_right_sec -->
          </div><!--/.Panel 4-->
          <div class="tab-pane fade" id="appointment" role="tabpanel">
            <div class="quote_left_fix text-center">
              <div class="form-group search_div">
                <input type="text" class="form-control search contact_search" name="Search" Placeholder="Search">
                <ul class="contact_list">
                  <?php
                    echo $cntct_li;
                  ?>
                </ul><!-- cotact_list -->
              </div><!-- search_div -->
            </div><!-- quote_left_fix -->
            <div class="quote_right_sec">
              <a href="#" class="addAppointment"><img src="<?php echo $siteURL;?>/add_quote.png"></a>
              <div class="appointment_box appointment_list">
                <div class="container">
                  <div class="row head">
                    <div class="col">
                      <h5>Date/Time</h5>
                    </div><!-- col -->
                    <div class="col-6">
                      <h5>Location</h5>
                    </div><!-- col -->
                    <div class="col">
                      <h5>User Note</h5>
                    </div><!-- col -->
                  </div><!-- row -->

                  <?php 
                    if(count($aarray)>0){

                      foreach($aarray as $_aarray){

                        echo '<div class="row  body">';
                          echo '<div class="col">';
                            echo '<h4>'.date('d F Y', $_aarray->created_at).'</h4>';
                            echo '<small>'.date('h:i a', $_aarray->created_at).'</small>';
                          echo '</div>';
                          echo '<div class="col-6">';
                            echo '<p>'.$_aarray->formatted_addr.'</p>';
                          echo '</div>';
                          echo '<div class="col">';
                            echo '<p>'.$_aarray->note.'</p>';
                          echo '</div>';
                        echo '</div>';

                      }//foreach
                    }
                  ?>

                </div><!-- container -->
              </div><!-- appointment_box -->
            </div><!-- quote_right_sec -->
          </div><!--/.Panel 5-->
        </div><!-- tab-content-top -->
      </div><!-- quote_detail_inner -->
    </div><!-- container -->


    <div class="wrap_footer">
      <div class="container">
        <a href="#" class="footer_back_logo"><img src="<?php echo $siteURL;?>/footer_logo.png" alt=""></a>
        <div class="room_detailfill">
          <h4>Joshua Bedroom</h4>
          <ul class="room_fillComplete">
            <li class="steps complete">
              <p>Ceiling</p>
              <span ></span>
            </li>
            <li class="steps complete">
              <p>Cornices</p>
              <span></span>
            </li>
            <li class="steps current">
              <p>Doors</p>
              <span></span>
            </li>
            <li class="steps">
              <p>Trims</p>
              <span></span>
            </li>
            <li class="steps">
              <p>Walls</p>
              <span></span>
            </li>
            <li class="steps">
              <p>Windows</p>
              <span></span>
            </li>
          </ul><!-- room_fill_complete -->
        </div><!-- room_detailfill -->
        <a href="#" class="nxt_room"><img src="<?php echo $siteURL;?>/addroom.png" alt=""> <span>Add Next Room</span></a>
      </div><!-- container -->
    </div><!-- wrap_footer -->
  </div><!-- quote_detail_wrap -->
  
</div><!-- wrap -->

<!-- Modal -->
<div id="addContact" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><img src="image/addContact.png" alt=""> Add Contact</h4>
      </div>
      <?php $form = ActiveForm::begin(['validationUrl' => Url::toRoute(['site/validate-form']), 'enableAjaxValidation' => true, 'id' => 'myidcontact']); ?>
      <div class="modal-body container">
        <div class="row existing_client_detail">
        <h2>Personal Information</h2>
        
        <div class="profile_div"><!-- <img src="image/avtar.png" alt=""><a href="#" class="profile_edit"><img src="image/edit_profile.png" alt=""></a> -->
        <div id="filee"></div>
        <?= $form->field($cntModel, 'image')->fileInput(['maxlength' => true, 'id'=>'file']) ?>
        </div>
        <div class="col">
          <div class="form-group">
            <?= $form->field($cntModel, 'name')->textInput(['maxlength' => true, 'class' => 'form-control name', 'placeholder' => 'Contact name', 'autocomplete' => 'off'])->label(false) ?>
          </div><!-- form-group -->
        </div><!-- col -->
        <div class="col">
          <div class="form-group">
            <?= $form->field($cntModel, 'email')->textInput(['maxlength' => true, 'class' => 'form-control email', 'placeholder' => 'Contact email', 'autocomplete' => 'off'])->label(false) ?>
          </div><!-- form-group -->
        </div><!-- col -->
        <div class="col">
          <div class="form-group">
            <?= $form->field($cntModel, 'phone')->textInput(['maxlength' => true, 'class' => 'form-control email', 'placeholder' => 'Contact phone', 'autocomplete' => 'off'])->label(false) ?>
          </div><!-- form-group -->
        </div><!-- col -->
      </div>

      <div class="row existing_menu">
        <h2>Contact Information</h2>
        <div class="col-8">
          <div class="form-group ui-front">
            <?= $form->field($addModel, 'street1')->textInput(['maxlength' => true, 'class' => 'form-control Address', 'id' => 'tbladdress_street2', 'placeholder' => 'Address'])->label(false) ?>
          </div><!-- form-group -->
          <div class="row">
            <div class="col">
              <div class="form-group">
                 <?= $form->field($addModel, 'suburb')->textInput(['maxlength' => true, 'class' => 'form-control Suburb', 'id' => 'tbladdress_suburb2', 'placeholder' => 'Suburb', 'autocomplete' => 'off'])->label(false) ?>
              </div><!-- form-group -->
            </div><!-- col -->
            <div class="col">
              <div class="form-group">
                <?= $form->field($addModel, 'state_id')->textInput(['maxlength' => true, 'class' => 'form-control state', 'id' => 'tbladdress_state_id2', 'placeholder' => 'State', 'autocomplete' => 'off'])->label(false) ?>
              </div><!-- form-group -->
            </div><!-- col -->
          </div><!-- row -->
          <div class="row">
            <div class="col-6">
              <div class="form-group">
                <?= $form->field($addModel, 'postal_code')->textInput(['maxlength' => true, 'class' => 'form-control Suburb', 'id' => 'tbladdress_postal2', 'placeholder' => 'Postcode', 'autocomplete' => 'off'])->label(false) ?>
              </div><!-- form-group -->
            </div><!-- col -->
          </div><!-- row -->
        </div><!-- col -->
        <div class="col-4">
          <div class="map_div">
          <div id="map2"></div></div>
        </div><!-- col -->
      </div>
    
    </div>
  <div class="goto_btn_div">
    <div class="col text-right">
      <!-- <button type="submit" class="gotoBtn btn btn-primary"><img src="image/goto_icon.png" alt=""></button> -->

      <?= Html::submitButton('Go To', ['class' =>'gotoBtn btn btn-primary subm']); ?>   
    </div><!-- col -->
  </div>

  <?php ActiveForm::end(); ?>
  </div>

  </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script type="text/javascript">

var abc = 0;      // Declaring and defining global increment variable.

$(document).ready(function() {

// Following function will executes on change event of file input to select different file.
$('body').on('change', '#file', function() {
  if (this.files && this.files[0]) {
  abc += 1; // Incrementing global variable by 1.
  var z = abc - 1;
  var x = $(this).parent().find('#previewimg' + z).remove();
  $('#filee').append("<div id='abcd" + abc + "' indx='" + z + "' class='abcd'><img id='previewimg" + abc + "' src=''/></div>");

  $(this).parent().hide();
  var reader = new FileReader();
  reader.onload = imageIsLoaded;
  reader.readAsDataURL(this.files[0]);
  $(this).hide();


  $("#abcd" + abc).append($("<p id='img' class='del'>Delete</p>").click(function() {
  
    $(this).parent().remove();

    $('#file').show();
    $('#file').parent().show();

  }));
  }
});
// To Preview Image
function imageIsLoaded(e) {
  $('#previewimg' + abc).attr('src', e.target.result);
};

$('#upload').click(function(e) {
  var name = $(":attachment").val();
  if (!name) {
  alert("First Image Must Be Selected");
  e.preventDefault();
  }
});
});

</script>
