<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\file\FileInput;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\TblContacts */
/* @var $form yii\widgets\ActiveForm */
?>
<style type="text/css">
    
<style type="text/css">
.tbl-emails-create h1 { display: none; } .cke_top { background: #00AEEF; } .selectRow { display : block; padding : 20px; } .select2-container { width: 200px; } .customclass{ color:red; } .abcd img { max-width: 100%; } .abcd { width: 20%; float: left; border: 3px solid #eee; margin: 2px; } div#filediv { float: left; width: 25%; padding: 0 15px; } p#img { font-size: 16px; text-align: center; cursor: pointer; background: #000; color: red; margin:0px !important; } .save-div { float: left; width: 100%; margin-top: 20px; } .save-div div#filediv { float: left; width: 100%; padding: 0; }#file{cursor : pointer !important;}.profile_div{z-index: 999999}
</style>
<!-- <div class="tbl-contacts-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'subscriber_id')->textInput() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address_id')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div> -->



</style>

      <div class="modal-header">        
        <h4 class="modal-title"><img src="image/addContact.png" alt=""> Add Contact</h4>
      </div>

            <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>
            <h2>Personal Information</h2>
            <div class="profile_div">
                <!-- <img src="image/avtar.png" alt=""><a href="#" class="profile_edit"><img src="image/edit_profile.png" alt=""></a> -->

                <div id="filee"></div>
                <div id="filediv"> <input name="TblContacts[image]" type="file" id="file"/> </div>
                <div class="save-div"></div>

            </div>
    
            <div class="col">                              
                <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Contact Name', 'class' => 'form-control name'])->label(false) ?>               
            </div><!-- col -->
            <div class="col"> 
                <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder' => 'Contact Email', 'class' => 'form-control email'])->label(false) ?>                
            </div><!-- col -->
            <div class="col">                
                <?= $form->field($model, 'phone')->textInput(['maxlength' => true, 'placeholder' => 'Contact Phone', 'class' => 'form-control phone'])->label(false) ?>                
            </div><!-- col -->

        <div class="row existing_menu">
            <h2>Contact Information</h2>
            <div class="col-8">
                <div class="form-group">
                    <input name="Address" class="form-control Address" placeholder="Address" type="text">
                </div><!-- form-group -->
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <input name="Suburb" class="form-control Suburb" placeholder="Suburb" type="text">
                        </div><!-- form-group -->
                    </div><!-- col -->
                    <div class="col">
                        <div class="form-group">
                            <select class="selectpicker" tabindex="-98">
                                <option>One</option>
                                <option>Two</option>
                                <option>Three</option>
                            </select>
                        </div><!-- form-group -->
                    </div><!-- col -->
                </div><!-- row -->
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <input name="Suburb" class="form-control Suburb" placeholder="Suburb" type="text">
                        </div><!-- form-group -->
                    </div><!-- col -->
                </div><!-- row -->
            </div><!-- col -->
            <div class="col-4">
                <div class="map_div">
                    <img src="image/map.png" alt="">
                    <span class="map-pin"><img src="image/location.png" alt=""></span>
                </div><!-- map_div -->
            </div><!-- col -->
        </div>

    <div class="form-group col text-right">
        <?= Html::submitButton('Save', ['class' => 'gotoBtn btn btn-primary btn-success']) ?>
    </div>

 <?php ActiveForm::end(); ?>

<script type="text/javascript">

    var abc = 0;      // Declaring and defining global increment variable.

    $(document).ready(function() {
    //  To add new input file field dynamically, on click of "Add More Files" button below function will be executed.

    var $imagefile = $('<input />').attr({
        type: 'file',
        name: 'TblContacts[image]',
        id: 'file'
    });

    
    // Following function will executes on change event of file input to select different file.
    $('body').on('change', '#file', function() {
      if (this.files && this.files[0]) {
      abc += 1; // Incrementing global variable by 1.
      var z = abc - 1;
      var x = $(this).parent().find('#previewimg' + z).remove();
      $("#filee").empty();
      $('#filee').append("<div id='abcd" + abc + "' indx='" + z + "' class='abcd'><img id='previewimg" + abc + "' src=''/></div>");
      var reader = new FileReader();
      reader.onload = imageIsLoaded;
      reader.readAsDataURL(this.files[0]);
      $(this).hide();


      $("#abcd" + abc).append($("<p id='img' class='del'>Delete</p>").click(function() {

        var attr = $(this).parent().attr('indx');
        //alert(attr);
        $('#filee').append("<input type='hidden' name='TblCommunications[indexx][]' value='" + attr + "'/>");
        $(this).parent().remove();
                
        $('#file').css("display", "block");
        $('#file').val(null); 

      }));
      }
    });
    // To Preview Image
    function imageIsLoaded(e) {
      $('#previewimg' + abc).attr('src', e.target.result);
    };

    $('#upload').click(function(e) {
      var name = $(":attachment").val();
      if (!name) {
      alert("First Image Must Be Selected");
      e.preventDefault();
      }
    });
    });

</script>
