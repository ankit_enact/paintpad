<?php
use yii\helpers\Html;
use yii\captcha\Captcha;

use yii\grid\GridView;
?>
<?php

echo GridView::widget([
            'dataProvider' => $brandProvider,
            'filterModel' => $brandModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'brand_id',
                'name',
                'logo',
                'enabled',
                'created_at',
                //'updated_at',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ])

?>