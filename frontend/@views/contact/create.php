<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TblContacts */

//$this->title = 'Create Tbl Contacts';
//$this->params['breadcrumbs'][] = ['label' => 'Tbl Contacts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-contacts-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
