<?php

use yii\helpers\Html;
use yii\grid\GridView;

use kartik\tabs\TabsX;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\TblContacts */

$this->title = 'Contact Detail';
$this->params['breadcrumbs'][] = ['label' => 'Tbl Contacts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

	// if(count($contactData)>0){
	// 	//echo "<pre>"; print_r($contactData); 
	// }

$content1 = 'this is content1';
$content2 = 'this is content2';
$content3 = 'this is content3';
$content4 = 'this is content4';


/*$items = [
    [
        'label'=>'<i class="glyphicon glyphicon-home"></i> Home',
        'content'=>$content1,
        'active'=>true
    ],
    [
        'label'=>'<i class="glyphicon glyphicon-user"></i> Profile',
        'content'=>$content2,
        'linkOptions'=>['data-url'=>Url::to(['/contact/index'])]
    ],
    [
        'label'=>'<i class="glyphicon glyphicon-list-alt"></i> Dropdown',
        'items'=>[
             [
                 'label'=>'Option 1',
                 'encode'=>false,
                 'content'=>$content3,
             ],
             [
                 'label'=>'Option 2',
                 'encode'=>false,
                 'content'=>$content4,
             ],
        ],
    ],
    [
        'label'=>'<i class="glyphicon glyphicon-king"></i> Disabled',
        'headerOptions' => ['class'=>'disabled']
    ],
];


echo TabsX::widget([
    'items'=>$items,
    'position'=>TabsX::POS_ABOVE,
    'encodeLabels'=>false
]);
*/


$tab1 = wrapPjax(GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => ['id' => 'inquiry-claimed-grid', 'class' => 'table table-striped table-bordered'],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'name',
                //'contact_id',
                //'subscriber_id',
                //'name',
                //'email:email',
                //'phone',
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]));

/*$tab2 = wrapPjax(GridView::widget([
            'dataProvider' => $brandProvider,
            'filterModel' => $brandModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'brand_id',
                'name',
                'logo',
                'enabled',
                'created_at',
                //'updated_at',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]));*/

        $tab2 = '';


echo TabsX::widget([
    'options' => ['tab' => 'div'],
    'itemOptions' => ['tab' => 'div'],
    'items' => [
        [
            'options' => ['id' => 'tab-c'],
            'label' => 'Contact',
            'content' => $tab1,
        ],
        [
            'options' => ['id' => 'tab-uc'],
            'label' => 'Brands',
            'content' => $tab2,
        ],
    ],
]);



function wrapPjax($grid) {
    ob_start();

    Pjax::begin(['timeout' => 10000]);
    echo $grid;
    Pjax::end();
    
    return ob_get_clean();
}


?>
