<?php
use yii\helpers\Html;
use yii\captcha\Captcha;

use yii\widgets\ActiveForm;
use yii\helpers\Url;

//echo "<pre>"; print_r($dataProvider);

?>
<style type="text/css">
	

/*==== CONTACT STYLE ====*/
.search_div .form-control.search {
	border: 1px solid #ececec;
	background: #fff;
	padding-left: 34px;
	background-image: url(../image/search.png);
	background-repeat: no-repeat;
	background-position: 10px center;
	background-size: 14px 15px;
	font-size: 15px;
}
.contact_icons img {
	margin-left: 10px;
}
.nav.nav-tabs.nav-justified.contact_tabs_list {
	border-bottom: 1px solid #ccc !important;
	width: 66%;
	right: 35px;
}
.contact_list {
	list-style: none;
	padding: 0;
	margin: 10px 0 0;
	text-align: left;
}
.contact_list li {
	padding: 15px 10px;
	float: left;
	width: 100%;
}
.contact_list li .cnt_img {
	float: left;
	margin-right: 12px;
}
.contact_list li .contact_person_detail {
	float: left;
}
.contact_person_detail p {
	font-size: 18px;
	margin: 0 0 2px;
	font-weight: 600;
	color: #545454;
}
.contact_person_detail small {
	color: #acacac;
}
.add_contact.text-right {
	width: 100%;
	float: left;
}
.contact_detail_heading h3 {
	color: #303030;
	font-size: 20px;
	font-weight: 600;
	float: left;
}
.edit.text-right {
	float: right;
}
.contact_detail_heading {
	float: left;
	width: 100%;
	border-bottom: 1px solid #efefef;
	padding: 0 10px 5px;
}
.personal_deatil.row {
	float: left;
	width: 100%;
	padding: 23px 0 0;
}
.personal_inner.mid {
	border-left: 1px solid #dfdfdf;
	border-right: 1px solid #dfdfdf;
}
.personal_inner h5 {
	font-size: 16px;
	font-weight: 600;
	color: #aeaeae;
}
.map_location {
	float: left;
	width: 100%;
	position: relative;
}
.location_map {
	float: left;
	width: 100%;
	position: relative;
}
.location_icon {
	position: absolute;
	left: 20px;
	top: 20px;
	z-index: 2;
}
.location_icon img {
	width: 22px;
}
.personal_count {
	padding: 20px 10px;
	list-style: none;
	float: left;
	width: 100%;
	text-align: center;
	margin-bottom: 0;
}
.personal_count li {
	float: left;
	width: 50%;
	text-align: center;
	border-right: 1px solid #dcdcdc;
	border-bottom: 1px solid #dcdcdc;
	min-height: 115px;
	padding: 20px 0;
	color: #cccccc;
	font-weight: 300;
}
.personal_count li:nth-child(2n){border-right:0px;}
.personal_count li:nth-child(3){border-bottom:0px;}
.personal_count li:nth-child(4){border-bottom:0px;}
.personal_count li h4 {
	font-size: 26px;
	font-weight: 300;
	color: #bcbcbc;
}
.contact_name {
	float: left;
	width: 100%;
	border-bottom: 1px solid #dfdfdf;
	font-size: 18px;
	padding-bottom: 5px;
	padding-top: 5px;
	margin-bottom: 5px;
}
.personal_inner.left a {
	color: #c6c6c6;
	font-size: 16px;
}
.quotes_list {
	float: left;
	width: 100%;
	padding: 15px 0;
	margin: 0;
	list-style: none;
	border-bottom: 1px solid #dbdbdb;
	display: flex;
	display: -webkit-flex;
}
.quotes_list li {
	padding: 0 15px;
	flex-flow: 1;
	-webkit-flex-grow: 1;
	box-flex: 1;
	-webkit-box-flex: 1;
}
.quotes_list li small {
	font-size: 16px;
	font-weight: 600;
	color: #444444;
	text-transform: uppercase;
}
.quotes_list li h4 {
	font-size: 32px;
	font-weight: lighter;
	color: #acacac;
}
.quotes_list li span {
	color: #00AEEF;
	font-size: 16px;
	font-weight: 600;
	letter-spacing: 1px;
}
.quotes_list li .name {
	font-size: 17px;
	white-space: nowrap;
	line-height: 29px;
}
.interior {
	text-align: center;
}
.quotes_list li .interior span {
	color: #b5b5b5;
	font-weight: normal;
	letter-spacing: 0;
}
.compt_Action {
	text-align: center;
}
.quotes_list li .compt_Action span {
	color: #7bdd8e;
}
#addContact .modal-dialog {
	max-width: 900px;
}
#addContact .modal-title {
	font-size: 18px;
	font-weight: normal;
}
#addContact .modal-title img {
	float: left;
	margin: 4px 10px 0 0;
}
#addContact .close {
	border: 0 !important;
	color: #149DE9;
	font-weight: 300 !important;
	font-size: 34px;
	line-height: 40px;
}
#addContact .row.existing_client_detail {
	padding: 0px 15px;
}
.profile_div {
	float: left;
	width: 100%;
	position: relative;
	margin-bottom: 15px;
}
.profile_div {
	float: left;
	width: 100%;
	position: relative;
	margin-bottom: 15px;
}
.profile_edit {
	position: absolute;
	left: 73px;
	top: 3px;
}
#addContact .row.existing_client_detail h2, #addContact .row.existing_menu h2 {
	font-size: 18px;
	margin-bottom: 15px;
	float:left;
	width:100%;
}
#addContact .row.existing_client_detail .col {
	padding: 0 5px;
}
#addContact .row.existing_menu {
	margin-top: 20px;
	padding: 0 15px;
}
#addContact .row.existing_menu .col-8, #addContact .row.existing_menu .col-4 {
	padding: 0 5px;
}
#addContact .goto_btn_div {
	background: transparent;
}
.quote_right_sec.contact_quotes > h3 {
	font-size: 20px;
	font-weight: 600;
	border-bottom: 1px solid #dbdbdb;
	padding-bottom: 10px;
}
.quotes_list:last-child {
	border-bottom: 0;
}
.mail_box {
	float: left;
	width: 100%;
	padding: 0 0 15px;
}
.mail_heading {
	float: left;
	width: 100%;
	background: #e4e4e4;
	padding: 4px 14px;
	font-size: 15px;
}
.mail_sub {
	font-weight: 600;
	font-size: 16px;
	float: left;
	width: 100%;
	margin-top: 10px;
	margin-bottom: 2px;
}
.mail_sub {
	font-weight: 600;
	font-size: 16px;
	float: left;
	width: 100%;
	margin-top: 10px;
	margin-bottom:10px;
}
.mail_box .form-group label {
	float: left;
	width: 16%;
	font-weight: 700;
	margin: 0;
	font-size:14px;
}
.mail_box .form-group p {
	float: left;
	width: 83%;
	margin: 0;
	font-size:14px;
}
.mail_box .form-group {
	float: left;
	width: 100%;
	margin: 1px 0;
}
.mail-content-box {
	float: left;
	width: 100%;
	margin-top: 10px;
}
.mail-content-box p {
	margin: 0 0 5px;
	float: left;
	width: 100%;
	font-size: 14px;
}
.inner_mail_box {
	float: left;
	width: 100%;
	padding: 0 20px;
}
.mail-content-box a {
	float: left;
	width: 100%;
	color: #323232;
	font-weight: 600;
}
.addAppointment {
	position: absolute;
	top: 0;
	right: 22px;
}
.appointment_box {
	float: left;
	width: 100%;
}
.appointment_box .row.head {
	background: #e4e4e4;
	padding: 5px 0;
	float: left;
	width: 100%;
}
.appointment_box .row.head h5 {
	margin: 0;
	font-size: 16px;
	font-weight: 600;
	color: #4c4c4c;
	line-height: 23px;
}
.appointment_box .row.body h4 {
	font-size: 18px;
	margin: 0;
}
.appointment_box .row.body small {
	color: #8c8c8c;
	line-height: 23px;
}
.appointment_box .row.body p {
	margin: 0;
	font-size: 16px;
	line-height: 20px;
}
.appointment_box .row.body {
	float: left;
	width: 100%;
	padding: 15px 0px;
	border-bottom: 1px solid #eaeaea;
}
.invoice_table.table th, .invoice_table.table td {
	padding: 10px 5px;
	font-weight: 600;
	font-size: 15px;
	background: #e4e4e4;
	border-top: 0;
	border-bottom: 1px solid #eceeef;
}
.invoice_table.table td {background:#fff}
.invoice_table.table td {
	background: #fff;
	font-size: 15px;
	color: #6c6c6c;
	font-weight: normal;
}

</style>
					<div class="tab-pane fade in show active" id="detail" role="tabpanel">
						<div class="quote_left_fix text-center">
							<div class="form-group search_div">
								<input type="text" class="form-control search" name="Search" Placeholder="Search">
								<ul class="contact_list">
									<li>
										<span class="cnt_img"><img src="image/contact1.png"></span>
										<div class="contact_person_detail">
											<p>Adman Ericesson</p>
											<small>0419565575</small>
										</div><!-- contact_person_detail -->
									</li>
									<li>
										<span class="cnt_img"><img src="image/contact2.png"></span>
										<div class="contact_person_detail">
											<p>Yugen Zenon</p>
											<small>yugenmo@gmail.com</small>
										</div><!-- contact_person_detail -->
									</li>
									<li>
										<span class="cnt_img"><img src="image/contact3.png"></span>
										<div class="contact_person_detail">
											<p>Afelaymonday</p>
											<small>0419565575</small>
										</div><!-- contact_person_detail -->
									</li>
									<li>
										<span class="cnt_img"><img src="image/contact4.png"></span>
										<div class="contact_person_detail">
											<p>Adman Ericesson</p>
											<small>0419565575</small>
										</div><!-- contact_person_detail -->
									</li>
									<li>
										<span class="cnt_img"><img src="image/contact5.png"></span>
										<div class="contact_person_detail">
											<p>Adman Ericesson</p>
											<small>0419565575</small>
										</div><!-- contact_person_detail -->
									</li>
								</ul><!-- cotact_list -->
								<a href="#" data-toggle="modal" data-target="#addContact" class="add_contact text-right"><img src="image/contact_blue.png"></a>
							</div><!-- search_div -->
						</div><!-- quote_left_fix -->
						<div class="quote_right_sec">
							<div class="contact_detail_heading">
								<h3>Personal Profile</h3>
								<a href="#" class="edit text-right"><img src="image/edit_new.png"></a>
							</div><!-- contact_detail_heading -->
							<div class="personal_deatil row">
								<div class="personal_inner left col text-center">
									<span class="personal_profile"><img src="image/profile.png"></span>
									<h4 class="contact_name">Adam ericesson</h4>
									<a href="#" class="contact_email">adamericesson@gmail.com</a>
									<a href="#" class="contact_phn">0419565575</a>
								</div><!-- personal_left -->
								<div class="personal_inner mid col">
									<h5>Address</h5>
									<p>1000 kaley Orlando FL 32804 united States</p>
									<div class="map_location">
										<span class="location_icon"><img src="image/location.png"></span>
										<div class="location_map"><img src="image/contact_map.png"></div>
									</div><!-- MAP LOCATION -->
								</div><!-- personal_left -->
								<div class="personal_inner right col">
									<ul class="personal_count">
										<li>
											<h4>10<h4>
											<span><img src="image/mail.png"></span>
										</li>
										<li>
											<h4>8<h4>
											<span><img src="image/Invoices_icon.png"></span>
										</li>
										<li>
											<h4>8<h4>
											<span><img src="image/Quote_icon.png"></span>
										</li>
										<li>
											<h4>12<h4>
											<span><img src="image/calender_new.png"></span>
										</li>
									</ul><!-- personal_count -->
								</div><!-- personal_left -->
							</div><!-- personal_deatail -->
							<div class="contact_detail_heading">
								<h3>Quotes</h3>
								<a href="#" class="edit text-right"><img src="image/add_quote_con.png"></a>
							</div><!-- contact_detail_heading -->
							<div class="personal_deatil">
								<ul class="quotes_list">
									<li><small>May</small><h4>01</h4></li>
									<li><span>142</span><h3 class="name">Adam Ericesson</h3></li>
									<li><p>Special items Testing </p></li>
									<li><div class="interior"><img src="image/quotes_Interior_icon.png"><span>Interiors</span></div></li>
									<li><div class="interior"><img src="image/quote_price-icon.png"><span>$1645.54</span></div></li>
									<li><div class="compt_Action"><img src="image/complet.png"><span>Completed</span></div></li>
								</ul><!-- quotes_list -->
								<ul class="quotes_list">
									<li><small>May</small><h4>01</h4></li>
									<li><span>142</span><h3 class="name">Adam Ericesson</h3></li>
									<li><p>Special items Testing </p></li>
									<li><div class="interior"><img src="image/quotes_Interior_icon.png"><span>Interiors</span></div></li>
									<li><div class="interior"><img src="image/quote_price-icon.png"><span>$1645.54</span></div></li>
									<li><div class="compt_Action"><img src="image/complet.png"><span>Completed</span></div></li>
								</ul><!-- quotes_list -->
							</div><!-- personal_detail -->
						</div><!-- quote_right_sec -->
					</div>
					<!--/.Panel 1-->