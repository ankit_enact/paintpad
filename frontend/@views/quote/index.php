<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\QuoteAsset;
QuoteAsset::register($this);

$this->title = 'Quotes';
$this->params['breadcrumbs'][] = $this->title;
$siteURL = Url::base(true);
?>

<style type="text/css">
  
   #ui-id-1 img {
      height: 45px;
      width: 45px;
      border-radius: 50%;
      margin-right: 10px;
      float: left;
  }
  .ui-menu li span{
    font-size:2em;
    padding:0 0 10px 10px;
    margin:0 0 10px 0 !important;
    white-space:nowrap;
  }
  .intActive{
    width: 30px;
    height: 30px;
    background-image: url(../image/quote_interior.png);
    overflow: hidden;
    margin: 0 auto;
    display: block;
  }
  .ui-widget.ui-widget-content {
      border: 1px solid lightgrey;
      background: #f8f8f8;
      padding: 0;
      max-height: 292px !important;
      overflow-y: auto;
  }
  .ui-widget.ui-widget-content li { 
      border-bottom: 1px solid #e6e6e6;
      padding: 10px 0px;
  }
  h3.client_name_list {
    font-weight: normal;
    font-size: 16px;
    line-height: 15px;
    margin: 0;
    height: 16px;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  }
  .ui-widget.ui-widget-content li small.client_phone_list {
      color: #b3b2b2;
      font-weight: 300 !important;
      font-size: 13px;
      font-family: 'Open Sans';
  }
  .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active, a.ui-button:active, .ui-button:active, .ui-button.ui-state-active:hover {
      border: 0px solid #e4e4e4;
      background: transparent;
      font-weight: normal;
      color: #000;
      margin: 0;
  }
  .ui-widget.ui-widget-content li:hover {
      background: #efefef;
  }
</style>

<div class="container">

  <div class="quote_detail_inner">
        <!-- Nav tabs -->
        <!-- Tab panels -->
        <div class="tab-content card">
          <!--Panel 1-->
          <div class="tab-pane fade in show active" id="detail" role="tabpanel">
            <div class="quote_left_fix text-center">
              <input type="hidden" id="quote_contact_id" value="">
              <input type="hidden" id="subscriber_id" value="<?= Yii::$app->user->id ?>">
              <input type="hidden" id="if_interior_quote" value="1">
              <input type="hidden" id="if_exterior_quote" value="1">
              <input type="hidden" id="quote_page" value="1">
              <input type="hidden" id="quote_status" value="">
              <input type="hidden" id="quote_url" value="<?= Url::toRoute('webservice/quotes', true) ?>">
              <input type="hidden" id="contact_url" value="<?= Url::toRoute('quote/list-subscriber-contacts-json', true) ?>">
              <input type="hidden" id="site_url" value="<?= $siteURL ?>">
              <input type="hidden" id="quote_view_url" value="<?= Url::toRoute('quote/view/', true) ?>">
              <div class="paint_lft_form">
                <h4>Filter</h4>
                <div class="form-group quote-form-group">
                  <label><img src="image/client_name_icon.png"> Client Name</label>
                  <input type="text" id="search_existing_client_quote" class="form-control searchfield" name="Search" value="" placeholder="Search">
                  <?php
                    $arr = [];
                    if(count($contacts['data'])){                            
                      foreach ($contacts['data']['contact'] as $key => $contact) {

                            $newarr['id']       = $contact['contact_id'];
                            $newarr['value']    = $contact['name'];
                            $newarr['label']    = $contact['name'];
                            $newarr['phone']    = $contact['phone'];
                            $newarr['img']      = $contact['image'];
                            $arr[] = $newarr;
                      }
                    }
                    $jsonUsers = json_encode($arr);
                    $jsonUsers = str_replace("'", "\'", $jsonUsers);
                  ?>
                  <input type="hidden" id="contact_users" value='<?= $jsonUsers ?>'>
                </div>
                <div class="form-group quote-form-group">
                  <label><img src="<?= $siteURL ?>/image/quote.png"> Quote No.</label>
                  <input type="text" class="form-control searchfield" name="Search" value="" placeholder="Search">
                </div><!-- form-group -->
                <div class="form-group quote-form-group">
                  <label><img src="<?= $siteURL ?>/image/date_icon.png"> Date</label>
                  <div class="dateDiv">
                    <div class="startDate">
                      <label class="control-label">From</label>
                      <input type="text" id="txtFromDate" placeholder="01/01/2018" />
                    </div>
                    <span class="hashSeparator">-</span>
                    <div class="EndDate">
                      <label >To</label>
                      <input type="text" id="txtToDate" placeholder="01/08/2018" />
                    </div>
                  </div><!-- dateDiv -->
                </div><!-- form-group -->
                <div class="form-group quote-form-group">
                  <label><img src="<?= $siteURL ?>/image/side_bar_ie_icon.png"> I/E</label>
                  <div class="quoteI-E">
                    <a href="javaScript:void(0)" data-type="1" data-tochange="if_interior_quote" data-check="if_exterior_quote" class="quoteTypeSel QuotePageInterior quotetypeactive"><span></span><p>Interior</p></a>
                    <a href="javaScript:void(0)" data-type="2" data-tochange="if_exterior_quote" data-check="if_interior_quote" class="quoteTypeSel QuotePageExterior quotetypeactive"><span></span><p>Exterior</p></a>
                  </div><!-- quoteI-E -->
                </div><!-- form-group -->
                <div class="form-group quote-form-group">
                  <label><img src="<?= $siteURL ?>/image/status.png"> Status</label>
                  <div class="quoteI-Status">
                    <a href="javaScript:void(0)" class="QuotePageStatus complt"><span></span><p>Complete</p></a>
                    <a href="javaScript:void(0)" class="QuotePageStatus progres"><span></span><p>Progress</p></a>
                    <a href="javaScript:void(0)" class="QuotePageStatus pending"><span></span><p>Pending</p></a>
                  </div><!-- quoteI-E -->
                </div><!-- form-group -->
                <!-- <div class="quoteAdvanceDrop">
                  <div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Advanced Search
                    <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                      <li><a href="#">HTML</a></li>
                      <li><a href="#">CSS</a></li>
                      <li><a href="#">JavaScript</a></li>
                    </ul>
                  </div>
                </div> --><!-- quoteAdvanceDrop-->
                <div class="form-group quote-form-group lft_btm_buttons">
                  <button type="button" name="Reset All" id="filterReset" class="btn quotelftbtns reset">Reset All</button>
                  <button type="button" name="Apply" id="filerApply" class="btn quotelftbtns Apply">Apply</button>
                </div>
              </div><!-- paint_lft_form -->
            </div><!-- quote_left_fix -->
            <div class="quote_right_sec quotePage_rhtSec">
              <div class="personal_deatil">
                <table class="Quotetables">
                  <thead>
                    <tr>
                      <th>Date</th>
                      <th>Client</th>
                      <th>Quote Note</th>
                      <th style="text-align: center;">I/E</th>
                      <th>Price(Inc. GST)</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody id="quotesTbody" class="mCustomScrollbar">
                    <?php 
                      if($response['success'] == '1'){
                        foreach ($response['data']['quotes'] as $key => $data) {
                          $type = ($data['type'] == 1)?'Interiors':'Exteriors';
                          $statusList = app\models\TblQuotes::quotesStatusStr($data['status']);
                          $statusStr = $statusList[0];
                          $statusImg = $statusList[1];
                          echo '<tr class="quoteTr" data-href="'.Url::toRoute(['quote/view/','quote_id' => $data['quote_id']], true).'">'.
                            '<td><small>'.date('M',$data['date']).'</small><h4>'.date('d',$data['date']).'</h4></td>'.
                            '<td><span>'.$data['quote_id'].'</span><h3 class="name">'.$data['contact']['name'].'</h3><span class="phnNo">'.$data['contact']['phone'].'</span></td>'.
                            '<td>'.$data['note'].'</td>'.
                            '<td><div class="interior"><img src="'.$siteURL.'/image/quotes_Interior_icon.png"><span>'.$type.'</span></div></td>'.
                            '<td><div class="interior"><img src="'.$siteURL.'/image/quote_price-icon.png"><span>$'.$data['price'].'</span></div></td>'.
                            '<td><div class="interior statusReport"><img src="'.$statusImg.'"><span>'.$statusStr.'</span></div></td>'.
                          '</tr>';
                        }
                      }
                      else{
                        echo '<tr colspan="6">'.$response['message'].'<tr>';
                      }
                    ?>
                  </tbody>
                </table>
              </div><!-- personal_detail -->
            </div><!-- quote_right_sec -->
          </div>
          <!--/.Panel 1-->
        </div><!-- tab-content-top -->
      </div><!-- quote_detail_inner -->



</div>


<div id="addContact" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><img src="image/addContact.png" alt=""> Add Contact</h4>
      </div>
      <div class="modal-body container">
        <div class="row existing_client_detail">
      <h2>Personal Information</h2>
      <div class="profile_div"><img src="image/avtar.png" alt=""><a href="javaScript:void(0)" class="profile_edit"><img src="image/edit_profile.png" alt=""></a></div>
      <div class="col">
        <div class="form-group">
          <input name="Name" class="form-control name" placeholder="Contact name" type="text">
        </div><!-- form-group -->
      </div><!-- col -->
      <div class="col">
        <div class="form-group">
          <input name="Email" class="form-control email" placeholder="Contact email" type="text">
        </div><!-- form-group -->
      </div><!-- col -->
      <div class="col">
        <div class="form-group">
          <input name="Name" class="form-control phone" placeholder="Contact phone" type="text">
        </div><!-- form-group -->
      </div><!-- col -->
    </div>
    <div class="row existing_menu">
      <h2>Contact Information</h2>
      <div class="col-8">
        <div class="form-group">
          <input name="Address" class="form-control Address" placeholder="Address" type="text">
        </div><!-- form-group -->
        <div class="row">
          <div class="col">
            <div class="form-group">
              <input name="Suburb" class="form-control Suburb" placeholder="Suburb" type="text">
            </div><!-- form-group -->
          </div><!-- col -->
          <div class="col">
            <div class="form-group">
              <select class="selectpicker" tabindex="-98">
                <option>One</option>
                <option>Two</option>
                <option>Three</option>
              </select>
            </div><!-- form-group -->
          </div><!-- col -->
        </div><!-- row -->
        <div class="row">
          <div class="col-6">
            <div class="form-group">
              <input name="Suburb" class="form-control Suburb" placeholder="Suburb" type="text">
            </div><!-- form-group -->
          </div><!-- col -->
        </div><!-- row -->
      </div><!-- col -->
      <div class="col-4">
        <div class="map_div">
          <img src="image/map.png" alt="">
          <span class="map-pin"><img src="image/location.png" alt=""></span>
        </div><!-- map_div -->
      </div><!-- col -->
    </div>
    
    </div>
  <div class="goto_btn_div">
    <div class="col text-right">
      <button type="submit" class="gotoBtn btn btn-primary"><img src="image/goto_icon.png" alt=""></button>
    </div><!-- col -->
  </div>
  </div>

  </div>
</div>