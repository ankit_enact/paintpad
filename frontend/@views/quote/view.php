<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use frontend\assets\QuoteAsset;
QuoteAsset::register($this);

$this->title = 'Quotes';
$this->params['breadcrumbs'][] = $this->title;
$siteURL = Url::base(true);

$commonLat = 28.517957;
$commonLng = -81.36591199999999;

$commonAddress = "2000 S Mills Ave, Orlando, FL 32806, USA";
$commonStreet = "2000 South Mills Avenue";
$commonSuburb = "Orlando";
$commonState = "Florida";
$commonZip = "32806";

?>
<style>
	#map {
		height: 100%;
	}
	html, body {
		height: 100%;
		margin: 0;
		padding: 0;
	}
	#floating-panel {
		position: absolute;
		top: 10px;
		left: 25%;
		z-index: 5;
		background-color: #fff;
		padding: 5px;
		border: 1px solid #999;
		text-align: center;
		font-family: 'Roboto','sans-serif';
		line-height: 30px;
		padding-left: 10px;
	}
	#map-canvas, #map-canvas-site, #static-map-canvas {
		height: 150px;
		margin: 0;
	}
	.centerMarker, .siteCenterMarker{
		position: absolute;
		background: url(<?= Url::base(true)."/image/mapIcon.png" ?>) no-repeat;
		top: 54%;
		left: 50%;
		z-index: 1;
		margin-left: -10px;
		margin-top: -34px;
		height: 34px;
		width: 20px;
		cursor: pointer;
	}
	.pac-container {
		z-index: 1050 !important;
	}
	.existing_client_list{
		max-height: 190px;
		overflow-y: scroll;
	}
	.existing_client_list li{
		cursor: pointer;
	}
	ul.existing_client_list img {
		height: 45px;
		width: 45px;
		border-radius: 50%;
		margin-right: 10px;
	}
	#client_image img {
		height: 60px;
		width: 60px;
		border-radius: 50%;
	}
	#google-map-overlay{
		height : 150px;
		width: 270px;
		background: transparent;
		position: absolute;
		top: 0px; 
		left: 0px; 
		z-index: 99;
	}
	#map-canvas-site-overlay {
		height : 150px;
		width: 270px;
		background: transparent;
		position: absolute;
		top: 0px; 
		left: 0px; 
		z-index: 0;
	}
	/*=== Naveen ====*/
	#InteriorQuote .modal-content {
		background: #f6fbff;
	}
	#InteriorQuote .modal-body {
		padding: 0px 0 0;
		background: #fff;
		margin-top: 30px;
	}
	.Detail_prt_div {
		width: 100%;
		margin-top: -18px;
	}
	.Detail_inner .nav.nav-tabs a.nav-link {
		padding: 3px 6px;
		display: inline-block;
	} 
	.Detail_inner .nav.nav-tabs {
		float: left;
		width: 100%;
	}
	.quote_right_sec{
		height: 634px;
		overflow: auto;
		display: block;
	}
</style>
<div class="container">

    <div class="quote_detail_inner">
	<?php
		if(count($quotes) > 0){
	?>
				<!-- Nav tabs -->
				<ul class="nav nav-tabs nav-justified">
					<li class="nav-item">
						<a class="nav-link active checkChange" data-toggle="tab" href="#panel1" role="tab">Quote Detail</a>
					</li>
					<li class="nav-item">
						<a class="nav-link checkChange" data-toggle="tab" href="#panel2" role="tab">Paint Defaults</a>
					</li>
					<li class="nav-item">
						<a class="nav-link checkChange" data-toggle="tab" href="#panel4" role="tab">Rooms</a>
					</li>
					<li class="nav-item">
						<a class="nav-link checkChange" data-toggle="tab" href="#panel5" role="tab">Summary</a>
					</li>
					<li class="nav-item">
						<a class="nav-link checkChange" data-toggle="tab" href="#panel6" role="tab">Communication</a>
					</li>
					<li class="nav-item">
						<a class="nav-link checkChange" data-toggle="tab" href="#panel7" role="tab">Site Notes</a>
					</li>
				</ul>
				<!-- Tab panels -->
				<div class="tab-content card">
					<!--Panel 1-->
					<input type="hidden" id="quote_type" value="<?= $quotes['type'] ?>" />
					<input type="hidden" name="quote_id" id="quote_id" value="<?= $quotes['quote_id'] ?>">
					<input type="hidden" name="tier_id" id="tier_id">
					<input type="hidden" id="ifPaintDefaultChanged" value="0" />
					<!-- <input type="hidden" id="ifPaintDefaultChanged" value="0" /> -->
					<div class="tab-pane fade in show active" id="panel1" role="tabpanel">
						<div class="quote_left_fix text-center">
							<div class="client_profile_detail">
								<span class="clientImg"><img src="<?= $siteURL ;?>/image/profile.png" alt=""></span>
								<span class="clientName"><?= $quotes['contact']['name']; ?></span>
								<span class="client_contact"><?= $quotes['contact']['email']; ?><br> <?= $quotes['contact']['phone']; ?></span>
							</div><!-- client_profile_detail -->
							<div class="quote_count_detail">
								<div class="quote_count email"><span><?= $quotes['counts']['jss']; ?></span> <img src="<?= $siteURL ;?>/image/mail.png" alt=""></div>
								<div class="quote_count invoice"><span><?= $quotes['counts']['contacts']; ?></span> <img src="<?= $siteURL ;?>/image/detail.png" alt=""></div>
								<div class="quote_count quote"><span><?= $quotes['counts']['quotes']; ?></span> <img src="<?= $siteURL ;?>/image/quote.png" alt=""></div>
								<div class="quote_count addquote"><span><?= $quotes['counts']['docs']; ?></span> <img src="<?= $siteURL ;?>/image/calender_new.png" alt=""></div>
							</div><!-- quote_count_detail -->
						</div><!-- quote_left_fix -->
						<div class="quote_right_sec">
							<div class="text-right top_icon">
								<span class="edit"><a href="javascript:void(0)" data-toggle="modal" data-target="#InteriorQuote" data-dismiss="modal"><img src="<?= $siteURL ?>/image/edit_new.png" alt=""></a></span>
								<span class="add_qt"><img src="<?= $siteURL ?>/image/add_quote.png" alt=""></span>
								<span class="copy"><img src="<?= $siteURL ?>/image/copy_new.png" alt=""></span>
							</div><!-- text-right -->
							<div class="Site_address_div">
								<h3 class="site-head">Site Address</h3>
								<p><?= $quotes['site']['street1']; ?></p>
								<div class="static-add">
									<span><img src="<?= $siteURL ?>/image/mapquote.png" alt=""></span>
									<span class="map-pin"><img src="<?= $siteURL ?>/image/location.png" alt=""></span>
								</div><!-- map-add -->
							</div><!-- site_address_div -->
							<div class="Site_contact_div">
								<h3 class="site-head">Site Contact</h3>
								<div class="site-contact-detail">
									<p><?= $quotes['site']['name']; ?></p>
									<p><?= $quotes['site']['email']; ?></p>
									<p><?= $quotes['site']['phone']; ?></p>
									<div class="site_status">
										<form action="status-update" id="quote_status" >
											<label>Update Status</label>
											<select name="update_status" class=" data-style="btn-new" tabindex="-98">
												<option>Pending</option>
												<option>In-Progress</option>
												<option>Completed</option>
												<option>Accepted</option>
												<option>Declined</option>
												<option>New</option>
												<option>Offered</option>
												<option>Open</option>
											</select>
											<button type="button" class="submit_btn btn" name="Submit">Submit</button>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!--/.Panel 1-->
					<!--Panel 2-->
					<div class="tab-pane fade" id="panel2" role="tabpanel">
						<!-- <input type="hidden" name="quote_id" id="quote_id">
						<input type="hidden" name="quote_id" id="quote_id">
						<input type="hidden" name="quote_id" id="quote_id">
						<input type="hidden" name="quote_id" id="quote_id"> -->
						<form id="paintDefaultForm">
							<input type="hidden" name="quote_type" value="<?= $quotes['type'] ?>" />
							<input type="hidden" name="quote_id" value="<?= $quotes['quote_id'] ?>">
							<div class="quote_left_fix text-center">
								<div class="paint_lft_form">
									<div class="form-group">
										<label>Brand</label>
										<select name="defaultBrand" id="selbrand" class="selbrand changeType" data-changetype="2" data-style="btn-new" tabindex="-98">
											<option value="">Select Brand</option>
										<?php 
											$ifFirst = 1;
											foreach ($brands as $key => $bv) {
												if($ifFirst==1){
													$defaultBrand = $bv->brand_id;
													$ifFirst = 0;
												}
												echo '<option data-logo="'.$bv->logo.'" value="'.$bv->brand_id.'">'.$bv->name.'</option>';
											}
										?>
										</select>
										<input type="hidden" id="defaultBrand" value="<?= $defaultBrand ?>">
									</div>
									<div class="form-group">
										<label>Quality / Tier</label>
										<select name="defaultTier" id="seltiers" class="seltiers changeType" data-changetype="2" data-style="btn-new" tabindex="-98">
											<option value="">Select Quality / Tier</option>
										 <?php 
											foreach ($brands as $key => $bv) {
												if(count($bv->tblTiers)){
													$tiers = $bv->tblTiers;
													foreach ($tiers as $key => $tv) {
														echo '<option data-brand='.$tv->brand_id.' class="tb_'.$tv->brand_id.' quote_tiers" value="'.$tv->tier_id.'">'.$tv->name.'</option>';
													}
												}
											}
										?>
										</select>
									</div>
									<div class="form-group">
										<label>Coats</label>
										<select name="defaultCoat" class="brand changeType" data-style="btn-new" data-changetype="2" tabindex="-98">
											<option value="">Select Coats</option>
											<option value="1">One</option>
											<option value="2">Two</option>
											<option value="3">Three</option>
											<option value="4">Four</option>
										</select>
									</div>
									<div class="form-group">
										<label>Prep. Level</label>
										<select name="defaultPrepLevel" class="brand changeType" data-style="btn-new" data-changetype="2" tabindex="-98">
											<option value="">Select Prep. Level</option>
											<option value="5">Test</option>
											<option value="10">Premium</option>
										</select>
									</div>
									<div class="form-group checkbox_div">
										<label class="checkbox_label">Undercoat
										  <input type="checkbox" name="defaultUnderCoat" id="underCoatMain" class="changeType" data-changetype="2" >
										  <span class="checkmark"></span>
										</label>
									</div>
									<div class="form-group checkbox_div">
										<label class="checkbox_label">Colour Consultant Required
										  <input type="checkbox" name="defaultColorConsultant" class="changeType" data-changetype="2" >
										  <span class="checkmark"></span>
										</label>
									</div>
									<div class="tire_img"><img src="<?= $siteURL ?>/image/dulux.png" alt=""></div><!-- tire_img -->
								</div>
							</div>
							<div class="quote_right_sec mCustomScrollbar" id="paintDefaultMain" style="display: none">
								<!-- <div class="job_default_details">
									<h3 class="default_heading"><span><a href="javascript:void(0)" data-toggle="modal" data-target="#colorCeleings" data-dismiss="modal"><img src="<?= $siteURL ?>/image/default_search.png" alt=""></a></span> Ceilings</h3>
									<div class="fill_default_form">
										<div class="form-group small">
											<input type="text" value="" placeholder="Colour Name" class="form-control colour-name">
										</div>
										<div class="form-group small">
											<select name="update_status" class="sheen" data-style="btn-new" tabindex="-98">
											<?php 
												/*foreach ($sheens as $key => $sh) {
													echo '<option class="quote_sheen" value="'.$sh->sheen_id.'">'.$sh->name.'</option>';
												}*/
											?>
											</select>
										</div>
										<div class="form-group long">
											<select name="update_status" class="Product" data-style="btn-new" tabindex="-98">
											<?php 
												/*foreach ($products as $key => $pr) {
													echo '<option class="quote_product" value="'.$pr->product_id.'">'.$pr->name.'</option>';
												}*/
											?>
											</select>
										</div>
									</div>
									<div class="fill_default_form">
										<div class="form-group small">
											<select name="update_status" class="strength" data-style="btn-new" tabindex="-98">
											<?php 
												/*foreach ($strengths as $key => $st) {
													echo '<option class="quote_product" value="'.$st->strength_id.'">'.$st->name.'</option>';
												}*/
											?>
											</select>
										</div>
										<div class="form-group small">
											<div class="form-group checkbox_div">
												<label class="checkbox_label">Undercoat
												  <input type="checkbox">
												  <span class="checkmark"></span>
												</label>
											</div>
										</div>
										<div class="form-group long">
											<select name="update_status" class="undercoat" data-style="btn-new" tabindex="-98">
											  <option>Select Undercoat</option>
											  <option>DLX CEILING WHITE</option>
											  <option>DLX LIGHT&SPACE CEILING FLAT</option>
											  <option>DLX W&W KIT & BATH CEILING</option>
											</select>
										</div>
									</div>
								</div>
								<div class="job_default_details">
									<h3 class="default_heading"><span><img src="<?= $siteURL ?>/image/default_search.png" alt=""></span> Cornices</h3>
									<div class="fill_default_form">
										<div class="form-group small">
											<input type="text" value="" placeholder="Colour Name" class="form-control colour-name">
										</div>
										<div class="form-group small">
											<select name="update_status" class="sheen" data-style="btn-new" tabindex="-98">
											<?php 
												/*foreach ($sheens as $key => $sh) {
													echo '<option class="quote_sheen" value="'.$sh->sheen_id.'">'.$sh->name.'</option>';
												}*/
											?>
											</select>
										</div>
										<div class="form-group long">
											<select name="update_status" class="Product" data-style="btn-new" tabindex="-98">
											<?php 
												/*foreach ($products as $key => $pr) {
													echo '<option class="quote_product" value="'.$pr->product_id.'">'.$pr->name.'</option>';
												}*/
											?>
											</select>
										</div>
									</div>
									<div class="fill_default_form">
										<div class="form-group small">
											<select name="update_status" class="strength" data-style="btn-new" tabindex="-98">
											<?php 
												/*foreach ($strengths as $key => $st) {
													echo '<option class="quote_product" value="'.$st->strength_id.'">'.$st->name.'</option>';
												}*/
											?>
											</select>
										</div>
										<div class="form-group small">
											<div class="form-group checkbox_div">
												<label class="checkbox_label">Undercoat
												  <input type="checkbox">
												  <span class="checkmark"></span>
												</label>
											</div>
										</div>
										<div class="form-group long">
											<select name="update_status" class="undercoat" data-style="btn-new" tabindex="-98">
											  <option>Select Undercoat</option>
											  <option>DLX CEILING WHITE</option>
											  <option>DLX LIGHT&SPACE CEILING FLAT</option>
											  <option>DLX W&W KIT & BATH CEILING</option>
											</select>
										</div>
									</div>
								</div>
								<div class="job_default_details">
									<h3 class="default_heading"><span><img src="<?= $siteURL ?>/image/default_search.png" alt=""></span> Doors</h3>
									<div class="fill_default_form">
										<div class="form-group small">
											<input type="text" value="" placeholder="Colour Name" class="form-control colour-name">
										</div>
										<div class="form-group small">
											<select name="update_status" class="sheen" data-style="btn-new" tabindex="-98">
											<?php 
												/*foreach ($sheens as $key => $sh) {
													echo '<option class="quote_sheen" value="'.$sh->sheen_id.'">'.$sh->name.'</option>';
												}*/
											?>
											</select>
										</div>
										<div class="form-group long">
											<select name="update_status" class="Product" data-style="btn-new" tabindex="-98">
											<?php 
												/*foreach ($products as $key => $pr) {
													echo '<option class="quote_product" value="'.$pr->product_id.'">'.$pr->name.'</option>';
												}*/
											?>
											</select>
										</div>
									</div>
									<div class="fill_default_form">
										<div class="form-group small">
											<select name="update_status" class="strength" data-style="btn-new" tabindex="-98">
											<?php 
												/*foreach ($strengths as $key => $st) {
													echo '<option class="quote_product" value="'.$st->strength_id.'">'.$st->name.'</option>';
												}*/
											?>
											</select>
										</div>
										<div class="form-group small">
											<div class="form-group checkbox_div">
												<label class="checkbox_label">Undercoat
												  <input type="checkbox">
												  <span class="checkmark"></span>
												</label>
											</div>
										</div>
										<div class="form-group long">
											<select name="update_status" class="undercoat" data-style="btn-new" tabindex="-98">
											  <option>Select Undercoat</option>
											  <option>DLX CEILING WHITE</option>
											  <option>DLX LIGHT&SPACE CEILING FLAT</option>
											  <option>DLX W&W KIT & BATH CEILING</option>
											</select>
										</div>
									</div>
								</div>
								<div class="job_default_details">
									<h3 class="default_heading"><span><img src="<?= $siteURL ?>/image/default_search.png" alt=""></span> Trim: Windows, Skirting, Door Frames, Rails</h3>
									<div class="fill_default_form">
										<div class="form-group small">
											<input type="text" value="" placeholder="Colour Name" class="form-control colour-name">
										</div>
										<div class="form-group small">
											<select name="update_status" class="sheen" data-style="btn-new" tabindex="-98">
											<?php 
												/*foreach ($sheens as $key => $sh) {
													echo '<option class="quote_sheen" value="'.$sh->sheen_id.'">'.$sh->name.'</option>';
												}*/
											?>
											</select>
										</div>
										<div class="form-group long">
											<select name="update_status" class="Product" data-style="btn-new" tabindex="-98">
											<?php 
												/*foreach ($products as $key => $pr) {
													echo '<option class="quote_product" value="'.$pr->product_id.'">'.$pr->name.'</option>';
												}*/
											?>
											</select>
										</div>
									</div>
									<div class="fill_default_form">
										<div class="form-group small">
											<select name="update_status" class="strength" data-style="btn-new" tabindex="-98">
											<?php 
												/*foreach ($strengths as $key => $st) {
													echo '<option class="quote_product" value="'.$st->strength_id.'">'.$st->name.'</option>';
												}*/
											?>
											</select>
										</div>
										<div class="form-group small">
											<div class="form-group checkbox_div">
												<label class="checkbox_label">Undercoat
												  <input type="checkbox">
												  <span class="checkmark"></span>
												</label>
											</div>
										</div>
										<div class="form-group long">
											<select name="update_status" class="undercoat" data-style="btn-new" tabindex="-98">
											  <option>Select Undercoat</option>
											  <option>DLX CEILING WHITE</option>
											  <option>DLX LIGHT&SPACE CEILING FLAT</option>
											  <option>DLX W&W KIT & BATH CEILING</option>
											</select>
										</div>
									</div>
								</div>
								<div class="job_default_details">
									<h3 class="default_heading"><span><img src="<?= $siteURL ?>/image/default_search.png" alt=""></span> Walls</h3>
									<div class="fill_default_form">
										<div class="form-group small">
											<input type="text" value="" placeholder="Colour Name" class="form-control colour-name">
										</div>
										<div class="form-group small">
											<select name="update_status" class="sheen" data-style="btn-new" tabindex="-98">
											<?php 
												/*foreach ($sheens as $key => $sh) {
													echo '<option class="quote_sheen" value="'.$sh->sheen_id.'">'.$sh->name.'</option>';
												}*/
											?>
											</select>
										</div>
										<div class="form-group long">
											<select name="update_status" class="Product" data-style="btn-new" tabindex="-98">
											<?php 
												/*foreach ($products as $key => $pr) {
													echo '<option class="quote_product" value="'.$pr->product_id.'">'.$pr->name.'</option>';
												}*/
											?>
											</select>
										</div>
									</div>
									<div class="fill_default_form">
										<div class="form-group small">
											<select name="update_status" class="strength" data-style="btn-new" tabindex="-98">
											<?php 
												/*foreach ($strengths as $key => $st) {
													echo '<option class="quote_product" value="'.$st->strength_id.'">'.$st->name.'</option>';
												}*/
											?>
											</select>
										</div>
										<div class="form-group small">
											<div class="form-group checkbox_div">
												<label class="checkbox_label">Undercoat
												  <input type="checkbox">
												  <span class="checkmark"></span>
												</label>
											</div>
										</div>
										<div class="form-group long">
											<select name="update_status" class="undercoat" data-style="btn-new" tabindex="-98">
											  <option>Select Undercoat</option>
											  <option>DLX CEILING WHITE</option>
											  <option>DLX LIGHT&SPACE CEILING FLAT</option>
											  <option>DLX W&W KIT & BATH CEILING</option>
											</select>
										</div>
									</div>
								</div>
								<div class="job_default_details">
									<h3 class="default_heading"><span><img src="<?= $siteURL ?>/image/default_search.png" alt=""></span> Window Surrounds & Sills</h3>
									<div class="fill_default_form">
										<div class="form-group small">
											<input type="text" value="" placeholder="Colour Name" class="form-control colour-name">
										</div>
										<div class="form-group small">
											<select name="update_status" class="sheen" data-style="btn-new" tabindex="-98">
											<?php 
												/*foreach ($sheens as $key => $sh) {
													echo '<option class="quote_sheen" value="'.$sh->sheen_id.'">'.$sh->name.'</option>';
												}*/
											?>
											</select>
										</div>
										<div class="form-group long">
											<select name="update_status" class="Product" data-style="btn-new" tabindex="-98">
											<?php 
												/*foreach ($products as $key => $pr) {
													echo '<option class="quote_product" value="'.$pr->product_id.'">'.$pr->name.'</option>';
												}*/
											?>
											</select>
										</div>
									</div>
									<div class="fill_default_form">
										<div class="form-group small">
											<select name="update_status" class="strength" data-style="btn-new" tabindex="-98">
											<?php 
												/*foreach ($strengths as $key => $st) {
													echo '<option class="quote_product" value="'.$st->strength_id.'">'.$st->name.'</option>';
												}*/
											?>
											</select>
										</div>
										<div class="form-group small">
											<div class="form-group checkbox_div">
												<label class="checkbox_label">Undercoat
												  <input type="checkbox">
												  <span class="checkmark"></span>
												</label>
											</div>
										</div>
										<div class="form-group long">
											<select name="update_status" class="undercoat" data-style="btn-new" tabindex="-98">
											  <option>Select Undercoat</option>
											  <option>DLX CEILING WHITE</option>
											  <option>DLX LIGHT&SPACE CEILING FLAT</option>
											  <option>DLX W&W KIT & BATH CEILING</option>
											</select>
										</div>
									</div>
								</div> -->
							</div>
							<div class="quote_right_sec" id="blankDefaultPane" >
								<h3>You have not choosen the Paint Defaults</h3>
								<h5>Please choose the Paint Defaults in the left panel</h5>
							</div>
						</form>
					</div>
					<!--/.Panel 2--> 
					<div class="tab-pane fade" id="panel4" role="tabpanel">
						<div class="quote_left_fix text-center">
							<div class="paint_lft_form">
								<h4>Rooms</h4>
								<ul class="rooms_type">
									<li class="active"><a href="#"><img src="<?= $siteURL ?>/image/bedroom.png" alt=""> <span class="count_no">2</span></a></li>
									<li class=""><a href="#"><img src="<?= $siteURL ?>/image/bathroom.png" alt=""> <span class="count_no">2</span></a></li>
									<li class=""><a href="#"><img src="<?= $siteURL ?>/image/dinningroome.png" alt=""> <span class="count_no">1</span></a></li>
									<li class=""><a href="#"><img src="<?= $siteURL ?>/image/entrance.png" alt=""> <span class="count_no">1</span></a></li>
								</ul><!-- rooms_type -->
								<div class="bottom_buttons">
									<button type="button" class="btn apply_btn">Apply</button>
									<button type="button" class="btn reset_btn">Reset</button>
								</div><!--bottom_buttons-->
							</div><!-- paint_lft_form -->
						</div><!-- quote_left_fix -->
						<div class="quote_right_sec">
							<a class="add_room_main" href="#" data-toggle="modal" data-target="#addRooms"><img src="<?= $siteURL ?>/image/add_room.png" alt=""></a>
							<div class="rooms_tab">
								<div class="left_tab_icons"><img src="<?= $siteURL ?>/image/bedroom_white.png" alt=""></div><!-- left_tab_icons -->
								<div class="right_tab_contentDetail">
									<div class="topRightTab">
										<h3>
											Marty's Bedroom
											<span class="CountClass"><small>2</small> Rooms</span>
										</h3>
										<ul class="edit_dlt_list">
											<li><a href="javascript:void(0);" ><img src="<?= $siteURL ?>/image/edit.png" alt=""></a></li>
											<li><a href="#"><img src="<?= $siteURL ?>/image/delete.png" alt=""></a></li>
										</ul><!-- edit_dlt_list -->
									</div><!-- topRightTab -->
									<ul class="rooms_tab_list">
										<li class="complete">
											<p>Ceilings</p>
											<span class="room_steps"></span>
										</li>
										<li class="complete">
											<p>Cornices</p>
											<span class="room_steps"></span>
										</li>
										<li class="current">
											<p>Doors</p>
											<span class="room_steps"></span>
										</li>
										<li>
											<p>Trims</p>
											<span class="room_steps"></span>
										</li>
										<li>
											<p>Walls</p>
											<span class="room_steps"></span>
										</li>
										<li>
											<p>Windows</p>
											<span class="room_steps"></span>
										</li>
									</ul><!-- rooms_tab_list -->
								</div><!-- right_tab_contentDetail -->
							</div><!-- rooms_tab -->
							<div class="rooms_tab">
								<div class="left_tab_icons"><img src="<?= $siteURL ?>/image/bedroom_white.png" alt=""></div><!-- left_tab_icons -->
								<div class="right_tab_contentDetail">
									<div class="topRightTab">
										<h3>
											Joshua Bedroom
											<span class="CountClass"><small>2</small> Rooms</span>
										</h3>
										<ul class="edit_dlt_list">
											<li><a href="#"><img src="<?= $siteURL ?>/image/edit.png" alt=""></a></li>
											<li><a href="#"><img src="<?= $siteURL ?>/image/delete.png" alt=""></a></li>
										</ul><!-- edit_dlt_list -->
									</div><!-- topRightTab -->
									<ul class="rooms_tab_list">
										<li class="complete">
											<p>Ceilings</p>
											<span class="room_steps"></span>
										</li>
										<li class="complete">
											<p>Cornices</p>
											<span class="room_steps"></span>
										</li>
										<li class="current">
											<p>Doors</p>
											<span class="room_steps"></span>
										</li>
										<li>
											<p>Trims</p>
											<span class="room_steps"></span>
										</li>
										<li>
											<p>Walls</p>
											<span class="room_steps"></span>
										</li>
										<li>
											<p>Windows</p>
											<span class="room_steps"></span>
										</li>
									</ul><!-- rooms_tab_list -->
								</div><!-- right_tab_contentDetail -->
							</div><!-- rooms_tab -->
							
						</div><!-- quote_right_sec -->
					</div><!--/.Panel 4-->
					<div class="tab-pane fade" id="panel5" role="tabpanel">
						<div class="quote_left_fix text-center">
							<div class="paint_lft_form">
								<div class="client_profile_detail text-center">
									<span class="clientImg"><img src="<?= $siteURL ?>/image/profile.png" alt=""></span>
									<span class="clientName">Adam Ericesson</span>
								</div>
								<div class="quote_inclusion">
									<div class="inclusion_heading">
										<h3>Quote Inclusion</h3>
										<a href="#" class="edit_inclusion"><img src="<?= $siteURL ?>/image/edit.png" alt=""></a>
									</div><!-- inclusion_heading -->
									<p>This quote includes the  paint and painting of:</p>
									<ul class="room_list">
										<li><p class="text-left">Dining Room</p><span class="text-right">1</span></li>
										<li><p class="text-left">Bathroom</p><span class="text-right">2</span></li>
									</ul><!-- room_list -->
									<p>Painted with NORGLASS  NORTHANE MARINE EPOXY quality paint, with premium preparation. Price fee includes all labour 
									and materials. </p>
									<div class="inclusion_pricing">
										<h3>Pricing<h3>
										<ul class="pricing_detail">
											<li>
												<p>Sub Total</p>
												<span>$2000.00</span>
											</li>
											<li>
												<p>GST</p>
												<span>$100.00</span>
											</li>
											<li>
												<p><strong>Total Inc. GST</strong></p>
												<span><strong>$2100.00</strong></span>
											</li>
										</ul><!-- pricing_detail -->
									</div><!-- inclusion_pricing -->
								</div><!-- quote_inclusion -->
							</div><!-- paint_lft_form -->
						</div><!-- quote_left_fix -->
						<div class="quote_right_sec">
							<nav class="add-room-tab">
							  <ul class="nav nav-tabs nav-justified">
								<li class="nav-item">
									<a class="nav-link active" data-toggle="tab" href="#summary_detail" role="tab">Details</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#summary_special-item" role="tab">Special Item</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#summary_quantities" role="tab">Quantites</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#summary_Note" role="tab">Note</a>
								</li>
							  </ul>
							</nav>
							<div class="tab-content card">
								<!--Panel 1-->
								<div class="tab-pane fade in show active" id="summary_detail" role="tabpanel">
									<div class="room_detail_form">
										<div class="row">
											<div class="col">
												<h3 class="default_heading">Payment Schedule <img src="<?= $siteURL ?>/image/add_pay.png" alt=""></h3>
												<table id="summary_detail_table" class="tabs_table table table-borderless table-condensed table-hover">
													<thead>
														<tr>
															<th>Description</th>
															<th>Amount($)</th>
															<th>Part Payment(%)</th>
															<th>Date</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>Deposit Amount</td>
															<td><input type="text" class="form-control payment-input" name="" Placeholder="2000.00"></td>
															<td><input type="text" class="form-control percent-input text-center" name="" Placeholder="25"></td>
															<td><input id="datepicker" placeholder="12/06/2018" class="form-control datepicker-input" /></td>
														</tr>
														<tr>
															<td>Deposit Amount</td>
															<td><input type="text" class="form-control payment-input" name="" Placeholder="800.00"></td>
															<td><input type="text" class="form-control percent-input text-center" name="" Placeholder="25"></td>
															<td><input id="datepicker1" placeholder="12/06/2018" class="form-control datepicker-input" /></td>
														</tr>
														<tr>
															<td><strong>Balance</strong></td>
															<td><p>4200.00</p></td>
															<td><p class="text-center">-</p></td>
															<td><input id="datepicker2" placeholder="12/06/2018" class="form-control datepicker-input" /></td>
														</tr>
													</tbody>
												</table>
											</div><!-- col -->
										</div><!-- row -->
										<div class="row Summary_row">
											<div class="col">
												<div class="form-group">
													<h3 class="default_heading">Dates</h3>
													<div class="painter_no">
														<label>Number of Painters</label>
														<input type="number" class="form-control painterNo" placeholder="1">
														<label>Est Commencement</label>
														<input id="datepicker3" placeholder="12/06/2018" class="form-control datepicker-input" />
													</div><!-- painter_no -->
													<div class="days_no">
														<label>Days to Complete</label>
														<p>2</p>
														<label>Est Completion</label>
														<input id="datepicker4" placeholder="12/06/2018" class="form-control datepicker-input" />
													</div><!-- days_no -->
												</div><!-- form-group -->
											</div><!-- col -->
											<div class="col">
												<div class="form-group">
													<h3 class="default_heading">Acceptance</h3>
													<div class="acceptance_div">
														<div class="form-group">
															<label>Authorised Person</label>
															<input  placeholder="" class="form-control authPerson" type="text" />
														</div><!-- form-group -->
														<div class="form-group">
															<label>Sign below to accept amount, <a href="#">terms & conditions</a></label>
															<textarea class="sign_box"></textarea>
														</div><!-- form-group -->
														<div class="form-group">
															<label>Confirm Per</label>
															<span>Email</span>
															<span>Signature</span>
															<span>Clear</span>
														</div><!-- form-group -->
													</div><!-- acceptance_div -->
												</div><!-- form-group -->
											</div><!-- col -->
										</div><!-- row -->
									</div><!-- room_detail_form -->
								</div><!-- Panel 1 -->
								<div class="tab-pane fade" id="summary_special-item" role="tabpanel">
									<div class="special_item_div">
										<div class="form-group">
											<select name="update_status" class="bedroom" data-style="btn-new" tabindex="-98">
											  <option>Select Item to Add</option>
											  <option>Color Consult</option>    
											  <option>Etch Concrete Floor</option>    
											  <option>Making Music While You Paint</option>    
											  <option>New Custom Item and Save to Library</option>    
											</select>
										</div><!-- form-group -->
										<p class="or_separator">or</p>
										<div class="form-group">
											<input type="text" class="form-control custom_tem_add" name="" placeholder="Add Custom Item">  
										</div><!-- form-group -->
									</div><!-- special_item_div -->
									<table id="special_item_list" class="tabs_table table table-borderless table-condensed table-hover">
										<thead>
											<tr>
												<th>Include</th>
												<th>Item</th>
												<th>Price Each(Inc.GST)</th>
												<th>Qty</th>
												<th>Price (Inc.GST)</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>
													<div class="table_checkbox">
														<label class="checkbox_label">
														  <input type="checkbox" checked="checked">
														  <span class="checkmark"></span>
														</label>
													</div><!-- checkbox_div -->
												</td>
												<td><input type="text" class="form-control item_input" name="" placeholder="" value="Dulux"> </td>
												<td>
													<div class="item_price">
														<label>$</label>
														<input type="number" value="" Placeholder="0" class="form-control number_item">
													</div><!-- item_price -->
												</td>
												<td><input type="number" value="" Placeholder="2" class="form-control number_item2"></td>
												<td>$2.00</td>
												<td><span class="cross_row">&times;</span></td>
											</tr>
											<tr>
												<td>
													<div class="table_checkbox">
														<label class="checkbox_label">
														  <input type="checkbox">
														  <span class="checkmark"></span>
														</label>
													</div><!-- checkbox_div -->
												</td>
												<td><input type="text" class="form-control item_input" name="" placeholder="" value="Dulux"> </td>
												<td>
													<div class="item_price">
														<label>$</label>
														<input type="number" value="" Placeholder="0" class="form-control number_item">
													</div><!-- item_price -->
												</td>
												<td><input type="number" value="" Placeholder="2" class="form-control number_item2"></td>
												<td></td>
												<td><span class="cross_row">&times;</span></td>
											</tr>
											<tr>
												<td>
													<div class="table_checkbox">
														<label class="checkbox_label">
														  <input type="checkbox" >
														  <span class="checkmark"></span>
														</label>
													</div><!-- checkbox_div -->
												</td>
												<td><input type="text" class="form-control item_input" name="" placeholder="" value="Dulux"> </td>
												<td>
													<div class="item_price">
														<label>$</label>
														<input type="number" value="" Placeholder="0" class="form-control number_item">
													</div><!-- item_price -->
												</td>
												<td><input type="number" value="" Placeholder="2" class="form-control number_item2"></td>
												<td></td>
												<td><span class="cross_row">&times;</span></td>
											</tr>
											<tr>
												<td>
													<div class="table_checkbox">
														<label class="checkbox_label">
														  <input type="checkbox" >
														  <span class="checkmark"></span>
														</label>
													</div><!-- checkbox_div -->
												</td>
												<td><input type="text" class="form-control item_input" name="" placeholder="" value="Dulux"> </td>
												<td>
													<div class="item_price">
														<label>$</label>
														<input type="number" value="" Placeholder="0" class="form-control number_item">
													</div><!-- item_price -->
												</td>
												<td><input type="number" value="" Placeholder="2" class="form-control number_item2"></td>
												<td></td>
												<td><span class="cross_row">&times;</span></td>
											</tr>
											<tr>
												<td>
													<div class="table_checkbox">
														<label class="checkbox_label">
														  <input type="checkbox">
														  <span class="checkmark"></span>
														</label>
													</div><!-- checkbox_div -->
												</td>
												<td><input type="text" class="form-control item_input" name="" placeholder="" value="Dulux"> </td>
												<td>
													<div class="item_price">
														<label>$</label>
														<input type="number" value="" Placeholder="0" class="form-control number_item">
													</div><!-- item_price -->
												</td>
												<td><input type="number" value="" Placeholder="2" class="form-control number_item2"></td>
												<td></td>
												<td><span class="cross_row">&times;</span></td>
											</tr>
											<tr>
												<td>
													<div class="table_checkbox">
														<label class="checkbox_label">
														  <input type="checkbox" >
														  <span class="checkmark"></span>
														</label>
													</div><!-- checkbox_div -->
												</td>
												<td><input type="text" class="form-control item_input" name="" placeholder="" value="Dulux"> </td>
												<td>
													<div class="item_price">
														<label>$</label>
														<input type="number" value="" Placeholder="0" class="form-control number_item">
													</div><!-- item_price -->
												</td>
												<td><input type="number" value="" Placeholder="2" class="form-control number_item2"></td>
												<td></td>
												<td><span class="cross_row">&times;</span></td>
											</tr>
										</tbody>
									</table>
								</div><!-- Panel 1 -->
								<div class="tab-pane fade" id="summary_quantities" role="tabpanel">
									<div class="quantites_tab_div">
										<div class="noteTopIcons">
											<a href="#"><img src="<?= $siteURL ?>/image/note_add.png" alt=""></a>
										</div>
										<nav class="quntites_Tabs">
										  <ul class="nav nav-tabs nav-justified">
											<li class="nav-item">
												<a class="nav-link active" data-toggle="tab" href="#bycomponent" role="tab">By Component</a>
											</li>
											<li class="nav-item">
												<a class="nav-link" data-toggle="tab" href="#byproduct" role="tab">By Product</a>
											</li>
										</ul>
										</nav>
										<div class="tab-content card">
											<!--Panel 1-->
											<div class="tab-pane fade in show active" id="bycomponent" role="tabpanel">
												<table id="bycomponent_table" class="tabs_table table table-borderless table-condensed table-hover">
													<thead>
														<tr>
															<th>Compnent</th>
															<th>Type</th>
															<th>Product</th>
															<th>Colour</th>
															<th>Hours</th>
															<th>Litres</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>Ceilings</td>
															<td>Standards</td>
															<td>DLX CEILING White</td>
															<td>Colorbond Surfmist Single</td>
															<td>1</td>
															<td>1.603</td>
														</tr>
														<tr>
															<td>Ceilings</td>
															<td>Standards</td>
															<td>DLX CEILING White</td>
															<td>Colorbond Surfmist Single</td>
															<td>1</td>
															<td>1.083</td>
														</tr>
														<tr>
															<td></td>
															<td></td>
															<td></td>
															<td><strong class="text-right">Total</strong></td>
															<td>2</td>
															<td>3.686</td>
														</tr>
													</tbody>
												</table>
												<table id="special_item_table" class="tabs_table table table-borderless table-condensed table-hover">
													<thead>
														<tr>
															<th>Special Item</th>
															<th>Qty</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>A Colour Consultant - Professional On-site Colour Consultation - 1hr - You will receive an 
															individual consultation to assist you select the right colours for you. Selections will be added to
															our Job Specification Sheet</td>
															<td>2</td>
														</tr>
														<tr>
															<td>Special item for Master Bedroom</td>
															<td>2</td>
														</tr>
													</tbody>
												</table>
											</div>
											<div class="tab-pane" id="bycomponent" role="tabpanel">
												
											</div>
										</div>	
									</div><!-- qunatites_tab_div -->
								</div><!-- Panel 1 -->
								<div class="tab-pane fade" id="summary_Note" role="tabpanel">
									<div class="note_div_main">
										<div class="noteTopIcons">
											<a href="#"><img src="<?= $siteURL ?>/image/note_add.png" alt=""></a>
											<a href="#"><img src="<?= $siteURL ?>/image/camera.png" alt=""></a>
										</div><!-- noteTopIcons -->
										<ul class="note_list">
											<li>
												<div class="row">
													<div class="col">
														<span class="note_img"><img src ="<?= $siteURL ?>/image/homeimg1.png" alt=""></span>
													</div><!-- col-->
													<div class="col-md-8">
														<p>Please make sure you don’t spill the paint on the flloor tiles</p>
														<small>18/05/2018 02:32 pm </small>
													</div><!-- col-md-8 -->
													<div class="col text-right">
														<a href="#" class="trash"><img src ="<?= $siteURL ?>/image/delete.png" alt=""></a>
													</div><!-- col -->
												</div><!-- row-->
											</li>
											<li>
												<div class="row">
													<div class="col">
														<span class="note_img"><img src ="<?= $siteURL ?>/image/homeimg2.png" alt=""></span>
													</div><!-- col-->
													<div class="col-md-8">
														<p>Please make sure you don’t spill the paint on the flloor tiles</p>
														<small>18/05/2018 02:32 pm </small>
													</div><!-- col-md-8 -->
													<div class="col text-right">
														<a href="#" class="trash"><img src ="<?= $siteURL ?>/image/delete.png" alt=""></a>
													</div><!-- col -->
												</div><!-- row-->
											</li>
											<li>
												<div class="row">
													<div class="col">
														<span class="note_img"><img src ="<?= $siteURL ?>/image/homeimg3.png" alt=""></span>
													</div><!-- col-->
													<div class="col-md-8">
														<p>Please make sure you don’t spill the paint on the flloor tiles</p>
														<small>18/05/2018 02:32 pm </small>
													</div><!-- col-md-8 -->
													<div class="col text-right">
														<a href="#" class="trash"><img src ="<?= $siteURL ?>/image/delete.png" alt=""></a>
													</div><!-- col -->
												</div><!-- row-->
											</li>
											<li>
												<div class="row">
													<div class="col">
														<span class="note_img"><img src ="<?= $siteURL ?>/image/homeimg4.png" alt=""></span>
													</div><!-- col-->
													<div class="col-md-8">
														<p>Please make sure you don’t spill the paint on the flloor tiles</p>
														<small>18/05/2018 02:32 pm </small>
													</div><!-- col-md-8 -->
													<div class="col text-right">
														<a href="#" class="trash"><img src ="<?= $siteURL ?>/image/delete.png" alt=""></a>
													</div><!-- col -->
												</div><!-- row-->
											</li>
											<li>
												<div class="row">
													<div class="col">
														<span class="note_img"><img src ="<?= $siteURL ?>/image/homeimg5.png" alt=""></span>
													</div><!-- col-->
													<div class="col-md-8">
														<p>Please make sure you don’t spill the paint on the flloor tiles</p>
														<small>18/05/2018 02:32 pm </small>
													</div><!-- col-md-8 -->
													<div class="col text-right">
														<a href="#" class="trash"><img src ="<?= $siteURL ?>/image/delete.png" alt=""></a>
													</div><!-- col -->
												</div><!-- row-->
											</li>
										</ul>
									</div><!-- note div main -->
								</div><!-- Panel 1 -->
							</div><!-- tab-content card -->
						</div><!-- quote_right_sec -->
					</div><!--/.Panel 5 -->
					<div class="tab-pane fade" id="panel6" role="tabpanel">
						<div class="quote_left_fix text-center">
							<div class="paint_lft_form">
								<div class="client_profile_detail text-center">
									<span class="clientImg"><img src="<?= $siteURL ?>/image/profile.png" alt=""></span>
									<span class="clientName">Adam Ericesson</span>
								</div>
								<div class="quote_inclusion">
									<div class="inclusion_heading">
										<h3>Quote Inclusion</h3>
										<a href="#" class="edit_inclusion"><img src="<?= $siteURL ?>/image/edit.png" alt=""></a>
									</div><!-- inclusion_heading -->
									<p>This quote includes the  paint and painting of:</p>
									<ul class="room_list">
										<li><p class="text-left">Dining Room</p><span class="text-right">1</span></li>
										<li><p class="text-left">Bathroom</p><span class="text-right">2</span></li>
									</ul><!-- room_list -->
									<p>Painted with NORGLASS  NORTHANE MARINE EPOXY quality paint, with premium preparation. Price fee includes all labour 
									and materials. </p>
									<div class="inclusion_pricing">
										<h3>Pricing<h3>
										<ul class="pricing_detail">
											<li>
												<p>Sub Total</p>
												<span>$2000.00</span>
											</li>
											<li>
												<p>GST</p>
												<span>$100.00</span>
											</li>
											<li>
												<p><strong>Total Inc. GST</strong></p>
												<span><strong>$2100.00</strong></span>
											</li>
										</ul><!-- pricing_detail -->
									</div><!-- inclusion_pricing -->
								</div><!-- quote_inclusion -->
							</div><!-- paint_lft_form -->
						</div><!-- quote_left_fix -->
						<div class="quote_right_sec">
							<div class="mail_box">
								<div class="mail_heading">03 May 2018</div><!-- mail_heading -->
								<div class="inner_mail_box">
									<h4 class="mail_sub">P1404 Professional Painting Quote</h4>
									<div class="form-group">
										<label>From</label>
										<p>David Esson</p>
									</div><!-- form-group -->
									<div class="form-group">
										<label>Date</label>
										<p>Wed Jan 24 2018 11:45</p>
									</div><!-- form-group -->
									<div class="form-group">
										<label>To</label>
										<p>info@paintpad.com.au</p>
									</div><!-- form-group -->
									<div class="form-group">
										<label>To</label>
										<p>info@paint.com.au, paintpad@gmail.com</p>
									</div><!-- form-group -->
									<div class="mail-content-box">
										<p>Bert Blogg hi,</p>
										<p>Great to meet with you,</p>
										<p>Thank you for the opportunity to provide you with our exceptional painting services.</p>
										<p>Please find attached our quotation as well as some supporting information about about our fine service you.</p>
										<a href="#" class="text-right">View More</a>
									</div><!-- mail-content-box -->
								</div><!-- inner_mail_box -->
							</div><!-- mail_box -->
							<div class="mail_box">
								<div class="mail_heading">03 May 2018</div><!-- mail_heading -->
								<div class="inner_mail_box">
									<h4 class="mail_sub">P1404 Professional Painting Quote</h4>
									<div class="form-group">
										<label>From</label>
										<p>David Esson</p>
									</div><!-- form-group -->
									<div class="form-group">
										<label>Date</label>
										<p>Wed Jan 24 2018 11:45</p>
									</div><!-- form-group -->
									<div class="form-group">
										<label>To</label>
										<p>info@paintpad.com.au</p>
									</div><!-- form-group -->
									<div class="form-group">
										<label>To</label>
										<p>info@paint.com.au, paintpad@gmail.com</p>
									</div><!-- form-group -->
									<div class="mail-content-box">
										<p>Bert Blogg hi,</p>
										<p>Great to meet with you,</p>
										<p>Thank you for the opportunity to provide you with our exceptional painting services.</p>
										<p>Please find attached our quotation as well as some supporting information about about our fine service you.</p>
										<a href="#" class="text-right">View More</a>
									</div><!-- mail-content-box -->
								</div><!-- inner_mail_box -->
							</div><!-- mail_box -->
						</div><!-- quote_right_sec -->
					</div><!-- panel16 -->
					<div class="tab-pane fade" id="panel7" role="tabpanel">
						<div class="quote_left_fix text-center">
							<div class="paint_lft_form">
								<div class="client_profile_detail text-center">
									<span class="clientImg"><img src="<?= $siteURL ?>/image/profile.png" alt=""></span>
									<span class="clientName">Adam Ericesson</span>
								</div>
								<div class="quote_inclusion">
									<div class="inclusion_heading">
										<h3>Quote Inclusion</h3>
										<a href="#" class="edit_inclusion"><img src="<?= $siteURL ?>/image/edit.png" alt=""></a>
									</div><!-- inclusion_heading -->
									<p>This quote includes the  paint and painting of:</p>
									<ul class="room_list">
										<li><p class="text-left">Dining Room</p><span class="text-right">1</span></li>
										<li><p class="text-left">Bathroom</p><span class="text-right">2</span></li>
									</ul><!-- room_list -->
									<p>Painted with NORGLASS  NORTHANE MARINE EPOXY quality paint, with premium preparation. Price fee includes all labour 
									and materials. </p>
									<div class="inclusion_pricing">
										<h3>Pricing<h3>
										<ul class="pricing_detail">
											<li>
												<p>Sub Total</p>
												<span>$2000.00</span>
											</li>
											<li>
												<p>GST</p>
												<span>$100.00</span>
											</li>
											<li>
												<p><strong>Total Inc. GST</strong></p>
												<span><strong>$2100.00</strong></span>
											</li>
										</ul><!-- pricing_detail -->
									</div><!-- inclusion_pricing -->
								</div><!-- quote_inclusion -->
							</div><!-- paint_lft_form -->
						</div><!-- quote_left_fix -->
						<div class="quote_right_sec">
							<div class="note_div_main">
								<div class="noteTopIcons">
									<a href="#"><img src="<?= $siteURL ?>/image/note_add.png" alt=""></a>
									<a href="#"><img src="<?= $siteURL ?>/image/camera.png" alt=""></a>
								</div><!-- noteTopIcons -->
								<ul class="note_list">
									<li>
										<div class="row">
											<div class="col">
												<span class="note_img"><img src ="<?= $siteURL ?>/image/homeimg1.png" alt=""></span>
											</div><!-- col-->
											<div class="col-md-8">
												<p>Please make sure you don’t spill the paint on the flloor tiles</p>
												<small>18/05/2018 02:32 pm </small>
											</div><!-- col-md-8 -->
											<div class="col text-right">
												<a href="#" class="trash"><img src ="<?= $siteURL ?>/image/delete.png" alt=""></a>
											</div><!-- col -->
										</div><!-- row-->
									</li>
									<li>
										<div class="row">
											<div class="col">
												<span class="note_img"><img src ="<?= $siteURL ?>/image/homeimg2.png" alt=""></span>
											</div><!-- col-->
											<div class="col-md-8">
												<p>Please make sure you don’t spill the paint on the flloor tiles</p>
												<small>18/05/2018 02:32 pm </small>
											</div><!-- col-md-8 -->
											<div class="col text-right">
												<a href="#" class="trash"><img src ="<?= $siteURL ?>/image/delete.png" alt=""></a>
											</div><!-- col -->
										</div><!-- row-->
									</li>
									<li>
										<div class="row">
											<div class="col">
												<span class="note_img"><img src ="<?= $siteURL ?>/image/homeimg3.png" alt=""></span>
											</div><!-- col-->
											<div class="col-md-8">
												<p>Please make sure you don’t spill the paint on the flloor tiles</p>
												<small>18/05/2018 02:32 pm </small>
											</div><!-- col-md-8 -->
											<div class="col text-right">
												<a href="#" class="trash"><img src ="<?= $siteURL ?>/image/delete.png" alt=""></a>
											</div><!-- col -->
										</div><!-- row-->
									</li>
									<li>
										<div class="row">
											<div class="col">
												<span class="note_img"><img src ="<?= $siteURL ?>/image/homeimg4.png" alt=""></span>
											</div><!-- col-->
											<div class="col-md-8">
												<p>Please make sure you don’t spill the paint on the flloor tiles</p>
												<small>18/05/2018 02:32 pm </small>
											</div><!-- col-md-8 -->
											<div class="col text-right">
												<a href="#" class="trash"><img src ="<?= $siteURL ?>/image/delete.png" alt=""></a>
											</div><!-- col -->
										</div><!-- row-->
									</li>
									<li>
										<div class="row">
											<div class="col">
												<span class="note_img"><img src ="<?= $siteURL ?>/image/homeimg5.png" alt=""></span>
											</div><!-- col-->
											<div class="col-md-8">
												<p>Please make sure you don’t spill the paint on the flloor tiles</p>
												<small>18/05/2018 02:32 pm </small>
											</div><!-- col-md-8 -->
											<div class="col text-right">
												<a href="#" class="trash"><img src ="<?= $siteURL ?>/image/delete.png" alt=""></a>
											</div><!-- col -->
										</div><!-- row-->
									</li>
								</ul>
							</div><!-- note div main -->
						</div><!-- quote_right_Sec -->
					</div><!-- panel17 -->
				</div><!-- tab-content-top -->
			</div><!-- quote_detail_inner -->

		<?php
		}
		?>

</div>

<div class="modal fade" id="InteriorQuote" tabindex="-1" role="dialog" aria-labelledby="modalLabelSmall" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header">
        
        <h3><img src="<?= Yii::$app->request->baseUrl ?>/image/newinteriorquote.png" alt=""><span class="quoteTypeText">New Interior Quote</span></h3>
        <div class="Interior_rht_headerIcons">
          <button type="button" id="saveQuote" data-savetype="update">
            <span aria-hidden="true"><img src="<?= Yii::$app->request->baseUrl ?>/image/tick@3x.png"></span>
          </button>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      </div>

      <div class="modal-body">
        <div class="container">
          <div class="row">
            <div class="Detail_prt_div text-center">
              <div class="Detail_inner">
                <ul class="nav nav-tabs" role="tablist">
                  <li class="nav-item"><a class="nav-link active" role="tab" data-toggle="tab" href="#home"><h3><span><img src="<?= Yii::$app->request->baseUrl ?>/image/client_blk.png" class="clientIcon"><img src="<?= Yii::$app->request->baseUrl ?>/image/client_detail.png" class="clientIcon icon1"></span> <span class="headingTxt">Client Details</span></h3></a></li>
                  <li class="nav-item"><a class="nav-link" role="tab" data-toggle="tab" href="#menu1"><h3><span><img src="<?= Yii::$app->request->baseUrl ?>/image/siteDetail_blk.png" class="clientIcon"><img src="<?= Yii::$app->request->baseUrl ?>/image/siteDetail.png" class="clientIcon icon1"></span> <span class="headingTxt">Site Details</span></h3></a></li>
                </ul>
              </div>
            </div>
            <div  class="tab-content">
              <div role="tabpanel" id="home" class="tab-pane in active">
                <div class="client_detail_form">
                  <div class="form-group">
                    <label>Quote Description</label>
                    <textarea class="description_input" id="description" placeholder="Please give your quote a brief description"></textarea>
                  </div>
                </div>
                <div class="new_exist_client">
                  <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item"><a class="nav-link active changeQuoteScreen" data-screen="new" role="tab" data-toggle="tab" href="#menu2"><h4>New Client</h4></a></li>
                    <li class="nav-item"><a class="nav-link changeQuoteScreen" data-screen="existing" role="tab" data-toggle="tab" href="#menu3"><h4>Existing Client</h4></a></li>
                  </ul>
                  <div class="tab-content">
                    <div role="tabpanel" id="menu2" class="tab-pane in active">
                      <div class="container">
                        <div class="row">
                          <div class="col">
                            <div class="form-group">
                              <input type="text" name="contactName" id="contactName" data-tochange="site_contactName" class="form-control name getChange" Placeholder="Contact name">
                            </div>
                          </div>
                          <div class="col">
                            <div class="form-group">
                              <input type="text" name="contactEmail" id="contactEmail" data-tochange="site_contactEmail" class="form-control email getChange" Placeholder="Contact email">
                            </div>
                          </div>
                          <div class="col">
                            <div class="form-group">
                              <input type="text" name="contactPhone" id="contactPhone" data-tochange="site_contactPhone" class="form-control phone getChange" Placeholder="Contact phone">
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-8">
                            <div class="form-group">
                              <input type="hidden" name="lat" id="lat" value="<?= $commonLat ?>">
                              <input type="hidden" name="lng" id="lng" value="<?= $commonLng ?>">
                              <input type="hidden" name="csi" id="csi" value="<?= Yii::$app->user->id; ?>">
                              <input type="hidden" name="contactCreateUrl" id="contactCreateUrl" value="<?= Url::toRoute(['webservice/create-contact'], true); ?>">
                              <input type="hidden" name="quoteCreateUrl" id="quoteCreateUrl" value="<?= Url::toRoute(['webservice/create-quote'], true); ?>">
                              <input type="hidden" name="quoteUpdateUrl" id="quoteUpdateUrl" value="<?= Url::toRoute(['webservice/update-quote'], true); ?>">
                              <input type="text" name="address" id="address" data-tochange="search_address" class="form-control Address getChange" Placeholder="Address" value="<?= $commonAddress ?>">
                            </div>
                            <div class="row">
                              <div class="col">
                                <div class="form-group">
                                  <input type="text" name="street" id="street" data-tochange="site_street" class="form-control Suburb getChange" Placeholder="Street" value="<?= $commonStreet ?>">
                                </div>
                              </div>
                              <div class="col">
                                <div class="form-group">
                                  <input type="text" name="suburb" id="suburb" data-tochange="site_suburb" class="form-control Suburb getChange" Placeholder="Suburb" value="<?= $commonSuburb ?>">
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-6">
                                <div class="form-group">
                                  <input type="text" name="state" id="state" data-tochange="site_state" class="form-control Suburb getChange" Placeholder="State" value="<?= $commonState ?>">
                                </div>
                              </div>
                              <div class="col-6">
                                <div class="form-group">
                                  <input type="text" name="zipCode" id="zipCode" data-tochange="site_zipCode" class="form-control ZipCode getChange" Placeholder="ZipCode" value="<?= $commonZip ?>">
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-4">
                            <div class="map_div">
                              <div id="map-canvas"></div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div role="tabpanel" id="menu3" class="tab-pane">
                      <div class="container">
                        <div class="row">
                          <div class="col">
                            <div class="form-group">
                              <input type="text" id="search_existing_client" name="Search" class="form-control search" placeholder="Search">
                            </div>
                            <ul class="existing_client_list">
                              <?php
                                $default_detail = [];
                                if(count($contacts['data']))
                                  if(array_key_exists(0, $contacts['data']['contact'])){
                                      $default_detail['name'] = $contacts['data']['contact'][0]['name'];
                                      $default_detail['phone'] = $contacts['data']['contact'][0]['phone'];
                                      $default_detail['email'] = $contacts['data']['contact'][0]['email'];
                                      $default_detail['image'] = $contacts['data']['contact'][0]['image'];
                                      $default_detail['formatted_addr'] = $contacts['data']['contact'][0]['address']['formatted_addr'];
                                      $default_detail['lat'] = $contacts['data']['contact'][0]['address']['lat'];
                                      $default_detail['lng'] = $contacts['data']['contact'][0]['address']['lng'];
                                  }
                                        
                                  foreach ($contacts['data']['contact'] as $key => $contact) {

                                      echo '<li class="contact_li" id="contact_'.$contact['contact_id'].'" data-id="'.$contact['contact_id'].'" data-name="'.$contact['name'].'" data-phone="'.$contact['phone'].'" data-image="'.$contact['image'].'" data-email="'.$contact['email'].'" data-street1="'.$contact['address']['street1'].'" data-street2="'.$contact['address']['street2'].'" data-suburb="'.$contact['address']['suburb'].'" data-state="'.$contact['address']['state'].'" data-postal="'.$contact['address']['postal'].'" data-country="'.$contact['address']['country'].'" data-formatted_addr="'.$contact['address']['formatted_addr'].'" data-lat="'.$contact['address']['lat'].'" data-lng="'.$contact['address']['lng'].'" >';
                                      echo '<img src="'.$contact['image'].'" />';
                                      echo '<div class="existing_client_detail"> <h3 class="client_name_list">'.$contact['name'].'</h3><small class="client_phone_list">'.$contact['phone'].'</small></div>';
                                      echo '</li>';
                                  }
                                  
                              ?>
                            </ul>
                          </div>
                          <div class="col border-leftright">
                            <div class="client_Detail text-center">
                              <input type="hidden" id="client_lat" value="<?= $default_detail['lat'] ?>">
                              <input type="hidden" id="client_lng" value="<?= $default_detail['lng'] ?>">
                              <input type="hidden" id="selected_client" value="">
                              <div id="client_image" class="profile_img"><img src="<?= $default_detail['image'] ?>" alt=""></div>
                              <div id="client_name" class="client_name"><?= $default_detail['name'] ?></div>
                              <div id="client_email" class="client_email"><?= $default_detail['email'] ?><br/><?= $default_detail['phone'] ?></div>
                            </div>
                          </div>
                          <div class="col address">
                            <h4>Address</h4>
                            <p id="client_address"><?= $default_detail['formatted_addr'] ?></p>
                            <div class="map_div">
                              <div id="static-map-canvas"></div>
                              <div id="google-map-overlay"></div>
                              <span class="map-pin"><img src="<?= Yii::$app->request->baseUrl ?>/image/location.png" alt=""></span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div role="tabpanel" id="menu1" class="tab-pane fade">
                <div class="container">
                  <div class="row existing_menu">
                    <div class="col-12">
                      <label class="clientDetail_Check">Clients details Same
                        <input type="checkbox" id="client_details_same" checked="checked">
                        <span class="checkmark"></span>
                      </label>
                    </div>
                    <div class="col-8 ">
                      <div class="row">
                        <div class="col">
                          <div class="form-group">
                            <input type="hidden" name="site_lat" id="site_lat" value="<?= $commonLat ?>">
                            <input type="hidden" name="site_lng" id="site_lng" value="<?= $commonLng ?>">
                            <input type="text" id="search_address" name="search_address" class="form-control search_address" Placeholder="Search Address" value="<?= $commonAddress ?>">
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col">
                          <div class="form-group">
                            <input type="text" id="site_street" name="site_street" class="form-control Street" Placeholder="Street" value="<?= $commonStreet ?>">
                          </div>
                        </div>
                        <div class="col">
                          <div class="form-group">
                            <input type="text" id="site_suburb" name="site_suburb" class="form-control Suburb" Placeholder="Suburb" value="<?= $commonSuburb ?>">
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-6">
                          <div class="form-group">
                            <input type="text" id="site_state" name="State" class="form-control Suburb" Placeholder="State" value="<?= $commonState ?>">
                          </div>
                        </div>
                        <div class="col-6">
                          <div class="form-group">
                            <input type="text" id="site_zipCode" name="Postcode" class="form-control Postcode" Placeholder="Postcode" value="<?= $commonZip ?>">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-4">
                      <div class="map_div">
                        <div id="map-canvas-site"></div>
                        <div id="map-canvas-site-overlay"></div>
                      </div>
                    </div>
                  </div>
                  <div class="row"><div class="col text-right"><button type="submit" class="clear btn-primary">Clear</button></div></div>
                  <div class="row existing_client_detail">
                    <div class="col">
                      <div class="form-group">
                        <input type="text" id="site_contactName" name="Name" class="form-control name" Placeholder="Contact name">
                      </div>
                    </div>
                    <div class="col">
                      <div class="form-group">
                        <input type="text" id="site_contactEmail" name="Email" class="form-control email" Placeholder="Contact email">
                      </div>
                    </div>
                    <div class="col">
                      <div class="form-group">
                        <input type="text" id="site_contactPhone" name="Name" class="form-control phone" Placeholder="Contact phone">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>


<div class="modal fade" id="colorCeleings" tabindex="-1" role="dialog" aria-labelledby="modalLabelSmall" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header">
        
        <h3><img src="<?= Yii::$app->request->baseUrl ?>/image/newinteriorquote.png" alt=""><span class="quoteTypeText">Select Color</span></h3>
        <div class="Interior_rht_headerIcons">
          <button type="button" id="saveQuote" data-savetype="update">
            <span aria-hidden="true"><img src="<?= Yii::$app->request->baseUrl ?>/image/tick@3x.png"></span>
          </button>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      </div>

      <div class="modal-body">
      	<div class="container">
			<?php
				$col=0;
				foreach ($colors as $key => $sc) {
					if($col == 0){
						echo '<div class="row">';
					}
						echo '<div class="col"><div class="upborder"><div style="height:200px;background-color:'.$sc->hex.'" data-hex="'.$sc->hex.'" data-name="'.$sc->name.'" data-tagid="'.$sc->tag_id.'"></div></div>'.$sc->name.'</div>';
					if($col == 3){
						echo '</div>';	
						$col = -1;
					}
					$col++;
				}
			?>
		</div>
      </div>
    </div>

  </div>
</div>
