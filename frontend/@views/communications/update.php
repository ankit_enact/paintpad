<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TblCommunications */

$this->title = 'Update Tbl Communications: ' . $model->comm_id;
$this->params['breadcrumbs'][] = ['label' => 'Tbl Communications', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->comm_id, 'url' => ['view', 'id' => $model->comm_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tbl-communications-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
