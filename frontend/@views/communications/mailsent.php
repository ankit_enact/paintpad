<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TblCommunications */

//$this->title = $model->comm_id;
/*$this->params['breadcrumbs'][] = ['label' => 'Tbl Communications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/

if(isset($head) && $head == 1){
  echo '';
}

?>

<style>
.mainClose{ display: none; } 
.text-center{ display: none; } 
.footer{ display: none; }


.tbl-communications-view h1{

    position: absolute;
  left: 50%;
  top: 50%;

  transform: translate(-50%, -50%);
}




.headingg{
    font-size: 36px;
    color: #0B9FE2;
    font-weight: bold;
    margin: auto;
    
}

.menu{ display: none; }
.mail_box .form-group label{
    text-align: inherit;
    width: 3% !important;
}    
.mail_box .form-group p {
    text-align: inherit;
}  
.groupp label{
    text-align: inherit;
    width: 5% !important;
    font-size: 14px;
}   

body {
    width: 100%;
    background-color: #ffffff;
    margin: 0;
    padding: 0;
    -webkit-font-smoothing: antialiased;
    mso-margin-top-alt: 0px;
    mso-margin-bottom-alt: 0px;
    mso-padding-alt: 0px 0px 0px 0px;
}

p,h1,h2,h3,h4 {
    margin-top: 0;
    margin-bottom: 0;
    padding-top: 0;
    padding-bottom: 0;
}
table{
    border-collapse: unset;
}

/* ----------- responsivity ----------- */

@media only screen and (max-width: 640px) {                     
    td, tr, table {
        width: 99% !important;
        float: left !important;
        margin: 0 !important;
    }
}

</style>

<div class="tbl-communications-view">
  <p><h1 class='headingg'>Your mail has been sent.</h1></p>
</div>
