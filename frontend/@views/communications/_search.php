<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TblCommunicationsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tbl-communications-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'comm_id') ?>

    <?= $form->field($model, 'contact_id') ?>

    <?= $form->field($model, 'subscriber_id') ?>

    <?= $form->field($model, 'quote_id') ?>

    <?= $form->field($model, 'receiver_email') ?>

    <?php // echo $form->field($model, 'newsletter_id') ?>

    <?php // echo $form->field($model, 'comm_from') ?>

    <?php // echo $form->field($model, 'date') ?>

    <?php // echo $form->field($model, 'comm_to') ?>

    <?php // echo $form->field($model, 'bcc') ?>

    <?php // echo $form->field($model, 'subject') ?>

    <?php // echo $form->field($model, 'body') ?>

    <?php // echo $form->field($model, 'attachment') ?>

    <?php // echo $form->field($model, 'receiver_name') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
