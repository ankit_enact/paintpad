<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TblCommunicationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tbl Communications';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tbl-communications-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tbl Communications', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'comm_id',
            'contact_id',
            'subscriber_id',
            'quote_id',
            'receiver_email:ntext',
            //'newsletter_id',
            //'comm_from',
            //'date',
            //'comm_to',
            //'bcc',
            //'subject',
            //'body:ntext',
            //'attachment',
            //'receiver_name',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
