<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\TblAppointmentType;
use app\models\TblMembers;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;

?>

<style>





.modal-backdrop.fade {
    opacity: 0.5;
}

</style>

<?php
 date_default_timezone_set('Asia/Kolkata');
$dat=[];
$dataSub=[];
//echo "<pre>";
//print_r($model);
$date= $model->date;
$dateShow= date("Y-m-d",$date);
$timeShowFrom = date("h:i A",$date);

$Totime=$model->duration;
$ftime=$date+$Totime;
$timeShowTo = date("h:i A",$ftime);
//  exit();
foreach($dataContact as $data)
{
  $dat[$data->contact_id]=$data->name;
  $datSub[$data->contact_id]=$data->subscriber_id;
}

?>
 <input type="hidden" id="calenderEventsIdUpdate" value='<?= json_encode($dateShow) ?>'>
<?php $form = ActiveForm::begin(['id' => 'myCalender2','options'=>['onsubmit'=>'return validateForm()']]); ?>
  <div class="NewEventForm">
     <div class="newEventLft_Form">
       <div class="form-group">
         <input type="hidden" class="form-control eventform_fields" name="TblAppointments[subscriber_id]" Placeholder="Customer Name" id="subsUPDATE"> 
         <?php  echo $form->field($model,"contact_id")->widget(Select2::classname(), [
                  'data' => $dat,                  
                  'options' => ['placeholder' =>'Customer Name','tabindex' => false,'class'=>'form-control eventform_fields','id'=>'userDataSelectUpdate'],
                  'pluginOptions' => [
                  'allowClear' => true,
                          
                  ],         
              ]);
          ?>
        </div><!-- form-control -->
        <div class="form-group">
            <div class="EventSelectBtns AppointSelect">
              <?= $form->field($model,'type')->dropDownList(ArrayHelper::map(TblAppointmentType::find()->all(),'id','name'),['prompt'=>'Appointment Type','class'=>'appointmentType','id'=>'appointmentTypeIDUpdate'])->label(false) ?>
              
            </div><!-- appointselect -->
            <div class="EventSelectBtns teamMember">
            
               <?= $form->field($model,'member_id')->dropDownList(ArrayHelper::map(TblMembers::find()->all(),'id',
                function($model2) {
                return $model2['f_name'].' '.$model2['l_name'];
                   }
              ),['prompt'=>'Team Member','class'=>'teamMemberSelect','id'=>'memberIDUpdate'])->label(false) ?>
            </div><!-- appointselect -->
        </div><!-- form-Group -->
        <div class="form-group">
          <div class="evnetFormLft_Calender">
               
                   <input type="hidden" name="calender" id="getDateUpdate" value="<?php echo $dateShow?>" >
                <div id="inlineDatepicker" class="cal2"></div>
          </div><!-- evnetFormLft_Calender -->

          <div class="EventSelectBtns timeSelect">
            <div class="form-group">
          
             
              <input type="text" name="dateSelect[From]" id="timepicker1Update" class="TimeSelect" placeholder="Time From" value="<?php echo $timeShowFrom;?>">

            </div><!-- form-group -->
            <div class="form-group">
            
              
               <input type="text" name="dateSelect[to]" id="timepicker2Update" class="TimeSelect" placeholder="Time To" value="<?php echo $timeShowTo;?>">
            </div><!-- form-group -->
            <div class="form-group">
              <label>All Day</label>
              <div class="RadioMainDiv">
                 
                 <?= $form->field($model, 'allDay')->radioList(array('1'=>'YES','0'=>'NO'))->label(false); ?>
              </div><!-- Radiomaindiv -->
            </div><!-- form-group -->
          </div><!-- EventSelectBtns -->
        </div><!-- form-group -->
        <div class="form-group">
        
         <?= $form->field($model, 'note')->textarea(['rows' => '6','class'=>'Notefield','placeholder'=>'NOTES','id'=>'noteIdUpdate' ])->label(false) ?>
        </div><!-- form-group -->
      </div><!-- mewEventlft_form -->
      <div class="newEvent_RhtForm">
        <div class="form-group">
           <?= $form->field($model, 'formatted_addr')->textInput(['class'=>'form-control eventform_fields location','placeholder'=>'LOCATIONS'])->label(false) ?>
           
        </div><!-- form-control -->
        <div class="Map_EventDiv">
        </div><!-- map_eventDiv -->
      </div><!-- newEvent_RhtForm -->
  </div><!-- neweventform -->
</div>
<div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success','id'=>'closeproduct','class'=>'btn btn-success pull-right']) ?>
    </div>
<?php ActiveForm::end(); ?> 
<?php
    $this->registerJs('
        var subsArray = '.json_encode($datSub).';
          console.log("subsArray-------->", subsArray);  
          console.log("userDataSelect------->", $("#userDataSelectUpdate").find(":selected").attr("value"));
          var selectedSubId = subsArray[$("#userDataSelectUpdate").find(":selected").attr("value")];
          console.log("selectedSubId------>", selectedSubId);
          $("#subsUPDATE").val(selectedSubId)
      ');

    $this->registerJs('
        var subsArray = '.json_encode($datSub).';
        console.log("subsArray-------->", subsArray);
        $(document).on("change","#userDataSelect", function() {
          console.log("userDataSelect------->", $("#userDataSelectUpdate").find(":selected").attr("value"));
          var selectedSubId = subsArray[$("#userDataSelectUpdate").find(":selected").attr("value")];
          console.log("selectedSubId------>", selectedSubId);
          $("#subsUPDATE").val(selectedSubId)
        })
      ');

?>


<script>
  $(function() {
    //$('#inlineDatepicker').datepick({onSelect: showDate,dateFormat: 'DD, MM d, yyyy'});
    // $(".day").click(function() {
    //   var date = $("#getDate").val();
    // }
    //console.log(<?php //echo $dateShow;?>);
    var dates = $("#calenderEventsIdUpdate").val();
    var eventArray = JSON.parse(dates);
    //console.log(eventArray);
    var event = [
        { date: eventArray }
     ];
    calendars.clndr2 = $('#inlineDatepicker').clndr({
        daysOfTheWeek: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        events: event,
        clickEvents: {
            click: function (target) {
                //console.log('Cal-1 clicked: ', target);
                var dateClicked = target.date._d;
              
                var dateObj = new Date(dateClicked)
                var month = dateObj.getMonth() + 1; //months from 1-12
                var day = dateObj.getDate();
                var year = dateObj.getFullYear();

                if (day.toString().length == 1) {
                  day = "0" + day;
                }
                if (month.toString().length == 1) {
                  month = "0" + month;
                }
                newdate = year + "-" + month + "-" + day;
                $('.day').removeClass('dateSelectedUpdate');
                $(".calendar-day-"+newdate).addClass("dateSelectedUpdate");
                $("#getDateUpdate").val(newdate);

            },
     
        },
        multiDayEvents: {
            singleDay: 'date',
            endDate: 'endDate',
            startDate: 'startDate'
        },
        showAdjacentMonths: true,
        adjacentDaysChangeMonth: false
    });
    
  });


</script>
<?php  Pjax::begin();
    $this->registerJsFile(Yii::getAlias('@web').'/frontend/web/js/timepicki.js',['depends' => [yii\web\JqueryAsset::className()]]);
    $this->registerJsFile(Yii::getAlias('@web').'/frontend/web/js/jquery.plugin.min.js',['depends' => [yii\web\JqueryAsset::className()]]);
    $this->registerJsFile(Yii::getAlias('@web').'/frontend/web/js/jquery.datepick.js',['depends' => [yii\web\JqueryAsset::className()]]);
    $this->registerCssFile(Yii::getAlias('@web').'/frontend/web/css/timepicki.css',['depends' => [yii\web\JqueryAsset::className()]]);
    $this->registerCssFile(Yii::getAlias('@web').'/frontend/web/css/clndr.css',['depends' => [yii\web\JqueryAsset::className()]]);
    $this->registerCssFile(Yii::getAlias('@web').'/frontend/web/css/jquery.datepick.css',['depends' => [yii\web\JqueryAsset::className()]]);
?>
<script>
$(document).ready(function(){
  $('#timepicker1Update').timepicki();
  $('#timepicker2Update').timepicki();
});  
</script>

<?php Pjax::end();?>
<script>
  $(document).ready(function(){
      var dates = $("#calenderEventsIdUpdate").val();
      var eventArray = JSON.parse(dates);
      //console.log(eventArray);
      var thisD = new Date(eventArray);
      calendars.clndr2.setMonth(thisD.getMonth());
      calendars.clndr2.setYear(thisD.getFullYear());
    })




  $(document).ready(function(){
      $(".event").addClass("dateSelectedUpdate");
       $(".event").removeClass("event");
  })
</script>





 
  