<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use app\models\TblContacts;
use app\models\TblAppointmentType;
/* @var $this yii\web\View */
/* @var $searchModel app\models\eventSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Events';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile(Yii::getAlias('@web').'/frontend/web/css/clndr.css',['depends' => [yii\web\JqueryAsset::className()]]);
?>
 

<style>

/* td.fc-other-month .fc-day-number {
        display: none;
        }*/
/*td.fc-other-month {
   visibility: hidden;
}*/
    </style>
<?php
//echo "<pre>";
$calenderEvents=[];
$cal=[];
$t=[];
foreach($appointment as $single)
{
    if(!in_array(date('Y-m-d',$single->date), $calenderEvents)){
        $calenderEvents[]['date']=date('Y-m-d',$single->date);
        
    }
    $cal[]=date('Y-m-d',$single->date);
    $t[]=date('Y-m-d H:i:s',$single->date);
}
//print_r($calenderEvents);
?>
<div class="container">
    <div class="CalenderMainPage">
          <div class="quote_left_fix text-center">
            <input type="hidden" id="calenderEventsId" value='<?= json_encode($calenderEvents) ?>'>
            <div class="cal1"></div>
            <div class="CalenderMainSearch">
              <div class="form-group">
                <input type="text" placeholder="Search" class="SearchEvent" id="search">
              </div>
              <div id="searchNote">
                  <ul class="SelectedEventlist">
                    <?php
                  
                        foreach($appointment as $value)
                        {
                            date_default_timezone_set('Asia/Kolkata');
                            $date =$value->date;
                            $time=$value->duration;
                            $ftime=$date+$time;
                            $newDateTimeFormat=date('Y-m-d',$date);
                            $newDateformat = date('d-M',$date);
                            $newTimeformat=date('H:i', $date);
                            $duration=date('H:i',$ftime);

                            echo  '<li id="tosearch" class="tosearch date'.$newDateTimeFormat.'">';
                            echo "<h3>".$newDateformat."(".$newTimeformat."- ".$duration.")</h3>";
                            $contact=$value->contact_id;
                            $contactName=TblContacts::find()->select('name')->where(['contact_id'=>$value->contact_id])->all();
                                foreach( $contactName as $name)
                                {
                                    
                                    echo '<span class="contactName" id="'.$value->contact_id.'">'.$name->name.'</span>';

                                }
                            $appointmentType=TblAppointmentType::find()->select('name')->where(['id'=>$value->type])->all();
                                foreach($appointmentType as $type)
                                {
                                    echo '<span>'.$type->name.'</span>';
                                }   
                            echo "</li>";                 
                       }
                    ?>
                    
                </ul>
          </div>
            </div><!--Calendermain search -->
          </div><!-- quote_left_fix text-center -->
        <div class="quote_right_sec">
            <div class="event-index">
            <?php Pjax::begin(['timeout' => 5000,'id'=>'eventpjax']); ?>
           <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
             <?php

                   Modal::begin([
                      'header'=>'Create Event',
                      'id'=>'modalevent',
                      'size'=>'modal-sl',

                    ]);
                      echo "<div id='modalEvent'></div>";
                      Modal::end();
                 ?>
             
               
                <?php //echo $this->render('/site/event/_searchevent', ['model' => $searchModel]); ?>

                <p>
                   <?php 
                    echo  Html::button('<img src="'.Yii::getAlias('@web').'/frontend/images/add_quote.png">', ['value'=>Url::toRoute('calendar/create',true),'class'=>'btn btn-primary pull-right' ,'id'=>'modalEventButton']);?>
                </p>

                <?= \yii2fullcalendar\yii2fullcalendar::widget(array(
                  //'events'=> $event,
                    'events' => Url::toRoute(['/calendar/events']),
                    'header'=>['right'=>'prev,next,today','left'=>'agendaDay,agendaWeek,month'],
                    'defaultView'=> 'agendaWeek',
                    'clientOptions' => [
                        'nowIndicator'=>true,
                        'eventColor'=> '#20b8ef',
                        'eventBorderColor'=> "#FFEB00",
                        //'scrollTime'=> '12:00',
                        ],

                    'eventClick' => "function(calEvent, jsEvent, view) {

                            $(this).css('border-color', 'red');

                            $.get('".Url::toRoute( ['/calendar/update'] ,true) ."',{'id':calEvent.id}, function(data){
                            $('#modalevent').modal('show')
                             .find('#modalEvent')
                             .html(data);
                                })
                            }",
                ));?>
               
            </div>
      </div>
  </div><!-- calenderMainPage-->
</div>

<?php 
    //$this->registerJsFile(Yii::getAlias('@web').'/frontend/web/js/jquery.js',['depends' => [yii\web\JqueryAsset::className()]]);
    $this->registerJsFile(Yii::getAlias('@web').'/frontend/web/js/underscore.js',['depends' => [yii\web\JqueryAsset::className()]]);
    $this->registerJsFile(Yii::getAlias('@web').'/frontend/web/js/moment.js',['depends' => [yii\web\JqueryAsset::className()]]);
    $this->registerJsFile(Yii::getAlias('@web').'/frontend/web/js/clndr.js',['depends' => [yii\web\JqueryAsset::className()]]);
    $this->registerJsFile(Yii::getAlias('@web').'/frontend/web/js/calendarInit.js',['depends' => [yii\web\JqueryAsset::className()]]);  
?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>


<?php Pjax::end();?>

<script>

    setTimeout(function() {
        $(".fc-today-button").click(function() {
            var tglCurrent = $('#w0').fullCalendar('getDate');
            var thisD = new Date(tglCurrent);
            calendars.clndr1.setMonth(thisD.getMonth());
            calendars.clndr1.setYear(thisD.getFullYear());
        });
    },1000);
    
 
    var event =<?php echo json_encode($cal);?>;
    $.each(event, function(key, value){
        $('.date'+value).click(function(){
           // console.log(value);
            $("#w0").fullCalendar( 'gotoDate',value) ;
            var thisD = new Date(value);
            calendars.clndr1.setMonth(thisD.getMonth());
            calendars.clndr1.setYear(thisD.getFullYear());
        });
    });


 // var t =<?php echo json_encode($t);?>;
 // $.each(t, function(key, value){
 //        //$('.date'+value).click(function(){
 //          //console.log(value);
            
 //            var thisD = new Date(value);
 //            var month = thisD.getMonth()+ 1;
 //            var day = thisD.getDate();
 //            var year = thisD.getFullYear();

 //            //console.log(date);
 //            if (month.toString().length == 1) {
 //                  month = "0" + month;
 //                }
 //            if (day.toString().length == 1) {
 //                  day = "0" + day;
 //                }    
 //            newdate = year + "-" + month + "-" + day;
 //            //console.log(newdate);
 //            var hours = thisD.getHours(); // => 9
 //            var min = thisD.getMinutes(); // =>  30
           
 //            if (hours.toString().length == 1) {
 //                  hours = "0" + hours;
 //                }
 //            if (min.toString().length == 1) {
 //                  min = "0" + min;
 //                }    
 //              newtime = hours + ":" + min;
 //              //console.log(newdate+"-------"+newtime);
 //              $('.date'+newdate).click(function(){
 //                  console.log(value);
 //                  $("#w0").fullCalendar( 'gotoDate',value) ;
 //                  $("#w0").fullCalendar('firstHour',newtime) ;
 //                  console.log(newtime);
 //                  var thisD = new Date(value);
 //                  calendars.clndr1.setMonth(thisD.getMonth());
 //                  calendars.clndr1.setYear(thisD.getFullYear());
 //              });
 //            // calendars.clndr1.setMonth(thisD.getMonth());
 //            // calendars.clndr1.setYear(thisD.getFullYear());
 //        //});
 //    });


    
    $(document).ready(function(){
        var tglCurrents = $('#w0').fullCalendar('getDate');
        var currentMonth=moment(tglCurrents).format('YYYY-MM');
        $(document).on('click','.fc-next-button, .fc-prev-button,.fc-today-button', function (){
            var tglCurrent = $('#w0').fullCalendar('getDate');
            var thisD = new Date(tglCurrent);
            calendars.clndr1.setMonth(thisD.getMonth());
            calendars.clndr1.setYear(thisD.getFullYear());        
        });
    });

  
$('.fc-today').html('<span class="todayDiv">Today</span>');


    $('#search').bind('keyup', function(e){
       var name=$("#search").val();
       var event=<?php echo json_encode($cal);?>;
       var datastring = {name:name,event:event};
       //console.log(datastring);
         $.ajax({
          url: window.location.origin + '/' + window.location.pathname.split ('/') [1] + "/calendar/searchfield",
          type: 'post',
          data: datastring,
          success: function (response) {
          //console.log(response);              
            $('#searchNote').empty();
            $('#searchNote').html(response);           
          },        
        }
     );
      e.stopImmediatePropagation();
     return false;
    });
</script>