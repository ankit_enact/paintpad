<?php

        $rooms      = '';
        $image      = '';
        $subtotal   = '';
        $gst        = '';
        $total      = '';
        $deposit    = '';
        $balance    = '';
        $start      = '';
        $completion = '';
        $includes   = '';

    if(isset($data) && count($data)>0){
        //echo '<pre>'; print_r($data);
        $scope      = $data['scope'];
        $rooms      = $data['rooms'];
        $brand_logo = $data['brand_logo'];
        $image      = $data['image'];
        $subtotal   = $data['subtotal'];
        $gst        = $data['gst'];
        $total      = $data['total'];
        $deposit    = $data['deposit'];
        $balance    = $data['balance'];
        $start      = $data['start'];
        $completion = $data['completion'];
        $includes   = $data['includes'];    
        $special    = $data['spl'];

        //echo '<pre>'; print_r($special); exit;

    } //data

    if(isset($cdata) && count($cdata)>0){
        $cname  = $cdata['cname'];
        $cemail = $cdata['cemail'];
        $cphone = $cdata['cphone'];
    } //cdata

    if(isset($qdata) && count($qdata)>0){
        $qdesc = $qdata['description'];
        $qid   = $qdata['number'];
        $qdate = $qdata['date'];
    } //qdata

?>

<style type="text/css">

#invoice_total table
{
    width:100%;
    border-top: 3px solid #ccc;
    border-spacing:0;
    border-collapse: collapse;      
    margin-top:5mm;
}

.brd-non{border-top: 0px solid #ccc !important;
    border-spacing:0 !important;
    border-collapse: inherit !important;  }

td, p, h1{font-family: Arial, Helvetica, sans-serif;}


</style>

<!-- main OUTER DIV -->

<div class="" style="margin:0 auto; width:1000px; overflow:hidden;">

<!-- scope div -->
<div id="invoice_body">
    <table style="width: 100%">
        <tr style="background:#0684c1; width: 100%">
            <td style=" padding: 10px; color:#fff; font-size: 13px">Job Scope</td>                
        </tr>    
        <tr style="width: 100%">
            <td style="background:#ececec;padding: 10px;font-weight: bold;font-size: 12px;color:#62615e"><?php echo $scope; ?></td>               
        </tr>   
    </table>
</div> <!-- scope div -->

<!-- service div -->
<div id="invoice_body">

<table>
        <tr>
            <td style="height: 10px"></td>
        </tr>
</table>


    <table style="width:100%">
        <tr style="">
            <td><h1 style="font-size:13px;font-weight:normal;color:#1d7db3;">PAINITING SERVICE INCLUDES</h1></td>
        </tr>
        <tr style="">
            <td  style="font-size:11px;color:#62615e;line-height: 20px;"><?php echo $includes;?></td>                
        </tr>
    </table>

<table>
        <tr>
            <td style="height: 10px"></td>
        </tr>
</table>

</div> <!-- service div -->

<!-- additional details div -->
<div id="" style="background:#ececec;">
<table>
        <tr>
            <td style="height: 10px"></td>
        </tr>
</table>
    <table style="width: 100%; padding: 0 10px">
        <tr style="">
            <td style=""><h1 style="font-size:13px;color:#1d7db3;font-weight:normal;font-style:normal;text-decoration: none">ADDITIONAL DETAILS</h1></td>
        </tr>
        <!-- <tr style="">
            <td style="color:#000; font-size: 11px; line-height: 20px;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</td> 
        </tr> -->        

            <?php 

            foreach($special as $_special){

                echo "<tr><td style='color:#62615e; font-size: 11px; line-height: 20px;'>".$_special."</td></tr>";

            }

            ?>

        
    </table>
    <table>
        <tr>
            <td style="height: 10px"></td>
        </tr>
</table>
</div> <!-- additional details div -->






<!-- estimate div -->
<div id="" style="padding: 10px;">
    <table style="width: 100%">
        <tr>
            <td>
            <b style="font-size:13px;color:#0684c1;">ESTIMATED COMPLETION TIME </b> &nbsp; <b style="font-size:13px;color:#62615e;"><?php echo ucwords($completion);?></b>
            </td>
        </tr>
        <tr>
            <td style="color:#62615e; font-size: 11px;">Your fixed-price fee includes all labour and materials unless otherwise specified</td>      
        </tr>
    </table>
</div> <!-- estimate div -->






<!-- main DIV -->
<div>
<table>
        <tr>
            <td style="height: 10px"></td>
        </tr>
</table>

    <table class="heading" style="border-top: 2px solid #e1e1e1; width:100%;">

        <tr>
            <td>
                <div> <img src="http://enacteservices.com/paintpad/image/Main_logo.jpg" alt="" style="width:80px; "> </div>
            </td>

            <td style="padding-left: 30px;">
                <h1 style="color:#0684c1;font-size: 13px">PAINT COLOURS</h1>
        
                <p style="font-size: 11px;color:#62615e;">Refer to Job Specification</p>
            </td>

            <td style="border:0px; text-align: right; padding-top: 20px;">
                <table>
                    <tr style="">
                     <td style="border-bottom: 2px solid #c7e3f2;font-size: 13px;color:#62615e;padding-bottom: 8px; padding-right: 11px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total of fixed price fee&nbsp;&nbsp;&nbsp;<?php echo '$'.$subtotal;?></td>
                    </tr>

                    <tr style="">
                    <td style="border-bottom: 2px solid #c7e3f2;font-size: 13px;color:#62615e;padding-bottom: 8px; padding-right: 11px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Plus GST&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo '$'.$gst;?></td>                  
                    </tr>
                
                    <tr style="">              
                    <td style="border-bottom: 2px solid #c7e3f2;font-size: 13px;color:#62615e;padding-bottom: 8px; padding-right: 11px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total Including GST&nbsp;&nbsp;&nbsp;<?php echo '$'.$total;?></td>  
                    </tr>
            
                    <tr style="">
                    <td style="border-bottom: 2px solid #47a4d1;font-size: 13px;color:#62615e;padding-bottom: 8px; padding-right: 11px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Deposit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo '$'.$deposit;?></td>                  
                    </tr>
                
                    <tr style="">       
                     <td style="font-size: 13px;color:#007aa9;padding-right: 11px">Balance&nbsp;&nbsp;&nbsp;<?php echo '$'.$balance;?></td> 
                    </tr>
                </table>
            </td>
        </tr>

    </table>
<table>
        <tr>
            <td style="height: 10px"></td>
        </tr>
</table>
</div>  <!-- main DIV -->









<div style="background:#0684c1;width: 100%">
    <table>
        <tr style="background:#0684c1;width: 100%;font-size: 13px;  font-weight: bold;">
            
            <td style="color:#fff;padding-left: 10px;">ACCEPTANCE ADVICE</td>
        
            
            <td style="color:#fff;padding-left: 300px; text-align: right;">
                <table>
                    <tr style="width: 100%">
                        <td  style="color:#fff; ">TAX INVOICE</td>           
                        <td style="color:#fff;">ABN</td>           
                        <td style=" color:#fff;">10245121</td>  
                    </tr>
                </table>
            </td>


                     
        </tr>
    </table>     
</div> 



<table>
        <tr>
            <td style="height: 10px"></td>
        </tr>
</table>


<div id="" style=";padding:10px;">
    <table>     
        <tr style="">
            <td style="color:#62615e; font-size: 11px"><b>Terms:</b> I/we accept the GST inclusive fixed price fee of <?php echo '<b>$'.$total.'</b>';?> from Demo Painters as outlined in this quote (No. <?php echo $qid; ?>) and wish to establish a date of commencment. I will pay the 3.73% deposit of <?php echo '<b>$'.$deposit.'</b>';?> either by credit card over the phone by calling or by electronic funds transfer to the account, BSB: , account number. The remainder will be due within 7 days of completion as per the agreed payment schedule. Recommended commencement date:   DATE.</td>                
        </tr>
    </table>
</div>

<table>
        <tr>
            <td style="height: 10px"></td>
        </tr>
</table>


<!-- main DIV -->
<div>
    <table class="heading" cellpadding="0" cellspacing="0" style="border-top: 0px solid #e1e1e1; width:100%;">
       
        <tr>
            <td style="padding-bottom: 10px;">
                <h1 style="color:#1d7db3; font-size: 13px; font-weight: bold;">AUTHORISATION</h1>
            </td>
        </tr>

        <tr>
            <td style="width:32%;border-bottom: 2px solid #aeaeae;border-right: 5px solid #fff;padding-bottom: 40px">
                <h1 style="font-size: 13px; font-weight: normal;color:#62615e">NAME</h1>
            </td>       
            <td style="width:32%;border-bottom: 2px solid #aeaeae;border-right: 5px solid #fff;padding-bottom: 40px">
                <h1 style="font-size: 13px; font-weight: normal;color:#62615e">SIGNATURE</h1>
            </td>
            <td style="width:32%; border-bottom: 2px solid #aeaeae;border-left: 5px solid #fff;padding-bottom: 40px">
                <h1 style="font-size: 13px; font-weight: normal;color:#62615e">DATE</h1>
            </td>
        </tr>

    </table>
</div>  <!-- main DIV -->

</div> <!-- main OUTER DIV -->

