<?php

		$image      = '';
		$subtotal   = '';
		$gst        = '';
		$total      = '';
		$deposit    = '';
		$balance    = '';
		$start      = '';
		$completion = '';
		$includes   = '';

	if(isset($data) && count($data)>0){
		//echo '<pre>'; print_r($data);

		$image      = $data['image'];
		$subtotal   = $data['subtotal'];
		$gst        = $data['gst'];
		$total      = $data['total'];
		$deposit    = $data['deposit'];
		$balance    = $data['balance'];
		$start      = $data['start'];
		$completion = $data['completion'];
		$includes   = $data['includes'];	

	} //data

	if(isset($cdata) && count($cdata)>0){
		$cname  = $cdata['cname'];
		$cemail = $cdata['cemail'];
		$cphone = $cdata['cphone'];
	} //cdata

	if(isset($qdata) && count($qdata)>0){
		$qdesc = $qdata['description'];
		$qid   = $qdata['number'];
		$qdate = $qdata['date'];
	} //qdata

?>

<style type="text/css">
	
.mono{
	font-family:Arial,serif;
	font-size:14.0px;
	color:rgb(9,127,197);
	font-weight:normal;
	font-style:normal;
	text-decoration: none
}	
#invoice_total table
{
    width:100%;
    border-top: 3px solid #ccc;
    border-spacing:0;
    border-collapse: collapse;      
    margin-top:5mm;
}

</style>

<!-- main DIV -->
<div>
    <table class="heading" cellpadding="0" cellspacing="0" style="border-bottom: 3px solid #e1e1e1; width:100%;">
        <tr>
            <td style="width:50%;">
                <img src="http://enacteservices.com/paintpad/image/Main_logo.jpg" alt="" style="width:140px;">
            </td>
            <td valign="top" align="right" style="width:50%; border:0px;">
                <h1 style="margin-bottom: 13px;color:#0684c1; font-size: 18px; font-weight: normal; float: left;width: 100%; text-align: right;">INTERIOR PAINTING QUOTE</h1>
                <table class="headerinnertable" style="background: #ececec;border:0px; float: right">
                    <tbody>
                    <tr>
                        <td style="width:18%; border:0px;padding: 5px 22px;line-height: 20px;color:#525251; font-size: 12px; font-weight: normal;text-align: left;">
	                        <b style="color:#0684c1; font-size: 14px; text-transform: uppercase;">Quote No.</b><br>
	                        <?php echo $qid; ?>
                        </td>
                        <td style="width:18%; border:0px;padding: 5px 22px;line-height: 20px;color:#525251; font-size: 12px; font-weight: normal;text-align: left;">
	                        <b style="color:#0684c1; font-size: 14px; text-transform: uppercase; ">Quote Date</b> <br>
	                        <?php echo $qdate; ?><br />
                        </td>
                        <td style="border:0px;padding: 10px 22px;"><img width= "200px" src="<?php echo $image; ?>" alt=""></td>
                    <tr>
                    </tbody>
                </table><!-- headerinnertable -->
            </td>
        </tr>
    </table>

</div>  <!-- main DIV -->

<!-- client details DIV -->
<div>
	
	<table>
		
		<tr>
			<td style="width:18%; margin-bottom: 13px;color:#0684c1; font-size: 14px; font-weight: normal; float: left;width: 100%; text-align: left;"><?php echo $cname; ?></td>
			<td style="width:30%; margin-bottom: 13px;color:#0684c1; font-size: 14px; font-weight: bold; float: left;width: 100%; text-align: left;">Site Details</td>
			<td style="width:10%; margin-bottom: 13px;color:#0684c1; font-size: 14px; font-weight: bold; float: left;width: 100%; text-align: left;">Quoted by</td>
		</tr>
		<tr>
			<td style="font-size: 12px;">Contact:<?php echo $cname; ?></td>
			<td style="font-size: 12px;">Contact:<?php echo $cname; ?></td>
			<td style="font-size: 12px;">NAME</td>
		</tr>
		<tr>
			<td style="font-size: 12px;">Mobile:<?php echo $cphone; ?></td>
			<td style="font-size: 12px;">Mobile:<?php echo $cphone; ?></td>
			<td style="font-size: 12px;">NUMBER</td>
		</tr>
		<tr>
			<td style="font-size: 12px;"></td>
			<td style="font-size: 12px;">ADDRESS</td>
			<td style="font-size: 12px;"><?php echo $cemail; ?></td>
		</tr>	

	</table>

</div> <!-- client details DIV -->


<!-- description div -->
<div id="invoice_body">
    <table>
	    <tr style="background:#4077B5;">
	        <td colspan="5" style="width:8%; padding: 10px; color:#fff"><b>Description</b></td>                
	    </tr>
    </table>     
    <table>
	    <tr>
	        <td colspan="5" style="width:8%;background:#eee;padding: 10px;"><?php echo $qdesc; ?></td>               
	    </tr>       
    </table>
</div> <!-- description div -->

<!-- service div -->
<div id="invoice_body">
    <table>
	    <tr style="">
	        <td colspan="5" style="width:8%;"><h1 style="font-family:Arial,serif;font-size:14.0px;color:rgb(9,127,197);font-weight:normal;font-style:normal;text-decoration: none">PAINITING SERVICE INCLUDES</h1></td>
	    </tr>
	    <tr style="">
	        <td colspan="5" style="width:8%; color:#000"><?php echo $includes;?></td>                
	    </tr>
    </table>
</div> <!-- service div -->

<!-- additional details div -->
<div id="" style="background:#eee;padding: 10px;">
    <table>
	    <tr style="">
	        <td colspan="5" style="width:8%;"><h1 style="font-family:Arial,serif;font-size:14.0px;color:rgb(9,127,197);font-weight:normal;font-style:normal;text-decoration: none">ADDITIONAL DETAILS</h1></td>
	    </tr>
	    <tr style="">
	        <td colspan="5" style="width:8%; color:#000">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</td>                
	    </tr>
    </table>
</div> <!-- additional details div -->

<!-- estimate div -->
<div id="" style="padding: 10px;">
    <table>
	    <tr style="">
	        <td colspan="5" style="width:8%;"><h1 style="font-family:Arial,serif;font-size:14.0px;color:rgb(9,127,197);font-weight:normal;font-style:normal;text-decoration: none">ESTIMATED COMPLETION TIME</h1> <?php echo ucwords($completion);?></td>
	    </tr>
	    <tr style="">
	        <td colspan="5" style="width:8%; color:#767678"><b>Your fixed-price fee includes all labour and materials unless otherwise specified</b></td>                
	    </tr>
    </table>
</div> <!-- estimate div -->

<div id="invoice_total">    
    <table>
        <tr>
            <td style="text-align:left; padding-left:10px;"></td>
            <td style="width:25%; border-bottom: 1px solid #eee;">Total of fixed price fee</td>
            <td style="width:15%; border-bottom: 1px solid #eee;" class="mono"><?php echo '$ '.$subtotal;?></td>
        </tr>
        <tr>
            <td style="text-align:left; padding-left:10px;"></td>
            <td style="width:25%; border-bottom: 1px solid #eee;">Plus GST</td>
            <td style="width:15%; border-bottom: 1px solid #eee;" class="mono"><?php echo '$ '.$gst;?></td>
        </tr>
        <tr>
            <td style="text-align:left; padding-left:10px;"></td>
            <td style="width:25%; border-bottom: 1px solid #eee;">Total Including GST</td>
            <td style="width:15%; border-bottom: 1px solid #eee;" class="mono"><?php echo '$ '.$total;?></td>
        </tr>
        <tr>
            <td style="text-align:left; padding-left:10px;"></td>
            <td style="width:25%; border-bottom: 1px solid rgb(9,127,197);">Deposit</td>
            <td style="width:15%; border-bottom: 1px solid rgb(9,127,197);" class="mono"><?php echo '$ '.$deposit;?></td>
        </tr>
        <tr>
            <td style="text-align:left; padding-left:10px;"></td>
            <td style="width:25%;font-family:Arial,serif;font-size:14.0px;color:rgb(9,127,197);font-weight:normal;font-style:normal;text-decoration: none;">Balance</td>
            <td style="width:15%;" class="mono"><?php echo '$ '.$balance;?></td>
        </tr>


    </table>
</div>
