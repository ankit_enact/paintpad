<?php

		$rooms      = [];
		$image      = '';
		$nop        = '';
		$qty        = '';
		$hrs        = '';
		$start      = '';
		$end        = '';
		$scope      = '';
		$special    = [];
		$siteNotes    = [];

		$rooms      = array();
		$materials  = array();
		$room_summary = '';
		$color_consultant = '';

		$consultant_text = 'A Colour Consultant - Professional On-site Colour Consultation - 1hr - You will receive an individual consultation to assist you select the right colours for you. Selections will be added to our Job Specification Sheet';

	if(isset($data) && count($data)>0){
		$scope      = $data['scope'];
		$brand_logo = $data['brand_logo'];
		$image      = $data['image'];
		$special    = $data['spl'];


		$nop       = $data['nop'];
		$qty       = $data['qty'];
		$hrs       = $data['hrs'];
		$start     = $data['start'];
		$end       = $data['end'];

		$rooms     = $data['rooms'];
		$materials = $data['materials'];
		$special    = $data['spl'];
		$room_summary = $data['rooms_summary'];
		$color_consultant = $data['color_consultant'];
		$specialInstructions = $data['specialInstructions'];
		$siteNotes = $data['siteNotes'];

		//echo count($rooms); exit;
		//echo '<pre>'; print_r($materials); exit;

	} //data
	
	//echo '<pre>'; print_r($rooms);exit;

?>

<style type="text/css">

#invoice_total table
{
    width:100%;
    border-top: 3px solid #ccc;
    border-spacing:0;
    border-collapse: collapse;      
    margin-top:5mm;
}

.brd-non{border-top: 0px solid #ccc !important;
    border-spacing:0 !important;
    border-collapse: inherit !important;  }

td, p, h1{font-family: Arial, Helvetica, sans-serif;}

/******************************************************************************************************************************/

.ft0{font: 1px 'Arial, Helvetica, sans-serif';line-height: 1px;}
.ft6{font: 13px 'Arial, Helvetica, sans-serif';color: #ffffff;line-height: 16px;}
.ft7{font: 1px 'Arial, Helvetica, sans-serif';line-height: 8px;}
.ft9{font: bold 13px 'Arial, Helvetica, sans-serif';line-height: 20px;}
.ft8{font: 13px 'Arial, Helvetica, sans-serif';line-height: 20px;}
.ft10{font: 13px 'Arial, Helvetica, sans-serif';line-height: 16px;}
.ft11{font: bold 13px 'Arial, Helvetica, sans-serif';line-height: 16px;}
.ft12{font: 13px 'Arial, Helvetica, sans-serif';line-height: 19px;}
.ft13{font: 1px 'Arial, Helvetica, sans-serif';line-height: 4px;}
.ft14{font: 9px 'Arial, Helvetica, sans-serif';line-height: 12px;position: relative; bottom: 6px;}

.ftt9{font: 13px 'Arial, Helvetica, sans-serif';line-height: 15px;}




.ft1{font: 18px 'Arial, Helvetica, sans-serif';color: #0a80c5;line-height: 21px;}
.ft2{font: 13px 'Arial, Helvetica, sans-serif';color: #62615e;line-height: 16px;}
.ft3{font: bold 15px 'Arial, Helvetica, sans-serif';color: #0a80c5;line-height: 18px;}
.ft4{font: bold 13px 'Arial, Helvetica, sans-serif';color: #62615e;line-height: 16px;}
.ft5{font: 1px 'Arial, Helvetica, sans-serif';line-height: 10px;}
.ft15{font: italic 11px 'Arial, Helvetica, sans-serif';line-height: 14px;}
.ft16{font: 1px 'Arial, Helvetica, sans-serif';line-height: 2px;}
.ft17{font: 19px 'Arial, Helvetica, sans-serif';color: #0a80c5;line-height: 22px;}
.ft18{font: 1px 'Arial, Helvetica, sans-serif';line-height: 3px;}
.ft19{font: 1px 'Arial, Helvetica, sans-serif';line-height: 9px;}
.ft20{font: 1px 'Arial, Helvetica, sans-serif';line-height: 15px;}
.ft21{font: bold 13px 'Arial, Helvetica, sans-serif';color: #0a80c5;line-height: 16px;}
.ft22{font: bold 16px 'Arial, Helvetica, sans-serif';color: #1d7db3;line-height: 19px;}






.p0{text-align: left;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p1{text-align: left;padding-left: 110px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p2{text-align: left;padding-left: 225px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p3{text-align: right;padding-right: 133px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p4{text-align: left;padding-left: 2px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
/*.p4 {text-align: right;padding-left: 2px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;padding-right: 10px;}*/
.p5{text-align: left;padding-left: 54px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p6{text-align: left;padding-left: 107px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p7{text-align: left;padding-left: 24px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p8{text-align: left;padding-left: 55px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p9{text-align: left;padding-left: 10px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p10{text-align: left;padding-left: 0px;padding-right: 91px;margin-top: 11px;margin-bottom: 0px;}
.p11{text-align: left;padding-left: 6px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p12{text-align: left;padding-right: 44px;margin-top: 2px;margin-bottom: 0px;}
.p13{text-align: left;margin-top: 0px;margin-bottom: 0px;}
.p14{text-align: left;padding-left: 4px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p15{text-align: left;padding-left: 3px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p16{text-align: left;padding-left: 33px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p17{text-align: left;padding-left: 12px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p18{text-align: right;padding-right: 8px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p19{text-align: right;padding-right: 145px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p20{text-align: left;padding-left: 52px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p21{text-align: left;padding-left: 252px;margin-top: 0px;margin-bottom: 0px;}
.p22{text-align: right;padding-right: 27px;margin-top: 0px;margin-bottom: 0px;}
.p23{text-align: left;padding-left: 460px;margin-top: 35px;margin-bottom: 0px;}
.p24{text-align: left;padding-left: 40px;margin-top: 27px;margin-bottom: 0px;}
.p25{text-align: left;padding-left: 44px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p26{text-align: left;padding-left: 125px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p27{text-align: left;padding-left: 73px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p28{text-align: left;padding-left: 14px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p29{text-align: left;padding-left: 29px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p30{text-align: left;padding-left: 26px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p31{text-align: right;padding-right: 134px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p32{text-align: center;padding-right: 12px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p33{text-align: left;padding-left: 91px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p34{text-align: left;padding-left: 39px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p35{text-align: center;padding-right: 13px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}

.td0{padding: 0px;margin: 0px;width: 197px;vertical-align: middle;}
.td1{padding: 0px;margin: 0px;width: 281px;vertical-align: middle;}
.td2{padding: 0px;margin: 0px;width: 251px;vertical-align: middle;}
.td3{padding: 0px;margin: 0px;width: 239px;vertical-align: middle;}
.td4{padding: 0px;margin: 0px;width: 12px;vertical-align: middle;}
.td5{padding: 0px;margin: 0px;width: 197px;vertical-align: middle;background: #1d7db3;}
.td6{padding: 0px;margin: 0px;width: 281px;vertical-align: middle;background: #1d7db3;}
.td7{padding: 0px;margin: 0px;width: 239px;vertical-align: middle;background: #1d7db3;}
.td8{padding: 0px;margin: 0px;width: 365px;vertical-align: middle;}
.td9{padding: 0px;margin: 0px;width: 352px;vertical-align: middle;}
.td10{padding: 0px;margin: 0px;width: 365px;vertical-align: middle;background: #666666;}
.td11{padding: 0px;margin: 0px;width: 352px;vertical-align: middle;background: #666666;}
.td12{border-left: #1d7db3 1px solid;border-right: #1d7db3 1px solid;padding: 0px;margin: 0px;width: 429px;vertical-align: middle;background: #1d7db3;}
.td13{border-right: #1d7db3 1px solid;padding: 0px;margin: 0px;width: 143px;vertical-align: middle;background: #1d7db3;}
.td14{border-right: #1d7db3 1px solid;padding: 0px;margin: 0px;width: 142px;vertical-align: middle;background: #1d7db3;}
.td15{border-left: #1d7db3 1px solid;border-right: #1d7db3 1px solid;padding: 0px;margin: 0px;width: 178px;vertical-align: middle;background: #1d7db3;}
.td16{border-right: #1d7db3 1px solid;padding: 0px;margin: 0px;width: 250px;vertical-align: middle;background: #1d7db3;}
.td17{border-right: #1d7db3 1px solid;padding: 0px;margin: 0px;width: 71px;vertical-align: middle;background: #1d7db3;}
.td18{border-right: #1d7db3 1px solid;padding: 0px;margin: 0px;width: 70px;vertical-align: middle;background: #1d7db3;}
.td19{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 180px;vertical-align: middle;}
.td20{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 251px;vertical-align: middle;}
.td21{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 144px;vertical-align: middle;}
.td22{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 72px;vertical-align: middle;}
.td23{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 71px;vertical-align: middle;}


.td24{border-left: #000000 1px solid;border-right: #000000 1px solid;padding:5px 5px;margin: 0px;width: 178px;vertical-align: middle;background: #cdcdcd;}
.td25{border-right: #000000 1px solid;border-top: #cdcdcd 1px solid;padding:5px 5px;margin: 0px;width: 250px;vertical-align: middle;background: #cdcdcd;}
.td26{border-right: #000000 1px solid;border-top: #cdcdcd 1px solid;padding:5px 5px;margin: 0px;width: 143px;vertical-align: middle;background: #cdcdcd;}
.td27{border-right: #000000 1px solid;border-top: #cdcdcd 1px solid;padding:5px 5px;margin: 0px;width: 71px;vertical-align: middle;background: #cdcdcd;}
.td28{border-right: #000000 1px solid;border-top: #cdcdcd 1px solid;padding:5px 5px;margin: 0px;width: 70px;vertical-align: middle;background: #cdcdcd;}


.td29{border-left: #000000 1px solid;border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 178px;vertical-align: middle;background: #cdcdcd;}
.td30{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 250px;vertical-align: middle;background: #cdcdcd;}
.td31{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 143px;vertical-align: middle;background: #cdcdcd;}
.td32{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 71px;vertical-align: middle;background: #cdcdcd;}
.td33{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 70px;vertical-align: middle;background: #cdcdcd;}

.td34{border-left: #000000 1px solid;border-right: #000000 1px solid;margin: 0px;width: 178px;vertical-align: middle;}
.td35{border-right: #000000 1px solid;margin: 0px;width: 250px;vertical-align: middle;}
.td36{border-right: #000000 1px solid;margin: 0px;width: 143px;vertical-align: middle;}
.td37{border-right: #000000 1px solid;margin: 0px;width: 71px;vertical-align: middle;}
.td38{border-right: #000000 1px solid;margin: 0px;width: 70px;vertical-align: middle;}

.td39{border-left: #000000 1px solid;border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 178px;vertical-align: middle;}
.td40{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 250px;vertical-align: middle;}
.td41{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 143px;vertical-align: middle;}
.td42{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 71px;vertical-align: middle;}
.td43{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 70px;vertical-align: middle;}
.td44{padding: 0px;margin: 0px;width: 195px;vertical-align: middle;}
.td45{border-right: #000000 1px solid;border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 250px;vertical-align: middle;background: #cdcdcd;}
.td46{border-right: #000000 1px solid;border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 143px;vertical-align: middle;background: #cdcdcd;}
.td47{border-right: #000000 1px solid;border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 71px;vertical-align: middle;background: #cdcdcd;}
.td48{border-right: #000000 1px solid;border-top: #000000 1px solid;padding: 0px;margin: 0px;width: 70px;vertical-align: middle;background: #cdcdcd;}
.td49{padding: 0px;margin: 0px;width: 169px;vertical-align: middle;}
.td50 {padding: 0 0 0 15px;margin: 0px;width: 250px;vertical-align: bottom;}
.td51{padding: 0px;margin: 0px;width: 216px;vertical-align: middle;}
.td52{padding: 0px;margin: 0px;width: 169px;vertical-align: middle;background: #cdcdcd;}
.td53{border-right: #1d7db3 1px solid;padding: 0px;margin: 0px;width: 147px;vertical-align: middle;background: #1d7db3;}
.td54{border-right: #1d7db3 1px solid;padding: 0px;margin: 0px;width: 138px;vertical-align: middle;background: #1d7db3;}
.td55{border-right: #1d7db3 1px solid;padding: 0px;margin: 0px;width: 67px;vertical-align: middle;background: #1d7db3;}
.td56{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 148px;vertical-align: middle;}
.td57{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 68px;vertical-align: middle;}
.td58{border-right: #000000 1px solid;border-top: #cdcdcd 1px solid;padding: 0px;margin: 0px;width: 147px;vertical-align: middle;background: #cdcdcd;}
.td59{border-right: #000000 1px solid;border-top: #cdcdcd 1px solid;padding: 0px;margin: 0px;width: 67px;vertical-align: middle;background: #cdcdcd;}
.td60{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 147px;vertical-align: middle;background: #cdcdcd;}
.td61{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 67px;vertical-align: middle;background: #cdcdcd;}
.td62{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 147px;vertical-align: middle;}
.td63{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 67px;vertical-align: middle;}
.td64{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 147px;vertical-align: middle;}
.td65{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 67px;vertical-align: middle;}
.td66{padding: 0px;margin: 0px;width: 180px;vertical-align: middle;}
.td67{padding: 0px;margin: 0px;width: 148px;vertical-align: middle;}
.td68{padding: 0px;margin: 0px;width: 68px;vertical-align: middle;}
.td69{padding: 0px;margin: 0px;width: 71px;vertical-align: middle;}
.td70{padding: 0px;margin: 0px;width: 54px;vertical-align: middle;}
.td71{padding: 0px;margin: 0px;width: 19px;vertical-align: middle;}
.td72{padding: 0px;margin: 0px;width: 140px;vertical-align: middle;}
.td73{padding: 0px;margin: 0px;width: 183px;vertical-align: middle;}
.td74{padding: 0px;margin: 0px;width: 82px;vertical-align: middle;}
.td75{padding: 0px;margin: 0px;width: 240px;vertical-align: middle;}
.td76{padding: 0px;margin: 0px;width: 11px;vertical-align: middle;}
.td77{padding: 0px;margin: 0px;width: 159px;vertical-align: middle;}
.td78{padding: 0px;margin: 0px;width: 73px;vertical-align: middle;}
.td79{border-left: #666666 1px solid;padding: 0px;margin: 0px;width: 212px;vertical-align: middle;background: #666666;}
.td80{border-right: #666666 1px solid;padding: 0px;margin: 0px;width: 182px;vertical-align: middle;background: #666666;}
.td81{padding: 0px;margin: 0px;width: 82px;vertical-align: middle;background: #666666;}
.td82{border-right: #666666 1px solid;padding: 0px;margin: 0px;width: 239px;vertical-align: middle;background: #666666;}
.td83{border-left: #666666 1px solid;padding: 0px;margin: 0px;width: 53px;vertical-align: middle;background: #666666;}
.td84{border-right: #666666 1px solid;padding: 0px;margin: 0px;width: 18px;vertical-align: middle;background: #666666;}
.td85{padding: 0px;margin: 0px;width: 140px;vertical-align: middle;background: #666666;}
.td86{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 54px;vertical-align: middle;}
.td87{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 19px;vertical-align: middle;}
.td88{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 140px;vertical-align: middle;}
.td89{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 183px;vertical-align: middle;}
.td90{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 82px;vertical-align: middle;}
.td91{border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 240px;vertical-align: middle;}




.td92{border-left: #000000 1px solid;border-top: #000000 1px solid;border-bottom: #000000 1px solid;padding: 5px;margin: 0px;width: 53px;vertical-align: middle;background: #cdcdcd;}
.td93{border-right: #000000 1px solid;border-top: #000000 1px solid;border-bottom: #000000 1px solid;padding: 5px;margin: 0px;width: 18px;vertical-align: middle;background: #cdcdcd;}
.td94{border-top: #cdcdcd 1px solid;border-bottom: #000000 1px solid;padding: 5px;margin: 0px;width: 140px;vertical-align: middle;background: #cdcdcd;}
.td95{border-right: #000000 1px solid;border-top: #cdcdcd 1px solid;border-bottom: #000000 1px solid;padding: 5px;margin: 0px;width: 182px;vertical-align: middle;background: #cdcdcd;}
.td96{border-top: #cdcdcd 1px solid;border-bottom: #000000 1px solid;padding: 5px;margin: 0px;width: 82px;vertical-align: middle;background: #cdcdcd;}
.td97{border-right: #000000 1px solid;border-top: #cdcdcd 1px solid;border-bottom: #000000 1px solid;padding: 5px;margin: 0px;width: 239px;vertical-align: middle;background: #cdcdcd;}
.td98{border-left: #000000 1px solid;padding: 5px;margin: 0px;width: 53px;vertical-align: middle;}
.td99{border-right: #000000 1px solid;padding: 5px;margin: 0px;width: 18px;vertical-align: middle;}
.td100{border-right: #000000 1px solid;padding: 5px;margin: 0px;width: 322px;vertical-align: middle;}
.td101{border-right: #000000 1px solid;padding: 5px;margin: 0px;width: 321px;vertical-align: middle;}
.td102{border-left: #000000 1px solid;border-bottom: #000000 1px solid;padding: 5px;margin: 0px;width: 53px;vertical-align: middle;}
.td103{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 5px;margin: 0px;width: 18px;vertical-align: middle;}
.td104{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 5px;width: 322px;vertical-align: middle;}



.td105{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 239px;vertical-align: middle;}
.td106{border-right: #000000 1px solid;padding: 0px;margin: 0px;width: 239px;vertical-align: middle;}
.td107{border-right: #000000 1px solid;border-bottom: #000000 1px solid;padding: 0px;margin: 0px;width: 182px;vertical-align: middle;}

.tr0{height: 45px;}
.tr1{height: 42px;}
.tr2{height: 21px;}
.tr3{height: 22px;}
.tr4{height: 10px;}
.tr5{height: 38px;}
.tr6{height: 8px;}
.tr7{height: 25px;}
.tr8{height: 17px;}
.tr9{height: 18px;}
.tr10{height: 38px;}
.tr11{height: 4px;}
.tr12{height: 20px;border-bottom: #000000 1px solid;vertical-align: middle !important; padding: 5px !important;}
.tr13{height: 19px;border-bottom: #000000 1px solid;vertical-align: middle !important;padding: 5px !important;}
.tr14{height: 27px;}
.tr15{height: 23px;}
.tr16{height: 2px;}
.tr17{height: 24px;}
.tr18{height: 16px;}
.tr19{height: 26px;}
.tr20{height: 38px;}
.tr21{height: 91px;}
.tr22{height: 3px;}
.tr23{height: 29px;}
.tr24{height: 9px;}
.tr25{height: 15px;}
.tr26{height: 46px;}
.tr27{height: 65px;}
.tr28{height: 37px;}
.tr29{height: 36px;}

.t0{width: 100%;font: 13px 'Arial, Helvetica, sans-serif';color: #62615e;}
.t1{width: 100%;font: bold 13px 'Arial, Helvetica, sans-serif';}
.t2{width: 100%;margin-top: 9px;font: 13px 'Arial, Helvetica, sans-serif';}
.t3{width: 100%;margin-left: 2px;font: 13px 'Arial, Helvetica, sans-serif';color: #62615e;}
.t4{width: 100%;margin-top: 5px;font: 13px 'Arial, Helvetica, sans-serif';}
.t5{width: 100%;margin-left: 48px;font: bold 13px 'Arial, Helvetica, sans-serif';color: #62615e;}
.t6{width: 100%;margin-left: 38px;margin-top: 5px;font: 13px 'Arial, Helvetica, sans-serif';}
.t7{width: 100%;font: 13px 'Arial, Helvetica, sans-serif';}

.pd-l{padding-left:15px}
.t0 {background: #1d7db3;}
.td24, .td25, .td26, .td27, .td28{border-bottom: #000 1px solid;vertical-align: middle;}

</style>
<!-- main DIV -->


<DIV>
<table>
	<tr>
		<td style="height: 10px"></td>
	</tr>
</table>
<TABLE cellpadding=0 cellspacing=0 class="t0">
<TR>
	<TD class="tr5 td5 pd-l" style="text-align:left;padding-left: 10px;"><P class="p9 ft6">Job Scope</P></TD>
	<TD class="tr5 td6"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr5 td7"><P class="p0 ft0">&nbsp;</P></TD>
	
</TR>

</TABLE>
<P class="p10 ft9"><b><?php echo strtoupper($scope); ?></b></P>
<p class="ftt9"><b>ROOMS: </b><?php echo $room_summary; ?></p>
<TABLE cellpadding=0 cellspacing=0 class="t1">
<TR>
	<TD class="tr7 td8"><P class="p11 ft11">No. Painters: <SPAN class="ft10"><?php echo $nop; ?></SPAN></P></TD>
	<TD class="tr7 td9"><P class="p0 ft11">Recommended Start: <SPAN class="ft10"><?php echo $start; ?></SPAN></P></TD>
</TR>
<TR>
	<TD class="tr8 td8"><P class="p11 ft11">Litres Paint: <SPAN class="ft10"><?php echo $qty; ?></SPAN></P></TD>
	<TD class="tr8 td9"><P class="p0 ft11">Expected Completion: <SPAN class="ft10"><?php echo $end; ?></SPAN></P></TD>
</TR>
<TR>
	<TD class="tr3 td8"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr3 td9"><P class="p0 ft11">Total Hours: <SPAN class="ft10"><?php echo $hrs; ?></SPAN></P></TD>
</TR>
<TR>
	<TD class="tr3 td8"><P class="p0 ft0">&nbsp;</P></TD>
	<TD class="tr3 td9"><P class="p0 ft11">&nbsp; </P></TD>
</TR>

</TABLE>


<?php 
	/*foreach($specialInstructions as $si){
		echo '<TR>';
			echo '<TD class="tr12 td34"><P class="p14 ft10">'.$si.'</P></TD>';
		echo '</TR>';
	}*/
	
	/********* for Special Items *************/
	/*foreach($special as $_special){
		echo '<p class="p12 ft12">'.$_special.'</p>';
	}//foreach*/


    if($color_consultant == 1){
    	echo "<p class='ft12'>".$consultant_text."</p>";
    }
	if(count($special) > 0 || count($siteNotes)>0){
		echo '<TABLE cellpadding=0 cellspacing=0 class="t1" style="background: #666666;">'.
				'<TR>'.
					'<TD class="tr5 td10 pd-l" style="text-align:left;padding-left: 10px;"><P class="p9 ft6">Special Instructions</P></TD>'.
				'</TR>'.
			'</TABLE>';

		if(count($special) > 0){
	        foreach($special as $_special){
	        	echo "<p class='p12 ft12'>".$_special."</p>";
	        }//foreach
	    }
		echo "<p class='p12 ft12'></p>";
	}
	if(count($siteNotes)>0){
			echo 	'<TABLE cellpadding=0 cellspacing=0 class="t1" style="background: #666666;">'.
						'<TR>'.
							'<TD class="tr5 td10 pd-l" style="text-align:left;padding-left: 10px;"><P class="p9 ft6">Site Notes</P></TD>'.
						'</TR>'.
					'</TABLE>';
			echo '<TABLE cellpadding=0 cellspacing=0 class="t7 lst-tone">';
				foreach ($siteNotes as $csnk => $csnv) {
					echo '<TR>';
						$imgElement='&nbsp;';
						if($csnv['image']!=''){
							//$imgElement = '<img src="'.$siteURL.'/notes/'.$csnv['image'].'" alt="" style="width:100px;">';
							$imgElement = '<img src="'.$csnv['image'].'" alt="" style="width:100px;">';
						}
						echo '<TD style="border-left:1px solid black;" class="tr12 td34"><P class="p14 ft10">'.$imgElement.'</P></TD>';
						echo '<TD style="border-left:1px solid black;" class="tr12 td35"><P class="p15 ft10">'.$csnv['description'].'</P></TD>';
					echo '</TR>';
				}
			echo '</TABLE>';

		/*if(count($siteNotes) > 0){
			foreach ($siteNotes as $sv) {
				echo "<p class='p12 ft12'>".$sv['description']."</p>";
			}//foreach
		}*/		
	}//if


	/********* for ROOMS  *************/

	if(count($rooms)>0){

		foreach($rooms as $_rooms){
			/* rooms heading */
			echo '<TABLE cellpadding=0 cellspacing=0 class="t2" style="background: #1d7db3;">';
			echo '<TR>';
				echo '<TD colspan=2 class="tr10 td12 pd-l" style="text-align:left;padding-left: 10px;"><P class="p9 ft6">'.$_rooms['name'].' - Total Hours: '.$_rooms['hrs'].'</P></TD>';
				echo '<TD class="tr10 td13"><P class="p0 ft0">&nbsp;</P></TD>';
				echo '<TD colspan=2 class="tr10 td14" style="text-align:right;padding-right: 10px;"><P class="p4 ft6">Prep Level '.$_rooms['prep_level'].'</P></TD>';
			echo '</TABLE>';

				/* rooms component */
				echo '<TABLE cellpadding=0 cellspacing=0 class="t2">';
				echo '<THEAD>';
				echo '<TR>';
					echo '<TD class="tr11 td19"><P class="p0 ft13">&nbsp;</P></TD>';
					echo '<TD class="tr11 td20"><P class="p0 ft13">&nbsp;</P></TD>';
					echo '<TD class="tr11 td21"><P class="p0 ft13">&nbsp;</P></TD>';
					echo '<TD class="tr11 td22"><P class="p0 ft13">&nbsp;</P></TD>';
					echo '<TD class="tr11 td23"><P class="p0 ft13">&nbsp;</P></TD>';
				echo '</TR>';
				echo '<TR>';
					echo '<TD class="tr9 td24"><P class="p14 ft10">Component</P></TD>';
					echo '<TD class="tr9 td25"><P class="p15 ft10">Product</P></TD>';
					echo '<TD class="tr9 td26"><P class="p15 ft10">Colour</P></TD>';
					echo '<TD class="tr9 td27"><P class="p16 ft10">Time <br>(h:m)</P></TD>';
					echo '<TD class="tr9 td28"><P class="p17 ft10">Paint (L)</P></TD>';
				echo '</TR>';
				echo '</THEAD>';

				echo '<TBODY>';

					/* rooms component body */
					foreach($_rooms['components'] as $components){

						echo '<TR>';
							echo '<TD class="tr12 td34"><P class="p14 ft10">'.$components['name'].'</P></TD>';
							echo '<TD class="tr12 td35"><P class="p15 ft10">'.$components['product'].'</P></TD>';
							echo '<TD class="tr12 td36"><P class="p15 ft10">'.$components['color'].'</P></TD>';
							echo '<TD class="tr12 td37"><P class="p18 ft10">'.$components['time'].'</P></TD>';
							echo '<TD class="tr12 td38"><P class="p18 ft10">'.$components['paint'].'</P></TD>';
						echo '</TR>';

					}//foreach

				echo '</TBODY>';

				echo '</TABLE>';

				if(count($_rooms['specialInstructions'])>0){
					echo 	'<TABLE cellpadding=0 cellspacing=0 class="t1" style="background: #666666;">'.
								'<TR>'.
									'<TD class="tr5 td10 pd-l" style="text-align:left;padding-left: 10px;"><P class="p9 ft6">Site Notes</P></TD>'.
								'</TR>'.
							'</TABLE>';
					echo '<TABLE cellpadding=0 cellspacing=0 class="t7 lst-tone">';
						foreach ($_rooms['specialInstructions'] as $csv) {
							echo '<TR>';
								echo '<TD class="tr12 td35"><P class="p15 ft10">'.$csv.'</P></TD>';
							echo '</TR>';
						}
					echo '</TABLE>';

				}

				if(count($_rooms['siteNotes'])>0){
					echo 	'<TABLE cellpadding=0 cellspacing=0 class="t1" style="background: #666666;">'.
								'<TR>'.
									'<TD class="tr5 td10 pd-l" style="text-align:left;padding-left: 10px;"><P class="p9 ft6">Site Notes</P></TD>'.
								'</TR>'.
							'</TABLE>';
					echo '<TABLE cellpadding=0 cellspacing=0 class="t7 lst-tone">';
						foreach ($_rooms['siteNotes'] as $csnk => $csnv) {
							echo '<TR>';
								$imgElement='&nbsp;';
								if($csnv['image']!=''){
									//$imgElement = '<img src="'.$siteURL.'/notes/'.$csnv['image'].'" alt="" style="width:100px;">';
									$imgElement = '<img src="'.$csnv['image'].'" alt="" style="width:100px;">';
								}
								echo '<TD style="border-left:1px solid black;" class="tr12 td34"><P class="p14 ft10">'.$imgElement.'</P></TD>';
								echo '<TD style="border-left:1px solid black;" class="tr12 td35"><P class="p15 ft10">'.$csnv['description'].'</P></TD>';
							echo '</TR>';
						}
					echo '</TABLE>';

				}

		}//foreach
	}//if

?>

</DIV>

<table>
	<tr>
		<td style="height: 10px"></td>
	</tr>
</table>

<TABLE cellpadding=0 cellspacing=0 class="t1" style="background: #666666;">
<TR>
	<TD class="tr5 td10 pd-l" style="text-align:left;padding-left: 10px;"><P class="p9 ft6">Materials</P></TD>
</TR>
</TABLE>


<?php

	/********* for MATERIALS  *************/

	if(count($materials)>0){

		foreach($materials as $_materials){
			/* rooms heading */
			echo '<TABLE cellpadding=0 cellspacing=0 class="t7 lst-tone">';
				echo '<TR>';
					echo '<TD class="tr28 td86"><P class="p0 ft22">'.$_materials['name'].'</P></TD>';
					echo '<TD class="tr29 td88"><P class="p0 ft0">&nbsp;</P></TD>';
					echo '<TD class="tr29 td88"><P class="p0 ft0">&nbsp;</P></TD>';
					echo '<TD class="tr29 td89"><P class="p0 ft0">&nbsp;</P></TD>';
				echo '</TR>';
				echo '<THEAD>';
				echo '<TR class="ppd">';
					echo '<TD class="tr3 td92" style="border-right:1px solid #000;border-top: 0;"><P class="p15 ft10">Product</P></TD>';
					echo '<TD class="tr3 td94" style="border-right:1px solid #000;border-top: 0;"><P class="p4 ft10">Sheen</P></TD>';
					echo '<TD class="tr3 td94" style="border-right:1px solid #000;border-top: 0;"><P class="p4 ft10">Colour</P></TD>';
					echo '<TD class="tr3 td96" style="border-right:1px solid #000;border-top: 0;"><P class="p35 ft10">Litres</P></TD>';
				echo '</TR>';
				echo '</THEAD>';
					/* rooms component body */
					foreach($_materials['stocks'] as $stocks){
						echo '<TR>';
							echo '<TD class="tr12 td34"><P class="p15 ft10">'.$stocks['product'].'</P></TD>';
							echo '<TD class="tr12 td35"><P class="p15 ft10">'.$stocks['sheen_name'].'</P></TD>';
							echo '<TD class="tr12 td35"><P class="p15 ft10">'.$stocks['Colour'].'</P></TD>';
							echo '<TD class="tr12 td36"><P class="p14 ft10">'.$stocks['litres'].'</P></TD>';
						echo '</TR>';
					}//foreach

				echo '</TABLE>';	

		}//foreach
	}//if
	//exit;
?>



