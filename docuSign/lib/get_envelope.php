<?php
    include_once 'example_base.php';

    class GetEnvelope extends ExampleBase {
        public function __construct($client) {
            parent::__construct($client);
        }

        public function getEnvelope($env_id) {
            $this->checkToken();
            $envelopeApi = new DocuSign\eSign\Api\EnvelopesApi(self::$apiClient);
            $options = new DocuSign\eSign\Api\EnvelopesApi\GetEnvelopeOptions();
            $options->setInclude(null);

            
            return $envelopeApi->getEnvelope(self::$accountID, $env_id,$options);

            // $envelope = $envelopeApi->getEnvelope(self::$accountID, $env_id,$options);

            // $this->assertNotEmpty($envelope);

            // print_r($envelope);die;


        }
    }
