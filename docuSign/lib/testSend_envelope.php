<?php
include_once 'example_base.php';

class TestSendEnvelope extends ExampleBase {

    public function send($name,$email,$file) {

        $this->checkToken();
      
        $envelope_events = [
            (new \DocuSign\eSign\Model\EnvelopeEvent())->setEnvelopeEventStatusCode("sent"),
            (new \DocuSign\eSign\Model\EnvelopeEvent())->setEnvelopeEventStatusCode("delivered"),
            (new \DocuSign\eSign\Model\EnvelopeEvent())->setEnvelopeEventStatusCode("completed"),
            (new \DocuSign\eSign\Model\EnvelopeEvent())->setEnvelopeEventStatusCode("declined"),
            (new \DocuSign\eSign\Model\EnvelopeEvent())->setEnvelopeEventStatusCode("voided"),
            (new \DocuSign\eSign\Model\EnvelopeEvent())->setEnvelopeEventStatusCode("sent"),
            (new \DocuSign\eSign\Model\EnvelopeEvent())->setEnvelopeEventStatusCode("sent")
        ];

        $recipient_events = [
            (new \DocuSign\eSign\Model\RecipientEvent())->setRecipientEventStatusCode("Sent"),
            (new \DocuSign\eSign\Model\RecipientEvent())->setRecipientEventStatusCode("Delivered"),
            (new \DocuSign\eSign\Model\RecipientEvent())->setRecipientEventStatusCode("Completed"),
            (new \DocuSign\eSign\Model\RecipientEvent())->setRecipientEventStatusCode("Declined"),
            (new \DocuSign\eSign\Model\RecipientEvent())->setRecipientEventStatusCode("AuthenticationFailed"),
            (new \DocuSign\eSign\Model\RecipientEvent())->setRecipientEventStatusCode("AutoResponded")
        ];

        $webhook_url = "https://www.enacteservices.com/invoice/docuSign/test_webhook.php";

        $event_notification = new \DocuSign\eSign\Model\EventNotification();
        $event_notification->setUrl($webhook_url);
        $event_notification->setLoggingEnabled("true");
        $event_notification->setRequireAcknowledgment("true");
        $event_notification->setUseSoapInterface("false");
        $event_notification->setIncludeCertificateWithSoap("false");
        $event_notification->setSignMessageWithX509Cert("false");
        $event_notification->setIncludeDocuments("true");
        $event_notification->setIncludeEnvelopeVoidReason("true");
        $event_notification->setIncludeTimeZone("true");
        $event_notification->setIncludeSenderAccountAsCustomField("true");
        $event_notification->setIncludeDocumentFields("true");
        $event_notification->setIncludeCertificateOfCompletion("true");
        $event_notification->setEnvelopeEvents($envelope_events);
        $event_notification->setRecipientEvents($recipient_events);

        $signer_name = $name;
        $signer_email = $email;

        $signer1 = new \DocuSign\eSign\Model\Signer([
            'email' => $signer_email, 'name' => $signer_name,
            'recipient_id' => "1", 'routing_order' => "1"]);
        $sign_here2 = new \DocuSign\eSign\Model\SignHere([
            'anchor_string' => '/sn1/', 'anchor_units' =>  'pixels',
            'anchor_y_offset' => '10', 'anchor_x_offset' => '20']);
        $signer1->setTabs(new \DocuSign\eSign\Model\Tabs([
            'sign_here_tabs' => [$sign_here2]]));
        $recipients = new \DocuSign\eSign\Model\Recipients([
            'signers' => [$signer1]]);
        $content_bytes = file_get_contents($demo_docs_path . $file);
        $doc3_b64 = base64_encode($content_bytes);
        $document3 = new \DocuSign\eSign\Model\Document([  
            'document_base64' => $doc3_b64,
            'name' => 'invoice',  
            'file_extension' => 'pdf', 
            'document_id' => '3' 
        ]);
        $envelope_definition = new \DocuSign\eSign\Model\EnvelopeDefinition();

        $envelope_definition->setEmailSubject("Please sign the " . 
            preg_replace('/\\.[^.\\s]{3,4}$/', '', $this->doc_document_name) . " document");
        $envelope_definition->setDocuments([$document3]);
        $envelope_definition->setRecipients($recipients);
        $envelope_definition->setEventNotification($event_notification);
        $envelope_definition->setStatus("sent");
        $envelopesApi = new DocuSign\eSign\Api\EnvelopesApi(self::$apiClient);
        $envelope_summary = $envelopesApi->createEnvelope(self::$accountID, $envelope_definition,null);

        if ( !isset($envelope_summary) || $envelope_summary->getEnvelopeId() == null ) {
            return ["ok" => false, html => "<h3>Problem</h3>" .
            "<p>Error calling DocuSign</p>"];
        }
        return $envelope_summary;
       
    }
}
