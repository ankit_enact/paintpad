<?php
    require_once('vendor/autoload.php');
    require_once('vendor/docusign/esign-client/autoload.php');
    include_once 'ds_config.php';
    include_once 'dbConfig.php';
    include_once 'lib/example_base.php';
    include_once 'lib/testSend_envelope.php';
    include_once 'lib/get_envelope.php';
    $config = new DocuSign\eSign\Configuration();
    $apiClient = new DocuSign\eSign\ApiClient($config);

    
    $name = $_POST['name'];
    $email = $_POST['to'];
    $invoice_id = $_POST['invoiceid'];
    if(empty($name)){
        $response = array("status" => false, "msg" => "Name should not be empty.");
        echo json_encode($response);
        return false;
    }

    if(empty($email)){
        $response = array("status" => false, "msg" => "Email should not be empty.");
        echo json_encode($response);
        return false;
    }

    if(empty($invoice_id)){
        $response = array("status" => false, "msg" => "Invoice Id should not be empty.");
        echo json_encode($response);
        return false;
    }

    if (empty($_FILES['invoice_attachment']['name'])) {
        $response = array("status" => false, "msg" => "Invoice Attachment should not be empty");
        echo json_encode($response);
        return false;
    }


    $uploads_dir = 'asset/mailAttachmentsTemp/';

        if (!is_dir($uploads_dir)) {
            mkdir($uploads_dir, 0777, true);
        }
        $unlink_files = array();
        $ext = pathinfo($_FILES['invoice_attachment']['name'], PATHINFO_EXTENSION);
        $pdf_attachment = rand().'.'.$ext;
        $pdf_file_type = $_FILES['invoice_attachment']['type'];
        $pdf_tmp_name = $_FILES["invoice_attachment"]["tmp_name"];
        if (move_uploaded_file($pdf_tmp_name, $uploads_dir . $pdf_attachment)) {
            $unlink_file = 'asset/mailAttachmentsTemp/'.$pdf_attachment;  
            $pdfs = 'asset/mailAttachmentsTemp/'.$pdf_attachment;  

        }

        try {
        // print("\nSending an envelope...\n");
            $sendHandler = new TestSendEnvelope($apiClient);
            $result = $sendHandler->send($name,$email,$pdfs);
            if($result->getStatus() == 'sent'){
                $sqlUpd = "UPDATE `invoice_details` SET `invoice_sent_status`= '".$result->getStatus()."', `docuEnvId` = '".$result->getEnvelopeId()."' WHERE `invoice_id` = ". $invoice_id."";
                mysqli_query($conn, $sqlUpd);

                $response = array(
                    "status" => true,
                    "pdf_status" => $result->getStatus(),
                    "envelop_id" => $result->getEnvelopeId(),
                    "msg" => "Invoice Sent Successfully."
                );
                echo json_encode($response);
                // return true;
            }else{
                $response = array(
                    "status" => false,
                    "msg" => "Invoice Not Sent."
                );
                echo json_encode($response);
                return false;
            }



        // printf("\nEnvelope status: %s. Envelope ID: %s\n", $result->getStatus(), $result->getEnvelopeId());
            unlink($unlink_file);
        // die;

        // print("\n Envelope from the account...");
            $envelopesHandler= new GetEnvelope($apiClient);
            $envelope = $envelopesHandler->getEnvelope($result->getEnvelopeId());

            if(!is_null($envelope)) {
               $response = array(
                "status" => true,
                "pdf_status" => $envelope->getStatus(),
                "envelop_id" => $envelope->getEnvelopeId(),
                "msg" => "Invoice Get Successfully."
            );
               echo json_encode($response);
           } else {
            $response = array(
                "status" => false,
                "msg" => "Invoice Not Get."
            );
            echo json_encode($response);
            return false;
        }
        die;
        # $envelopesList is an object that implements ArrayAccess. Convert to a regular array:
        $results = json_decode((string)$envelopesList, true);
        # pretty print it:
        print (json_encode($results, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));


        print("\nList envelopes in the account...");
        $listEnvelopesHandler= new ListEnvelopes($apiClient);
        $envelopesList = $listEnvelopesHandler->listEnvelopes();
        $envelopes = $envelopesList->getEnvelopes();

        if(!is_null($envelopesList)  && count($envelopes) > 2) {
            printf("\nResults for %d envelopes were returned. Showing the first two:\n", count($envelopes));
            $envelopesList->setEnvelopes(array($envelopes[0],$envelopes[1]));
        } else {
            printf("\nResults for %d envelopes were returned:\n", count($envelopes));
        }
        # $envelopesList is an object that implements ArrayAccess. Convert to a regular array:
        $results = json_decode((string)$envelopesList, true);
        # pretty print it:
        print (json_encode($results, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
    } catch (Exception $e) {
        print ("\n\nException!\n");
        print ($e->getMessage());

        if ($e instanceof DocuSign\eSign\ApiException) {
            print ("\nAPI error information: \n");
            print ($e->getResponseObject());
        }


    }

    print("\nDone.\n");

