<?php
include_once 'dbConfig.php';


$name = $_POST['name'];
$manager_id = $_POST['managerid'];
$email = $_POST['to'];
$subject = $_POST['subject'];
$body = $_POST['body'];
$invoice_id = $_POST['invoiceid'];
if(empty($name)){
    $response = array("status" => false, "msg" => "Name should not be empty.");
    echo json_encode($response);
    return false;
}

if(empty($manager_id)){
    $response = array("status" => false, "msg" => "Manager Id should not be empty.");
    echo json_encode($response);
    return false;
}

if(empty($email)){
    $response = array("status" => false, "msg" => "Email should not be empty.");
    echo json_encode($response);
    return false;
}

if(empty($subject)){
    $response = array("status" => false, "msg" => "Subject should not be empty.");
    echo json_encode($response);
    return false;
}

if(empty($body)){
    $response = array("status" => false, "msg" => "Body should not be empty.");
    echo json_encode($response);
    return false;
}

if(empty($invoice_id)){
    $response = array("status" => false, "msg" => "Invoice Id should not be empty.");
    echo json_encode($response);
    return false;
}

if (empty($_FILES['invoice_attachment']['name'])) {
    $response = array("status" => false, "msg" => "Invoice Attachment should not be empty");
    echo json_encode($response);
    return false;
}



$sqlSelect = "SELECT clientId,secretKey,GUID FROM `company_info` WHERE `manager_id` = '". $manager_id."'";
$sqlData = mysqli_query($conn, $sqlSelect);
if($sqlData->num_rows > 0){
    $dataDocu = $sqlData->fetch_assoc();
    // $dataDocu['clientId'] = '';

    if(!empty($dataDocu['clientId']) && !empty($dataDocu['secretKey']) && !empty($dataDocu['GUID'])){

        $dbClientid = $dataDocu['clientId'];
        $dbPrivateKey= $dataDocu['secretKey'];
        $GUID= $dataDocu['GUID'];
    }else{
        die('1');
        $dbClientid = "463fc9c9-24b7-4388-ad78-0301a29b8cc0";
        $GUID = "6e76a741-9e2e-4726-9a5e-44959c4855b3";
        $dbPrivateKey= "-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAhzwsjR2kuXXbiRcoKeU9HzkpV8o560akslzof0XcA56OUJXd
o40Zr4SxDyC7qOfWFLqBRe8qMCtMEevoWbxnIIGlQxK8TZMLb84sHKyMUlBfQx+l
eSsExWKN0jqEnSy/iQ9SpnNLZmbo7au7gwkM5EcN56gAx64jeLs457nHzyVUv/i/
5aaqrLpFqYigd5VqpteXuSbwij4SQH8OxkzjmblDey1jUB4BNSTQNPRawVy0gemh
xKMotazq8EEfkOPUFOb9LTfNxbU0Pst02CsdDi01bLkuRaRSS+AlLoi+FWbSWcMQ
VHAA8LfKTsX8hA32+QHPGvO6rIks59CVpjcVAQIDAQABAoIBAAQ6ZGYxFBJcVfXO
TIoqyaNtRRaikvcBKWsOLpK0G3HdhjOMy/YuFix/qI2Bh3x7ax0Qy7PlC1ugLuus
hYIwusSwaPtU3aBLRaNEPerTljuImcHefmzwZGDLeQWmoLjNSlu9p+beWfAkrYmo
wd8R7F5CvnOMtCGKFz/SErBkBZQ7WcksEetxJ9sSMjuitc/QAOY9Yy6MH2WNQ0D1
ym6J95Y4rbe9dB1x7N19kkzh6iWxKGBwe1qSdTQk52aHC+mV77UOAsu+5wAACsHj
uxDyGvtGHZipt65lG6BcbtKvU54k3nK3NZF2t4H4/rcClhiZ+7TOpUG6JqDaU3sp
8vYYG9UCgYEA2XoPIRGY1bp8tHayhj4wE0dyyb/B6rY71XzQiNjURu8kK5x7ZmHO
9W5vKmnIB5NsWZVsXbS3AxY+FR3eBqMWcH2LFLSzTSV8ZQvsW7Q/DXsmKvvaRctm
6I6VogHAUGOgKeE2B54IDcW5W7pJw+qZjr9/YOoYb6upbhAcPHWUtxMCgYEAnzCw
4vsmiFSPnTa3b736rezJ89a/nEueL341yHqaNHGTuf0ItrpUQTK6K+vhW5l5J5/L
YOqW13XwlCzJfgzuVhlUrieYRkqiKcm8nt2PXX0mqqPue4RC6F50eApn8BJrAPrH
uErsQ4riXBaiUHB2/g18rN0Yt/Mi68JpPpKT4hsCgYASysBJ7DpXpZCnC2arwO3o
jJD3q/E2td5/uRRP7uSQ6JMojZIAK811WcCTWqalOPDw4L+HbVkvFBnzOyx3SFpl
CAQfmumvbeDM3nXjCrU0FgMbTb3zr/rJd6CoahxTFfi+Ba7kg5xQjFhS+8rp+fig
5B0+o5vO8ndmgk/eTY63LwKBgQCMzhSnsF402BbviCZYwEGq9XbsJ1b0eC5C8+++
lDMxkASKzjWW6tj7klL55no++1Eeq0jJQLaLfdZ+Od8/j1CA94bfREZ8ZidprvcX
zgGxP7Gj3bxjpGlJ/8IlK48j2iarhyaA7ERuubnemmaXn+KgdG2CuU3T50tMHXnZ
zuzfYQKBgDCiZURvuP6yNFa8DqftWgrNbqlXzk4Yexr0Zh28Yph6swPey/2ZzInA
0/Rf+alMi67YwmylDYxd70x4tKeS/w5nP2nM/PCSlfUxdItb35G3gNJcQl5z/coN
Gx/eiH43Uums+bWT/SM0Cw/c90gxHy4WURQGbNDGnSM3b/BySWp7
-----END RSA PRIVATE KEY-----";
    }
}else{
    die('2');
    $dbClientid = "463fc9c9-24b7-4388-ad78-0301a29b8cc0";
    $GUID = "6e76a741-9e2e-4726-9a5e-44959c4855b3";
    $dbPrivateKey= "-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAhzwsjR2kuXXbiRcoKeU9HzkpV8o560akslzof0XcA56OUJXd
o40Zr4SxDyC7qOfWFLqBRe8qMCtMEevoWbxnIIGlQxK8TZMLb84sHKyMUlBfQx+l
eSsExWKN0jqEnSy/iQ9SpnNLZmbo7au7gwkM5EcN56gAx64jeLs457nHzyVUv/i/
5aaqrLpFqYigd5VqpteXuSbwij4SQH8OxkzjmblDey1jUB4BNSTQNPRawVy0gemh
xKMotazq8EEfkOPUFOb9LTfNxbU0Pst02CsdDi01bLkuRaRSS+AlLoi+FWbSWcMQ
VHAA8LfKTsX8hA32+QHPGvO6rIks59CVpjcVAQIDAQABAoIBAAQ6ZGYxFBJcVfXO
TIoqyaNtRRaikvcBKWsOLpK0G3HdhjOMy/YuFix/qI2Bh3x7ax0Qy7PlC1ugLuus
hYIwusSwaPtU3aBLRaNEPerTljuImcHefmzwZGDLeQWmoLjNSlu9p+beWfAkrYmo
wd8R7F5CvnOMtCGKFz/SErBkBZQ7WcksEetxJ9sSMjuitc/QAOY9Yy6MH2WNQ0D1
ym6J95Y4rbe9dB1x7N19kkzh6iWxKGBwe1qSdTQk52aHC+mV77UOAsu+5wAACsHj
uxDyGvtGHZipt65lG6BcbtKvU54k3nK3NZF2t4H4/rcClhiZ+7TOpUG6JqDaU3sp
8vYYG9UCgYEA2XoPIRGY1bp8tHayhj4wE0dyyb/B6rY71XzQiNjURu8kK5x7ZmHO
9W5vKmnIB5NsWZVsXbS3AxY+FR3eBqMWcH2LFLSzTSV8ZQvsW7Q/DXsmKvvaRctm
6I6VogHAUGOgKeE2B54IDcW5W7pJw+qZjr9/YOoYb6upbhAcPHWUtxMCgYEAnzCw
4vsmiFSPnTa3b736rezJ89a/nEueL341yHqaNHGTuf0ItrpUQTK6K+vhW5l5J5/L
YOqW13XwlCzJfgzuVhlUrieYRkqiKcm8nt2PXX0mqqPue4RC6F50eApn8BJrAPrH
uErsQ4riXBaiUHB2/g18rN0Yt/Mi68JpPpKT4hsCgYASysBJ7DpXpZCnC2arwO3o
jJD3q/E2td5/uRRP7uSQ6JMojZIAK811WcCTWqalOPDw4L+HbVkvFBnzOyx3SFpl
CAQfmumvbeDM3nXjCrU0FgMbTb3zr/rJd6CoahxTFfi+Ba7kg5xQjFhS+8rp+fig
5B0+o5vO8ndmgk/eTY63LwKBgQCMzhSnsF402BbviCZYwEGq9XbsJ1b0eC5C8+++
lDMxkASKzjWW6tj7klL55no++1Eeq0jJQLaLfdZ+Od8/j1CA94bfREZ8ZidprvcX
zgGxP7Gj3bxjpGlJ/8IlK48j2iarhyaA7ERuubnemmaXn+KgdG2CuU3T50tMHXnZ
zuzfYQKBgDCiZURvuP6yNFa8DqftWgrNbqlXzk4Yexr0Zh28Yph6swPey/2ZzInA
0/Rf+alMi67YwmylDYxd70x4tKeS/w5nP2nM/PCSlfUxdItb35G3gNJcQl5z/coN
Gx/eiH43Uums+bWT/SM0Cw/c90gxHy4WURQGbNDGnSM3b/BySWp7
-----END RSA PRIVATE KEY-----";
}

$data = @parse_ini_file("ds_config.ini");
            // print_r($data);die;
$privateKey= $dbPrivateKey;
$uid = $GUID;
$clientId = $dbClientid;

$filepath = 'ds_config.ini';
$content = "";
$parsed_ini = parse_ini_file($filepath, true);

foreach($data as $section => $values){
    if($section === "DS_PRIVATE_KEY"){
      $content .= $section ."='". $privateKey . "'\n";
  }elseif($section === "DS_CLIENT_ID"){
    $content .= $section ."='". $clientId . "'\n";
}elseif($section === "DS_IMPERSONATED_USER_GUID"){
    $content .= $section ."='". $uid . "'\n";
}else{
    $content .= $section ."='". $values . "'\n";
}
}
              //write it into file
if (!$handle = fopen($filepath, 'w')) {
    return false;
}
$success = fwrite($handle, $content);
fclose($handle);
            // }
            // update_ini_file($data, $filepath);
//               print_r($data);die;
// echo get_cfg_var('DS_PRIVATE_KEY');die;


include_once 'ds_config.php';
require_once('vendor/autoload.php');
require_once('vendor/docusign/esign-client/autoload.php');
include_once 'lib/example_base.php';
include_once 'lib/send_envelope.php'; 
include_once 'lib/get_envelope.php';





$config = new DocuSign\eSign\Configuration();
$apiClient = new DocuSign\eSign\ApiClient($config);




$uploads_dir = 'asset/mailAttachmentsTemp/';

if (!is_dir($uploads_dir)) {
    mkdir($uploads_dir, 0777, true);
}
$unlink_files = array();
$ext = pathinfo($_FILES['invoice_attachment']['name'], PATHINFO_EXTENSION);
$pdf_attachment = rand().'.'.$ext;
$pdf_file_type = $_FILES['invoice_attachment']['type'];
$pdf_tmp_name = $_FILES["invoice_attachment"]["tmp_name"];
if (move_uploaded_file($pdf_tmp_name, $uploads_dir . $pdf_attachment)) {
    $unlink_file = 'asset/mailAttachmentsTemp/'.$pdf_attachment;  
    $pdfs = 'asset/mailAttachmentsTemp/'.$pdf_attachment;  

}

try {
        // print("\nSending an envelope...\n");
    $sendHandler = new SendEnvelope($apiClient);
    $result = $sendHandler->send($name,$email,$subject,$body,$pdfs);
    if($result->getStatus() == 'sent'){
        $sqlUpd = "UPDATE `invoice_details` SET `invoice_sent_status`= '".$result->getStatus()."', `docuEnvId` = '".$result->getEnvelopeId()."' WHERE `invoice_id` = ". $invoice_id."";
        mysqli_query($conn, $sqlUpd);

        $response = array(
            "status" => true,
            "pdf_status" => $result->getStatus(),
            "envelop_id" => $result->getEnvelopeId(),
            "msg" => "Invoice Sent Successfully."
        );
        echo json_encode($response);
        unlink($unlink_file);
                // return true;
    }else{
        $response = array(
            "status" => false,
            "msg" => "Invoice Not Sent."
        );
        echo json_encode($response);
        unlink($unlink_file);
        return false;
    }



        // printf("\nEnvelope status: %s. Envelope ID: %s\n", $result->getStatus(), $result->getEnvelopeId());
    
        // die;

        // // print("\n Envelope from the account...");
        //     $envelopesHandler= new GetEnvelope($apiClient);
        //     $envelope = $envelopesHandler->getEnvelope($result->getEnvelopeId());

        //     if(!is_null($envelope)) {
        //        $response = array(
        //         "status" => true,
        //         "pdf_status" => $envelope->getStatus(),
        //         "envelop_id" => $envelope->getEnvelopeId(),
        //         "msg" => "Invoice Get Successfully."
        //     );
        //        echo json_encode($response);
        //    } else {
        //     $response = array(
        //         "status" => false,
        //         "msg" => "Invoice Not Get."
        //     );
        //     echo json_encode($response);
        //     return false;
        // }
} catch (Exception $e) {
    print ("\n\nException!\n");
    print ($e->getMessage());

    if ($e instanceof DocuSign\eSign\ApiException) {
        print ("\nAPI error information: \n");
        print ($e->getResponseObject());
    }


}

