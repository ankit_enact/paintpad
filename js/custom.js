var base_url = window.location.origin + '/' + window.location.pathname.split ('/') [1] + '/';
const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

function setQuoteDetail(){
	//alert('chc');
	var quote_id = $("#quote_id").val();
	var subscriber_id = $("#subscriber_id").val();
	var contact_id = $("#contact_id_main").val();
	url = siteBaseUrl+'/webservice/quotes?subscriber_id='+subscriber_id+'&quote_id='+quote_id;

	$.ajax({
		url:url,
		method:'get',
		dataType:'JSON',
		success:function(response){
			//console.log(response)
			if(response.success == "1"){
				// console.log(response)
				var curQuote =response.data.quotes[0];
				
				
				// $("#quoteContactName").html(curQuote.contact.name)
				// $("#quoteContactEmailPhone").html(curQuote.contact.email+"<br>"+curQuote.contact.phone)

				if(curQuote.description == ""){
					$("#quoteNameDesc").html("Project "+curQuote.quote_id+" - "+curQuote.client_name)
				}
				else{
					$("#quoteNameDesc").html("Project "+curQuote.quote_id+" "+curQuote.description+" - "+curQuote.client_name)
				}
					
				$("#quoteContactSiteAddress").html(curQuote.site.formatted_addr)

				$("#quoteSiteName").html(curQuote.site.name);
				$("#quoteSiteEmail").html(curQuote.site.email);
				$("#quoteSitePhone").html(curQuote.site.phone);
				$("#quoteClientName").html(curQuote.client_name);

				if(curQuote.contact.image != ""){
					var useImg = curQuote.contact.image;
				}else{
					var useImg = base_url+"/image/profileIcon.png";
				}

				console.log(contact_id);
				// Client Detail And counts
				var html = '';
				html = '<div class="client_profile_detail"><span class="clientImg"><img id="" src="'+useImg+'" alt=""></span><span class="clientName" id="">'+curQuote.contact.name+'</span><span class="client_contact" id="">'+curQuote.contact.email+'<br>'+curQuote.contact.phone+'</span></div><div class="quote_count_detail"><div class="quote_count email emailCount" id=""><a href="'+base_url+'contact?contact_id='+contact_id+'&select=communications" >'+curQuote.counts.contacts+'</a></div><div class="quote_count invoice contactsCount" id=""><a href="'+base_url+'contact?contact_id='+contact_id+'&select=invoices" >'+curQuote.counts.docs+'</a></div><div class="quote_count quote quotesCount" id=""><a href="'+base_url+'contact?contact_id='+contact_id+'&select=quotes" >'+curQuote.counts.quotes+'</a></div><div class="quote_count addquote docsCount" id=""><a href="'+base_url+'contact?contact_id='+contact_id+'&select=appointments" >'+curQuote.counts.jss+'</a></div></div>';
				
				$('#quoteDetailLeft').html(html);
				$('#quoteNoteLeft').html(html);
				$('#quoteCommunicationLeft').html(html);

				$("#paintDefaultExists").val(curQuote.paintDefaultExists);
				$("#roomsExists").val(curQuote.roomsExists);

				$("#quote_status").val(curQuote.status);

				initializeEditQuotePopUp(curQuote);

				var li = document.getElementById('contact_'+curQuote.contact.contact_id);

				//$(li).click();
				
			}
			else{
				//dummy alert
				alert(response.message)
			}

		},
		error:function(err){
			console.log(err)
		}
	});
}

$(document).ready(function(){


	window.addEventListener('load', function(){
		var allimages= document.getElementsByClassName('lazy_load_image');
		for (var i=0; i<allimages.length; i++) {
			if (allimages[i].getAttribute('data-src')) {
				allimages[i].setAttribute('src', allimages[i].getAttribute('data-src'));
			}
		}
	}, false)


	var subscriber_id = $("#subscriber_id").val();
	var contact_id;
	var quoteTypes;
	var status;
	var from;
	var to;
	var quote_id;
	var if_filter_applied = 0;
	var site_url = $("#site_url").val();
	var undercoatOptions ='';

	function printBrandPrefDefaults(){
		var sbText = $("#selbrand option:selected").text();
		var stText = $("#seltiers option:selected").text();
		var dcText = $("#defaultCoat option:selected").text();
		var dplText = $("#defaultPrepLevel option:selected").text();
		var ucmText = "No Undercoats";

		if($("#underCoatMain:checked").length > 0){
			ucmText = "With Undercoats";
		}

		$('.room_default_setting').html(sbText+', '+stText+', '+dcText+', '+dplText+', '+ucmText)
	}

	if($("#ifUpdate").length > 0){
		printBrandPrefDefaults()
	}

	

	if($("#page_name").val() == "quoteEdit"){	
		setQuoteDetail();		
	}

	$(document).on('click','#editQuoteTrigger',function(){
		$('#InteriorQuote').modal('show');
		$('#menu2').removeClass('active')
		$('#menu2').removeClass('show')
		$('#new_client_link').removeClass('active')
		$('#new_client_link').removeClass('show')
		$('#existing_client_link').addClass('active')
		$('#menu3').addClass('in active show')
		//$('.existing_client_list').animate({ scrollTop: $('.existing_client_list li:nth-child(14)').position().top}, 200);

		var selected_client = $("#selected_client").val();
		setTimeout(function(){
			var listItem = $( "#contact_"+selected_client );
			//alert( "Index: " + listItem.index( ".existing_client_list li" ) );
			var lichild = listItem.index( ".existing_client_list li" );

			$('.existing_client_list').css("opacity",1);
			
			// console.log(lichild)


			if (lichild ==0) {

				$('.existing_client_list li:first-child').css("background",'#d5d5d5')

			}else{
				$('.existing_client_list').scrollTop($('.existing_client_list li:nth-child('+lichild+')').position().top);
				$('.existing_client_list li:nth-child('+lichild+')').next().css("background",'#d5d5d5')
				

			}


		}, 1000);

			
	});

	$(document).on('keyup','#search_existing_client',function(){
		var searchClient = $(this).val();
		searchClient = searchClient.toLowerCase();
		if(searchClient != ""){
		  $('.contact_li').show();
		  $(".client_name_list").each(function(){
		    var thisHtml = $(this).html();
		    thisHtml = thisHtml.toLowerCase();
		    if(thisHtml.indexOf(searchClient) < 0 ){
		      $(this).parent().parent().hide();
		    }
		  });
		}
		else{
			$('.contact_li').show();
		}
	});

	$(".fa-bars").click(function() {
		$(".menu").removeClass('menuClose');
		$(".menu").addClass('menuOpen');

		$(".mainClose").addClass('mainOpen');
		$(".mainOpen").removeClass('mainClose');

		$(".fa-bars").hide(500);
		$(".fa-times").show(500);
	});

	$(".fa-times").click(function() {
		$(".menu").addClass('menuClose');
		$(".menu").removeClass('menuOpen');

		$(".mainOpen").addClass('mainClose');
		$(".mainClose").removeClass('mainOpen');

		$(".fa-times").hide(500);
		$(".fa-bars").show(500);

	});

	$(document).on("click",".quoteTr",function() {
		window.location = $(this).data("href");
	});

	/*
	* Name: Jotpal
	* Date: 20180912
	* Comment: 
	*/
	function getQuoteStatus(status,site_url){
		statusStr = "Pending";
		statusImg = site_url+'/image/quote_pending_active.png';
		switch (status) {
		  case 2:
		      statusStr = "In-progress";
		      statusImg = site_url+'/image/quote_progress_active.png';
		      break;
		  case 3:
		      statusStr = "Completed";
		      break;
		  case 4:
		      statusStr = "Accepted";
		      break;
		  case 5:
		      statusStr = "Declined";
		      break;
		  case 6:
		      statusStr = "New";
		      break;
		  case 7:
		      statusStr = "Offered";
		      break;
		  case 8:
		      statusStr = "Open";
		      break;
		  default:
		      statusStr = "Pending";
		      break;
		}

		return [statusStr,statusImg];
	}

	/*
	* Name: Jotpal
	* Date: 20180912
	* Comment: 
	*/
	function getQuotes(event,page){

	    if(if_filter_applied == 1){
	      var data = {"subscriber_id":subscriber_id,"contact_id":contact_id,"type":quoteTypes,"status":status,"from":from,"to":to,"quote_id":quote_id,"page":page}
	    }
	    else{
	      var data = {"subscriber_id":subscriber_id,"page":page}
	    }
	    var quote_url = $("#quote_url").val();

	    $.ajax({
	          url:quote_url,
	          method:'get',
	          data:data,
	          dataType:'JSON',
	          beforeSend: function(){
	          	$(".app-ovelray").fadeIn();
	          },
	          success:function(response){

	            if(response.success == "1"){
	              page = parseInt(page)+1;
	              var quote_view_url = $("#quote_view_url").val();
	              $("#quote_page").val(page);
	              if(response.data != "" && response.data.quotes.length > 0){
	                  var str = "";
	                  for (var i = 0; i < response.data.quotes.length; i++) {
	                      var curData = response.data.quotes[i];

	                      statusList = getQuoteStatus(curData['status'],site_url);
	                      statusStr = statusList[0];
	                      statusImg = statusList[1];

	                      // console.log(statusStr)

	                      if (statusStr=="New") {
	                      	statusImg = "/paintpad/image/quoteStatus/status-new.png"
	                      }
	                      if (statusStr=="Completed") {
	                      	statusImg = "/paintpad/image/quoteStatus/status-completed.png"
	                      }
	                      if (statusStr=="Accepted") {
	                      	statusStr = "Approved";
	                      	statusImg = "/paintpad/image/quoteStatus/status-approved.png"
	                      }
	                      if (statusStr=="Declined") {
	                      	statusImg = "/paintpad/image/quoteStatus/status-declined.png"
	                      }
	                      if (statusStr=="Offered") {
	                      	statusStr = "Submitted";
	                      	statusImg = "/paintpad/image/quoteStatus/status-submitted.png"
	                      }
	                      if (statusStr=="Open") {
	                      	statusImg = "/paintpad/image/quoteStatus/status-open.png"
	                      }
	                      


	                      var d = new Date(curData['date']*1000);
	                      var curDate = d.getDate();
	                      if(curDate < 10){
	                        curDate = String(curDate);
	                        curDate= 0 + curDate;
	                      }
	                      
	                      
	                      var curMonth = monthNames[d.getMonth()]

	                      typeStr = (curData['type'] == 1)?'Interiors':'Exteriors';


	                      var curNote  = ( curData['description'] != null ) ? curData['description'] : '';
	                      // console.log(curData['note'])


	                      str += '<tr class="quoteTr" data-href="'+quote_view_url+'/'+curData['quote_id']+'">'+
	                        '<td><small>'+curMonth+'</small><h4>'+curDate+'</h4></td>'+
	                        '<td><span>'+curData['quote_id']+'</span><h3 class="name">'+curData['contact']['name']+'</h3><span class="phnNo">'+curData['contact']['phone']+'</span></td>'+
	                        '<td>'+curNote+'</td>'+
	                        '<td><div class="interior"><img src="'+site_url+'/image/quotes_Interior_icon.png"><span>'+typeStr+'</span></div></td>'+
	                        '<td><div class="interior"><img src="'+site_url+'/image/quote_price-icon.png"><span>$'+curData['price']+'</span></div></td>'+
	                        '<td><div class="interior statusReport"><img style="width:40px" src="'+statusImg+'"><span>'+statusStr+'</span></div></td>'+
	                      '</tr>';
	                  }
	                  if(event == "endScroll"){
	                    $("#mCSB_1_container").append(str);
	                  }
	                  else{
	                    $("#mCSB_1_container").html(str);
	                  }
	              }
	              else{
	                if(event != "endScroll"){
	                  $("#mCSB_1_container").html("<tr><td style='text-align: center;' colspan='6'>No Results Found.</td></tr>");
	                }
	              }
	          		
	          		$(".app-ovelray").fadeOut();



	            }
	            else{

	            }
	          },
	          error:function(error){
	          		$(".app-ovelray").fadeOut();


	          },
	    });
	}

	/*
	* Name: Jotpal
	* Date: 20180912
	* Comment: 
	*/
	/*
		$(document).on('scroll','#mCSB_1', function () {
		//mCSB_1_container
		console.log($(this).scrollTop())
		  if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
		    var page = $("#quote_page").val();
		    getQuotes('endScroll',page);
		  }
		})
	*/

	if($('#quotesTbody').length > 0){

		$('#rooms_tab_list').mCustomScrollbar({
			callbacks:{
				onTotalScroll:function(){
					var page = $("#quote_page").val();
					getQuotes('endScroll',page);
				}
			}
		});

	}


	if($('#allinvoicesTbody').length > 0){

		$('#allinvoicesTbody').mCustomScrollbar({
			callbacks:{
				onTotalScroll:function(){
					var page = $('#pagevalAllinvoices').data("value");
					var company_id = $('#pagevalAllinvoices').data("company");
					var contact_id = 0;
					var filter = 0;
					getAllinvoices(company_id,contact_id,page,filter,'endScroll');
				}
			}
		});

	}

	if($('#paintorderTbody').length > 0){

		$('#paintorderTbody').mCustomScrollbar({
			callbacks:{
				onTotalScroll:function(){
					var page = $('#pagevalPaintoreder').data("value");
					var company_id = $('#pagevalPaintoreder').data("company");
					var subscriber_id = $('#pagevalPaintoreder').data("subscriber");
					var filter = 0;
					getSubscriberOreders(company_id,subscriber_id,page,filter,'endScroll');
				}
			}
		});

	}


	if($('#colorsMainDiv').length > 0){

		$('#colorsMainDiv').mCustomScrollbar({
			callbacks:{
				onTotalScroll:function(){
					var page = $('#colorsMainDiv').data("page");
					var company_id = $('#colorsMainDiv').data("company_id");
					var subscriber_id = $('#colorsMainDiv').data("subscriber_id");
					var filter = $('#searchColor').val();
					getProductColors(company_id,subscriber_id,page,filter,'endScroll');
				}
			}
		});

	}

	$(document).on('change','.searchTag-dropdown',function(){
		var page = $('#colorsMainDiv').data("page");
		var company_id = $('#colorsMainDiv').data("company_id");
		var subscriber_id = $('#colorsMainDiv').data("subscriber_id");
		var filter = '';
		getProductColors(company_id,subscriber_id,page,filter,1);
	});

	$(document).on('click','.default_heading span',function(){
		$('.searchTag-dropdown option[value="0"]').prop('selected',true);
		$('#searchColor').val('');
		var page = $('#colorsMainDiv').data("page");
		var company_id = $('#colorsMainDiv').data("company_id");
		var subscriber_id = $('#colorsMainDiv').data("subscriber_id");
		var filter = '';
		getProductColors(company_id,subscriber_id,page,filter,1);
	});

	// For colour search
		//on keyup, start the countdown
		var typingTimer;                //timer identifier
		var doneTypingInterval = 500;  
		var $inputColour = $('#searchColor');
		$inputColour.on('keyup', function () {
			clearTimeout(typingTimer);
			var page = $('#colorsMainDiv').data("page");
			var company_id = $('#colorsMainDiv').data("company_id");
			var subscriber_id = $('#colorsMainDiv').data("subscriber_id");
			var filter = $('#searchColor').val();
			
			typingTimer = setTimeout(function() {
			    getProductColors(company_id,subscriber_id,page,filter,1);
			}, doneTypingInterval)
		});

		//on keydown, clear the countdown 
		$inputColour.on('keydown', function () {
		  clearTimeout(typingTimer);
		});


	if($('#invoiceBody').length > 0){

		$('#invoiceBody').mCustomScrollbar({
			callbacks:{
				onTotalScroll:function(){
					var sid = $('#subscriber_id').val();
					var id = $('.activeContact').data('id');					
					getContactInvoices(id,sid);
				}
			}
		});

	}

	// Comment by Ankit
	// if($('#contact_list').length > 0){

	// 	$('#contactListMain').mCustomScrollbar({
	// 		callbacks:{
	// 			onTotalScroll:function(){
	// 				var page = $('#contactListMain').data('page');					
	// 				getContactAllContacts(page);
	// 			}
	// 		}
	// 	});

	// }


	$(document).on('click','#paintorderTbody tr',function(){		
		// console.log($(this).data('paintdetail'));
		var paintDetail = $(this).data('paintdetail');
		var pdfUrl = $(this).data('pdfurl');
		var deliverTo = $(this).data('deliverto');
		var note = $(this).data('note');

		$('.subscriber-order-pdf').attr('href',pdfUrl);

		var detailHtml = '<table class="subscriber-order-tables Quotetables modal-first"><thead><tr><th>Brand</th><th>Product</th><th>Sheen</th><th>This Order (L)</th><th>Colour</th></tr></thead></tbody>';

		$.each(paintDetail, function(d,i){
			if(i != ''){
				if(i["color"] != ''){
					var colour = i["color"];
				}else{
					var colour = "-";
				}
				detailHtml += '<tr>';
				detailHtml += '<td><span>'+i["brand"]+'</span></td> ';
				detailHtml += '<td><span>'+i["product"]+'</span></td> ';
				detailHtml += '<td><span>'+i["sheen"]+'</span></td> ';
				detailHtml += '<td><span>'+i["litres"]+'</span></td> ';
				detailHtml += '<td><span>'+colour+'</span></td> ';
				detailHtml += '</tr> ';
			}			
		});
		detailHtml += '</table><table class="subscriber-order-tables Quotetables"><tr><td colspan="2" style="vertical-align:top;">Deliver To</td><td colspan="3"><span class="deliverTo">'+deliverTo+'</span></td></tr> <tr rowspan="2"><td colspan="2" style="vertical-align:top;">Add a Note:</td><td colspan="3"><span class="notes">'+note+'</span></td></tr> </tbody></table>';
    	$(".subscriber-order-modal-body").html(detailHtml);

	});

	function getAllinvoices(company_id,contact_id,page,filter,event) {
		// $('#loader').show();
	    var data = '{"company_id":"'+company_id+'", "contact_id" : '+contact_id+', "page": "'+page+'", "filter":'+filter+' }';
		$.ajax({
			url: siteBaseUrl+'/webservice/get-all-invoices',
			type: 'post',
			data: {'json':data},
			success: function (response) {
				var response = JSON.parse(response);
				if(response.success == 1){
					var str = '';
					$.each(response.data.payments, function(d,i){

						var part_payment = (i['part_payment'] % 1 === 0) ? i['part_payment'] : parseFloat(i['part_payment']).toFixed(2);
						var amount = (i['amount'] % 1 === 0) ? currencyFormat(i['amount']) : currencyFormat(parseFloat(i['amount']).toFixed(2));
					    var descp = (i['descp'] != '') ? i['descp'] : '-';
					    var payment_date = (i['payment_date'] != '') ? i['payment_date'] : '-';
						str += "<tr><td><span><a href='"+i['qb_invoice_pdf']+"' target='_blank'>"+i['qb_invoice_DocNumber']+"</a></span></td><td><span>"+i['quote_id']+"</span></td><td style='width: 35%;'><span>"+descp+"</span></td><td><span>"+part_payment+"</span></td><td><span>"+amount+"</span></td><td><span>"+i['qb_invoice_status']+"</span></td><td><span>"+payment_date+"</span></td></tr>";
					});
					var nextpage = $('#pagevalAllinvoices').data('value')+1;
					
					$('#pagevalAllinvoices').attr("data-value",nextpage);
					$('#pagevalAllinvoices').data("value",nextpage);

					if(event == "endScroll"){
	                  $("#mCSB_1_container").append(str);
	                }else{
	                  $("#mCSB_1_container").html(str);
	                }
				}else{
					alert(response.message);
					$('#pagevalAllinvoices').removeAttr("data-value");
					$('#pagevalAllinvoices').removeClass();
				}
				$('#loader').hide();
			},
			error: function () {
				$('#loader').hide();
				alert("Something went wrong");
			}
		});
	}

	$(document).on('click','#add_room_main',function(){
		// $('#loader').show();
		var company_id = $('#company_id').val();
		var type_id = $('#quote_type').val();
		var room_id = 0;
		$.ajax({
			url: siteBaseUrl+'/webservice/rooms?company_id='+company_id+'&room_id='+room_id+'&type_id='+type_id,
			type: 'get',
			success: function (response) {
				console.log(response);
				var response = JSON.parse(response);
				if(response.success == 1){
					var str = '';
					$.each(response.data.componentGroup, function(d,i){

						str += '<li><label class="checkbox_label">'+i['name']+'<input type="checkbox" name="roomComponent[]" class="roomComponent" value="'+i['id']+'"><span class="checkmark"></span></label></li>';
					});
					
	                  $(".paint_lft_form .room-comp-list").html(str);
				}

				$('#loader').hide();
			},
			error: function () {
				$('#loader').hide();
				alert("Something went wrong");
			}
		});
	});
// Add Custome Item Row to room special item tab
	$(document).on('click','.cross_row',function(){
		$(this).closest('tr').remove();
	});

	// $(document).on('click','#add-custome-specialItem-btn',function(){
	// 	var str = '';
	// 	str += '<tr><td><div class="table_checkbox"><label class="checkbox_label"><input type="checkbox" checked="checked"><span class="checkmark"></span></label></div></td><td><input type="text" class="form-control item_input" name="" placeholder="" value=""></td>';
	// 	str += '<td><input type="number" class="form-control item_input" name="" placeholder="" value="0"></td><td><span class="cross_row" style="color: blue;">&times;</span></td>';
	// 	str += '<td><input type="number" class="form-control item_input" name="" placeholder="" value="0"></td>';
	// 	str += '<td><div class=""><input type="number" value="" Placeholder="" class="form-control number_item"></div></td>';
	// 	str += '<td><input type="number" value="" value="1" class="form-control number_item2"></td>';
	// 	str += '<td><div class="table_checkbox"><span class="cross_row">&#9432;</span><label class="checkbox_label"><input type="checkbox" checked="checked"><span class="checkmark"></span></label></div></td>';
	// 	str += '<td><p style="font-size: 17px">0.00</p></td><td><span class="cross_row">&times;</span></td></tr>';
	// 	$("#special_item_list tbody").append(str);
	// });

	$(document).on('click','#add-custome-specialItem-btn',function(){
		addspecialitemsrow();
	});

	$(document).on('change','#selectSpecialItemMain',function(){
		var name = $(this).children("option:selected").text();
		var price = $(this).children("option:selected").data('price');
		addspecialitemsrow(name,price);
	});

	function addspecialitemsrow(name='',price=''){
		var price = (price != '') ?  price : 0;
		var str = '';
		var oldFinal = $('#totalRoomSiPrice').text();
		oldFinal = (oldFinal != '') ? oldFinal : 0;

		str += '<tr><td><div class="table_checkbox"><label class="checkbox_label"><input type="checkbox" checked="checked"><span class="checkmark"></span></label></div></td><td><input type="text" class="form-control item_input" name="" placeholder="" value="'+name+'"></td>';
		str += '<td><input type="number" class="form-control item_input" name="labour" placeholder="" min="0" value="0"></td><td><span class="cross_row" style="color: blue;">&times;</span></td>';
		str += '<td><input type="number" class="form-control item_input" name="hours" placeholder="" min="0" value="0"></td>';
		str += '<td><div class=""><input type="number" name="mat_price" value="'+parseFloat(price).toFixed(2)+'" Placeholder="" class="form-control item_input number_item"></div></td>';
		str += '<td><input type="number" value="1" name="qty" class="form-control item_input number_item2"></td>';
		str += '<td><div class="table_checkbox"><span class="cross_row">&#9432;</span><label class="checkbox_label"><input type="checkbox" checked="checked"><span class="checkmark"></span></label></div></td>';
		str += '<td><p style="font-size: 17px" class="rowTotal">'+parseFloat(price).toFixed(2)+'</p></td><td><span class="cross_row">&times;</span></td></tr>';		
		
	    var sumFinal = (parseFloat(oldFinal) + parseFloat(price)).toFixed(2); 

		$("#special_item_list_main tbody").append(str);
	    $('.totalPriceRoomSi').show();
	    $('#totalRoomSiPrice').html(sumFinal);
	}

	$(document).on('keyup change','.item_input',function(){
		rowSumRoomSpecialItem($(this).closest('tr'));
	});
	function rowSumRoomSpecialItem(selectRow){
		var labour = selectRow.find('input[name="labour"]').val();
		var hours = selectRow.find('input[name="hours"]').val();
		var price = selectRow.find('input[name="mat_price"]').val();
		var qty = selectRow.find('input[name="qty"]').val();

		var calC = (parseFloat(labour) * parseFloat(hours)) + (parseFloat(price) * parseFloat(qty));
		calC = calC.toFixed(2);
		selectRow.find('.rowTotal').text(calC);
		var fTot = parseFloat('0');
		$.each($('#special_item_list_main tbody .rowTotal'),function(i,x){
			fTot += parseFloat($(x).text());
		});
		fTot = fTot.toFixed(2);
		$('#totalRoomSiPrice').html(fTot);

	}

	$(document).on('click','.quoteDetailMainTabs',function(){
		var tab = $(this);
		var tabSelector = tab.data('href');

		var company_id = $('#company_id').val();
		var quote_id = $('#quote_id').val();
		var type_id = $('#quote_type').val();
		var subscriber_id = $('#subscriber_id').val();

		if(tabSelector == '#panel5'){
			// get quote summary
			var data = '{"company_id":"'+company_id+'", "quote_id" : "'+quote_id+'", "sub_id" : "'+subscriber_id+'" }';
			$.ajax({
				url: siteBaseUrl+'/webservice/get-quote-summary',
				type: 'post',
				data : {'json':data},
				success: function (response) {
					var response = JSON.parse(response);
					console.log(response);
					if(response.success == 1){
						var str = '';
						var finalBal = 0;
						$.each(response.data.payments, function(d,i){
							// str += '<option value="'+i['id']+'" data-price="'+i['price']+'">'+i['name']+'</option>';
							var payAmt = tofixed(i['amount'],2);
							var payPercent = tofixed(i['part_payment'],1);
							i['payment_date'] = (i['payment_date'] != '') ? i['payment_date'] :'-' ;

							if(i['final_balance'] == 1){
								str += '<tr><td><strong>Balance</strong></td>';
								str += '<td><p id="finalBalance">'+payAmt+'</p></td>';
								str += '<td></td>';
								str += '<td><input id="datepicker2" value="'+i['payment_date']+'" class="form-control datepicker-input" /></td></tr>';
							}else{
								finalBal += +parseInt(i['amount']);
								str += '<tr><td><textarea class="description">'+i['descp']+'</textarea></td>';
								str += '<td><input type="text" class="form-control payment-input" name="" value="'+payAmt+'"></td>';
								str += '<td><input type="text" class="form-control percent-input text-center" name="" value="'+payPercent+'"></td>';
								str += '<td><input id="datepicker" class="form-control datepicker-input"   value="'+i['payment_date']+'" /><span class="summary-close-icon">&times;</span></td></tr>';
							}
						});
		                $("#summary_detail_table tbody").html(str);

		                // Payment Notes
		                var str = '<option value="0">Select Payment Notes</option>';
						$.each(response.data.paymentNotes, function(d,i){	
							str += '<option value="'+i['id']+'">'+i['name']+'</option>';
						});
		                $("#paymentNoteOptions").html(str);

		                // Payment Options
		                var str = '';
						$.each(response.data.paymentOptions, function(d,i){	
							str += '<option value="'+i['id']+'">'+i['name']+'</option>';
						});
		                $("#paymentOptions").html(str);

		                // Payment Method
		                var str = '';
						$.each(response.data.paymentMethods, function(d,i){	
							str += '<option value="'+i['id']+'">'+i['name']+'</option>';
						});
		                $("#paymentMethods").html(str);

		                // Quote Inclusions Rooms Side Bar
		                var str = '';
						$.each(response.data.quoteInclusions, function(d,i){
							str += '<li><p class="text-left">'+i['name']+'</p><span class="text-right">'+i['count']+'</span></li>';
						});
		                $("#quoteInclusions").html(str);

		                $('#headerDesc').text(response.data.headerDesc);
		                $('#footerDesc').text(response.data.footerDesc);

		                $('#authorisedPerson').val(response.data.authorisedPerson);

		                $('#noOfPainters').val(response.data.noOfPainters);
		                $('#daysToComplete').data('totalHours',response.data.totalHours);
		                $('#daysToComplete').text(response.data.daysToComplete);

		                $('#sideBarSubTot span').text('$'+tofixed(response.data.totalExGst,2));
		                $('#sideBarGst span').text('$'+tofixed(response.data.gst,2));
		                $('#sideBarTotalWithGst span strong').text('$'+tofixed(response.data.totalInGst,2));
					}

					$('#loader').hide();
				},
				error: function () {
					$('#loader').hide();
					alert("Something went wrong");
				}
			});

			//get all common special items
			$.ajax({
				url: siteBaseUrl+'/webservice/special-items?company_id='+company_id+'&type_id='+type_id,
				type: 'get',
				success: function (response) {
					var response = JSON.parse(response);
					if(response.success == 1){
						var str = '<option value="0">Select special item</option>';
						$.each(response.data.items, function(d,i){
							str += '<option value="'+i['id']+'" data-price="'+i['price']+'">'+i['name']+'</option>';
						});
						
		                $(".special_item_div select[name='update_status']").html(str);
					}

					$('#loader').hide();
				},
				error: function () {
					$('#loader').hide();
					alert("Something went wrong");
				}
			});
		}

		if(tabSelector == '#panel7'){

			var data = '{"company_id":"'+company_id+'", "quote_id" : "'+quote_id+'" }';
			getQuoteNotes(data);
			
		}
		if(tabSelector == '#panel6'){
			var data = '?company_id='+company_id+'&quote_id='+quote_id+'&subscriber_id='+subscriber_id;
			getQuoteCommunications(data);
		}
	});


	function getQuoteCommunications(data){
		$.ajax({
				url: siteBaseUrl+'/webservice/communication'+data,
				type: 'get',
				success: function (response) {
					var response = JSON.parse(response);
					console.log(response);
					if(response.success == 1){
						var str = '';
						$.each(response.data.emails, function(d,i){
							var d = new Date(parseInt((i['date']*1000)));
							

	                  		var months = {0 : "Jan",1 : "Feb",2 : "Mar",3 : "Apr",4 : "May",5 : "Jun",6 : "Jul",7 : "Aug",8 : "Sep",9 : "Oct",10 : "Nov",11 : "Dec"};
							var days = {0 : "Sun",1 : "Mon",2 : "Tue",3 : "Wed",4 : "Thu",5 : "Fri",6 : "Sat"};

							var date = days[d.getDay()]+', '+ d.getDate() + months[d.getMonth()] +' '+ d.getFullYear() ;

	                  		str += '<div class="mail_box"><div class="inner_mail_box"><h4 class="mail_sub">'+i['subject']+'</h4><div class="form-group"><label>From</label><p>'+i['From']+'</p></div><div class="form-group"><label>Date</label><p> '+date+' '+d.getHours()+':'+d.getMinutes()+'</p></div><div class="form-group"><label>To</label><p>'+i['to']+'</p></div><div class="mail-content-box"><p>'+i['body'].replace("\n", "<br />", "g")+'</p><a href="'+siteBaseUrl+'/communications/view?id='+i['comm_id']+'" class="text-right">View More</a></div></div></div>';
						});
		                $("#quoteCommunicationContent").html(str);
					}

					$('#loader').hide();
				},
				error: function () {
					$('#loader').hide();
					alert("Something went wrong");
				}
			});
	}


	$(document).on('click','.add-room-tab .nav-link',function(){
		// $('#loader').show();
		var tab = $(this);
		var tabSelector = tab.attr('href');

		var company_id 	  = $('#company_id').val();
		var quote_id 	  = $('#quote_id').val();
		var type_id 	  = $('#quote_type').val();
		var subscriber_id = $('#subscriber_id').val();

		if(tabSelector == '#summary_special-item'){
			// Get All Special Items
			
		}
		
		if(tabSelector == '#summary_quantities'){
			var data = '{"company_id":"'+company_id+'", "quote_id" : "'+quote_id+'" }';
			$.ajax({
				url: siteBaseUrl+'/webservice/get-room-quantities',
				type: 'post',
				data : {'json':data},
				success: function (response) {
					var response = JSON.parse(response);
					console.log(response);
					if(response.success == 1){
						//Set Special Items
						var strSi = '<table id="special_item_table_quantities" class="tabs_table table table-borderless table-condensed table-hover"><thead><tr><th>Special Items</th><th>Qty</th></tr></thead><tbody>';
						$.each(response.data.allSpecialItems, function(d,i){
							strSi += '<tr><td>'+i['name']+' - '+i['desc']+'</td><td>'+i['qty']+'</td></tr>';
						});		
						strSi += '</tbody></table>';				
		                

		                //Set Substrate Data
		                var strSbustrate = '<table id="bycomponent_table_summary" class="tabs_table table table-borderless table-condensed table-hover"><thead><tr><th>Room</th><th>Substrate</th><th>Type</th><th>Material</th><th>Colour</th><th>Hours</th><th>Litre</th></tr></thead><tbody>';
		                var totLtr = parseFloat(0);
		                var totHrs = parseFloat(0);
						$.each(response.data.substrate, function(a,b){
							totLtr += parseFloat(b['litre']);
							totHrs += parseFloat(b['hours']);
							var color = (b['isUndercoat'] != 1) ? ((b['custom_color_name'] != '') ? b['custom_color_name'] : b['color']) : '-';
							
							strSbustrate += '<tr><td>'+b['room_name']+'</td><td>'+b['substrate']+'</td><td>'+b['type']+'</td><td>'+b['material']+'</td><td>'+color+'</td><td>'+(b['hours']).toFixed(2)+'</td><td>'+(b['litre']).toFixed(2)+'</td></tr>';
						});
						strSbustrate += '<tr><td colspan="4"></td><td><span>Total</span></td><td><span>'+totHrs.toFixed(2)+'</span></td><td><span>'+totLtr.toFixed(2)+'</span></td><tr>';
						strSbustrate += '</tbody></table>';

		                $("#bycomponentMain").html(strSbustrate+strSi);

						//Set Material Data
		                var strMaterial = '<table id="byproduct_table_summary" class="tabs_table table table-borderless table-condensed table-hover"><thead><tr><th>Material</th><th>Colour</th><th>Qty</th><th>Price</th></tr></thead><tbody>';
		                var paintSubTotQty = parseFloat(0);
		                var paintSubTotPrice = parseFloat(0);
						$.each(response.data.material, function(e,f){
							paintSubTotQty += parseFloat(f['qty']);
							paintSubTotPrice += parseFloat(f['price']);
							var color = (f['custom_color_name'] != '') ? f['custom_color_name'] : f['color'];
							
							strMaterial += '<tr><td>'+f['material']+'</td><td>'+color+'</td><td>'+f['qty']+'</td><td>'+(f['price']).toFixed(2)+'</td></tr>';
						});
						strMaterial += '<tr><td></td><td><span><b>Paint Sub Total</b></span></td><td><span>'+(response.data.total.paintSubTotal.qty).toFixed(2)+'</span></td><td><span>$'+(response.data.total.paintSubTotal.price).toFixed(2)+'</span></td></tr>';
						strMaterial += '<tr><td></td><td><span><b>Application</b></span></td><td><span>'+(response.data.total.appliaction.qty).toFixed(2)+'</span></td><td><span>'+(response.data.total.appliaction.price).toFixed(2)+'</span></td></tr>';
						strMaterial += '<tr><td></td><td><span><b>Paint Prepration</b></span></td><td><span>'+(response.data.total.paintPreparation.qty).toFixed(2)+'</span></td><td><span>'+(response.data.total.paintPreparation.price).toFixed(2)+'</span></td></tr>';
						strMaterial += '<tr><td></td><td><span><b>Application Prepration</b></span></td><td><span>'+(response.data.total.appPreparation.qty).toFixed(2)+'</span></td><td><span>'+(response.data.total.appPreparation.price).toFixed(2)+'</span></td></tr>';
						strMaterial += '<tr><td></td><td><span><b>Sub Total</b></span></td><td><span></span></td><td><span>'+(response.data.total.subTotal.price).toFixed(2)+'</span></td></tr>';
						strMaterial += '</tbody></table>';

						var strSiMat = '<table id="special_item_table_quantities" class="tabs_table table table-borderless table-condensed table-hover"><thead><tr><th>Special Items</th><th>Qty</th><th>Price</th></tr></thead><tbody>';
						$.each(response.data.allSpecialItems, function(d,i){
							strSiMat += '<tr><td>'+i['name']+' - '+i['desc']+'</td><td>'+i['qty']+'</td><td>$'+i['price']+'</td></tr>';
						});		
						strSiMat += '<tr><td></td><td>Margin</td><td>'+(response.data.specialItemTotal.margin).toFixed(2)+'</td></tr>';
						strSiMat += '<tr><td></td><td>Total(ex. GST)</td><td>'+(response.data.specialItemTotal.totalExGst).toFixed(2)+'</td></tr>';
						strSiMat += '<tr><td></td><td>GST</td><td>'+(response.data.specialItemTotal.gst).toFixed(2)+'</td></tr>';
						strSiMat += '<tr><td></td><td>Grand Total</td><td>'+(response.data.specialItemTotal.grandTotal).toFixed(2)+'</td></tr>';
						strSiMat += '</tbody></table>';	

						$("#byproductMain").html(strMaterial+strSiMat);
		                
					}

					$('#loader').hide();
				},
				error: function () {
					$('#loader').hide();
					alert("Something went wrong");
				}
			});
		
		}


		if(tabSelector == '#summary_Invoices'){
			var data = '{"company_id":"'+company_id+'", "quote_id" : "'+quote_id+'", "sub_id" : "'+subscriber_id+'" }';
			getQuantityInvoices(data);
		}
	
	});

	function getQuantityInvoices(data){
		$.ajax({
				url: siteBaseUrl+'/webservice/get-quote-scheduled-payments',
				type: 'post',
				data : {'json':data},
				success: function (response) {
					var response = JSON.parse(response);
					console.log(response);
					if(response.success == 1){
						//Set Special Items
						if(response.data.payments != ''){
						var strSi = '<table id="Invoice_table_quantities" class="tabs_table table table-borderless table-condensed table-hover"><thead><tr><th>Invoice Id</th><th>Description</th><th>%</th><th>Amount</th><th>Create/Send</th><th>Paid</th><th>Date</th></tr></thead><tbody>';
						$.each(response.data.payments, function(d,i){
							var payment_date = (i['payment_date'] != null) ? i['payment_date'] : '-';
							var amount = parseFloat(i["amount"]);
							var part_payment = parseFloat(i["part_payment"]);
							strSi += '<tr id="payment_id_'+i['payment_schedule_id']+'" class="payment_tr">';
							if(i['qb_invoice_DocNumber'] != null){
								strSi += '<td  class="invoice_docNum"><a href="'+i['qb_invoice_pdf']+'"  target="_blank">'+i['qb_invoice_DocNumber']+'</a></td>';
							}else{
								strSi += '<td  class="invoice_docNum"><a href="javascript:void(0)"  target="_blank">-</a></td>';
							}
							strSi += '<td class="descp">'+i['descp']+'</td>';
							strSi += '<td class="part_payment">'+part_payment.toFixed(1)+'</td>';
							strSi += '<td>$'+amount.toFixed(2)+'</td>';
							if(i['qb_invoice_DocNumber'] != null){
								strSi += '<td class="create_send" data-href="'+i['payment_schedule_id']+'"  data-quickbookmemo="'+i['qb_invoice_memo']+'" data-qbinvoiceid="'+i['qb_invoice_id']+'" style="color:ornage;">Recreate/Resend</td>';
							}else{
								strSi += '<td class="create_send" data-href="'+i['payment_schedule_id']+'" data-qbinvoiceid="'+i['qb_invoice_id']+'" data-quickbookmemo="'+i['qb_invoice_memo']+'" style="color:blue;">Create/Send</td>';
							}
							
							strSi += '<td class="invoice_status">'+i['qb_invoice_status']+'</td>';
							strSi += '<td>'+payment_date+'</td></tr>';
						});		
						strSi += '</tbody></table>';				
		                
						$("#invoices_div_main").html(strSi);
		                }
					}
					$('#loader').hide();
				},
				error: function () {
					$('#loader').hide();
					alert("Something went wrong");
				}
			});		
	}

	function getQuoteNotes(data){
		$.ajax({
				url: siteBaseUrl+'/webservice/get-quote-notes',
				type: 'post',
				data: {'json':data},
				success: function (response) {
					var response = JSON.parse(response);
					if(response.success == 1){
						var str = '';
						$.each(response.data.notes, function(d,i){
							var d = new Date(parseInt((i['created_at']*1000)));
	                  		var date = ("0" + d.getDate()).slice(-2) +'/'+ ("0" + (d.getMonth()+1)).slice(-2) +'/'+ d.getFullYear();
	                  		var ampm = d.getHours() >= 12 ? 'pm' : 'am';
	                  		var hours = d.getHours() % 12;
	  						hours = hours ? hours : 12;
	                  		var time = ("0" + hours).slice(-2) +':'+ ("0" + (d.getMinutes())).slice(-2) +':'+ ampm ;
							str += '<li><div class="row"><div class="col"><span class="note_img"><img width="77" height="66" style="border-radius:7px;" src="'+i['image']+'" alt=""></span></div><div class="col-md-8"><p>'+i['description']+'</p><small>'+date+' '+time+' </small></div><div class="col text-right"><a href="javascript:void(0)"  data-noteid="'+i['id']+'" class="note_trash"><img src="https://enacteservices.net/paintpad/image/delete.png" alt=""></a></div></div></li>';
						});
		                $(".site_note_div_main .note_list").html(str);
					}

					$('#loader').hide();
				},
				error: function () {
					$('#loader').hide();
					alert("Something went wrong");
				}
			});
	}

	//Quote Note form
	$(document).on("click",".site_note_div_main .noteTopIcons a", function(e){
		var html = '';
		html ='<div>Description : <textarea rows="5" style="width: 55%" id="tbl_quote_notes" name="tbl_quote_notes"></textarea><br>Invoice Memo : <img width="77" height="66" src="https://enacteservices.net/paintpad/image/edit.png" id="quote_note_img" onclick="$(\'#quote_note_img_input\').trigger(\'click\');"><input type="file" name="image_0" id="quote_note_img_input" style="display: none;"><input type="hidden" name="imageCount" id="quoteNote_imageCount" value="1"><button type="button" name="dismiss_modal" data-dismiss="modal" style="display: none;"></button></div>'; 
		$('#saveQuoteSiteNoteForm #modal-body').html(html);
		$('#saveQuoteSiteNoteForm').modal('show');
	});
	//

	$("#quote_note_form").on('submit', function(e){
		e.preventDefault();
		var quote_id 	  = $('#quote_id').val();
		var tbl_quote_notes = $('#tbl_quote_notes').val();
		var imageCount = $('#quoteNote_imageCount').val();
		var company_id 	  = $('#company_id').val();
		var data = '{"quote_id" : "'+quote_id+'","tbl_quote_notes" : [{"created_at" : 1582195524,"description" : "'+tbl_quote_notes+'"}],"imageCount" : "'+imageCount+'","company_id" : "'+company_id+'"}';
		var formData = new FormData(this);
		formData.append('json', data);

        
        $.ajax({
            type: 'POST',
            url: siteBaseUrl+'/webservice/save-quote-notes',
            data : formData,
            dataType: 'json',
            contentType: false,
            cache: false,
            processData:false,
            success: function(response){ 
                if(response.success == 1){ 
                
	                var str = '';
					$.each(response.data.notes, function(d,i){
						var d = new Date(parseInt((i['created_at']*1000)));
	              		var date = ("0" + d.getDate()).slice(-2) +'/'+ ("0" + (d.getMonth()+1)).slice(-2) +'/'+ d.getFullYear();
	              		var ampm = d.getHours() >= 12 ? 'pm' : 'am';
	              		var hours = d.getHours() % 12;
							hours = hours ? hours : 12;
	              		var time = ("0" + hours).slice(-2) +':'+ ("0" + (d.getMinutes())).slice(-2) +':'+ ampm ;
						str += '<li><div class="row"><div class="col"><span class="note_img"><img width="77" height="66" style="border-radius:7px;" src="'+i['image']+'" alt=""></span></div><div class="col-md-8"><p>'+i['description']+'</p><small>'+date+' '+time+' </small></div><div class="col text-right"><a href="#"  data-noteid="'+i['id']+'" class="note_trash"><img src="https://enacteservices.net/paintpad/image/delete.png" alt=""></a></div></div></li>';
					});
	                $(".site_note_div_main .note_list").append(str);
	                $('#saveQuoteSiteNoteForm').modal('toggle');

                }else{
                    alert(response.message);
                }
                $('#dismiss_modal').trigger('click');
            }
        });
    });

	$(document).on('click',".note_trash", function(e){
		if (confirm("Are you sure to remove?")) {
		  deleteQuoteNotes(this);
		}
		
	});

	function deleteQuoteNotes(qwerty){
		var company_id 	  = $('#company_id').val();
		var quote_id 	  = $('#quote_id').val();
		var note_id 	  = $(qwerty).data('noteid');
		var data = '{"note_id" : "'+note_id+'","company_id" : "'+company_id+'"}';
        
        $.ajax({
            type: 'POST',
            url: siteBaseUrl+'/webservice/delete-quote-notes',
            data : {"json":data},
            success: function(response){ 
        	var response = JSON.parse(response);
                if(response.success == 1){  
                	$(qwerty.closest('li')).hide();
                }else{
                    alert(response.message);
                }
            }
        });
    }

	$(document).on("change","#quote_note_img_input", function(e){
		var input = this;
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();
	        reader.onload = function (e) {
	            $('#quote_note_img').attr('src', e.target.result);
	        }
	        reader.readAsDataURL(input.files[0]);
	    }
	});

	//Invoice send form 
	$(document).on("click",".create_send", function(e){
		console.log($(this));
		var des = $($(this).closest('tr')).find('.descp').text();
		var payment_id = $($(this).closest('tr')).find('.create_send').data('href');
		var qb_invoice_id = $($(this).closest('tr')).find('.create_send').data('qbinvoiceid');
		var qb_invoice_memo = $($(this).closest('tr')).find('.create_send').data('quickbookmemo');

		$('#invoice_model_desc').val(des);
		$('#invoice_model_memo').val(qb_invoice_memo);
		$('#selectedInvoice').attr("data-paymentid", payment_id);
		$('#selectedInvoice').attr("data-descriptoin", des);
		$('#selectedInvoice').attr("data-qbinvoiceid", qb_invoice_id);
		$('#selectedInvoice').attr("data-quickbookmemo", qb_invoice_memo);

		$('#sendInvoiceForm').modal('show');
		e.stopImmediatePropagation();
		return false;
	});

	$(document).on("click","#skipmemoQuickbook", function(e){
		var des = $('#selectedInvoice').data("descriptoin");
		var qb_invoice_memo = $('#selectedInvoice').data("quickbookmemo");
		generateQuickbookInvoice(des,qb_invoice_memo);
	});

	$(document).on("click","#createQuickbook", function(e){
		var des = $('#invoice_model_desc').val();
		var qb_invoice_memo = $('#invoice_model_memo').val();
		generateQuickbookInvoice(des,qb_invoice_memo);
	});

	function generateQuickbookInvoice(descpription,qb_invoice_memo) {
		// $('#loader').show();
		var company_id 	  = $('#company_id').val();
		var quote_id 	  = $('#quote_id').val();
		var subscriber_id = $('#subscriber_id').val();
		var payment_id = $('#selectedInvoice').data("paymentid");
		var qb_invoice_id = $('#selectedInvoice').data("qbinvoiceid");
		qb_invoice_id = ( qb_invoice_id != undefined) ? qb_invoice_id : '';
		qb_invoice_memo = ( qb_invoice_memo != undefined) ? qb_invoice_memo : '';

		var time_zone = (new Date().getTimezoneOffset()) * 60;
	    var data = '{"time_zone":"'+time_zone+'", "payment_id": "'+payment_id+'","company_id":"'+company_id+'", "descp":"'+descpription+'", "subscriber_id" : '+subscriber_id+', "qb_invoice_id":"'+qb_invoice_id+'", "quote_id":"'+quote_id+'", "qb_invoice_memo":"'+qb_invoice_memo+'" }';
		$.ajax({
			url: siteBaseUrl+'/webservice/create-quickbook-invoice',
			type: 'post',
			data: {'json':data},
			success: function (response) {
				var response = JSON.parse(response);
				// console.log(response);exit();
				if(response.success == 1){
					var data = '{"company_id":"'+company_id+'", "quote_id" : "'+quote_id+'", "sub_id" : "'+subscriber_id+'" }';
					getQuantityInvoices(data);
				}else{
					alert(response.message);
					$('#loader').hide();
				}
			},
			error: function () {
				$('#loader').hide();
				alert("Something went wrong");
			}
		});
		$('#selectedInvoice').removeAttr("data-paymentid");
		$('#selectedInvoice').removeAttr("data-descriptoin");
		$('#selectedInvoice').removeAttr("data-qbinvoiceid");
		$('#selectedInvoice').removeAttr("data-quickbookmemo");
	}
	
	function getSubscriberOreders(company_id,subscriber_id,page,filter,event) {
		// $('#loader').show();
	    var data = '{"company_id":"'+company_id+'", "subscriber_id" : '+subscriber_id+', "page": "'+page+'", "filter":'+filter+' }';
		$.ajax({
			url: siteBaseUrl+'/webservice/get-paint-order-subscriber-wise',
			type: 'post',
			data: {'json':data},
			success: function (response) {
				var response = JSON.parse(response);
				if(response.success == 1){
					var str = '';
					$.each(response.data, function(d,i){
						if(i['deliver_detail'] != null){
							var client_name = i['deliver_detail']['name'];
						}else{
							var client_name = 'N/A';
						}
						var type_icon = (i['quote_type'] != 1) ? base_url+'image/quote_exterior.png' : base_url+'image/quote_interior.png'; 
						i['quote_type'] = (i['quote_type'] != 1) ? 'Exterior' : 'Interior'; 
                  		var d = new Date(parseInt((i['created_at']*1000)));
                  		var date = ("0" + d.getDate()).slice(-2) +'/'+ ("0" + (d.getMonth()+1)).slice(-2) +'/'+ d.getFullYear();
                  		var ampm = d.getHours() >= 12 ? 'pm' : 'am';
                  		var hours = d.getHours() % 12;
  						hours = hours ? hours : 12;
                  		var time = ("0" + hours).slice(-2) +':'+ ("0" + (d.getMinutes())).slice(-2) +':'+ ampm ;
						str += "<tr><td><span>"+i['quote_id']+"</span></td><td><span>"+client_name+"</span></td><td style='width: 35%;'><span>"+i['notes']+"</span></td><td><span><img style='width: 25%;' src='"+type_icon+"'></span><br><span>"+i['quote_type']+"</span></td><td><span>"+date+"<br><span class='time'>"+time+"</span></span></td></tr>";
					});
					var nextpage = $('#pagevalPaintoreder').data('value')+1;
					
					$('#pagevalPaintoreder').attr("data-value",nextpage);
					$('#pagevalPaintoreder').data("value",nextpage);

					if(event == "endScroll"){
	                  $("#mCSB_1_container").append(str);
	                }else{
	                  $("#mCSB_1_container").html(str);
	                }
				}else{
					alert(response.message);
					$('#pagevalPaintoreder').removeAttr("data-value");
					$('#pagevalPaintoreder').removeClass();
				}
				$('#loader').hide();
			},
			error: function () {
				$('#loader').hide();
				alert("Something went wrong");
			}
		});
	}


	function getProductColors(company_id,subscriber_id,page,filter,event) {
		$('.app-ovelray').show();
		var tag_id = $('.searchTag-dropdown').val();
		if(tag_id == ''){
			tag_id = '';
		}
		$.ajax({
			url: siteBaseUrl+'/webservice/product-colors?company_id='+company_id+'&page='+page+'&search='+filter+'&tag_id='+tag_id+'',
			type: 'get',
			// data: {'json':data},
			success: function (response) {
				var response = JSON.parse(response);
				if(response.success == 1){
					var str = '';
					$.each(response.data.colors, function(d,i){
						str += '<li class="col"><div  data-dismiss="modal" class="colorPick" style="height:175px;background-color:'+i['hex']+'" data-color_id="'+i['id']+'" data-hex="'+i['hex']+'" data-name="' + i['name'] + '" data-tagid="' + i['tag_id'] + '"></div><p class="colorName">' + i['name'] + '</p></li>';
					});
					var nextpage = $('#colorsMainDiv').data("page")+1;
					
					$('#colorsMainDiv').attr("data-page",nextpage);
					$('#colorsMainDiv').data("page",nextpage);

					if(event == "endScroll"){
	                  $("#colorsMainDiv .container").append(str);
	                }else{
	                  $("#colorsMainDiv .container").html(str);
	                  $('#colorsMainDiv').attr("data-page",'0');
					  $('#colorsMainDiv').data("page",'0');
	                }
				}else{
					var str = '';
					if(event == "endScroll"){
	                  $("#colorsMainDiv .container").append(str);
	                }else{
	                  $("#colorsMainDiv .container").html(str);
	                  $('#colorsMainDiv').attr("data-page",'0');
					  $('#colorsMainDiv').data("page",'0');
	                }
					$('#colorsMainDiv').removeAttr("data-value");
					// $('#colorsMainDiv').removeClass();
				}
				$('.app-ovelray').hide();
			},
			error: function () {
				$('.app-ovelray').hide();
				alert("Something went wrong");
			}
		});
	}

	$(document).on('click','.colorPick',function(){
		var com_id 	  = $('#colorCeleings').attr('data-comp_id');
		var colorCode = $(this).data('hex');
		var colorName = $(this).data('name');
		var color_id = $(this).data('color_id');
		
		$('#colorSpan_'+com_id+'').attr("style","background-color:"+colorCode);
		$('[name="component['+com_id+'][color]"]').val(colorName);
		$('[name="component['+com_id+'][color]"]').attr('data-color_id',color_id);
		$('#colorSpan_'+com_id+' img').css('visibility','hidden');
	});

	$(document).on('keyup','#customColor',function(){
		var com_id = $('#colorCeleings').attr('data-comp_id');
		if($(this).val() != ''){
			$('[name="component['+com_id+'][color]"]').val($(this).val());
			$('#colorSpan_'+com_id+' img').css('visibility','visible');
			$('[name="component['+com_id+'][color]"]').attr('data-color_id','null');
			$('#colorSpan_'+com_id+'').attr("style","background-color:");
		}
		
	});

	$(document).on('click','.default_heading span',function(){
		$('#customColor').val('');
	});
	
	// Comment by Ankit
	// function getContactAllContacts(page) {
	// 	// $('#loader').show();
	//     var data = '{"page": "'+page+'"}';
	// 	$.ajax({
	// 		url: siteBaseUrl+'/contact/all-contacts',
	// 		type: 'post',
	// 		data: {'json':data},
	// 		success: function (responsedata) {

	// 			var response = JSON.parse(responsedata);
				
	// 			if(response.success == 1){


	// 				var str = '';
	// 				$.each(response.data, function(d,i){
	// 					img = (i.image != '') ? base_url+"uploads/"+i.image : base_url+"uploads/avtar.png"
	// 					str +="<li class='tosearch' data-id='"+i.contact_id+"'><span class='cnt_img'><img src='"+img+"' height='58px'  width='58px' style='border-radius: 50%;''></span><div class='contact_person_detail'><p class='contact_nameSearch'>"+i.name+"</p><small>"+i.phone+"</small></div></li>";
	// 				});
	// 				var nextpage = $('#contactListMain').data('page')+1;
					
	// 				$('#contactListMain').attr("data-page",nextpage);
	// 				$('#contactListMain').data("page",nextpage);
	//                 $("#contact_list").append(str);
	// 			}else{
	// 				// console.log(response.message);
	// 				$('#contactListMain').removeAttr("data-value");
	// 				$('#contactListMain').removeClass();
	// 			}
	// 		},
	// 		error: function () {
	// 			console.log("Something went wrong");
	// 		}
	// 	});
	// }

	function currencyFormat(amount) {
	  return '$' + amount.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
	}

	$(document).on('click','#filerApply',function(){
	  page = 0;
	  subscriber_id = $("#subscriber_id").val();
	  contact_id 	= $("#search_existing_client_quote").attr('clientid');
	  console.log('contact_id : ',contact_id);

	  var if_interior_quote = $("#if_interior_quote").val();
	  var if_exterior_quote = $("#if_exterior_quote").val();
	  var typeArr = [];
	  
	  if(if_interior_quote == 1){typeArr.push(1)}
	  if(if_exterior_quote == 1){typeArr.push(2)}
	  


	  	quoteTypes = typeArr.join(',');

			var selectedStatus = [];

	    $.each($('.QuotePageStatus'), function(){

	        if (!$(this).hasClass('exclude-filter')) {
	        	selectedStatus.push( $(this).data('value'))
	        }
	    });

	    var ss = selectedStatus.join(',');

		$("#quote_status").val( ss );



	  status = $("#quote_status").val();
	  from = $("#txtFromDate").val();
	  to = $("#txtToDate").val();
	  quote_id = $("#quote_id").val();

	  if(from != ""){
	    from = from.split("/").reverse().join("-");
	    from = new Date(from).getTime() / 1000;
	  }
	  if(to != ""){
	    to = to.split("/").reverse().join("-");
	    to = new Date(to).getTime() / 1000;
	  }
	  if_filter_applied = 1
	  getQuotes('filterapply',page);
	});

	$(document).on('click','#filterReset',function(){

		$('.QuotePageStatus').removeClass('exclude-filter');
		if_filter_applied = 0
		page = 0;
		getQuotes('filterReset',page);
	});

	$(document).on('click','.quoteTypeSel',function(){

		var curtochange = $(this).data('tochange');
			curVal 		= $("#"+curtochange).val();
			ifSet 		= (curVal == 1)?0:1
		
		if(ifSet == 0){

			var checkAttr = $(this).data('check');
			check = $("#"+checkAttr).val();

			if(check == 1){
				
				$("#"+curtochange).val(ifSet);
				
				if($(this).hasClass('quotetypeactive')){

					$(this).removeClass('quotetypeactive')
				}
				else{
					$(this).addClass('quotetypeactive')
				}
			}

		}else{

			$("#"+curtochange).val(ifSet);

			if($(this).hasClass('quotetypeactive')){
				
				$(this).removeClass('quotetypeactive')

			}
			else{
				
				$(this).addClass('quotetypeactive')

			}
		}
	});



	$(".fa-bars").click(function() {
		$(".menu").removeClass('menuClose');
		$(".menu").addClass('menuOpen');

		$(".mainClose").addClass('mainOpen');
		$(".mainOpen").removeClass('mainClose');

		$(".fa-bars").hide(500);
		$(".fa-times").show(500);
	});



	$(".fa-times").click(function() {

		$(".menu").addClass('menuClose');
		$(".menu").removeClass('menuOpen');

		$(".mainOpen").addClass('mainClose');
		$(".mainClose").removeClass('mainOpen');

		$(".fa-times").hide(500);
		$(".fa-bars").show(500);

	});

	if($("#txtFromDate").length > 0){

		$("#txtFromDate").datepicker({
			dateFormat: 'dd/mm/yy',
			onSelect: function(selected) {
				$("#txtToDate").datepicker("option","minDate", selected)
			}
		});

		$("#txtToDate").datepicker({
			dateFormat: 'dd/mm/yy',
			onSelect: function(selected) {
				$("#txtFromDate").datepicker("option","maxDate", selected)
			}
		});

	}


	$(function() {  

		var ifSelectedFromACList = 0;

		if($("#contact_users").length>0){

			var contact_users = $("#contact_users").val();
				contact_users = JSON.parse(contact_users);

			$("#search_existing_client_quote").autocomplete({
				source: contact_users,
				minLength: 1,
				select: function(event, ui) {
					ifSelectedFromACList = 1;
					$("#quote_contact_id").val(ui.item.id)
				},
				html: true,
				focus: function(event, ui) {
					// ifSelectedFromACList = 0;
					// console.log(ifSelectedFromACList)
				},
				open: function(event, ui) {
					$(".ui-autocomplete").css("z-index", 1000);
				},
				change: function() {
					if(ifSelectedFromACList == 0){
					$("#search_existing_client_quote").val('');
					$("#quote_contact_id").val('');
					}
					//var curAcUser = $("#search_existing_client_quote").val();
				}
			}).autocomplete( "instance" )._renderItem = function( ul, item ) {
				return $( "<li class='contact_li'><div class='contact_li_img'><img src='"+item.img+"'></div><div class='existing_client_detail'><h3 class='client_name_list'>"+item.value+"</h3><small class='client_phone_list'>"+item.phone+"</small></div></li>" ).appendTo( ul );
			};

			$(document).on('focus','#search_existing_client_quote',function(){
				ifSelectedFromACList = 0;
				console.log(ifSelectedFromACList)
			})
		}
	});

	/*
		var date = $("#calenderEventsId").val();
		date = JSON.parse(date);
		//var fieldhtml= "<span>.</span>";
		$.each(date, function(key, value){
			console.log('calendar-day-'+value);
			$('.calendar-day-'+value).addClass('isEvent');
			$('.calendar-day-'+value).addClass('event');
		});
	*/
  
	if($('#defaultBrand').length > 0){
	  	/*setTimeout(function(){
			defaultBrand = $('#defaultBrand').val();
			$('.quote_tiers').each(function(){
				var c = $(this).attr('class');
				var mc = c.split(' ');
				if(mc[0]=='tb_'+defaultBrand){
					$(this).parent().show()
				}
				else{
					$(this).parent().hide()
				}
			});
		}, 2000);
		*/

		$(document).on('change','#selbrand',function(){
			defaultBrand = $(this).val();
			/*var found = 0;
			$('.quote_tiers').each(function(){
				var c = $(this).attr('class');
				var mc = c.split(' ');
				if(mc[0]=='tb_'+defaultBrand){
					found = 1
					$("#seltiers").val($(this).val());
					$("#seltiers").selectpicker('refresh');
					return;
				}
			});*/
			
			$('.quote_tiers').each(function(){
				var c = $(this).attr('class');
				var mc = c.split(' ');
				if(mc[0]=='tb_'+defaultBrand){
					$(this).show()
				}
				else{
					$(this).hide()
				}
			});

			$('#seltiers>option:eq(0)').attr('selected', true);
			
			printBrandPrefDefaults();
		});

		$(document).on('change','#seltiers, #defaultCoat, #defaultPrepLevel',function(){
			printBrandPrefDefaults();
		});
	}

	$(document).on('change','.sheendd',function(){
		var curSheen = $(this).val();
		var curGroup = $(this).data('group');

		$("#product_"+curGroup).find('option').hide();
		//$("#product_"+curGroup).find('option').hide();

		$("#product_"+curGroup+" option:selected").removeAttr('selected');

		var sheen_products = $("#product_"+curGroup).find('.sheen_'+curSheen);

		sheen_products.each(function(){
			
			var isdefault = $(this).data('isdefault');
			if(isdefault == 1){
				console.log($(this).val())
				$(this).attr("selected",true);
			}
		});
		$("#product_"+curGroup).find('.sheen_'+curSheen).show();

	});


	$(document).on('click','.underChecks',function(){

		var groupId =  $(this).data('group');
		
		if($('#undercheck_'+groupId+':checked').length > 0 ){
			
			$('#undercoatdd_'+groupId).parent().show()

		}
		else{

			$('#undercoatdd_'+groupId).parent().hide()

		}
	})

	$(document).ready(function(){
		if($('.underChecks:checked').length > 0 ){
			$('#underCoatMain').prop('checked',true)
		}
	});
	$(document).on('click','.underChecks',function(){
		if($('.underChecks:checked').length == 0 ){
			$('#underCoatMain').prop('checked',false)
		}else{
			$('#underCoatMain').prop('checked',true)
		}
	});


	$(document).on('click','#underCoatMain',function(){

		if($('#underCoatMain:checked').length > 0 && $('.underChecks').length  == $('.underChecks:checked').length ){

			$('.underChecks').prop('checked',false)
			$('.undercoat').parent().hide()
		}
		else if($('#underCoatMain:checked').length > 0 && ($('.underChecks').length  != $('.underChecks:checked').length &&  $('.underChecks:checked').length > 0)){

			// $('.underChecks').prop('checked',true)
			// $('.undercoat').parent().show()
		}else if($('#underCoatMain:checked').length > 0 && $('.underChecks:checked').length == 0){

			$('.underChecks').prop('checked',true)
			$('.undercoat').parent().show()
		}
		else{

			$('.underChecks').prop('checked',false)
			$('.undercoat').parent().hide()

		}

		printBrandPrefDefaults();

	})

	$(document).on('click','.checkChange',function(){

		if($("#ifPaintDefaultChanged").val() == 1){

			
			// console.log($("#paintDefaultForm").serialize());
			//console.log('change detected');
			//var data = $("#paintDefaultForm").serialize();

			var type_id 	  		  = $("#quote_type").val();
			var quote_id 	  		  = $("#quote_id").val();
			var subscriber_id 		  = $("#subscriber_id").val();
			var brand_pref_id 		  = $("input[name='brandPrefId']").val();
			var company_id 	  		  = $("#company_id").val();
			var tier_id 	  		  = $("#seltiers").val();
			var coats 	  	  		  = $("#defaultCoat").val();
			var brand_id 	  		  = $("#selbrand").val();
			var prep_level 	  		  = $("#defaultPrepLevel").val();
			var apply_undercoat 	  = $("#defaultPrepLevel").val();
			var color_consultant 	  = $("#colorConsultanMain").val();
			
			/*
			var paint_default = [];

			$(".job_default_details").each(function(){

				var curCompId 			= $(this).data('id');
				var curCompPaintDefault = {};
				
				// console.log($("#undercheck_"+curCompId+":checked").length)

				curCompPaintDefault.topcoat = $("select[name='component["+curCompId+"][product]']:selected").val();

				// console.log(curCompPaintDefault.topcoat)

				curCompPaintDefault.undercoat = null;

				
					if(){
						curCompPaintDefault.undercoat = 
					} 
				
				
				paint_default.push(curCompPaintDefault)

			});
			*/
			 	
			
			
		}
	})

	$(document).on('change','.changeType',function(){

		// console.log('change event firing');

		var changetype = $(this).data('changetype');

		if(changetype == 2){

			// console.log('making changes');
			$("#ifPaintDefaultChanged").val(1)

		}
	});

	$(document).on('change','.getPaintDetails',function(){
		var selbrand = $('#selbrand').val();
		var selTier = $('#seltiers').val();
		var quoteType = $('#quote_type').val();
		var quote_id = $("#quote_id").val();
		var defaultCoat = $('#defaultCoat').val();
		var defaultPrepLevel = $('#defaultPrepLevel').val();

		if(selbrand != "" && selTier!=""){
			
			url = siteBaseUrl+'/webservice/tiers-component';
			//url = siteBaseUrl+'/quote/get-paint-components';
			$.ajax({
				url: url,
				type: 'get',
				data: {'tier_id':selTier,"type_id":quoteType,"quote_id":quote_id},
				dataType:'JSON',
				success: function (response) {
					
					if(response.success==1){
						var groupStr='';
						groups = response.data;

						groups_length = Object.keys(groups).length;
						if(groups_length > 0){
							components = groups.tier_types;
							//var selectedSheen = response.selectedSheen;
							//var selectedSheen = response.selectedSheen;
							
							/* undercoat = response.undercoat;
							topcoat = response.topcoat;
							sheens = response.sheens;
							sheenProducts = response.sheenProducts; */
							components_length = Object.keys(components).length;
							if(components_length > 0){

								for (var ci=0; ci<components_length; ci++) {

									var curComp = components[ci];
									var gk = curComp.id;
									var sheens = curComp.sheens;
									var undercoat = curComp.under_coat;

									undercoatOptions ='';
									undercoat_length = Object.keys(undercoat).length;
									if(undercoat_length > 0){
										for (var uck in undercoat) {
											if(undercoat[uck]['default'] == 1){
												undercoatOptions+='<option selected value="'+undercoat[uck]['product_id']+'" >'+undercoat[uck]['name']+'</option>';
											}
											else{
												undercoatOptions+='<option value="'+undercoat[uck]['product_id']+'" >'+undercoat[uck]['name']+'</option>';
											}
										}
									}

									//console.log(undercoatOptions);

									var sheenOptions ='';
									var productOptions ='';
									sheens_length = Object.keys(sheens).length;
									if(sheens_length>0){
										for(var cs=0; cs<sheens.length; cs++) {
											var curSheen = sheens[cs];
											if(curSheen.default == 1){
												sheenOptions+='<option selected value="'+curSheen.sheen_id+'" >'+curSheen.name+'</option>';
											}
											else{
												sheenOptions+='<option value="'+curSheen.sheen_id+'" >'+curSheen.name+'</option>';
											}												

											var topcoat = curSheen.top_coats;
											if(topcoat.length>0){
												for(var ctc=0; ctc<topcoat.length; ctc++) {
													var curTopCoat = topcoat[ctc];
													if(curSheen.default == 1){
														if(curTopCoat.default == 1){
															productOptions+='<option style="display:block;" selected data-isdefault="'+curTopCoat.default+'" class="sheen_'+curSheen.sheen_id+'" value="'+curTopCoat.product_id+'" >'+curTopCoat.name+'</option>';
														}
														else{
															productOptions+='<option style="display:block;" data-isdefault="'+curTopCoat.default+'" class="sheen_'+curSheen.sheen_id+'" value="'+curTopCoat.product_id+'" >'+curTopCoat.name+'</option>';
														}
													}
													else{
														productOptions+='<option style="display:none;" data-isdefault="'+curTopCoat.default+'" class="sheen_'+curSheen.sheen_id+'" value="'+curTopCoat.product_id+'" >'+curTopCoat.name+'</option>';
													}
												}
											}
										}
									}

									groupStr+=
									'<div class="job_default_details" data-id="'+gk+'">'+
										'<h3 class="default_heading"><span id="colorSpan_'+gk+'"><a href="javascript:void(0)" data-toggle="modal" data-target="#colorCeleings" onclick="$(\'#colorCeleings\').attr(\'data-comp_id\','+gk+')" data-dismiss="modal"><img src="'+siteBaseUrl+'/image/coloricon.png" alt=""></a></span>'+curComp.name+'</h3>'+
										'<div class="fill_default_form">'+
											'<div class="form-group small">'+
												'<input name="component['+gk+'][compId]" type="hidden" value="'+gk+'">'+
												'<input name="component['+gk+'][color]" type="text" value="" data-color_id="null" placeholder="Colour Name" class="form-control colour-name">'+
											'</div>'+
											'<div class="form-group small">'+
												'<select name="component['+gk+'][sheen]" id="sheen_'+gk+'" data-changetype="2" data-group="'+gk+'" class="sheen sheendd changetype" data-style="btn-new" tabindex="-98">'+
												sheenOptions+
												'</select>'+
											'</div>'+
											'<div class="form-group long">'+
												'<select name="component['+gk+'][product]" id="product_'+gk+'" data-changetype="2" data-group="'+gk+'" class="Product changetype" data-style="btn-new" tabindex="-98">'+
												productOptions+
												'</select>'+
											'</div>'+
										'</div>'+
										'<div class="fill_default_form">'+
											'<div class="form-group small">'+
												'<select name="component['+gk+'][strength]" class="strength changetype" data-changetype="2" data-style="btn-new" tabindex="-98">'+
													'<option value="1">One</option>'+
													'<option value="2">Two</option>'+
													'<option value="3">Three</option>'+
													'<option value="4">Four</option>'+
												'</select>'+
											'</div>'+
											'<div class="form-group small">'+
												'<div class="form-group checkbox_div">'+
													'<label class="checkbox_label">Undercoat'+
														'<input name="component['+gk+'][underChecks]" data-group="'+gk+'" data-changetype="2" class="underChecks changetype" id="undercheck_'+gk+'" type="checkbox">'+
														'<span class="checkmark"></span>'+
													'</label>'+
												'</div>'+
											'</div>'+
											'<div class="form-group long" style="display:none;">'+
												'<select name="component['+gk+'][undercoat]" id="undercoatdd_'+gk+'" data-changetype="2" class="undercoat changetype" data-style="btn-new" tabindex="-98">'+
													'<option>Select Undercoat</option>'+
													undercoatOptions+
												'</select>'+
											'</div>'+
										'</div>'+
									'</div>';
								}
							}
							$("#blankDefaultPane").hide();
							//console.log(groupStr);
							$("#mCSB_2_container").html(groupStr);
							$("#paintDefaultMain").show();
						}
					}
					
				},
					error: function () {
					alert("Something went wrong");
				}
			});


		}
	});

	$(document).on('change','#roomType',function(){
		var roomType = $('#roomType').val();
		var quote_type = $('#quote_type').val();
		if(roomType!=""){
			
			url = siteBaseUrl+'/quote/get-room-components';
			$.ajax({
				url: url,
				type: 'get',
				data: {'roomType':roomType,'quote_type':quote_type},
				dataType:'JSON',
				success: function (response) {
					$('#custom_room_name').val("");
					roomCompList = response.roomCompList;
					roomComp = response.roomComp;
					roomCompDetail = response.roomCompDetail;
					var str = "";
					var count = 0;
					if(roomCompList.length > 0){
						
							for (var ck in roomComp) {
								if(count%2 == 0){
									str+='<div class="row addroom_row">';
								}
									str+='<div class="col-6">'+
										'<div class="form-group">'+
											'<h3 class="default_heading"><span><img src="'+siteBaseUrl+'/image/coloricon.png" alt=""></span> '+roomComp[ck]['name']+' <small>(Area: 0 m <sup>2</sup> )</small></h3>'+
											'<div class="color-checkbox" style="display:none" id="ucCheck_'+ck+'" data-compid="'+ck+'"><input type="checkbox" class="ucIsSelected" name="uc_'+ck+'">U/C</div>'+
											'<div class="select_ceilings staircase">';
												for (var rcdk in roomCompDetail[ck]) {
													if(roomCompDetail[ck][rcdk]['price_method_id'] == 1 || roomCompDetail[ck][rcdk]['price_method_id'] == 3){
														str+='<label class="ceilint_container">'+
															'<input type="checkbox" class="selectiveCompOption" data-compid="'+ck+'" name="compDetail['+ck+'][selOption][]" value="'+roomCompDetail[ck][rcdk]['comp_type_id']+'">'+
															'<span class="checkbox_text">'+roomCompDetail[ck][rcdk]['component_type_name']+'</span>'+
														'</label>';
													}
													else{
														str+='<input data-compid="'+ck+'" name="compDetail['+ck+'][inputOption][]" type="text" value="" placeholder="'+roomCompDetail[ck][rcdk]['component_type_name']+'" class="form-control number_small  class="inputCompOption"">';
													}
												}
											str+='</div>'+
										'</div>'+
									'</div>';

								if(count%2 != 0){
									str+='</div>';
								}

								count++;
							}
						$("#room_comp_list").html(str);
					}
				},
				error: function (error) {

				}
			});

		}
	});


	$(document).on('click','.selectiveCompOption', function() {
		var $box = $(this);
		if ($box.is(":checked")) {
			var group = "input:checkbox[name='" + $box.attr("name") + "']";
			$(group).prop("checked", false);
			$box.prop("checked", true);
			$("#ucCheck_"+$box.data('compid')).show();
		}else {
			$box.prop("checked", false);
			$("#ucCheck_"+$box.data('compid')).hide();
		}
	});	

	$(document).on('click','.roomTabChange',function(e){
		var href = $(this).data('href');
		if($('.active.roomTabChange').data('href') == "detail" && href != "detail"){

			rl = $("#r_l").val();
			rw = $("#r_w").val();
			rh = $("#r_h").val();

			var rname = $("#room_name").val();
			if(rname == ''){
				alert('Please fill the Room Name!');
			}else if(rl == '' || rw == '' || rh == ''){
				alert('Please fill in the Room Dimesions');
			}
			else{
				$('.roomTabChange').removeClass('active');
				$(this).addClass('active');
				$('.roomTabChangePane').hide();
				$('#'+href).show();	
			}
		}
		else{
			$('.roomTabChange').removeClass('active');
			$(this).addClass('active');
			$('.roomTabChangePane').hide();
			$('#'+href).show();
		}
	});
	
	$(document).on('click','.add_dim_btn',function(){

		var ts = Math.round((new Date()).getTime() / 1000);
		var str = "";
		str+='<div class="form-group dimension_row">'+
			'<input type="text" min="0" value="" placeholder="0" class="form-control number_input" name="l[]">'+
			'<input type="text" min="0" value="" placeholder="0" class="form-control number_input" name="w[]">'+
			'<span class="checkboxmainSpan"><input type="checkbox" class="l_w_check" name="check_'+ts+'" value="1">L</span><span class="checkboxmainSpan"><input type="checkbox" class="l_w_check" name="check_'+ts+'" value="2">W</span>'+
			'<span class="btn btn-transparent pull-right remove_dim_btn" style=""><i class="material-icons">-</i></span>'+
		'</div>';

		$('.room_dimesions').append(str);

		
	});

	$(document).on('click','.remove_dim_btn',function(){
		$(this).parent().remove();
	});

	$(document).on('click','.quoteDetailMainTabs',function(){
		var href = $(this).data('href');

		var panelNoArr = href.split('panel');
		var panelNo = panelNoArr[1];

		if(panelNo < 3){
			$('.quoteDetailMainTabs').removeClass('active');
			$(this).addClass('active');
			$('.quoteDetailMainPane').hide();
			$(href).show();
		}
		else{
			sb = $("#selbrand").val();
			st = $("#seltiers").val();
			dc = $("#defaultCoat").val();
			dpl = $("#defaultPrepLevel").val();
			error = 0;
			if(sb==""){
				msg = "Please choose Brand in the Paint Defaults";
				error = 1;
			}
			else if(st==""){
				msg = "Please choose Quality/Tier in the Paint Defaults";
				error = 1;
			}
			else if(dc==""){
				msg = "Please choose Caots in the Paint Defaults";
				error = 1;
			}
			else if(dpl==""){
				msg = "Please choose Prep. Level in the Paint Defaults";
				error = 1;
			}

			if (error == 1) {
				alert(msg);
				return false;
			}
			else{
				var ids = $('.job_default_details').map(function() {
					// alert($(this).attr('data-comp_id'));
				  // return $(this).attr('data-id');
				  return $(this).data('comp_id');
				});
				ids = ids.toArray();
// console.log(ids);

				var painrDefaultVal = [];
				$.each( ids,function(i,v){

					var compPaintVals = {};

					// alert($('input[name="component['+v+'][color]"]').attr('data-color_id'));
					if($('input[name="component['+v+'][color]"]').attr('data-color_id') == 'null'){
						var color_id = null;
						var custom_color = $('input[name="component['+v+'][color]"]').val();
					}else{
						var color_id = $('input[name="component['+v+'][color]"]').attr('data-color_id');
						var custom_color = '';
					}
					// $( "#myselect option:selected" ).text();
					compPaintVals.color_id 		= color_id;
					compPaintVals.topcoat  		= $('[name="component['+v+'][product]"] option:selected').val();
					compPaintVals.id 			= 0;
					// compPaintVals.id 			= $('input[name="component['+v+'][compId]"]').val();
					compPaintVals.comp_id 		= $('input[name="component['+v+'][compId]"]').val();
					compPaintVals.custom_name 	= custom_color;
					compPaintVals.strength 		= $('[name="component['+v+'][strength]"] option:selected').val();
					if($('input[name="component['+v+'][underChecks]"]').val() == 'on'){
						var hasUndercoatVal = 1;
					}else{
						var hasUndercoatVal = 0;
					}
					compPaintVals.hasUnderCoat  = hasUndercoatVal;
					compPaintVals.undercoat 	= $('[name="component['+v+'][undercoat]"] option:selected').val();
					compPaintVals.sheen_id 		= $('[name="component['+v+'][sheen]"] option:selected').val();

					painrDefaultVal.push(compPaintVals);
				});
				painrDefaultVal = JSON.stringify(painrDefaultVal);

				var type_id 	  		  = $("#quote_type").val();
				var quote_id 	  		  = $("#quote_id").val();
				var subscriber_id 		  = $("#subscriber_id").val();
				var brand_pref_id 		  = $("input[name='brandPrefId']").val();
				var company_id 	  		  = $("#company_id").val();
				var tier_id 	  		  = $("#seltiers").val();
				var coats 	  	  		  = $("#defaultCoat").val();
				var brand_id 	  		  = $("#selbrand").val();
				var prep_level 	  		  = $("#defaultPrepLevel").val();
				var apply_undercoat 	  = $("#defaultPrepLevel").val();
				var color_consultant 	  = $("#colorConsultanMain").val();

				if(painrDefaultVal == ''){
					alert('values are empty');exit();
					return false();
				}


				data='{"brand_pref_id":'+brand_pref_id+',"company_id":'+company_id+',"quote_id":'+quote_id+',"subscriber_id":'+subscriber_id+',"tier_id":'+tier_id+',"paint_default":'+painrDefaultVal+',"tbl_brand_pref":{"coats" : '+coats+',"tier_id" : '+tier_id+',"brand_id" : '+brand_id+',"prep_level" : '+prep_level+',"apply_undercoat" : '+apply_undercoat+',"color_consultant" : '+color_consultant+'}}';
				url = siteBaseUrl+'/webservice/save-paint-defaults';
				$('.app-ovelray').show();
				$.ajax({
					url: url,
					type: 'post',
					data: {'json':data},
					success: function (response) {
						var response = JSON.parse(response);
						$("#ifPaintDefaultChanged").val(0);
						$("input[name='brandPrefId']").val(response.data.brand_pref_id);
						$('.app-ovelray').hide();
					},
					error: function () {
						$('.app-ovelray').hide();
						alert("Something went wrong");
					}
				}); 



				$('.quoteDetailMainTabs').removeClass('active');
				$(this).addClass('active');
				$('.quoteDetailMainPane').hide();
				
				if(href == "#panel4"){ // Rooms/Areas Section 
					var cqid = $("#quote_id").val();
					var ctid = $("#quote_type").val();
					var data = '{"quote_id" : '+cqid+',"type_id" : '+ctid+'}';
					getQUoteAllRooms(data);
					
				}
				$(href).show();
			}
		}
	});


	$('#roomTypeApplyBtn').click(function(){
		if($('#rooms_type_left .active').length > 0){
			$(".rooms_tab ").hide();
		    $.each($('#rooms_type_left .active'), function(){
		        var id = $(this).data('type');	  
		        $(".room_type_"+id).show();         
		    });
		}else{
			$(".rooms_tab").show();
		}	
	});

	$('#roomTypeResetBtn').click(function(){
	    $.each($('#rooms_type_left .active'), function(){
	        $(this).removeClass('active'); 
	    });
	   $(".rooms_tab").show();
	});

	function strDes(a, b) {
	   if (a.comp_name>b.comp_name) return 1;
	   else if (a.comp_name<b.comp_name) return -1;
	   else return 0;
	 }

	$(document).on("change","#room_name",function(){
		$('#custom_room_name').val("");
	});

	$(document).on("change","#custom_room_name",function(){
		$('#room_name').val("");
		$('#roomType>option:eq(0)').attr('selected', false);
		$('#roomType>option:eq(0)').attr('selected', true);
	});

	$("#saveRoom").click(function(){
		rl = $("#r_l").val();
		rw = $("#r_w").val();
		rh = $("#r_h").val();
		//console.log(rl,rw,rh);
		if(rl == '' || rw == '' || rh == ''){
			alert('Please fill in the Room Dimesions');
			return false;
		}

		room_id = 0;
		room_type_id = $("#roomType").val();
		room_name = $("#room_name").val();
		/*is_custom

		if(){

		}*/
		$(".selectiveCompOption").each(function(){

		});
	})

	$(document).on("click","#quote_status_button",function(){
		var quote_status = $("#quote_status").val();
		var quote_id = $("#quote_id").val();
		var url = siteBaseUrl+'/webservice/update-status';

		$.ajax({
			url: url,
			method:'POST',
			dataType:'JSON',
			data: {'quote_id':quote_id,'status':quote_status},
			success: function (response) {
				alert(response.message)
			},
			error: function () {
			  alert("Something went wrong");
		  }
	   });
	   return false;
	});

});



$('body').on('beforeSubmit', '#myid', function () {
     var form = $(this);
     //console.log("sdadas");
     // submit form
     $.ajax({
          url: form.attr('action'),
          type: 'post',
          data: form.serialize(),
          success: function (response) {
               // do something with response
               console.log(response);

               if(response == 1){
                 console.log('here11');

               }
          },
          error: function () {
            alert("Something went wrong");
        }
     });
     return false;
});

/************************ [for quote address] ****************************************/

  $(function() {
      /*
            var addresspickerMap = $( "#tbladdress-street1" ).addresspicker({
              updateCallback: showCallback,
              mapOptions: {
                zoom: 4,
                center: new google.maps.LatLng(-34, 138),
                scrollwheel: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP
              },
              elements: {
                map:      "#map",
                lat:      "#lat",
                lng:      "#lng",
                street_number: '#street_number',
                route: '#route',
                locality: '#tbladdress-suburb',
                sublocality: '#sublocality',
                administrative_area_level_3: '#administrative_area_level_3',
                administrative_area_level_2: '#administrative_area_level_2',
                administrative_area_level_1: '#tbladdress-state_id',
                country:  '#country',
                postal_code: '#tbladdress-postal_code',
                type:    '#type'
              }
            });

            var gmarker = addresspickerMap.addresspicker( "marker");
            gmarker.setVisible(true);
            addresspickerMap.addresspicker( "updatePosition");


            $("#tbladdress-street1").addresspicker("option", "reverseGeocode", true);

            function showCallback(geocodeResult, parsedGeocodeResult){
              $('#callback_result').text(JSON.stringify(parsedGeocodeResult, null, 4));
            }
            // Update zoom field
            var map = $("#tbladdress-street1").addresspicker("map");
            google.maps.event.addListener(map, 'idle', function(){
              $('#zoom').val(map.getZoom());
            });
      */
  });


/************************ [for site address address] ****************************************/

	$(function() {
		/*
		    var addresspickerMap2 = $( "#site-address" ).addresspicker({
		      updateCallback: showCallback2,
		      mapOptions: {
		        zoom: 4,
		        center: new google.maps.LatLng(-34, 138),
		        scrollwheel: false,
		        mapTypeId: google.maps.MapTypeId.ROADMAP
		      },
		      elements: {
		        map:      "#map2",
		        locality: '#site-suburb',
		        administrative_area_level_1: '#site-state',
		        postal_code: '#site-postcode',
		      }
		    });

		    var gmarker2 = addresspickerMap2.addresspicker( "marker");
		    gmarker2.setVisible(true);
		    addresspickerMap2.addresspicker( "updatePosition");

		    $("#tbladdress-street1").addresspicker("option", "reverseGeocode", true);

		    function showCallback2(geocodeResult, parsedGeocodeResult){
		      $('#callback_result').text(JSON.stringify(parsedGeocodeResult, null, 4));
		    }
		    // Update zoom field
		    var map2 = $("#site-address").addresspicker("map");
		    google.maps.event.addListener(map2, 'idle', function(){
		      $('#zoom').val(map2.getZoom());
		    });
		*/
	});


$('body').on('beforeSubmit', '#myidcontact', function (event) {

    event.preventDefault();
    //do something
    $('.subm').prop('disabled', true);


    var formData = new FormData($('#myidcontact')[0]);
    var baseUrl = base_url + 'contact/';
    $.ajax({
        url: baseUrl,  //Server script to process data
        type: 'POST',

        // Form data
        data: formData,

        success: function(response) {
            console.log(response);

            if(response == 1){

                $('#filee').html('');
                $('#file').show();
                $('#file').parent().show();
                $('.subm').prop('disabled', false);

                $('#addContact').modal('hide');
                $('#addContact').find('form').trigger('reset');



                var url = base_url + 'contact/search/';
                $.ajax({
                  type: "POST",
                  url: url,
                  data: {
                      'search' : ''
                  },
                  success: function(resp){
                     //console.log(response);                   
                     var datta = $.parseJSON(resp);

                     if(datta.success == 1){
                        console.log('here12');
                        $('.contact_list').html('');           
                        $('.contact_list').html(datta.searchdata);

                     }
                  }
                });


            }

        },
        error: function(){
            alert('ERROR at PHP side!!');
        },

        //Options to tell jQuery not to process data or worry about content-type.
        cache: false,
        contentType: false,
        processData: false
    });

    return false;
});


/*****************[ search contacts using AJAX ]************************/


var searchRequest = null;

$(function () {
    var minlength = 0

    var baseUrl = base_url + 'contact/search/';

    $(".contact_search").keyup(function () {
        var that = this,
        value = $(this).val();

        //if (value.length >= minlength ) {
            if (searchRequest != null) 
                searchRequest.abort();
                searchRequest = $.ajax({
                    type: "GET",
                    url: baseUrl,
                    data: {
                        'search' : value
                    },
                    success: function(response){
                       //console.log(response);                   
                       var data = $.parseJSON(response);

                       if(data.success == 1){
                          console.log('here12');
                          $('.contact_list').html('');           
                          $('.contact_list').html(data.searchdata);
                       }
                    }
                });
        //}
    });
});


/************* get contact id on click of li ******************/

$(document).on('click', '.ciid', function(){
	var baseUrl = base_url + 'contact/data/';
	var cid = $(this).attr('cid');
	var datastring = {cid:cid};
	$.ajax({
		url: baseUrl,
		type: 'post',
		data: datastring,
		success: function (response) {
			// do something with response
			var data = $.parseJSON(response)     
			if(data.success == 1){
				$('.personal_div1').html('');
				$('.personal_div2').html('');
				$('.contact_quotes').html('');
				$('.comm_sec').html('');
				$('.invoices_list').html('');
				$('.appointment_list').html('');                  

				$('.personal_div1').html(data.profiledata);
				$('.personal_div2').html(data.quotedata);
				$('.contact_quotes').html(data.quotetabdata);
				$('.comm_sec').html(data.commdata);
				$('.invoices_list').html(data.invoicedata);
				$('.appointment_list').html(data.appointmentdata);
			}
		},
		error: function () {
			alert("Something went wrong");
		}
	});
});

/***********[ to reset form when  bootstrap modal is hidden or closed]**************/

$(document).on('hidden.bs.modal', '#addContact', function () {

    $(this).find('form').trigger('reset');
});




$(document).ready(function(){



   /* 
      comment:for modal pop up 
      name :ravikant 
      date :20181209

    */
    $(function(){
      $('#modalEventButton').click(function(){
        $('#modalevent').modal('show')
        .find('#modalEvent')
        .load ($(this).attr('value'));
       });
    });

    /*

      comment:to remove attribute tab index 
      name :ravikant 
      date :20181209

    */
    $("#modalEventButton").click(function(){
        $("#modalevent").removeAttr("tabindex");
    });


    /*

      comment:for searching the event
      name :ravikant 
      date :20181209

    */
    // $("#search").on("keyup", function(){
    //   $('.noAppointment').remove();
    //   var searchClient = $(this).val();
    //   //console.log(searchClient);
    //   searchClient = searchClient.toLowerCase();
    //   if(searchClient != ""){
    //       $('.tosearch').show();
    //       var flag=0;
    //       $(".contactName").each(function(){
    //           var thisHtml = $(this).html();
    //           thisHtml = thisHtml.toLowerCase();
    //           //console.log(thisHtml);
    //           if(thisHtml.indexOf(searchClient) < 0 ){
    //             $(this).parent().hide();
    //           }
    //           else
    //           {
    //             flag=1;
    //           }

    //       });
    //       if(flag ==0)
    //       {
    //         var fieldHtml="<li class='alert alert-danger noAppointment'>NO Appointments</li>";
    //         $('.SelectedEventlist').append(fieldHtml);
    //       }
    //   }
    //   else
    //   {
    //     $('.tosearch').show();
    //   }
    // }); 
})




/*
	comment:form validation and form submission
	name :ravikant 
	date :20181209
*/

function validateData()
{
	var name=document.getElementById('userDataSelect').value;
	var type =document.getElementById('appointmentTypeID').value;
	var member=document.getElementById('memberID').value;
	var from =document.getElementById('timepicker1').value;
	console.log(from);
	var to   =document.getElementById('timepicker2').value;
	var note =document.getElementById('noteId').value;

	var error = 0;
	var text;
	if(name=="")
	{
	error = 1;
	text = "Please Enter Customer Name";
	}
	else if(type=="")
	{
	error = 1;
	text = "Please Enter Appointment Type";
	}
	else if(member=="")
	{
	error = 1;
	text = "Please Select Team Member";
	}
	else if(from=="")
	{
	error = 1;
	text = "Please Select Appointment Start Time";          
	}
	else if(note=="")
	{
	error = 1;
	text = "Please Enter Notes";
	}
	else if(to=="")
	{
	error = 1;
	text = "Please Select Appointment End Time";
	}

	if(error == 1){
	swal({
		title: 'PAINTPAD',
		text: text,
		type: 'error',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'OK!'
	});  
	return false;
	}
	else
	{
		$('body').on('beforeSubmit', '#myCalender', function (e) {
		var pathname = window.location.href;       
		var form = $(this);
		$.ajax({
			url: form.attr('action'),
			type: 'post',
			data: form.serialize(),
			success: function (response) {
			if(response == 1){
				$("#modalevent").modal('hide');
				$("#w0").fullCalendar( 'refetchEvents' );
				//$.pjax.reload('#eventpjax' , {timeout : false});
			}
			}
		});
		e.stopImmediatePropagation();
		return false;
		});
	}
}


/*

	comment:form validation and form updation
	name :ravikant 
	date :20181209

*/


function validateForm() {

	var name   = document.getElementById('userDataSelectUpdate').value;
	var type   = document.getElementById('appointmentTypeIDUpdate').value;
	var member = document.getElementById('memberIDUpdate').value;
	var from   = document.getElementById('timepicker1Update').value;
	console.log(from);
	var to     = document.getElementById('timepicker2Update').value;
	var note   = document.getElementById('noteIdUpdate').value;

	var error = 0;
	var text;

	if(name=="")
	{
		error = 1;
		text = "Please Enter Customer Name";
	}
	else if(type=="")
	{
		error = 1;
		text = "Please Enter Appointment Type";
	}
	else if(member=="")
	{
		error = 1;
		text = "Please Select Team Member";
	}
	else if(from=="")
	{
		error = 1;
		text = "Please Select Appointment Start Time";          
	}
	else if(note=="")
	{
		error = 1;
		text = "Please Enter Notes";
	}
	else if(to=="")
	{
		error = 1;
		text = "Please Select Appointment End Time";
	}

	if(error == 1){
		swal({
			title: 'PAINTPAD',
			text: text,
			type: 'error',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'OK!'
		});  
		return false;
	}
	else
	{
		$('body').on('beforeSubmit', '#myCalender2', function (e) {
		var pathname = window.location.href;       
		var form = $(this);
		$.ajax({
			url: form.attr('action'),
			type: 'post',
			data: form.serialize(),
			success: function (response) {
			if(response == 1){
				$("#modalevent").modal('hide');
				$("#w0").fullCalendar( 'refetchEvents' );
				//$.pjax.reload('#eventpjax' , {timeout : false});
			}
			}
		});
		e.stopImmediatePropagation();
		return false;
		});
	}
}





/*
 * Custom js for PaintPad
 * By Sat Singh Rana
 */

$(function(){



	$("#client_details_same").click( function(){

		var checkedStatus = $(this).is(":checked");
		var clearBtn 	  = $(".clear-site-details");

		if (checkedStatus) {
			clearBtn.addClass('disabled');
			clearBtn.attr('disabled', 'disabled');
		}else {
			clearBtn.removeAttr('disabled');
			clearBtn.removeClass('disabled');
		}
	});

	$(document).on('click','.clear-site-details', function(){	
		
		$(".site-details-holder").find("input[type=text]").val('')

	});
	$(document).on("click", '.new-client-details-clear', function(){

		$('.new-client-details-holder').find("input[type=text]").val("");
	});


	$('.newQuote-clientDetails-clear').click(function(){

		console.log("newQuote-clientDetails-clear  clicked")

		$(".newQuote-clientDetails-holder input[type=text]").val('');

	});



		$("#clientName").change(function(){
			
			if($('#client_details_same').is(':checked') == true){
			
				$("#site_contactName").val($(this).val());

			}

		});
		$("#contactEmail").change(function(){
			if($('#client_details_same').is(':checked') == true){

				$("#site_contactEmail").val($(this).val());

			}

		});
		$("#contactPhone").change(function(){
			
			if($('#client_details_same').is(':checked') == true){
				
				$("#site_contactPhone").val($(this).val());

			}

		});



		/*
		 *
		 * Same site details 
		 *
		 */


		$("#client_details_same").click(function(){
			var status = $(this).is(':checked');

			// alert(status)
			if (status) {


				$(".site-details-holder-clear").addClass("disabled");
				
				$(".site-details-holder-clear").attr("disabled", "disabled");
			
			}else{
				
				$(".site-details-holder-clear").removeClass("disabled");
				$(".site-details-holder-clear").removeAttr("disabled");

			}


		});



		$("#client_details_same").change(function(){

			var status = $(this).is(':checked');

			if (status) {


				var address 	 = $('#address').val();
				var street 		 = $('#street').val();
				var suburb 		 = $('#suburb').val();
				var state 		 = $('#state').val();
				var postcode 	 = $('#zipCode').val();
				var contactName  = $('#clientName').val();
				var contactEmail = $('#contactEmail').val();
				var contactPhone = $('#contactPhone').val();


				$("#search_address").val( address );
				$("#site_street").val( street );
				$("#site_suburb").val( suburb );
				$("#site_state").val( state );
				$("#site_zipCode").val( postcode );
				$("#site_contactName").val( contactName );
				$("#site_contactEmail").val( contactEmail );
				$("#site_contactPhone").val( contactPhone );


			}

		});

		$(".site-details-holder-clear").click(function(){

			$(".site-details-holder input[type=text]").val('')

		});



		$('.edit-existing-contact').click(function(){

            // $('#InteriorQuote').modal('hide');

            var id         = $(this).data("contact_id");
            var sid        = $(this).data('subscriber_id');
            var contactUrl = base_url+"webservice/update-contact";

            $(".app-ovelray").fadeIn();


            $('#edit-contact-popup').modal('show');

            $(function () {
                $.getJSON(
                base_url+"webservice/contact-details?subscriber_id="+sid+"&contact_id="+id,
                function (data) {


                    $('#contactName-clientPopup').val(data.data.contact.name);
                    $('#contactEmail-clientPopup').val(data.data.contact.email);
                    $('#contactPhone-clientPopup').val(data.data.contact.phone);
                    $('#lat-clientPopup').val(data.data.contact.lat);
                    $('#lng-clientPopup').val(data.data.contact.lng);
                    $('#address-clientPopup').val(data.data.contact.formatted_addr);
                    $('#street-clientPopup').val(data.data.contact.street1);
                    $('#suburb-clientPopup').val(data.data.contact.suburb);
                    $('#state-clientPopup').val(data.data.contact.state);
                    $('#zipCode-clientPopup').val(data.data.contact.postal);
                    $('#contactImage-clientPopup').attr("src",data.data.contact.image);
                    $('#contact_idForm-clientPopup').val(data.data.contact.contact_id);
                    
                    $('#subscriber_idForm').val(sid);
                    $('#contact_idForm').val(id);

                    if (mapContact != undefined) {

						mapContact.setCenter(new google.maps.LatLng(data.data.contact.lat, data.data.contact.lng));

                    }

            		$(".app-ovelray").fadeOut();


                }
                )
            })
        })

		/*
		 * Close contact edit popup
		 */
		
		$("#close-contactEdit-popup").click( function(){
		 	
		 	$("#edit-contact-popup input[type=text]").val('');
		 	$("#edit-contact-popup").modal('hide');


		});

		$("#save-contactEdit-popup").click( function(){
			
			var contactUrl = base_url+"webservice/update-contact";

			var form = $('form')[0]; // You need to use standard javascript object here
			var form = $('#contactId')[0];
			var data = new FormData(form);

			$(".app-ovelray").fadeIn();

			$.ajax({
				url:  contactUrl,
				type: "POST",
				data: data,
				success: function(response) {

					

					response = JSON.parse( response );

						
					var new_contact_id 		= response.data.contact_id;
					// var new_country 		= response.data.country;
					var new_email 			= response.data.email;
					// var new_formatted_addr 	= response.data.formatted_addr;
					var new_image 			= response.data.image;
					// var new_lat 			= response.data.lat;
					// var new_lng 			= response.data.lng;
					var new_name 			= response.data.name;
					var new_phone 			= response.data.phone;
					// var new_postal 			= response.data.postal;
					// var new_state 			= response.data.state;
					// var new_street1 		= response.data.street1;
					// var new_street2 		= response.data.street2;
					// var new_suburb 			= response.data.suburb;

					if (new_image != "") {
						$(".profile_img img").attr("src", new_image );
						$("#contact_"+new_contact_id+" img").attr("src", new_image);
						

						$("#contact_"+new_contact_id).attr("data-image", new_image);




					}

					$("#client_name").text( new_name );
					$("#client_email").html( new_email+"<br>"+new_phone );

					$("#contact_"+new_contact_id+" .existing_client_detail .client_name_list").text( new_name )
					$("#contact_"+new_contact_id+" .existing_client_detail .client_phone_list").text( new_phone )
					
					
					$("#edit-contact-popup input[type=text]").val('');
		 			$("#edit-contact-popup").modal('hide');
					
					$(".app-ovelray").fadeOut();

				},
				contentType: false,
				processData:false,
			});
				
		});



		$('#new-contact-image').click( function(){
			// console.log('image clicked')
			// $("#contact-edit-image").trigger('click');
			document.getElementById("contact-edit-image").click();

		});
		
		/*	
			$('#show-communications').click( function(){
				
				// $('#page-title-paintpad').text('communication');					
				// $('.show-quotes-holder').fadeIn(100);
				
				$.ajax({
					url: 'http://www.enacteservices.com/paintpad/webservice/quotes?company_id=1',
					method: 'GET',
					dataType:'JSON',
					// data : {company_id:12},
					success : function(res){
						
						console.log(res);
						
						// $.each(res.data, function(d,i){
							
						// 	console.log(d)
						// 	console.log(i)
							
						// });
						
					},
				});
				
				
				
				
				
			});
			
			$('#close-communication-holder').click( function(){
				
				$('.show-quotes-holder-data').html('');
				$('.show-quotes-holder').hide();
				
				
			});
		*/

		/*
		 * COMMUNICATION
		 *
		 */
		 
		 var currentPage = window.location.href;
		 
		 		 
		if ( currentPage.indexOf('communications') > -1) {
						
			var data = '{"company_id":1}';
			
			$('#page-title-paintpad').text('communication');
			
			$.ajax( {
				url : base_url+'webservice/communication?subscriber_id=1',
				// https://www.enacteservices.net/paintpad/webservice/communication?subscriber_id=1
				method : 'GET',
				type:'JSON',
				// data : {json: data},
				success : function(res){
					
					var res = JSON.parse(res);
					var q = res.data.emails;
					
					var qHtml = '';
					
					$.each(q, function(i,d){
						
						var desc = d.body;
						
						if (!desc) {
							desc = '';
						}else{
							
							desc = d.body.substr(0, 380);							
							
							if (desc.length == 380) {
								desc = desc+'...';
								
								
							}
						}
						
						var date =  d.date;
						
						var months = {0 : "January",1 : "Feburary",2 : "March",3 : "April",4 : "May",5 : "June",6 : "July",7 : "August",8 : "September",9 : "October",10 : "November",11 : "December"};
						
						// multiplied by 1000 so that the argument is in milliseconds, not seconds.
						var date 	= new Date(date*1000);						
						var day 	= date.getDate();						
						var monthIndex 	= date.getMonth();						
						var year = date.getFullYear();

						var fullDate = day + ' ' + months[monthIndex] + ' ' + year;
												
						qHtml += '<div class="communication-row-main">';
						qHtml += '<table class="table" style="border-bottom: 1px solid #ebebeb;">';
							qHtml += '<tr><th style="border:none" colspan="2" class="com-sub">'+d.subject+'</th></tr>';
							qHtml += '<span style="display:none;" class="com-view-id" >'+d.comm_id;+'</span>';
							qHtml += '<tr><th style="border:none" class="com-title">From</th><td style="border:none" class="com-from">'+d.From+'</td></tr>';
							qHtml += '<tr><th style="border:none" class="com-title">Date</th><td style="border:none" class="com-date">'+fullDate+'</td></tr>';
							qHtml += '<tr><th style="border:none" class="com-title">To</th><td style="border:none" class="com-to">'+d.to+'</td></tr>';
							qHtml += '<tr><th style="border:none"></th><td style="border:none" class="com-desc"><div style="text-align:justify;">'+desc+'</div></td></tr>';
							qHtml += '<tr><th style="border:none"></th><td style="border:none">';
							qHtml += '<span class="com-view-more"><a href="'+base_url+'communications/view?id='+d.comm_id+'">View More</a></span>';
							qHtml += '<span class="com-view-body" style="display:none">'+d.body+'</span>';
							qHtml += '</td></tr>';
						qHtml += '</table>';
						qHtml += '</div>';
						
					});
					
					$('.communications-data').append(qHtml);
					$(".com-desc img").hide();
				}				
			});
		 	
		}
				
		$(document).on('click', '.hide-com-show' , function(){			
			$('#com-view-popup').modal('hide');
		});
		
		$(".search-btn").click(function(){
			
			if ($(this).hasClass('open')) {			
				$(this).removeClass('open');	
				$("#com-search").fadeOut(400);
				
				setTimeout(function(){
					$("#com-search").val('');
				},400);
				
				$(".search-btn").css('right',"10px");
				$(".search-btn").attr('src', base_url+'image/search@3x.png');
			} else {
				$(this).addClass('open');
				$("#com-search").hide().fadeIn(500);
				$(".search-btn").css('right',"250px");
				$(".search-btn").attr('src', base_url+'image/close@3x.png');				
			}
			
			
		});
		
		/*
		 * Search communication...
		 */
		 
		//setup before functions
		var typingTimer;                //timer identifier
		var doneTypingInterval = 500;  
		var $input = $('#com-search');

		//on keyup, start the countdown
		$input.on('keyup', function () {
		  clearTimeout(typingTimer);
		  typingTimer = setTimeout(doneTypingForSearch, doneTypingInterval);
		});

		//on keydown, clear the countdown 
		$input.on('keydown', function () {
		  clearTimeout(typingTimer);
		});



		$(document).ready(function(){

			$('#modalEventButton').click(function(){
				$('#myModal').modal('show')
					.find('.modal-content')
					.load ($(this).attr('value'));
			});
		});


		function openFile(thi){
			console.log(thi);
			$('#modalEventButton').click(function(){
				$('#myModal').modal('show')
					.find('.modal-content')
					.load ($(this).attr('value'));
			});
		}



		//Communication File Modal
		$("div").on("click",".fileCommunication", function(e){
			alert('yuhooo!!!');
			console.log(this);exit();



			var sid='<?php echo $sid; ?>';
			var id = $(".createCommunications").attr("data-id");
			$.ajax({
				url: base_url+'/communications/create',
				headers: {'Quoteid': id,'SubscriberID':sid,'fromWeb':1},
				success: function (response) {
					$('#modal-bodyCommunicationsCreate').html(response);
				}
			});
			$('#createCommunicationsModal').modal('show');
			e.stopImmediatePropagation();
			return false;
		});

		//Communication create 
		$("div").on("click",".createCommunications", function(e){
			var sid='<?php echo $sid; ?>';
			var id = $(".createCommunications").attr("data-id");
			$.ajax({
				url: base_url+'/communications/create',
				headers: {'Quoteid': id,'SubscriberID':sid,'fromWeb':1},
				success: function (response) {
					$('#modal-bodyCommunicationsCreate').html(response);
				}
			});
			$('#createCommunicationsModal').modal('show');
			e.stopImmediatePropagation();
			return false;
		});

		
		function doneTypingForSearch () {
		  
		  /*
		   * Call the ajax to filter the results
		   *
		   */
		   
		   var filter = $("#com-search").val();
		   
		   
		   var data = '{"company_id":1,"filter":"'+filter+'"}';
			
			$('#page-title-paintpad').text('communication');
			
			$.ajax( {
				url : base_url+'webservice/communication?subscriber_id=1&filter='+filter,
				method : 'GET',
				type:'JSON',
				// data : {json: data},
				beforeSend: function(){
					
					$(".app-ovelray").show();
					
					$(".communications-data").html("");
					
				},
				success : function(res){
					$(".app-ovelray").hide();
					
					var res = JSON.parse(res);
					var q = res.data.emails;
					
					var qHtml = '';
					
					$.each(q, function(i,d){
						
						var desc = d.body;
						
						if (!desc) {
							desc = '';
						}else{
							
							desc = d.body.substr(0, 380);
							
							if (desc.length == 380) {
								desc = desc+'...';
								
								
							}
						}
						
						var date =  d.date;
						
						var months = {0 : "January",1 : "Feburary",2 : "March",3 : "April",4 : "May",5 : "June",6 : "July",7 : "August",8 : "September",9 : "October",10 : "November",11 : "December"};
						
						// multiplied by 1000 so that the argument is in milliseconds, not seconds.
						var date 	= new Date(date*1000);						
						var day 	= date.getDate();						
						var monthIndex 	= date.getMonth();						
						var year = date.getFullYear();

						var fullDate = day + ' ' + months[monthIndex] + ' ' + year;
												
						qHtml += '<div class="communication-row-main">';
						qHtml += '<table class="table" style="border-bottom: 1px solid #ebebeb;">';
							qHtml += '<tr><th style="border:none" colspan="2" class="com-sub">'+d.subject+'</th></tr>';
							qHtml += '<tr><th style="border:none" class="com-title">From</th><td style="border:none" class="com-from">'+d.From+'</td></tr>';
							qHtml += '<tr><th style="border:none" class="com-title">Date</th><td style="border:none" class="com-date">'+fullDate+'</td></tr>';
							qHtml += '<tr><th style="border:none" class="com-title">To</th><td style="border:none" class="com-to">'+d.to+'</td></tr>';
							qHtml += '<tr><th style="border:none"></th><td style="border:none" class="com-desc"><div style="text-align:justify;">'+desc+'</div></td></tr>';
							qHtml += '<tr><th style="border:none"></th><td style="border:none">';
							qHtml += '<span class="com-view-more">View More</span>';
							qHtml += '<span class="com-view-body" style="display:none">'+d.body+'</span>';
							qHtml += '</td></tr>';
						qHtml += '</table>';
						qHtml += '</div>';
						
					});
					
					$('.communications-data').append(qHtml);
					$(".com-desc img").hide();
				}				
			});
		  		  		  
		}
		
		
})


function tofixed(val,range){
	var res = parseFloat(val).toFixed(range);
	return res;
}

$("#paymentNoteOptions").on('change',function(){
	var valText = $("#paymentNoteOptions option:selected").text();
	$('#progress-desc ').val(valText);
});


$("#noOfPainters").on('change, keyup',function(){
	var noOfpainter = $("#noOfPainters").val();
	if(noOfpainter > 0){
		var totHours = $('#daysToComplete').data('totalHours');
		var totNoOfDays = Math.ceil(totHours/8);
		var finalDays = Math.ceil(parseFloat(totNoOfDays) / parseFloat(noOfpainter));
		if(finalDays < 1){
			finalDays = 1;
		}

		$('#daysToComplete').text(finalDays);
	}
	
});
$('#accept_tnc_btn').on('click',function(){
	$('#tnc_on_off').toggle();
	if($('#accept_tnc_btn').text() == 'Accepted'){
		$('#accept_tnc_btn').text('I Accept');
	}else{
		$('#accept_tnc_btn').text('Accepted');
	}
});


$("#roomSpecialItmesAll").on('change',function(){
	// alert('yuhoo');
	var itemName = $("#roomSpecialItmesAll option:selected").text();
	var itemPrice = $("#roomSpecialItmesAll option:selected").data('price');
	var itemId = $("#roomSpecialItmesAll option:selected").val();

	var str = '';

	str += '<tr data-id="'+itemId+'"><td><div class="table_checkbox"><label class="checkbox_label"><input type="checkbox" checked="checked">';
	str += '<span class="checkmark"></span></label></div></td><td><input type="text" class="form-control item_input" name="" placeholder="" value="'+itemName+'"> </td>';
	str += '<td><input type="number" class="form-control item_input" name="labour" placeholder="" min="0" value="0"></td><td><span class="cross_row" style="color: blue;">×</span></td>';
	str += '<td><input type="number" class="form-control item_input" name="hours" placeholder="" min="0" value="0"></td>';
	str += '<td><div class=""><input type="number" name="" value="'+itemPrice+'" placeholder="" class="form-control item_input number_item"></div></td>';
	// str += '<td><div class="item_price"><input type="text" value="'+itemPrice+'" Placeholder="0" class="form-control number_item"></div></td>';
	str += '<td><input type="text" value="" Placeholder="0" class="form-control item_input number_item2"></td><td><div class="table_checkbox"><span class="cross_row">ⓘ</span><label class="checkbox_label"><input type="checkbox" checked="checked"><span class="checkmark"></span></label></div>';
	str += '<td><p style="font-size: 17px" class="rowTotal"><span>$<span>0.00</p></td><td><span onclick="$($(this).offset(\'tr\')).remove()" class="cross_row">&times;</span></td></tr>';
	
	$('#special_item_list tbody').append(str);
});


$(document).on('click',".delRoom", function(e){
	if (confirm("Are you sure to remove?")) {
	  deleteQuoteRoom(this);
	}
	
});

function deleteQuoteRoom(comp){
	var company_id 	  = $('#company_id').val();
	var quote_id 	  = $('#quote_id').val();
	var room_id 	  = $(comp).data('room_id');
	var data = '{"room_id" : "'+room_id+'","quote_id":"'+quote_id+'","company_id" : "'+company_id+'"}';
    $('.app-ovelray').show();
    $.ajax({
        type: 'POST',
        url: siteBaseUrl+'/webservice/delete-room',
        data : {"json":data},
        success: function(response){ 
    	var response = JSON.parse(response);
            if(response.success == 1){  
            	// $(comp.closest('.rooms_tab')).remove();
            	$('[data-href="#panel4"]').trigger('click');
            	$('.app-ovelray').hide();
            }else{
            	$('.app-ovelray').hide();
                alert(response.message);
            }
        }
    });
}

$(document).on('click',".room_default_btn", function(e){

	var length = $("#r_l").val();
	var width = $("#r_w").val();
	var height = $("#r_h").val();

	var rname = $("#room_name").val();
	if(rname == ''){
		alert('Please fill the Room Name!');
	}else if(length == '' || width == '' || height == ''){
		alert('Please fill in the Room Dimesions');
	}else{
		addQuoteRoom();
	}
});

$(document).on('click','#saveRoom',function(){
	var length = $("#r_l").val();
	var width  = $("#r_w").val();
	var height = $("#r_h").val();
	var rname  = $("#room_name").val();

	if(rname == ''){
		alert('Please fill the Room Name!');
	}else if(length == '' || width == '' || height == ''){
		alert('Please fill in the Room Dimesions');
	}else{
		addQuoteRoom();
	}
	$('#addRooms').modal('hide');
});


function addQuoteRoom(){
	var length 			= $("#r_l").val();
	var width 			= $("#r_w").val();
	var height 			= $("#r_h").val();
	var rname 			= $("#room_name").val();
	var company_id 	  	= $('#company_id').val();
	var quote_id 	  	= $('#quote_id').val();
	var type_id		  	= $('#quote_type').val();
	var roomType		= $('#roomType option:selected').val();
	var tbl_room_notes  = [];
	var tbl_room_pref 	= '[{ "isSelected" : 0, "room_comp_pref" : [ ],"comp_id" : 1, "isUndercoat" : 1, "custom_color_name" : "Wattyl Acapulco Violet", "color_id" : null },{ "isSelected" : 0, "room_comp_pref" : [ ],"comp_id" : 4, "isUndercoat" : 1, "custom_color_name" : "Wattyl Acapulco Violet", "color_id" : null },{ "isSelected" : 0, "room_comp_pref" : [ ],"comp_id" : 5, "isUndercoat" : 1, "custom_color_name" : "Wattyl Acapulco Violet", "color_id" : null },{ "isSelected" : 0, "room_comp_pref" : [ ],"comp_id" : 6, "isUndercoat" : 1, "custom_color_name" : "Wattyl Acapulco Violet", "color_id" : null },{ "isSelected" : 0, "room_comp_pref" : [ ],"comp_id" : 73, "isUndercoat" : 1, "custom_color_name" : "Wattyl Acapulco Violet", "color_id" : null },{ "isSelected" : 0, "room_comp_pref" : [ ],"comp_id" : 16, "isUndercoat" : 1, "custom_color_name" : "", "color_id" : null },{ "isSelected" : 0, "room_comp_pref" : [ ],"comp_id" : 10, "isUndercoat" : 1, "custom_color_name" : "", "color_id" : null },{ "isSelected" : 0, "room_comp_pref" : [ ],"comp_id" : 11, "isUndercoat" : 1, "custom_color_name" : "", "color_id" : null },{ "isSelected" : 0, "room_comp_pref" : [ ],"comp_id" : 12, "isUndercoat" : 1, "custom_color_name" : "", "color_id" : null },{ "isSelected" : 0, "room_comp_pref" : [ ],"comp_id" : 13, "isUndercoat" : 1, "custom_color_name" : "", "color_id" : null },{ "isSelected" : 0, "room_comp_pref" : [ ],"comp_id" : 14, "isUndercoat" : 1, "custom_color_name" : "", "color_id" : null },{ "isSelected" : 0, "room_comp_pref" : [ ],"comp_id" : 15, "isUndercoat" : 1, "custom_color_name" : "", "color_id" : null },{ "isSelected" : 0, "room_comp_pref" : [ ],"comp_id" : 17, "isUndercoat" : 1, "custom_color_name" : "", "color_id" : null },{ "isSelected" : 0, "room_comp_pref" : [ ],"comp_id" : 18, "isUndercoat" : 1, "custom_color_name" : "", "color_id" : null },{ "isSelected" : 0, "room_comp_pref" : [ ],"comp_id" : 20, "isUndercoat" : 1, "custom_color_name" : "", "color_id" : null },{ "isSelected" : 0, "room_comp_pref" : [ ],"comp_id" : 75, "isUndercoat" : 1, "custom_color_name" : "", "color_id" : null },{ "isSelected" : 0, "room_comp_pref" : [ ],"comp_id" : 7, "isUndercoat" : 1, "custom_color_name" : "", "color_id" : null },{ "isSelected" : 0, "room_comp_pref" : [ ],"comp_id" : 8, "isUndercoat" : 1, "custom_color_name" : "", "color_id" : null },{ "isSelected" : 0, "room_comp_pref" : [ ],"comp_id" : 9, "isUndercoat" : 1, "custom_color_name" : "", "color_id" : null },{ "isSelected" : 0, "room_comp_pref" : [ ],"comp_id" : 19, "isUndercoat" : 1, "custom_color_name" : "", "color_id" : null },{ "isSelected" : 0, "room_comp_pref" : [ ],"comp_id" : 40, "isUndercoat" : 1, "custom_color_name" : "", "color_id" : null }]';
	var tbl_room_special_items = [];
	/*var length = $('#r_l').val();
	var width = $('#r_w').val();
	var height = $('#r_h').val();*/

	// var tbl_room_dimensions = '[{"length" : '+length+',"width" : '+width+',"height" : '+height+',"selected" : 0}]';
	var tbl_room_dimensions = ['{"length" : 0,"width" : 0,"height" : 0,"selected" : 0}'];
	var tbl_rooms = '{"is_custom" : 0,"room_name" : "'+rname+'","room_type_id" : '+roomType+',"include" : 1,"room_id" : 0}';

	// var data = '{"tbl_room_notes" : "'+tbl_room_notes+'","company_id" : "'+company_id+'","tbl_room_pref" :"'+tbl_room_pref+'","type_id" : "'+type_id+'","tbl_room_special_items" :"'+tbl_room_special_items+'","quote_id":"'+quote_id+'","tbl_room_dimensions":"'+tbl_room_dimensions+'","tbl_rooms" : "'+tbl_rooms+'"}';
	var data = '{"tbl_room_notes" : [],"company_id" : "'+company_id+'","tbl_room_pref" :'+tbl_room_pref+',"type_id" : "'+type_id+'","tbl_room_special_items" :[],"is_save_cust_special_items" : 0,"quote_id":"'+quote_id+'","tbl_room_dimensions":'+tbl_room_dimensions+',"tbl_rooms" : '+tbl_rooms+'}';
	

    $('.app-ovelray').show();
    $.ajax({
        type: 'POST',
        url: siteBaseUrl+'/webservice/save-room-details',
        data : {"json":data},
        success: function(response){ 
    	var response = JSON.parse(response);
            if(response.success == 1){  
            	var dataToGetRooms = '{"quote_id" : '+quote_id+',"type_id" : '+type_id+'}';
            	getQUoteAllRooms(dataToGetRooms);
            	$('.app-ovelray').hide();
            }else{
            	$('.app-ovelray').hide();
                alert(response.message);
            }
        }
    });
}
function getQUoteAllRooms(data){
	$('.app-ovelray').show();
	$.ajax({
		url: siteBaseUrl+'/webservice/get-room-details',
		type: 'post',
		data: {'json':data},
		success: function (response) {							
			var response = JSON.parse(response);

			if(response.success == '1'){
				var roomTypeStr = '';
				$.each(response.data.roomCountWithType, function(a,b){
					roomTypeStr += '<li onclick="$(this).toggleClass(\'active\')" data-type="'+b.id+'"><a href="javasript:void(0)"><img src="'+b.blue_image+'" alt=""> <span class="count_no">'+b.count+'</span></a></li>'
				});
				$('#rooms_type_left').html(roomTypeStr);

				var str = '';
				$.each(response.data.rooms, function(i,d){
					str += '<div class="rooms_tab room_type_'+d.roomDetails.room_type.id+'" ><div class="left_tab_icons"><img style="width:85%;" src="'+d.roomDetails.room_type.white_image+'" alt=""></div>	<div class="right_tab_contentDetail"><div class="topRightTab"><h3>'+d.roomDetails.room_type.name+'<span class="CountClass">'+d.roomDetails.room_name+'</span></h3>'; 
					str +='<ul class="edit_dlt_list"><li><a href="javascript:void(0);"><img src="https://enacteservices.net/paintpad/image/edit.png" alt=""></a></li><li class="delRoom" data-room_id="'+d.roomDetails.room_id+'"><a href="javascript:void(0)"><img src="https://enacteservices.net/paintpad/image/delete.png" alt=""></a></li></ul></div>';
					str += '<div class="rooms_tab_main"><ul class="rooms_tab_list mCustomScrollbar" style="cursor: all-scroll;">';
					
					var dataArwithSavedData = [];
					var dataArwithSavedDatabutNoSelect = [];
					var dataArwithoutSavedData = [];
					$.each(d.components, function(x,y){	
						if(y.savedData != ''){	
							y.savedData.isSelected
							if(y.savedData.isSelected == 1){
								dataArwithSavedData.push(y);
							}else{
								dataArwithoutSavedData.push(y);
							}											
						}
					});
					dataArwithSavedData = (dataArwithSavedData.sort(strDes)).concat((dataArwithoutSavedData).sort(strDes));
					$.each(dataArwithSavedData, function(x,y){										
						var isSelected = (y.savedData.isSelected == 1) ? "complete" : "";
						var compName = ((y.comp_name).length > 11) ? (y.comp_name).substr(0,11)+'...' : y.comp_name;
						str += '<li class="'+isSelected+'"><p>'+compName+'</p><span class="room_steps"></span></li>';
					});
					str += '</ul></div></div></div>';
				});
				$('#add_room_main').show();
				$('#rooms_tabs').html(str);
			}
			$('.app-ovelray').hide();
		},
		complete: function () {
			$('.app-ovelray').hide();
	        $('.rooms_tab_list').mCustomScrollbar({
				axis:"x",
				hideScrollbar: true
			});
	    },
		error: function () {
			$('.app-ovelray').hide();
			alert("Something went wrong");
		}
	});
}
function strDes(a, b) {
	if (a.comp_name>b.comp_name) return 1;
	else if (a.comp_name<b.comp_name) return -1;
	else return 0;
}






