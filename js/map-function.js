/*
 * Name 	   : PPMF
 * Dev  	   :
 * Description : All the map related functions will be in this script
 * Created 	   : 
 * Updated	   : 08-04-2019
 */


var marker, editQuoteMapStatic, mapStatic, mapSite;

var base_url 	   = window.location.origin + '/' + window.location.pathname.split ('/') [1] + '/';
var map 	 	   = null;
var regex 		   = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z]{2,4})+$/;
var quoteType 	   = 0;
var eventMoves 	   = ['dragend', 'zoom_changed'];
var saveType 	   = 'create';
var curQuoteScreen = 0;

function showlocation() {
	if ("geolocation" in navigator) {
		navigator.geolocation.getCurrentPosition(callback, error);
	} else {
		console.warn("geolocation IS NOT available");
	}
}

function callback(position) {
	var lat = position.coords.latitude;
	var lon = position.coords.longitude;
	document.getElementById('default_latitude').value = lat;
	document.getElementById('default_longitude').value = lon;
	var latLong = new google.maps.LatLng(lat, lon);
	map.setZoom(16);
	map.setCenter(latLong);
}



var placeSearch, autocomplete;

var componentForm = {
	street_number: 'short_name',
	route: 'long_name',
	locality: 'long_name',
	administrative_area_level_1: 'long_name',
	country: 'long_name',
	postal_code: 'short_name'
};

var inUseForm = {
	sublocality: 'short_name',
	locality: 'long_name',
	postal_code: 'short_name'
};

	function fillInAddress() {

		var place 		   = autocomplete.getPlace();
		var haveRoute 	   = 0;
		var haveStreet     = 0;
		var haveSubloc     = 0;
		var haveLocality   = 0;
		var havePostalCode = 0;

		for (var i = 0; i < place.address_components.length; i++) {

			var addressType = place.address_components[i].types[0];

			var val = place.address_components[i][componentForm[addressType]];

			if (addressType == 'street_number') {
				street_number = val;
				haveStreet = 1;
				$("#street").val(street_number);
			}

			if (addressType == 'route') {
				route = val;
				haveRoute = 1;
				$("#street").val(street_number+" "+val);
			}

			if (addressType == 'locality') {
				haveSubloc = 1;
				document.getElementById('suburb').value = val;
			}

			if (addressType == 'administrative_area_level_1') {
				haveLocality = 1;
				document.getElementById('state').value = val;
			}

			if (addressType == 'postal_code') {
				havePostalCode = 1;
				document.getElementById('zipCode').value = val;
			}
		}

		$("#lat").val(place.geometry.location.lat);
		$("#lng").val(place.geometry.location.lng);

		if(haveStreet == 0 && haveRoute == 0) {
			document.getElementById('suburb').value = "";
		}
		if(haveSubloc == 0) {
			document.getElementById('suburb').value = "";
		}
		if(haveLocality == 0) {
			document.getElementById('state').value = "";
		}
		if(havePostalCode == 0) {
			document.getElementById('zipCode').value = "";
		}
	}
	function fillInAddressSite() {

		var place 		   = autocompleteSite.getPlace();
		var haveSubloc 	   = 0;
		var haveLocality   = 0;
		var havePostalCode = 0;

		for (var i = 0; i < place.address_components.length; i++) {

			var addressType = place.address_components[i].types[0];
			var val = place.address_components[i][componentForm[addressType]];

			if (addressType == 'sublocality') {

				haveSubloc = 1;
				document.getElementById('site_suburb').value = val;
			}

			if (addressType == 'locality') {
				haveLocality = 1;
				document.getElementById('site_state').value = val;
			}

			if (addressType == 'postal_code') {
				havePostalCode = 1;
				document.getElementById('site_zipCode').value = val;
			}
		}

		if(haveSubloc == 0) {
			document.getElementById('site_suburb').value = "";
		}
		if(haveLocality == 0) {
			document.getElementById('site_state').value = "";
		}
		if(havePostalCode == 0) {
			document.getElementById('site_zipCode').value = "";
		}
	}
	function fillInAddressContact() {

		var place 		   = autocomplete.getPlace();
		var haveRoute 	   = 0;
		var haveStreet     = 0;
		var haveSubloc     = 0;
		var haveLocality   = 0;
		var havePostalCode = 0;

		for (var i = 0; i < place.address_components.length; i++) {

			var addressType = place.address_components[i].types[0];

			var val = place.address_components[i][componentForm[addressType]];

			if (addressType == 'street_number') {
				street_number = val;
				haveStreet = 1;
				$("#street-clientPopup").val(street_number);
			}

			if (addressType == 'route') {
				route = val;
				haveRoute = 1;
				$("#street-clientPopup").val(street_number+" "+val);
			}

			if (addressType == 'locality') {
				haveSubloc = 1;
				document.getElementById('suburb-clientPopup').value = val;
			}

			if (addressType == 'administrative_area_level_1') {
				haveLocality = 1;
				document.getElementById('state-clientPopup').value = val;
			}

			if (addressType == 'postal_code') {
				havePostalCode = 1;
				document.getElementById('zipCode-clientPopup').value = val;
			}

			$("#lat-clientPopup").val(place.geometry.location.lat);
			$("#lng-clientPopup").val(place.geometry.location.lng);
		}


		if(haveStreet == 0 && haveRoute == 0) {
			document.getElementById('suburb').value = "";
		}
		if(haveSubloc == 0) {
			document.getElementById('suburb').value = "";
		}
		if(haveLocality == 0) {
			document.getElementById('state').value = "";
		}
		if(havePostalCode == 0) {
			document.getElementById('zipCode').value = "";
		}
	}
	function initMap() {

		var mapDiv = document.getElementById('map-canvas');

		map = new google.maps.Map(document.getElementById('map-canvas'), {
			zoom: 15,
			disableDefaultUI: true,
			center: {lat: 28.517957, lng: -81.36591199999999}
		});

		var geocoder = new google.maps.Geocoder();

		autocomplete = new google.maps.places.Autocomplete((document.getElementById('address')),{types: ['geocode']});
		autocomplete.addListener('place_changed', fillInAddress);

		document.getElementById('address').addEventListener('change', function() {
			geocodeAddress(geocoder, map, 1);
		});


		for(var ek in eventMoves){

			google.maps.event.addListener(map, eventMoves[ek], function() {
				
				var gLink = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+map.getCenter().lat()+","+map.getCenter().lng()+"&sensor=true&key=AIzaSyC2Qg3ywv9Dhg-Ptjv-5fG3I3UEt0rLufQ"

				$.post(gLink,function(response){

					if(response.results.length > 0){
						$("#address").val(response.results[0].formatted_address);
						place = response.results[0];
						lat = place.geometry.location.lat;
						lng = place.geometry.location.lng;
						if($("#client_details_same:checked").length > 0){
						$("#search_address").val(response.results[0].formatted_address);
						mapSite.setCenter(new google.maps.LatLng(lat, lng))
						}

						var street_number = "";

						for (var i = 0; i < place.address_components.length; i++) {
						var addressType = place.address_components[i].types[0];
						var val = place.address_components[i][componentForm[addressType]];

						if (addressType == 'street_number') {
							street_number = val;
						}

						if (addressType == 'route') {
							route = val;
							$("#street").val(street_number+" "+val);
							if($("#client_details_same:checked").length > 0){
								$("#site_street").val(val);
							}
						}

						if (addressType == 'locality') {
							$('#suburb').val(val);
							if($("#client_details_same:checked").length > 0){
								$("#site_suburb").val(val);
							}
						}

						if (addressType == 'administrative_area_level_1') {
							$('#state').val(val);
							if($("#client_details_same:checked").length > 0){
								$("#site_state").val(val);
							}
						}

						if (addressType == 'postal_code') {
							document.getElementById('zipCode').value = val;
							if($("#client_details_same:checked").length > 0){
								$("#site_zipCode").val(val);
							}
						}

						}

						$("#lat").val(lat);
						$("#lng").val(lng);

					}
					else{
						$("#address").val("");
						$("#street").val("");
						$("#state").val("");
						$("#suburb").val("");
						$("#zipCode").val("");
					}
				});
			});
		}
		
		$('<div/>').addClass('centerMarker').appendTo(map.getDiv()).click(function() {
			
			var that = $(this);
			
			if (!that.data('win')) {
				that.data('win', new google.maps.InfoWindow({
					content: 'this is the center'
				}));
				that.data('win').bindTo('position', map, 'center');
			}
			that.data('win').open(map);
		});




		// map-canvas-site

		var mapDivSite = document.getElementById('map-canvas-site');
			mapSite = new google.maps.Map(mapDivSite, {
				zoom: 15,
				disableDefaultUI: true,
				center: {lat: 28.517957, lng: -81.36591199999999}
			});


		var geocoder = new google.maps.Geocoder();

		autocompleteSite = new google.maps.places.Autocomplete((document.getElementById('search_address')),{types: ['geocode']});
		autocompleteSite.addListener('place_changed', fillInAddressSite);


		document.getElementById('search_address').addEventListener('change', function() {
			geocodeAddress(geocoder, mapSite, 0);
		});

		for(var ek in eventMoves){
			google.maps.event.addListener(mapSite, eventMoves[ek], function() {
				
				var gLink = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+mapSite.getCenter().lat()+","+mapSite.getCenter().lng()+"&sensor=true&key=AIzaSyC2Qg3ywv9Dhg-Ptjv-5fG3I3UEt0rLufQ"

				$.post(gLink,function(response){
					if(response.results.length > 0){
						$("#search_address").val(response.results[0].formatted_address);
						place = response.results[0];

						for (var i = 0; i < place.address_components.length; i++) {
						var addressType = place.address_components[i].types[0];
						var val = place.address_components[i][componentForm[addressType]];

						if (addressType == 'street_number') {
							street_number = val;
						}

						if (addressType == 'route') {
							route = val;
							document.getElementById('site_street').value = street_number+" "+val;
						}

						if (addressType == 'locality') {
							document.getElementById('site_suburb').value = val;
						}

						if (addressType == 'administrative_area_level_1') {
							document.getElementById('site_state').value = val;
						}

						if (addressType == 'postal_code') {
							document.getElementById('site_zipCode').value = val;
						}
						}
					}
					else{
						$("#search_address").val("");
						$("#site_street").val("");
						$("#site_suburb").val("");
						$("#site_state").val("");
						$("#site_zipCode").val("");
					}
				});
			});
		}
		
		$('<div/>').addClass('siteCenterMarker').appendTo(mapSite.getDiv()).click(function() {
			var that = $(this);
			if (!that.data('win')) {
					that.data('win', new google.maps.InfoWindow({
					content: 'this is the center'
				}));
				that.data('win').bindTo('position', mapSite, 'center');
			}
			that.data('win').open(mapSite);
		});


		//static-map-canvas

		var mapDivSite = document.getElementById('static-map-canvas');
		var static_lat = document.getElementById('client_lat').value;
		var static_lng = document.getElementById('client_lng').value;
		
		mapStatic = new google.maps.Map(mapDivSite, {
			zoom: 15,
			disableDefaultUI: true
		});

		mapStatic.setCenter(new google.maps.LatLng(static_lat, static_lng))
		

		if ($("#static-edit-quote-map-canvas").length > 0) {

			editQuotemapDivSite = document.getElementById('static-edit-quote-map-canvas');
			editQuoteMapStatic = new google.maps.Map(editQuotemapDivSite, {
				zoom: 15,
				disableDefaultUI: true
			});
			
		}


		//editQuoteMapStatic.setCenter(new google.maps.LatLng(static_lat, static_lng))


		/* Sat */

		// map-canvas-contacts
		var mapDivContact = document.getElementById('map-canvas-contact');
		mapContact = new google.maps.Map(mapDivContact, { zoom: 15, disableDefaultUI: true, center: { lat: 28.517957, lng: -81.36591199999999 } } );
		
		var geocoder = new google.maps.Geocoder();


			autocompleteSite = new google.maps.places.Autocomplete((document.getElementById('address-clientPopup')),{types: ['geocode']});
			autocompleteSite.addListener('place_changed', fillInAddressContact);

		
			document.getElementById('address-clientPopup').addEventListener('change', function() {
				geocodeAddress(geocoder, mapContact, 0);
			});

			for(var ek in eventMoves){
				google.maps.event.addListener(mapContact, eventMoves[ek], function() {
					
					var gLink = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+mapContact.getCenter().lat()+","+mapContact.getCenter().lng()+"&sensor=true&key=AIzaSyC2Qg3ywv9Dhg-Ptjv-5fG3I3UEt0rLufQ"

					$.post(gLink,function(response){

						if(response.results.length > 0){

							$("#address-clientPopup").val(response.results[0].formatted_address);

							place = response.results[0];

							lat = place.geometry.location.lat;
							lng = place.geometry.location.lng;

							for (var i = 0; i < place.address_components.length; i++) {

								var addressType = place.address_components[i].types[0];

								var val = place.address_components[i][componentForm[addressType]];

								if (addressType == 'street_number') {
									street_number = val;
								}

								if (addressType == 'route') {
									route = val;
									document.getElementById('street-clientPopup').value = street_number+" "+val;
								}

								if (addressType == 'locality') {
									document.getElementById('suburb-clientPopup').value = val;
								}

								if (addressType == 'administrative_area_level_1') {
									document.getElementById('state-clientPopup').value = val;
								}

								if (addressType == 'postal_code') {
									document.getElementById('zipCode-clientPopup').value = val;
								}
							}
							$("#lat-clientPopup").val(lat);
							$("#lng-clientPopup").val(lng);

						}
						else{
							$("#address-clientPopup").val("");
							$("#street-clientPopup").val("");
							$("#suburb-clientPopup").val("");
							$("#state-clientPopup").val("");
							$("#zipCode-clientPopup").val("");
						}
					});
				});
			}
			

			/*
				$('<div/>').addClass('siteCenterMarker').appendTo(mapContact.getDiv()).click(function() {
					var that = $(this);
					if (!that.data('win')) {
							that.data('win', new google.maps.InfoWindow({
							content: 'this is the center'
						}));
						that.data('win').bindTo('position', mapSite, 'center');
					}
					that.data('win').open(mapSite);
				});
			*/
	}
	function geocodeAddress(geocoder, resultsMap, check) {
		var address = document.getElementById('address').value;
		geocoder.geocode({'address': address}, function(results, status) {
			if (status === 'OK') {
				resultsMap.setCenter(results[0].geometry.location);

				if(check == 1){

					var place = results[0];

					$("#lat").val(place.geometry.location.lat);
					$("#lng").val(place.geometry.location.lng);

					$("#lat-clientPopup").val(place.geometry.location.lat);
					$("#lng-clientPopup").val(place.geometry.location.lng);

					if($("#client_details_same:checked").length > 0){

						var lat = place.geometry.location.lat;
						var lng = place.geometry.location.lng;

						var address = $("#address").val();
						var street = $("#street").val();
						var state = $("#state").val();
						var suburb = $("#suburb").val();
						var zipCode = $("#zipCode").val();



						$("#search_address").val(address);
						$("#site_street").val(street);
						$("#site_state").val(state);
						$("#site_suburb").val(suburb);
						$("#site_zipCode").val(zipCode);

						$("#site_lat").val(lat);
						$("#site_lng").val(lng);
						
						mapSite.setCenter(new google.maps.LatLng(lat, lng))
					}
				}

			} else {
				//alert('Geocode was not successful for the following reason: ' + status);
			}
		});
	}
	function initializeNewQuotePopUp(){
		curQuoteScreen = 0;
		$("#selected_client").val('');
		$("#client_details_same").prop("checked",true);

		$("#description").val("");
		$("#clientName").val("");
		$("#contactName").val("");
		$("#contactEmail").val("");
		$("#contactPhone").val("");

		$("#address").val("2000 S Mills Ave, Orlando, FL 32806, USA");
		$("#street").val("2000 South Mills Avenue");
		$("#suburb").val("Orlando");
		$("#state").val("Florida");
		$("#zipCode").val("32806");

		$('#client_details_same').trigger('change');

		map.setCenter(new google.maps.LatLng(28.517957, -81.36591199999999))
		mapStatic.setCenter(new google.maps.LatLng(28.517957, -81.36591199999999))
		mapContact.setCenter(new google.maps.LatLng(28.517957, -81.36591199999999))
		//mapSite.setCenter(new google.maps.LatLng(28.517957, -81.36591199999999))
	}
	function initializeEditQuotePopUp(curQuote){
		curQuoteScreen = 0;
		$("#selected_client").val(curQuote.contact.contact_id);

		// $("#description").val(curQuote.description);
		// $("#clientName").val(curQuote.client_name);
		// $("#contactName").val(curQuote.contact.name);
		// $("#contactEmail").val(curQuote.contact.email);
		// $("#contactPhone").val(curQuote.contact.phone);

		$("#address").val(curQuote.contact.formatted_addr);
		$("#street").val(curQuote.contact.street1);
		$("#suburb").val(curQuote.contact.suburb);
		$("#state").val(curQuote.contact.state);
		$("#zipCode").val(curQuote.contact.postal);

		if(curQuote.site.s_checked == 0){
			$("#client_details_same").prop("checked",false);

			$("#search_address").val(curQuote.site.formatted_addr);
			$("#site_state").val(curQuote.site.state);
			$("#site_suburb").val(curQuote.site.suburb);
			$("#site_zipCode").val(curQuote.site.postal);
			$("#site_street").val(curQuote.site.street1);

			mapSite.setCenter(new google.maps.LatLng(curQuote.site.lat, curQuote.site.lng))
		}
		else{
			$("#client_details_same").prop("checked",true);
		}

		$('#client_details_same').trigger('change')

		map.setCenter(new google.maps.LatLng(curQuote.contact.lat, curQuote.contact.lng))
		mapStatic.setCenter(new google.maps.LatLng(curQuote.contact.lat, curQuote.contact.lng))
		//mapSite.setCenter(new google.maps.LatLng(28.517957, -81.36591199999999))

		console.log(curQuote.site.lat, curQuote.site.lng);
		editQuoteMapStatic.setCenter(new google.maps.LatLng(curQuote.site.lat, curQuote.site.lng))
		//editQuoteMapStatic.setCenter(new google.maps.LatLng(-37.6653786, 145.0167694))
	}
	function initializeContactEditPopUp(){
		
		curQuoteScreen = 0;
		$("#address-clientPopup").val("2000 S Mills Ave, Orlando, FL 32806, USA");
		$("#street-clientPopup").val("2000 South Mills Avenue");		
		$("#suburb-clientPopup").val("Orlando");
		$("#state-clientPopup").val("Florida");
		$("#zipCode-clientPopup").val("32806");
		$('#client_details_same').trigger('change');
		map.setCenter(new google.maps.LatLng(28.517957, -81.36591199999999));
		mapSite.setCenter(new google.maps.LatLng(28.517957, -81.36591199999999));
		mapContact.setCenter(new google.maps.LatLng(28.517957, -81.36591199999999));
		// mapStatic.setCenter(new google.maps.LatLng(28.517957, -81.36591199999999))
	}

	$(document).ready(function(){

		saveType = $("#saveQuote").data('savetype');
		$(".contact_li").click(function(){
			//alert("Asa");
			var cid = $(this).data('id');
			$('.edit-existing-contact').data("contact_id", cid)



			var pkid = $(this).data("id");
			var name = $(this).data("name");
			var image = $(this).data("image");
			var phone = $(this).data("phone");
			var email = $(this).data("email");
			var lat = $(this).data("lat");
			var lng = $(this).data("lng");

			var street1 = $(this).data("street1");
			var suburb = $(this).data("suburb");
			var client_state = $(this).data("state");
			var postal_code = $(this).data("postal");

			var formatted_addr = $(this).data("formatted_addr");

			$("#client_image img").attr("src",image)
			$("#client_name").html(name)
			$("#client_email").html(email+"<br>"+phone)
			$("#client_address").html(formatted_addr)
			$("#selected_client").val(pkid);


			$("#clientName").val(name);
			$("#contactName").val(name);
			$("#contactEmail").val(email);
			$("#contactPhone").val(phone);
			$("#address").val(formatted_addr);
			$("#street").val(street1);
			$("#suburb").val(suburb);
			$("#state").val(client_state);
			$("#zipCode").val(postal_code);
			$("#lat").val(lat);
			$("#lng").val(lng);

			map.setCenter(new google.maps.LatLng(lat, lng))
			mapStatic.setCenter(new google.maps.LatLng(lat, lng))

			$("#menu3").removeClass('active show')
			$("#menu2").addClass('active show');

			$(".changeQuoteScreen[href='#menu3']").removeClass('active show');
			$(".changeQuoteScreen[href='#menu2']").addClass('active show');

			$('#client_details_same').trigger('change')
		});

		$(document).on('change','#client_details_same',function(){

			if($("#client_details_same:checked").length > 0){

				var lat = $("#lat").val();
				var lng = $("#lng").val();

				var address = $("#address").val();
				var state = $("#state").val();
				var suburb = $("#suburb").val();
				var zipCode = $("#zipCode").val();
				var street = $("#street").val();

				$("#search_address").val(address);
				$("#site_state").val(state);
				$("#site_suburb").val(suburb);
				$("#site_zipCode").val(zipCode);
				$("#site_street").val(street);

				mapSite.setCenter(new google.maps.LatLng(lat, lng))

				$("#map-canvas-site-overlay").css('z-index',1);

				$("#search_address").attr('disabled',true);
				$("#site_state").attr('disabled',true);
				$("#site_suburb").attr('disabled',true);
				$("#site_zipCode").attr('disabled',true);
				$("#site_street").attr('disabled',true);
			}
			else{
				$("#map-canvas-site-overlay").css('z-index',-1);
				$("#search_address").attr('disabled',false);
				$("#site_state").attr('disabled',false);
				$("#site_suburb").attr('disabled',false);
				$("#site_zipCode").attr('disabled',false);
				$("#site_street").attr('disabled',false);
			}
		});

		$(document).on('click','.changeQuoteScreen',function(){
			if($(this).data('screen')=='new'){
				curQuoteScreen = 0;
			}
			else{
				curQuoteScreen = 1;
			}
		});

		function saveQuote(contact_id){
			var description  = $("#description").val();
			var client_name  = $("#clientName").val();
			var site_contactName  = $("#site_contactName").val();
			var site_contactEmail  = $("#site_contactEmail").val();
			var site_contactPhone  = $("#site_contactPhone").val();
			var search_address  = $("#search_address").val();
			var site_lat  = $("#site_lat").val();
			var site_lng  = $("#site_lng").val();
			var site_street  = $("#site_street").val();
			var site_suburb  = $("#site_suburb").val();
			var site_state  = $("#site_state").val();
			var site_zipCode  = $("#site_zipCode").val();
			var company_id = $("#company_id").val();



			var csi  = $("#csi").val();
			var quoteSaveUrl = (saveType == 'create')?$("#quoteCreateUrl").val():$("#quoteUpdateUrl").val();
			quoteType=1;

			var s_checked = 0;
			if($("#client_details_same:checked").length > 0){
				s_checked = 1;
			}

			if((saveType == 'create')){
				var data = {'contact_id':contact_id,'client_name':client_name,'description':description,'s_checked':s_checked,'s_country':'','s_formatted_addr':search_address,'s_lat':site_lat,'s_long':site_lng,'s_postal':site_zipCode,'s_state':site_state,'s_street1':site_street,'s_street2':'','s_suburb':site_suburb,'subscriber_id':csi,'type':quoteType, company_id:company_id};
			}
			else{ 
				var curUrl = window.location.href;
				curUrlArr = curUrl.split('/');
				quote_id = curUrlArr[curUrlArr.length-1];
				var data = {'contact_id':contact_id,'client_name':client_name,'quote_id':quote_id,'description':description,'s_checked':s_checked,'s_country':'','s_formatted_addr':search_address,'s_lat':site_lat,'s_long':site_lng,'s_postal':site_zipCode,'s_state':site_state,'s_street1':site_street,'s_street2':'','s_suburb':site_suburb,'subscriber_id':csi,'type':quoteType, company_id:company_id};
			}

			$.ajax({
				url:quoteSaveUrl,
				method:'post',
				data:data,
				dataType:'JSON',
				success:function(response){
					if(response.success == 1){

						console.log( response );

						if((saveType == 'create')){

							quote_id = response.data.quote_id;
							window.location = siteBaseUrl+'/quote/view/'+quote_id;
						}else{
							//console.log(response);return;
							//window.location = window.location.href;
							setQuoteDetail();
							$('#InteriorQuote').modal('hide');
						}
					}
					else{

					}
				},
				error:function(error){

				},
			});

		}

		$(document).on('click','#saveQuote',function(){
			//console.log("curQuoteScreen : "+curQuoteScreen)
			//if(curQuoteScreen == 0){
				var clientName  = $("#clientName").val();
				var contactName  = $("#contactName").val();
				var contactEmail  = $("#contactEmail").val();
				var contactPhone  = $("#contactPhone").val();
				var address  = $("#address").val();
				var lat  = $("#lat").val();
				var lng  = $("#lng").val();
				var address  = $("#address").val();
				var street  = $("#street").val();
				var suburb  = $("#suburb").val();
				var state  = $("#state").val();
				var zipCode  = $("#zipCode").val();
				var csi  = $("#csi").val();
				var contactCreateUrl  = $("#contactCreateUrl").val();
				var contactUpdateUrl  = $("#contactUpdateUrl").val();

				var error = 0;
				if(clientName==""){
					alert('Please enter Client Name');
					error = 1;
				}
				else if(contactName==""){
					alert('Please enter Contact Name');
					error = 1;
				}
				else if(contactEmail==""){
					alert('Please enter Contact Email');
					error = 1;
				}
				else if(!regex.test(contactEmail)){
					alert('Please enter Valid Email');
					error = 1;
				}
				else if(contactPhone==""){
					alert('Please enter Contact Phone');
					error = 1;
				}
				else if(address==""){
					alert('Please enter Address');
					error = 1;
				}

				var contact_id = $("#selected_client").val();

				if(contact_id == ""){
					var data = {'force':0,'country':'','email':contactEmail,'formatted_addr':address,'lat':lat,'long':lng,'name':contactName,'phone':contactPhone,'postal':zipCode,'state':state,'street1':street,'street2':'','subscriber_id':csi,'suburb':suburb};
					var url = contactCreateUrl;
				}
				else{
					var data = {'contact_id':contact_id,'country':'','email':contactEmail,'formatted_addr':address,'lat':lat,'long':lng,'name':contactName,'phone':contactPhone,'postal':zipCode,'state':state,'street1':street,'street2':'','subscriber_id':csi,'suburb':suburb};
					var url = contactUpdateUrl;
				}
				if(error == 0){
					$.ajax({
						url:url,
						method:'post',
						data:data,
						dataType:'JSON',
						success:function(response){
							if(response.success == 1){
								saveQuote(response.data.contact_id);
							}
							else if(response.success == 2){
								var x = confirm('This Email already exists. Do you want to add contact with duplicate email?');
								if(x){
									var data = {'force':1,'country':'','email':contactEmail,'formatted_addr':address,'lat':lat,'long':lng,'name':contactName,'phone':contactPhone,'postal':zipCode,'state':state,'street1':street,'street2':'','subscriber_id':csi,'suburb':suburb};
									$.ajax({
										url:url,
										method:'post',
										data:data,
										dataType:'JSON',
										success:function(response){
											if(response.success == 1){
												saveQuote(response.data.contact_id);
											}
											else{
												
											}
										},
										error:function(error){
				
										},
									});
								}else{

								}
							}
							else{

							}
						},
						error:function(error){

						},
					});
				}

				
			/* }
			else{
				contact_id = $("#selected_client").val();
				saveQuote(contact_id);
			} */
		});
		
		$(document).on('click','.quoteTypeSelect',function(){
			quoteType = $(this).data('type');
			initializeNewQuotePopUp();
			if(quoteType == 1){
				$(".quoteTypeText").html("New Interior Quote");
			}
			else{
				$(".quoteTypeText").html("New Exterior Quote");
			}
		});

		$(document).on('change','.getChange',function(){
			console.log($(this).data('tochange'));
			if($("#client_details_same:checked").length > 0){
				var toChange = $(this).data('tochange');
				var curVal = $(this).val();
				$("#"+toChange).val(curVal);
			}
		});

		$(document).on('change','#clientName',function(){
			$("#contactName").val($("#clientName").val())
		});

	});

